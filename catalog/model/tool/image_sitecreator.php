<?php
/*
 *  LICENSE
 *  Opencart
 *  image model  for OpenCart 1.5 & 2+
 *  Copyright (c) 2017-2018 sitecreator.ru
 *  version 1.7.0 (sitecreator.ru)
 *  Права на данный файл принадлежат разработчику - sitecreator.ru
 *  Копирование и распространение без согласия разработчика (sitecreator.ru) не допускается.
 *
 */
class ModelToolImage extends Model {
  public function resize($filename, $width, $height, $type = '') {

    $oc23 = (version_compare(VERSION, "2.3", ">="))? true:false;
    $oc15 = (version_compare(VERSION, "2.0", "<"))? true:false;

    // получим кешированные настройки если доступны (и не устарели) или создадим новый кеш
    $setting = $this->cache->get('watermark_by_sitecreator');

    if(empty($setting)) { // отсутствие файла кеша либо кеш просрочен === false
      $this->load->model('setting/setting');
      $setting = $this->model_setting_setting->getSetting('watermark_by_sitecreator');
      $this->cache->set('watermark_by_sitecreator', $setting);
    }

    $enable_no_image = (!empty($setting['watermark_by_sitecreator_no_image'])) ? true : false;  // если по указанному пути не найден исходник, то подставить no_image
    $enable_crop_by_theme = (!empty($setting['watermark_by_sitecreator_crop_by_theme'])) ? true : false;  // даем  шаблону приоритет в управлении адаптивной обрезкой

    if ($enable_no_image) {
      if (!is_file(DIR_IMAGE . $filename)) {
        if (is_file(DIR_IMAGE . 'no_image.jpg')) {
          $filename = 'no_image.jpg';
        } elseif (is_file(DIR_IMAGE . 'no_image.png')) {
          $filename = 'no_image.png';
        } else return false;
      }
    } elseif (!is_file(DIR_IMAGE . $filename)) return;


    $extension = pathinfo($filename, PATHINFO_EXTENSION);
  $image_old = $filename;
  $image_new = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . (int)$width . 'x' . (int)$height . '.' . $extension;

  if (!is_file(DIR_IMAGE . $image_new) || (filectime(DIR_IMAGE . $image_old) > filectime(DIR_IMAGE . $image_new))) {
    $image_info = getimagesize(DIR_IMAGE . $image_old);
    list($width_orig, $height_orig, $image_type) = $image_info;

    if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF)) && strtolower($extension) != 'svg') return DIR_IMAGE . $image_old;

    $path = '';
    $directories = explode('/', dirname(str_replace('../', '', $image_new)));
    foreach ($directories as $directory) {
      $path = $path . '/' . $directory;
      if (!is_dir(DIR_IMAGE . $path)) @mkdir(DIR_IMAGE . $path, 0777);
    }


    if (empty($setting['watermark_by_sitecreator_status'])) $setting['watermark_by_sitecreator_status'] = 0;
    if (empty($setting['watermark_by_sitecreator_posx'])) $setting['watermark_by_sitecreator_posx'] = 0;
    if (empty($setting['watermark_by_sitecreator_posy'])) $setting['watermark_by_sitecreator_posy'] = 0;
    if (empty($setting['watermark_by_sitecreator_degree'])) $setting['watermark_by_sitecreator_degree'] = 0;
    if (empty($setting['watermark_by_sitecreator_width'])) $setting['watermark_by_sitecreator_width'] = 100;
    if (empty($setting['watermark_by_sitecreator_height'])) $setting['watermark_by_sitecreator_height'] = 100;
    if (empty($setting['watermark_by_sitecreator_opacity'])) $setting['watermark_by_sitecreator_opacity'] = 100;
    if (empty($setting['watermark_by_sitecreator_quality'])) $setting['watermark_by_sitecreator_quality'] = 80;
    if (empty($setting['watermark_by_sitecreator_min_width'])) $setting['watermark_by_sitecreator_min_width'] = 101;
    if (empty($setting['watermark_by_sitecreator_max_width'])) $setting['watermark_by_sitecreator_max_width'] = 899;
    if (empty($setting['watermark_by_sitecreator_dirs'])) $setting['watermark_by_sitecreator_dirs'] = [];
    if (empty($setting['watermark_by_sitecreator_dirs_noTrim'])) $setting['watermark_by_sitecreator_dirs_noTrim'] = [];
    if (empty($setting['watermark_by_sitecreator_crop_type'])) $setting['watermark_by_sitecreator_crop_type'] = ''; // $crop_type - тип обрезки: w h auto
    if (empty($setting['watermark_by_sitecreator_test_compressing'])) $setting['watermark_by_sitecreator_test_compressing'] = '';

    $minW = $setting['watermark_by_sitecreator_min_width'];
    $maxW = $setting['watermark_by_sitecreator_max_width'];


    $compare_file_size = (!empty($setting['watermark_by_sitecreator_test_compressing'])) ? true : false; // сравнения разных способов сжатия
    $extra_parameters['webp_jpeg'] = (!empty($setting['watermark_by_sitecreator_webp_enable_jpeg'])) ? true : false;
    $extra_parameters['webp_png'] = (!empty($setting['watermark_by_sitecreator_webp_enable_png'])) ? true : false;
    $extra_parameters['mozjpeg'] = (!empty($setting['watermark_by_sitecreator_mozjpeg_enable'])) ? true : false;
    $extra_parameters['optipng'] = (!empty($setting['watermark_by_sitecreator_optipng_enable'])) ? true : false;
    $extra_parameters['webp_png_lossless'] = (!empty($setting['watermark_by_sitecreator_webp_png_lossless'])) ? true : false;
    $extra_parameters['white_back'] = (!empty($setting['watermark_by_sitecreator_white_back'])) ? true : false;
    $extra_parameters['imagick_disable'] = (!empty($setting['watermark_by_sitecreator_imagick_disable'])) ? true : false;

    $extra_parameters['for_popup_img_noborder'] = (!empty($setting['watermark_by_sitecreator_for_popup_img_noborder'])) ? true : false;
    $extra_parameters['for_popup_img_fit_to_width_nocrop'] = (!empty($setting['watermark_by_sitecreator_for_popup_img_fit_to_width_nocrop'])) ? true : false;
    $extra_parameters['for_popup_img_no_max_fit'] = (!empty($setting['watermark_by_sitecreator_for_popup_img_no_max_fit'])) ? true : false;
    $extra_parameters['for_popup_img_white_back'] = (!empty($setting['watermark_by_sitecreator_for_popup_img_white_back'])) ? true : false;

    $extra_parameters['for_thumb_img_noborder'] = (!empty($setting['watermark_by_sitecreator_for_thumb_img_noborder'])) ? true : false;
    $extra_parameters['for_thumb_img_fit_to_width_nocrop'] = (!empty($setting['watermark_by_sitecreator_for_thumb_img_fit_to_width_nocrop'])) ? true : false;
    $extra_parameters['for_thumb_img_no_max_fit'] = (!empty($setting['watermark_by_sitecreator_for_thumb_img_no_max_fit'])) ? true : false;
    $extra_parameters['for_thumb_img_white_back'] = (!empty($setting['watermark_by_sitecreator_for_thumb_img_white_back'])) ? true : false;

    // параметры работы с фоном исходника
    $extra_parameters['enable_trim'] = (!empty($setting['watermark_by_sitecreator_enable_trim'])) ? true : false;
    $extra_parameters['trim_cache'] = (!empty($setting['watermark_by_sitecreator_trim_cache'])) ? true : false;
    $extra_parameters['enable_multitrim'] = (!empty($setting['watermark_by_sitecreator_enable_multitrim'])) ? true : false;
    $extra_parameters['border_after_trim1'] = (!empty($setting['watermark_by_sitecreator_border_after_trim1'])) ? true : false;
    $extra_parameters['enable_color_for_fill'] = (!empty($setting['watermark_by_sitecreator_enable_color_for_fill'])) ? true : false;
    // заливка бордера цветом фона
    $extra_parameters['enable_border_fill'] = (!empty($setting['watermark_by_sitecreator_enable_border_fill'])) ? true : false;
    $extra_parameters['fuzz'] = (!empty($setting['watermark_by_sitecreator_fuzz'])) ? $setting['watermark_by_sitecreator_fuzz'] : 1000;
    $extra_parameters['trim_border'] = (!empty($setting['watermark_by_sitecreator_trim_border'])) ? $setting['watermark_by_sitecreator_trim_border'] : 1000;

    $trim_maxi_w = (!empty($setting['watermark_by_sitecreator_trim_maxi_w'])) ? $setting['watermark_by_sitecreator_trim_maxi_w'] : 800;
    $trim_maxi_h = (!empty($setting['watermark_by_sitecreator_trim_maxi_h'])) ? $setting['watermark_by_sitecreator_trim_maxi_h'] : 800;
    $trim_mini_w = (!empty($setting['watermark_by_sitecreator_trim_mini_w'])) ? $setting['watermark_by_sitecreator_trim_mini_w'] : 50;
    $trim_mini_h = (!empty($setting['watermark_by_sitecreator_trim_mini_h'])) ? $setting['watermark_by_sitecreator_trim_mini_h'] : 50;


    $extra_parameters['optipng_level'] = (!empty($setting['watermark_by_sitecreator_optipng_level'])) ? $setting['watermark_by_sitecreator_optipng_level'] : 2;
    $extra_parameters['webp_quality'] = (!empty($setting['watermark_by_sitecreator_webp_quality'])) ? $setting['watermark_by_sitecreator_webp_quality'] : 85;
    $extra_parameters['quality'] = (!empty($setting['watermark_by_sitecreator_quality'])) ? $setting['watermark_by_sitecreator_quality'] : 90;

    // качество для ВСЕХ маленьких изображений с размерами равными или меньше img_mini_w img_mini_h  (по ширине ИЛИ высоте)
    $extra_parameters['mini_quality'] = (!empty($setting['watermark_by_sitecreator_mini_quality'])) ? $setting['watermark_by_sitecreator_mini_quality'] : 78;
    $extra_parameters['img_mini_w'] = (!empty($setting['watermark_by_sitecreator_img_mini_w'])) ? $setting['watermark_by_sitecreator_img_mini_w'] : 90;
    $extra_parameters['img_mini_h'] = (!empty($setting['watermark_by_sitecreator_img_mini_h'])) ? $setting['watermark_by_sitecreator_img_mini_h'] : 90;
    // только при условии "И" (ширина и высота соответсвуют)
    $extra_parameters['img_mini_if_and'] = (!empty($setting['watermark_by_sitecreator_img_mini_if_and'])) ? true : false;
   
    // качество для ВСЕХ БОЛЬШИХ изображений с размерами равными или больше img_maxi_w img_maxi_h  (по ширине ИЛИ высоте)
    $extra_parameters['maxi_quality'] = (!empty($setting['watermark_by_sitecreator_maxi_quality'])) ? $setting['watermark_by_sitecreator_maxi_quality'] : 85;
    $extra_parameters['img_maxi_w'] = (!empty($setting['watermark_by_sitecreator_img_maxi_w'])) ? $setting['watermark_by_sitecreator_img_maxi_w'] : 800;
    $extra_parameters['img_maxi_h'] = (!empty($setting['watermark_by_sitecreator_img_maxi_h'])) ? $setting['watermark_by_sitecreator_img_maxi_h'] : 800;
    // только при условии "И" (ширина и высота соответсвуют)
    $extra_parameters['img_maxi_if_and'] = (!empty($setting['watermark_by_sitecreator_img_maxi_if_and'])) ? true : false;
    $extra_parameters['img_maxi_no_compress'] = (!empty($setting['watermark_by_sitecreator_img_maxi_no_compress'])) ? true : false;


    $format_jpeg = preg_match('#\.jpe?g$#i', $image_old);

    $extra_parameters['image_popup_width'] = 0;
    $extra_parameters['image_popup_height'] = 0;


    if(!$oc23) { // 1.5 ... 2.2
      $pre_config = 'config_';
    } else { // 2.3
      $pre_config = $this->config->get('config_theme').'_'; // название темы (theme_defaul, например)
    }

    // если заданы параметры обработки всплывающих изображений
    if($extra_parameters['for_popup_img_noborder'] || $extra_parameters['for_popup_img_fit_to_width_nocrop'] ||
      $extra_parameters['for_popup_img_no_max_fit'] || $extra_parameters['for_popup_img_white_back']) // не создавать белые поля для всплывающих изображений
    {
      $extra_parameters['image_popup_width'] = $this->config->get($pre_config.'image_popup_width');
      $extra_parameters['image_popup_height'] = $this->config->get($pre_config.'image_popup_height');
    }

    // если заданы параметры обработки БОЛЬШИХ (thumb) изображений товара
    if($extra_parameters['for_thumb_img_noborder'] || $extra_parameters['for_thumb_img_fit_to_width_nocrop'] ||
      $extra_parameters['for_thumb_img_no_max_fit'] || $extra_parameters['for_thumb_img_white_back']) // не создавать белые поля для всплывающих изображений
    {
      $extra_parameters['image_thumb_width'] = $this->config->get($pre_config.'image_thumb_width');
      $extra_parameters['image_thumb_height'] = $this->config->get($pre_config.'image_thumb_height');
    }


    if (strtolower($extension) != 'svg' && !empty($width_orig) && ($width_orig != $width || $height_orig != $height || $setting['watermark_by_sitecreator_status']
      || ($format_jpeg && $extra_parameters['mozjpeg']) || (!$format_jpeg && $extra_parameters['optipng']))) {	// если watermart нужен, то всегда
      // пережмем всегда если разрешено суперсжатие для соответствующего формата, даже если размеры совпадают, иначе суперсжатие не сработает
      $image = new Image(DIR_IMAGE . $image_old);

      if (empty($crop_type)) { // по умолчанию возьмем из настроек модуля
        $crop_type = $setting['watermark_by_sitecreator_crop_type'];
      }
      if(!empty($type) && !empty($enable_crop_by_theme)) $crop_type = $type; // если есть значение на входе, то отдаем ему приоритет если разрешено

      $path_img = dirname($image_old);

      // проверить исключения для обрезки
      $dirs_noTrim = $setting['watermark_by_sitecreator_dirs_noTrim']; // список директорий для исключения обрезки исходника
      foreach($dirs_noTrim as $dir_off) {
        // является ли $dir_off  частью пути $path_img и НАЧИНАЕТСЯ с этого пути
        // либо сам файл изображения есть в списке исключения
        // регистр учитываем
        if (strpos($path_img, $dir_off) === 0 || $image_old == $dir_off) {
          $extra_parameters['enable_trim'] = false;  // запрет обрезки
          break;
        }
      }

      if(($width >= $trim_maxi_w && $height >= $trim_maxi_h) || ($width <= $trim_mini_w && $height <= $trim_mini_h)) $extra_parameters['enable_trim'] = false;  // запрет обрезки

      $image->resize($width, $height, $crop_type, $extra_parameters); // $type - тип обрезки: w h auto

      $reg = '/nowatermark|no_image/i'; // название (части) папки или часть имени в файле  для исключения watermark
      if (!preg_match($reg, $image_old) && $width >= $minW && $width <= $maxW && $height >= $minW && $height <= $maxW) {
        // исключить директории

        $dirs_off = $setting['watermark_by_sitecreator_dirs']; // список директорий для исключения watermark


        $path_img = dirname($image_old);
        $watermark_break = false;

        foreach($dirs_off as $dir_off) {
          // является ли $dir_off  частью пути $path_img и НАЧИНАЕТСЯ с этого пути
          // либо сам файл изображения есть в списке исключения
          // регистр учитываем
          if (strpos($path_img, $dir_off) === 0 || $image_old == $dir_off) {
            $watermark_break = true;
            break;
          }
        }


        // если не отменено как исключение для категорий и есть картинка для watermark, и есть статус=включено (1)
        if (!$watermark_break && $setting['watermark_by_sitecreator_status'] && !empty($setting['watermark_by_sitecreator_image']))
          $image->watermark(DIR_IMAGE . $setting['watermark_by_sitecreator_image'],
            $setting['watermark_by_sitecreator_posx'],
            $setting['watermark_by_sitecreator_posy'],
            $setting['watermark_by_sitecreator_degree'],
            $setting['watermark_by_sitecreator_width'],
            $setting['watermark_by_sitecreator_height'],
            $setting['watermark_by_sitecreator_opacity']);

      }


      // запись изображения с заданным настройкой качеством
      $image->save(DIR_IMAGE . $image_new, $setting['watermark_by_sitecreator_quality'], true, $compare_file_size, $extra_parameters);
    } else copy(DIR_IMAGE . $image_old, DIR_IMAGE . $image_new);
  }



  $imagepath_parts = explode('/', $image_new);
  // приведено к единообразию согласно
  //https://github.com/myopencart/ocStore/commit/7bc6b37c5c2fece3b3cd6d560e528539b05bd5af
  $image_new = implode('/', array_map('rawurlencode', $imagepath_parts));

  if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')))
    $domain = (defined('HTTPS_CATALOG'))? HTTPS_CATALOG: HTTPS_SERVER;
  else $domain = (defined('HTTP_CATALOG'))? HTTP_CATALOG: HTTP_SERVER;
  return $domain . 'image/' . $image_new;
	}
}
