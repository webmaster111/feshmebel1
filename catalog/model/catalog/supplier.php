<?php
class ModelCatalogSupplier extends Model {
	public function getSupplier($supplier_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "suppliers  WHERE supplier_id = '" . (int)$supplier_id ."'");

		return $query->row;
	}

	public function getSuppliers($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "suppliers";

			$sort_data = array(
				'name',
				'sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$supplier_data = $this->cache->get('supplier.' . (int)$this->config->get('config_store_id'));

			if (!$supplier_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "suppliers ORDER BY name");

				$supplier_data = $query->rows;

				$this->cache->set('supplier.' . (int)$this->config->get('config_store_id'), $supplier_data);
			}

			return $supplier_data;
		}
	}
}