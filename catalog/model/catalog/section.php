<?php
class ModelCatalogSection extends Model {
	public function getSection($section_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "section i LEFT JOIN " . DB_PREFIX . "section_description id ON (i.section_id = id.section_id) LEFT JOIN " . DB_PREFIX . "section_to_store i2s ON (i.section_id = i2s.section_id) WHERE i.section_id = '" . (int)$section_id . "' AND id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1'");

		return $query->row;
	}

	public function getSections($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "section i LEFT JOIN " . DB_PREFIX . "section_description id ON (i.section_id = id.section_id) LEFT JOIN " . DB_PREFIX . "section_to_store i2s ON (i.section_id = i2s.section_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.category_id = '".$category_id."' ORDER BY i.sort_order, LCASE(id.title) ASC");

		return $query->rows;
	}

}