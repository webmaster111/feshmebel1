<?php
class ModelCatalogCategory extends Model {
	public function getCategoryFiltersSection($category_id,$filter_group_id) {
		$query = $this->db->query("
SELECT f.filter_id, fd.name, fd.filter_group_url, fd.filter_url FROM `oc_product` as p
LEFT JOIN `oc_product_to_category` as ptc ON ptc.product_id = p.product_id
LEFT JOIN `oc_product_filter` as pf ON pf.product_id = p.product_id
LEFT JOIN `oc_filter` as f ON f.filter_id = pf.filter_id
LEFT JOIN `oc_filter_description` as fd ON fd.filter_id = f.filter_id
WHERE fd.language_id = " . (int)$this->config->get('config_language_id') . " and p.stock_status_id = 7 AND ptc.category_id = ".$category_id." AND f.filter_group_id in  (".$filter_group_id.") 
GROUP BY 
filter_id ORDER BY RAND() 
");

		return $query->rows;
	}

	public function getCategory($category_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row;
	}

	public function getCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}

    public function getCategoriesHome() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.status_home = '1' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
		return $query->rows;
	}

    public function getCategories2($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c 
		LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) 
		LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) 
		WHERE c.parent_id2 = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

		return $query->rows;
	}

    public function getCategoriesMod($parent_id = 0,$language_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$language_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }


    public function getCategoriesById($cat_id_arr = 0) {
		if(count($cat_id_arr)){
			$comma_separated = implode(",", $cat_id_arr);
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id IN (" . $comma_separated . ") AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

			return $query->rows;
		}else{
			return false;
		}
	}

	public function getCategoryFilters($category_id) {
		$implode = array();

		$query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();

				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}

				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}

		return $filter_group_data;
	}

	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");

		return $query->row['total'];
	}
	
	// start Tkach web-promo
	public function getCategoryFiltersSeoUrl(){//`oc_category_filter`
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter, " . DB_PREFIX . "filter_description WHERE " . DB_PREFIX . "category_filter.filter_id = " . DB_PREFIX . "filter_description.filter_id");

			/* foreach ($CatFil->rows as $row)
			    {
			    	$filterURL["category_id"] 		= $row["category_id"];
			    	$filterURL["language_id"] 		= $row["language_id"];
			    	$filterURL["filter_id"] 		= $row["filter_id"];
			    	$filterURL["filter_group_url"] 	= $row["filter_group_url"];
			    	$filterURL["filter_url"] 		= $row["filter_url"];
			    }
		*/
		return $query->rows;
	}
	
	public function getMyLanguages(){
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language`");

		return $query->rows;
	}
	
	public function getFilterDescriptions(){
		$query = $this->db->query("SELECT `filter_url`, `description`, `name`, `meta_title`, `meta_h1`, `meta_description`, `language_id`, `showmeta` FROM `" . DB_PREFIX . "filter_url_description`");

		 $filterDescription = array();
		 

		 
		 foreach ($query->rows as $row => $value)
		    {
		    	$filterDescription[$row]["filter_url"] 			= $value["filter_url"];
		    	$filterDescription[$row]["description"] 		= $value["description"];
		    	$filterDescription[$row]["name"] 		= $value["name"];
		    	$filterDescription[$row]["meta_title"] 		= $value["meta_title"];
                $filterDescription[$row]["meta_h1"] 		= $value["meta_h1"];
		    	$filterDescription[$row]["meta_description"] 		= $value["meta_description"];
		    	$filterDescription[$row]["language_id"] 		= $value["language_id"];
		    	$filterDescription[$row]["showmeta"] 		= $value["showmeta"];
		    }

		return $filterDescription;
	}
    public function getFilterDescriptionsUrl($url){
        $value  = $this->db->query("SELECT `filter_url`, `description`, `name`, `meta_title`, `meta_h1`, `meta_description`, `language_id`, `showmeta` FROM `" . DB_PREFIX . "filter_url_description` WHERE `filter_url` = '".$url."'")->row;
        if(!isset( $value["filter_url"]))
            return array();
        return array(
            "filter_url" => $value["filter_url"],
            "description" => $value["description"],
            "name" => $value["name"],
            "meta_title" => $value["meta_title"],
            "meta_h1" => $value["meta_h1"],
            "meta_description" => $value["meta_description"],
            "language_id" => $value["language_id"],
            "showmeta" => $value["showmeta"],
        );
    }
}