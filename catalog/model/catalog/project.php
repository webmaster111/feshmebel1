<?php
class ModelCatalogProject extends Model {
	public function getProject($project_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project m LEFT JOIN " . DB_PREFIX . "project_to_store m2s ON (m.project_id = m2s.project_id) WHERE m.project_id = '" . (int)$project_id . "' AND m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		return $query->row;
	}

	public function getProjects($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "project m LEFT JOIN " . DB_PREFIX . "project_to_store m2s ON (m.project_id = m2s.project_id) WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

			$sort_data = array(
				'name',
				'sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$project_data = $this->cache->get('project.' . (int)$this->config->get('config_store_id'));

			if (!$project_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "project m LEFT JOIN " . DB_PREFIX . "project_to_store m2s ON (m.project_id = m2s.project_id) WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY name");

				$project_data = $query->rows;

				$this->cache->set('project.' . (int)$this->config->get('config_store_id'), $project_data);
			}

			return $project_data;
		}
	}
}