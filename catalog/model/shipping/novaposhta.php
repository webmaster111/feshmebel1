<?php
class ModelShippingNovaPoshta extends Model {
    public function __construct($registry) {
        parent::__construct($registry);

        require_once(DIR_SYSTEM . 'helper/novaposhta.php');

        $registry->set('novaposhta', new NovaPoshta($registry));
    }

    function getQuote($address) {
        $query = $this->db->query("
            SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone
            WHERE geo_zone_id = '" . (int) $this->config->get('novaposhta_geo_zone_id') . "'
            AND country_id = '" . (int)$address['country_id'] . "'
            AND (zone_id = '" . (int)$address['zone_id'] . "'
            OR zone_id = '0')");

        if (!$this->config->get('novaposhta_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $method_data = array();
        $this->load->language('shipping/novaposhta');

        $flat_text = $this->language->get('text_free');


        $this->load->model('extension/extension');

        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        // Display prices
        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status') && $result['code'] != 'shipping') {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                }
            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);
        }
        if($total < 15000)
            $flat_text = $this->language->get('text_free2');

        if ($status) {


            // Set vars
            $recipient_city_ref = '';
            $weight = 0;
            $volume_weight = 0;

            // Get recipient city Ref
            if (($this->config->get('novaposhta_cost') ||  $this->config->get('novaposhta_delivery_period')) && !empty($address['city'])) {
                $recipient_city_ref = $this->novaposhta->getCityRef($address['city']);
            }

            // Get products
            $products = $this->cart->getProducts();

            //Get subtotal
            $sub_total = $this->currency->convert($this->getSubTotal($products), $this->config->get('config_currency'), 'UAH');

            // Weight   
            $weight = $this->novaposhta->getWeight($products);

            $this->session->data['shippingdata']['cart_weight'] = $weight;

            // VolumeWeight
            if ($this->config->get('novaposhta_calculate_volume')) {
                $volume_weight = $this->novaposhta->getVolume($products) * 250;
            }
            $quote_data = array();

            $shipping_sub_methods = array(
                array(
                    'code' => 'warehouse',
                    'service_type' => 'Warehouse',
                    'status' => true
                ),
                array(
                    'code' => 'doors',
                    'service_type' => 'Doors',
                    'status' => $this->config->get('novaposhta_courier_shipping')
                )
            );

            foreach ($shipping_sub_methods as $method) {
                if (!$method['status']) {
                    continue;
                }

                $description = ($this->config->get('novaposhta_shipping_' . $method['code'] . '_name_' . $this->config->get('config_language_id'))) ? $this->config->get('novaposhta_shipping_' . $method['code'] . '_name_' . $this->config->get('config_language_id')) : $this->language->get('text_description_' . $method['code']);

                $cost = 0;
                $period = 0;

                // Cost of shipping
                if ($this->config->get('novaposhta_cost') && $weight && (!$this->config->get('novaposhta_free_shipping') || ($this->config->get('novaposhta_free_shipping') > 0 && $sub_total < $this->config->get('novaposhta_free_shipping')))) {
                    if ($this->config->get('novaposhta_api_calculation') && $recipient_city_ref) {
                        $properties_cost = array (
                            'Sender'		=> $this->config->get('novaposhta_sender'),
                            'CitySender'	=> $this->config->get('novaposhta_sender_city'),
                            'CityRecipient'	=> $recipient_city_ref,
                            'ServiceType'	=> $this->config->get('novaposhta_sender_address_type') . $method['service_type'],
                            'Weight'		=> $weight,
                            'VolumeWeight'	=> $volume_weight,
                            'Cost'			=> $sub_total,
                            'DateTime' 		=> date('d.m.Y')
                        );

                        $cost = $this->novaposhta->getDocumentPrice($properties_cost);
                    }

                    if ($this->config->get('novaposhta_tariff_calculation') && !$cost) {
                        if ($volume_weight > $weight) {
                            $weight = $volume_weight;
                        }

                        $cost = $this->novaposhta->tariffCalculation($this->config->get('novaposhta_sender_address_type') . $method['service_type'], $sub_total, $weight);
                    }

                    // Currency correcting
                    $currency_value = $this->currency->getValue('UAH');

                    if ($cost && $currency_value != 1) {
                        $cost /= $currency_value;
                    }
                }

                // Period of delivery
                if ($this->config->get('novaposhta_delivery_period') && $recipient_city_ref) {
                    $properties_period = array (
                        'CitySender'	=> $this->config->get('novaposhta_sender_city'),
                        'CityRecipient'	=> $recipient_city_ref,
                        'ServiceType'	=> $this->config->get('novaposhta_sender_address_type') . $method['service_type'],
                        'DateTime' 		=> date('d.m.Y')
                    );

                    $period = $this->novaposhta->getDocumentDeliveryDate($properties_period);
                }

                $quote_data[$method['code']] = array(
                    'code'         => 'novaposhta.' . $method['code'],
                    'title'        => $this->language->get('text_title'),
                    'sub_title'    => $this->language->get('text_description'),
                    'cost'         => $flat_text,
                    'tax_class_id' => $this->config->get('novaposhta_tax_class_id'),
                    'text'         => ($cost) ? $this->currency->format($this->tax->calculate($cost, $this->config->get('novaposhta_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']) : '',
                    'flat_text'         => $flat_text,
                    'text_period'  => $this->language->get('text_period'),
                    'period'  	   => $period
                );
            }

            $method_data = array(
                'code'       => 'novaposhta',
                'title'      => $this->language->get('text_title'),
                'quote'      => $quote_data,
                'sort_order' => $this->config->get('novaposhta_sort_order'),
                'error'      => false
            );
        }

        return $method_data;
    }

    private function getSubTotal($products) {
        $sub_total = 0;
        $totals = array();
        $taxes = $this->cart->getTaxes();

        $total_data = array(
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$sub_total
        );

        foreach ($products as $product) {
            $sub_total += $product['total'];
        }

        if (isset($this->session->data['coupon'])) {
            $this->load->model('total/coupon');

            if (version_compare(VERSION, '2.2', '>=')) {
                $this->model_total_coupon->getTotal($total_data);

            } else {
                $this->model_total_coupon->getTotal($totals, $sub_total, $taxes);
            }

        }

        if (isset($this->session->data['voucher'])) {
            $this->load->model('total/voucher');

            if (version_compare(VERSION, '2.2', '>=')) {
                $this->model_total_voucher->getTotal($total_data);

            } else {
                $this->model_total_voucher->getTotal($totals, $sub_total, $taxes);
            }
        }

        return $sub_total;
    }
}
