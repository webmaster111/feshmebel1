<?php
class ModelModuleShippingData extends Model {
    private $config_language;
    private $languages;

    public function __construct($registry) {
        parent::__construct($registry);


        /*
        if (isset($this->session->data['language2'])) {
            $query = $this->db->query("SELECT `value` FROM `" . DB_PREFIX . "setting` WHERE `key` = 'config_language'");
            $this->config_language = $query->row['value'];

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE status = '1'");

            foreach ($query->rows as $result) {
                $this->languages[$result['code']] = $result;
            }

            $code = $this->session->data['language2'];

            if (!isset($this->session->data['language']) || $this->session->data['language'] != $code) {
                $this->session->data['language'] = $code;
            }


            $xhttprequested =
                isset($this->request->server['HTTP_X_REQUESTED_WITH'])
                && (strtolower($this->request->server['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

            $captcha = isset($this->request->get['route']) && $this->request->get['route'] == 'tool/captcha';

            if (!$xhttprequested && !$captcha) {
                setcookie('language', $code, time() + 60 * 60 * 24 * 30, '/',
                    ($this->request->server['HTTP_HOST'] != 'localhost') ? $this->request->server['HTTP_HOST'] : false);
            }


            $this->config->set('config_language_id', $this->languages[$code]['language_id']);
            $this->config->set('config_language', $this->languages[$code]['code']);

            $language = new Language($this->languages[$code]['directory']);
            $language->load('default');
            $language->load($this->languages[$code]['directory']);
            $this->registry->set('language', $language);
        }
        */
    }
	public function getNovaPoshtaCities($area, $search = '') {
		$description = ($this->session->data['language2'] == 'ua') ? 'Description' : 'DescriptionRu';
		$area = $this->db->escape($area);
		$search = $this->db->escape($search);

		$sql ="SELECT `" . $description . "` as `Description` FROM `" . DB_PREFIX . "novaposhta_cities` WHERE `Area` = '" . $area . "'";

		if ($search) {
			$sql .= " AND  `" . $description . "` LIKE '" . $search . "%'";
		}

		$sql .= " ORDER BY  `" . $description . "`";

		return $this->db->query($sql)->rows;
	}

	public function getUkrPoshtaCities($area, $search = '') {
		$description = ($this->session->data['language2'] == 'ua') ? 'Description' : 'DescriptionRu';
		$area = $this->db->escape($area);
		$search = $this->db->escape($search);

		$sql ="SELECT `" . $description . "` as `Description` FROM `" . DB_PREFIX . "ukrposhta_cities` WHERE `Area` = '" . $area . "'";

		if ($search) {
			$sql .= " AND  `" . $description . "` LIKE '" . $search . "%'";
		}

		$sql .= " ORDER BY  `" . $description . "`";

		return $this->db->query($sql)->rows;
	}

	public function getNovaPoshtaWarehouses($city, $search = '') {

		$description = ($this->session->data['language2'] == 'ua') ? 'Description' : 'DescriptionRu';
		$city_description = ($this->session->data['language2'] == 'ua') ? 'CityDescription' : 'CityDescriptionRu';
		$city = $this->db->escape($city);
		$search = $this->db->escape($search);

		if (ctype_digit($search)) {
			$sql = "SELECT `" . $description . "` as `Description` FROM `" . DB_PREFIX . "novaposhta_warehouses` WHERE `" . $city_description . "` = '" . $city . "'";

			if ($search) {
				$sql .= " AND `Number` LIKE '" . $search . "%'";
			}
		} else {
			$sql = "SELECT `" . $description . "` as `Description` FROM `" . DB_PREFIX . "novaposhta_warehouses` WHERE `" . $city_description . "` = '" . $city . "'";

			if ($search) {
				$sql .= " AND `" . $description . "` LIKE '%" . $search . "%'";
			}
		}

		if (is_array($this->config->get('novaposhta_warehouse_types'))) {
			$warehouse_types = $this->config->get('novaposhta_warehouse_types');

			foreach ($warehouse_types as $k => $v) {
				$warehouse_types[$k] = "'" . $v . "'";
			}

			$sql .= " AND `TypeOfWarehouse` IN (" . implode(',', $warehouse_types) . ")";
		}

		if ($this->config->get('novaposhta_warehouses_filter_weight') && isset($this->session->data['shippingdata']['cart_weight'])) {
			$sql .= " AND (`TotalMaxWeightAllowed` >= '" . $this->session->data['shippingdata']['cart_weight'] . "' OR (`TotalMaxWeightAllowed` = 0 AND (`PlaceMaxWeightAllowed` >= '" . $this->session->data['shippingdata']['cart_weight'] . "' OR `PlaceMaxWeightAllowed` = 0)))";
		}

		$sql .= " ORDER BY `Number`+0";

		return $this->db->query($sql)->rows;
	}

    public function getUkrPoshtaWarehouses($city, $search = '') {
        $description = ($this->session->data['language2'] == 'ua') ? 'Description' : 'DescriptionRu';
        $city_description = ($this->session->data['language2'] == 'ua') ? 'CityDescription' : 'CityDescriptionRu';
        $city = $this->db->escape($city);
        $search = $this->db->escape($search);

        if (ctype_digit($search)) {
            $sql = "SELECT `" . $description . "` as `Description` FROM `" . DB_PREFIX . "ukrposhta_warehouses` WHERE `" . $city_description . "` = '" . $city . "'";

            if ($search) {
                $sql .= " AND `Number` LIKE '" . $search . "%'";
            }
        } else {
            $sql = "SELECT `" . $description . "` as `Description` FROM `" . DB_PREFIX . "ukrposhta_warehouses` WHERE `" . $city_description . "` = '" . $city . "'";

            if ($search) {
                $sql .= " AND `" . $description . "` LIKE '%" . $search . "%'";
            }
        }

        if (is_array($this->config->get('ukrposhta_warehouse_types'))) {
            $warehouse_types = $this->config->get('ukrposhta_warehouse_types');

            foreach ($warehouse_types as $k => $v) {
                $warehouse_types[$k] = "'" . $v . "'";
            }

            $sql .= " AND `TypeOfWarehouse` IN (" . implode(',', $warehouse_types) . ")";
        }

        if ($this->config->get('ukrposhta_warehouses_filter_weight') && isset($this->session->data['shippingdata']['cart_weight'])) {
            $sql .= " AND (`TotalMaxWeightAllowed` >= '" . $this->session->data['shippingdata']['cart_weight'] . "' OR (`TotalMaxWeightAllowed` = 0 AND (`PlaceMaxWeightAllowed` >= '" . $this->session->data['shippingdata']['cart_weight'] . "' OR `PlaceMaxWeightAllowed` = 0)))";
        }

        $sql .= " ORDER BY `Number`+0";

        return $this->db->query($sql)->rows;
    }
}