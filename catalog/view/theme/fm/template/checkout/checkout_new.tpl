<?php
	echo $header;
?>
<script>
    gtag('event', 'page_view', {
        'send_to': 'AW-971613570',
        'ecomm_prodid' : <?php echo $products_id; ?>,
        'ecomm_pagetype': 'cart',
        'ecomm_totalvalue' : '<?php echo (int)$totals[count($totals)-1]['text']; ?>'
    });
</script>

<!-- breadcrumbs -->
<section class="breadcrumbs ">
	<div class="wrapper">
		<ul class="breadcrumbs__items" itemscope itemtype="https://schema.org/BreadcrumbList">

			<?php $breadcrumbs_item = 0;
			 foreach ($breadcrumbs as $breadcrumb) { ?>
			<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
				<a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
					<span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
					<meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a>
			</li>
			<?php } ?>
			<!--ss_breadcrums_list:<?php foreach ($breadcrumbs as $k=>$breadcrumb) { ?><?php echo $breadcrumb['text']; ?><?php if(count($breadcrumb) != $k) { ?> >> <?php } ?><?php } ?>-->
		</ul>
	</div>
</section>

<style>
	.privatbank_paymentparts_pp .panel-default,
	.privatbank_paymentparts_ii .panel-default,
	.monobank .panel-default
	{
		padding: 10px;

	}
	.privatbank_paymentparts_pp .panel-default select,
	.privatbank_paymentparts_ii .panel-default select,
	.monobank .panel-default select
	{
		display: inline-block;
		width: 70px;
	}
	#simplemodal-container .modal {
		opacity: 1;
	}
	button[disabled], html input[disabled]
	{
		background-color: #fff;
	}
	select.form-control
	{
		border: 1px solid;
	}
	input[type="checkbox"]:checked,
	input[type="checkbox"]:not(:checked) {
		position: absolute;
		left: -9999px;
	}

	input[type="checkbox"]:checked + label,
	input[type="checkbox"]:not(:checked) + label {
		position: relative;
		padding-left: 30px;
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	input[type="checkbox"]:checked + label:before,
	input[type="checkbox"]:not(:checked) + label:before {
		position: absolute;
		content: "";
		width: 16px;
		height: 16px;
		background-color: transparent;
		border: 1px solid #808994;
		top: 0;
		left: 0;
	}

	input[type="checkbox"]:checked + label:after,
	input[type="checkbox"]:not(:checked) + label:after {
		position: absolute;
		content: "";
		width: 10px;
		height: 10px;
		background-color: #121726;
		top: 3px;
		left: 3px;
	}

	input[type="checkbox"]:not(:checked) + label:after {
		opacity: 0;
	}

	input[type="checkbox"]:checked + label:after {
		opacity: 1;
	}

	input[type="radio"]:checked,
	input[type="radio"]:not(:checked) {
		position: absolute;
		left: -9999px;
	}

	input[type="radio"]:checked + label,
	input[type="radio"]:not(:checked) + label {
		position: relative;
		padding-left: 30px;
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	input[type="radio"]:checked + label:before,
	input[type="radio"]:not(:checked) + label:before {
		position: absolute;
		content: "";
		width: 16px;
		height: 16px;
		background-color: transparent;
		border: 1px solid #808994;
		-webkit-border-radius: 50%;
		border-radius: 50%;
		top: 0;
		left: 0;
	}

	input[type="radio"]:checked + label:after,
	input[type="radio"]:not(:checked) + label:after {
		position: absolute;
		content: "";
		width: 10px;
		height: 10px;
		background-color: #121726;
		-webkit-border-radius: 50%;
		border-radius: 50%;
		top: 3px;
		left: 3px;
	}

	input[type="radio"]:not(:checked) + label:after {
		opacity: 0;
	}

	input[type="radio"]:checked + label:after {
		opacity: 1;
	}

	.select {
		height: 44px;
		border: 1px solid #C8CBD1;
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
	}

	.select.opened {
		border-bottom: 2px solid #121726;
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
	}

	.select .jq-selectbox__select {
		height: 44px;
		background: transparent;
		border: none !important;
		-webkit-box-shadow: none;
		box-shadow: none;
		padding: 0 40px 0 15px;
	}

	.select .jq-selectbox__select:active {
		-webkit-box-shadow: none;
		box-shadow: none;
		border: 1px solid #C8CBD1;
	}

	.select .jq-selectbox__select-text {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		min-width: 20px;
		height: 100%;
	}

	.select .jq-selectbox__trigger {
		border: none;
	}

	.select .jq-selectbox__trigger .jq-selectbox__trigger-arrow {
		top: 19px;
	}

	.select .jq-selectbox__dropdown {
		background: #f9f9f9;
		border: none;
		-webkit-box-shadow: 0px 8px 12px rgba(83, 95, 110, 0.18), 0px 4px 4px rgba(83, 95, 110, 0.25);
		box-shadow: 0px 8px 12px rgba(83, 95, 110, 0.18), 0px 4px 4px rgba(83, 95, 110, 0.25);
		margin-top: 20px;
	}

	.select .jq-selectbox__dropdown ul {
		max-height: 460px;
		padding-top: 10px;
	}

	.select .jq-selectbox__dropdown li {
		font-size: 12px;
		line-height: 16px;
		color: #121726;
	}

	.select .jq-selectbox__dropdown li.selected {
		background: rgba(200, 203, 209, 0.25);
	}

	.select .jq-selectbox__dropdown li:hover {
		background: rgba(200, 203, 209, 0.25);
	}

	.scrollbar-inner > .scroll-element .scroll-element_track {
		background: rgba(200, 203, 209, 0.25);
		opacity: 1;
	}

	.scrollbar-inner > .scroll-element .scroll-bar {
		background-color: #121726;
		opacity: 1;
		-webkit-border-radius: none;
		border-radius: none;
	}

	.scrollbar-inner > .scroll-element .scroll-bar:hover {
		background-color: #121726;
	}

	.scrollbar-inner > .scroll-element .scroll-bar:active {
		background-color: #121726;
	}

	.scrollbar-inner > .scroll-element .scroll-bar:focus {
		background-color: #121726;
	}

	.scrollbar-inner > .scroll-element .scroll-element_outer,
	.scrollbar-inner > .scroll-element .scroll-element_track,
	.scrollbar-inner > .scroll-element .scroll-bar {
		-webkit-border-radius: 0;
		border-radius: 0;
	}


	.checkout__field-form .eye-pass, .checkout__field-form .eye-pass:hover, .order__remove::before {
		background: url("/new-site/img/sprite.svg") no-repeat;
	}

	.checkout__field-form .eye-pass, .checkout__field-form .eye-pass:hover, .order__remove::before {
		background: url("/new-site/img/sprite.svg") no-repeat;
	}

	h1, .h1, .head {
		font-family: "GothamProMedium", sans-serif;
		font-size: 30px;
		line-height: 40px;
		letter-spacing: -0.5px;
		color: #121726;
		margin-top: 0;
		margin-left: 50px;
	}

	h3, .h3 {
		font-family: "GothamProMedium", sans-serif;
		font-size: 17px;
		line-height: 22px;
		color: #121726;
	}

	h4, .h4 {
		font-family: "GothamProMedium", sans-serif;
		font-size: 14px;
		line-height: 24px;
		letter-spacing: 0.5px;
		text-transform: uppercase;
		color: #121726;
	}

	h5, .h5 {
		font-family: "GothamProMedium", sans-serif;
		font-size: 12px;
		text-transform: uppercase;
	}

	@media screen and (max-width: 1199px) {
		h1, .h1 {
			font-size: 26px;
		}
	}

	@media screen and (max-width: 991px) {
		h1, .h1 {
			margin-left: 0;
		}
	}

	@media screen and (max-width: 575px) {
		h1, .h1 {
			margin-bottom: 40px;
		}
	}

	.facebook-btn {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		height: 44px;
		font-size: 13px;
		line-height: 18px;
		color: #808994;
		text-decoration: none;
		border: 1px solid #808994;
	}

	.facebook-btn::before {
		display: inline-block;
		content: "";
		width: 20px;
		height: 20px;
		margin-right: 20px;
		background: url("../img/svg/fb.svg") no-repeat center;
	}

	.facebook-btn:hover {
		color: #121726;
		border-color: #121726;
		text-decoration: none;
	}

	.google-btn {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		height: 44px;
		font-size: 13px;
		line-height: 18px;
		color: #808994;
		text-decoration: none;
		border: 1px solid #808994;
	}

	.google-btn::before {
		display: inline-block;
		content: "";
		width: 20px;
		height: 20px;
		margin-right: 20px;
		background: url("../img/svg/google.svg") no-repeat center;
	}

	.google-btn:hover {
		color: #121726;
		border-color: #121726;
		text-decoration: none;
	}

	.sign-up-btn {
		width: 100%;
		height: 44px;
		font-size: 11px;
		font-family: "GothamProMedium", sans-serif;
		text-transform: uppercase;
		color: #fff;
		background-color: #121726;
		border: none;
	}

	.basket-proceed-btn {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 100%;
		height: 44px;
		font-family: "GothamProMedium", sans-serif;
		font-size: 11px;
		line-height: 11px;
		letter-spacing: 1px;
		text-transform: uppercase;
		color: #121726;
		border: 1px solid #121726;
	}

	.checkout {
		padding-top: 30px;
	}

	.checkout__content {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		margin-top: 47px;
	}

	.checkout__head {
		color: #121726;
		font-family: "GothamProBold", sans-serif;
	}

	.checkout__way-registration {
		width: 48%;
	}

	.checkout__registration {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		border-bottom: 1px solid #C8CBD1;
		padding-bottom: 27px;
	}

	.checkout__guest, .checkout__registered {
		font-family: "GothamProMedium", sans-serif;
		font-size: 12px;
		text-transform: uppercase;
		letter-spacing: 0.5px;
		color: #C8CBD1;
		cursor: pointer;
	}
	.checkout__guest:hover, .checkout__registered:hover,
	.checkout__guest.select-item, .checkout__registered.select-item {
		color: #121726;
	}

	.checkout .select-item {
		color: #121726;
	}

	.checkout__registration-info {
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
		color: #535F6E;
		padding-top: 29px;
	}

	.checkout__sign-in-social {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		padding-top: 20px;
	}

	.checkout__facebook, .checkout__google {
		width: 270px;
	}

	.checkout__google {
		margin-left: 30px;
	}

	.checkout__another-way {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		position: relative;
		height: 40px;
		margin-bottom: 20px;
	}

	.checkout__another-way::before {
		content: '';
		position: absolute;
		width: 100%;
		height: 1px;
		left: 0px;
		background: #121726;
		margin-top: 3px;
	}

	.checkout__another-way span {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 64px;
		height: 100%;
		background-color: #f9f9f9;
		z-index: 1;
	}

	.checkout__field-form {
		position: relative;
		margin-bottom: 26px;
	}

	.checkout__field-form span {
		display: none;
	}

	.checkout__field-form .eye-pass {
		display: block;
		position: absolute;
		content: "";
		top: 50%;
		right: 16px;
		margin-top: -8px;
		background-position: 13.973799126637555% 100%;
		width: 16px;
		height: 16px;
	}

	.checkout__field-form .eye-pass:hover {
		background-position: 6.986899563318778% 100%;
		width: 16px;
		height: 16px;
	}

	.checkout__field-form.phone {
		margin-bottom: 0;
	}

	.checkout__input {
		font-size: 16px;
		border: 0.5px solid #C8CBD1;
		padding: 10px 40px 10px 15px;
		display: block;
		width: 100%;
		height: 44px;
		background-color: #f9f9f9;
	}

	.checkout__input:focus {
		outline: none;
		border-bottom: 2px solid #121726;
	}
	/*
	.checkout__input.form-input:focus ~ .form-label,
	.checkout__input.form-input:not(:focus):valid ~ .form-label {
		font-family: "GothamProRegular", sans-serif;
		font-size: 11px;
		top: -11px;
	}
*/
	.checkout__input.first {
		border-top: 1px;
	}

	.checkout__label {
		color: #808994;
		font-size: 13px;
		line-height: 18px;
		position: absolute;
		pointer-events: none;
		left: 15px;
		top: 13px;
		-webkit-transition: 0.3s ease all;
		-o-transition: 0.3s ease all;
		transition: 0.3s ease all;
		background: #f9f9f9;
		padding: 0 4px 0 4px;
		font-family: "GothamProRegular", sans-serif;

		font-family: "GothamProRegular", sans-serif;
		font-size: 11px;
		top: -11px;
	}

	.checkout__sign-in-btn {
		width: 100%;
		border: 0.5px solid #C8CBD1;
		background-color: #121726;
		color: #fff;
		margin-top: 26px;
		text-transform: uppercase;
		letter-spacing: 1px;
		font-family: "GothamProMedium", sans-serif;
		font-size: 11px;
		height: 55px;
		outline: none;
		padding-left: 15px;
	}

	.checkout__remember {
		padding-top: 26px;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
	}

	.checkout__form-check {
		font-size: 13px;
		line-height: 18px;
		color: #535F6E;
	}

	.checkout__forgot-password-link {
		font-size: 12px;
		line-height: 16px;
		color: #808994;
		text-decoration: underline;
	}

	.checkout__forgot-password-link:hover {
		color: #808994;
	}

	.checkout__data-heading {
		font-size: 14px;
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
		text-transform: uppercase;
		padding: 26px 0 42px 0;
	}

	.checkout__customer-data {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-ms-flex-direction: column;
		flex-direction: column;
	}

	.checkout__customer-name, .checkout__customer-email {
		font-size: 13px;
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
		padding: 2px 0 2px 0;
	}

	.checkout__change-data-customer {
		-webkit-text-decoration-line: underline;
		text-decoration-line: underline;
		color: #535F6E;
		font-size: 14px;
		font-family: "GothamProMedium", sans-serif;
		padding: 23px 0 29px 0;
	}

	@media screen and (max-width: 991px) {
		.checkout__way-registration {
			width: 48%;
		}
	}

	@media screen and (max-width: 340px) {
		.checkout__registration {
			-webkit-flex-wrap: wrap;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
		}
		.checkout__registered {
			margin-top: 15px;
		}
	}

	@media screen and (max-width: 575px) {
		.checkout__content {
			-webkit-flex-wrap: wrap;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
		}
		.checkout__way-registration {
			width: 100%;
		}
	}

	.checkout-guest {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.checkout-guest .checkout__email-form.guest-email {
		padding-top: 29px;
	}

	.checkout-guest .delivery__counters-payments {
		color: #121726;
		font-size: 14px;
		font-family: "GothamProMedium", sans-serif;
		padding-right: 12px;
		line-height: 45px;
	}

	.checkout-guest .delivery__counters-text {
		color: #121726;
		font-size: 14px;
		padding-right: 14px;
		line-height: 45px;
	}

	.checkout-guest .delivery__installment-info-dropdown {
		padding-bottom: 32px;
		padding-top: 23px;
	}

	.checkout-guest .delivery__dropdown-list {
		border: 0.5px solid #C8CBD1;
		width: 82px;
		height: 44px;
	}

	.checkout-guest .delivery__dropdown-container {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.checkout-guest .delivery__colortext {
		padding: 0 7px 0 7px;
	}

	.checkout-guest .delivery__dropdown-select {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		width: 82px;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-ms-flex-direction: column;
		flex-direction: column;
	}

	.checkout-guest .delivery__options-container {
		color: #535F6E;
		max-height: 0;
		width: 100%;
		opacity: 0;
		-webkit-transition: all 0.4s;
		-o-transition: all 0.4s;
		transition: all 0.4s;
		overflow: hidden;
		-webkit-box-ordinal-group: 2;
		-webkit-order: 1;
		-ms-flex-order: 1;
		order: 1;
	}

	.checkout-guest .delivery__selected {
		border: 0.5px solid #C8CBD1;
		margin-bottom: 19px;
		color: black;
		position: relative;
		-webkit-box-ordinal-group: 1;
		-webkit-order: 0;
		-ms-flex-order: 0;
		order: 0;
	}

	.checkout-guest .delivery__selected:after {
		content: "";
		background: url("/img/svg/vector.svg");
		-webkit-background-size: contain;
		background-size: contain;
		background-repeat: no-repeat;
		position: absolute;
		height: 100%;
		width: 12px;
		right: 7px;
		top: 19px;
	}

	.checkout-guest .delivery__options-container.active {
		max-height: 440px;
		opacity: 1;
		overflow-y: scroll;
	}

	.checkout-guest .delivery__options-container.active + .delivery__selected::after {
		-webkit-transform: rotateX(180deg);
		transform: rotateX(180deg);
		top: -17px;
	}

	.checkout-guest .delivery__options-container::-webkit-scrollbar {
		width: 4px;
		background: #fff;
	}

	.checkout-guest .delivery__options-container::-webkit-scrollbar-thumb {
		background: #121726;
	}

	.checkout-guest .delivery__option-item, .checkout-guest .delivery__selected {
		padding: 12px 30px;
		cursor: pointer;
	}

	.checkout-guest .delivery__option-item:hover {
		background: rgba(200, 203, 209, 0.25);
		color: #121726;
	}

	.checkout-guest .delivery__dropdown-select label {
		cursor: pointer;
	}

	.checkout-guest .delivery__radio-item {
		display: none;
	}

	.checkout-guest .order__name.not-registered:before {
		content: '!';
		color: red;
		padding-right: 3px;
	}

	.checkout-guest .order__title:before {
		content: '!';
		color: red;
		padding-right: 3px;
	}

	.checkout-guest .order__prepayment {
		padding-top: 105px;
	}

	.checkout-guest .order__prepayment-title {
		font-size: 13px;
		color: #535F6E;
		font-family: "GothamProMedium", sans-serif;
		padding: 0;
	}

	.checkout-guest .order__prepayment-title:before {
		content: '!';
		color: red;
		padding-right: 3px;
	}

	.checkout-guest .order__prepayment-title__prepayment-list {
		color: #808994;
		font-size: 13px;
	}

	.checkout-guest .order__prepayment-title__prepayment-percentage {
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
	}

	.checkout-guest .order__prepayment-title__info-contact.guest-contact {
		padding-top: 50px;
	}

	input[type=radio] + .details {
		display: none;
	}

	input[type=radio]:checked + .fordebit {
		display: block;
		-webkit-transform: scale(1);
		-ms-transform: scale(1);
		transform: scale(1);
	}

	input[type=radio]:checked + .forcredit {
		display: block;
	}

	input[type=radio] {
		float: left;
	}

	.checkout-guest-drop {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.checkout-guest-drop .checkout__email-form.guest-email {
		padding-top: 29px;
	}

	.checkout-guest-drop .delivery__container.flex {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.checkout-guest-drop .delivery__counters-payments {
		color: #121726;
		font-size: 14px;
		font-family: "GothamProMedium", sans-serif;
		padding-right: 12px;
		line-height: 45px;
	}

	.checkout-guest-drop .delivery__counters-text {
		color: #121726;
		font-size: 14px;
		padding-right: 14px;
		line-height: 45px;
	}

	.checkout-guest-drop .delivery__installment-info-dropdown {
		padding-bottom: 32px;
		padding-top: 23px;
	}

	.checkout-guest-drop .delivery__dropdown-list {
		border: 0.5px solid #C8CBD1;
		width: 82px;
		height: 44px;
	}

	.checkout-guest-drop .delivery__dropdown-container {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.checkout-guest-drop .delivery__colortext {
		padding: 0 7px 0 7px;
	}

	.checkout-guest-drop .delivery__dropdown-select {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		width: 270px;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-ms-flex-direction: column;
		flex-direction: column;
	}

	.checkout-guest-drop .delivery__options-container {
		color: #535F6E;
		max-height: 0;
		width: 100%;
		opacity: 0;
		-webkit-transition: all 0.4s;
		-o-transition: all 0.4s;
		transition: all 0.4s;
		overflow: hidden;
		-webkit-box-ordinal-group: 2;
		-webkit-order: 1;
		-ms-flex-order: 1;
		order: 1;
	}

	.checkout-guest-drop .delivery__selected {
		border: 0.5px solid #C8CBD1;
		margin-bottom: 19px;
		color: #535F6E;
		position: relative;
		-webkit-box-ordinal-group: 1;
		-webkit-order: 0;
		-ms-flex-order: 0;
		order: 0;
	}

	.checkout-guest-drop .delivery__selected:after {
		content: "";
		background: url("/img/svg/vector.svg");
		-webkit-background-size: contain;
		background-size: contain;
		background-repeat: no-repeat;
		position: absolute;
		height: 100%;
		width: 12px;
		right: 7px;
		top: 19px;
	}

	.checkout-guest-drop .delivery__options-container.active {
		max-height: 440px;
		opacity: 1;
		overflow-y: scroll;
	}

	.checkout-guest-drop .delivery__options-container.active + .delivery__selected::after {
		-webkit-transform: rotateX(180deg);
		transform: rotateX(180deg);
		top: -17px;
	}

	.checkout-guest-drop .delivery__options-container::-webkit-scrollbar {
		width: 4px;
		background: #fff;
	}

	.checkout-guest-drop .delivery__options-container::-webkit-scrollbar-thumb {
		background: #121726;
	}

	.checkout-guest-drop .delivery__option-item, .checkout-guest-drop .delivery__selected {
		padding: 12px 30px;
		cursor: pointer;
	}

	.checkout-guest-drop .delivery__option-item:hover {
		background: rgba(200, 203, 209, 0.25);
		color: #121726;
	}

	.checkout-guest-drop .delivery__dropdown-select label {
		cursor: pointer;
	}

	.checkout-guest-drop .delivery__radio-item {
		display: none;
	}

	.checkout-guest-drop .order__confirn-order.confirm-da {
		height: 60px;
		background-color: #43BE58;
	}

	.checkout-guest-drop .order__name.not-registered:before {
		content: '!';
		color: red;
		padding-right: 3px;
	}

	.checkout-guest-drop .order__title:before {
		content: '!';
		color: red;
		padding-right: 3px;
	}

	.checkout-guest-drop .order__prepayment {
		padding-top: 105px;
	}

	.checkout-guest-drop .order__prepayment-title {
		font-size: 13px;
		color: #535F6E;
		font-family: "GothamProMedium", sans-serif;
		padding: 0;
	}

	.checkout-guest-drop .order__prepayment-title:before {
		content: '!';
		color: red;
		padding-right: 3px;
	}

	.checkout-guest-drop .order__prepayment-title li {
		color: #808994;
		font-size: 13px;
		margin-left: 12px;
	}

	.checkout-guest-drop .order__prepayment-title li span {
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
	}

	.checkout-guest-drop .order__info-contact.guest-contact {
		padding-top: 50px;
	}

	.checkout-guest-drop .order__info-contact.guest-contact.after-prepay {
		margin-top: 0;
	}

	.delivery {
		padding: 80px 0;
		margin-left: -15px;
		/* Style the indicator (dot/circle) */
	}

	.delivery__head {
		font-size: 14px;
		letter-spacing: 0.5px;
		text-transform: uppercase;
		font-family: "GothamProMedium", sans-serif;
		color: #121726;
		padding-bottom: 36px;
		margin-top: 70px;
	}

	.delivery__head.pay {
		margin-top: 70px;
	}

	.delivery__head:first-of-type {
		margin-top: 0;
	}

	.delivery__address-info-dropdown {
		position: relative;
		left: 0;
		margin: 15px 0 0;
		padding: 5px;
		list-style: none;
	}

	.delivery__address-info-dropdown span {
		display: block;
	}

	.delivery__address-info-dropdown span:first-of-type {
		color: #121726;
		font-size: 13px;
		font-family: "GothamProMedium", sans-serif;
	}

	.delivery__change-adress {
		margin: 23px 0 0;
		-webkit-text-decoration-line: underline;
		text-decoration-line: underline;
		color: #535F6E;
		font-family: "GothamProMedium", sans-serif;
		cursor: pointer;
	}

	.delivery__comment {
		-webkit-text-decoration-line: underline;
		text-decoration-line: underline;
		font-size: 14px;
		font-family: "GothamProMedium", sans-serif;
		color: #535F6E;
		padding-top: 39px;
		cursor: pointer;
		margin-bottom: 26px;
	}

	.delivery__text-field {
		max-width: 570px;
		width: 100%;
		height: 100px;
		max-height: 200px;
		border: 1px solid #C8CBD1;
		background-color: #f9f9f9;
		padding: 15px;
	}

	.delivery__text-field::-webkit-input-placeholder {
		font-size: 13px;
		line-height: 18px;
		color: #808994;
	}

	.delivery__text-field::-moz-placeholder {
		font-size: 13px;
		line-height: 18px;
		color: #808994;
	}

	.delivery__text-field:-moz-placeholder {
		font-size: 13px;
		line-height: 18px;
		color: #808994;
	}

	.delivery__text-field:-ms-input-placeholder {
		font-size: 13px;
		line-height: 18px;
		color: #808994;
	}

	.delivery__text-field:focus {
		border-color: #121726;
	}

	.delivery__text-field:focus::-webkit-input-placeholder {
		color: transparent;
	}

	.delivery__text-field:focus::-moz-placeholder {
		color: transparent;
	}

	.delivery__text-field:focus:-moz-placeholder {
		color: transparent;
	}

	.delivery__text-field:focus:-ms-input-placeholder {
		color: transparent;
	}

	.delivery__choice-item {
		margin-bottom: 30px;
	}

	.delivery__choice-item label {
		font-size: 14px;
		color: #535F6E;
	}

	.delivery__choice-item label::before {
		top: 2px !important;
	}

	.delivery__choice-item label::after {
		top: 5px !important;
	}

	.delivery__choice-item .checkout__field-form {
		margin-top: 20px;
	}

	.delivery__choice-item .checkout__field-form .form-label {
		color: #808994;
	}

	.delivery__choice-item p {
		font-size: 11px;
		color: #808994;
		padding-left: 30px;
	}

	.delivery__choice-item input:checked ~ .installments {
		display: block;
	}

	.delivery .pay-method .delivery__choice-item {
		margin-bottom: 20px;
	}

	.delivery .installments {
		margin-top: 20px;
		display: none;
	}

	.delivery .installments label {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		font-family: "GothamProMedium", sans-serif;
		line-height: 24px;
		color: #121726;
	}

	.delivery .installments label::before {
		top: 3px !important;
	}

	.delivery .installments label::after {
		top: 6px !important;
	}

	.delivery .installments .checkout__field-form {
		margin: 0;
	}

	.delivery .installments .form-label {
		/*
		font-family: "GothamProRegular", sans-serif;
		top: 10px;*/
	}

	.delivery .installments__count {
		display: inline-block;
		font-family: "GothamProMedium", sans-serif;
	}

	.delivery .installments__count:first-of-type {
		margin-right: 15px;
	}

	.delivery .installments__count-months {
		position: relative;
		display: inline-block;
		margin: 0 10px;
	}

	.delivery .installments__month {
		position: absolute;
		width: -webkit-max-content;
		width: -moz-max-content;
		width: max-content;
		top: -8px;
		right: 0;
		left: 0;
		margin: 0 auto;
		font-size: 11px;
		color: #808994;
		background-color: #f9f9f9;
		z-index: 99;
		padding: 0 10px;
	}

	.delivery .installments .locality {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		-webkit-flex-wrap: wrap;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
	}

	.delivery .installments .locality__select {
		width: 48%;
	}

	.delivery .installments .locality__select li {
		color: #535F6E;
	}

	.delivery .installments .locality__select li:first-of-type {
		color: #808994;
	}

	.delivery .installments .locality__select .jq-selectbox__select-text {
		font-size: 13px;
		line-height: 18px;
		color: #808994;
	}

	.delivery .installments .locality .checkout__field-form {
		width: 48%;
	}

	.delivery .installments .locality__address {
		width: 100%;
		margin-top: 26px;
	}

	.delivery .installments .locality__address-item {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		margin-bottom: 26px;
	}

	.delivery .installments .locality__address-item:last-of-type {
		margin-bottom: 0;
	}

	.delivery .installments .locality__address .locality__label {
		margin-top: 10px;
		margin-bottom: auto;
	}

	.delivery .installments .locality__address .locality__label::before, .delivery .installments .locality__address .locality__label::after {
		content: "";
		width: 10px;
		height: 10px;
	}

	.delivery .installments .locality__address .locality__label::before {
		top: 6px !important;
		left: 3px;
	}

	.delivery .installments .locality__address .select {
		width: 100%;
	}

	.delivery .installments .locality__address .checkout__field-form {
		width: 100%;
	}

	@media screen and (max-width: 1199px) {
		.delivery .installments .locality__select {
			width: 100%;
			margin-bottom: 26px;
		}
		.delivery .installments .locality .checkout__field-form {
			width: 100%;
		}
		.delivery .installments .locality__address .locality__select {
			margin-bottom: 0;
		}
	}

	.order {
		width: 48%;
		margin-top: -29px;
		margin-left: 29px;
		margin-bottom: 150px;
	}

	.order__wrapper {
		padding: 29px 39px 44px;
		position: relative;
		background-color: #ededef;
	}

	.order__collapse-order::after {
		content: '';
		position: absolute;
		top: 38%;
		right: -16px;
		width: 40px;
		height: 40px;
		background-color: #f9f9f9;
		background-image: url("/img/svg/arrow.svg");
		background-position: center;
		background-repeat: no-repeat;
		-webkit-box-shadow: 10px 10px 22px rgba(83, 95, 110, 0.32);
		box-shadow: 10px 10px 22px rgba(83, 95, 110, 0.32);
	}

	.order__head {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		border-bottom: 1px solid #808994;
		padding-bottom: 24px;
	}

	.order__number-of-orders, .order__title {
		font-size: 14px;
		text-transform: uppercase;
		font-family: "GothamProMedium", sans-serif;
		position: relative;
	}

	.order__card {
		padding-top: 33px;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-flex-wrap: wrap;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		border-bottom: 1px solid #DBDEE2;
		padding-bottom: 28px;
	}

	.order__name {
		font-size: 13px;
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
		position: relative;
	}

	.order__color {
		color: #808994;
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
	}

	.order__value {
		font-size: 13px;
		font-family: "GothamProRegular", sans-serif;
		color: #121726;
	}

	.order__remove {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		padding-top: 16px;
	}

	.order__remove::before {
		display: inline-block;
		content: "";
		background-position: 66.09442060085837% 98.28326180257511%;
		width: 12px;
		height: 12px;
	}

	.order__delete-products {
		padding-left: 15px;
		margin-top: 3px;
		color: #808994;
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
		cursor: pointer;
	}

	.order__about-product {
		padding-left: 20px;
		width: -webkit-calc(100% - 90px);
		width: calc(100% - 100px);
	}

	.order__calc-input {
		width: 24px;
		text-align: center;
	}

	.order__quantity-price {
		margin-left: auto;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		-ms-flex-direction: column;
		flex-direction: column;
		-webkit-box-align: end;
		-webkit-align-items: flex-end;
		-ms-flex-align: end;
		align-items: flex-end;
	}

	.order__calc {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
	}

	.order__calc-button-minus {
		position: relative;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 24px;
		height: 24px;
		border: 1px solid #C8CBD1;
		cursor: pointer;
	}

	.order__calc-button-minus::before {
		position: absolute;
		content: "";
		width: 10px;
		height: 1px;
		background-color: #C8CBD1;
	}

	.order__calc-button-minus.disabled {
		border-color: #DBDEE2;
	}

	.order__calc-button-minus.disabled::before, .order__calc-button-minus.disabled::after {
		background-color: #DBDEE2;
	}

	.order__calc-button-plus {
		position: relative;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 24px;
		height: 24px;
		border: 1px solid #C8CBD1;
		cursor: pointer;
		margin-left: 6px;
	}

	.order__calc-button-plus::before {
		position: absolute;
		content: "";
		width: 10px;
		height: 1px;
		background-color: #C8CBD1;
	}

	.order__calc-button-plus.disabled {
		border-color: #DBDEE2;
	}

	.order__calc-button-plus.disabled::before, .order__calc-button-plus.disabled::after {
		background-color: #DBDEE2;
	}

	.order__calc-button-plus::after {
		position: absolute;
		content: "";
		width: 1px;
		height: 10px;
		background-color: #C8CBD1;
	}

	.order__calc-input {
		font-family: "GothamProMedium", sans-serif;
		font-size: 13px;
		line-height: 20px;
		text-align: center;
		width: 24px;
		height: 24px;
		border: 1px solid #C8CBD1;
		color: #121726;
		margin-left: 6px;
	}

	.order__cost-digits {
		padding-bottom: 15px;
	}

	.order__price-for-one {
		color: #808994;
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
	}

	.order__price-for-value {
		font-size: 13px;
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
	}

	.order__sum-label {
		font-size: 13px;
		color: #808994;
		font-family: "GothamProMedium", sans-serif;
	}

	.order__cost {
		font-size: 13px;
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
	}

	.order__sum {
		padding-top: 15px;
	}

	.order__promo {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		padding-top: 32px;
	}

	.order__promo .checkout__label {
		background: #EDEDEF;
	}

	.order__coupon-code {
		outline: 0;
		border: 0.5px solid #C8CBD1;
		width: 310px;
		height: 44px;
		padding-left: 15px;
		background: transparent;
	}

	.order__coupon-code::-webkit-input-placeholder {
		color: #808994;
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
	}

	.order__coupon-code::-moz-placeholder {
		color: #808994;
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
	}

	.order__coupon-code:-moz-placeholder {
		color: #808994;
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
	}

	.order__coupon-code:-ms-input-placeholder {
		color: #808994;
		font-family: "GothamProRegular", sans-serif;
		font-size: 13px;
	}

	.order__promo-code-submit {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		border: 1px solid #121726;
		-webkit-flex-basis: 164px;
		-ms-flex-preferred-size: 164px;
		flex-basis: 164px;
		text-align: center;
		height: 44px;
		cursor: pointer;
	}

	.order__promo-code-btn {
		border: 0;
		text-transform: uppercase;
		letter-spacing: 1px;
		font-size: 11px;
		font-family: "GothamProMedium", sans-serif;
		color: #121726;
		background-color: transparent;
	}

	.order__sub-total-items {
		width: 100%;
	}

	.order__sub-total-item {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		margin-bottom: 16px;
	}

	.order__sub-total-item:last-of-type {
		margin-bottom: 0;
	}

	.order__sub-total, .order__grand-total {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
	}

	.order__sub-total {
		padding-top: 28px;
		padding-bottom: 32px;
		border-bottom: 1px solid #121726;
	}

	.order__subtotal-text {
		font-size: 14px;
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
	}

	.order__subtotal-value {
		color: #121726;
		font-size: 17px;
		font-family: "GothamProMedium", sans-serif;
		text-align: right;
	}

	.order__subtotal-value.sale-sum {
		color: #FF3B30;
	}

	.order__grand-total {
		padding-top: 43px;
		padding-bottom: 44px;
	}

	.order__total-text {
		color: #121726;
		font-size: 22px;
		font-family: "GothamProBold", sans-serif;
	}

	.order__total {
		color: #121726;
		font-size: 17px;
		font-family: "GothamProMedium", sans-serif;
	}

	.order__confirm-order {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		text-align: center;
		position: relative;
	}

	.order__confirm-btn {
		width: 100%;
		height: 60px;
		border: 0;
		text-transform: uppercase;
		letter-spacing: 1px;
		font-size: 11px;
		font-family: "GothamProMedium", sans-serif;
		color: #fff;
		background-color: #121726;
		display: block;

	}
	a.order__confirm-btn {
		text-align: center;
		line-height: 60px;

		cursor: pointer;
	}
	a.order__confirm-btn:hover
	{
		color: #fff;
	}
	.order__confirm-btn.green-btn {
		background-color: #43BE58;
	}

	.order__agreement {
		position: relative;
		width: 100%;
		font-size: 11px;
		text-align: center;
		color: #808994;
		margin-top: 20px;
	}

	.order__agreement a {
		text-decoration: none;
	}

	.order__agreement-link {
		color: #535F6E;
		background-color: transparent;
		border-bottom: 0.5px solid #535F6E;
	}

	.order__info-contact {
		margin-top: 70px;
	}

	.order__info-email, .order__phone {
		font-size: 14px;
		color: #121726;
		font-family: "GothamProMedium", sans-serif;
	}

	.order__help {
		color: #535F6E;
	}

	.order__phone {
		padding: 3px 0 3px 0;
	}

	.order__phone span {
		color: #535F6E;
	}

	@media screen and (max-width: 991px) {
		.order {
			width: 48%;
		}
		.order__wrapper {
			padding: 20px 20px 40px;
		}
		.order-promo-form {
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-pack: justify;
			-webkit-justify-content: space-between;
			-ms-flex-pack: justify;
			justify-content: space-between;
		}
		.order-promo-form .checkout__field-form {
			margin-bottom: 0;
		}
		.order__coupon-code, .order__promo-code-submit {
			width: 82%;
		}
	}

	@media screen and (max-width: 767px) {
		.order__wrapper {
			padding: 20px 9px 40px;
		}
		.order__coupon-code {
			width: 94%;
		}
	}

	@media screen and (max-width: 575px) {
		.order {
			width: 100%;
			max-height: 1010px;
			margin-left: 0;
			margin-bottom: 260px;
		}
	}

	.checkout-success {
		padding-bottom: 80px;
	}

	.checkout-success__block {
		max-width: 677px;
	}

	.checkout-success .h1 {
		margin: 25px 0 35px;
	}

	.checkout-success .h3 {
		padding-bottom: 30px;
		border-bottom: 1px solid #C8CBD1;
		margin-bottom: 30px;
	}

	.checkout-success p, .checkout-success__description {
		max-width: 545px;
		color: #535F6E;
		line-height: 24px;
		margin-bottom: 25px;
	}

	.checkout-success p a, .checkout-success__description a {
		font-family: "GothamProMedium", sans-serif;
		color: #535F6E;
		text-decoration: underline;
	}

	.checkout-success .order__info-contact {
		margin-top: 5px;
	}

	.checkout-success__btn {
		margin-top: 54px;
	}

	.checkout-success__btn a {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		max-width: 570px;
		width: 100%;
		height: 44px;
		background: #f9f9f9;
		border: 1px solid #121726;
		font-family: "GothamProMedium", sans-serif;
		font-size: 11px;
		color: #121726;
		text-transform: uppercase;
	}

</style>
<!-- checkout -->

<section class="checkout checkout_form">
	<div class="wrapper">
		<h1 class="checkout__head"><?=$text_title_order;?></h1>

		<?php if ($attention) { ?>
		<div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>

		<div class="checkout__content">
			<div class="checkout__way-registration">
				<?php if (!isset($customer_id)) { ?>
				<div class="checkout__registration">

					<div class="checkout__guest select-item"><?=$text_guest;?></div>
					<div class="checkout__registered"><?=$text_registered;?></div>
				</div>
				<div class="table_registered"  style="display: none;">
					<div class="checkout__registration-info"><?=$text_login;?></div>
					<div class="checkout__sign-in-social">

						<?php echo $content_top; ?>
					</div>
					<div class="checkout__another-way"><!-- span>или</span --></div>
					<form class="login-form" >
						<div class="error"></div>
						<fieldset class="checkout__field-form">
							<input class="checkout__input form-input" type="text" name="email" value=""  id="input-email" />
							<label class="checkout__label form-label" for="input-email"><?php echo $entry_email; ?></label>
						</fieldset>
						<fieldset class="checkout__field-form">
							<input class="checkout__input form-input" type="password" name="password" value="" id="input-password"  />
							<label class="checkout__label form-label" for="input-password"><?php echo $entry_password; ?></label>
							<span class="eye-pass"></span>
						</fieldset>
						<input class="checkout__sign-in-btn"  type="button" value="<?php echo $button_login; ?>" id="button-login" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>"  />

						<div class="checkout__remember">
							<div class="checkout__forgotten-password">
								<a href="<?php echo $forgotten; ?>" class="checkout__forgot-password-link"><?php echo $text_forgotten; ?></a>
							</div>
						</div>
					</form>
				</div>

				<?php } ?>
				<div class="table_guest">
					<fieldset class="checkout__field-form">
						<input type="text" name="firstname" value="<?php if (isset($address['firstname'])) echo $address['firstname']; elseif (isset($firstname)) echo $firstname; ?>"  id="input-payment-firstname" class="checkout__input form-input" />
						<label class="checkout__label form-label" for="input-payment-firstname"><?php echo $entry_firstname; ?></label>

					</fieldset>

					<fieldset class="checkout__field-form">
						<input type="text" name="telephone" value="<?php if (isset($telephone)) echo $telephone;?>" id="input-payment-telephone" class="checkout__input form-input" />
						<label class="checkout__label form-label" for="input-payment-telephone"><?php echo $entry_telephone; ?></label>
					</fieldset>

					<fieldset class="checkout__field-form">
						<input type="text" name="email" value="<?php if (isset($email)) echo $email;?>" id="input-payment-email" class="checkout__input form-input" />
						<label class="checkout__label form-label" for="input-payment-email"><?php echo $entry_email; ?></label>
					</fieldset>



				</div>




				<section class="delivery">
					<div class="wrapper">
						<form class="delivery__form">
							<div class="delivery__head"><?php echo $text_shipping_method; ?></div>
							<div class="delivery__choice-block">
								<div class="delivery__choice-items">
									<?php foreach ($shipping_methods as $shipping_method) { ?>
										<?php if (!$shipping_method['error']) { ?>
												<?php foreach ($shipping_method['quote'] as $quote) { ?>
													<div class="delivery__choice-item">
														<?php if ($quote['code'] == $code || !$code) { ?>
															<?php $code = $quote['code']; ?>
															<input id="<?php echo $quote['code']; ?>" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" checked="checked" />
														<?php } else { ?>
															<input id="<?php echo $quote['code']; ?>" type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" />
														<?php } ?>
														<label for="<?php echo $quote['code']; ?>"><?php echo $shipping_method['title']; ?></label>
														<p class="delivery__description"><?php echo $quote['sub_title']; ?></p>

														<?php if ($quote['code'] == 'flat.flat') { ?>
															<div class="installments">
																<fieldset class="checkout__field-form">
																	<input type="text" name="address_1" value="<?php if (isset($address_1)) echo $address_1;?>" id="input-payment-address-1" class="checkout__input form-input" />
																	<label class="checkout__label form-label" for="input-payment-address-1"><?php echo $text_address2; ?></label>
																</fieldset>
															</div>

														<?php } ?>
														<?php if ($quote['code'] == 'free.free') { ?>
															<div class="installments">
																<fieldset class="checkout__field-form">
																	<input class="checkout__input form-input" type="text" required id="text_address3" >
																	<label class="checkout__label form-label"><?php echo $text_address3; ?>*</label>
																</fieldset>
															</div>
															<div class="installments">

																<fieldset class="checkout__field-form">
																	<input type="text" name="km" value="<?php if (isset($km)) echo $km;?>" id="input-km" class="checkout__input form-input" />
																	<label class="checkout__label form-label" for="input-km"><?php echo $text_km; ?></label>
																</fieldset>
															</div>

														<?php } ?>
														<?php if ($quote['code'] == 'novaposhta.warehouse') { ?>
														<div class="installments">
															<div class="locality">


																<select id="region" name="zone_id" class="locality__select select" placeholder="Регион / область*" >
																	<? foreach ($zones as $zone) {
                														if(!$zone['status']) continue; ?>
																			<option <? echo ($zone['zone_id'] == $zone_id) ? 'selected' : ''; ?>
																			value="<? echo $zone['zone_id']; ?>"><? echo $zone['name']; ?></option>
																	<? } ?>
																</select>

																<select id="city" name="city" class="locality__select select" placeholder="Город / населенный пункт*" >

																</select>

																<div class="locality__address">
																	<div class="locality__address-item locality__detachment">
																		<input type="radio" name="detachment" id="detachment">
																		<label for="detachment" class="locality__label"></label>

																		<select id="address_2" name="address_2" class="locality__select select" placeholder="Город / населенный пункт*" >

																		</select>
																	</div>
																	<div class="locality__address-item locality__street">
																		<input type="radio" name="detachment" id="street">
																		<label for="street" class="locality__label"></label>
																		<fieldset class="checkout__field-form">
																			<input class="checkout__input form-input" type="text" required>
																			<label class="checkout__label form-label"><?php echo $text_address2; ?>*</label>
																		</fieldset>
																	</div>
																</div>
															</div>
														</div>
														<?php } ?>
													</div>
												<?php } ?>
											<?php } else { ?>
												<div class="alert alert-danger"><?php echo $shipping_method['error']; ?></div>
											<?php } ?>
										<?php } ?>




								</div>
							</div>

							<div class="delivery__head"><?php echo $text_payment_method; ?></div>
							<div class="delivery__choice-block pay-method payment-method">
								<div class="delivery__choice-items">
									<?php foreach ($payment_methods as $payment_method) { ?>
										<div class="delivery__choice-item radio <?php echo $payment_method['code']; ?>">
											<?php if ($payment_method['code'] == $code || !$code) { ?>
											<?php $code = $payment_method['code']; ?>
											<input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" title="" checked="checked" />
											<?php } else { ?>
											<input id="<?php echo $payment_method['code']; ?>" type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" title="" />
											<?php } ?>

											<label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?>
												<?php if ($payment_method['code'] != 'monobank' ) { ?>
													<div class=" panel-default" style="display: none;">

													</div>
												<?php } ?>
											</label>


										</div>

									<?php } ?>
								</div>
							</div>
						</form>

						<div class="delivery__comment-block">
							<div class="delivery__comment"><?php echo $text_comments; ?></div>
							<textarea name="comment" rows="3" class="delivery__text-field" placeholder="<?php echo $text_comments2; ?>:"><?php echo $comment; ?></textarea>
						</div>
					</div>
				</section>
			</div>

			<div class="order">
				<div class="order__wrapper">
					<!-- <div class="order__collapse-order"></div> -->
					<div class="order__head">
						<div class="order__title"><?=$text_title2;?></div>
						<div class="order__number-of-orders"><?=$text_products;?> (<?=count($products);?>)</div>
					</div>
					<?php foreach ($products as $product) { ?>
						<div class="order__card">
							<div class="order_product-image">
								<?php if ($product['thumb']) { ?>
									<a href="<?php echo $product['href']; ?>">
										<img src="<?=($product['option_img'])?$product['option_img']:$product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"  />
									</a>
								<?php } ?>
								<div class="order__remove" onclick="cart.remove('<?php echo $product['cart_id']; ?>');">
									<span class="order__delete-products"><?php echo $text_remove2; ?><span>
								</div>
							</div>
							<div class="order__about-product">
								<a class="order__name" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?>
									<?php if (!$product['stock']) { ?>
									<span class="text-danger">***</span>
									<?php } ?>
								</a>
								<?php foreach ($product['option'] as $option) { ?>
									<div class="order__attributes">
										<span class="order__color"><?php echo $option['name']; ?>: </span>
										<span class="order__value"><?php if (isset($option['option_value']) && !empty($option['option_value'])) echo $option['option_value'];else if (isset($option['value'])) echo $option['value']; ?></span>
									</div>
								<?php } ?>
							</div>
							<div class="order__quantity-price">
								<div class="order__cost-digits">
									<span class="order__price-for-one"><?php echo $text_price; ?></span>
									<span class="order__price-for-value">: <?php echo $product['price']; ?></span>
								</div>
								<div class="order__calc">
									<div class="order__calc-button-minus"></div>
									<input class="order__calc-input"  value="<?php echo $product['quantity']; ?>" disabled >
									<input type="hidden"  class="order__calc-cart_id"  value="<?php echo $product['cart_id']; ?>"  />
									<div class="order__calc-button-plus"></div>
								</div>
								<div class="order__sum">
									<span class="order__sum-label"><?php echo $text_sum; ?>:</span>
									<span class="order__cost"><?php echo $product['total']; ?></span>
								</div>
							</div>
						</div>

					<?php } ?>


					<?php echo $coupon; ?>




					<div class="order__sub-total">
						<div class="order__sub-total-items">


							<?php foreach ($totals as $k => $total) { ?>
								<?php if($k+1 == count($totals)) break;  ?>
								<div class="order__sub-total-item">
									<span class="order__subtotal-text"><?php echo $total['title']; ?></span>
									<span class="order__subtotal-value"><?php echo isset($total['flat_text'])? $total['flat_text'] : $total['text']; ?></span>
								</div>

							<?php } ?>
						</div>
					</div>

					<div class="order__grand-total">
						<span class="order__total-text"><?php echo $total['title']; ?></span>
						<span class="order__total"><?php echo $total['text']; ?></span>
					</div>



					<div>

							<div class="  clearfix">
								<div class="payment-register">
									<?php if ($payment) echo $payment; else { ?>
									<input type="button" class="order__confirm-btn" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" id="button-register" value="<?php echo $send_order;?>">
									<?php }?>
								</div>
								<div class="payment-box">

								</div>
							</div>

							<?php if ($text_agree) { ?>

							<?php } else { ?>
								<div class="buttons">
									<div class="back-btn">
										<a href="<?=($url_refferer)?$url_refferer:$_SERVER['HTTP_REFERER'] ?>" id="button-payment-method" data-loading-text="<?php if (isset($text_loading)) echo $text_loading;else echo 'loading ...' ?>" class="order__confirm-btn" ><?php echo $button_back; ?></a>
									</div>
								</div>
							<?php } ?>

					</div>
				</div>
				<?php echo $text_info2; ?>

				<?php echo $text_info; ?>
			</div>


		</div>
</section>
<!-- checkout-guest -->

<!-- end breadcrumbs -->




<script type="text/javascript"><!--
var error = true;

if( $('html').attr('lang') == 'uk' )
{
uri = '/ua/index.php?route=';
}else{
uri = '/index.php?route=';
}

// Login
$(document).delegate('.order__calc-button-plus', 'click', function() {
	div = $(this).closest('.order__calc');
	input = $(div).find('.order__calc-input');
	cart_id = $(div).find('.order__calc-cart_id').val();
	val_input = $(input).val()/1+1;
	$(input).val(val_input);
	edit_cart(cart_id,val_input);

})
$(document).delegate('.order__calc-button-minus', 'click', function() {
	div = $(this).closest('.order__calc');
	input = $(div).find('.order__calc-input');
	cart_id = $(div).find('.order__calc-cart_id').val();
	val_input = $(input).val()/1-1;
	if(val_input<1)
		val_input = 1;

	$(input).val(val_input);
	edit_cart(cart_id,val_input);
})



$(document).delegate('#button-login', 'click', function() {
    $.ajax({
        url: uri+'checkout/checkout/login_validate',
        type: 'post',
        data: $('.login-form :input'),
        dataType: 'json',
        beforeSend: function() {
        	$('#button-login').button('loading');
		},  
        complete: function() {
            $('#button-login').button('reset');
        },              
        success: function(json) {
            $('.alert, .text-danger').remove();
            
            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
				//error = true;
				if (json['error']['warning']) {
					$('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}

            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    }); 
});

var confirm_btn = 1;
// Register
$(document).delegate('#button-register', 'click', function() 
{
	var curr_paymethod = $('.payment-method input[name=payment_method]:checked').val(); 
	var liqpay_regl = $('#instock-check').prop('checked');

	/* if(curr_paymethod == 'liqpay' && liqpay_regl === false){
		$('.pay_online_error').css('display', 'block');
		return false;
	} else { */
	
		var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
		data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');
		
		$.ajax({
			url: uri+'checkout/checkout/validate',
			type: 'post',
			data: data,
			dataType: 'json',
			beforeSend: function() {
				$('#button-register').button('loading');
			},  
			complete: function() {
				$('#button-register').button('reset');
			},          
			success: function(json) {
				$('.alert, .text-danger').remove();
			//	console.log(json);

				if (json['redirect'] && typeof json['error'] == "undefined") {
					location = json['redirect'];
				} else if (json['error']) {
					error = true;
					if (json['error']['warning']) {
						$('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
					
					for (i in json['error']) {
								$('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
					}
				} else 
				{
					error = false;
					//jQuery('[name=\'payment_method\']:checked').click();
					var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
					data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');

					console.log(data);

					$.ajax({
						url: uri+'checkout/checkout/confirm',
						type: 'post',
						data: data,
						success: function(html)
						{
							jQuery(".payment-register").hide();
							jQuery(".payment-box").html(html);

							var curr_paymethod = $('.payment-method input[name=payment_method]:checked').val();
							if(curr_paymethod == 'liqpay') {
								jQuery(".lqpayform-sbth").click();
							}else {
								jQuery("#button-confirm").click();

								if(curr_paymethod == 'privatbank_paymentparts_ii' || curr_paymethod == 'privatbank_paymentparts_pp'){

								}else{


								}
							}


						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});

				}    
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		}); 
	/* } */
});

$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
        url: uri+'checkout/checkout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
        },
        complete: function() {
			$('.fa-spinner').remove();
        },          
        success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}
			            
            html = '<option value=""><?php echo $text_select; ?></option>';
            
            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';
                    
                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }
    
                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }
            
            $('select[name=\'zone_id\']').html(html).val("");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});


$('select[name=\'shipping_country_id\']').on('change', function() {
	$.ajax({
        url: uri+'checkout/checkout/country&country_id=' + this.value,
        dataType: 'json',
        beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-spinner fa-spin"></i>');
        },
        complete: function() {
			$('.fa-spinner').remove();
        },          
        success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}
			            
            html = '<option value=""><?php echo $text_select; ?></option>';
            
            if (json['zone'] && json['zone'] != '') {
                for (i = 0; i < json['zone'].length; i++) {
                    html += '<option value="' + json['zone'][i]['zone_id'] + '"';
                    
                    if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
                        html += ' selected="selected"';
                    }
    
                    html += '>' + json['zone'][i]['name'] + '</option>';
                }
            } else {
                html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
            }
            
            $('select[name=\'shipping_zone_id\']').html(html).val("");
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});


$('select[name=\'zone_id\']').on('change', function()
{
	var shipping_method = $('.checkout_form input[name="shipping_method"]:checked').val();
	var zone_id = $('.checkout_form select[name="zone_id"] option:selected').val();
	if (typeof shipping_method !== 'undefined' && zone_id > 0 && (shipping_method == 'novaposhta.warehouse' || shipping_method == 'novaposhta.doors' )) {
		var opt = '';

		$.ajax({
			url: 'index.php?route=module/shippingdata/getShippingData', type: 'post', data: {'shipping': shipping_method, 'action': 'getCities', 'filter': zone_id, 'search': ''}, success: function (data) {

				opt += '<option value="" selected disabled><?=$text_select_city;?></option>';
				$.each(data, function (index, obj) {
					opt += '<option value="' + obj.Description + '">' + obj.Description + '</option>';
				});
				$('.checkout_form select[name="city"]').html(opt);
				$('.checkout_form select[name="address_2"]').html('');
			}
		});
	} else {

	}
});

$('.locality__street .form-input').on('focus', function() {
	$('.checkout_form select[name="address_2"]').html('');
	$('#street').click();
	$('.locality__street .form-input').attr('name','address_2');
})
$('#street').on('click', function() {
	$('.checkout_form select[name="address_2"]').html('');
	$('.locality__street .form-input').attr('name','address_2');
})
$('#detachment').on('click', function() {
	$('.locality__street .form-input').attr('name','');
	if($('.checkout_form select[name="address_2"]').html()=='')
		set_address_2();
})
function set_address_2()
{
	var shipping_method = $('.checkout_form input[name="shipping_method"]:checked').val();
	var city = $('.checkout_form select[name="city"] option:selected').val();
	if (typeof shipping_method !== 'undefined' && (shipping_method == 'novaposhta.warehouse' || shipping_method == 'ukrposhta.warehouse')) {

		var opt = '';
		$.ajax({
			url: 'index.php?route=module/shippingdata/getShippingData', type: 'post', data: {'shipping': shipping_method, 'action': 'getWarehouses', 'filter': city, 'search': ''}, success: function (data) {

				opt += '<option value="" selected disabled><?=$text_select_warehouses;?></option>';
				$.each(data, function (index, obj) {
					opt += '<option value="' + obj.Description + '">' + obj.Description + '</option>';
				});
				$('.checkout_form select[name="address_2"]').html(opt);
				$('#detachment').click();
			}
		});
	} else {

	}
}
$('select[name=\'city\']').on('change', function()
{
	set_address_2();
});
$('.checkout_form select[name="address_2"]').on('focus', function()
{
	if($('.checkout_form select[name="address_2"]').html()=='')
		set_address_2();
});

/*
$('select[name=\'country_id\'], select[name=\'zone_id\'], select[name=\'shipping_country_id\'], select[name=\'shipping_zone_id\'], input[type=\'radio\'][name=\'payment_address\'], input[type=\'radio\'][name=\'shipping_address\']').on('change', function() 
{
	if (this.name == 'contry_id') jQuery("select[name=\'zone_id\']").val("");
	if (this.name == 'shipping_country_id') jQuery("select[name=\'shipping_zone_id\']").val("");
	
    jQuery(".shipping-method").load('index.php?route=checkout/checkout/shipping_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function() 
    {
		if (jQuery("input[name=\'shipping_method\']:first").length) 
		{
			jQuery("input[name=\'shipping_method\']:first").attr('checked', 'checked').prop('checked', true).click();
		} else
		{
			jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));	
		}
    });

	jQuery(".payment-method").load('index.php?route=checkout/checkout/payment_method', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked,input[name=\'shipping_method\']:first, .checkout_form textarea, .checkout_form select'), function() 
	{
		jQuery("[name=\'payment_method\']").removeAttr("checked").prop('checked', false);
	});
});
*/

$(document).delegate('input[name=\'shipping_method\']', 'click', function()
{
    jQuery("#cart_table").load('index.php?route=checkout/checkout/cart', $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select'));
});


$( document).on('change',"input[name='payment_method']",function(){
	var payment_method = $( this ).val();
	$(".privatbank_paymentparts_pp .panel-default").html('');
	$(".privatbank_paymentparts_ii .panel-default").html('');

	if(payment_method == 'privatbank_paymentparts_pp')
		$(".privatbank_paymentparts_pp .panel-default").load('index.php?route=payment/privatbank_paymentparts_pp/show');

	if(payment_method == 'privatbank_paymentparts_ii')
		$(".privatbank_paymentparts_ii .panel-default").load('index.php?route=payment/privatbank_paymentparts_ii/show');



})

//.checkout__guest, .checkout__registered

$(document).delegate('input[name=\'shipping_method\']', 'change', function() {
	var shipping_method = $( this ).val();

	if( shipping_method == 'flat.flat') {
		$('#text_address3').attr('name','');
		$('#input-payment-address-1').attr('name', 'address_1');
	}

	if(shipping_method == 'free.free') {
		$('#input-payment-address-1').attr('name', '');
		$('#text_address3').attr('name', 'address_1');
	}
	update_checkout();
});
$("#input-km").bind("change paste keyup", function() {
	update_checkout();
});
	update_checkout();

$('body').delegate('[name=\'payment_method\']','click', function() 
{


	jQuery(".payment-register").show();
	jQuery(".payment-box").html('');
	/*
	var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
	data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');

	console.log(data);

	if (!error)
    $.ajax({
        url: uri+'checkout/checkout/confirm',
        type: 'post',
        data: data,
        success: function(html) 
        {
			jQuery(".payment-register").hide();
			jQuery(".payment-box").html(html);

			var curr_paymethod = $('.payment-method input[name=payment_method]:checked').val();
			if(curr_paymethod == 'liqpay') {
				jQuery(".lqpayform-sbth").click();
			}else {
				if(curr_paymethod == 'privatbank_paymentparts_ii' || curr_paymethod == 'privatbank_paymentparts_pp'){

				}else{
					jQuery("#button-confirm").click();
				}
			}


			
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

	 */
});

$('select[name=\'country_id\']').trigger('change');
jQuery(document).ready(function()
{
	jQuery('input:radio[name=\'payment_method\']:first').attr('checked', true).prop('checked', true);
	<?php /*if ($opencart_version < 2000) {?>
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
	<?php }*/?>	
	
});
//--></script>

<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery(".payment-method input[type=radio]").change(function () {
			var payment_method = $('.payment-method input[type=radio]:checked').val();

			console.log(payment_method);


			/*
			$.ajax({
				url: 'index.php?route=checkout/checkout/validate',
				data: 'payment=' + payment_method,
				dataType: 'json',
				type: 'post',
				success: function (json) {

					$('#cart_table').load('index.php?route=checkout/checkout/cart');
				}
			});

			var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
			data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');

			$.ajax({
				url: uri+'checkout/checkout/validate',
				type: 'post',
				data: data,
				dataType: 'json',
				beforeSend: function() {
					$('#button-register').button('loading');
				},
				complete: function() {
					$('#button-register').button('reset');
				},
				success: function(json) {
					$('.alert, .text-danger').remove();

					if (json['redirect']) {
						location = json['redirect'];
					} else if (json['error']) {
						error = true;
						if (json['error']['warning']) {
							$('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
						}

						for (i in json['error']) {
							$('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
						}
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
			*/
		});
	});
</script>
<script>
	$(document).delegate('.checkout__guest', 'click', function()
	{
		$('.checkout__registered').removeClass('select-item');
		$('.table_registered').hide();
		$('.checkout__guest').addClass('select-item');
		$('.table_guest').show();
	});
	$(document).delegate('#simplemodal-container button', 'click', function() {
		$('#simplemodal-container').hide();
		$('#simplemodal-overlay').hide();
		$('.payment-method input[name=payment_method]:checked').click();
	});
	$(document).delegate('.checkout__registered', 'click', function()
	{
		$('.checkout__guest').removeClass('select-item');
		$('.table_guest').hide();
		$('.checkout__registered').addClass('select-item');
		$('.table_registered').show();
	});

</script>

<?php echo $footer;?>
