<!-- div id="search" class="search-box">
	  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>..." class="search-input">
		<button type="button" class="seach-button">
			<i class="fa fa-search" aria-hidden="true"></i>
		</button>
</div -->
<div class="header__search header-search">
	<div class="header-search">
		<div id="search"  style="position: relative;">
			<input name="search" type="text" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>"
				   class="header-search__input">
			<div class="header-search__search-btn"></div>
			<div class="header-search__close-btn"></div>
			<div style="display: none;" class="search-result-ajax" id="search-result-ajax"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		jQuery('#special-OnOff').on('click', function(){
			var checked = jQuery(this).attr('checked');
			if (typeof checked !== "undefined") checked = 0; else checked = 1;

			var data = {
				action: 'specialOnOff',
				val:	checked,
			};
			var baseurl = 'http://'+window.location.hostname+'/';
			var ajaxurl = baseurl + 'tools/ajax.php';

			jQuery.post(ajaxurl, data, function(response) {
				console.log(response);
			});
			if (checked){
				jQuery(this).attr('checked','checked');
			} else {
				jQuery(this).removeAttr('checked');
			};
		});
	})
</script>
