<!DOCTYPE html>
<!-- test -->

<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-TV5TH6K');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - AdWords: 971613570 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-971613570"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-971613570');
</script>



  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>

  <?php if(isset($_REQUEST['sort']) || isset($_REQUEST['mfp'])){ ?>
	<meta name="robots" content="noindex, nofollow" />
  <?php } ?>
    <link href="catalog/view/javascript/jquery/ui/jquery-ui.min.css?v=1.0" rel="stylesheet" />
  <link href="catalog/view/theme/fm/stylesheet/jquery.bxslider.css?v=1.0" rel="stylesheet" />
  <script  src="catalog/view/javascript/jquery/jquery-2.1.1.min.js?v=1.0" type="text/javascript"></script>

  <script src="catalog/view/javascript/jquery.bxslider.min.js?v=1.0"></script>

  <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css?v=1.0" rel="stylesheet" media="screen" />
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js?v=1.0" type="text/javascript"></script>
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css?v=1.0" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="catalog/view/theme/fm/stylesheet/fonts.css?v=1.0">
    <link rel="stylesheet" href="catalog/view/javascript/flipclock/flipclock.css?v=1.0">

<?php echo $alternate; ?>

  <script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js?v=1.0"></script>
  <script src="catalog/view/javascript/jquery/jquery.inputmask.bundle.min.js?v=1.0"></script>
    <script src="catalog/view/javascript/jquery/ui/jquery-ui.min.js?v=1.0"></script>
  <script src="catalog/view/javascript/flipclock/flipclock.js?v=1.0"></script>
  <link href="catalog/view/theme/fm/stylesheet/stylesheet.css?v=8.0" rel="stylesheet">
  <link href="catalog/view/theme/fm/stylesheet/unicorns.css?v=2.0" rel="stylesheet">
  <link href="catalog/view/theme/fm/stylesheet/media.css?v=8.0" rel="stylesheet">
  <?php foreach ($styles as $style) { ?>
  <link href="<?php echo $style['href']; ?>?v=1.0" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>
  <script src="catalog/view/javascript/common.js?v=1.0" type="text/javascript"></script>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo 'https:'.$link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>?v=1.0" type="text/javascript"></script>
  <?php } ?>
  <?php foreach ($analytics as $analytic) { ?>
  <?php echo $analytic; ?>
  <?php } ?>

  <script type="text/javascript" src="catalog/view/javascript/callback.js?v=1.0"></script>
  <script type="text/javascript" src="catalog/view/javascript/jquery.simplemodal.js?v=1.0"></script>
  <link href="catalog/view/theme/fm/stylesheet/callback.css?v=1.0" rel="stylesheet" type="text/css" />

<?php  ?>


 <meta property="og:type" content="website" />
              <meta property="og:title" content="<?=$heading_title?>" />
              <meta property="og:url" content="<?='https://feshmebel.com.ua'.$_SERVER['REQUEST_URI']?>" />
              <meta property="og:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
              <meta property="article:author" content="https://www.facebook.com/feshemebel/" />
              <meta property="twitter:card" content="summary" />
              <meta property="twitter:title" content="<?=$heading_title?>" />
              <meta property="twitter:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
              <meta property="og:image" content="<?=$image['popup']?>" />
              <meta property="twitter:image" content="<?=$image['popup']?>" />
              <meta property="og:publisher" content="https://www.facebook.com/feshemebel/" />
              <meta property="og:site_name" content="Интернет магазин мебели Фешемебельный" />



  <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png?v=1.0">
  <link rel="shortcut icon" type="image/png" href="/favicon/favicon-32x32.png?v=1.0">
  <link rel="manifest" href="/favicon/manifest.json?v=1.0">
  <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg?v=1.0" color="#2d4059">
  <meta name="msapplication-config" content="/favicon/browserconfig.xml?v=1.0">
  <meta name="theme-color" content="#ffd460">


</head>
<?php 
	require_once('./tools/Mobile_Detect.php'); 
	$detect = new Mobile_Detect;
?>
<body class="<?php echo $class; if($detect->isTablet()){echo ' tabletVersion';} ?>">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TV5TH6K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div class="wrapper-main" style="position: relative; z-index: 10000; display: none;">
        <header class="header ">
            <div class="header__top header-top">
                <div class="wrapper">
                    <div class="row">
                        <div class="header-top__left">
                            <div class="header-top__left-block">
                                <div class="header-top__logo logo">
                                    <div class="logo__img"><a href=""><img src="img/svg/logo-main_ru.svg" alt="" onmouseout="this.src='img/svg/logo-main_ru.svg'" onmouseover="this.src='img/svg/fm_logo-main_ru_hover.svg'"></a></div>
                                </div>
                            </div>
                            <div class="header-top__right-block">
                                <div class="header-top__phone phone">
                                    <div class="phone__visible">
                                        <span>Бесплатно</span>
                                        <a href="tel:0800330190">0 800 330 190</a>
                                        <div class="phone__block">
                                            <div class="phone__dropdown">
                                                <div class="phone__kind">
                                                    <span>Тел.</span>
                                                    <a href="tel:">+380 44 38 38 38 5</a>
                                                </div>
                                                <div class="phone__kind">
                                                    <span>Viber</span>
                                                    <a href="tel:">+380 68 904 65 45</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="header-top__schedule">
                                    <div>
                                        <span>Пн-Пт</span>
                                        <p>9:00 - 20:00</p>
                                    </div>
                                    <div>
                                        <span>Сб-Вс</span>
                                        <p>10:00 - 20:00</p>
                                    </div>
                                </div>
                                <div class="header-top__languages">
                                    <a href="#" class="active">ua</a>
                                    <a href="#">ru</a>
                                </div>
                            </div>
                        </div>
                        <div class="header-top__right">
                            <div class="header-top__user">
                                <div class="header-top__user-img"></div>
                                <div class="header-top__user-block">
                                    <div class="header-top__user-bg"></div>
                                    <div class="header-top__user-dropdown user-dropdown">
                                        <div class="user-dropdown__sign-up">
                                            <div class="user-dropdown__close"></div>
                                            <h4 class="user-dropdown__head .h4">личный кабинет</h4>
                                            <p class="user-dropdown__description">Зарегистрируйтесь с помощью вашего аккаунта:</p>
                                            <div class="user-dropdown__socials">
                                                <a href="" class="user-dropdown__social-link facebook-btn">Facebook</a>
                                                <a href="" class="user-dropdown__social-link google-btn">Google</a>
                                            </div>
                                            <div class="user-dropdown__either"><span>или</span></div>
                                            <form action="" class="user-dropdown__form">
                                                <div class="form__input">
                                                    <input type="text" id="email" class="login-field" placeholder="Ваш Email*"><label for="email">Ваш Email*</label>
                                                    <span class="form__required" data-title="Обязательное окно">Ваш Email*</span>
                                                    <span class="form__please">Пожалуйста, введите ваш Email</span>
                                                    <span class="form__error">Неверный адрес электронной почты</span>
                                                </div>
                                                <div class="form__input">
                                                    <input type="password" id="pass" class="login-field" placeholder="Пароль*"><label for="pass">Пароль*</label>
                                                    <span class="form__required" data-title="Обязательное окно">Пароль*</span>
                                                    <span class="form__please">Пожалуйста, введите ваш пароль</span>
                                                    <span class="form__error">Неверный пароль</span>
                                                </div>
                                                <div class="form__input">
                                                    <input type="password" id="pass-again" class="login-field" placeholder="Повторите пароль*"><label for="pass-again">Повторите пароль*</label>
                                                    <span class="form__required" data-title="Обязательное окно">Пароль*</span>
                                                    <span class="form__please">Пожалуйста, введите ваш пароль</span>
                                                    <span class="form__error">Неверный пароль</span>
                                                </div>
                                                <div>
                                                    <input type="checkbox" class="checkbox" id="notifications" name="" value=""><label for="notifications" class="user-dropdown__checkbox-label">Получать уведомления о новинках, акциях, скидках</label>
                                                </div>
                                                <div>
                                                    <input type="submit" class="user-dropdown__button sign-up-btn" value="зарегистрироваться"></div>
                                            </form>
                                            <div class="user-dropdown__agreement">
                                                <p>Регистрируясь, я принимаю условия <a href="">пользовательского соглашения</a></p>
                                            </div>
                                            <div class="user-dropdown__come-in">
                                                <p class="user-dropdown__question">Уже зарегистрирован?</p>
                                                <span class="user-dropdown__come-in-link">Войти</span>
                                            </div>
                                        </div>
                                        <div class="user-dropdown__sign-in">
                                            <div class="user-dropdown__close"></div>
                                            <h4 class="user-dropdown__head .h4">личный кабинет</h4>
                                            <p class="user-dropdown__description">Войдите с помощью вашего аккаунта:</p>
                                            <div class="user-dropdown__socials">
                                                <a href="" class="user-dropdown__social-link facebook-btn">Facebook</a>
                                                <a href="" class="user-dropdown__social-link google-btn">Google</a>
                                            </div>
                                            <div class="user-dropdown__either"><span>или</span></div>
                                            <form action="" class="user-dropdown__form">
                                                <div class="form__input">
                                                    <input type="text" id="email" class="login-field" placeholder="Ваш Email*"><label for="email">Ваш Email*</label>
                                                    <span class="form__required" data-title="Обязательное окно">Ваш Email*</span>
                                                    <span class="form__please">Пожалуйста, введите ваш Email</span>
                                                    <span class="form__error">Неверный адрес электронной почты</span>
                                                </div>
                                                <div class="form__input">
                                                    <input type="password" id="pass" class="login-field" placeholder="Пароль*"><label for="pass">Пароль*</label>
                                                    <span class="form__required" data-title="Обязательное окно">Пароль*</span>
                                                    <span class="form__please">Пожалуйста, введите ваш пароль</span>
                                                    <span class="form__error">Неверный пароль</span>
                                                </div>
                                                <div class="user-dropdown__remember-forgot remember-forgot">
                                                    <div class="remember-forgot__checkbox">
                                                        <input type="checkbox" class="checkbox" id="remember-me" name="" value="">
                                                        <label for="remember-me" class="user-dropdown__checkbox-label">Запомнить меня</label>
                                                    </div>
                                                    <a href="" class="remember-forgot__link">Забыли пароль?</a>
                                                </div>
                                                <div>
                                                    <input type="submit" class="user-dropdown__button sign-up-btn" value="Войти"></div>
                                            </form>
                                            <!-- <div class="user-dropdown__agreement">
                                                <p>Регистрируясь, я принимаю условия <a href="">пользовательского соглашения</a></p>
                                            </div> -->
                                            <div class="user-dropdown__come-in">
                                                <p class="user-dropdown__question">Еще нет аккаунта?</p>
                                                <span class="user-dropdown__come-in-link user-dropdown__reg-link">Зарегистрируйтесь</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="user-dropdown__menu-block">
                                    <div class="user-dropdown__menu">
                                        <div class="user-dropdown__menu-content">
                                            <div class="user-dropdown__close"></div>
                                            <h4 class="user-dropdown__menu-head .h4">lilya</h4>
                                            <div class="user-dropdown__menu-items">
                                                <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Личные данные</a></div>
                                                <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Адрес доставки</a></div>
                                                <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">История заказов</a></div>
                                                <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Обмен и возврат</a></div>
                                                <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Выход</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-top__basket basket">
                                <div class="basket__wish basket-wish basket__block">
                                    <div class="img"></div>
                                    <div class="basket-wish__block">
                                        <div class="basket-buy__bg"></div>



                                        <div class="basket-wish__product">
                                            <div class="user-dropdown__close basket-wish__close"></div>
                                            <div class="basket-wish__content">
                                                <h4 class="basket-wish__head h4">избранные товары</h4>
                                                <div class="basket-wish__product-block">
                                                    <div class="basket-wish__product-item">
                                                        <div class="basket-wish__product-img-del">
                                                            <div class="basket-wish__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-wish__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-wish__product-information">
                                                            <div class="basket-wish__product-head">Стул Navarra (Наварра) - 113226</div>
                                                            <ul class="basket-wish__product-inf">
                                                                <li>Цвет: <span>Белый</span></li>
                                                            </ul>
                                                            <div class="basket-wish__product-price">
                                                                <div class="basket-wish__product-discount">-632</div>
                                                                <div class="basket-wish__product-old">1932</div>
                                                                <div class="basket-wish__product-cost">1300 грн</div>
                                                            </div>
                                                            <div class="basket-wish__add-basket">в корзину</div>
                                                        </div>
                                                    </div>
                                                    <div class="basket-wish__product-item">
                                                        <div class="basket-wish__product-img-del">
                                                            <div class="basket-wish__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-wish__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-wish__product-information">
                                                            <div class="basket-wish__product-head">Стул Navarra (Наварра) - 113226</div>
                                                            <ul class="basket-wish__product-inf">
                                                                <li>Цвет: <span>Белый</span></li>
                                                            </ul>
                                                            <div class="basket-wish__product-price">
                                                                <div class="basket-wish__product-discount">-632</div>
                                                                <div class="basket-wish__product-old">1932</div>
                                                                <div class="basket-wish__product-cost">1300 грн</div>
                                                            </div>
                                                            <div class="basket-wish__add-basket">в корзину</div>
                                                        </div>
                                                    </div>
                                                    <div class="basket-wish__product-item">
                                                        <div class="basket-wish__product-img-del">
                                                            <div class="basket-wish__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-wish__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-wish__product-information">
                                                            <div class="basket-wish__product-head">Стул Navarra (Наварра) - 113226</div>
                                                            <ul class="basket-wish__product-inf">
                                                                <li>Цвет: <span>Белый</span></li>
                                                            </ul>
                                                            <div class="basket-wish__product-price">
                                                                <div class="basket-wish__product-discount">-632</div>
                                                                <div class="basket-wish__product-old">1932</div>
                                                                <div class="basket-wish__product-cost">1300 грн</div>
                                                            </div>
                                                            <div class="basket-wish__add-basket">в корзину</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="basket-wish__amount-block basket-amount">
                                                    <div class="basket-amount">
                                                        <div class="basket-amount__goods">
                                                            <div class="basket-amount__goods-name"><span>Товаров</span></div>
                                                            <div class="basket-amount__goods-count">1<span> шт</span></div>
                                                        </div>
                                                        <div class="basket-amount__price">
                                                            <div class="basket-amount__price-name"><span>Сумма</span></div>
                                                            <div class="basket-amount__price-value">14 999<span> грн</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="" class="basket-proceed-btn basket-wish-btn">добавить все в корзину</a>
                                            <a href="" class="basket-proceed-btn">продолжить покупки</a>
                                        </div>

                                    </div>

                                </div>
                                <div class="basket__compare basket-compare basket__block">
                                    <div class="basket-compare__count">3</div>
                                    <div class="img"></div>
                                    <div class="basket-compare__block">
                                        <div class="basket-buy__bg"></div>

                                        <!-- <div class="basket-compare__empty">
                                        <div class="user-dropdown__close basket-compare__close"></div>
                                        <div class="basket-compare__content">
                                            <h4 class="basket-compare__head h4">ваши сравнения</h4>
                                            <span class="basket-compare__inf">У вас пока нет товаров для сравнения.</span>
                                        </div>
                                        <a href="" class="basket-proceed-btn">продолжить покупки</a>
                                    </div> -->

                                        <div class="basket-compare__product">
                                            <div class="user-dropdown__close basket-compare__close"></div>
                                            <div class="basket-compare__content">
                                                <h4 class="basket-compare__head h4">ваши сравнения</h4>
                                                <div class="basket-compare__product-block">
                                                    <div class="basket-compare__product-item">
                                                        <div class="basket-compare__product-img-del">
                                                            <div class="basket-compare__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-compare__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-compare__product-information">
                                                            <div class="basket-compare__product-head">Стул Navarra (Наварра) - 113226</div>
                                                            <ul class="basket-compare__product-inf">
                                                                <li>Цвет: <span>Белый</span></li>
                                                            </ul>
                                                            <div class="basket-compare__product-price">
                                                                <div class="basket-compare__product-discount">-632</div>
                                                                <div class="basket-compare__product-old">1932</div>
                                                                <div class="basket-compare__product-cost">1300 грн</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basket-compare__product-item">
                                                        <div class="basket-compare__product-img-del">
                                                            <div class="basket-compare__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-compare__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-compare__product-information">
                                                            <div class="basket-compare__product-head">Стул Navarra (Наварра) - 113226</div>
                                                            <ul class="basket-compare__product-inf">
                                                                <li>Цвет: <span>Белый</span></li>
                                                            </ul>
                                                            <div class="basket-compare__product-price">
                                                                <div class="basket-compare__product-discount"></div>
                                                                <div class="basket-compare__product-old"></div>
                                                                <div class="basket-compare__product-cost">1300 грн</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basket-compare__product-item">
                                                        <div class="basket-compare__product-img-del">
                                                            <div class="basket-compare__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-compare__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-compare__product-information">
                                                            <div class="basket-compare__product-head">Стул Navarra (Наварра) - 113226</div>
                                                            <ul class="basket-compare__product-inf">
                                                                <li>Цвет: <span>Белый</span></li>
                                                            </ul>
                                                            <div class="basket-compare__product-price">
                                                                <div class="basket-compare__product-discount">-632</div>
                                                                <div class="basket-compare__product-old">1932</div>
                                                                <div class="basket-compare__product-cost">1300 грн</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="basket-compare__amount-block basket-amount">
                                                    <div class="basket-amount">
                                                        <div class="basket-amount__goods">
                                                            <div class="basket-amount__goods-name"><span>Товаров</span></div>
                                                            <div class="basket-amount__goods-count">1<span> шт</span></div>
                                                        </div>
                                                        <div class="basket-amount__price">
                                                            <div class="basket-amount__price-name"><span>Сумма</span></div>
                                                            <div class="basket-amount__price-value">14 999<span> грн</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="" class="basket-proceed-btn basket-compare-btn">сравнить&nbsp;<span class="basket-compare__product-count">4</span></a>
                                            <a href="" class="basket-proceed-btn">продолжить покупки</a>
                                        </div>

                                    </div>
                                </div>
                                <div class="basket__buy basket-buy basket__block">
                                    <div class="basket-buy__count">3</div>
                                    <div class="img"></div>
                                    <div class="basket-buy__block">

                                        <div class="basket-buy__bg"></div>

                                        <!-- <div class="basket-buy__empty">
                                            <div class="user-dropdown__close basket-buy__close"></div>
                                            <div class="basket-buy__content">
                                                <h4 class="basket-buy__head h4">ваша корзина</h4>
                                                <span class="basket-buy__inf">У вас пока нет товаров для покупки.</span>
                                                <div class="basket-buy__amount-block basket-amount">
                                                    <div class="basket-amount">
                                                        <div class="basket-amount__goods">
                                                            <div class="basket-amount__goods-name"><span>Товаров</span></div>
                                                            <div class="basket-amount__goods-count">0<span> шт</span></div>
                                                        </div>
                                                        <div class="basket-amount__price">
                                                            <div class="basket-amount__price-name"><span>Сумма</span></div>
                                                            <div class="basket-amount__price-value">0<span> грн</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="" class="basket-proceed-btn">продолжить покупки</a>
                                        </div> -->

                                        <div class="basket-buy__product">
                                            <div class="user-dropdown__close basket-buy__close"></div>
                                            <div class="basket-buy__content">
                                                <h4 class="basket-buy__head h4">ваша корзина</h4>
                                                <div class="basket-buy__product-block">

                                                    <div class="basket-buy__product-item">
                                                        <div class="basket-buy__product-img-del">
                                                            <div class="basket-buy__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-buy__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-buy__product-information">
                                                            <div class="basket-buy__product-head">Стол Edge Octopus (Эдж Октопус) дуб – 211544</div>
                                                            <ul class="basket-buy__product-inf">
                                                                <li>Тонировка: <span>Дуб 1</span></li>
                                                                <li>Цвет каркаса: <span>Прозрачный лак</span></li>
                                                                <li>Размер стола: <span>900 х 1800</span></li>
                                                            </ul>
                                                            <div class="basket-buy__product-price">
                                                                <div class="basket-buy__product-cost">Цена: <span>14 999 грн</span></div>
                                                                <div class="basket-buy__product-count">
                                                                    <div class="basket-buy__product-minus disabled"></div>
                                                                    <input type="text" class="basket-buy__product-number" value="1">
                                                                    <div class="basket-buy__product-plus"></div>
                                                                </div>
                                                                <div class="basket-buy__product-value">Сумма: <span>14 999 грн</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="basket-buy__product-item">
                                                        <div class="basket-buy__product-img-del">
                                                            <div class="basket-buy__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                            <div class="basket-buy__product-del">Удалить</div>
                                                        </div>
                                                        <div class="basket-buy__product-information">
                                                            <div class="basket-buy__product-head">Стол Edge Octopus (Эдж Октопус) дуб – 211544</div>
                                                            <ul class="basket-buy__product-inf">
                                                                <li>Тонировка: <span>Дуб 1</span></li>
                                                                <li>Цвет каркаса: <span>Прозрачный лак</span></li>
                                                                <li>Размер стола: <span>900 х 1800</span></li>
                                                            </ul>
                                                            <div class="basket-buy__product-price">
                                                                <div class="basket-buy__product-cost">Цена: <span>14 999 грн</span></div>
                                                                <div class="basket-buy__product-count">
                                                                    <div class="basket-buy__product-minus disabled"></div>
                                                                    <input type="text" class="basket-buy__product-number" value="1">
                                                                    <div class="basket-buy__product-plus"></div>
                                                                </div>
                                                                <div class="basket-buy__product-value">Сумма: <span>14 999 грн</span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="basket-buy__amount-block basket-amount">
                                                    <div class="basket-amount">
                                                        <div class="basket-amount__goods">
                                                            <div class="basket-amount__goods-name"><span>Товаров</span></div>
                                                            <div class="basket-amount__goods-count">1<span> шт</span></div>
                                                        </div>
                                                        <div class="basket-amount__price">
                                                            <div class="basket-amount__price-name"><span>Сумма</span></div>
                                                            <div class="basket-amount__price-value">14 999<span> грн</span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="" class="basket-proceed-btn">продолжить покупки</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header__bottom header-bottom">
                <div class="wrapper">
                    <div class="row">
                        <div class="header-bottom__left">
                            <div class="header-bottom__logo logo">
                                <div class="logo__img"><a href=""><img src="img/svg/fm_icon-logo.svg" alt="" onmouseout="this.src='img/svg/fm_icon-logo.svg'" onmouseover="this.src='img/svg/fm_icon-logo_hover.svg'"></a></div>
                            </div>
                            <div class="header__menu header-menu">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                                <ul class="header-menu__list">
                                    <?php foreach ($categories as $category) { ?>
                                    <li class="sub">
                                        <a href="<?=$category['href'];?>"><span><?=$category['name'];?></span></a>
                                        <div class="header-menu__dropdown-block">
                                            <div class="wrapper">
                                                <ul>
                                                    <?php if ($category['children']) { ?>
                                                    <?php foreach ($category['children'] as $child) { ?>
                                                    <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                                    <?php } ?>
                                                    <?php } ?>
                                                </ul>
                                            </div>

                                        </div>
                                    </li>
                                    <?php } ?>

                                    <!-- li><a href="#">Офис</a></li>
                                    <li><a href="#">Кафе</a></li>
                                    <li><a href="#" class="menu-red">Акции</a></li>
                                    <li><a href="#">Отп</a></li>
                                    <li class="sub"><span>Информация</span>
                                        <div class="header-menu__dropdown-block">
                                            <ul class="header-menu__dropdown">
                                                <div class="wrapper">
                                                    <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                        <li class="header-menu__cat-name">
                                                            <ul class="header-menu__submenu active sameheight-menu">
                                                                <div class="header-menu__feature">
                                                                    <span>Сервис</span>
                                                                    <ul>
                                                                        <li><a href="">Оплата и доставка</a></li>
                                                                        <li><a href="">Гарантия и возврат</a></li>
                                                                        <li><a href="">Рассрочка </a></li>
                                                                        <li><a href="">Частые вопросы</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="header-menu__feature">
                                                                    <span>Клиентам</span>
                                                                    <ul>
                                                                        <li><a href="">Оптовым клиентам</a></li>
                                                                        <li><a href="">Система скидок</a></li>
                                                                        <li><a href="">Услуга «Примерка»</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="header-menu__feature">
                                                                    <span>Компаниям</span>
                                                                    <ul>
                                                                        <li><a href="">О ФешеМебельном</a></li>
                                                                        <li><a href="">Блог</a></li>
                                                                        <li><a href="">Контакты</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="header-menu__feature all we-work">
                                                                    <div class="timetable">
                                                                        <div class="timetable__left-block">
                                                                            <span><a href="">Мы работаем</a></span>
                                                                            <ul>
                                                                                <li class="timetable__work-days">
                                                                                    <span>Пн-Пт</span>9:00 - 18:00
                                                                                    <span>Сб</span>10:00 - 20:00
                                                                                </li>
                                                                                <li class="timetable__weekend">
                                                                                    Выходные:
                                                                                    <div><span>Воскресенье</span></div>
                                                                                    <div><span>Гос. праздники</span></div>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                        <div class="timetable__right-block">
                                                                            <ul>
                                                                                <li><a href=""><img src="img/we-work.jpg" alt=""></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </ul>
                                                        </li>
                                                    </div>
                                                </div>
                                            </ul>
                                        </div>
                                    </li -->
                                </ul>
                            </div>
                        </div>
                        <div class="header-bottom__right">
                            <div class="header__search header-search">
                                <div class="header-search">
                                    <form action="">
                                        <input type="text" placeholder="Поиск по сайту" class="header-search__input">
                                        <div class="header-search__search-btn"></div>
                                        <div class="header-search__close-btn"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
<?php if($banner_home['status']){ ?>
<div class="header-banner">

<div class="banner-img container w320">
	<img src="/image/<?=$banner_home['image1']?>" width="" height="auto" />
</div>
<div class="banner-img container w768">
	<img src="/image/<?=$banner_home['image2']?>" width="" height="auto" />
</div>
<div class="banner-img container w1024">
	<img src="/image/<?=$banner_home['image3']?>" width="" height="auto" />
</div>

</div>
<?php } ?>
<nav class="top-nav">
  <div class="container">
    <div class="row">
      <div class="top-nav__left col-xs-2 col-sm-8 col-md-9">
        <button type="button" class="top-nav__menu-btn btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".top-nav__list">
          <i class="fa fa-bars"></i>
        </button>
        <ul class="collapse navbar-collapse top-nav__list">
          <li><a href="<?php echo $url_about; ?>"><?php echo $top_menu_about; ?></a></li>
          <li><a href="<?php echo $url_delivery; ?>"><?php echo $top_menu_delivery; ?></a></li>
          <li><a href="<?php echo $url_guaranty; ?>"><?php echo $top_menu_guaranty; ?></a></li>
          <li><a href="<?php echo $url_exchange; ?>"><?php echo $top_menu_exchange; ?></a></li>
          <li><a href="<?php echo $url_discount; ?>"><?php echo $top_menu_discount; ?></a></li>
            <li><a href="<?php echo $url_try_on; ?>"><?php echo $top_menu_try_on; ?></a></li>
        </ul>
      </div>
      <div class="top-nav__right col-xs-10 col-sm-4 col-md-3">
		<p class="mb-work-tel work-tel">Тел: <a href="tel:+380685108199">+38-068-510-81-99</a></p>
        <div class="top-nav__profile dropdown">
          <a href="/login" title="<?php echo $text_account; ?>" rel="nofollow" class="top-nav__profile-link dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <span class="top-nav__profile-link-text"><?php echo $text_account; ?></span>
            <!-- <span class="caret"></span> -->
          </a>
          <ul class="top-nav__dropdown-menu dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <!--li><a href="<!?php echo $download; ?>"><!?php echo $text_download; ?></a></li-->
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>" rel="nofollow" ><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>" rel="nofollow"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php echo $language; ?>
      </div>
    </div>
  </div>
</nav>
<header class="header <?php if ($detect->isMobile() && !$detect->isTablet()) { ?> mb-header<? } ?>">
  <div class="header__container container <?php if ($detect->isMobile() && !$detect->isTablet()) { ?>mb-header__container<?}?>">
  
  <div class="category-menu mb-menu">
	<nav id="mb-menu" class="navbar">
      <div class="navbar-header category-button" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <i class="fa fa-bars" aria-hidden="true"></i>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <?php foreach ($categories as $category) { ?>
		  
		  <?php 
			if($category['id'] == 88){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні', 'href' => '/ua/stolovaja/stulja/#obedennye'),
						array('name' => 'Барні кухонні', 'href' => '/ua/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки і лави', 'href' => '/ua/stolovaja/taburety-i-skami/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные', 'href' => '/stolovaja/stulja/#obedennye'),
						array('name' => 'Барные кухонные', 'href' => '/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки и скамьи', 'href' => '/stolovaja/taburety-i-skami/')
					);
				}
			}
			if($category['id'] == 87){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні столи', 'href' => '/ua/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обідні столи-трансформери', 'href' => '/ua/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетний столик', 'href' => '/ua/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столи письмові', 'href' => '/ua/ofis/stoly-pismennye/'),
						array('name' => 'Каркаси і опори для столів', 'href' => '/ua/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекти столів і стільців', 'href' => '/ua/stolovaja/obedennye-komplekty/'),
						array('name' => 'Стільниці для столів', 'href' => '/ua/stoli/stoleshnitsy-dlja-stolov/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные столы', 'href' => '/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обеденные столы-трансформеры', 'href' => '/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетный столик', 'href' => '/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столы письменные', 'href' => '/ofis/stoly-pismennye/'),
						array('name' => 'Каркасы и опоры для столов', 'href' => '/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекты столов и стульев', 'href' => '/stolovaja/obedennye-komplekty/'),
						array('name' => 'Столешницы для столов', 'href' => '/stoli/stoleshnitsy-dlja-stolov/')
					);
				}
			}
		  ?>
		  
          <?php if ($category['children']) { ?>
          <li class="dropdown">
            <a href="<?php echo $category['href']; ?>" class="dropdown-toggle"><?php echo $category['name']; ?></a>
            <div class="dropdown-menu">
              <div class="dropdown-inner">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($children as $child) { ?>
                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </div>
            </div>
          </li>
          <?php } else { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </nav>
    </div>
	<?php if ($detect->isMobile() && !$detect->isTablet()) { ?>
	<div class="mb-header-block">
		<div class="mb-header__logo header__logo" itemscope itemtype="http://schema.org/Organization">
		  <a href="/" class="header__logo-link" itemprop="url">
			  <img itemprop="logo" class="logo__img" src="/image/mblogo.png?v=2.0" alt="">
		  </a>
		</div>
		
			<div class="mb-nav__search nav__search">
			  <?php echo $search; ?>
			  <div style="display: none;" class="search-result-ajax" id="search-result-ajax"></div>
			</div>
		
		<div class="mb-top-nav__profile top-nav__profile dropdown">
          <a href="/login" title="<?php echo $text_account; ?>" class="top-nav__profile-link dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
          </a>
          <ul class="top-nav__dropdown-menu dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <!--li><a href="<!?php echo $download; ?>"><!?php echo $text_download; ?></a></li-->
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>
		<div class="mb-header__cart header__cart">
			<?php echo $cart; ?>
			
		</div>
	</div>
	<?php } ?>
  
    <div class="header__logo" >
      <a href="/" class="header__logo-link" >
        <div class="logo">
          <i class="logo__icon logo__icon--1"></i>
          <i class="logo__icon logo__icon--2"></i>
          <i class="logo__icon logo__icon--3"></i>
          <i class="logo__icon logo__icon--4"></i>
          <i class="logo__icon logo__icon--5"></i>
          <img class="logo__img" src="/image/catalog/logo.svg?v=1.0" alt="">
        </div>
      </a>
	  <?php if ($detect->isMobile() && !$detect->isTablet()) { ?>
	  <div class="mb-header__schedule">
		  <p><?php echo $text_work; ?></p>
		  <p>Тел: <a href="tel:+380685108199 ">+38-068-510-81-99</a></p>
		</div>
	  <?php } ?>
    </div>

    <div class="header__schedule">
      <p><?php echo $text_work; ?></p>
      <p>Тел: <a href="tel:+380443838385">+38-044-38-38-38-5</a></p>
    </div>
	<?php if( !$detect->isMobile() || $detect->isTablet()){ ?>
    <div class="header__cart">
  		<?php echo $cart; ?>
      <div class="header__wish-compare">
        <a class="bookmark" href="/login/" id="wishlist-total" rel="nofollow" title="<?php echo $text_wishlist; ?>">
          <?php echo $text_wishlist; ?>
        </a>
        <a class="compare" href="/compare/" id="compare-total" rel="nofollow" title="<?php echo $text_compare; ?>" class="header-product-btn-i">
          <?php echo $text_compare; ?>
        </a>
      </div>
		</div>
	<?php } ?>
  </div>
</header>
<?php if( !$detect->isMobile() || $detect->isTablet()){ ?>
<?php if ($categories) { ?>
<div class="category-menu">
  <div class="container">
    <nav id="menu" class="navbar">
      <div class="navbar-header category-button" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <i class="fa fa-th" aria-hidden="true"></i>
        <span id="category"><?php echo $text_category; ?></span>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <?php foreach ($categories as $category) { ?>
		  
		  <?php 
			if($category['id'] == 88){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні', 'href' => '/ua/stolovaja/stulja/#obedennye'),
						array('name' => 'Барні кухонні', 'href' => '/ua/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки і лави', 'href' => '/ua/stolovaja/taburety-i-skami/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные', 'href' => '/stolovaja/stulja/#obedennye'),
						array('name' => 'Барные кухонные', 'href' => '/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки и скамьи', 'href' => '/stolovaja/taburety-i-skami/')
					);
				}
			}
			if($category['id'] == 87){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні столи', 'href' => '/ua/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обідні столи-трансформери', 'href' => '/ua/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетний столик', 'href' => '/ua/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столи письмові', 'href' => '/ua/ofis/stoly-pismennye/'),
						array('name' => 'Каркаси і опори для столів', 'href' => '/ua/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекти столів і стільців', 'href' => '/ua/stolovaja/obedennye-komplekty/'),
						array('name' => 'Стільниці для столів', 'href' => '/ua/stoli/stoleshnitsy-dlja-stolov/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные столы', 'href' => '/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обеденные столы-трансформеры', 'href' => '/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетный столик', 'href' => '/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столы письменные', 'href' => '/ofis/stoly-pismennye/'),
						array('name' => 'Каркасы и опоры для столов', 'href' => '/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекты столов и стульев', 'href' => '/stolovaja/obedennye-komplekty/'),
						array('name' => 'Столешницы для столов', 'href' => '/stoli/stoleshnitsy-dlja-stolov/')
					);
				}
			}
		  ?>
		  
          <?php if ($category['children']) { ?>
          <li class="dropdown">
            <a href="<?php echo $category['href']; ?>" class="dropdown-toggle"><?php echo $category['name']; ?></a>
            <div class="dropdown-menu">
              <div class="dropdown-inner">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($children as $child) { ?>
                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </div>
            </div>
          </li>
          <?php } else { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </nav>
	
		<div class="nav__search">
		  <?php echo $search; ?>
		  <div style="display: none;" class="search-result-ajax" id="search-result-ajax"></div>
		</div>
	
  </div>
</div>
<?php } ?>
<?php } ?>
