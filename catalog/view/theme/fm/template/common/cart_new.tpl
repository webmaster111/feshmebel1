<div class="basket__buy basket-buy basket__block">
    <a href="<?php echo $checkout; ?>">
        <div class="img">
            <div class="basket-buy__count"><?=$quantity;?></div>
        </div>
    </a>
    <?php if($quantity) { ?>
    <div class="basket-buy__block">
        <div class="basket-buy__bg"></div>
        <div class="basket-buy__product">
            <div class="user-dropdown__close basket-buy__close"></div>
            <div class="basket-buy__content">
                <h4 class="basket-buy__head h4"><?=$text_title;?></h4>
                <div class="basket-buy__product-block">
                    <?php foreach ($products as $product) { ?>
                        <div class="basket-buy__product-item">
                            <div class="basket-buy__product-img-del">
                                <a href="<?php echo $product['href']; ?>" class="basket-buy__product-img">
                                        <img src="<?=($product['option_img'])?$product['option_img']:$product['thumb']; ?>"
                                             alt="<?php echo $product['name']; ?>">
                                </a>
                                <div class="basket-buy__product-del" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" ><?=$text_remove;?></div>
                            </div>
                            <div class="basket-buy__product-information">
                                <a href="<?php echo $product['href']; ?>"
                                   class="basket-buy__product-head"><?php echo $product['name']; ?></a>
                                <?php if ($product['option']) { ?>
                                <ul class="basket-buy__product-inf">
                                    <?php foreach ($product['option'] as $option) { ?>
                                        <li><?php echo $option['name']; ?> : <span><?php echo $option['value']; ?></span></li>
                                    <?php } ?>
                                </ul>
                                <?php } ?>
                                <?php if ($product['recurring']) { ?>
                                <ul class="basket-buy__product-inf">
                                    <li><?php echo $text_recurring; ?> : <span><?php echo $product['recurring']; ?></span></li>
                                </ul>


                                <?php } ?>

                                <div class="basket-buy__product-price">
                                    <div class="basket-buy__product-cost"><?=$text_price;?>: <span><?php echo $product['price']; ?></span></div>
                                    <div class="basket-buy__product-count">
                                        <div class="basket-buy__product-minus"></div>
                                        <input type="text" class="basket-buy__product-number" value="<?php echo $product['quantity']; ?>" disabled >
                                        <input type="hidden"  class="order__calc-cart_id"  value="<?php echo $product['cart_id']; ?>"  />

                                        <div class="basket-buy__product-plus"></div>
                                    </div>
                                    <div class="basket-buy__product-value"><?=$text_total;?>: <span><?php echo $product['total']; ?></span></div>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                </div>
                <div class="basket-buy__amount-block basket-amount">
                    <div class="basket-amount">
                        <div class="basket-amount__goods">
                            <div class="basket-amount__goods-name"><span><?=$text_items_new;?></span></div>
                            <div class="basket-amount__goods-count"><?=$quantity;?><span> шт</span></div>
                        </div>
                        <div class="basket-amount__price">
                            <div class="basket-amount__price-name"><span><?=$text_total;?></span></div>
                            <div class="basket-amount__price-value"><?php echo $totals[count($totals)-1]['text']; ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="<?php echo $checkout; ?>" class="basket-proceed-btn"><?=$text_btn;?></a>
        </div>

    </div>
    <?php } ?>
</div>

