<br />
<!--footers_block-->
<style>

  .advantages {
    padding: 80px 0;
  }

  .advantages__items {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    border-bottom: 1px solid #DBDEE2;
    padding-bottom: 40px;
  }

  .advantages__item {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    width: 22%;
  }

  .advantages__img {
    margin-right: 24px;
  }

  .advantages__text {
    line-height: 24px;
    color: #535F6E;
  }

  @media screen and (max-width: 1199px) {
    .advantages__items {
      max-width: 912px;
      margin: 0 auto;
    }
    .advantages__item {
      width: 24%;
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -webkit-flex-direction: column;
      -ms-flex-direction: column;
      flex-direction: column;
    }
    .advantages__img {
      margin-right: 0;
      margin-bottom: 20px;
    }
    .advantages__text {
      font-size: 13px;
      line-height: 20px;
      text-align: center;
    }
  }

  @media screen and (max-width: 767px) {
    .advantages__items {
      border-bottom: none;
    }
  }

  @media screen and (max-width: 575px) {
    .advantages__items {
      -webkit-flex-wrap: wrap;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
    }
    .advantages__item {
      width: 48%;
      margin-bottom: 15px;
    }
  }


  .wrapper {
    max-width: 1250px;
    width: 100%;
    padding: 0 15px;
    margin: auto;
  }

  .minwrapper {
    max-width: 1090px;
    margin: 0 auto;
  }


  .footer {
    background-color: #121726;
    padding: 75px 0 60px;
  }

  .footer .wrapper {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
  }

  .footer__left-block {
    max-width: 460px;
    width: 41%;
  }

  .footer__logo {
    margin-bottom: 54px;
  }

  .footer__social-blocks {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
  }

  .footer__schedule div, .footer__phone div {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    margin-bottom: 5px;
  }

  .footer__schedule span, .footer__phone span {
    font-size: 12px;
    line-height: 16px;
    color: #808994;
  }

  .footer__schedule p,
  .footer__schedule a, .footer__phone p,
  .footer__phone a {
    font-size: 13px;
    line-height: 20px;
    color: #fff;
    margin-bottom: 0;
  }

  .footer__schedule span {
    width: 80px;
  }

  .footer__phone {
    margin-bottom: 30px !important;
  }

  .footer__phone div {
    width: 230px;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
  }

  .footer__social-email {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: end;
    -webkit-justify-content: flex-end;
    -ms-flex-pack: end;
    justify-content: flex-end;
  }

  .footer__social-email .footer-block {
    width: -webkit-max-content;
    width: -moz-max-content;
    width: max-content;
  }

  .footer__email {
    margin-bottom: 15px;
  }

  .footer__email a {
    line-height: 24px;
    letter-spacing: 1.3px;
    color: #C8CBD1;
  }

  .footer__social {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
  }

  .footer__social a {
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.5px;
    text-transform: uppercase;
    color: #C8CBD1;
    margin-right: 24px;
  }

  .footer__social a:last-of-type {
    margin-right: 0;
  }

  .footer__right-block {
    max-width: 700px;
    width: 100%;
  }

  .footer__top-inf {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -webkit-justify-content: space-between;
    -ms-flex-pack: justify;
    justify-content: space-between;
    margin-bottom: 30px;
  }

  .footer__information ul, .footer__clients ul, .footer__topcat ul {
    list-style-type: none;
    padding: 0;
  }

  .footer__information li, .footer__clients li, .footer__topcat li {
    margin-bottom: 7px;
  }

  .footer__information li:last-of-type, .footer__clients li:last-of-type, .footer__topcat li:last-of-type {
    margin-bottom: 0;
  }

  .footer__information li a, .footer__clients li a, .footer__topcat li a {
    font-size: 12px;
    line-height: 16px;
    color: #808994;
  }

  .footer__head-name {
    font-family: "GothamProMedium", sans-serif;
    font-size: 13px;
    line-height: 18px;
    color: #f9f9f9;
    border-bottom: 1px solid #808994;
    padding-bottom: 17px;
    margin-bottom: 20px;
  }

  .footer__bottom-inf {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
  }

  .footer__address {
    max-width: 225px;
    margin-right: 30px;
  }

  .footer__address-inf {
    display: inline-block;
    font-size: 12px;
    line-height: 16px;
    color: #808994;
    margin-bottom: 19px;
  }

  .footer__address-map {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    font-size: 9px;
    line-height: 9px;
    -webkit-text-decoration-line: underline;
    text-decoration-line: underline;
    text-transform: uppercase;
    color: #808994;
  }

  .footer__address-map::before {
    display: inline-block;
    width: 9px;
    height: 15px;
    content: "";
    background: url("../img/svg/footer-icon-map.svg") no-repeat center;
    margin-right: 7px;
  }

  .footer__text {
    max-width: 175px;
  }

  .footer__text div {
    font-size: 12px;
    line-height: 16px;
    color: #808994;
    margin-bottom: 15px;
  }

  .footer__text div:last-of-type {
    margin-bottom: 0;
  }

  @media screen and (max-width: 991px) {
    .footer .wrapper {
      -webkit-flex-wrap: wrap;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      max-width: 686px;
    }
    .footer__logo {
      width: 100%;
    }
    .footer__right-block {
      max-width: 100%;
      width: 100%;
    }
    .footer__information, .footer__clients, .footer__topcat {
      width: 27.5%;
    }
    .footer__left-block {
      max-width: 100%;
      width: 100%;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: end;
      -webkit-justify-content: flex-end;
      -ms-flex-pack: end;
      justify-content: flex-end;
    }
    .footer__bottom-inf {
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      -ms-flex-pack: justify;
      justify-content: space-between;
    }
    .footer__address {
      margin-right: 0;
    }
    .footer__phone-email, .footer__social-blocks, .footer__address {
      width: 27.5%;
    }
    .footer__social-email {
      width: 202px;
    }
    .footer__phone div {
      width: 100%;
    }
    .footer__text {
      margin-top: -40px;
    }
  }

  @media screen and (max-width: 767px) {
    .footer .wrapper {
      max-width: 560px;
    }
    .footer__left-block {
      -webkit-box-orient: vertical;
      -webkit-box-direction: normal;
      -webkit-flex-direction: column;
      -ms-flex-direction: column;
      flex-direction: column;
      -webkit-box-pack: start;
      -webkit-justify-content: flex-start;
      -ms-flex-pack: start;
      justify-content: flex-start;
      width: 39%;
    }
    .footer__bottom-inf {
      -webkit-flex-wrap: wrap;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      width: 100%;
    }
    .footer__phone-email, .footer__social-blocks, .footer__address {
      width: 100%;
    }
    .footer__social-blocks {
      margin-top: 30px;
      display: block;
    }
    .footer__address {
      margin-top: 30px;
    }
    .footer__right-block {
      width: 48%;
    }
    .footer__top-inf {
      -webkit-flex-wrap: wrap;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
    }
    .footer__top-inf ul {
      padding: 8px 0 27px 40px;
      margin-bottom: 0;
      display: none;
    }
    .footer__information, .footer__clients, .footer__topcat {
      width: 100%;
    }
    .footer__head-name {
      position: relative;
    }
    .footer__head-name::before {
      position: absolute;
      content: "";
      width: 12px;
      height: 12px;
      background: url("../img/svg/icon-drop-plus.svg") no-repeat center;
      right: 0;
      top: 2px;
    }
    .footer__head-name.active::before {
      background: url("../img/svg/icon-drop-minus.svg") no-repeat center;
    }
    .footer__text {
      margin-top: -15px;
    }
  }

  @media screen and (max-width: 575px) {
    .footer__left-block, .footer__right-block {
      width: 100%;
    }
    .footer__right-block {
      margin-top: 55px;
    }
    .footer__left-block {
      padding-left: 40px;
    }
    .footer__phone-email {
      width: 202px;
    }
  }
</style>
<!-- advantages -->
<section class="advantages">
  <div class="wrapper">
    <div class="advantages__items">
      <div class="advantages__item">
        <div class="advantages__img"><img src="img/svg/Pay.svg" alt=""></div>
        <div class="advantages__text"><?=$advantages_text_1;?></div>
      </div>
      <div class="advantages__item">
        <div class="advantages__img"><img src="img/svg/Return.svg" alt=""></div>
        <div class="advantages__text"><?=$advantages_text_2;?></div>
      </div>
      <div class="advantages__item">
        <div class="advantages__img"><img src="img/svg/Showroom.svg" alt=""></div>
        <div class="advantages__text"><?=$advantages_text_3;?></div>
      </div>
      <div class="advantages__item">
        <div class="advantages__img"><img src="img/svg/Try-on.svg" alt=""></div>
        <div class="advantages__text"><?=$advantages_text_4;?></div>
      </div>
    </div>
  </div>
</section>
<!-- end advantages -->

<footer class="footer">
  <div class="wrapper">
    <div class="footer__left-block">
      <div class="footer__logo">
        <div class="footer__logo-img"><a hre=""><img src="img/svg/footer-logo.svg" alt=""></a></div>
      </div>
      <div class="footer__social-blocks">
        <div class="footer__schedule">
          <div>
            <span><?=$footer_schedule_1;?>:</span>
            <p>9:00 – 18:00</p>
          </div>
          <div>
            <span><?=$footer_schedule_2;?>:</span>
            <p>10:00 – 20:00</p>
          </div>

        </div>
        <div class="footer__phone-email">
          <div class="footer__phone">
            <div>
              <span><?=$footer_phone_1;?></span>
              <a href="tel:0800330190">0 800 330 190 </a>
            </div>
            <div>
              <span><?=$footer_phone_1;?>.</span>
              <a href="tel:+380443343532">+380 44 33 43 53 2</a>
            </div>
            <div>
              <span>Viber</span>
              <a href="viber://chat?number=%2B380689046545">+380 68 904 65 45 </a>
            </div>
          </div>
          <div class="footer__social-email">
            <div class="footer_block">
              <div class="footer__email">
                <a href="mailto:info@feshemebel.com.ua">info@feshemebel.com.ua</a>
              </div>
              <div class="footer__social">
                <a href="https://www.facebook.com/feshemebel/" target="_blank">facebook</a>
                <a href="https://www.instagram.com/feshemebel/" target="_blank">instagram</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer__right-block">
      <div class="footer__top-inf">
        <?php if ($informations) { ?>
        <div class="footer__information">
          <div class="footer__head">

            <div class="footer__head-name"><?=$footer_head_name_1;?></div>
            <ul>
              <li><a href="<?php echo $url_menu_delivery; ?>"><?php echo $footer_menu_delivery; ?></a></li>
              <li><a href="<?php echo $url_menu_guaranty; ?>"><?php echo $footer_menu_guaranty; ?></a></li>
              <li><a href="<?php echo $url_menu_exchange; ?>"><?php echo $footer_menu_exchange; ?></a></li>
              <li><a href="<?php echo $url_menu_discount; ?>"><?php echo $footer_menu_discount; ?></a></li>
              <li><a href="<?php echo $url_menu_help; ?>"><?php echo $footer_menu_help; ?></a></li>
            </ul>
          </div>
        </div>

        <?php } ?>

        <div class="footer__clients">
          <div class="footer__head">
            <div class="footer__head-name"><?=$footer_head_name_2;?></div>
            <ul>
              <li><a href="<?php echo $url_menu_about; ?>"><?php echo $footer_menu_about; ?></a></li>
              <li><a href="<?php echo $url_menu_articles; ?>"><?php echo $footer_menu_articles; ?></a></li>
              <li><a href="<?php echo $url_menu_contacts; ?>"><?php echo $footer_menu_contacts; ?></a></li>
              <li><a href="<?php echo $url_menu_sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
            </ul>
          </div>
        </div>
        <div class="footer__topcat">
          <div class="footer__head">
            <div class="footer__head-name"><?=$footer_head_name_3;?></div>
            <ul>
              <?php foreach ($categories as $category) { ?>
                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer__bottom-inf">
        <div class="footer__address">
          <span class="footer__address-inf"><?=$footer_address_inf_3;?></span>
          <a href="https://www.google.com/maps/place/Sofia+Mall/@50.4026186,30.3331926,18z/data=!4m9!1m2!2m1!1z0KLQpiBTb2ZpYSBNYWxsLCAg0J_RgNC-0YHQv9C10LrRgiDQk9C10YDQvtC10LIg0J3QtdCx0LXRgdC90L7QuSDQodC-0YLQvdC4IDI0LzgzLCDQodC-0YTQuNC10LLRgdC60LDRjyDQkdC-0YDRidCw0LPQvtCy0LrQsA!3m5!1s0x40d4cb65e6249397:0x36888b78f1103405!8m2!3d50.4027993!4d30.3336751!15sCnzQotCmIFNvZmlhIE1hbGwsICDQn9GA0L7RgdC_0LXQutGCINCT0LXRgNC-0LXQsiDQndC10LHQtdGB0L3QvtC5INCh0L7RgtC90LggMjQvODMsINCh0L7RhNC40LXQstGB0LrQsNGPINCR0L7RgNGJ0LDQs9C-0LLQutCwWnsiedGC0YYgc29maWEgbWFsbCDQv9GA0L7RgdC_0LXQutGCINCz0LXRgNC-0LXQsiDQvdC10LHQtdGB0L3QvtC5INGB0L7RgtC90LggMjQgODMg0YHQvtGE0LjQtdCy0YHQutCw0Y8g0LHQvtGA0YnQsNCz0L7QstC60LCSAQ9zaG9wcGluZ19jZW50ZXI" class="footer__address-map" target="_blank"><?=$footer_address_inf_2;?></a>
        </div>
        <div class="footer__address">
          <span class="footer__address-inf"><?=$footer_address_inf;?></span>
          <a href="https://www.google.com/maps?ll=50.408209,30.334222&z=16&t=m&hl=uk&gl=UA&mapclient=embed&q=%D0%B2%D1%83%D0%BB%D0%B8%D1%86%D1%8F+%D0%AF%D0%B3%D1%96%D0%B4%D0%BD%D0%B0,+18+%D0%A1%D0%BE%D1%84%D1%96%D1%97%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%91%D0%BE%D1%80%D1%89%D0%B0%D0%B3%D1%96%D0%B2%D0%BA%D0%B0+%D0%9A%D0%B8%D1%97%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB.+08147" class="footer__address-map" target="_blank"><?=$footer_address_inf_2;?></a>
        </div>
        <div class="footer__text">
          <div><?=$footer_1;?></div>
          <div><?=$footer_2;?></div>
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- footer class="footer">
  <div class="container">
    <div class="row">
      <div class="footer-block col-md-3 col-sm-6">
        <p>© 2012-<?php echo date("Y"); ?> feshmebel.com.ua</p>
        <p><a href="mailto:info@feshemebel.com.ua">info@feshemebel.com.ua</a></p>
        <p><a href="tel:+380443343532">+38-044-334-35-32</a></p>

        <p><?php echo $footer_schedule; ?></p>
      </div>
      <?php if ($informations) { ?>
      <div class="footer-block col-md-3 col-sm-6">
        <ul class="list-unstyled">
          <li><a href="<?php echo $url_menu_delivery; ?>"><?php echo $footer_menu_delivery; ?></a></li>
          <li><a href="<?php echo $url_menu_guaranty; ?>"><?php echo $footer_menu_guaranty; ?></a></li>
          <li><a href="<?php echo $url_menu_exchange; ?>"><?php echo $footer_menu_exchange; ?></a></li>
          <li><a href="<?php echo $url_menu_discount; ?>"><?php echo $footer_menu_discount; ?></a></li>
          <li><a href="<?php echo $url_menu_help; ?>"><?php echo $footer_menu_help; ?></a></li>
        </ul>

      </div>
      <?php } ?>
      <div class="footer-block col-md-3 col-sm-6">
        <ul class="list-unstyled">
          <li><a href="<?php echo $url_menu_about; ?>"><?php echo $footer_menu_about; ?></a></li>
          <li><a href="<?php echo $url_menu_articles; ?>"><?php echo $footer_menu_articles; ?></a></li>
          <li><a href="<?php echo $url_menu_contacts; ?>"><?php echo $footer_menu_contacts; ?></a></li>
          <li><a href="<?php echo $url_menu_sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>

      <div class="footer-block col-md-3 col-sm-6">
        <ul class="soc-icons">


          </li>
		 
        </ul>
		 <div>
            <img style="display: inline-block; width: 33%;" src="/image/mastercard_securecode.png"  />
           <img style="display: inline-block; width: 27%;" src="/image/verified_by_visa.png"  />


          </div>
      </div>
    </div>
  </div>
  <br>

</footer -->

<div id="product-where-watch" style="display: none">
  <div class="col-sm-6">
    <a href="https://goo.gl/maps/mAmtvaxAJWehwXuY8" target="_blank"><?php echo $map_address2?></a>
    <div class="where_watch_shedule"><?php echo $where_watch_shedule2; ?></div>
    <div id="where-watch-map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1271.5327041792095!2d30.3331926!3d50.4026186!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cb65e6249397%3A0x36888b78f1103405!2sSofia%20Mall!5e0!3m2!1sru!2sua!4v1633073893623!5m2!1sru!2sua" width="100%" height="300" frameborder="0" style="border: 1px solid #121726; allowfullscreen=""></iframe></div>
  </div>
  <div class="col-sm-6">
    <a href="https://goo.gl/maps/p3PtVeDiEjp" target="_blank"><?php echo $map_address?></a>
    <div class="where_watch_shedule"><?php echo $where_watch_shedule; ?></div>
    <div id="where-watch-map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2542.7628540489804!2d30.3307559831889!3d50.4082570359722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4cbaa58942dc7%3A0x1571fd747a618d1b!2z0LLRg9C70LjRhtGPINCv0LPRltC00L3QsCwgMTgsINCh0L7RhNGW0ZfQstGB0YzQutCwINCR0L7RgNGJ0LDQs9GW0LLQutCwLCDQmtC40ZfQstGB0YzQutCwINC-0LHQuy4sIDA4MTMx!5e0!3m2!1suk!2sua!4v1560163205954!5m2!1suk!2sua" width="100%" height="300" frameborder="0" style="border: 1px solid #121726;" allowfullscreen=""></iframe></div>

  </div>
</div>

<div id="product-try-on" style="display: none;">
  <div style="padding: 10px">
  <p><?php echo $try_on_text; ?></p>
  <a href="<?php echo $try_on_link_more; ?>"><?php echo $text_link_more; ?></a>

  <form style="margin-top: 30px" id="try-on-form" method="post" action="<?php echo $try_on_action; ?>" class="form-horizontal">
    <input type="hidden" value="" name="options" id="try-on-options">
    <input type="hidden" value="" name="prod_id" id="try-on-prod-id">
    <div class="form-group">
      <label class="col-sm-12 col-md-2 control-label" for="try-on-name"><?php echo $text_try_on_f_name; ?></label>
      <div class="col-sm-12 col-md-10">
        <input type="text" name="name" value="" placeholder="<?php echo $text_try_on_f_name; ?>" id="try-on-name" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-12 col-md-2 control-label" for="try-on-name"><?php echo $text_try_on_f_phone; ?></label>
      <div class="col-sm-12 col-md-10">
        <input type="text" name="phone" value="" placeholder="<?php echo $text_try_on_f_phone; ?>" id="try-on-phone" class="form-control">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-12 col-md-2 control-label" for="try-on-name"><?php echo $text_try_on_f_address; ?></label>
      <div class="col-sm-12 col-md-10">
        <textarea rows="5" name="address"  placeholder="<?php echo $text_try_on_f_address; ?>" id="try-on-address" class="form-control"></textarea>
      </div>
    </div>
    <div class="alert alert-success" style="display: none"><?php echo $text_try_on_f_success; ?></div>
    <div class="clearfix">
      <input type="submit" class="btn btn-info pull-right" onclick="gtag('event', 'click', {'event_category': 'check'}); " value="<?php echo $text_try_on_f_submit; ?>">
    </div>
  </form>
  </div>
</div>
<style>
	<?php //echo $_SESSION['style_all']; ?> 
</style>
<?php
	if(isset($contact_information) || $contact_information != '')
	{
		echo $contact_information;
	}
?>

	<script type="text/javascript">

</script> 
	<?php /*
	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46241763 = new Ya.Metrika({
                    id:46241763,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/46241763" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
*/ ?>
<input type="hidden" id="csrf" value="<?php echo $csrf; ?>">




<div class="my-form"></div>
<script>
  $(document).ready(function(){
    $('body').copyright();
    /// console.log('copyright');
   // setTimeout(function () {

      (function(d, w, s) {
        var widgetHash = 'sdBUrXvAman5', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true;
        gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js';
        var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);
      })(document, window, 'script');
   // } ,7000)

  });
</script>

<div id="widgetmess">

  <div class="wm-container">
    <div class="launcher">
      <div class="launcher-button-wrapper" style="width: 60px; height: 60px">
        <a id="launcher-button" href="#" class="launcher-button" style="background-color: #ffd460; background-image: url(https://feshmebel.com.ua/image/widgetmess/ic_new2.png)">
          <div id="rings" class="rings" style="display: block;">
            <div class="ring" style="border-color: #ffd460"></div>
            <div class="ring" style="border-color: #ffd460"></div>
            <div class="ring" style="border-color: #ffd460"></div>
          </div>
        </a>
      </div>
    </div>
  </div>

  <div class="wm-icons">

    <div class="channels-wrapper desktop right">
      <div class="channels">
        <div class="channel-wrapper">
          <div class="channel bingc-run"
               style="width: 50px;height: 50px;background-color: #ffd460; background-size: 50% 50%; background-image: url(https://feshmebel.com.ua/image/widgetmess/phone-call-new.png);">
            <div class="tooltip">
              <div class="tooltip-inner">Binotel</div>
            </div>
          </div>
        </div>

        <div class="channel-wrapper">
          <div id="showEmailForm"data-toggle="modal" data-target="#myModal"  class="channel"
             style="width: 50px;height: 50px;background-color: #ffd460; background-size: 55% 55%; background-image: url(https://feshmebel.com.ua/image/widgetmess/email_new.png);">
            <div class="tooltip">
              <div class="tooltip-inner">E-mail</div>
            </div>
          </div>
        </div>

        <div class="channel-wrapper">

          <a href="viber://chat?number=%2B380689046545" target="_self" class="channel"
             style="width: 50px;height: 50px;background-color: #ffd460; background-size: 55% 55%; background-image: url(https://feshmebel.com.ua/image/widgetmess/viber_new2.png);">
            <div class="tooltip">
              <div class="tooltip-inner">Viber</div>
            </div>
          </a>
        </div>



        <!-- div class="channel-wrapper">
          <a href="https://wa.me/380966501715" target="_blank" class="channel"
             style="width: 50px;height: 50px;background-color: #4fc85c; background-size: 55% 55%; background-image: url(https://feshmebel.com.ua/image/widgetmess/whatsapp.png);">
            <div class="tooltip">
              <div class="tooltip-inner">WhatsApp</div>
            </div>
          </a>
        </div -->

      </div>
    </div>

  </div>

</div>

<script>



  // filter open

  $(window).resize(function () {
    if ($(window).width() <= 991) {
      $('.footer .wrapper').prepend($('.footer__logo'));
      $('.footer__logo').after($('.footer__right-block'));
      $('.footer__bottom-inf').prepend($('.footer__phone-email'));
      $('.footer__phone-email').after($('.footer__social-blocks'));
      $('.footer__text').appendTo($('.footer__left-block'));
    }

    if ($(window).width() <= 767) {
      $('.footer__logo').after($('.footer__left-block'));
      $('.footer__bottom-inf').appendTo($('.footer__left-block'));
      $('.footer__text').appendTo($('.footer__right-block'));
    }
  })

  if ($(window).width() <= 991) {
    $('.footer .wrapper').prepend($('.footer__logo'));
    $('.footer__logo').after($('.footer__right-block'));
    $('.footer__bottom-inf').prepend($('.footer__phone-email'));
    $('.footer__phone-email').after($('.footer__social-blocks'));
    $('.footer__text').appendTo($('.footer__left-block'));
  }

  if ($(window).width() <= 767) {
    $('.footer__logo').after($('.footer__left-block'));
    $('.footer__bottom-inf').appendTo($('.footer__left-block'));
    $('.footer__text').appendTo($('.footer__right-block'));

    $('.footer__head-name').click(function () {
      $(this).toggleClass('active');
      $(this).next().toggle();
    })
  }

  $('.my-form').load('index.php?route=information/contact/form');

  $(".launcher-button").click(function(){
    return $(".launcher-button").toggleClass("opened"),
            $(".launcher .rings").toggle(),
            $(".menu-round").toggleClass("open"),
            $(".wm-icons").toggle(),
            !1;
  });
  $(".bingc-run").click(function () {
    $('#bingc-phone-button-icon-text').click();
    $('#bingc-phone-button').click();
    $(".launcher-button").click();
  });
  $("#showEmailForm").click(function () {

    $(".launcher-button").click();
  });
</script>

<style>
  #bingc-phone-button.bingc-show
  {
    display: none;
  }
  #widgetmess, #widgetmess .wm-container {
    position: fixed;
    bottom: 0px;
    z-index: 9999996;
    right: 0;
    height: 122px;
    width: 115px;
  }
  #widgetmess .wm-container .launcher {
    text-align: right;
    height: 100%;
    width: 100%;
    position: relative;
    overflow: hidden;
  }
  #widgetmess .wm-container .launcher-button-wrapper {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    height: 60px;
    width: 60px;
    margin: auto;
    z-index: 3;
  }
  #widgetmess .wm-container .launcher-button {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    border: 1px solid transparent;
    background: #ff4242 center center no-repeat;
    background-size: 50%;
    left: -1px;
    position: relative;
    box-shadow: 0 0 30px rgba(116, 116, 116, .33);
    animation: launcher-button-init 0.7s cubic-bezier(0.62, 0.28, 0.23, 0.99), launcher-button infinite 6s 2s;
    transition: background-color 0.4s;
  }
  #widgetmess .wm-container .launcher-button.opened {
    background: #fff no-repeat center center !important;
    background-size: 40%;
    animation: launcher-button-init 0.7s cubic-bezier(0.62, 0.28, 0.23, 0.99), launcher-button-rotate 0.4s;
    border: 1px solid #ececec !important;
    background-image: url(https://feshmebel.com.ua/image/widgetmess/ic-close.png) !important;
  }
  #widgetmess .wm-container .launcher .rings {
    position: absolute;
    height: 100%;
    width: 100%;
    opacity: 0.6;
    z-index: -1;
    display: none;
  }
  #widgetmess .wm-container .launcher .rings .ring:nth-child(1) {
    animation-delay: 0.1s;
  }
  #widgetmess .wm-container .launcher .rings .ring {
    border: 1px solid #ff4242;
    border-radius: 50%;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    opacity: 0;
    animation: launcher-button-pulsate infinite 3s;
  }


  #widgetmess .wm-icons {
    overflow: hidden;
    animation: blinger-channels-desktop-show 0.4s cubic-bezier(0.16, 0.44, 0.2, 0.96);
    left: auto;
    right: 27px;
    display: none;
    position: fixed;
    bottom: 100px;
    width: 62px;
    max-height: calc(100% - 96px);
    z-index: 9999995;
  }
  #widgetmess .wm-icons .channels-wrapper {
    height: 100%;
  }


  #widgetmess .wm-icons .channels-wrapper.right .channels {
    float: right;
  }

  #widgetmess .wm-icons .channels-wrapper.desktop .channels {
    width: 62px;
  }
  #widgetmess .wm-icons .channels-wrapper .channel-wrapper {
    width: 100%;
    float: left;
  }
  #widgetmess .wm-icons .channels-wrapper .channel {
    position: relative;
    display: block;
    margin: 0 auto;
    border-radius: 50%;
    background: #ff4242 no-repeat center center;
    transition: all 0.6s;
    margin-bottom: 10px;
    width: 50px;
    height: 50px;
    box-shadow: 0 0 3px rgba(0, 0, 0, .2);
    cursor: pointer;
  }
  #widgetmess .wm-icons .channels-wrapper .tooltip {
    display: none;
    position: absolute;
    right: 60px;
    top: 50%;
    width: 180px;
    transform: translate(0, -50%);
    animation: channels-tooltip 0.2s;
  }
  #widgetmess .wm-icons .channels-wrapper.right .tooltip-inner {
    float: right;
    text-align: right;
  }

  button.scroll_up.icon-angle-up.btn,
  button.scroll_up.icon-angle-up.btn:hover {
    border-radius: 50px;
    width: 58px;
    height: 58px;
  }
  @keyframes launcher-powered-init{0%{transform:translateY(10px);opacity:0}to{transform:translateY(0);opacity:1}}
  @keyframes launcher-powered-close{0%{transform:translateY(0);opacity:1}to{transform:translateY(10px);opacity:0}}
  @keyframes launcher-button-rotate{0%{transform:rotate(-90deg)}100%{transform:rotate(0)}}
  @keyframes launcher-button-pulsate{60%{transform:scale(1, 1);opacity:0}70%{transform:scale(1, 1);opacity:1}100%{transform:scale(1.7, 1.7);opacity:0}}
  @keyframes launcher-button-init{0%{transform:scale(0);opacity:0}30%{transform:scale(0.5) rotate(-180deg);opacity:0}to{transform:scale(1) rotate(0);opacity:1}}
  @keyframes launcher-button{0%{transform:rotate(0)}5%{transform:rotate(359deg)}to{transform:rotate(360deg)}}
  @keyframes channels-tooltip{0%{opacity:0}to{opacity:1}}

</style>

</body>
</html>
