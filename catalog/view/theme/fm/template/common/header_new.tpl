<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s)
        {
            if (f.fbq) return;
            n = f.fbq = function (){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1382122958608780');
        fbq('track', 'PageView');
    </script>
    <noscript><img height=""1"" width=""1"" style=""display:none""
        src=""https://www.facebook.com/tr?id=1382122958608780&ev=PageView&noscript=1""
        /></noscript>
    <!-- End Facebook Pixel Code -->


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-TV5TH6K');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - AdWords: 971613570 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-971613570"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-971613570');
</script>



  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>

  <?php if(isset($_REQUEST['sort']) || $noindex){ ?>
	<meta name="robots" content="noindex, nofollow" />
  <?php } ?>
    <?php if($route=='product-category' || $route=='common-home') { ?>
    <?php
       $url=$_SERVER['REQUEST_URI'];
       if(str_replace('/ua/','/ua/amp',$url)!=$url)
       {
            $url = str_replace('/ua/','/ua/amp/',$url);
       }
       else
       {
            $url = '/amp'.$url;
       }
    ?>
        <link rel="amphtml" href="//feshmebel.com.ua<?=$url;?>">
    <?php } ?>

    <script>
        var url_search = '<?=$url_search;?>';
    </script>

  <?php
   		function minify($output) { $output = preg_replace("/(\n)+/", "\n", $output); $output = preg_replace("/\r\n+/", "\n", $output); $output = preg_replace("/\n(\t)+/", "\n", $output); $output = preg_replace("/\n(\ )+/", "\n", $output); $output = preg_replace("/\>(\n)+</", '><', $output); $output = preg_replace("/\>\r\n</", '><', $output); return preg_replace("/(\r)+/", " ", preg_replace("/(\n)+/", " ", $output)); }
		$styles_main=[
			
			'/catalog/view/javascript/bootstrap/css/bootstrap.min.css',
			'/catalog/view/javascript/font-awesome/css/font-awesome.min.css',
			'/catalog/view/theme/fm/stylesheet/jquery.bxslider.css',
			'/catalog/view/theme/fm/stylesheet/fonts.css',
            '/catalog/view/theme/fm/stylesheet/stylesheet.css',
			'/catalog/view/javascript/flipclock/flipclock.css',

			'/catalog/view/theme/fm/stylesheet/unicorns.css',
			'/catalog/view/theme/fm/stylesheet/media.css',
			
		];
		$styles_text='';
		 foreach ($styles_main as $style) {
		 	//$styles_text .= file_get_contents($_SERVER['DOCUMENT_ROOT'].$style)."\n";
		 	?>
			<link rel="stylesheet" href="<?=$style;?>?v=<?=time();?>" >
			<?php
		 }
	
	 ?>
     <style>
         @media (min-width: 992px) {
             #column-left {
                 width: 25%;
                 float: left;
             }
         }
     </style>
    <?php foreach ($styles as $style) { ?>
        <?php $styles_text .= file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.strtok($style['href'],'?'))."\n"; ?>
    <?php } ?>
    <?php
        $styles_text .= file_get_contents($_SERVER['DOCUMENT_ROOT'].'/catalog/view/theme/fm/stylesheet/callback.css')."\n";
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/catalog/view/theme/fm/stylesheet/m/'.$route.'.css',$styles_text);
        //echo $styles_text;
    ?>

    <link rel="stylesheet" href="/catalog/view/theme/fm/stylesheet/m/<?=$route;?>.css?v=<?=time();?>" >
    <style>
        .image-home{
            width: auto;
        }
    </style>
<script>
	<?php
		$scripts_main=[
			'/catalog/view/javascript/jquery/jquery-2.1.1.min.js',
            '/catalog/view/javascript/jquery/ui/jquery-ui.min.js',
			'/catalog/view/javascript/jquery.bxslider.min.js',
			'/catalog/view/javascript/bootstrap/js/bootstrap.min.js',
			'/catalog/view/javascript/jquery/jquery.maskedinput.min.js',
			'/catalog/view/javascript/jquery/jquery.inputmask.bundle.min.js',
			'/catalog/view/javascript/flipclock/flipclock.js',
			'/catalog/view/javascript/common.js',
			
		];
		$scripts_text='';
		 foreach ($scripts_main as $script) {
		 	$scripts_text .= file_get_contents($_SERVER['DOCUMENT_ROOT'].$script)."\n";
	
		 }
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/catalog/view/theme/fm/stylesheet/m/'.$route.'80.js',$scripts_text);
	 ?>
	
</script>
    <script src="/catalog/view/theme/fm/stylesheet/m/<?=$route;?>80.js?v=<?=time();?>"></script>
  
<?php echo $alternate; ?>
    <?php if(isset($_GET['page']) && $_GET['page'] > 1) { ?>
    <!--ss_pagination_page-->
    <?php } ?>
  <?php foreach ($links as $link) { ?>
    <?php if( $link['rel'] =='canonical' ){ ?>
        <?php if(!isset($_REQUEST['sort']) && !$noindex && isset($_REQUEST['mfp']) ){ ?>

            <link href="<?php echo $_SERVER['REQUEST_URI']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } else { ?>
            <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
    <?php } else { ?>
  	    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
  <?php } ?>
  <script>
  <?php
  $scripts_text='';
  foreach ($scripts as $script) { ?>
  <?php $scripts_text.=file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.strtok($script,'?'))."\n"; ?>
  <?php } ?>
  <?php 
  $scripts_main=[
	'/catalog/view/javascript/callback.js',
	'/catalog/view/javascript/jquery.simplemodal.js',
	'/catalog/view/javascript/copyright.min.js',

  ];
 foreach ($scripts_main as $script) {
 	$scripts_text .= file_get_contents($_SERVER['DOCUMENT_ROOT'].$script)."\n";

 }
  file_put_contents($_SERVER['DOCUMENT_ROOT'].'/catalog/view/theme/fm/stylesheet/m/'.$route.'-8.js',$scripts_text);
	//echo $scripts_text;
	 ?>

  </script>
    <script src="/catalog/view/theme/fm/stylesheet/m/<?=$route;?>-8.js?v=<?=time();?>"></script>


  <?php foreach ($analytics as $analytic) { ?>
  <?php echo $analytic; ?>
  <?php } ?>


<?php  ?>


  <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png?v=1.0">
  <link rel="shortcut icon" type="image/png" href="/favicon/favicon-32x32.png?v=1.0">
  <link rel="manifest" href="/favicon/manifest.json?v=1.0">
  <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg?v=1.0" color="#2d4059">
  <meta name="msapplication-config" content="/favicon/browserconfig.xml?v=1.0">
  <meta name="theme-color" content="#ffd460">
	 <script src="https://api.fondy.eu/static_common/v1/checkout/ipsp.js"></script>


</head>
<?php 
	require_once('./tools/Mobile_Detect.php'); 
	$detect = new Mobile_Detect;
?>
<body class="<?php echo $class; if($detect->isTablet()){echo ' tabletVersion';} ?>">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TV5TH6K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<?php if($banner_home['status']){ ?>
<div class="header-banner">

<div class="banner-img container w320">
	<img src="/image/<?=$banner_home['image1']?>" width="" height="auto" />
</div>
<div class="banner-img container w768">
	<img src="/image/<?=$banner_home['image2']?>" width="" height="auto" />
</div>
<div class="banner-img container w1024">
	<img src="/image/<?=$banner_home['image3']?>" width="" height="auto" />
</div>

</div>
<?php } ?>
<nav <?php if(true) { ?> style="display: none;" <?php } ?>  class="top-nav">
  <div class="container">
    <div class="row">
      <div class="top-nav__left col-xs-2 col-sm-8 col-md-9 hidden-xs">
        <button type="button" class="top-nav__menu-btn btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".top-nav__list">
          <i class="fa fa-bars"></i>
        </button>
        <ul class="collapse navbar-collapse top-nav__list">
          <li><a href="<?php echo $url_about; ?>"><?php echo $top_menu_about; ?></a></li>
          <li><a href="<?php echo $url_delivery; ?>"><?php echo $top_menu_delivery; ?></a></li>
          <li><a href="<?php echo $url_guaranty; ?>"><?php echo $top_menu_guaranty; ?></a></li>
          <li><a href="<?php echo $url_exchange; ?>"><?php echo $top_menu_exchange; ?></a></li>
          <li><a href="<?php echo $url_discount; ?>"><?php echo $top_menu_discount; ?></a></li>
            <li><a href="<?php echo $url_try_on; ?>"><?php echo $top_menu_try_on; ?></a></li>
            <li><a href="<?php echo $url_articles; ?>"><?php echo $text_articles; ?></a></li>
        </ul>
      </div>
      <div class="top-nav__right col-xs-12 col-sm-4 col-md-3">
		<p class="mb-work-tel work-tel">
            <span style="display: inline-block; vertical-align: top;">Тел: </span>
            <span style="display: inline-block; vertical-align: top; margin-left: 10px;"><a href="tel:+380443343532">+38(044)334-35-32</a><br><a href="tel:0800330190">0-800-330-190 (безкоштовно)</a></span>
        </p>

        <div class="top-nav__profile dropdown">
          <a href="/login" title="<?php echo $text_account; ?>" rel="nofollow" class="top-nav__profile-link dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <span class="top-nav__profile-link-text"><?php echo $text_account; ?></span>
            <!-- <span class="caret"></span> -->
          </a>
          <ul class="top-nav__dropdown-menu dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <!--li><a href="<!?php echo $download; ?>"><!?php echo $text_download; ?></a></li-->
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>" rel="nofollow" ><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>" rel="nofollow"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php echo $language; ?>
      </div>
    </div>
  </div>
</nav>
<header <?php if(true) { ?> style="display: none;" <?php } ?> class="header_mod <?php if ($detect->isMobile() && !$detect->isTablet()) { ?> mb-header<? } ?>">
  <div class="header__container container <?php if ($detect->isMobile() && !$detect->isTablet()) { ?>mb-header__container<?}?>">
  
  <div class="category-menu mb-menu">
	<nav id="mb-menu" class="navbar">
      <div class="navbar-header category-button" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <i class="fa fa-bars" aria-hidden="true"></i>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <?php foreach ($categories as $category) { ?>
		  
		  <?php 
			if($category['id'] == 88){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні', 'href' => '/ua/stolovaja/stulja/#obedennye'),
						array('name' => 'Барні кухонні', 'href' => '/ua/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки і лави', 'href' => '/ua/stolovaja/taburety-i-skami/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные', 'href' => '/stolovaja/stulja/#obedennye'),
						array('name' => 'Барные кухонные', 'href' => '/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки и скамьи', 'href' => '/stolovaja/taburety-i-skami/')
					);
				}
			}
			if($category['id'] == 87){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні столи', 'href' => '/ua/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обідні столи-трансформери', 'href' => '/ua/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетний столик', 'href' => '/ua/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столи письмові', 'href' => '/ua/ofis/stoly-pismennye/'),
						array('name' => 'Каркаси і опори для столів', 'href' => '/ua/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекти столів і стільців', 'href' => '/ua/stolovaja/obedennye-komplekty/'),
						array('name' => 'Стільниці для столів', 'href' => '/ua/stoli/stoleshnitsy-dlja-stolov/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные столы', 'href' => '/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обеденные столы-трансформеры', 'href' => '/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетный столик', 'href' => '/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столы письменные', 'href' => '/ofis/stoly-pismennye/'),
						array('name' => 'Каркасы и опоры для столов', 'href' => '/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекты столов и стульев', 'href' => '/stolovaja/obedennye-komplekty/'),
						array('name' => 'Столешницы для столов', 'href' => '/stoli/stoleshnitsy-dlja-stolov/')
					);
				}
			}
		  ?>
		  
          <?php if ($category['children']) { ?>
          <li class="dropdown">
            <a href="<?php echo $category['href']; ?>" class="dropdown-toggle"><?php echo $category['name']; ?></a>
            <div class="dropdown-menu">
              <div class="dropdown-inner">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($children as $child) { ?>
                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </div>
            </div>
          </li>
          <?php } else { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </nav>
    </div>
	<?php if ($detect->isMobile() && !$detect->isTablet()) { ?>
	<div class="mb-header-block">
		<div class="mb-header__logo header__logo" itemscope itemtype="http://schema.org/Organization">
		  <a href="/" class="header__logo-link" itemprop="url">
			  <img itemprop="logo" class="logo__img" src="/image/mblogo.png?v=2.0" alt="">
		  </a>
		</div>
		
			<div class="mb-nav__search nav__search">
			  <?php echo $search; ?>
			  <div style="display: none;" class="search-result-ajax" id="search-result-ajax"></div>
			</div>
		
		<div class="mb-top-nav__profile top-nav__profile dropdown">
          <a href="/login" title="<?php echo $text_account; ?>" class="top-nav__profile-link dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
          </a>
          <ul class="top-nav__dropdown-menu dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <!--li><a href="<!?php echo $download; ?>"><!?php echo $text_download; ?></a></li-->
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>
		<div class="mb-header__cart header__cart">
			<?php echo $cart; ?>
			
		</div>
	</div>
	<?php } ?>
  
    <div class="header__logo" itemscope itemtype="http://schema.org/Organization">
      <a href="/" class="header__logo-link" itemprop="url">
        <div class="logo">
          <i class="logo__icon logo__icon--1"></i>
          <i class="logo__icon logo__icon--2"></i>
          <i class="logo__icon logo__icon--3"></i>
          <i class="logo__icon logo__icon--4"></i>
          <i class="logo__icon logo__icon--5"></i>
          <img itemprop="logo" class="logo__img" src="/image/catalog/logo.svg?v=1.0" alt="">
        </div>
      </a>
    <?php if ($detect->isMobile() && !$detect->isTablet()) { ?>
	  <div class="mb-header__schedule">
		  <p><?php echo $text_work; ?></p>
		  <p>Тел: <a href="tel:+380443343532">+38-044-334-35-32</a></p>
		  <p>Тел: <a href="tel:0800330190">0-800-330-190 (безкоштовно)</a></p>
		</div>
    <?php } ?>
    </div>

    <div class="header__schedule">
      <p><?php echo $text_work; ?></p>
      <p>Тел: <a href="tel:+380443343532">+38-044-334-35-32</a></p>
	  <p> <a href="tel:0800330190">0-800-330-190 (безкоштовно)</a></p>
    </div>
	<?php if( !$detect->isMobile() || $detect->isTablet()){ ?>
    <div class="header__cart">
  		<?php echo $cart; ?>
      <div class="header__wish-compare">
        <a class="bookmark" href="/login/" id="wishlist-total" rel="nofollow" title="<?php echo $text_wishlist; ?>">
          <?php echo $text_wishlist; ?>
        </a>
        <a class="compare" href="/compare/" id="compare-total" rel="nofollow" title="<?php echo $text_compare; ?>" class="header-product-btn-i">
          <?php echo $text_compare; ?>
        </a>
      </div>
		</div>
	<?php } ?>
  </div>
</header>
<?php if( !$detect->isMobile() || $detect->isTablet()){ ?>
<?php if ($categories) { ?>
<div class="category-menu" <?php if(true) { ?> style="display: none;" <?php } ?> >
  <div class="container">
    <nav id="menu" class="navbar">
      <div class="navbar-header category-button" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <i class="fa fa-th" aria-hidden="true"></i>
        <span id="category"><?php echo $text_category; ?></span>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <?php foreach ($categories as $category) { ?>
		  
		  <?php 
			if($category['id'] == 88){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні', 'href' => '/ua/stolovaja/stulja/#obedennye'),
						array('name' => 'Барні кухонні', 'href' => '/ua/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки і лави', 'href' => '/ua/stolovaja/taburety-i-skami/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные', 'href' => '/stolovaja/stulja/#obedennye'),
						array('name' => 'Барные кухонные', 'href' => '/stolovaja/barnye-stulja/#kuhonnye'),
						array('name' => 'Табуретки и скамьи', 'href' => '/stolovaja/taburety-i-skami/')
					);
				}
			}
			if($category['id'] == 87){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні столи', 'href' => '/ua/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обідні столи-трансформери', 'href' => '/ua/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетний столик', 'href' => '/ua/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столи письмові', 'href' => '/ua/ofis/stoly-pismennye/'),
						array('name' => 'Каркаси і опори для столів', 'href' => '/ua/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекти столів і стільців', 'href' => '/ua/stolovaja/obedennye-komplekty/'),
						array('name' => 'Стільниці для столів', 'href' => '/ua/stoli/stoleshnitsy-dlja-stolov/')
					);
				}else{
					$category['children'] = array(
						array('name' => 'Кухонные обеденные столы', 'href' => '/stolovaja/stoly/#kuhonnye'),
						array('name' => 'Обеденные столы-трансформеры', 'href' => '/stolovaja/stoly-transformery/#obedennye'),
						array('name' => 'Туалетный столик', 'href' => '/spalnya/tualetnye-stoliki/#stolik'),
						array('name' => 'Столы письменные', 'href' => '/ofis/stoly-pismennye/'),
						array('name' => 'Каркасы и опоры для столов', 'href' => '/stolovaja/opory-dlja-stolov/'),
						array('name' => 'Комплекты столов и стульев', 'href' => '/stolovaja/obedennye-komplekty/'),
						array('name' => 'Столешницы для столов', 'href' => '/stoli/stoleshnitsy-dlja-stolov/')
					);
				}
			}
		  ?>
		  
          <?php if ($category['children']) { ?>
          <li class="dropdown">
            <a href="<?php echo $category['href']; ?>" class="dropdown-toggle"><?php echo $category['name']; ?></a>
            <div class="dropdown-menu">
              <div class="dropdown-inner">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($children as $child) { ?>
                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </div>
            </div>
          </li>
          <?php } else { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </nav>
	
		<div class="nav__search">


		</div>
	
  </div>
</div>
<?php } ?>
<?php } ?>

    <header class="header header-fixed"  >
        <div class="header__top header-top">
            <div class="wrapper">
                <div class="row">
                    <div class="header-top__left">
                        <div class="header-top__left-block">
                            <div class="header-top__logo logo">
                                <div class="logo__img"><a href=""><img src="img/svg/logo-main_ru.svg" alt="" onmouseout="this.src='img/svg/logo-main_ru.svg'" onmouseover="this.src='img/svg/fm_logo-main_ru_hover.svg'"></a></div>
                            </div>
                        </div>
                        <div class="header-top__right-block">
                            <div class="header-top__phone phone">
                                <div class="phone__visible">
                                    <span><?=$text_free;?></span>
                                    <a href="tel:0800330190">0 800 330 190</a>
                                    <div class="phone__block">
                                        <div class="phone__dropdown">
                                            <div class="phone__kind">
                                                <span>Тел.</span>
                                                <a href="tel:+380443343532">+38 044 334 35 32</a>
                                            </div>
                                            <!-- div class="phone__kind">
                                                <span>Viber</span>
                                                <a href="tel:">+380 68 904 65 45</a>
                                            </div --->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header-top__schedule">
                                <div>
                                    <span>Пн-Пт</span>
                                    <p>9:00 - 20:00</p>
                                </div>
                                <div>
                                    <span>Сб-Вс</span>
                                    <p>10:00 - 20:00</p>
                                </div>
                            </div>
                            <?php echo $language; ?>
                        </div>
                    </div>
                    <div class="header-top__right">
                        <div class="header-top__user">
                            <?php if ($logged) { ?>

                                <a href="<?=$account;?>"><div class="header-top__user-img"></div></a>
                            <?php } else { ?>
                                <a href="<?=$login;?>"><div class="header-top__user-img"></div></a>
                            <?php } ?>


                            <div class="header-top__user-block">
                                <div class="header-top__user-bg"></div>
                                <div class="header-top__user-dropdown user-dropdown">
                                    <div class="user-dropdown__sign-up">
                                        <div class="user-dropdown__close"></div>
                                        <h4 class="user-dropdown__head .h4">личный кабинет</h4>
                                        <p class="user-dropdown__description">Зарегистрируйтесь с помощью вашего аккаунта:</p>
                                        <div class="user-dropdown__socials">
                                            <a href="" class="user-dropdown__social-link facebook-btn">Facebook</a>
                                            <a href="" class="user-dropdown__social-link google-btn">Google</a>
                                        </div>
                                        <div class="user-dropdown__either"><span>или</span></div>
                                        <form action="" class="user-dropdown__form">
                                            <div class="form__input">
                                                <input type="text" id="email" class="login-field" placeholder="Ваш Email*"><label for="email">Ваш Email*</label>
                                                <span class="form__required" data-title="Обязательное окно">Ваш Email*</span>
                                                <span class="form__please">Пожалуйста, введите ваш Email</span>
                                                <span class="form__error">Неверный адрес электронной почты</span>
                                            </div>
                                            <div class="form__input">
                                                <input type="password" id="pass" class="login-field" placeholder="Пароль*"><label for="pass">Пароль*</label>
                                                <span class="form__required" data-title="Обязательное окно">Пароль*</span>
                                                <span class="form__please">Пожалуйста, введите ваш пароль</span>
                                                <span class="form__error">Неверный пароль</span>
                                            </div>
                                            <div class="form__input">
                                                <input type="password" id="pass-again" class="login-field" placeholder="Повторите пароль*"><label for="pass-again">Повторите пароль*</label>
                                                <span class="form__required" data-title="Обязательное окно">Пароль*</span>
                                                <span class="form__please">Пожалуйста, введите ваш пароль</span>
                                                <span class="form__error">Неверный пароль</span>
                                            </div>
                                            <div>
                                                <input type="checkbox" class="checkbox" id="notifications" name="" value=""><label for="notifications" class="user-dropdown__checkbox-label">Получать уведомления о новинках, акциях, скидках</label>
                                            </div>
                                            <div>
                                                <input type="submit" class="user-dropdown__button sign-up-btn" value="зарегистрироваться"></div>
                                        </form>
                                        <div class="user-dropdown__agreement">
                                            <p>Регистрируясь, я принимаю условия <a href="">пользовательского соглашения</a></p>
                                        </div>
                                        <div class="user-dropdown__come-in">
                                            <p class="user-dropdown__question">Уже зарегистрирован?</p>
                                            <span class="user-dropdown__come-in-link">Войти</span>
                                        </div>
                                    </div>
                                    <div class="user-dropdown__sign-in">
                                        <div class="user-dropdown__close"></div>
                                        <h4 class="user-dropdown__head .h4">личный кабинет</h4>
                                        <p class="user-dropdown__description">Войдите с помощью вашего аккаунта:</p>
                                        <div class="user-dropdown__socials">
                                            <a href="" class="user-dropdown__social-link facebook-btn">Facebook</a>
                                            <a href="" class="user-dropdown__social-link google-btn">Google</a>
                                        </div>
                                        <div class="user-dropdown__either"><span>или</span></div>
                                        <form action="" class="user-dropdown__form">
                                            <div class="form__input">
                                                <input type="text" id="email" class="login-field" placeholder="Ваш Email*"><label for="email">Ваш Email*</label>
                                                <span class="form__required" data-title="Обязательное окно">Ваш Email*</span>
                                                <span class="form__please">Пожалуйста, введите ваш Email</span>
                                                <span class="form__error">Неверный адрес электронной почты</span>
                                            </div>
                                            <div class="form__input">
                                                <input type="password" id="pass" class="login-field" placeholder="Пароль*"><label for="pass">Пароль*</label>
                                                <span class="form__required" data-title="Обязательное окно">Пароль*</span>
                                                <span class="form__please">Пожалуйста, введите ваш пароль</span>
                                                <span class="form__error">Неверный пароль</span>
                                            </div>
                                            <div class="user-dropdown__remember-forgot remember-forgot">
                                                <div class="remember-forgot__checkbox">
                                                    <input type="checkbox" class="checkbox" id="remember-me" name="" value="">
                                                    <label for="remember-me" class="user-dropdown__checkbox-label">Запомнить меня</label>
                                                </div>
                                                <a href="" class="remember-forgot__link">Забыли пароль?</a>
                                            </div>
                                            <div>
                                                <input type="submit" class="user-dropdown__button sign-up-btn" value="Войти"></div>
                                        </form>

                                        <div class="user-dropdown__come-in">
                                            <p class="user-dropdown__question">Еще нет аккаунта?</p>
                                            <span class="user-dropdown__come-in-link user-dropdown__reg-link">Зарегистрируйтесь</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="user-dropdown__menu-block">
                                <div class="user-dropdown__menu">
                                    <div class="user-dropdown__menu-content">
                                        <div class="user-dropdown__close"></div>
                                        <h4 class="user-dropdown__menu-head .h4">lilya</h4>
                                        <div class="user-dropdown__menu-items">
                                            <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Личные данные</a></div>
                                            <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Адрес доставки</a></div>
                                            <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">История заказов</a></div>
                                            <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Обмен и возврат</a></div>
                                            <div class="user-dropdown__menu-item"><a class="user-dropdown__menu-link" href="">Выход</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-top__basket basket">
                            <div class="basket__wish basket-wish basket__block">
                                <a href="<?=$wishlist;?>"><div class="img"></div></a>

                                <!--div class="basket-wish__block">
                                    <div class="basket-buy__bg"></div>


                                    <div class="basket-wish__product">
                                        <div class="user-dropdown__close basket-wish__close"></div>
                                        <div class="basket-wish__content">
                                            <h4 class="basket-wish__head h4">избранные товары</h4>
                                            <div class="basket-wish__product-block">
                                                <div class="basket-wish__product-item">
                                                    <div class="basket-wish__product-img-del">
                                                        <div class="basket-wish__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                        <div class="basket-wish__product-del">Удалить</div>
                                                    </div>
                                                    <div class="basket-wish__product-information">
                                                        <div class="basket-wish__product-head">Стул Navarra (Наварра) - 113226</div>
                                                        <ul class="basket-wish__product-inf">
                                                            <li>Цвет: <span>Белый</span></li>
                                                        </ul>
                                                        <div class="basket-wish__product-price">
                                                            <div class="basket-wish__product-discount">-632</div>
                                                            <div class="basket-wish__product-old">1932</div>
                                                            <div class="basket-wish__product-cost">1300 грн</div>
                                                        </div>
                                                        <div class="basket-wish__add-basket">в корзину</div>
                                                    </div>
                                                </div>
                                                <div class="basket-wish__product-item">
                                                    <div class="basket-wish__product-img-del">
                                                        <div class="basket-wish__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                        <div class="basket-wish__product-del">Удалить</div>
                                                    </div>
                                                    <div class="basket-wish__product-information">
                                                        <div class="basket-wish__product-head">Стул Navarra (Наварра) - 113226</div>
                                                        <ul class="basket-wish__product-inf">
                                                            <li>Цвет: <span>Белый</span></li>
                                                        </ul>
                                                        <div class="basket-wish__product-price">
                                                            <div class="basket-wish__product-discount">-632</div>
                                                            <div class="basket-wish__product-old">1932</div>
                                                            <div class="basket-wish__product-cost">1300 грн</div>
                                                        </div>
                                                        <div class="basket-wish__add-basket">в корзину</div>
                                                    </div>
                                                </div>
                                                <div class="basket-wish__product-item">
                                                    <div class="basket-wish__product-img-del">
                                                        <div class="basket-wish__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                        <div class="basket-wish__product-del">Удалить</div>
                                                    </div>
                                                    <div class="basket-wish__product-information">
                                                        <div class="basket-wish__product-head">Стул Navarra (Наварра) - 113226</div>
                                                        <ul class="basket-wish__product-inf">
                                                            <li>Цвет: <span>Белый</span></li>
                                                        </ul>
                                                        <div class="basket-wish__product-price">
                                                            <div class="basket-wish__product-discount">-632</div>
                                                            <div class="basket-wish__product-old">1932</div>
                                                            <div class="basket-wish__product-cost">1300 грн</div>
                                                        </div>
                                                        <div class="basket-wish__add-basket">в корзину</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="basket-wish__amount-block basket-amount">
                                                <div class="basket-amount">
                                                    <div class="basket-amount__goods">
                                                        <div class="basket-amount__goods-name"><span>Товаров</span></div>
                                                        <div class="basket-amount__goods-count">1<span> шт</span></div>
                                                    </div>
                                                    <div class="basket-amount__price">
                                                        <div class="basket-amount__price-name"><span>Сумма</span></div>
                                                        <div class="basket-amount__price-value">14 999<span> грн</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="" class="basket-proceed-btn basket-wish-btn">добавить все в корзину</a>
                                        <a href="" class="basket-proceed-btn">продолжить покупки</a>
                                    </div>

                                </div -->

                            </div>
                            <div class="basket__compare basket-compare basket__block">
                                <!--div class="basket-compare__count">3</div-->
                                <a href="<?=$compare;?>"><div class="img"></div></a>

                                <!--div class="basket-compare__block">
                                    <div class="basket-buy__bg"></div>


                                    <div class="basket-compare__product">
                                        <div class="user-dropdown__close basket-compare__close"></div>
                                        <div class="basket-compare__content">
                                            <h4 class="basket-compare__head h4">ваши сравнения</h4>
                                            <div class="basket-compare__product-block">
                                                <div class="basket-compare__product-item">
                                                    <div class="basket-compare__product-img-del">
                                                        <div class="basket-compare__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                        <div class="basket-compare__product-del">Удалить</div>
                                                    </div>
                                                    <div class="basket-compare__product-information">
                                                        <div class="basket-compare__product-head">Стул Navarra (Наварра) - 113226</div>
                                                        <ul class="basket-compare__product-inf">
                                                            <li>Цвет: <span>Белый</span></li>
                                                        </ul>
                                                        <div class="basket-compare__product-price">
                                                            <div class="basket-compare__product-discount">-632</div>
                                                            <div class="basket-compare__product-old">1932</div>
                                                            <div class="basket-compare__product-cost">1300 грн</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="basket-compare__product-item">
                                                    <div class="basket-compare__product-img-del">
                                                        <div class="basket-compare__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                        <div class="basket-compare__product-del">Удалить</div>
                                                    </div>
                                                    <div class="basket-compare__product-information">
                                                        <div class="basket-compare__product-head">Стул Navarra (Наварра) - 113226</div>
                                                        <ul class="basket-compare__product-inf">
                                                            <li>Цвет: <span>Белый</span></li>
                                                        </ul>
                                                        <div class="basket-compare__product-price">
                                                            <div class="basket-compare__product-discount"></div>
                                                            <div class="basket-compare__product-old"></div>
                                                            <div class="basket-compare__product-cost">1300 грн</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="basket-compare__product-item">
                                                    <div class="basket-compare__product-img-del">
                                                        <div class="basket-compare__product-img"><img src="img/promotions/chair.jpg" alt=""></div>
                                                        <div class="basket-compare__product-del">Удалить</div>
                                                    </div>
                                                    <div class="basket-compare__product-information">
                                                        <div class="basket-compare__product-head">Стул Navarra (Наварра) - 113226</div>
                                                        <ul class="basket-compare__product-inf">
                                                            <li>Цвет: <span>Белый</span></li>
                                                        </ul>
                                                        <div class="basket-compare__product-price">
                                                            <div class="basket-compare__product-discount">-632</div>
                                                            <div class="basket-compare__product-old">1932</div>
                                                            <div class="basket-compare__product-cost">1300 грн</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="basket-compare__amount-block basket-amount">
                                                <div class="basket-amount">
                                                    <div class="basket-amount__goods">
                                                        <div class="basket-amount__goods-name"><span>Товаров</span></div>
                                                        <div class="basket-amount__goods-count">1<span> шт</span></div>
                                                    </div>
                                                    <div class="basket-amount__price">
                                                        <div class="basket-amount__price-name"><span>Сумма</span></div>
                                                        <div class="basket-amount__price-value">14 999<span> грн</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="" class="basket-proceed-btn basket-compare-btn">сравнить&nbsp;<span class="basket-compare__product-count">4</span></a>
                                        <a href="" class="basket-proceed-btn">продолжить покупки</a>
                                    </div>

                                </div -->
                            </div>
                            <?php echo $cart; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="header__bottom header-bottom">
            <div class="wrapper">
                <div class="row">
                    <div class="header-bottom__left">
                        <div class="header-bottom__logo logo">
                            <div class="logo__img"><a href=""><img src="img/svg/fm_icon-logo.svg" alt="" onmouseout="this.src='img/svg/fm_icon-logo.svg'" onmouseover="this.src='img/svg/fm_icon-logo_hover.svg'"></a></div>
                        </div>
                        <div class="header__menu header-menu">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <?php if($lang != 'uk') { ?>
                            <ul class="header-menu__list">
                                <li class="sub"><span>Каталог</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <a href="/stulya/" class="cat-name active">Стулья</a>
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/stolovaja/stulja/">Стулья кухонные</a></li>
                                                                    <li><a href="/ofis/ofisnye-stulja/">Офисные стулья</a></li>
                                                                    <li><a href="/stolovaja/barnye-stulja/">Барные стулья</a></li>
                                                                    <li><a href="/stolovaja/stulja/priznachennya_kafe-i-bary/">Стулья для кафе</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Популярные</span>
                                                                <ul>
                                                                    <li><a href="/stolovaja/stulja/material-karkasa_derevo/">Стулья деревянные</a></li>
                                                                    <li><a href="/stulya/material-karkasa_metall/">Стулья на металлокаркасе</a></li>
                                                                    <li><a href="/stolovaja/stulja/material-sidinnya_plastik/">Пластиковые стулья</a></li>
                                                                    <li><a href="/stolovaja/stulja/osoblivosti_myagkie/">Мягкие стулья</a></li>
                                                                    <li><a href="/stulya/osoblivosti_s-podlokotnikami/">Стулья с подлокотниками</a></li>
                                                                    <li><a href="/stulya/osoblivosti_raskladnye/">Стулья складные</a></li>
                                                                    <li><a href="/letnjaja-mebel/stulja-ulichnye/">Стулья уличные</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Стиль</span>
                                                                <ul>
                                                                    <li><a href="/stolovaja/stulja/stil_sovremennyy/">Стулья Модерн</a></li>
                                                                    <li><a href="/stolovaja/stulja/stil_klassicheskiy/">Стулья Классика</a></li>
                                                                    <li><a href="/stolovaja/stulja/stil_loft/">Стулья Лофт</a></li>
                                                                    <li><a href="/stolovaja/stulja/stil_dizaynerskie-repliki/">Стулья Дизайнерские</a></li>
                                                                    <li><a href="/stulya/stil_skandinavskiy/">Стулья Скандинавские</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/stulya/">Все стулья</a></span>
                                                                <ul>
                                                                    <li><a href="/stulya/"><img src="/image/catalog/subcategory/New_Menu/chairs.jpg" alt="Все стулья"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name">
                                                        <a href="/stoli/" class="cat-name">Столы</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/stolovaja/stoly/">Столы кухонные</a></li>
                                                                    <li><a href="/ofis/stoly-pismennye/">Письменные столы</a></li>
                                                                    <li><a href="/gostinye/zhurnalnye-stoly/">Журнальные столики</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Популярные</span>
                                                                <ul>
                                                                    <li><a href="/stolovaja/stoly/osoblivosti_raskladnye/">Раскладные столы</a></li>
                                                                    <li><a href="/stolovaja/stoly/material-stil-nitsi_derevo/">Деревянные столы</a></li>
                                                                    <li><a href="/stolovaja/stoly/material-stil-nitsi_steklo/">Стеклянные столы</a></li>
                                                                    <li><a href="/stoli/forma-stola_kruglyy/">Круглые столы</a></li>
                                                                    <li><a href="/letnjaja-mebel/stoly-ulichnye/">Столы уличные</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Стиль</span>
                                                                <ul>
                                                                    <li><a href="/stoli/stil_sovremennyy/">Столы Модерн</a></li>
                                                                    <li><a href="/stoli/stil_klassicheskiy/">Столы Классические</a></li>
                                                                    <li><a href="/stoli/stil_dizaynerskie-repliki/">Столы Дизайнерские</a></li>
                                                                    <li><a href="/stoli/stil_loft/">Столы Лофт</a></li>
                                                                    <li><a href="/stoli/stil_skandinavskiy/">Столы Скандинавские</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/stoli/">Все столы</a></span>
                                                                <ul>
                                                                    <li><a href="/stoli/"><img src="/image/catalog/subcategory/New_Menu/Tables.jpg" alt="Все столы"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name">
                                                        <a href="/kresla/" class="cat-name">Кресла</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/gostinye/kresla/">Кресла для дома</a></li>
                                                                    <li><a href="/ofis/kresla-ofisnie/">Офисные кресла</a></li>
                                                                    <li><a href="/mjagkaja-mebel/kresla-barnye/">Барные кресла</a></li>
                                                                    <li><a href="/mjagkaja-mebel/kreslo/priznachennya_kafe-i-bary/">Кресла для кафе и ресторанов</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Популярные</span>
                                                                <ul>
                                                                    <li><a href="/mjagkaja-mebel/kreslo/">Мягкие кресла</a></li>
                                                                    <li><a href="/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-rukovoditeley/">Кресла для руководителей</a></li>
                                                                    <li><a href="/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-personala/">Кресла для персонала</a></li>
                                                                    <li><a href="/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-posetiteley/">Кресла для посетителей</a></li>
                                                                    <li><a href="/ofis/kresla-ofisnie/tip-krisla_geymerskie-kresla/">Геймерские кресла</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Матириал изготовления</span>
                                                                <ul>
                                                                    <li><a href="/gostinye/kresla/material-sidinnya_tkan/ ">Кресла в ткани</a></li>
                                                                    <li><a href="/gostinye/kresla/material-sidinnya_kozhzam/ ">Кресла в кожзаме</a></li>
                                                                    <li><a href="/gostinye/kresla/material-karkasa_derevo/ ">Кресла на деревянном каркасе</a></li>
                                                                    <li><a href="/gostinye/kresla/material-karkasa_metall/ ">Кресла на металлокаркасе</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/kresla/">Все кресла</a></span>
                                                                <ul>
                                                                    <li><a href="/kresla/"><img src="/image/catalog/subcategory/New_Menu/Armchairs.jpg" alt="Все кресла"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
													<li class="header-menu__cat-name">
                                                        <a href="/mjagkaja-mebel/divany/" class="cat-name">Диваны</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/mjagkaja-mebel/divany/tip-divana_pryamye-divany/ ">Диваны прямые</a></li>
                                                                    <li><a href="/mjagkaja-mebel/divany/tip-divana_uglovye-divany/ ">Диваны угловые</a></li>
                                                                    <li><a href="/mjagkaja-mebel/banketki/">Диванчики-банкетки</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Назначение</span>
                                                                <ul>
                                                                    <li><a href="/mjagkaja-mebel/divany/priznachennya_dom/ ">Диваны для дома</a></li>
                                                                    <li><a href="/mjagkaja-mebel/divany/priznachennya_ofis/ ">Диваны для офиса</a></li>
                                                                    <li><a href="/mjagkaja-mebel/divany/priznachennya_kafe-i-bary/ ">Диваны для кафе</a></li>

                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Стиль</span>
                                                                <ul>
                                                                    <li><a href="/mjagkaja-mebel/divany/stil_sovremennyy/ ">Диваны Модерн</a></li>
                                                                    <li><a href="/mjagkaja-mebel/divany/stil_klassicheskiy/ ">Диваны Классические</a></li>
                                                                    <li><a href="/mjagkaja-mebel/divany/stil_loft/ ">Диваны Лофт</a></li>
                                                                    <li><a href="/mjagkaja-mebel/divany/stil_skandinavskiy/ ">Диваны Скандинавские</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/mjagkaja-mebel/divany/">Все диваны</a></span>
                                                                <ul>
                                                                    <li><a href="/mjagkaja-mebel/divany/"><img src="/image/catalog/subcategory/New_Menu/sofa.jpg" alt="Все диваны"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name mod">
                                                        <a href="/stolovaja/opory-dlja-stolov/" class="cat-name mod">Опоры для столов</a>
                                                    </li>
                                                    <li class="header-menu__cat-name mod">
                                                        <a href="/stoli/stoleshnitsy-dlja-stolov/" class="cat-name mod">Столешницы</a>

                                                    </li>
                                                    
                                                    <li class="header-menu__cat-name">
                                                        <a href="/gostinye/" class="cat-name">Мебель для гостиной</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <ul>
                                                                    <li><a href="/gostinye/kresla/">Кресла</a></li>
                                                                    <li><a href="/gostinye/zhurnalnye-stoly/">Журнальные столы</a></li>
                                                                    <li><a href="/gostinye/knizhnye-shkafy/">Полки и Этажерки</a></li>
                                                                    <li><a href="/gostinye/vitriny/">Витрины</a></li>
                                                                    <li><a href="/gostinye/komody_i_tumby_v_gostinuu/">Тумбы и Комоды</a></li>
                                                                    <li><a href="/gostinye/tumby-pod-tv/">Тумбы под ТВ</a></li>

                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name">
                                                        <a href="/spalnya/" class="cat-name">Мебель для спальни</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                      
                                                                <ul>
                                                                    <li><a href="/spalnya/krovati/">Кровати</a></li>
                                                                    <li><a href="/matrasy">Матрасы</a></li>
                                                                    <li><a href="/spalnya/tumby/">Тумбы</a></li>
                                                                    <li><a href="/spalnya/komody/">Комоды</a></li>
                                                                    <li><a href="/spalnya/garderoby/">Гардеробы</a></li>
                                                                    <li><a href="/spalnya/zerkala-v-spalnu/">Зеркала</a></li>
                                                                    <li><a href="/spalnya/tualetnye-stoliki/">Туалетные столики</a></li>
                                                                    <li><a href="/spalnya/puffs/">Пуфы для спальни</a></li>

                                                                </ul>
                                                            </div>


                                                        </ul>
                                                    </li>

                                                    <li class="header-menu__cat-name">
                                                        <a href="/mjagkaja-mebel/" class="cat-name">Мягкая мебель</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <ul>
                                                                    <li><a href="/mjagkaja-mebel/kreslo/">Кресла</a></li>
                                                                    <li><a href="/mjagkaja-mebel/kresla-barnye/">Кресла барные</a></li>
                                                                    <li><a href="/mjagkaja-mebel/banketki/">Банкетки</a></li>
                                                                    <li><a href="/mjagkaja-mebel/pufiki/">Пуфики</a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>

                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Кухня</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">

                                                                <ul>
                                                                    <li><a href="/stolovaja/stoly/">Столы кухонные</a></li>
                                                                    <li><a href="/stolovaja/stulja/">Стулья кухонные</a></li>
                                                                    <li><a href="/stolovaja/barnye-stulja/">Барные стулья</a></li>
                                                                    <li><a href="/stolovaja/taburety-i-skami/">Табуреты и скамьи</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">

                                                                        <ul>     <li><a href="/stolovaja/opory-dlja-stolov/">Опоры для столов</a></li>
                                                                    <li><a href="/stoli/stoleshnitsy-dlja-stolov/">Столешницы для столов</a></li>
                                                                    <li><a href="/stolovaja/stoly-transformery/">Столы-трансформеры</a></li>
                                                                    <li><a href="/stolovaja/obedennye-komplekty/">Обеденные комплекты</a></li>

                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/stolovaja/">Вся мебель для кухни</a></span>
                                                                <ul>
                                                                    <li><a href="/stolovaja/"><img src="/image/catalog/subcategory/New_Menu/Kitchen.jpg" alt="Вся мебель для кухни"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Кафе</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">

                                                                <ul>
                                                                    <li><a href="/mebel-dlja-kafe/stoly-dlja-kafe">Столы для кафе</a></li>
                                                                    <li><a href="/stolovaja/opory-dlja-stolov/">Опоры для столов</a></li>
                                                                    <li><a href="/stoli/stoleshnitsy-dlja-stolov/">Столешницы для столов</a></li>
                                                                    <li><a href="/stolovaja/stulja/priznachennya_kafe-i-bary/#kafe">Стулья для кафе</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">

                                                                <ul>    <li><a href="/mjagkaja-mebel/kreslo/priznachennya_kafe-i-bary/#kafe">Кресла для кафе</a></li>
                                                                    <li><a href="/stolovaja/barnye-stulja/#kafe">Барные стулья</a></li>
                                                                    <li><a href="/stolovaja/barnye-stulja/osoblivosti_bez-spinki/">Барные табуреты</a></li>
                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/mebel-dlja-kafe/">Вся мебель для кафе</a></span>
                                                                <ul>
                                                                    <li><a href="/mebel-dlja-kafe/"><img src="/image/catalog/subcategory/New_Menu/Kafe.jpg" alt="Вся мебель для кафе"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Офис</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">

                                                                <ul>
                                                                    <li><a href="/ofis/kresla-ofisnie/">Кресла офисные</a></li>
                                                                    <li><a href="/ofis/ofisnye-stulja/">Стулья офисные</a></li>
                                                                    <li><a href="/ofis/stoly-pismennye/">Письменные, офисные столы</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">

                                                                <ul>    <li><a href="/ofis/stellazh/">Стеллажи</a></li>
                                                                    <li><a href="/ofis/karkasy-i-opory-dlja-stolov/">Каркасы и опоры для столов</a></li>
                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ofis/">Вся мебель для офиса</a></span>
                                                                <ul>
                                                                    <li><a href="/ofis/"><img src="/image/catalog/subcategory/New_Menu/office.jpg" alt="Вся мебель для офиса"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Улица</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <ul>
                                                                    <li><a href="/letnjaja-mebel/stulja-ulichnye/">Стулья уличные</a></li>
                                                                    <li><a href="/letnjaja-mebel/stoly-ulichnye/">Столы уличные</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">

                                                                <ul>    <li><a href="/letnjaja-mebel/barnye-stulja-ulichnye/">Барные стулья уличные</a></li>
                                                                    <li><a href="/letnjaja-mebel/shezlongi/">Шезлонги</a></li>
                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/letnjaja-mebel/">Вся мебель для улицы</a></span>
                                                                <ul>
                                                                    <li><a href="/letnjaja-mebel/"><img src="/image/catalog/subcategory/New_Menu/Outdoor.jpg" alt="Вся мебель для улицы"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="/specials/" class="menu-red">Акция</a></li>
                                <li><a href="/wholesalers/">Опт</a></li>
                                <li class="sub"><span>Информация</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Сервис</span>
                                                                <ul>
                                                                    <li><a href="/shipping-and-payment/">Оплата и доставка</a></li>
                                                                    <li><a href="/return/">Гарантия и возврат</a></li>

                                                                    <li><a href="/help/">Частые вопросы</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Клиентам</span>
                                                                <ul>
                                                                    <li><a href="/wholesalers/">Оптовым клиентам</a></li>
                                                                    <li><a href="/discount/">Система скидок</a></li>
                                                                    <li><a href="/try-on/">Услуга «Примерка»</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Компания</span>
                                                                <ul>
                                                                    <li><a href="/about/">О ФешеМебельном</a></li>
                                                                    <li><a href="/articles/">Блог</a></li>
                                                                    <li><a href="/contacts/">Контакты</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all we-work">
                                                                <div class="timetable">
                                                                    <div class="timetable__left-block">
                                                                        <span><a href="">Мы работаем</a></span>
                                                                        <ul>
                                                                            <li class="timetable__work-days">
                                                                                <span>Пн-Пт</span>9:00 - 20:00
                                                                                <span>Сб-Вс</span>10:00 - 20:00
                                                                            </li>

                                                                        </ul>
                                                                    </div>
                                                                    <div class="timetable__right-block">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="/contacts/">
                                                                                    <img src="img/we-work.jpg" alt="">
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                            <?php } else { ?>
                            <ul class="header-menu__list">
                                <li class="sub"><span>Каталог</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <a href="/ua/stulya/" class="cat-name active">Стільці</a>
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/ua/stolovaja/stulja/">Стільці</a></li>
                                                                    <li><a href="/ua/ofis/ofisnye-stulja/">Офісні стільці</a></li>
                                                                    <li><a href="/ua/stolovaja/barnye-stulja/">Барні стільці</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/priznachennya_kafe-i-bary/">Стільці для кафе</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Популярні</span>
                                                                <ul>
                                                                    <li><a href="/ua/stolovaja/stulja/material-karkasa_derevo/">Стільці дерев'яні</a></li>
                                                                    <li><a href="/ua/stulya/material-karkasa_metall/">Стільці на металлокаркасе</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/material-sidinnya_plastik/">Пластикові стільці</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/osoblivosti_myagkie/">М'які стільці</a></li>
                                                                    <li><a href="/ua/stulya/osoblivosti_s-podlokotnikami/">Стільці з підлокітниками</a></li>
                                                                    <li><a href="/ua/stulya/osoblivosti_raskladnye/">Стільці складні</a></li>
                                                                    <li><a href="/ua/letnjaja-mebel/stulja-ulichnye/">Стільці вуличні</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Стиль</span>
                                                                <ul>
                                                                    <li><a href="/ua/stolovaja/stulja/stil_sovremennyy/">Стільці Модерн</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/stil_klassicheskiy/">Стільці Класика</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/stil_loft/">Стільці Лофт</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/stil_dizaynerskie-repliki/">Стільці Дизайнерські</a></li>
                                                                    <li><a href="/ua/stulya/stil_skandinavskiy/">Стільці Скандинавські</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/stulya/">всі стільці</a></span>
                                                                <ul>
                                                                    <li><a href="/ua/stulya/"><img src="/image/catalog/subcategory/New_Menu/chairs.jpg" alt="всі стільці"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name">
                                                        <a href="/ua/stoli/" class="cat-name">Столи</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/ua/stolovaja/stoly/">Столи кухонні</a></li>
                                                                    <li><a href="/ua/ofis/stoly-pismennye/">Письмові столи</a></li>
                                                                    <li><a href="/ua/gostinye/zhurnalnye-stoly/">Журнальні столики</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Популярні</span>
                                                                <ul>
                                                                    <li><a href="/ua/stolovaja/stoly/osoblivosti_raskladnye/">розкладні столи</a></li>
                                                                    <li><a href="/ua/stolovaja/stoly/material-stil-nitsi_derevo/">Дерев'яні столи</a></li>
                                                                    <li><a href="/ua/stolovaja/stoly/material-stil-nitsi_steklo/">Скляні столи</a></li>
                                                                    <li><a href="/ua/stoli/forma-stola_kruglyy/">Круглі столи</a></li>
                                                                    <li><a href="/ua/letnjaja-mebel/stoly-ulichnye/">Столи вуличні</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Стиль</span>
                                                                <ul>
                                                                    <li><a href="/ua/stoli/stil_sovremennyy/">Столи Модерн</a></li>
                                                                    <li><a href="/ua/stoli/stil_klassicheskiy/">Столи Класичні</a></li>
                                                                    <li><a href="/ua/stoli/stil_dizaynerskie-repliki/">Столи Дизайнерські</a></li>
                                                                    <li><a href="/ua/stoli/stil_loft/">Столи Лофт</a></li>
                                                                    <li><a href="/ua/stoli/stil_skandinavskiy/">Столи Скандинавські</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/stoli/">всі столи</a></span>
                                                                <ul>
                                                                    <li><a href="/ua/stoli/"><img src="/image/catalog/subcategory/New_Menu/Tables.jpg" alt="всі столи"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name">
                                                        <a href="/ua/kresla/" class="cat-name">Крісла</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/ua/gostinye/kresla/">Крісла для будинку</a></li>
                                                                    <li><a href="/ua/ofis/kresla-ofisnie/">Офісні крісла</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/kresla-barnye/">барні крісла</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/kreslo/priznachennya_kafe-i-bary/">Крісла для кафе і ресторанів</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Популярні</span>
                                                                <ul>
                                                                    <li><a href="/ua/mjagkaja-mebel/kreslo/">М'які крісла</a></li>
                                                                    <li><a href="/ua/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-rukovoditeley/">Крісла для керівників</a></li>
                                                                    <li><a href="/ua/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-personala/">Крісла для персоналу</a></li>
                                                                    <li><a href="/ua/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-posetiteley/">Крісла для відвідувачів</a></li>
                                                                    <li><a href="/ua/ofis/kresla-ofisnie/tip-krisla_geymerskie-kresla/">Геймерські крісла</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Матіріал виготовлення</span>
                                                                <ul>
                                                                    <li><a href="/ua/gostinye/kresla/material-sidinnya_tkanina/">Крісла в тканині</a></li>
                                                                    <li><a href="/ua/gostinye/kresla/material-sidinnya_shkirzam/">Крісла у шкірзамі</a></li>
                                                                    <li><a href="/ua/gostinye/kresla/material-karkasa_derevo/">Крісла на дерев'яному каркасі</a></li>
                                                                    <li><a href="/ua/gostinye/kresla/material-karkasa_metal/">Крісла на металевому каркасі</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/kresla/">всі крісла</a></span>
                                                                <ul>
                                                                    <li><a href="/ua/kresla/"><img src="/image/catalog/subcategory/New_Menu/Armchairs.jpg" alt="всі крісла"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
													<li class="header-menu__cat-name">
                                                        <a href="/ua/mjagkaja-mebel/divany/" class="cat-name">Дивани</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Тип</span>
                                                                <ul>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/tip-divana_pryami-divani/">Дивани прямі</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/tip-divana_kutovi-divani/">Дивани кутові</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/banketki/">Диванчики-банкетки</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Призначення</span>
                                                                <ul>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/priznachennya_dom/">Дивани для дому</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/priznachennya_ofis/">Дивани для офісу</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/priznachennya_kafe-i-bari/">Дивани для кафе</a></li>

                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Стиль</span>
                                                                <ul>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/stil_suchasniy/">Дивани Модерн</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/stil_klasichniy/">Дивани Класичні</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/stil_loft/">Дивани Лофт</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/stil_skandinavs-kiy/">Дивани Скандинавські</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/mjagkaja-mebel/divany/">всі дивани</a></span>
                                                                <ul
                                                                    <li><a href="/ua/mjagkaja-mebel/divany/"><img src="/image/catalog/subcategory/New_Menu/sofa.jpg" alt="всі дивани"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name mod">
                                                        <a href="/ua/stolovaja/opory-dlja-stolov/" class="cat-name mod">Опори для столів</a>
                                                    </li>
                                                    <li class="header-menu__cat-name mod">
                                                        <a href="/ua/stoli/stoleshnitsy-dlja-stolov/" class="cat-name mod">Стільниці</a>

                                                    </li>
                                                    
                                                    <li class="header-menu__cat-name">
                                                        <a href="/ua/gostinye/" class="cat-name">Меблі для вітальні</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <ul>
                                                                    <li><a href="/ua/gostinye/kresla/">Крісла</a></li>
                                                                    <li><a href="/ua/gostinye/zhurnalnye-stoly/">Журнальні столи</a></li>
                                                                    <li><a href="/ua/gostinye/knizhnye-shkafy/">Полиці і Етажерки</a></li>
                                                                    <li><a href="/ua/gostinye/vitriny/">Вітрини</a></li>
                                                                    <li><a href="/ua/gostinye/komody_i_tumby_v_gostinuu/">Тумби та Комоди</a></li>
                                                                    <li><a href="/ua/gostinye/tumby-pod-tv/">Тумби під ТВ</a></li>

                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                    <li class="header-menu__cat-name">
                                                        <a href="/ua/spalnya/" class="cat-name">Меблі для спальні</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">

                                                                <ul>
                                                                    <li><a href="/ua/spalnya/krovati/">Ліжка</a></li>
                                                                    <li><a href="/ua/matrasy">Матраци</a></li>
                                                                    <li><a href="/ua/spalnya/tumby/">Тумби</a></li>
                                                                    <li><a href="/ua/spalnya/komody/">Комоди</a></li>
                                                                    <li><a href="/ua/spalnya/garderoby/">Гардероби</a></li>
                                                                    <li><a href="/ua/spalnya/zerkala-v-spalnu/">Дзеркала</a></li>
                                                                    <li><a href="/ua/spalnya/tualetnye-stoliki/">Туалетні столики</a></li>
                                                                    <li><a href="/ua/spalnya/puffs/">Пуфи для спальні</a></li>

                                                                </ul>
                                                            </div>


                                                        </ul>
                                                    </li>

                                                    <li class="header-menu__cat-name">
                                                        <a href="/ua/mjagkaja-mebel/" class="cat-name">М'які меблі</a>
                                                        <ul class="header-menu__submenu sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <ul>
                                                                    <li><a href="/ua/mjagkaja-mebel/kreslo/">Крісла</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/kresla-barnye/">Крісла барні</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/banketki/">Банкетки</a></li>
                                                                    <li><a href="/ua/mjagkaja-mebel/pufiki/">Пуфи</a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>

                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Кухня</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">

                                                                <ul>
                                                                    <li><a href="/ua/stolovaja/stoly/">Столи кухонні</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/">Стільці</a></li>
                                                                    <li><a href="/ua/stolovaja/barnye-stulja/">Барні стільці</a></li>
                                                                    <li><a href="/ua/stolovaja/taburety-i-skami/">Табурети і лави</a></li>
                                                                </ul>
                                                            </div>
                                                             <div class="header-menu__feature">

                                                                 <ul>

                                                                     <li><a href="/ua/stolovaja/opory-dlja-stolov/">Опори для столів</a></li>
                                                                    <li><a href="/ua/stoli/stoleshnitsy-dlja-stolov/">Стільниці для столів</a></li>
                                                                    <li><a href="/ua/stolovaja/stoly-transformery/">Столи-трансформери</a></li>
                                                                    <li><a href="/ua/stolovaja/obedennye-komplekty/">Обідні комплекти</a></li>

                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/stolovaja/">Всі меблі для кухні</a></span>
                                                                <ul>
                                                                    <li><a href="/ua/stolovaja/"><img src="/image/catalog/subcategory/New_Menu/Kitchen.jpg" alt="Всі меблі для кухні"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Кафе</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">

                                                                <ul>

                                                                    <li><a href="/ua/mebel-dlja-kafe/stoly-dlja-kafe">Столи для кафе</a></li>
                                                                    <li><a href="/ua/stolovaja/opory-dlja-stolov/">Опори для столів</a></li>
                                                                    <li><a href="/ua/stoli/stoleshnitsy-dlja-stolov/">Стільниці для столів</a></li>
                                                                    <li><a href="/ua/stolovaja/stulja/priznachennya_kafe-i-bary/#kafe">Стільці для кафе</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">

                                                                <ul>      <li><a href="/ua/mjagkaja-mebel/kreslo/priznachennya_kafe-i-bary/#kafe">Крісла для кафе</a></li>
                                                                    <li><a href="/ua/stolovaja/barnye-stulja/#kafe">Барні стільці</a></li>
                                                                    <li><a href="/ua/stolovaja/barnye-stulja/osoblivosti_bez-spinki/">Барні табурети</a></li>
                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/mebel-dlja-kafe/">Всі меблі для кафе</a></span>
                                                                <ul>
                                                                    <li><a href="/ua/mebel-dlja-kafe/"><img src="/image/catalog/subcategory/New_Menu/Kafe.jpg" alt="Всі меблі для кафе"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Офіс</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">

                                                                <ul>
                                                                    <li><a href="/ua/ofis/kresla-ofisnie/">Крісла офісні</a></li>
                                                                    <li><a href="/ua/ofis/ofisnye-stulja/">Стільці офісні</a></li>
                                                                    <li><a href="/ua/ofis/stoly-pismennye/">Письмові, офісні столи</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">

                                                                <ul>     <li><a href="/ua/ofis/stellazh/">Стелажі</a></li>
                                                                    <li><a href="/ua/ofis/karkasy-i-opory-dlja-stolov/">Каркаси і опори для столів</a></li>
                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/ofis/">Всі меблі для офісу</a></span>
                                                                <ul>
                                                                    <li><a href="/ua/ofis/"><img src="/image/catalog/subcategory/New_Menu/office.jpg" alt="Всі меблі для офісу"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li class="sub"><span>Вулиця</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <ul>
                                                                    <li><a href="/ua/letnjaja-mebel/stulja-ulichnye/">Стільці вуличні</a></li>
                                                                    <li><a href="/ua/letnjaja-mebel/stoly-ulichnye/">Столи вуличні</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">

                                                                <ul>     <li><a href="/ua/letnjaja-mebel/barnye-stulja-ulichnye/">Барні стільці вуличні</a></li>
                                                                    <li><a href="/ua/letnjaja-mebel/shezlongi/">Шезлонги</a></li>
                                                                </ul>
                                                            </div>

                                                            <div class="header-menu__feature all">
                                                                <span><a href="/ua/letnjaja-mebel/">Всі меблі для вулиці</a></span>
                                                                <ul>
                                                                    <li><a href="/ua/letnjaja-mebel/"><img src="/image/catalog/subcategory/New_Menu/Outdoor.jpg" alt="Всі меблі для вулиці"></a></li>
                                                                </ul>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                                <li><a href="/ua/specials/" class="menu-red">Акція</a></li>
                                <li><a href="/ua/wholesalers/">Опт</a></li>
                                <li class="sub"><span>Інформація</span>
                                    <div class="header-menu__dropdown-block">
                                        <ul class="header-menu__dropdown">
                                            <div class="wrapper">
                                                <div class="header-menu__cat-block header-menu__cat-none sameheight-menu">
                                                    <li class="header-menu__cat-name">
                                                        <ul class="header-menu__submenu active sameheight-menu">
                                                            <div class="header-menu__feature">
                                                                <span>Сервіс</span>
                                                                <ul>
                                                                    <li><a href="/ua/shipping-and-payment/">Оплата і доставка</a></li>
                                                                    <li><a href="/ua/return/">Гарантія та повернення</a></li>

                                                                    <li><a href="/ua/help/">Часті питання</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Клієнтам</span>
                                                                <ul>
                                                                    <li><a href="/ua/wholesalers/">Оптовим клієнтам</a></li>
                                                                    <li><a href="/ua/discount/">Система знижок</a></li>
                                                                    <li><a href="/ua/try-on/">Послуга копіювання</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature">
                                                                <span>Компанія</span>
                                                                <ul>
                                                                    <li><a href="/ua/about/">Про Фешемебельний</a></li>
                                                                    <li><a href="/ua/articles/">Блог</a></li>
                                                                    <li><a href="/ua/contacts/">Контакти</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="header-menu__feature all we-work">
                                                                <div class="timetable">
                                                                    <div class="timetable__left-block">
                                                                        <span><a href="">Ми працюємо</a></span>
                                                                        <ul>
                                                                            <li class="timetable__work-days">
                                                                                <span>Пн-Пт</span> 9:00 - 20:00
                                                                                <span>Сб-Нд</span> 10:00 - 20:00
                                                                            </li>

                                                                        </ul>
                                                                    </div>
                                                                    <div class="timetable__right-block">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="/ua/contacts/">
                                                                                    <img src="img/we-work.jpg" alt="">
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ul>
                                                    </li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="header-bottom__right">
                        <?php echo $search; ?>

                    </div>
                </div>
            </div>
        </div>
    </header>
<script>


    if ($(window).width() > 767) {
        function sameHeight(columnHeight) {
            var blockHeight = 0;

            columnHeight.each(function () {
                thisHight = $(this).height();
                if (thisHight > blockHeight) {
                    blockHeight = thisHight;
                }
            });

            columnHeight.height(blockHeight);
        }
        sameHeight($('.sameheight-menu'));
    }


    // menuItems

    if ($(window).width() > 767) {
        $('.header-menu__dropdown-block .cat-name').hover(function () {
            $('.header-menu__dropdown-block .cat-name').removeClass('active');
            $(this).addClass('active');
            $('.header-menu__dropdown-block .header-menu__submenu').removeClass('active');
            $(this).siblings('.header-menu__submenu').addClass('active');
            $('.header-menu__dropdown-block .header-menu__cat-none .header-menu__submenu').addClass('active');
        });
    }


    // menu

    if ($(window).width() <= 767) {
        $('.header-menu__cat-name .cat-name').removeClass('active');

        $('.header i.fa-bars').click(function () {
            $('.header-menu__list').toggle();
        })

        $('.header-menu__list .sub>span').click(function () {
            var li =  $(this).closest('li');
            $(li).find('.header-menu__dropdown-block').toggle();

            $(this).toggleClass('active');
        })

        $('.header-menu__cat-name .cat-name').click(function () {

            if(!$(this).hasClass('mod'))
            {
                $(this).removeAttr('href');
                $(this).next().toggle();
                $(this).toggleClass('active');
            }

        })

        $('.header-menu__submenu .header-menu__feature span').click(function () {
            $(this).next().toggle();
            $(this).toggleClass('active');
        })
    }



    // phone move

    if ($(window).width() <= 767) {
        $('.header-bottom__logo').after($('.header-top__phone'));
        $('.header').addClass('header-fixed');
        $('.header-menu__submenu').removeClass('sameheight-menu')
    } else {
        $('.header-top__right-block').prepend($('.header-top__phone'));
        $('.header').removeClass('header-fixed');
    }

    $(window).resize(function () {
        if ($(window).width() <= 767) {
            $('.header-bottom__logo').after($('.header-top__phone'));
            $('.header').addClass('header-fixed');
        } else {
            $('.header-top__right-block').prepend($('.header-top__phone'))
            $('.header').removeClass('header-fixed');
        }
    })


    // user-form
    /*
    if ($(window).width() <= 767) {

        $('.header-top__user-img').click(function () {
            $('body').css({'overflow':'hidden'});
            $('.header-top__user-block').css({'display':'flex', 'overflow-y':'auto'});
        })

        $('.user-dropdown__close').click(function () {
            $('.header-top__user-block').hide();
            $('body').css('overflow-y','auto');
        })
        $('.header-top__user-bg').click(function () {
            $('.header-top__user-block').hide();
            $('body').css('overflow-y','auto');
        })

    } else {

        $('.header-top__user-img').hover(function () {
            $('.header-top__user-block').css('display', 'flex');
            $('.basket-wish__block').hide();
            $('.basket-compare__block').hide();
            $('.basket-buy__block').hide();
        })

        $('.user-dropdown__close').click(function () {
            $('.header-top__user-block').hide();
        })

    }*/


    // basket

    if ($(window).width() <= 767) {

        $('.basket-wish .img').click(function() {
            $('body').css({'overflow':'hidden'});
            $('.basket-wish__block').css({'display':'flex', 'overflow-y':'auto'});
        })
        $('.basket-wish__close').click(function () {
            $('.basket-wish__block').hide();
            $('body').css('overflow-y','auto');
        })

        $('.basket-compare .img').click(function() {
            $('body').css({'overflow':'hidden'});
            $('.basket-compare__block').css({'display':'flex', 'overflow-y':'auto'});
        })
        $('.basket-compare__close').click(function () {
            $('.basket-compare__block').hide();
            $('body').css('overflow-y','auto');
        })

        $('.basket-buy .img').click(function() {
            $('body').css({'overflow':'hidden'});
            $('.basket-buy__block').css({'display':'flex', 'overflow-y':'auto'});
        })



        $('.basket-buy__bg').click(function () {
            $('.basket-wish__block').hide();
            $('.basket-compare__block').hide();
            $('.basket-buy__block').hide();
            $('body').css('overflow-y','auto');
        })

    } else {
        /*
        $('.basket-wish .img').hover(function () {
            $('.basket-wish__block').css('display', 'flex');
            $('.basket-compare__block').hide();
            $('.basket-buy__block').hide();
            $('.header-top__user-block').hide();
        })

        $('.basket-wish__close').click(function () {
            $('.basket-wish__block').hide();
        })

        $('.basket-compare .img').hover(function () {
            $('.basket-compare__block').css('display', 'flex');
            $('.basket-wish__block').hide();
            $('.basket-buy__block').hide();
            $('.header-top__user-block').hide();
        })

        $('.basket-compare__close').click(function () {
            $('.basket-compare__block').hide();
        })

        $('.basket-buy .img').hover(function () {
            $('.basket-buy__block').css('display', 'flex');
            $('.basket-wish__block').hide();
            $('.basket-compare__block').hide();
            $('.header-top__user-block').hide();
        })

        $('.basket-buy__close').click(function () {

            $('.basket-buy__block').hide();
        })
*/
    }


    // search input

    if ($(window).width() >= 992) {

        $('.header-search input').focus(function () {
         //   $(this).css({'padding-left':'40px', 'border':'2px solid #121726'});
        //    $('.header-search__block').show();
           // $('.header-search__search-btn').addClass('active');
     //       $('.header-search__close-btn').show();
        })

        $('.header-search__close-btn').click(function() {
            $(this).hide();
       //     $('.header-search input').css({'padding-left':'12px', 'border':'1px solid #121726'});
      //      $('.header-search__block').hide();
         //   $('.header-search__search-btn').removeClass('active');
        })

    } else {

        $('.header-search__search-btn').click(function() {
            $('.header-search input').show();
        //    $('.header-search input').css({'padding-left':'40px', 'border':'2px solid #121726'});
            $('.header-search__block').show();
       //     $('.header-search__search-btn').addClass('active');
            $('.header-search__close-btn').show();
        })

        $('.header-search__close-btn').click(function() {
            $(this).hide();
            $('.header-search input').hide();
          //  $('.header-search input').css({'padding-left':'12px', 'border':'1px solid #121726'});
            $('.header-search__block').hide();
       //     $('.header-search__search-btn').removeClass('active');
        })

    }


    // footer

    $(window).resize(function () {
        if ($(window).width() <= 991) {
            $('.footer .wrapper').prepend($('.footer__logo'));
            $('.footer__logo').after($('.footer__right-block'));
            $('.footer__bottom-inf').prepend($('.footer__phone-email'));
            $('.footer__phone-email').after($('.footer__social-blocks'));
            $('.footer__text').appendTo($('.footer__left-block'));
        }

        if ($(window).width() <= 767) {
            $('.footer__logo').after($('.footer__left-block'));
            $('.footer__bottom-inf').appendTo($('.footer__left-block'));
            $('.footer__text').appendTo($('.footer__right-block'));
        }
    })

    if ($(window).width() <= 991) {
        $('.footer .wrapper').prepend($('.footer__logo'));
        $('.footer__logo').after($('.footer__right-block'));
        $('.footer__bottom-inf').prepend($('.footer__phone-email'));
        $('.footer__phone-email').after($('.footer__social-blocks'));
        $('.footer__text').appendTo($('.footer__left-block'));
    }

    if ($(window).width() <= 767) {
        $('.footer__logo').after($('.footer__left-block'));
        $('.footer__bottom-inf').appendTo($('.footer__left-block'));
        $('.footer__text').appendTo($('.footer__right-block'));

        $('.footer__head').click(function () {
            $(this).toggleClass('active');
            $(this).next().toggle();
        })
    }
    $(document).delegate('.basket-buy__product-plus', 'click', function() {
        div = $(this).closest('.basket-buy__product-count');
        input = $(div).find('.basket-buy__product-number');
        cart_id = $(div).find('.order__calc-cart_id').val();
        val_input = $(input).val()/1+1;
        $(input).val(val_input);
        edit_cart(cart_id,val_input);

    })
    $(document).delegate('.basket-buy__product-minus', 'click', function() {
        div = $(this).closest('.basket-buy__product-count');
        input = $(div).find('.basket-buy__product-number');
        cart_id = $(div).find('.order__calc-cart_id').val();
        val_input = $(input).val()/1-1;
        if(val_input<1)
            val_input = 1;

        $(input).val(val_input);
        edit_cart(cart_id,val_input);
    })
    function edit_cart(cart_id,val_input)
    {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit_cart',
            type: 'post',
            data: {
                'cart_id': cart_id,
                'quantity': val_input
            },
            beforeSend: function() {
            },
            complete: function() {
            },
            success: function(data) {
                update_checkout();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
</script>



