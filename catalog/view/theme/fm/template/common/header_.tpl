<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-28915158-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-28915158-1');
</script>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <base href="<?php echo $base; ?>" />
  <?php if ($description) { ?>
  <meta name="description" content="<?php echo $description; ?>" />
  <?php } ?>
  <?php if ($keywords) { ?>
  <meta name="keywords" content= "<?php echo $keywords; ?>" />
  <?php } ?>

  <link href="catalog/view/theme/fm/stylesheet/jquery.bxslider.css?v=1.0" rel="stylesheet" />
  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js?v=1.0" type="text/javascript"></script>

  <script src="catalog/view/javascript/jquery.bxslider.min.js?v=1.0"></script>

  <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css?v=1.0" rel="stylesheet" media="screen" />
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js?v=1.0" type="text/javascript"></script>
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css?v=1.0" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="catalog/view/theme/fm/stylesheet/fonts.css?v=1.0">

<?php echo $alternate; ?>

  <script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js?v=1.0"></script>
  <link href="catalog/view/theme/fm/stylesheet/stylesheet.css?v=6.0" rel="stylesheet">
  <link href="catalog/view/theme/fm/stylesheet/unicorns.css?v=1.0" rel="stylesheet">
  <link href="catalog/view/theme/fm/stylesheet/media.css?v=3.0" rel="stylesheet">
  <?php foreach ($styles as $style) { ?>
  <link href="<?php echo $style['href']; ?>?v=1.0" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>
  <script src="catalog/view/javascript/common.js?v=1.0" type="text/javascript"></script>
  <?php foreach ($links as $link) { ?>
  <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>
  <?php foreach ($scripts as $script) { ?>
  <script src="<?php echo $script; ?>?v=1.0" type="text/javascript"></script>
  <?php } ?>
  <?php foreach ($analytics as $analytic) { ?>
  <?php echo $analytic; ?>
  <?php } ?>

  <script type="text/javascript" src="catalog/view/javascript/callback.js?v=1.0"></script>
  <script type="text/javascript" src="catalog/view/javascript/jquery.simplemodal.js?v=1.0"></script>
  <link href="catalog/view/theme/fm/stylesheet/callback.css?v=1.0" rel="stylesheet" type="text/css" />

<?php  ?>

  <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png?v=1.0">
  <link rel="shortcut icon" type="image/png" href="/favicon/favicon-32x32.png?v=1.0">
  <link rel="manifest" href="/favicon/manifest.json?v=1.0">
  <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg?v=1.0" color="#2d4059">
  <meta name="msapplication-config" content="/favicon/browserconfig.xml?v=1.0">
  <meta name="theme-color" content="#ffd460">
</head>
<body class="<?php echo $class; ?>">
<?php if($banner_home['status']){ ?>
<div class="header-banner">

<div class="banner-img container w320">
	<img src="/image/<?=$banner_home['image1']?>" width="" height="auto" />
</div>
<div class="banner-img container w768">
	<img src="/image/<?=$banner_home['image2']?>" width="" height="auto" />
</div>
<div class="banner-img container w1024">
	<img src="/image/<?=$banner_home['image3']?>" width="" height="auto" />
</div>

</div>
<?php } ?>
<nav class="top-nav">
  <div class="container">
    <div class="row">
      <div class="top-nav__left col-xs-2 col-sm-8 col-md-9">
        <button type="button" class="top-nav__menu-btn btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".top-nav__list">
          <i class="fa fa-bars"></i>
        </button>
        <ul class="collapse navbar-collapse top-nav__list">
          <li><a href="<?php echo $url_about; ?>"><?php echo $top_menu_about; ?></a></li>
          <li><a href="<?php echo $url_delivery; ?>"><?php echo $top_menu_delivery; ?></a></li>
          <li><a href="<?php echo $url_guaranty; ?>"><?php echo $top_menu_guaranty; ?></a></li>
          <li><a href="<?php echo $url_exchange; ?>"><?php echo $top_menu_exchange; ?></a></li>
          <li><a href="<?php echo $url_discount; ?>"><?php echo $top_menu_discount; ?></a></li>
        </ul>
      </div>
      <div class="top-nav__right col-xs-10 col-sm-4 col-md-3">
        <div class="top-nav__profile dropdown">
          <a href="/login" title="<?php echo $text_account; ?>" class="top-nav__profile-link dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <span class="top-nav__profile-link-text"><?php echo $text_account; ?></span>
            <!-- <span class="caret"></span> -->
          </a>
          <ul class="top-nav__dropdown-menu dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php echo $language; ?>
      </div>
    </div>
  </div>
</nav>
<header class="header">
  <div class="header__container container">
    <div class="header__logo" itemscope itemtype="http://schema.org/Organization">
      <a href="/" class="header__logo-link" itemprop="url">
        <div class="logo">
          <i class="logo__icon logo__icon--1"></i>
          <i class="logo__icon logo__icon--2"></i>
          <i class="logo__icon logo__icon--3"></i>
          <i class="logo__icon logo__icon--4"></i>
          <i class="logo__icon logo__icon--5"></i>
          <img itemprop="logo" class="logo__img" src="/image/catalog/logo.svg?v=1.0" alt="">
        </div>
      </a>
    </div>

    <div class="header__schedule">
      <p><?php echo $text_work; ?></p>
      <p>Тел: <a href="tel:+380685108199 ">+38-068-510-81-99</a></p>
    </div>

    <div class="header__cart">
  		<?php echo $cart; ?>
      <div class="header__wish-compare">
        <a class="bookmark" href="/login/" id="wishlist-total" title="<?php echo $text_wishlist; ?>">
          <?php echo $text_wishlist; ?>
        </a>
        <a class="compare" href="/compare/" id="compare-total" title="<?php echo $text_compare; ?>" class="header-product-btn-i">
          <?php echo $text_compare; ?>
        </a>
      </div>
		</div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="category-menu">
  <div class="container">
    <nav id="menu" class="navbar">
      <div class="navbar-header category-button" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <i class="fa fa-th" aria-hidden="true"></i>
        <span id="category"><?php echo $text_category; ?></span>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <?php foreach ($categories as $category) { ?>
          <?php if ($category['children']) { ?>
          <li class="dropdown">
            <a href="<?php echo $category['href']; ?>" class="dropdown-toggle"><?php echo $category['name']; ?></a>
            <div class="dropdown-menu">
              <div class="dropdown-inner">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($children as $child) { ?>
                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </div>
              <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?></a> </div>
          </li>
          <?php } else { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
      </div>
    </nav>
    <div class="nav__search">
      <?php echo $search; ?>
      <div style="display: none;" class="search-result-ajax" id="search-result-ajax"></div>
    </div>
  </div>
</div>
<?php } ?>
