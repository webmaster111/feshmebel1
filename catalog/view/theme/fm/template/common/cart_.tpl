<div id="cart" class="btn-group btn-block">
  

  <!-- <a href="" id="checkout-button"> -->
    <div class="checkout-wrap">
      <i class="fa fa-shopping-cart" aria-hidden="true"></i>
    <span class="cart-span" id="cart-span"><?php echo $text_items; ?></span> | <span><a class="checkout-link" href="<?php echo $checkout; ?>">ОФОРМИТЬ ЗАКАЗ</a></span>
    </div>
    
  <!-- </a>
 -->
  <button id="open-cart-click" type="button" data-toggle="dropdown" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-inverse btn-block btn-lg dropdown-toggle"><i class="fa fa-shopping-cart shopping-cart-dropdown"></i> <span id="cart-total"><?php echo $text_items; ?></span>
  </button>


  <ul class="dropdown-menu pull-right">
   <li><p class="text-center dropdown-cart-title">В корзине <?php echo $text_items; ?></p></li>
    <span class="close-ddm"><svg width="20px" height="20px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill="#303030" d="M10.707 10.5l5.646-5.646c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.646 5.646-5.646-5.646c-0.195-0.195-0.512-0.195-0.707 0s-0.195 0.512 0 0.707l5.646 5.646-5.646 5.646c-0.195 0.195-0.195 0.512 0 0.707 0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146l5.646-5.646 5.646 5.646c0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146c0.195-0.195 0.195-0.512 0-0.707l-5.646-5.646z"></path></svg></span>
    
    <?php if ($products || $vouchers) { ?>

    <li>
      <div>
        <?php foreach ($products as $product) { ?>
        <div class="dropdown-cart-item">
          <div class="dropdown-cart-img"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
            <?php } ?>
          </div>
          <div class="text-left dropdown-cart-pn"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if ($product['option']) { ?>
            <?php foreach ($product['option'] as $option) { ?>
            <br />
            - <?php echo $option['name']; ?> <?php echo $option['value']; ?>
            <?php } ?>
            <?php } ?>
            <?php if ($product['recurring']) { ?>
            <br />
            - <?php echo $text_recurring; ?> <?php echo $product['recurring']; ?>
            <?php } ?></div>
          <div class="text-right"></div>
          <div class="dropdown-cart-price"><span class="span"><?php echo $product['price']; ?></span>
              <span class="dropdown-cart-amount"><?php echo $product['quantity']; ?> шт  </span>
          <div class="text-center"><button class="dropdown-button-cart" type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><svg width="20px" height="20px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill="#fe9003" d="M10.707 10.5l5.646-5.646c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.646 5.646-5.646-5.646c-0.195-0.195-0.512-0.195-0.707 0s-0.195 0.512 0 0.707l5.646 5.646-5.646 5.646c-0.195 0.195-0.195 0.512 0 0.707 0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146l5.646-5.646 5.646 5.646c0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146c0.195-0.195 0.195-0.512 0-0.707l-5.646-5.646z"></path></svg></button></div>
        </div>
        <?php } ?>
      </div>
    </li>
    
    <li>
      <div>
        <table class="table">          
          <tr>
            <td class="amount-table-title"><?php echo $totals[1]['title']; ?>:</td>
            <td class="text-right amount-table-price"><?php echo $totals[1]['text']; ?></td>
          </tr>          
        </table>
        <p class="text-right clearfix">
<!-- onclick=" return false;"       '//'.$_SERVER['SERVER_NAME'].'/'.$_SERVER['REQUEST_URI'];  -->
        <a href="#" onclick=" document.getElementById('cart-span').innerHTML = document.getElementById('text_items').value; $('.dropdown-menu pull-right').css('display', 'none');return false;" class="dropdown-cart-link fl continue-purchases-button">Продолжить покупки<!-- <?php echo $text_cart; ?> --><!-- <?php echo $cart; ?> --></a>
        <a href="<?php echo $checkout; ?>" class="dropdown-cart-link fr checkout-button"><?php echo $text_checkout; ?></a>
		<input type='hidden' name='text_items' id='text_items' value='<?php echo $text_items; ?>'>
        </p>
		
        <p class="text-right clearfix">
<!--        <a href="index.php?route=checkout/cart/clear" class="dropdown-cart-link fl continue-purchases-button">Очистить корзину</a> -->
        <a href="#" onclick="return cartClear(this);" class="dropdown-cart-link fl continue-purchases-button">Очистить корзину</a>
        </p>

      </div>
    </li>
    <?php } else { ?>
    <li>
      <p class="text-center"><?php echo $text_empty; ?></p>
    </li>
    <?php } ?>
  </ul> 
</div>
