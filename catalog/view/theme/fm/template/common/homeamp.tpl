<!DOCTYPE html>
<html amp>
<head>
	<meta charset="utf-8"/>
	<title><?php echo $title_meta; ?></title>
	<?php if ($description_meta) { ?>
	<meta name="description" content="<?php echo $description_meta; ?>" />
	<?php } ?>
	<?php if($noindex){ ?>
	<meta name="robots" content="noindex, nofollow" />
	<?php } ?>
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1"/>
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png?v=1.0">
	<link rel="shortcut icon" type="image/png" href="/favicon/favicon-32x32.png?v=1.0">
	<link rel="manifest" href="/favicon/manifest.json?v=1.0">
	<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg?v=1.0" color="#2d4059">
	<meta name="msapplication-config" content="/favicon/browserconfig.xml?v=1.0">

	<?php echo $alternate; ?>

	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<style amp-boilerplate >
		body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both} @-webkit-keyframes -amp-start

		{from{visibility:hidden}to{visibility:visible}} @-moz-keyframes -amp-start

		{from{visibility:hidden}to{visibility:visible}} @-ms-keyframes -amp-start

		{from{visibility:hidden}to{visibility:visible}} @-o-keyframes -amp-start

		{from{visibility:hidden}to{visibility:visible}} @keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
	</style>
	<noscript>
		<style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
	</noscript>
	<style amp-custom>
		html {
			position: relative;
			min-height: 100%;
		}

		body {
			position: relative;
			margin: 0px;
			padding: 0px;
			min-height: 100%;
			font-size: 14px;
			font-family: 'Ubuntu', sans-serif;
			color: #292929;
			background: #FFFFFF;
		}

		td {
			font-size: 13px;
		}

		p, form {
			margin: 0px;
			padding: 0px;
		}

		h1 {
			padding: 0px;
			margin: 0px;
			font-weight: normal;
			font-size: 30px;
			text-transform: uppercase;
			color: #292929;
		}

		h2 {
			padding: 0px;
			margin: 0px;
			font-weight: normal;
			font-size: 24px;
			text-transform: uppercase;
			color: #292929;
		}

		h3 {
			padding: 0px;
			margin: 0px;
			font-weight: normal;
			font-size: 21px;
			text-transform: uppercase;
			color: #292929;
		}

		h4 {
			padding: 0px;
			margin: 0px;
			font-weight: normal;
			font-size: 18px;
			text-transform: uppercase;
			color: #292929;
		}

		h5 {
			padding: 0px;
			margin: 0px;
			font-weight: normal;
			font-size: 14px;
		}

		select, option, input, textarea {
			font-size: 14px;
			font-family: 'Ubuntu', sans-serif;
			vertical-align: middle;
			position: relative;
			color: #000000;
		}

		input:focus, textarea:focus {
			outline: none;
		}

		input[type=text], input[type=email], input[type=password], input[type=tel], input[type=number], textarea {
			padding: 0px;
			border: 0px;
		}

		a {
			color: #4097B4;
			text-decoration: underline;
		}

		a:active {
			color: #3A82B0;
			text-decoration: none;
		}

		a:hover {
			color: #3A82B0;
			text-decoration: none;
		}

		img {
			border: 0px;
			vertical-align: middle;
			max-width: 100%;
		}

		ul {
			margin: 10px 0px;
			padding: 0px 0px 0px 22px;
		}

		ul li {
			margin: 0px 0px 3px 0px;
			padding: 0px;
		}

		label {
			cursor: pointer;
		}

		iframe {
			border: 0px;
			max-width: 100%;
		}

		#page {
			position: relative;
			min-height: 100%;
		}

		#headerspace {
			height: 80px;
		}

		#footerspace {
			height: 120px;
		}

		.pagewrap {
			position: relative;
			margin: 0px auto;
			max-width: 768px;
		}

		.clear {
			clear: both;
			font-size: 0px;
			height: 0px;
		}

		.al-left {
			text-align: left;
		}

		.al-center {
			text-align: center;
		}

		.al-right {
			text-align: right;
		}

		.al-justify {
			text-align: justify;
		}

		.img-left {
			float: left;
			margin: 0px 10px 10px 0px;
		}

		.img-left img {
			max-width: 100%;
		}

		.img-right {
			float: right;
			margin: 0px 0px 10px 10px;
			max-width: 100%;
		}

		.img-right img {
			max-width: 100%;
		}

		.butt1, .butt4 {
			display: inline-block;
			height: 36px;
			padding: 0px 30px;
			font-size: 14px;
			line-height: 36px;
			background: transparent;
			text-align: center;
			color: #2D4059;
			border: 1px solid #2D4059;
			text-decoration: none;
			white-space: nowrap;
			vertical-align: middle;
			cursor: pointer;
			user-select: none;
			transition: 0.2s;
			outline: none;
			font-weight: bold;
		}

		.butt1:hover, .butt4:hover {
			color: #fff;
			background: #2D4059;
		}

		.butt1:active, .butt4:active {
			color: #fff;
			background: #2D4059;
		}

		.butt2 {
			display: inline-block;
			height: 36px;
			padding: 0px 30px;
			font-size: 18px;
			line-height: 36px;
			background: #2D4059;
			text-align: center;
			color: #FFFFFF;
			text-shadow: 0px 0px 0px #FFFFFF;
			border: 0px;
			text-decoration: none;
			white-space: nowrap;
			vertical-align: middle;
			cursor: pointer;
			user-select: none;
			transition: 0.2s;
			outline: none;
		}

		.butt2:hover {
			color: #FFFFFF;
			background: #FFD015;
			background: linear-gradient(to bottom, #2D4059 0%, #088FAF 100%);
		}

		.butt2:active {
			color: #FFFFFF;
			background: #2D4059;
		}

		.butt3 {
			display: inline-block;
			height: 36px;
			padding: 0px 30px;
			font-size: 14px;
			line-height: 36px;
			background: #2D4059;
			text-align: center;
			color: #FFFFFF;
			text-shadow: 0px 0px 0px #FFFFFF;
			border: 0px;
			text-decoration: none;
			white-space: nowrap;
			vertical-align: middle;
			cursor: pointer;
			user-select: none;
			transition: 0.2s;
			outline: none;
		}

		.butt3:hover {
			color: #FFFFFF;
			background: #FFD015;
			background: linear-gradient(to bottom, #2D4059 0%, #088FAF 100%);
		}

		.butt3:active {
			color: #FFFFFF;
			background: #2D4059;
		}

		.anchor {
			color: #4097B4;
			text-decoration: underline;
			cursor: pointer;
		}

		.anchor:active {
			color: #3A82B0;
		}

		.anchor:hover {
			color: #3A82B0;
		}

		.table {
			margin: 0px;
			display: table;
		}

		.table .thead {
			display: table-header-group;
		}

		.table .row {
			display: table-row;
		}

		.table .cell {
			display: table-cell;
		}

		#header {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 70px;
			background: #F8F8F8;
			z-index: 11;
		}

		#header .client {
			position: absolute;
			right: 50px;
			top: 8px;
			height: 26px;
			line-height: 26px;
			font-size: 13px;
			cursor: pointer;
		}

		#header .client svg {
			margin: -2px 3px 0px 0px;
			display: inline-block;
			width: 26px;
			height: 26px;
			vertical-align: middle;
		}

		#header .client svg path {
			fill: #9D9D9D;
			transition: 0.2s;
		}

		#header .client span:before {
			content: 'Личный кабинет';
			color: #9D9D9D;
			transition: 0.2s;
		}

		#header .client span:after {
			content: '';
			position: relative;
			margin: -1px 0px 0px 6px;
			display: inline-block;
			width: 0;
			height: 0;
			border-left: 4px solid transparent;
			border-right: 4px solid transparent;
			border-top: 4px solid #9D9D9D;
			vertical-align: middle;
			transition: 0.2s;
		}

		#header .client:hover svg path {
			fill: #FDD01B;
		}

		#header .client:hover span:before {
			color: #FDD01B;
		}

		#header .client:hover span:after {
			border-top-color: #FDD01B;
		}

		#header .client.opened span:after {
			transform: rotate(-180deg);
		}

		#header .cmp {
			position: absolute;
			right: 150px;
			top: 20px;
			display: block;
			width: 20px;
			height: 20px;
			cursor: pointer;
		}

		#header .cmp svg {
			width: 20px;
			height: 20px;
		}

		#header .cmp svg path {
			fill: #292929;
			transition: 0.2s;
		}

		#header .cmp:hover svg path {
			fill: #FDD01B;
		}

		#header .wl {
			position: absolute;
			right: 190px;
			top: 20px;
			display: block;
			width: 20px;
			height: 20px;
			cursor: pointer;
		}

		#header .wl svg {
			width: 20px;
			height: 20px;
		}

		#header .wl svg path {
			fill: #292929;
			transition: 0.2s;
		}

		#header .wl:hover svg path {
			fill: #FDD01B;
		}

		#header .butt-search {
			position: absolute;
			right: 310px;
			top: 20px;
			width: 20px;
			height: 20px;
			cursor: pointer;
			transition: 0.2s;
		}

		#header .butt-search svg {
			position: absolute;
			left: 0px;
			top: 0px;
			width: 20px;
			height: 20px;
		}

		#header .butt-search svg ellipse {
			stroke: #292929;
			fill: #F8F8F8;
			transition: 0.2s;
		}

		#header .butt-search:hover svg ellipse {
			stroke: #FDD01B;
		}

		#header .butt-search svg line {
			stroke: #292929;
			transition: 0.2s;
		}

		#header .butt-search:hover svg line {
			stroke: #FDD01B;
		}

		#header .logo {
			position: absolute;
			left: 0px;
			top: 0px;
			display: block;
			width: 70px;
			height: 70px;
			background: url('https://feshmebel.com.ua/image/mblogo.png') ;
			background-size: contain;
			background-repeat: no-repeat;
		}

		#header .butt-menu {
			position: absolute;
			right: 0px;
			top: 0px;
			display: block;
			width: 83px;
			height: 70px;
			background: #555555;
			cursor: pointer;
		}

		#header .butt-menu:before {
			content: '';
			position: absolute;
			left: 26px;
			top: 23px;
			display: block;
			width: 35px;
			height: 15px;
			border: 5px solid #FFFFFF;
			border-width: 5px 0px;
			transition: 0.2s;
		}

		#header .butt-menu:after {
			content: '';
			position: absolute;
			left: 26px;
			top: 33px;
			display: block;
			width: 35px;
			height: 5px;
			background: #FFFFFF;
			transition: 0.2s;
		}

		#header .cart {
			position: absolute;
			right: 110px;
			top: 20px;
			height: 22px;
			line-height: 22px;
			font-size: 13px;
			cursor: pointer;
		}

		#header .cart svg {
			display: inline-block;
			width: 22px;
			height: 22px;
			vertical-align: middle;
		}

		#header .cart svg path {
			stroke: #292929;
			stroke-width: 2px;
			fill: #292929;
		}

		#header .phone {
			position: absolute;
			left: 80px;
			top: 0px;
		}

		#header .phone span:before {
			content: '+38';
			font-size: 18px;
			font-weight: bold;
			color: #B4B4B4;
		}

		#header .phone span span:before {
			content: '(0xx)';
			margin: 0px 5px 0px 3px;
			font-size: 18px;
			font-weight: bold;
			color: #252525;
		}

		#header .phone span span span {
			cursor: pointer;
		}

		#header .phone span span span:before {
			content: 'показать все номера';
			font-weight: normal;
			font-size: 13px;
			color: #4097B4;
			text-decoration: underline;
			text-decoration-style: dotted;
			transition: 0.2s;
		}

		#header .phone span span span:hover:before {
			text-decoration: none;
			color: #3A82B0;
		}

		#header .phone div {
			margin-top: 2px;
			font-size: 14px;
			color: #555555;
			text-shadow: 0px 0px 0px #555555;

		}

		#header .lang {
			position: absolute;
			right: 220px;
			top: 16px;
			height: 26px;
			font-size: 18px;
			font-weight: lighter;
			line-height: 26px;
			color: #7E7E7E;
		}

		#header .lang a {
			color: #292929;
			text-decoration: none;
			transition: 0.2s;
			display: inline-block;
			margin: 0 1px;
		}
		#header .lang span
		{
			display: inline-block;
			margin: 0 1px;
		}
		#header .lang a:hover {
			color: #FDD01B;
		}

		#header .lang a::after {

		}

		.block {
			margin-top: 30px;
		}

		.block:first-child {
			margin-top: 0px;
		}

		.block h3, .block .title {
			margin-bottom: 30px;
			font-size: 26px;
			font-weight: bold;
			color: #252525;
			text-transform: uppercase;
		}

		.block .title2 {
			position: relative;
			margin-bottom: 25px;
		}

		.block .title2 span {
			position: relative;
			display: inline-block;
			height: 20px;
			padding-right: 20px;
			font-size: 16px;
			line-height: 20px;
			font-weight: bold;
			text-transform: uppercase;
			color: #292929;
			background: #FFFFFF;
		}

		.block .title2:before {
			content: '';
			position: absolute;
			left: 0px;
			top: 9px;
			display: block;
			width: 100%;
			height: 1px;
			background: #B4B4B4;
		}

		.btext {
			line-height: 1.5em;
		}

		.btext h1 {
			line-height: 36px;
		}

		.btext h2 {
			line-height: 30px;
		}

		.btext h3 {
			line-height: 26px;
		}

		.btext h4 {
			line-height: 22px;
		}

		.btext p {
			margin-top: 0px;
		}

		.btext p:first-child {
			margin-top: 0px;
		}

		.banner-line {
			margin-top: 20px;
			width: 100%;
			text-align: center;
			overflow: hidden;
			box-sizing: border-box;
		}

		.banner-line img {
			max-width: 100%;
		}

		.banner-line div {
			margin-top: 10px;
		}

		.banner-line div:first-child {
			margin-top: 0px;
		}

		.stores1 {
			list-style: none;
			margin: 0px 0px 0px 0px;
			padding: 0px;
			width: calc(100%);
		}

		.stores1:after {
			content: '';
			display: block;
			clear: both;
			font-size: 0px;
			height: 0px;
		}

		.stores1 li {
			float: left;
			position: relative;
			width: 33.3%;
			min-height: 390px;
		}

		.stores1 li > div {
			background: #f9f9f9;
			padding: 10px;

			border: 1px solid #ddd;
		}

		.stores1 li .name {
			display: block;
			height: 40px;
			font-size: 14px;
			line-height: 20px;
			text-transform: uppercase;
			overflow: hidden;
			text-align: center;
			padding-top: 10px;
		}

		.stores1 li .name a {
			color: #414141;
			text-decoration: none;
		}

		.stores1 li .name a:hover {
			text-decoration: underline;
		}

		.stores1 li .icon {
			position: relative;
			display: block;
			width: 100%;
			text-align: center;
			line-height: 100%;
			overflow: hidden;
		}

		.stores1 li .icon img {
			max-width: 100%;
			max-height: 100%;
			transition: 0.2s;
		}

		.stores1 li .icon iframe {
			position: absolute;
			left: 50%;
			top: 50%;
			transform: translate3d(-50%, -50%, 0px);
			width: 100%;
			height: 100%;
		}

		.stores1 li .icon.ajax {
			background: url('/catalog/view/theme/fm/image/ajax-loader.gif') 50% 50% no-repeat;
		}

		.stores1 li .icon.ajax img {
			opacity: 0;
		}

		.stores1 li .rating {
			position: relative;

			display: inline-block;
			height: 16px;

			line-height: 16px;
			text-decoration: none;
		}

		.stores1 li .rating span {

			left: 0px;
			top: 2px;
			width: 10px;
			height: 11px;
			display: inline-block;
		}

		.stores1 li .rating span:after {
			content: '';
			display: block;
			height: 11px;
			background: url('/catalog/view/theme/fm/image/star11.png') 0px -11px;
		}

		.stores1 li .rating .rating1:after {
			width: 12px;
		}

		.stores1 li .rating .rating2:after {
			width: 24px;
		}

		.stores1 li .rating .rating3:after {
			width: 36px;
		}

		.stores1 li .rating .rating4:after {
			width: 48px;
		}

		.stores1 li .rating .rating5:after {
			width: 60px;
		}

		.stores1 li .rating b {
			display: block;
			font-size: 14px;
			color: #B4B4B4;
		}

		.stores1 li .rating b:before {
			content: '';
			margin-right: 8px;
			display: inline-block;
			width: 16px;
			height: 16px;
			background: url('/catalog/view/theme/fm/image/comment.svg');
			background-size: 100%;
			vertical-align: middle;
		}

		.stores1 li .price {

			font-size: 14px;
			font-weight: bold;
			color: #1c1c1c;
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}

		.stores1 li .price del {
			margin-right: 14px;
			font-size: 14px;
			font-weight: normal;
			color: #AAAAAA;
			text-decoration: line-through;
		}

		.stores1 li .price span {
			color: #808080;
			font-size: 13px;
			position: relative;
			display: inline-block;
		}

		.stores1 li .price span:after {
			content: '';
			display: block;
			height: 0;
			border-top: 1px solid #808080;
			position: absolute;
			width: 100%;
			top: 50%;
		}

		.stores1 li .order {
			position: relative;
			margin-top: 20px;
			display: block;
			text-align: center;
		}

		.stores1 li .buy:after {
			content: '<?=$button_cart;?>';
		}

		.stores1 li .wait {
			height: 32px;
			padding: 2px 30px;
			font-size: 14px;
			line-height: 16px;
		}

		.stores1 li .compare {
			position: absolute;
			right: 46px;
			top: 0px;
			width: 36px;
			height: 36px;
			background: #F5F5F5;
			transition: 0.2s;
			cursor: pointer;
		}

		.stores1 li .compare svg {
			position: absolute;
			left: 9px;
			top: 9px;
			display: block;
			width: 18px;
			height: 18px;
		}

		.stores1 li .compare svg path {
			fill: #56BBD3;
			transition: 0.2s;
		}

		.stores1 li .compare:after {
			content: '';
			position: absolute;
			right: -8px;
			bottom: -8px;
			display: block;
			width: 23px;
			height: 22px;
			background: url('/catalog/view/theme/fm/image/checked.png');
			opacity: 0;
		}

		.stores1 li .compare:hover {
			background: #FFF2BF;
		}

		.stores1 li .compare:hover svg path {
			fill: #66645F;
		}

		.stores1 li .compare.checked {
			background: #FFF2BF;
		}

		.stores1 li .compare.checked svg path {
			fill: #66645F;
		}

		.stores1 li .compare.checked:after {
			opacity: 1;
		}

		.stores1 li .wishlist {
			position: absolute;
			right: 0px;
			top: 0px;
			width: 36px;
			height: 36px;
			background: #F5F5F5;
			transition: 0.2s;
			cursor: pointer;
		}

		.stores1 li .wishlist svg {
			position: absolute;
			left: 9px;
			top: 9px;
			display: block;
			width: 18px;
			height: 18px;
		}

		.stores1 li .wishlist svg path {
			fill: #56BBD3;
			transition: 0.2s;
		}

		.stores1 li .wishlist:after {
			content: '';
			position: absolute;
			right: -8px;
			bottom: -8px;
			display: block;
			width: 23px;
			height: 22px;
			background: url('/catalog/view/theme/fm/image/checked.png');
			opacity: 0;
		}

		.stores1 li .wishlist:hover {
			background: #FFF2BF;
		}

		.stores1 li .wishlist:hover svg path {
			fill: #66645F;
		}

		.stores1 li .wishlist.checked {
			background: #FFF2BF;
		}

		.stores1 li .wishlist.checked svg path {
			fill: #66645F;
		}

		.stores1 li .wishlist.checked:after {
			opacity: 1;
		}

		.stores1 .labels {
			position: absolute;
			left: 0px;
			top: 0px;
		}

		.stores1 .labels span {
			position: relative;
			margin-bottom: 5px;
			display: block;
			width: 20px;
			height: 20px;
		}

		.stores1 .labels .lhit {
			background: #90D930;
		}

		.stores1 .labels .lhit:before {
			content: '';
			position: absolute;
			left: 2px;
			top: 2px;
			display: block;
			width: 16px;
			height: 16px;
			background-size: 100%;
		}

		.stores1 .labels .lvideo {
			background: #3EB4D0;
			cursor: pointer;
		}

		.stores1 .labels .lvideo:before {
			content: '';
			position: absolute;
			left: 2px;
			top: 2px;
			display: block;
			width: 16px;
			height: 16px;
			background-size: 100%;
		}

		.stores1 .labels .lchoise {
			background: #FF6D00;
		}

		.stores1 .labels .lchoise:before {
			content: '';
			position: absolute;
			left: 2px;
			top: 2px;
			display: block;
			width: 16px;
			height: 16px;
			background-size: 100%;
		}

		.stores1 .labels .ldiscount {
			background: #ED1B24;
			height: 70px;
		}

		.stores1 .labels .ldiscount span {
			position: absolute;
			left: 0px;
			bottom: 5px;
			height: 20px;
			white-space: nowrap;
			font-size: 12px;
			line-height: 20px;
			color: #FFFFFF;
			transform: rotate(-90deg);
		}

		.stores1 .labels .ldiscount span:before {
			content: 'sale -';
		}

		.stores1 .labels .ldiscount span:after {
			content: '%';
		}

		.stores1 .labels .lnew {
			background: #FDD01B;
			height: 40px;
		}

		.stores1 .labels .lnew:before {
			content: 'NEW';
			position: absolute;
			left: -3px;
			bottom: 10px;
			height: 20px;
			white-space: nowrap;
			font-size: 12px;
			line-height: 20px;
			font-weight: bold;
			color: #000000;
			transform: rotate(-90deg);
		}

		.stores1 .labels .lpresent {
			background: #FF3333;
		}

		.stores1 .labels .lpresent:before {
			content: '';
			position: absolute;
			left: 2px;
			top: 2px;
			display: block;
			width: 16px;
			height: 16px;
			background: url('/catalog/view/theme/fm/image/present.svg');
			background-size: 100%;
		}

		#menu {
			max-width: 360px;
			background: #FFFFFF;
			box-sizing: border-box;
		}

		#menu ul {
			list-style: none;
			margin: 0px;
			padding: 10px 20px;
			border-bottom: 1px solid #E3E3E3;
		}

		#menu ul li {
			margin: 0px;
			padding: 4px 0px;
			font-size: 14px;
			line-heihgt: 18px;
			text-transform: uppercase;
			text-shadow: 0px 0px 0px #292929;
		}

		#menu ul li a {
			color: #292929;
			text-decoration: none;
		}

		#menu ul li div {
			padding-top: 3px;
		}

		#menu ul li div a {
			display: block;
			padding: 4px 0px 4px 20px;
			font-size: 13px;
		}

		#menu .phones {
			padding: 20px;
		}

		#menu .phones div {
			font-size: 14px;
			line-height: 30px;
			color: #3A82B0;
		}

		#menu .phones div:first-child {
			margin-bottom: 5px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
		}

		#menu .phones a {
			text-decoration: none;
		}

		#menu .phones a:hover {
			text-decoration: underline;
			text-decoration-style: dotted;
		}

		#menu .phones .city-phone span {
			margin-right: 5px;
			color: #292929;
		}

		#menu .phones .line {
			margin: 0px;
			height: 10px;
		}

		#menu .phones .mobile-phone {
			position: relative;
			padding-left: 22px;
		}

		#menu .phones .mobile-phone:after {
			margin-left: 5px;
			color: #979797;
			font-size: 11px;
		}

		#menu .phones .mobile-phone.kyivstar:after {
			content: '(Киевстар)';
		}

		#menu .phones .mobile-phone.vodafone:after {
			content: '(Vodafone)';
		}

		#menu .phones .mobile-phone.lifecell:after {
			content: '(Life)';
		}

		#menu .phones .mobile-phone:before {
			content: '';
			position: absolute;
			display: block;
			background: url('/catalog/view/theme/fm/image/mobile.gif');
		}

		#menu .phones .mobile-phone.kyivstar:before {
			left: 5px;
			top: 9px;
			width: 11px;
			height: 11px;
		}

		#menu .phones .mobile-phone.vodafone:before {
			left: 6px;
			top: 11px;
			width: 9px;
			height: 9px;
			background-position: -11px 0px;
		}

		#menu .phones .mobile-phone.lifecell:before {
			left: 8px;
			top: 9px;
			width: 6px;
			height: 13px;
			background-position: -20px 0px;
		}

		#search-layer {
			background: rgba(0, 0, 0, 0.2);
		}

		#search-layer form {
			position: absolute;
			left: 0px;
			top: 0px;
			margin: 0px;
			width: 100%;
			height: 70px;
			padding: 0px;
			background: #FFFFFF;
		}

		#search-layer form input {
			position: relative;
			width: 100%;
			height: 70px;
			padding: 15px 30px;
			border: 0px;
			background: #FFFFFF;
			box-sizing: border-box;
		}

		#phones-layer .holder {
			position: absolute;
			left: 0px;
			top: 0px;
			width: 100%;
		}

		#phones-layer .phones {
			position: absolute;
			left: 340px;
			top: 50px;
			margin-left: -90px;
			display: block;
			width: 180px;
			padding: 30px;
			background: #FFFFFF;
			border: 1px solid #E5E5E5;
			z-index: 51;
		}

		#phones-layer .phones div {
			font-size: 14px;
			line-height: 30px;
			color: #3A82B0;
		}

		#phones-layer .phones a {
			text-decoration: none;
		}

		#phones-layer .phones a:hover {
			text-decoration: underline;
			text-decoration-style: dotted;
		}

		#phones-layer .phones .city-phone span {
			margin-right: 5px;
			color: #292929;
		}

		#phones-layer .phones .line {
			margin: 20px 0px;
			width: 100%;
			height: 1px;
			background: #E5E5E5;
		}

		#phones-layer .phones .mobile-phone {
			position: relative;
			padding-left: 22px;
		}

		#phones-layer .phones .mobile-phone:after {
			margin-left: 5px;
			color: #979797;
			font-size: 11px;
		}

		#phones-layer .phones .mobile-phone.kyivstar:after {
			content: '(Киевстар)';
		}

		#phones-layer .phones .mobile-phone.vodafone:after {
			content: '(Vodafone)';
		}

		#phones-layer .phones .mobile-phone.lifecell:after {
			content: '(Life)';
		}

		#phones-layer .phones .mobile-phone.viber:after {
			content: '(Viber)';
		}

		#phones-layer .phones .mobile-phone:before {
			content: '';
			position: absolute;
			display: block;
			background: url('/catalog/view/theme/fm/image/mobile.gif');
		}

		#phones-layer .phones .mobile-phone.kyivstar:before {
			left: 5px;
			top: 9px;
			width: 11px;
			height: 11px;
		}

		#phones-layer .phones .mobile-phone.vodafone:before {
			left: 6px;
			top: 11px;
			width: 9px;
			height: 9px;
			background-position: -11px 0px;
		}

		#phones-layer .phones .mobile-phone.lifecell:before {
			left: 8px;
			top: 9px;
			width: 6px;
			height: 13px;
			background-position: -20px 0px;
		}

		#phones-layer .phones .mobile-phone.viber:before {
			left: 3px;
			top: 9px;
			width: 13px;
			height: 13px;
			background-position: -26px 0px;
		}

		#phones-layer .phones:before {
			content: '';
			position: absolute;
			left: 60px;
			top: -10px;
			display: block;
			width: 0px;
			height: 0px;
			border-left: 11px solid transparent;
			border-right: 11px solid transparent;
			border-bottom: 10px solid #E5E5E5;
		}

		#phones-layer .phones:after {
			content: '';
			position: absolute;
			left: 61px;
			top: -9px;
			display: block;
			width: 0px;
			height: 0px;
			border-left: 10px solid transparent;
			border-right: 10px solid transparent;
			border-bottom: 10px solid #FFFFFF;
		}

		#footer {
			position: relative;
			margin-top: -90px;

			background: #2D4059;
			overflow: hidden;

			color: #fff;
		}
		#footer ul
		{
			list-style: none;
			margin: 0;
			padding: 0;
		}
		#footer ul li a
		{
			color: #fff;
			display: inline-block;
			text-decoration: none;
			padding: 2px;
		}
		#footer .footer-block
		{
			display: inline-block;
			vertical-align: top;
			padding: 10px;
		}
		#footer .footer-block p
		{
			line-height: 23px;
		}
		#footer .copyright {
			position: absolute;
			left: 0px;
			top: 53px;
			font-size: 13px;
			color: #7E7E81;
		}

		#footer .copyright:before {
			content: 'Copyright © 2007-';
		}

		#footer .copyright span:before {
			content: ' компания "';
		}

		#footer .copyright span:after {
			content: '"';
		}

		#footer .copyright a {
			color: #7E7E81;
			text-decoration: none;
		}

		#footer .copyright a:after {
			content: 'Feshmebel.com.ua';
		}

		#footer .copyright a:hover {
			text-decoration: underline;
		}

		#footer .lastupdate {
			position: absolute;
			left: 0px;
			top: 24px;
			font-size: 13px;
			color: #7E7E81;
		}

		#footer .lastupdate:before {
			content: 'Последнее обновление магазина: ';
		}

		#footer .lastupdate:after {
			content: ' CMS магазина Melbis Shop v6.1.0 ';
			marggin-top: 5px;
			display: block;
		}

		#footer .logo-sensepro {
			position: absolute;
			right: 0px;
			top: 35px;
			display: block;
			width: 128px;
			height: 25px;
		}

		#footer .logo-melbis {
			position: absolute;
			right: 148px;
			top: 35px;
			display: block;
			width: 116px;
			height: 25px;
		}

		#footer .logo-3wstudio {
			position: absolute;
			right: 284px;
			top: 35px;
			display: block;
			width: 107px;
			height: 25px;
		}

		.lightbox {
			position: absolute;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			background: rgba(0, 0, 0, 0.2);
		}

		@media all and (max-width: 664px) {
			#header .phone {
				left: 80px;
			}
		}

		@media all and (max-width: 610px) /* 320 */ {
			h1 {
				font-size: 18px;
			}

			h2 {
				font-size: 16px;
			}

			h3 {
				font-size: 14px;
			}

			h4 {
				font-size: 14px;
			}

			h5 {
				font-size: 14px;
			}

			#headerspace {
				height: 65px;
			}

			.pagewrap {
				margin: 0px 5px;
				padding-top: 25px;
			}

			.table {
				margin: 0px;
				display: block;
			}

			.table .thead {
				display: block;
			}

			.table .row {
				display: block;
			}

			.table .cell {
				display: block;
			}

			#header {
				height: 50px;
			}

			#header .pagewrap {
				margin: 0px;
			}

			#header .logo {
				width: 50px;
				height: 50px;

			}

			#header .client {
				display: none;
			}

			#header .cmp {
				display: none;
			}

			#header .wl {
				display: none;
			}

			#header .butt-search {
				right: 140px;
				top: 16px;
			}
			#header .lang
			{
				right: 180px;
				top: 14px;
			}
			#header .butt-menu {
				width: 57px;
				height: 50px;
			}

			#header .butt-menu span {
				display: none;
			}

			#header .butt-menu:before {
				left: 16px;
				top: 16px;
				width: 25px;
				height: 13px;
				border-width: 3px 0px;
			}

			#header .butt-menu:after {
				left: 16px;
				top: 24px;
				width: 25px;
				height: 3px;
			}

			#header .cart {
				right: 90px;
				top: 13px;
			}

			#header .phone {

				left: inherit;
				right: inherit;
				width: 100%;
				top: 50px;
				text-align: center;
				background-color: #f8f8f8;

				border-bottom: 1px solid #555;
			}

			#header .phone span:before {

			}

			#header .phone span span:before {

			}

			#header .phone span span span {
				display: block;
				width: 20px;
				height: 20px;
				background: url('/catalog/view/theme/fm/image/phone.svg');
				background-size: 100%;
			}

			#header .phone span span span:before {

			}
			#header .phone div
			{

			}
			#header .phone * {
				display: inline-block;
				vertical-align: middle;
				padding: 0 2px;

			}

			#path {

			}

			.stores1 {
				width: 100%;
			}

			.stores1 li {
				width: calc(50% - 2px);
				margin: 1px;
				height: auto;
				padding: 0px;
				min-height: auto;
			}
			.stores1 li > div
			{
				padding: 1px;
			}

			#phones-layer .phones {
				position: absolute;
				left: 115px;
			}

			#phones-layer .phones:before {
				left: 137px;
			}

			#phones-layer .phones:after {
				left: 138px;
			}

			#footer .lastupdate {
				top: 10px;
			}

			#footer .logo-sensepro {
				display: none;
			}

			#footer .logo-melbis {
				display: none;
			}

			#footer .logo-3wstudio {
				display: none;
			}
		}

		#path {
			list-style: none;
			margin: 0px;
			padding: 5px 0px 0px 0px;
			overflow: hidden;
		}

		#path li {
			float: left;
			margin: 0px 0px 3px 10px;
			padding: 0px;
			font-size: 13px;
			font-weight: 100;
			color: #292929;
		}

		#path li a {
			text-decoration: none;
			color: #2D4059;
		}

		#path li a:hover {
			text-decoration: underline;
		}

		#path li:before {
			content: '';
			margin-right: 10px;
			display: inline-block;
			width: 0px;
			height: 0px;
			border-top: 4px solid transparent;
			border-left: 4px solid #484848;
			border-bottom: 4px solid transparent;
			vertical-align: middle;
		}

		#path li.home {
			margin-left: 0px;
		}

		#path li.home a span:after {
			content: 'Главная';
		}

		#path li.home:before {
			display: none;
		}

		#page-intro {
			margin-top: 20px;
		}

		#topicname {
			padding-top: 10px;
			text-transform: none;
			text-align: center;
			font-size: 20px;

		}

		#filterline {
			position: relative;
			margin-top: 10px;
			height: 34px;
			padding-bottom: 10px;
		}

		#filterline .filterbutt {
			position: absolute;
			right: 0px;
			top: 0px;
			height: 34px;
			padding: 0px 30px;
			line-height: 34px;
			color: #141414;
			background: #FFCC01;
			cursor: pointer;
			width: calc(33.3% - 20px);
			padding-left: 10px;
			padding-right: 10px;
		}

		#filterline .filterbutt:hover {
			background: linear-gradient(to bottom, #FFD015 0%, #F9B706 100%);
		}

		#filterline .filterbutt svg {
			margin-right: 10px;
			display: inline-block;
			width: 16px;
			height: 16px;
			vertical-align: middle;
		}

		#filterline .filterbutt svg path {
			fill: #292929;
		}

		#filterline .filterbutt:after {
			content: '<?=$button_filter_amp;?>';
			font-weight: bold;
		}

		#filterline .order-butt {
			position: absolute;
			left: 0px;
			top: 0px;
			min-width: 90px;
			height: 34px;
			padding: 0px 50px 0px 10px;
			font-size: 14px;
			line-height: 34px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
			background: #F8F8F8;
			z-index: 1;
			cursor: pointer;
			padding-left: 5px;
			padding-right: 5px;
			width: calc(33.3% - 10px);

		}

		#filterline .order-butt:before {
			content: '<?=$order_butt_before;?>';
			margin-right: 0px;
			display: block;
			vertical-align: middle;
		}

		#filterline .order-butt:after {
			content: '';
			position: absolute;
			right: 10px;
			top: 15px;
			width: 0px;
			height: 0px;
			border-left: 5px solid transparent;
			border-right: 5px solid transparent;
			border-top: 6px solid #292929;
			transition: 0.2s;
		}

		#filter-layer .holder {
			position: fixed;
			right: 0px;
			top: 0px;
			width: 390px;
			height: 100%;
			padding-top: 60px;
			background: #FFFFFF;
			box-sizing: border-box;
		}

		#filter-layer .title {
			position: absolute;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 65px;
			padding: 0px 20px;
			font-size: 18px;
			line-height: 65px;
			font-weight: bold;
			color: #414141;
			box-sizing: border-box;
		}

		#filter-layer .title svg {
			margin-right: 10px;
			display: inline-block;
			width: 18px;
			height: 18px;
			vertical-align: middle;
		}

		#filter-layer .title svg:path {
			fill: #414141;
		}

		.close-butt {
			position: absolute;
			right: 20px;
			top: 24px;
			display: block;
			width: 16px;
			height: 16px;
			cursor: pointer;
		}

		.close-butt:before {
			content: '';
			position: absolute;
			left: -3px;
			top: 7px;
			display: block;
			width: 21px;
			height: 1px;
			background: #0A0A0A;
			transform: rotate(45deg);
		}

		.close-butt:after {
			content: '';
			position: absolute;
			left: -3px;
			top: 7px;
			display: block;
			width: 21px;
			height: 1px;
			background: #0A0A0A;
			transform: rotate(-45deg);
		}

		#filter-layer .total {
			position: absolute;
			left: 0px;
			top: 65px;
			width: 100%;
			height: 60px;
			padding: 0px 20px;
			font-size: 14px;
			color: #292929;
			line-height: 65px;
			color: #292929;
			box-sizing: border-box;
		}

		#filter-layer .total span {
			font-weight: bold;
		}

		#filter-layer amp-selector {
			position: relative;
			display: flex;
			flex-wrap: wrap;
			height: 100%;
		}

		#filter-layer amp-selector .butt {
			list-style: none;
			width: 50%;
			height: 34px;
			font-size: 18px;
			line-height: 34px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
			flex-grow: 1;
			text-align: center;
			border-bottom: 2px solid #FFCC01;
			cursor: pointer;
			outline: none;
		}

		#filter-layer amp-selector .butt:hover {
			background: #FFF6D1;
		}

		#filter-layer amp-selector .butt[selected] {
			background: #FFCC01;
			cursor: default;
		}

		#filter-layer amp-selector .butt[selected]:hover {
			background: #FFCC01;
		}

		#filter-layer amp-selector .сontent {
			position: relative;
			display: none;
			width: 100%;
			height: calc(100% - 70px);
			order: 1;
			overflow: auto;
		}

		#filter-layer amp-selector .butt[selected] + .сontent {
			display: block;

		}

		#filter-layer .filter {
			padding: 25px;
			border-bottom: 1px solid #EAEAEA;
		}

		#filter-layer .filter .name {
			position: relative;
			padding-right: 70px;
			font-size: 18px;
			font-weight: bold;
			color: #414141;
			cursor: pointer;
		}

		#filter-layer .filter ul {
			list-style: none;
			margin: 0px;
			padding: 0px;
		}

		#filter-layer .filter ul li {
			position: relative;
			margin: 10px 0px 0px 0px;
			padding: 0px 0px 0px 26px;
			font-size: 14px;
			line-height: 18px;
			color: #B4B4B4;
		}

		#filter-layer .filter ul li a {
			color: #292929;
			text-decoration: none;
		}

		#filter-layer .filter ul li a:hover {
			text-decoration: underline;
		}

		#filter-layer .filter ul li span {
			position: absolute;
			left: 0px;
			top: 2px;
			display: block;
			width: 14px;
			height: 14px;
			border: 1px solid #B4B4B4;
			cursor: pointer;
		}

		#filter-layer .filter ul li.checked span:after {
			position: absolute;
			left: 3px;
			top: 3px;
			content: '';
			display: block;
			width: 8px;
			height: 8px;
			background: #FFCC01;
			border-radius: 50%;
		}

		#filter-layer .filter ul li.fastfilter {
			padding-left: 0px;
		}

		#filter-layer .filter ul li.series {
			margin-left: 26px;
		}

		#filter-layer .filter:last-child {
			border-bottom: 0px;
		}

		#filter-layer .filter.advice .name {
			cursor: default;
		}

		#filter-layer .filter.advice .name:after {
			display: none;
		}

		#filter-layer .filter.advice .name:before {
			content: 'Советы';
		}

		#filter-layer .filter.advice span {
			margin-top: 10px;
			display: block;
			font-size: 14px;
		}

		#filter-layer .filter.advice span a {
			color: #2D4059;
			text-decoration: none;
		}

		#filter-layer .filter.advice span a:hover {
			text-decoration: underline;
		}

		#filter-layer .chosen {
			padding: 25px;
		}

		#filter-layer .chosen .name {
			position: relative;
			font-size: 18px;
			font-weight: bold;
			color: #414141;
		}

		#filter-layer .chosen a {
			position: relative;
			margin: 13px 10px 0px 0px;
			display: inline-block;
			height: 36px;
			padding: 0px 40px 0px 15px;
			background: #EAEAEA;
			font-size: 14px;
			line-height: 36px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
			text-decoration: none;
			cursor: pointer;
			transition: 0.2s;
		}

		#filter-layer .chosen a:hover {
			background: #FFF2BF;
		}

		#filter-layer .chosen a:before {
			content: '';
			position: absolute;
			right: 10px;
			top: 18px;
			display: block;
			width: 22px;
			height: 1px;
			background: #0D0D0D;
			transform: rotate(45deg);
		}

		#filter-layer .chosen a:after {
			content: '';
			position: absolute;
			right: 10px;
			top: 18px;
			display: block;
			width: 22px;
			height: 1px;
			background: #0D0D0D;
			transform: rotate(-45deg);
		}

		#filter-layer .clear-all {
			padding: 25px;
		}

		#filter-layer .clear-all a {
			display: block;
			height: 36px;
			background: #EAEAEA;
			text-align: center;
			font-size: 14px;
			line-height: 36px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
			text-decoration: none;
			cursor: pointer;
			transition: 0.2s;
		}

		#filter-layer .clear-all a:hover {
			background: #FFF2BF;
		}

		#filter-layer .clear-all a:before {
			content: 'Очистить все фильтры';
		}

		#order-layer .holder {
			position: absolute;
			left: 50%;
			top: 50%;
			width: 300px;
			padding: 10px;
			background: #FFFFFF;
			transform: translate(-50%, -50%);
		}

		#order-layer .holder a {
			margin-top: 5px;
			display: block;
			width: 100%;
			height: 34px;
			font-size: 18px;
			line-height: 34px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
			text-decoration: none;
			text-align: center;
			background: #F8F8F8;
			cursor: pointer;
		}

		#order-layer .holder a:hover {
			background: #FFF6D1;
		}

		#order-layer .holder span {
			margin-top: 5px;
			display: block;
			width: 100%;
			height: 34px;
			font-size: 18px;
			line-height: 34px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
			text-align: center;
			background: #FFCC01;
		}

		#order-layer .clear:after {
			content: 'популярные';
		}

		#order-layer .cheap:after {
			content: 'недорогие';
		}

		#order-layer .expensive:after {
			content: 'дорогие';
		}

		#order-layer .new:after {
			content: 'новинки';
		}

		#order-layer .disc:after {
			content: 'со скидкой';
		}

		#guideline {
			margin: 35px auto 0px auto;
			text-align: center;
		}

		#guideline .pagination {
			margin: 0;
			padding: 0;
			list-style: none;
			margin-top: 15px;
			text-align: center;
		}

		#guideline .pagination li {
			display: inline-block;
		}

		#guideline .pagination a {
			margin-right: 10px;
			display: inline-block;
			min-width: 26px;
			height: 34px;
			padding: 0px 5px;
			font-size: 16px;
			line-height: 34px;
			color: #292929;
			text-shadow: 0px 0px 0px #292929;
			text-decoration: none;
			text-align: center;
			background: #F0F0F0;
			border-bottom: 2px solid #2D4059;
			vertical-align: middle;
			transition: 0.2s;
		}

		#guideline .pagination a:hover {
			background: #FFF6D1;
		}

		#guideline .pagination span {
			margin-right: 10px;
			display: inline-block;
			min-width: 26px;
			height: 34px;
			padding: 0px 5px;
			font-size: 16px;
			line-height: 34px;
			font-weight: normal;
			color: #FFFFFF;
			text-shadow: 0px 0px 0px #FFFFFF;
			text-decoration: none;
			text-align: center;
			background: #2D4059;
			border-bottom: 2px solid #2D4059;
			vertical-align: middle;
		}

		#guideline .pagination li:last-child a {
			margin-right: 0px;
		}

		#guideline .pagination .prev {
			height: 36px;
			border-bottom: 0px;
		}

		#guideline .pagination .prev:after {
			content: '';
			display: inline-block;
			vertical-align: middle;
			width: 0px;
			height: 0px;
			border-top: 8px solid transparent;
			border-right: 8px solid #626262;
			border-bottom: 8px solid transparent;
		}

		#guideline .pagination .next {
			height: 36px;
			border-bottom: 0px;
		}

		#guideline .pagination .next:after {
			content: '';
			display: inline-block;
			vertical-align: middle;
			width: 0px;
			height: 0px;
			border-top: 8px solid transparent;
			border-left: 8px solid #626262;
			border-bottom: 8px solid transparent;
		}
		.select-colors,
		.form-group
		{
			height: 32px;
			text-align: center;
		}
		.form-group select
		{
			width: 100%;
			display: inline-block;
		}
		.select-colors a
		{
			height: 32px;
			width: 32px;
			display: inline-block;
			line-height: 32px;
			text-decoration: none;
			vertical-align: top;
		}
		.sale_status_image
		{
			height: 40px;

		}
		.sale_status_image .privat
		{
			float: right;
		}

		.category-list
		{
			list-style: none;
			padding: 0;
			margin: 0;
		}
		.category-list li
		{
			width: 24%;
			display: inline-block;
			vertical-align: top;
			text-align: center;
		}
		.category-list li.articles
		{
			width: 48%;
		}
		.category-list li .category-list-title
		{
			height: 35px;
			display: block;
			overflow: hidden;
		}
		.rating-box
		{
			height: 20px;
			text-align: center;
		}
		.price
		{
			height: 14px;
		}
		.order
		{
			height: 38px;
		}
		.stagh2
		{
			text-align: center;
			font-weight: bold;
			font-size: 20px;
			display: block;
		}
		.content_bottom
		{
			padding: 10px 0;
		}
		.sale_status
		{
			display: block;
			position: absolute;
			width: 111px;
			height: 20px;
			top: 0;
			left: 0;
			color: #fff;
			text-align: center;
			padding: 5px 0;
			z-index: 1;
		}
		@media all and (max-width: 610px) /* 320 */ {

			#filterline .filterbutt {
				padding: 0px 10px;

				width: calc(50% - 20px);
				text-align: center;
			}
			#filterline .order-butt
			{

				width: calc(50% - 10px);
				text-align: left;
				padding-left: 5px;
				padding-right: 5px;
			}
			.stores1 li .order .butt1.buy
			{
				width: 100%;
				padding-left: 0;
				padding-right: 0;

			}
			#filter-layer .holder {
				width: 280px;
			}

			#guideline {
				margin: 5px auto 0px auto;
				width: auto;
			}

			#guideline .pagination {
				margin-top: 0px;
			}

			#guideline .pagination a, #guideline .pagination b, #guideline .pagination span {
				margin-top: 15px;
			}
		}
	</style>
	<script async src="https://cdn.ampproject.org/v0.js"></script>
	<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
	<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
	<script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Article",
			"mainEntityOfPage": {
				"@type": "WebPage",
				"@id": "https://google.com/article"
			},
			"name": "<?php if($curr_lang == 'ru'){ ?>Фешемебельный интернет магазин<? }else{ ?>Фешемебельний інтернет магазин<? } ?>",
			"headline": "<?php if($curr_lang == 'ru'){ ?>Фешемебельный интернет магазин<? }else{ ?>Фешемебельний інтернет магазин<? } ?>",
			"description": "",
			"image": {
				"@type": "ImageObject",
				"url": "https:",
				"width": "300",
				"height": "300"
			},
			"author": {
				"@type": "Person",
				"name": "Feshmebel.com.ua"
			},
			"publisher": {
				"@type": "Organization",
				"name": "Feshmebel.com.ua",
				"logo": {
					"@type": "ImageObject",
					"url": "https://feshmebel.com.ua/image/mblogo.png",
					"width": "200",
					"height": "200"
				}
			},
			"datePublished": "2019-06-07T09:22:07+03:00",
			"dateModified": "2019-06-07T09:22:07+03:00"
		}
	</script>
	<meta name="amp-google-client-id-api" content="googleanalytics">

</head>
<body>

<amp-analytics type="gtag" data-credentials="include">
	<script type="application/json">
		{
			"vars" : {
				"gtag_id": "UA-28915158-1",
				"config" : {
					"UA-28915158-1": { "groups": "default" }
				}
			}
		}
	</script>
</amp-analytics>

<div id="page">
	<div id="headerspace"></div>
	<div class="pagewrap">

		<h1 id="topicname"><?php if($curr_lang == 'ru'){ ?>Фешемебельный интернет магазин<? }else{ ?>Фешемебельний інтернет магазин<? } ?></h1>

		<div class="home-promo">
			<?php foreach($home_banner as $val){ ?>
			<div class="home-promo-item">
				<a href="<?php if($val['language_id'] == 1){echo $val['link'];}else{echo '/ua'.$val['link'];}?>" >
					<amp-img src="/image/<?=$val['image_mobile'];?>" width="600px" height="400px" layout="responsive"></amp-img>
				</a>
			</div>
			<?php } ?>
		</div>
		<div class="content_bottom">
			<?php echo $content_bottom; ?>
		</div>
		<div class="container">
			<?php if ($articles) { ?>
				<h2><?php echo $blog_title;?> </h2>
				<div class="categories">
					<ul class="category-list">
						<?php foreach ($articles as $category) { ?>
							<?php if(!empty($category['thumb'])) { ?>
							<li class="category-list-item articles">
								<a class="category-list-img" href="<?php echo $category['href']; ?>">
									<amp-img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"
											 title="<?php echo $category['name']; ?>" width="256px" height="256px"
											 layout="responsive"></amp-img>
								</a>
								<a class="category-list-title" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
							</li>
							<?php } ?>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>

		</div>

	</div>
	<div id="footerspace"></div>
</div>
<div id="header">
	<div class="pagewrap">
		<a href="<?php echo $compare; ?>" class="cmp">
			<svg viewBox="0 0 477.774 477.774">
				<path d="M169.613,379.673l-64.589-207.565c-1.619-5.203-6.433-8.75-11.88-8.75c-5.445,0-10.258,3.541-11.881,8.75L16.661,379.673 c-6.078,0.445-11.588,3.824-14.506,9.238c-3.129,5.793-2.836,12.846,0.777,18.355c19.255,29.367,52.463,48.77,90.211,48.77 c37.748,0,70.941-19.402,90.211-48.77c3.615-5.51,3.923-12.568,0.779-18.363C181.218,383.482,175.691,380.111,169.613,379.673z M42.788,379.503L93.144,217.7l50.343,161.803H42.788z"/>
				<path d="M475.611,388.904c-2.916-5.422-8.428-8.793-14.522-9.23l-64.585-207.565c-1.621-5.203-6.435-8.75-11.883-8.75 c-5.445,0-10.259,3.541-11.879,8.75l-64.602,207.565c-6.079,0.445-11.589,3.824-14.507,9.238 c-3.129,5.793-2.835,12.846,0.779,18.355c19.254,29.359,52.462,48.752,90.209,48.752c37.75,0,70.943-19.393,90.213-48.752 C478.448,401.757,478.757,394.704,475.611,388.904z M334.249,379.503l50.371-161.811l50.359,161.811H334.249z"/>
				<path d="M34.537,142.952h150.667c-1.639-5.285-2.771-10.785-2.771-16.596c0-4.004,0.939-9.611,2.689-16.598H34.537 c-9.173,0-16.597,7.43-16.597,16.598C17.941,135.521,25.365,142.952,34.537,142.952z"/>
				<path d="M443.228,142.952c9.175,0,16.598-7.432,16.598-16.596c0-9.168-7.423-16.598-16.598-16.598H292.644 c1.75,6.986,2.69,12.594,2.69,16.598c0,5.811-1.136,11.311-2.772,16.596H443.228z"/>
				<path d="M238.884,21.738c-1.67,0-3.178,0.988-3.841,2.51c-22.563,51.832-36.015,90.002-36.015,102.109 c0,21.967,17.878,39.846,39.855,39.846c21.977,0,39.853-17.879,39.853-39.846c0-12.115-13.453-50.285-35.996-102.092 C242.06,22.726,240.552,21.738,238.884,21.738z M256.518,126.357c0,9.738-7.895,17.635-17.634,17.635 c-9.742,0-17.637-7.896-17.637-17.635c0-9.742,7.895-17.635,17.637-17.635C248.623,108.722,256.518,116.615,256.518,126.357z"/>
			</svg>
		</a>
		<a href="<?php echo $wishlist; ?>" class="wl">
			<svg viewBox="0 0 455 455">
				<path d="M326.632,10.346c-38.733,0-74.991,17.537-99.132,46.92c-24.141-29.384-60.398-46.92-99.132-46.92 C57.586,10.346,0,67.931,0,138.714c0,55.426,33.05,119.535,98.23,190.546c50.161,54.647,104.728,96.959,120.257,108.626l9.01,6.769 l9.01-6.768c15.529-11.667,70.098-53.978,120.26-108.625C421.949,258.251,455,194.141,455,138.714 C455,67.931,397.414,10.346,326.632,10.346z M334.666,308.974c-41.259,44.948-85.648,81.283-107.169,98.029 c-21.52-16.746-65.907-53.082-107.166-98.03C61.236,244.592,30,185.717,30,138.714c0-54.24,44.128-98.368,98.368-98.368 c35.694,0,68.652,19.454,86.013,50.771l13.119,23.666l13.119-23.666c17.36-31.316,50.318-50.771,86.013-50.771 c54.24,0,98.368,44.127,98.368,98.368C425,185.719,393.763,244.594,334.666,308.974z"/>
			</svg>
		</a>
		<div class="butt-search" on="tap:search-layer" role="button" tabindex="2">
			<svg viewBox="0 0 40 40">
				<ellipse ry="14.25" rx="14.25" cy="16.375" cx="16.375" stroke-width="4"/>
				<line y2="39" x2="39.125" y1="26.97" x1="27" stroke-width="6"/>
			</svg>
		</div>
		<a href="<?php echo $home; ?>" class="logo"></a>
		<span class="butt-menu" on="tap:menu" role="button" tabindex="0"></span>
		<a class="cart" href="<?php echo $checkout; ?>">
			<svg viewBox="0 0 64.26 64.26">
				<path d="M56.806,40.576L64.26,8.63H11.512l-0.582-8H0v2h9.07l3.932,54h0.002c0.04,4,3.161,6.964,6.996,6.964  c3.52,0,6.432-2.577,6.92-5.964h10.16c0.488,3.387,3.401,6,6.92,6c3.86,0,7-3.14,7-7s-3.14-7-7-7c-3.52,0-6.432,2.613-6.92,6H26.92  c-0.488-3.387-3.401-6-6.92-6c-2.132,0-4.041,0.961-5.326,2.469l-0.621-8.54L56.806,40.576z M61.74,10.63l-6.546,28.054  l-41.287,2.88l-2.25-30.934H61.74z M44,51.63c2.757,0,5,2.243,5,5s-2.243,5-5,5s-5-2.243-5-5S41.243,51.63,44,51.63z M20,51.63  c2.757,0,5,2.243,5,5s-2.243,5-5,5s-5-2.243-5-5S17.243,51.63,20,51.63z"/>
			</svg>
		</a>
		<div class="phone">
			<div class="city-phone"><a href="tel:+380443343532">+38(044)334-35-32</a></div>
			<div class="city-phone"><a href="tel:0800330190">0(800)330-190 (бесплатно)</a></div>
			<div>
				<div>
					Пн-Пт з 9:00 до 18:00
				</div>
				<div>
					Сб з 10:00 до 16:00
				</div>
			</div>
		</div>
		<div class="lang">
			<?=$language;?>
		</div>
	</div>
</div>
<div id="footer">
	<div class="pagewrap">
		<div class="footer-block">
			<p>© 2012-<?php echo date("Y"); ?> feshmebel.com.ua</p>
			<p><a href="mailto:info@feshemebel.com.ua">info@feshemebel.com.ua</a></p>
			<p><a href="tel:+380443343532">+38(044)334-35-32</a></p>
			<p><?php echo $footer_schedule; ?></p>
		</div>
		<div class="footer-block">
			<ul>
				<li><a href="<?php echo $url_menu_delivery; ?>"><?php echo $footer_menu_delivery; ?></a></li>
				<li><a href="<?php echo $url_menu_guaranty; ?>"><?php echo $footer_menu_guaranty; ?></a></li>
				<li><a href="<?php echo $url_menu_exchange; ?>"><?php echo $footer_menu_exchange; ?></a></li>
				<li><a href="<?php echo $url_menu_discount; ?>"><?php echo $footer_menu_discount; ?></a></li>
			</ul>
		</div>
		<div class="footer-block">
			<ul>
				<li><a href="<?php echo $url_menu_help; ?>"><?php echo $footer_menu_help; ?></a></li>
				<li><a href="<?php echo $url_menu_about; ?>"><?php echo $footer_menu_about; ?></a></li>
				<li><a href="<?php echo $url_menu_articles; ?>"><?php echo $footer_menu_articles; ?></a></li>
				<li><a href="<?php echo $url_menu_contacts; ?>"><?php echo $footer_menu_contacts; ?></a></li>
				<li><a href="<?php echo $url_menu_sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
			</ul>
		</div>
	</div>
</div>
<amp-sidebar id="menu" layout="nodisplay" side="right">
	<div class="close-butt" on="tap:menu.close" role="button" tabindex="0"></div>
	<ul>
		<li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
		<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
		<li><a href="<?php echo $compare; ?>"><?php echo $text_compare; ?></a></li>
		<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
		<li><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
	</ul>
	<ul>
		<?php foreach ($categories as $category) { ?>

		<?php
			if($category['id'] == 88){
				if($lang == 'uk'){
					$category['children'] = array(
						array('name' => 'Кухонні обідні', 'href' => '/ua/stolovaja/stulja/#obedennye'),
		array('name' => 'Барні кухонні', 'href' => '/ua/stolovaja/barnye-stulja/#kuhonnye'),
		array('name' => 'Табуретки і лави', 'href' => '/ua/stolovaja/taburety-i-skami/')
		);
		}else{
		$category['children'] = array(
		array('name' => 'Кухонные обеденные', 'href' => '/stolovaja/stulja/#obedennye'),
		array('name' => 'Барные кухонные', 'href' => '/stolovaja/barnye-stulja/#kuhonnye'),
		array('name' => 'Табуретки и скамьи', 'href' => '/stolovaja/taburety-i-skami/')
		);
		}
		}
		if($category['id'] == 87){
		if($lang == 'uk'){
		$category['children'] = array(
		array('name' => 'Кухонні обідні столи', 'href' => '/ua/stolovaja/stoly/#kuhonnye'),
		array('name' => 'Обідні столи-трансформери', 'href' => '/ua/stolovaja/stoly-transformery/#obedennye'),
		array('name' => 'Туалетний столик', 'href' => '/ua/spalnya/tualetnye-stoliki/#stolik'),
		array('name' => 'Столи письмові', 'href' => '/ua/ofis/stoly-pismennye/'),
		array('name' => 'Каркаси і опори для столів', 'href' => '/ua/stolovaja/opory-dlja-stolov/'),
		array('name' => 'Комплекти столів і стільців', 'href' => '/ua/stolovaja/obedennye-komplekty/'),
		array('name' => 'Стільниці для столів', 'href' => '/ua/stoli/stoleshnitsy-dlja-stolov/')
		);
		}else{
		$category['children'] = array(
		array('name' => 'Кухонные обеденные столы', 'href' => '/stolovaja/stoly/#kuhonnye'),
		array('name' => 'Обеденные столы-трансформеры', 'href' => '/stolovaja/stoly-transformery/#obedennye'),
		array('name' => 'Туалетный столик', 'href' => '/spalnya/tualetnye-stoliki/#stolik'),
		array('name' => 'Столы письменные', 'href' => '/ofis/stoly-pismennye/'),
		array('name' => 'Каркасы и опоры для столов', 'href' => '/stolovaja/opory-dlja-stolov/'),
		array('name' => 'Комплекты столов и стульев', 'href' => '/stolovaja/obedennye-komplekty/'),
		array('name' => 'Столешницы для столов', 'href' => '/stoli/stoleshnitsy-dlja-stolov/')
		);
		}
		}
		?>

		<?php if ($category['children']) { ?>
		<li>
			<a href="<?php echo $category['href']; ?>" class="dropdown-toggle"><?php echo $category['name']; ?></a>
			<?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
			<div>
				<?php foreach ($children as $child) { ?>
				<a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
				<?php } ?>
			</div>
			<?php } ?>

		</li>
		<?php } else { ?>
		<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
		<?php } ?>
		<?php } ?>
	</ul>

	<div class="phones">

		<div class="city-phone"><a href="tel:+380443343532">+38(044)334-35-32</a></div>

	</div>
</amp-sidebar>
<amp-lightbox id="search-layer" layout="nodisplay">
	<div class="lightbox" on="tap:search-layer.close" role="button" tabindex="0"></div>
	<form method="get" action="/search" target="_top">
		<div class="pagewrap">

			<input type="search" name="search" placeholder="Быстрый поиск товара"/>
		</div>
	</form>
</amp-lightbox>
<amp-lightbox id="phones-layer" layout="nodisplay">
	<div class="lightbox" on="tap:phones-layer.close" role="button" tabindex="0"></div>
	<div class="holder">
		<div class="pagewrap">
			<div class="phones">

				<div class="city-phone"><a href="tel:+380443343532">+38(044)334-35-32</a></div>
			</div>
		</div>
	</div>
</amp-lightbox>
</body>
</html>