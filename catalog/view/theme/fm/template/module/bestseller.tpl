<h3 class="best-sales">Топ продаж</h3><!-- <?php echo $heading_title; ?> -->
<div class="row product-list product-list--full">
<!--
<div class="first-slider-wrap clearfix">
<div class="bxslider">
-->
  <?php foreach ($products as $product) { ?>
	<?php if ((strpos( $heading_title, 'Рекомендуемые') !== false) || (strpos( $heading_title, 'продаж') !== false)) { ?>
<!--
  <div class="new-slider col-lg-3 col-md-3 col-sm-6 col-xs-12" style='width:250px;'>
-->
  <?php //} else { ?>
  <div class="product-layout">
  <?php }; ?>
    <?php if($product['privat']){ ?>
      <div class=" privat">
        <img class="bestseller" src="/catalog/view/javascript/jquery/pp_calculator/img/pp_logo.png" >
      </div>
    <?php } ?>
    <div class="product-thumb">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <!-- <p><?php echo $product['description']; ?></p> -->
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
        <?php foreach ($product['options'] as $option) { ?>
                     <?php if ($option['type'] == 'select') { ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> variationProdCat">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <div class="varSelectWrap">
                  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                    <option value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                  </div>
                </div>
            <?php } ?>

            <?php if ($option['type'] == 'image') { ?>
             <!-- <p class="chose-option">Выберете цвет:</p> -->
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> variationProdCat">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                <div class="radio radio-image">
                  <label>
                    <input class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
<!--                    <img src="<?php //echo $option_value['image']; ?>" alt="<?php //echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"> --><?php //echo $option_value['name']; ?>
                    <img style="width: 32px; border: 1px solid #000000;;" src="<?php echo '//'.$_SERVER['SERVER_NAME'].'/image/'.$option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php } ?>

         <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>

      </div>
       <div class="button-group">
        <div class="clearfix wish-buttons-card noaa">
          <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare?></button>
          <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist?></button>

        </div>
        <div class="button-cart-add clearfix">
          <button class="button-cart-p" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span></button>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
</ul>
</div>
