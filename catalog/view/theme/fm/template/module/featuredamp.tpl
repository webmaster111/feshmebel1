<ul class="stores1">
  <?php foreach ($products as $product) { ?>
  <li>
    <div>

      <div class="sale_status_image">
        <?php if ($product['sale_status_image']) { ?>

        <span class="sale_status" style="background-color: #<?=$product['sale_status_color'];?>;" ><?=$product['sale_status_name'];?></span>


        <?php } ?>
        <?php if($product['privat']){ ?>
        <span class="privat">
                                 <amp-img src="/catalog/view/javascript/jquery/pp_calculator/img/pp_logo.png" width="28px" height="28px" layout="fixed"></amp-img>
                            </span>


        <?php } ?>
      </div>

      <amp-carousel width="256"
                    height="256"
                    layout="responsive"
                    type="slides">
        <amp-img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                 title="<?php echo $product['name']; ?>" width="256px" height="256px"
                 layout="responsive"></amp-img>

        <?php foreach($product['images'] as $image) { ?>

        <amp-img src="<?php echo $image['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                 title="<?php echo $product['name']; ?>" width="256px" height="256px"
                 layout="responsive"></amp-img>

        <?php } ?>
      </amp-carousel>
      <strong class="name"><a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>"><?php echo $product['name']; ?></a></strong>

      <div class="price">
        <?php if ($product['price']) { ?>

        <?php if (!$product['special']) { ?>
        <?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
        <span ><?php echo ceil($product['old_price']).' грн.'; ?></span>
        <?php } ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span ><?php echo $product['price']; ?></span>
        <?php echo $product['special']; ?>
        <?php } ?>


        <?php } else { ?>
        <amp-img src="<?php echo $product['stock_image']; ?>" width="157px" height="27px" layout="fixed"></amp-img>
        <?php } ?>
      </div>
      <div class="rating-box">
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
      </div>

      <?php foreach ($product['options'] as $option) { ?>
      <?php if (!$is_sortoption_id) { ?>
      <?php if ($option['type'] == 'select') { ?>
      <?php

                                    if (!isset($select_nomer) or empty($select_nomer)){
                                        $select_nomer = trim($option['product_option_id']);
                                    }else{
                                        $select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
                                    }

                                ?>
      <div class="form-group">
        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
        <div class="varSelectWrap">
          <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
            <option value=""><?php echo $text_select; ?></option>
            <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
            <option  value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
      </div>
      <?php } ?>

      <?php if ($option['type'] == 'image') { ?>
      <?php

                                    if (!isset($option_nomer) or empty($option_nomer)){
                                        $option_nomer = $option['product_option_id'];
                                    }else{
                                        $option_nomer = $option_nomer .', '.$option['product_option_id'];
                                    }

                                ?>

      <div class="select-colors">
        <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
        <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>">
          <amp-img src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" width="32px" height="32px" layout="fixed"  />
          </amp-img>
        </a>
        <?php if($num>2 && count($option['product_option_value'])>3) { ?>
        <?php if($curr_lang == 'ru'){ ?>
        <a href="<?php echo $product['href']; ?>" class="show-more">ещё</a>
        <?php }else{ ?>
        <a href="<?php echo $product['href']; ?>" class="show-more">ще</a>
        <?php } ?>
        <?php } ?>
        <?php if($num>2) break; ?>
        <?php } ?>
      </div>

      <?php } ?>

      <?php break; ?>
      <?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
      <?php if ($option['type'] == 'select') { ?>
      <?php

                                    if (!isset($select_nomer) or empty($select_nomer)){
                                        $select_nomer = trim($option['product_option_id']);
                                    }else{
                                        $select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
                                    }

                                ?>
      <div class="form-group">
        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
        <div class="varSelectWrap">
          <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
            <option value=""><?php echo $text_select; ?></option>
            <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
            <option value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
            <?php if ($option_value['price']) { ?>
            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
            <?php } ?>
            </option>
            <?php } ?>
          </select>
        </div>
      </div>
      <?php } ?>


      <?php if ($option['type'] == 'image') { ?>
      <?php

                                    if (!isset($option_nomer) or empty($option_nomer)){
                                        $option_nomer = $option['product_option_id'];
                                    }else{
                                        $option_nomer = $option_nomer .', '.$option['product_option_id'];
                                    }

                                ?>
      <div class="form-group">
        <label class="control-label"><?php echo $option['name']; ?></label>
        <div class="select-colors 2">
          <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
            <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
            <div class="radio radio-image">
              <label title="<?php echo $option_value['name'] ?>" class="change-price">
                <input  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>

                <amp-img style="width: 32px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?>
                  (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                  <?php } ?></span>
              </label>
            </div>
            <?php } ?>
          </div>
        </div>

      </div>
      <?php } ?>
      <?php } ?>
      <?php } ?>

      <div class="order">
        <?php if ($product['price']) { ?>
        <a href="<?php echo $product['href']; ?>" class="butt1 buy"></a>
        <?php } ?>
      </div>
    </div>
  </li>
  <?php } ?>

</ul>