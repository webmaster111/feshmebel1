
<?php if ($products) { ?>
<!-- brand -->
<section class="promotions">
  <div class="wrapper">
    <div class="row">
      <div class="promotions__head h1"><?=$heading_title;?></div>
      <div class="promotions__slider promotions__slider-block">
      <div class="row featured">
        <?php foreach ($products as $product) { ?>

        <div class="categories-goods__item">
          <div class="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo (!empty( $product['special']) ?  $product['special'] : $product['price'] ); ?></div>
          <div class="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
          <div class="categories-goods__information">
            <?php if($product['sale_status_image']){ ?>
            <div class="categories-goods__label" style="background-color: #<?=$product['sale_status_color'];?>;" ><?=$product['sale_status_name'];?></div>
            <?php } ?>
            <div class="categories-goods__image">
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
              </a>
            </div>
            <div class="categories-goods__rating">
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="categories-goods__price">
            <div class="categories-goods__link"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
            <?php if ($product['price']) { ?>
            <div class="categories-goods__price-block">
              <?php if (!$product['special']) { ?>
              <?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
              <div class="categories-goods__old-price">
                <!-- div class="categories-goods__discount">-3001</div -->
                <div class="categories-goods__before-discount"><?php echo ceil($product['old_price']).' грн.'; ?></div>
              </div>
              <?php } ?>
              <div class="categories-goods__new-price"><?php echo $product['price']; ?></div>
              <?php } else { ?>
              <div class="categories-goods__old-price">
                <!-- div class="categories-goods__discount">-3001</div -->
                <div class="categories-goods__before-discount"><?php echo $product['price']; ?></div>
              </div>
              <div class="categories-goods__new-price"><?php echo $product['special']; ?></div>
              <?php } ?>
            </div>
            <?php } ?>
            <div class="categories-goods__options">

              <?php
										//start Tkach web-promo
										$option_nomer = '';
										$select_nomer = '';
										// end

										uasort($product['options'], function($a, $b){
											if ($a['option_id'] === $b['option_id']) return 0;
											return $a['option_id'] < $b['option_id'] ? -1 : 1;
											}
										);

										$is_sortoption_id = 0;

										foreach($product['options'] as $val){
											if($val['sort_option']){
												$is_sortoption_id = $val['option_id'];
												break;
											}
										}

										?>
              <?php foreach ($product['options'] as $option) { ?>
              <?php if (!$is_sortoption_id) { ?>
              <?php if ($option['type'] == 'select') { ?>
              <?php
													// start Tkach web-promo
													if (!isset($select_nomer) or empty($select_nomer)){
														$select_nomer = trim($option['product_option_id']);
													}else{
														$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
													}
													// end
													?>
              <div class="form-group">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="varSelectWrap">
                  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                    <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <?php } ?>


              <?php if ($option['type'] == 'image') { ?>
              <?php
													// start Tkach web-promo
													if (!isset($option_nomer) or empty($option_nomer)){
														$option_nomer = $option['product_option_id'];
													}else{
														$option_nomer = $option_nomer .', '.$option['product_option_id'];
													}
													// end
													?>
              <div class="form-group">
                <label class="control-label"><?php echo $option['name']; ?></label>
                <div class="select-colors">
                  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                    <div class="radio radio-image">
                      <label title="<?php echo $option_value['name'] ?>" class="change-price">
                        <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                        <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                        <span class="hidden-descr"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
																			</span>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="show-more-box">
                  <?php if($curr_lang == 'ru'){ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                  <?php }else{ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                  <?php } ?>
                </div>

              </div>
              <?php } ?>

              <?php break; ?>
              <?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
              <?php if ($option['type'] == 'select') { ?>
              <?php
															// start Tkach web-promo
															if (!isset($select_nomer) or empty($select_nomer)){
																$select_nomer = trim($option['product_option_id']);
															}else{
																$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
															}
															// end
														?>
              <div class="form-group">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="varSelectWrap">
                  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                    <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <?php } ?>


              <?php if ($option['type'] == 'image') { ?>
              <?php
														// start Tkach web-promo
														if (!isset($option_nomer) or empty($option_nomer)){
															$option_nomer = $option['product_option_id'];
														}else{
															$option_nomer = $option_nomer .', '.$option['product_option_id'];
														}
														// end
													?>
              <div class="form-group">
                <label class="control-label"><?php echo $option['name']; ?></label>
                <div class="select-colors">
                  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                    <div class="radio radio-image">
                      <label title="<?php echo $option_value['name'] ?>" class="change-price">
                        <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                        <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?></span>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="show-more-box">
                  <?php if($curr_lang == 'ru'){ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                  <?php }else{ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                  <?php } ?>
                </div>
              </div>
              <?php } ?>
              <?php } ?>
              <?php } ?>

            </div>
            <div class="categories-goods__actions">
              <?php if (!empty($product['stock_image'])) { ?>
              <img class="stock_image" src="<?=$product['stock_image'];?>">
              <?php } else { ?>
              <div class="categories-goods__wish" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></div>
              <div class="categories-goods__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"></div>
              <?php if (strpos( $product['stock_status'],'жида') !== false) { ?>
              <div class="categories-goods__buy" ><span><?php echo $button_expect; ?></span></div>
              <?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>

              <div class="categories-goods__buy" ><span><?php echo $button_out_of_sale; ?></span></div>

              <?php } elseif (strpos( $product['stock_status'],'Предзаказ') !== false) { ?>
              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_toorder1; ?></span></div>

              <?php } else { ?>
              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_cart; ?></span></div>

              <?php } ?>
              <?php } ?>


            </div>

          </div>
        </div>

        <?php } ?>

      </div>

    </div>
  </div>
</section>
<!-- end brand -->
<?php } ?>



<script>
	jQuery(document).ready(function($){
      $(document).on('click','.categories-goods__item .radio-image',function(){
        var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
        var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');

        if(newProdImg){
          $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', newProdImg);
        }else{
          $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', oldProdImg);
        }
      });

	});
</script>
