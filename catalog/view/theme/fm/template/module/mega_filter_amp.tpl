<?php
    $url_filt = $_SERVER['REQUEST_URI'];
    $url_filt=str_replace('/amp','',$url_filt);

    if(strpos($url_filt, "?"))
    {
        $str = strpos($url_filt, "?");
        $url_filt = substr($url_filt, 0, $str);
    }

    if(substr($url_filt, -1) == '/')
    {
        $url_filt  = substr($url_filt, 0, -1);
    }

    $endSeoUrl = end(explode( "/", $url_filt));

    if(strpos($endSeoUrl ,'_') or strpos($_SERVER['REQUEST_URI'] ,'?mfp='))
    {
        $uri = str_replace('/'.$endSeoUrl, '',$url_filt);
        $no_filter = true;
    }else{
        $uri = $url_filt;
        $no_filter = false;
    }
?>

    <?php foreach( $filters as $kfilter => $filter ) { ?>
    <div class="filter">
        <div class="name"><?php echo $filter['name']; ?></div>
        <ul>
            <?php foreach( $filter['options'] as $option_id => $option ) { ?>
                <li <?php echo ! empty( $params[$filter['seo_name']] ) && in_array( $option['value'], $params[$filter['seo_name']] ) ? ' class="checked"' : ''; ?> ><a href="<?=$uri.'/'.$filter['filter_group_url'].'_'.$option['filter_group_url'].'/';?>" class="link-filter"><?=$option['name'];?><span></span></a></li>
            <?php } ?>
        </ul>
    </div>
    <?php } ?>


