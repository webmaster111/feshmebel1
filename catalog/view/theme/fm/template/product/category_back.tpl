<?php $_SESSION['URI'] = '//'.$_SERVER['SERVER_NAME'].'/'.$_SERVER['REQUEST_URI']; ?>
<?php echo $header; ?>
<?php $img_url = ''; ?>
<?php
$min_price = 9999999999999999;
$max_price = 0;
?>
<script>
    gtag('event', 'page_view', {
        'send_to': 'AW-971613570',
            'ecomm_prodid' : <?php echo $products_id; ?>,
    'ecomm_pagetype': 'category',

    });
</script>

<?php
	if ($min_price_prods && $max_price_prods){
		echo "<!--dg_min_cat_price:".$min_price_prods.";;dg_max_cat_price:".$max_price_prods."-->";
	}
	if ($arr_lower_price_prods){
		foreach ($arr_lower_price_prods as $single_lower_price_prod) {
			echo "<!--dg_lower_price_prod_h1:".$single_lower_price_prod['h1'].";;dg_lower_price_prod_url:".$single_lower_price_prod['url']."-->";
		}
	}
	if ($arr_random_prods_of_cat){
		foreach ($arr_random_prods_of_cat as $single_lower_price_prod) {
			echo "<!--dg_random_cat_prod_h1:".$single_lower_price_prod['h1'].";;dg_random_cat_prod_url:".$single_lower_price_prod['url']."-->";
		}
	}
?>
<style>

	@font-face {
		font-family: 'GothamProRegular';
		src: url("/new-site/fonts/GothamProRegular/GothamPro.woff2") format("woff2"), url("/new-site/fonts/GothamProRegular/GothamPro.woff") format("woff");
		font-weight: normal;
		font-style: normal;
	}

	@font-face {
		font-family: 'GothamProLight';
		src: url("/new-site/fonts/GothamProLight/GothamPro-Light.woff2") format("woff2"), url("/new-site/fonts/GothamProLight/GothamPro-Light.woff") format("woff");
		font-weight: normal;
		font-style: normal;
	}

	@font-face {
		font-family: 'GothamProMedium';
		src: url("/new-site/fonts/GothamProMedium/GothamPro-Medium.woff2") format("woff2"), url("/new-site/fonts/GothamProMedium/GothamPro-Medium.woff") format("woff");
		font-weight: normal;
		font-style: normal;
	}

	@font-face {
		font-family: 'GothamProBold';
		src: url("/new-site/fonts/GothamProBold/GothamPro-Bold.woff2") format("woff2"), url("/new-site/fonts/GothamProBold/GothamPro-Bold.woff") format("woff");
		font-weight: normal;
		font-style: normal;
	}
	
	h3, .h3 {
		font-family: "GothamProMedium", sans-serif;
		font-size: 17px;
		line-height: 22px;
		color: #121726;
	}
	img {
		max-width: 100%;
	}
	.checkbox, .radio
	{
		margin-bottom: 6px;
	}
	.checkbox+.checkbox, .radio+.radio
	{
		margin-top: -3px;
	}
	.wrapper {
		max-width: 1250px;
		width: 100%;
		padding: 0 15px;
		margin: auto;
	}

	.minwrapper {
		max-width: 1090px;
		margin: 0 auto;
	}

	.categories-goods__wish::before, .categories-goods__compare::before, .categories-goods__wish:hover::before, .categories-goods__compare:hover::before, .categories-popups__sort::after, .categories-popups__filter::after {
		background: url("img/sprite.svg") no-repeat;
	}

	.categories-goods__wish::before, .categories-goods__compare::before, .categories-goods__wish:hover::before, .categories-goods__compare:hover::before, .categories-popups__sort::after, .categories-popups__filter::after {
		background: url("/img/sprite.svg") no-repeat;
	}

	.categories-ban__img {
		width: 100%;
	}

	.categories-ban__img img {
		width: 100%;
	}

	@media screen and (max-width: 767px) {
		.categories-ban {
			display: none;
		}
	}

	.breadcrumbs {
		padding-top: 24px;
	}

	.breadcrumbs__items {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-flex-wrap: wrap;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		list-style: none;
		padding: 0;
	}

	.breadcrumbs__item {
		font-size: 12px;
		line-height: 16px;
	}

	.breadcrumbs__item:last-of-type a {
		color: #121726;
		pointer-events: none;
	}

	.breadcrumbs__item:last-of-type::after {
		display: none;
	}

	.breadcrumbs__item::after {
		display: inline-block;
		content: "/";
		font-size: 12px;
		margin: 0 9px 0 7px;
	}

	.breadcrumbs__item a {
		color: #808994;
	}

	@media screen and (max-width: 767px) {
		/*
		.breadcrumbs {
			margin-top: 110px;
		}
		.breadcrumbs.categories {
			margin-top: 141px;
		}

		 */
	}

	.categories-views {
		/*
		padding: 0px 0 30px;
		*/
	}

	.categories-views__head {
		/*
		margin-bottom: 65px;
		*/
		margin-bottom: 40px;
	}

	.categories-views__items {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-flex-wrap: wrap;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
	}

	.categories-views__item {
		width: 31%;
		margin: 0 3.5% 50px 0;
	}
	.categories-views__item img
	{
		border: 1px solid #ddd;
	}
	.categories-views__item:nth-of-type(3n) {
		margin-right: 0;
	}

	.categories-views__link {
		display: inline-block;
		text-decoration: none;
		margin-top: 16px;
		text-align: center;
	}

	@media screen and (max-width: 1199px) {
		.categories-views {
			/*
			padding-top: 25px;
			 */
			padding-top: 0px;
		}
		.categories-views .minwrapper {
			max-width: 992px;
			padding: 0 40px;
		}
		.categories-views__head {
			/*

			margin-bottom: 50px;
			*/
			margin-bottom: 20px;
		}
	}

	@media screen and (max-width: 991px) {
		.categories-views .minwrapper {
			padding: 0;
		}
	}

	@media screen and (max-width: 767px) {
		.categories-views {
			/*
			padding-top: 15px;
			 */
			padding-top: 0px;
		}
	}

	@media screen and (max-width: 575px) {
		.categories-views__item {
			width: 46.7%;
			margin-right: 6%;
		}
		.categories-views__item:nth-of-type(3n) {
			margin-right: 6%;
		}
		.categories-views__item:nth-of-type(2n) {
			margin-right: 0;
		}
	}

	.filter {
		float: left;
		width: 100%;
		width: 23.3%;
		border-top: 1px solid #C8CBD1;
		border-bottom: 1px solid #C8CBD1;
		padding-top: 18px;
		margin-right: -webkit-calc(2 * 0.84%);
		margin-right: calc(2 * 0.84%);
	}

	.filter__heading, .filter__head {
		font-family: "GothamProMedium", sans-serif;
		font-size: 12px;
		line-height: 16px;
		letter-spacing: 0.5px;
		text-transform: uppercase;
		color: #121726;
	}

	.filter__heading {
		margin-bottom: 50px;
		padding: 0 15px;
	}

	.filter__item {
		border-top: 1px solid #C8CBD1;
		padding: 0 15px;
	}

	.filter__head {
		position: relative;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		height: 50px;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
		user-select: none;
	}

	.filter__head::after {
		position: absolute;
		content: "";
		width: 12px;
		height: 2px;
		background-color: #121726;
		right: 0;
	}

	.filter__head::before {
		position: absolute;
		content: "";
		width: 2px;
		height: 12px;
		background-color: #121726;
		right: 5px;
	}

	.filter__head.open::before {
		display: none;
	}

	.filter__content {
		display: none;
		padding-bottom: 24px;
		margin-top: 10px;
	}

	.filter__input-range {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		margin-bottom: 23px;
	}

	.filter__input-range div {
		width: 14px;
		height: 1px;
		background-color: #121726;
		margin: 0 16px;
	}

	.filter-range-min, .filter-range-max {
		width: 98px;
		height: 32px;
		text-align: center;
		color: #121726;
		border: 1px solid #C8CBD1;
	}

	.filter__range {
		max-width: 222px;
		margin: 0 auto 8px;
	}

	.filter__range.ui-widget.ui-widget-content {
		border: none;
		height: 2px;
		background-color: #C8CBD1;
		-webkit-border-radius: none;
		border-radius: none;
	}

	.filter__range.ui-slider-horizontal .ui-slider-range {
		background-color: #121726;
	}

	.filter__range .ui-slider-handle.ui-corner-all.ui-state-default {
		width: 20px;
		height: 20px;
		background-color: #f9f9f9;
		border: 1px solid #121726;
		-webkit-border-radius: 0;
		border-radius: 0;
		top: -9.5px;
		margin-left: -10px;
	}

	.filter__color-items {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-flex-wrap: wrap;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		margin-top: 15px;
	}

	.filter__color-item {
		width: 30%;
		text-align: center;
		margin: 0 1px 23px;
	}

	.filter__color-img {
		margin-bottom: 15px;
	}

	.filter__color-name {
		font-size: 12px;
		line-height: 16px;
		color: #121726;
	}

	.filter__check-item {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		margin-bottom: 16px;
	}

	.filter__check-item:last-of-type {
		margin-bottom: 0;
	}

	.filter__check-item input[type="checkbox"]:checked + label:before,
	.filter__check-item input[type="checkbox"]:not(:checked) + label:before {
		width: 24px;
		height: 24px;
		border-color: #C8CBD1;
		top: -4px;
	}

	.filter__check-item input[type="checkbox"]:checked + label:after,
	.filter__check-item input[type="checkbox"]:not(:checked) + label:after {
		width: 16px;
		height: 16px;
		background-color: #C8CBD1;
		top: 0px;
		left: 4px;
	}

	.filter__check-item label {
		font-size: 12px;
		line-height: 16px;
		color: #121726;
		padding-left: 40px !important;
		cursor: pointer;
	}

	.filter__count-item {
		font-size: 12px;
		line-height: 16px;
		color: #808994;
	}

	.filter-sort {
		margin-bottom: 40px;
	}

	.filter-sort__select-block {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: end;
		-webkit-justify-content: flex-end;
		-ms-flex-pack: end;
		justify-content: flex-end;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
	}

	.filter-sort__heading {
		font-family: "GothamProMedium", sans-serif;
		font-size: 12px;
		line-height: 16px;
		letter-spacing: 0.5px;
		text-transform: uppercase;
		margin-right: 12px;
	}

	.filter-sort__select {
		border-right: none;
		border-left: none;
	}

	.filter-sort__select .jq-selectbox__dropdown {
		margin-top: 0;
	}

	.filter-sort__items-block {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		margin-top: 42px;
	}

	.filter-sort__items {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		margin-left: 1%;
	}

	.filter-sort__item {
		background: rgba(200, 203, 209, 0.25);
		margin-right: 16px;
	}

	.filter-sort__item:last-of-type {
		margin-right: 0;
	}

	.filter-sort__item-text {
		font-size: 11px;
		color: #535F6E;
		padding: 4.5px 6px 4.5px 12px;
	}

	.filter-sort__item-text:hover {
		color: #121726;
		cursor: pointer;
	}

	.filter-sort__item-text span {
		color: #121726;
	}

	.filter-sort__item-del {
		position: relative;
		display: inline;
		margin-left: 22px;
	}

	.filter-sort__item-del::before {
		position: absolute;
		content: "";
		background: url("../img/svg/close-hover.svg") no-repeat center;
		width: 10px;
		height: 10px;
		top: 50%;
		right: 0;
		margin-top: -5px;
		-webkit-background-size: contain;
		background-size: contain;
	}

	.filter-sort__item-delete {
		background-color: transparent;
		border: 1px solid #535F6E;
	}

	@media screen and (max-width: 991px) {
		.filter__head {
			font-size: 11px;
		}
	}

	@media screen and (max-width: 767px) {
		.filter-sort {
			display: none;
		}
	}

	.categories-goods {
		padding: 80px 0 20px;
	}

	.categories-goods.subcat {
		padding-top: 30px;
	}

	.categories-goods.subcat h1 {
		margin-bottom: 70px;
	}

	.categories-goods .wrapper {
		max-width: 1200px;
	}

	.categories-goods__item {
		position: relative;
		width: 23.1%;
		float: left;
		background-color: #fff;
		padding: 44px 14px 20px;
		margin: 0 0.84% 30px 0.84%;
	}
	.categories-goods__item .control-label
	{
		display: none;
	}
	.categories-goods__rating
	{
		min-height: 30px;
	}
	.categories-goods__item  .form-group .wrap-select-colors
	{
		height: 79px;
		overflow-y: auto;
	}
	.categories-goods__item .categories-goods__link
	{
		min-height: 40px;
	}
	.categories-goods__label {
		position: absolute;
		width: 115px;
		height: 24px;
		font-family: "GothamProMedium", serif;
		font-size: 11px;
		line-height: 11px;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		text-align: center;
		color: #FFFFFF;
		top: 10px;
		left: 14px;
		background: #43BE58;
	}

	.categories-goods__delivery {
		position: absolute;
		background: url("../img/svg/icon-delivery.svg") no-repeat center;
		width: 21px;
		height: 16px;
		top: 14px;
		right: 14px;
	}

	.categories-goods__rating {
		margin: 5px 0;
		border-bottom: 1px solid #121726;
		padding-bottom: 5px;
	}

	.categories-goods__image img {
		width: 100%;
	}

	.categories-goods__price {
		margin-top: 16px;
	}

	.categories-goods__link a {
		font-family: "GothamProMedium", serif;
		font-size: 12px;
		line-height: 16px;
		color: #121726;
		text-decoration: none;
	}

	.categories-goods__price-block {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: end;
		-webkit-justify-content: flex-end;
		-ms-flex-pack: end;
		justify-content: flex-end;
		-webkit-box-align: end;
		-webkit-align-items: flex-end;
		-ms-flex-align: end;
		align-items: flex-end;
		/*
		margin-top: 10px;*/
	}

	.categories-goods__discount {
		font-family: "GothamProBold", serif;
		font-size: 13px;
		line-height: 18px;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		text-align: right;
		color: #FF3B30;
	}

	.categories-goods__before-discount {
		font-size: 13px;
		line-height: 18px;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		text-align: right;
		-webkit-text-decoration-line: line-through;
		text-decoration-line: line-through;
		color: #808994;
	}

	.categories-goods__new-price {
		font-family: "GothamProBold", serif;
		font-size: 22px;
		line-height: 21px;
		color: #121726;
		margin-left: 10px;
	}

	.categories-goods__new-price span {
		font-family: "GothamProRegular", serif;
	}

	.categories-goods__actions {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		margin-top: 28px;
	}

	.categories-goods__wish, .categories-goods__compare {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 57px;
		height: 42px;
		border: 1px solid #121726;
	}

	.categories-goods__wish::before, .categories-goods__compare::before {
		position: absolute;
		content: "";
		background-position: 99.55555555555556% 34.64912280701754%;
		width: 20px;
		height: 17px;
	}

	.categories-goods__wish:hover, .categories-goods__compare:hover {
		cursor: pointer;
	}

	.categories-goods__wish:hover::before, .categories-goods__compare:hover::before {
		background-position: 99.55555555555556% 42.10526315789474%;
		width: 20px;
		height: 17px;
	}

	.categories-goods__compare {
		margin-left: -1px;
	}

	.categories-goods__compare::before {
		position: absolute;
		content: "";
		background-position: 78.73303167420815% 52.63157894736842%;
		width: 24px;
		height: 17px;
	}

	.categories-goods__compare:hover::before {
		background-position: 89.59276018099547% 52.63157894736842%;
		width: 24px;
		height: 17px;
	}

	.categories-goods__buy {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 173px;
		height: 42px;
		color: #fff;
		background-color: #121726;
	}

	.categories-goods__buy:hover {
		background-color: #535F6E;
		cursor: pointer;
	}

	.categories-goods__buy span {
		font-family: "GothamProMedium", serif;
		font-size: 11px;
		line-height: 11px;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		text-align: center;
		letter-spacing: 1px;
		text-transform: uppercase;
	}

	.categories-goods__buy span::after {
		display: inline-block;
		content: "";
		width: 19px;
		height: 18px;
		background: url("../img/svg/icon-basket.svg") no-repeat center;
		margin-left: 12px;
	}

	.categories-goods__colors {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		margin-top: 15px;
	}

	.categories-goods__color {
		width: 36px;
		height: 36px;
		border: 1px solid transparent;
		padding: 4px;
	}

	.categories-goods__color:hover {
		border-color: #121726;
	}

	@media screen and (max-width: 1199px) {
		.categories-goods__filter-block {
			max-width: 226px;
			width: 23.3%;
			margin-right: 17px;
		}
		.categories-goods__item {
			width: 23.3%;
		}
	}

	@media screen and (max-width: 991px) {
		.categories-goods__filter-block {
			max-width: unset;
			width: 29.3%;
			margin-right: 4%;
		}
		.categories-goods__item {
			width: 29.3%;
			margin: 0 2% 30px 2%;
		}
	}

	@media screen and (max-width: 767px) {
		.categories-goods__filter-block {
			display: none;
		}
		.categories-goods__items {
			margin: 0 -10px;
		}
		.categories-goods__label {
			width: 85px;
			top: 9px;
			left: 9px;
		}
		.categories-goods__delivery-fitting, .categories-goods__credit {
			right: 9px;
		}
		.categories-goods__link {
			font-size: 12px;
		}
		.categories-goods__price-block {
			-webkit-flex-wrap: wrap;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
		}
		.categories-goods__old-price {
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-pack: end;
			-webkit-justify-content: flex-end;
			-ms-flex-pack: end;
			justify-content: flex-end;
			width: 100%;
		}
		.categories-goods__discount {
			margin-right: 9px;
		}
		.categories-goods__new-price {
			font-size: 20px;
		}
		.categories-goods__actions {
			margin-top: 14px;
		}
		.categories-goods__buy {
			width: 33.33%;
		}
		.categories-goods__buy span {
			font-size: 0;
		}
		.categories-goods__buy span::after {
			margin: 0 0 0 -6px;
		}
	}

	@media screen and (max-width: 575px) {
		.categories-goods__item .form-group .wrap-select-colors
		{
			height: auto;
		}
		.categories-goods__item {
			width: auto;
			margin: 0 10px 40px;
			padding: 44px 7px 20px;
		}
		.categories-goods__label {
			width: 100px;
		}
		.categories-goods__delivery-fitting, .categories-goods__credit {
			right: 7px;
		}
		.categories-goods__buy {
			width: 40%;
		}
	}

	.categories-popups {
		position: absolute;
		width: 100%;
		top: 96px;
		background-color: #f9f9f9;
		z-index: 98;
		display: none;
	}

	.categories-popups__items {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		border-top: 1px solid #C8CBD1;
		-webkit-box-shadow: 0px 4px 8px rgba(83, 95, 110, 0.24);
		box-shadow: 0px 4px 8px rgba(83, 95, 110, 0.24);
	}

	.categories-popups__item {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 50%;
		min-height: 56px;
	}

	.categories-popups__item span {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		font-family: "GothamProMedium", sans-serif;
		font-size: 12px;
		line-height: 16px;
		letter-spacing: 0.5px;
		text-transform: uppercase;
	}

	.categories-popups__sort::after {
		display: inline-block;
		content: "";
		background-position: 67.57990867579909% 54.29864253393665%;
		width: 26px;
		height: 24px;
		margin-left: 16px;
	}

	.categories-popups__filter::after {
		display: inline-block;
		content: "";
		background-position: 44.34389140271493% 55.29953917050691%;
		width: 24px;
		height: 28px;
		margin-left: 16px;
	}

	@media screen and (max-width: 767px) {
		.categories-popups {
			display: block;
		}
	}

	.pagination {
		display: inline-block;
		width: 100%;
		padding: 20px 0;
	}

	.pagination__items {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: end;
		-webkit-justify-content: flex-end;
		-ms-flex-pack: end;
		justify-content: flex-end;
		list-style: none;
	}

	.pagination__item {
		margin: 0 7px;
	}

	.pagination__item a {
		color: #808994;
		text-decoration: underline;
		padding: 5px;
	}

	.pagination__item.active a {
		color: #121726;
	}

	.pagination__item:last-of-type {
		margin-right: 0;
	}

	.pagination__next a {
		position: relative;
		display: inline-block;
		content: "";
		width: 20px;
		height: 13px;
		background: url("../img/svg/paginations-next.svg") no-repeat center;
		top: 3px;
	}

	.pagination.search-result {
		margin-top: -15px;
		margin-bottom: 125px;
	}

	.marks {
		display: inline-block;
		width: 100%;
		padding: 70px 0 50px;
	}

	.marks__items {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-flex-wrap: wrap;
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
	}

	.marks__item {
		margin: 0 45px 16px 0;
	}

	.marks__link {
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		line-height: 24px;
		color: #535F6E;
	}

	.marks__link:hover {
		text-decoration: none;
		color: #535F6E;
	}

	.marks__link::before {
		display: inline-block;
		content: "";
		width: 20px;
		height: 20px;
		background: url("../img/svg/marks.svg") no-repeat center;
		margin-right: 11px;
	}

</style>

<?php if ($thumb_banner) { ?>
<!-- categories-ban -->
<section class="categories-ban">
	<div class="wrapper">
		<div class="categories-ban__img"><img src="<?php echo $thumb_banner; ?>" alt="<?php echo $heading_title; ?>"></div>
	</div>
</section>
<!-- end categories-ban -->
<?php } ?>

<!-- breadcrumbs -->
<section class="breadcrumbs ">
	<div class="wrapper">
		<ul class="breadcrumbs__items" itemscope itemtype="https://schema.org/BreadcrumbList">

			<?php $breadcrumbs_item = 0;
			 foreach ($breadcrumbs as $breadcrumb) { ?>
				<li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
					<a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
					<span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
					<meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a>
				</li>
			<?php } ?>
			<!--ss_breadcrums_list:<?php foreach ($breadcrumbs as $k=>$breadcrumb) { ?><?php echo $breadcrumb['text']; ?><?php if(count($breadcrumb) != $k) { ?> >> <?php } ?><?php } ?>-->
		</ul>
	</div>
</section>
<!-- end breadcrumbs -->

<!-- categories-views -->
<section class="categories-views">
	<div class="wrapper">
		<h1 class="categories-views__head h1"><?php echo $heading_title; ?></h1>
		<?php if ($categories) { ?>
		<div class="minwrapper">
			<div class="categories-views__items">
				<?php foreach ($categories as $category) { ?>
				<div class="categories-views__item">
					<a href="<?php echo $category['href']; ?>" class="categories-views__img-link"><img class="categories-views__img" src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a>
					<a href="<?php echo $category['href']; ?>" class="categories-views__link h3"><?php echo $category['name']; ?></a>
				</div>
				<?php } ?>
			</div>
		</div>
		<?php } ?>
	</div>
</section>
<!-- end	categories-views -->


<!-- categories-goods -->
<section class="categories-goods" style="display: none;">
	<div class="wrapper">
		<div class="categories-goods__block">
			<?php if ($products) { ?>
				<div class="categories-goods__items">
				<?php foreach ($products as $product) { ?>

						<div class="categories-goods__item">
							<div class="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo (!empty( $product['special']) ?  $product['special'] : $product['price'] ); ?></div>
							<div class="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
							<div class="categories-goods__information">
								<?php if($product['sale_status_image']){ ?>
									<div class="categories-goods__label" style="background-color: #<?=$product['sale_status_color'];?>;" ><?=$product['sale_status_name'];?></div>
								<?php } ?>
								<div class="categories-goods__image">
									<a href="<?php echo $product['href']; ?>">
										<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
									</a>
								</div>
								<div class="categories-goods__rating">
									<?php if ($product['rating']) { ?>
									<div class="rating">
										<?php for ($i = 1; $i <= 5; $i++) { ?>
										<?php if ($product['rating'] < $i) { ?>
										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
										<?php } else { ?>
										<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
										<?php } ?>
										<?php } ?>
									</div>
									<?php } ?>
								</div>
							</div>
							<div class="categories-goods__price">
								<div class="categories-goods__link"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
								<?php if ($product['price']) { ?>
									<div class="categories-goods__price-block">
										<?php if (!$product['special']) { ?>
											<?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
												<div class="categories-goods__old-price">
													<!-- div class="categories-goods__discount">-3001</div -->
													<div class="categories-goods__before-discount"><?php echo ceil($product['old_price']).' грн.'; ?></div>
												</div>
											<?php } ?>
											<div class="categories-goods__new-price"><?php echo $product['price']; ?></div>
										<?php } else { ?>
											<div class="categories-goods__old-price">
												<!-- div class="categories-goods__discount">-3001</div -->
												<div class="categories-goods__before-discount"><?php echo $product['price']; ?></div>
											</div>
											<div class="categories-goods__new-price"><?php echo $product['special']; ?></div>
										<?php } ?>
									</div>
								<?php } ?>
								<div class="categories-goods__options">

									<?php
										//start Tkach web-promo
										$option_nomer = '';
										$select_nomer = '';
										// end

										uasort($product['options'], function($a, $b){
											if ($a['option_id'] === $b['option_id']) return 0;
											return $a['option_id'] < $b['option_id'] ? -1 : 1;
											}
										);

										$is_sortoption_id = 0;

										foreach($product['options'] as $val){
											if($val['sort_option']){
												$is_sortoption_id = $val['option_id'];
												break;
											}
										}

										?>
										<?php foreach ($product['options'] as $option) { ?>
											<?php if (!$is_sortoption_id) { ?>
												<?php if ($option['type'] == 'select') { ?>
													<?php
													// start Tkach web-promo
													if (!isset($select_nomer) or empty($select_nomer)){
														$select_nomer = trim($option['product_option_id']);
													}else{
														$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
													}
													// end
													?>
													<div class="form-group">
														<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
														<div class="varSelectWrap">
															<select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
																<option value=""><?php echo $text_select; ?></option>
																<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
																	<option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
																		<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																		<?php } ?>
																	</option>
																<?php } ?>
															</select>
														</div>
													</div>
												<?php } ?>


												<?php if ($option['type'] == 'image') { ?>
													<?php
													// start Tkach web-promo
													if (!isset($option_nomer) or empty($option_nomer)){
														$option_nomer = $option['product_option_id'];
													}else{
														$option_nomer = $option_nomer .', '.$option['product_option_id'];
													}
													// end
													?>
													<div class="form-group">
														<label class="control-label"><?php echo $option['name']; ?></label>
														<div class="select-colors">
															<div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
																<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
																	<div class="radio radio-image">
																		<label title="<?php echo $option_value['name'] ?>" class="change-price">
																			<input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
																			<img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
																			<span class="hidden-descr"><?php echo $option_value['name']; ?>
																				<?php if ($option_value['price']) { ?>
																					(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																				<?php } ?>
																			</span>
																		</label>
																	</div>
																<?php } ?>
															</div>
														</div>
														<div class="show-more-box">
															<?php if($curr_lang == 'ru'){ ?>
																<a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
															<?php }else{ ?>
																<a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
															<?php } ?>
														</div>

													</div>
												<?php } ?>

												<?php break; ?>
											<?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
													<?php if ($option['type'] == 'select') { ?>
														<?php
															// start Tkach web-promo
															if (!isset($select_nomer) or empty($select_nomer)){
																$select_nomer = trim($option['product_option_id']);
															}else{
																$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
															}
															// end
														?>
														<div class="form-group">
															<label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
															<div class="varSelectWrap">
																<select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
																	<option value=""><?php echo $text_select; ?></option>
																	<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
																		<option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																				(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</option>
																	<?php } ?>
																</select>
															</div>
														</div>
													<?php } ?>


												<?php if ($option['type'] == 'image') { ?>
													<?php
														// start Tkach web-promo
														if (!isset($option_nomer) or empty($option_nomer)){
															$option_nomer = $option['product_option_id'];
														}else{
															$option_nomer = $option_nomer .', '.$option['product_option_id'];
														}
														// end
													?>
													<div class="form-group">
														<label class="control-label"><?php echo $option['name']; ?></label>
														<div class="select-colors">
															<div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
																<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
																	<div class="radio radio-image">
																		<label title="<?php echo $option_value['name'] ?>" class="change-price">
																			<input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
																			<img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																				(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?></span>
																		</label>
																	</div>
																<?php } ?>
															</div>
														</div>
														<div class="show-more-box">
															<?php if($curr_lang == 'ru'){ ?>
																<a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
															<?php }else{ ?>
																<a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
															<?php } ?>
														</div>
													</div>
											<?php } ?>
										<?php } ?>
									<?php } ?>

								</div>
								<div class="categories-goods__actions">
									<div class="categories-goods__wish" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></div>
									<div class="categories-goods__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"></div>
									<div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span>купить</span></div>
								</div>
								<!-- div class="categories-goods__colors-block">
									<div class="categories-goods__colors">
										<div class="categories-goods__color">
											<div class="categories-goods__color-img"><img src="img/svg/bg-item-product.svg" alt=""></div>
										</div>
										<div class="categories-goods__color">
											<div class="categories-goods__color-img"><img src="img/svg/bg-item-product.svg" alt=""></div>
										</div>
										<div class="categories-goods__color">
											<div class="categories-goods__color-img"><img src="img/svg/bg-item-product.svg" alt=""></div>
										</div>
									</div>
								</div -->
							</div>
						</div>

				<?php } ?>
				</div>
			<?php } ?>

		</div>
	</div>
</section>

<div class="container">


  <header class="category-header row">
  
  <meta property="og:type" content="website" />
			<meta property="og:title" content="<?=$heading_title;?>" />
			<meta property="og:url" content="<?='https://feshmebel.com.ua'.$_SERVER['REQUEST_URI']?>" />
			<meta property="og:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
			<meta property="article:author" content="https://www.facebook.com/feshemebel/" />
			<meta property="twitter:card" content="summary" />
			<meta property="twitter:title" content="<?=$heading_title;?>" />
			<meta property="twitter:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
			<meta property="og:image" content="<?=$products[0]['thumb']?>" />
			<meta property="twitter:image" content="<?=$products[0]['thumb']?>" />
			<meta property="og:publisher" content="https://www.facebook.com/feshemebel/" />
			<meta property="og:site_name" content="Интернет магазин мебели Фешемебельный" />
  
	  <!-- h1 class="category-title bordered-title col-sm-5"><?php echo $heading_title; ?></h1 -->
	  <!--ss_category_name:<?php echo $heading_title; ?>-->
	  <?php if(!isset($_GET['sort']) && !$noindex ){ ?>

	  <?php
	  	$f=0;
	  	$heading_title_tmp = explode(': ',$heading_title);
	  	if(isset($heading_title_tmp[1]))
	  	{
	  	$f++;
	  		$heading_title_tmp = explode(', ',$heading_title_tmp[1]);
	  		foreach($heading_title_tmp as $item)
	  		{ ?>
	  			<!--ss_selected_filters_info|<?php echo str_replace( ' - ','|',$item); ?>-->
	  		<?php
	  		}
	  	}
	  ?>

	  <?php
	  	$heading_title_tmp = explode(' (',$heading_title);
	  	if(isset($heading_title_tmp[1]))
	  	{
	  		$f++;
	  		$heading_title_tmp = explode(')',$heading_title_tmp[1]);


	  		 ?>
	  <!--ss_selected_filters_info|<?php echo str_replace( ' - ','|',$heading_title_tmp[0]); ?>-->
	  <?php

	  	}

	  ?>
	  <?php if($f){ ?>
	  <!--seoshield_formulas--fil-traciya-->
	  <?php } ?>
	  <?php } ?>
	  <?php if ($products) { ?>

  </header>

  <div class="row">
		<?php echo $column_left;?>


    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="col-md-9 col-sm-8"><?php echo $content_top; ?>

     <!--  <?php echo $class; ?> -->
		<style>
			.category-product-list
			{
				margin: 0;
			}
			.product-list .product-layout
			{
				position: relative;
			}
			.privat
			{
				position: absolute;
				right: 0;
				z-index: 1;
			}
			.privat img
			{
				height: 40px;
				width: 40px;
				margin-left: -50px;
				margin-top: 10px;
			}
			.show-more-btn
			{
				cursor: pointer;
				display: inline-block;

				padding: 10px 20px;
				color: #2d4059;
				border: 1px solid #2d4059;
				background-color: transparent;
				text-transform: uppercase;
				font-size: 10px;

				line-height: 10px;
				margin: 5px auto;
				transition: background-color .5s ease,color .5s ease;
			}
			.show-more-text
			{
				cursor: pointer;
				display: inline-block;

				padding: 10px 20px;
				color: #2d4059;
				border: 1px solid #2d4059;
				background-color: transparent;
				text-transform: uppercase;
				font-size: 10px;

				line-height: 10px;
				margin: 5px auto;
				transition: background-color .5s ease,color .5s ease;
				margin-top: 20px;
			}
			@media all and (min-width: 611px) {
				.privat {

					left: 0px;
				}
				.product-layout .privat img.bestseller
				{
					position: relative;
					float: right;
					margin: 0;
					padding: 0;
				}
				.product-layout .privat img.sale_status
				{
					margin: 0;
					padding: 0;
					position: relative;
					z-index: 1;
					width: 100px;
					height: inherit;
					top: 0px;
				}
			}
			.btn-show-filter-box
			{
				display: none;
			}
			@media all and (max-width: 610px) {
				#content,
				#content *
				{
					padding: 0;
					margin: 0;
				}
				#content .faq-content__answer-text
				{
					padding: 10px;
				}

				#content .stagh2
				{
					margin-top: 10px;
					margin-bottom: 10px;
				}

				#content .categories-title
				{
					padding-bottom: 10px;
				}
				#content button.button-cart-p
				{
					width: 100%;
					padding: 10px;
				}

				#content  .btn-show-filter-box
				{
					display: block;
				}
				#content .btn-show-filter
				{
					background: linear-gradient(to bottom, #FFD015 0%, #F9B706 100%);
					text-align: center;
					display: block;
					width: 100%;
					padding: 5px 0 ;
					font-weight: bold;
				}
				#content .btn-show-filter svg {
					margin-right: 10px;
					display: inline-block;
					width: 16px;
					height: 16px;
					vertical-align: middle;
				}
				.product-thumb .caption .old-h4
				{
					height: 36px;
					font-size: 14px;
					text-align: center;
					overflow: hidden;
				}
				.general_img .thumbnail .sale_status, .product-thumb .image-home .sale_status, .product-thumb .image .sale_status,
				.privat,
				img.recommended, img.bestseller
				{
					position: relative;
				}
				.privat
				{
					height: 30px;
				}
				#content .product-layout .sale_status
				{
					padding: 5px;

				}
				.privat
				{

					display: block;
				}
				.product-list .product-thumb
				{
					display: block;
				}
				.category-list-item
				{
					margin: 0;
					width: 25%;
				}
				.product-list .product-layout
				{
					width: 50%;
					margin: 0;
					display: inline-block;
					padding: 5px;
				}
				#content .product-list .product-layout
				{
					padding: 5px;
				}
				#content .product-layout .form-group .show-more-box
				{
					height: 30px;
				}
				#content .product-layout .select-colors
				{
					height: 30px;
				}
				.rating-box
				{
					height: 20px;
				}
				#content .product-layout .form-group .show-more
				{
					float: none;
					display: block;
					padding: 10px;
					text-align: center;

				}
				#content .product-thumb .price {
					color: #2d4057;
				}

				#content .show-more-btn
				{
					padding: 10px;
					display: inline-block;
					width: calc(100% - 20px);
				}
				#content .pagination>li>a, #content .pagination>li>span
				{
					padding: 10px;
				}
				#content .category-select-group .category-select label
				{
					text-align: center;
					display: block;
					width: 100%;
				}
				#content .category-select-group .category-select
				{
					width: 50%;
					text-align: center;
				}
				#content .wish-buttons-card button
				{
					padding: 10px 0;
					margin: 0;
					width: 48%;
					text-align: center;
				}
				.product-layout .privat img.sale_status
				{
					width: calc(100% - 44px);
					height: inherit;
				}
				.product-layout .privat img.bestseller
				{
					float: right;
				}
				.product-thumb .price
				{
					font-size: 12px!important;
				}
				.product-thumb .price .listing_text_price
				{
					font-size: 11px!important;
				}
				.product-thumb .price #oldPrice
				{
					font-size: 12px!important;
				}
			}

		</style>

		<div class="row">
			<div class="col-sm-12">
				<div class="category-select-group">
					<div class="category-select">
						<label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
						<select id="input-sort" class="form-control" onchange="location = this.value;">
							<?php foreach ($sorts as $sorts) { ?>
							<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
							<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</div>

					<div class="category-select hidden-xs">
						<label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
						<select id="input-limit" class="form-control" onchange="location = this.value;">
							<?php foreach ($limits as $limits) { ?>
							<?php if ($limits['value'] == $limit) { ?>
							<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
							<?php } ?>
							<?php } ?>
						</select>
					</div>
					<div class="category-select btn-show-filter-box">
						<label class="control-label" for="input-limit">&nbsp;</label>
						<div class="btn-show-filter">
							<svg viewBox="0 0 512 512"><path d="m191.8,480.6v-174.3l-177.3-263.5c-7.2-8.3-4.1-31.1 16.9-31.8h449.2c12.2,0 27.7,14.4 16.9,31.8l-177.3,263.5v108.2c0,6.4-3,12.4-8.1,16.3l-87.6,66.1c-5,5.6-30.1,9.3-32.7-16.3zm-122-428.8l159.4,236.9c2.3,3.4 3.5,7.3 3.5,11.4v139.6l46.8-35.3v-104.3c0-4.1 1.2-8 3.5-11.4l159.2-236.9h-372.4z"></path></svg>
							Фильтры
						</div>
					</div>
				</div>
			</div>
		</div>

      <div class="row product-list category-product-list">
	<!--isset_listing_page-->
				<?php $num = 0; ?>
        <?php $top = 0; ?>
        <?php foreach ($products as $product) {

					if((int)$product['price'] > $max_price )
					{
						$max_price = (int)$product['price'];
					}
					if((int)$product['price'] < $min_price  )
					{
						$min_price = (int)$product['price'];
					}
		?>

        <div class="product-layout">
		<!--product_in_listingEX-->
            <div class="lable">
                <?php if($product['bestseller']){ ?>
                <img class="bestseller" src="image/catalog/lable/bestseller.png?v=1.0" alt="bestseller">
                <?php } ?>
                <?php if($product['recommended']){ ?>
                <img class="recommended" src="image/catalog/lable/recommended.png?v=1.0" alt="recommended">
                <?php } ?>
				<div class=" privat">
					<?php if($product['privat']){ ?>
					<img class="bestseller" src="/catalog/view/javascript/jquery/pp_calculator/img/pp_logo.png" >
					<?php } ?>
					<?php if($product['sale_status_image']){ ?>
					<span class="sale_status" style="background-color: #<?=$product['sale_status_color'];?>;"><?=$product['sale_status_name'];?></span>
					<?php } ?>
				</div>


            </div>

<!--
<div  style="position:absolute;">
<div id="triangle-topleft">
<div class="rotatedBlock-100">
<font style="font-size: 15px;">Ожидается</font>
</div>
</div>
</div>
-->

<!--
          <div class="product-thumb" style="position:absolute; top:<?php echo $top; ?>px;">
          <div class="product-thumb" style="top:<?php echo $top; ?>px;">
          <div class="product-thumb">
-->
          <div class="product-thumb" id="product<?php echo  $product['product_id']; ?>">
          <!-- start Tkach web-promo Dynamic price -->
          <div id="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo (!empty( $product['special']) ?  $product['special'] : $product['price'] ); ?></div>
          <div id="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
          <!-- end web-promo -->
            <div class="image-home">
				<a href="<?php echo $product['href']; ?>">

					<img <?php if (isset($product['thumb'])){ ?>src="<?php echo $product['thumb']; ?>"<?php }else{ ?><?php } ?> alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
				</a>
			</div>
              <div class="caption">
                <div class="old-h4"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                <p><?php echo $product['description']; ?></p>
				  <div class="rating-box">
					<?php if ($product['rating']) { ?>
					<div class="rating">
					  <?php for ($i = 1; $i <= 5; $i++) { ?>
					  <?php if ($product['rating'] < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } ?>
					  <?php } ?>
					</div>
					<?php } ?>
				  </div>
                <?php if ($product['price']) { ?>

                <p class="price">
                	<?php 
                		if($curr_lang == 'ru'){
                			echo '<span class="listing_text_price">Цена:</span>';
	                	} else {
	                		echo '<span class="listing_text_price">Ціна:</span>';
	                	}
                	?>
                  <?php if (!$product['special']) { ?>
				  <?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
						<span id="oldPrice"><?php echo ceil($product['old_price']).' грн.'; ?></span>
					  <?php } ?>
                  <span class="<?=(isset($product['old_price']) && $product['old_price'] > 0)?"isnew_price":""?> newPrice" ><?php echo $product['price']; ?></span>
                  <?php } else { ?>
					<span id="oldPrice"><?php echo $product['price']; ?></span>
                  <span class="isnew_price newPrice" ><?php echo $product['special']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
              </div>
              <?php 
              //start Tkach web-promo
               $option_nomer = '';
               $select_nomer = '';
              // end
			  
			  uasort($product['options'], function($a, $b){
					if ($a['option_id'] === $b['option_id']) return 0;
					return $a['option_id'] < $b['option_id'] ? -1 : 1;
				});
				
				$is_sortoption_id = 0;
				
				foreach($product['options'] as $val){
					if($val['sort_option']){
						$is_sortoption_id = $val['option_id'];
						break;
					}
				}
			  
              ?>
                <?php foreach ($product['options'] as $option) { ?>
					<?php if (!$is_sortoption_id) { ?>
						<?php if ($option['type'] == 'select') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($select_nomer) or empty($select_nomer)){
								$select_nomer = trim($option['product_option_id']);
							}else{
								$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
							}               
						// end
						?>
						<div class="form-group">
						  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
						  <div class="varSelectWrap">
						  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
							<option value=""><?php echo $text_select; ?></option>
							<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
							<option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?>
							</option>
							<?php } ?>
						  </select>
						  </div>
						</div>
					<?php } ?>
								<!-- <script>
									<?php echo 'var option['.$option['product_option_id'].'] = 0; '; ?>
								</script> -->

					<?php if ($option['type'] == 'image') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($option_nomer) or empty($option_nomer)){
								$option_nomer = $option['product_option_id'];
							}else{
								$option_nomer = $option_nomer .', '.$option['product_option_id'];
							}               
						// end
						?>
					<div class="form-group">
					  <label class="control-label"><?php echo $option['name']; ?></label>
					  <div class="select-colors">
					  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
						<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
						<div class="radio radio-image">
						  <label title="<?php echo $option_value['name'] ?>" class="change-price">
							<input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
							
							<img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?></span>
						  </label>
						</div>
						<?php } ?>
					  </div>
					  </div>
						<div class="show-more-box">
							<?php if($curr_lang == 'ru'){ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
							<?php }else{ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
							<?php } ?>
						</div>

					</div>
					<?php } ?>

					<?php break; ?>
				<?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
					<?php if ($option['type'] == 'select') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($select_nomer) or empty($select_nomer)){
								$select_nomer = trim($option['product_option_id']);
							}else{
								$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
							}               
						// end
						?>
						<div class="form-group">
						  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
						  <div class="varSelectWrap">
						  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
							<option value=""><?php echo $text_select; ?></option>
							<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
							<option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?>
							</option>
							<?php } ?>
						  </select>
						  </div>
						</div>
					<?php } ?>
								<!-- <script>
									<?php echo 'var option['.$option['product_option_id'].'] = 0; '; ?>
								</script> -->

					<?php if ($option['type'] == 'image') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($option_nomer) or empty($option_nomer)){
								$option_nomer = $option['product_option_id'];
							}else{
								$option_nomer = $option_nomer .', '.$option['product_option_id'];
							}               
						// end
						?>
					<div class="form-group">
					  <label class="control-label"><?php echo $option['name']; ?></label>
					  <div class="select-colors">
					  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
						<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
						<div class="radio radio-image">
						  <label title="<?php echo $option_value['name'] ?>" class="change-price">
							<input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
							
							<img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?></span>
						  </label>
						</div>
						<?php } ?>
					  </div>
					  </div>
						<div class="show-more-box">
					  <?php if($curr_lang == 'ru'){ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
						<?php }else{ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
						<?php } ?>
						</div>
					</div>
					<?php } ?>
				<?php } ?>
            <?php } ?>

              <div class="button-group">
                <div class="clearfix wish-buttons-card">
	                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist?></button>
	                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare?></button>
                </div>
								<div class="button-cart-add clearfix">
									<?php if (!empty($product['stock_image'])) { ?>
										<img class="stock_image" src="<?=$product['stock_image'];?>">
									<?php } else { ?>
										<?php if (strpos( $product['stock_status'],'жида') !== false) { ?>
											<button class="button-cart-p" type="button" onclick="" disabled><i class="fa fa-shopping-cart"></i> <span><?php echo $button_expect; ?></span></button>
										<?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>
											<button class="button-cart-p" type="button" onclick="" disabled><i class="fa fa-shopping-cart"></i> <span><?php echo $button_out_of_sale; ?></span></button>
										<?php } elseif (strpos( $product['stock_status'],'Предзаказ') !== false) { ?>
											<button class="button-cart-p" type="button" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_toorder1; ?></span></button>
										<?php } else { ?>
											<button class="button-cart-p" type="button" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span></button>
										<?php } ?>
									<?php } ?>
								</div>
              </div>
<?php //echo '<br>$product[stock_status]='.$product['stock_status']; ?>
<!--
<?php if (strpos( $product['stock_status'],'продажи') >0) { ?>
<div  class="triange-label" style="position:relative; z-index: 10000; top:0; left:0;">
<div id="triangle-topleft" style="border-top: 100px solid #9370DB">
<div class="rotatedBlock-100" style="top: -90px; left: 0px;">
<font style="font-size: 15px;">снято&nbsp;с<br>продажи</font>
</div>
</div>
</div>
<?php } else if (strpos( $product['stock_status'],'жида') >0) { ?>
<div  class="triange-label" style="position:relative; z-index: 10000; top:0; left:0;">
<div id="triangle-topleft">
<div class="rotatedBlock-100">
<font style="font-size: 15px;">Ожидается</font>
</div>
</div>
</div>
<?php } else if (strpos( $product['stock_status'],'заказ') >0) { ?>
<div  class="triange-label" style="position:relative; z-index: 10000; top:0; left:0;">
<div id="triangle-topleft" style="border-top: 100px solid #6B8E23">
<div class="rotatedBlock-100" style="top: -86px; left: 11px;">
<font style="font-size: 15px;">&nbsp;под<br>заказ</font>
</div>
</div>
</div>
<?php } ?>
-->
          </div>
        </div>
        <?php } ?>
      </div>
		<div style="text-align: center;">
			<span class="show-more-btn ">Показать еще</span>
		</div>
      <div class="row">
        <div class="col-sm-12 text-right"><?php echo $pagination; ?></div>
        <!-- <div class="col-sm-6 text-right"><?php echo $results; ?></div> -->
      </div>
      <?php } //------------------------------------ ?>
      <!-- links_block -->
	  
	
      <style>
      	.content_bottom
      	{
      		padding: 0;
      		margin: 0;
      	}
      	.content_bottom .product-layout img
      	{
      		max-width: 100%;
      		display: block;
      	}
		#content .product-reviews
		{
			padding: 10px 0;
			float: left;
		}
		#content .product-review
		{
			padding-bottom: 20px;
			width: calc(100% - 0px);
		}
		.seo_text.short_text {
			max-height: 200px;
			overflow: hidden;
			position: relative;
		}
		@media (max-width: 610px)
		{
			#content .product-review-image
			{
				float: none !important;
				text-align: center;
				margin: 0;
				margin-bottom: 10px;
			}
		}
      </style>
      <div class="content_bottom">
		  <div class="product-reviews">
			  <?php if ($reviews_cat && $page_number<2) { ?>
				  <?php foreach ($reviews_cat as $review) { ?>
				  <div class="product-review">
					  <div class="product-review-image" style="float: left;border: 10px solid #eee;margin-right: 10px;">
						  <a href="<?php echo $review['href']; ?>">
							  <img src="<?php echo $review['thumb']; ?>" alt="<?php echo $review['name']; ?>" title="<?php echo $review['name']; ?>" class="img-responsive" />
						  </a>
					  </div>
					  <div class="">
						  Отзыв к товару: <a href="<?php echo $review['href']; ?>"><?php echo $review['name']; ?></a>


						  <strong class="product-review-name" style="display: inline-block; padding-left: 20px;"><?php echo $review['author']; ?></strong>

					  </div>
					  <div class="product-review-body"><p><?php echo $review['text']; ?></p>

						  <?php if($review['rating']>0){ ?>
						  <div class="product-review-rating" >
							  <?php for ($i = 1; $i <= 5; $i++) { ?>
							  <?php if ($review['rating'] < $i) { ?>
							  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
							  <?php } else { ?>
							  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
							  <?php } ?>
							  <?php } ?>
						  </div>
						  <?php } ?>
					  </div>
					  <div style="clear: both;"></div>

				  </div>
			  		<hr/>
				  <?php } ?>
			  <?php } ?>
			  <div style="clear: both;"></div>
		  </div>
		  <div style="clear: both;"></div>
		  <div class="seo_text short_text">
      	<?php // description
	if(isset($description) and strlen($description) > 100){
    		echo '<!--seo_text_start-->'.$description.'<!--seo_text_end-->';
	}else{ ?>
    		<!--seo_text_start--><!--seo_text_end-->
		  <div class="text_seo">
			  <?php echo $text_seo; ?>
		  </div>
	<?php } ?>

		  </div>


		  <?php if ($articles) { ?>
		  <h2><?php echo $blog_title;?> </h2>
		  <div class="row">
			  <?php foreach ($articles as $article) { ?>
			  <div class="product-layout product-list col-xs-6 col-sm-3">
				  <div>
					  <?php if ($article['thumb']) { ?><div class="image"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div><?php } ?>
					  <div class="caption_new">
						  <div><?php
					if($cuur_lang == 'ru'){
						$_monthsList = array(
						  ".01." => "января",
							  ".02." => "февраля",
							  ".03." => "марта",
							  ".04." => "апреля",
							  ".05." => "мая",
							  ".06." => "июня",
							  ".07." => "июля",
							  ".08." => "августа",
							  ".09." => "сентября",
							  ".10." => "октября",
							  ".11." => "ноября",
							  ".12." => "декабря"
							  );
							  }else{
							  $_monthsList = array(
							  ".01." => "січня",
							  ".02." => "лютого",
							  ".03." => "березня",
							  ".04." => "квітня",
							  ".05." => "травня",
							  ".06." => "червня",
							  ".07." => "липня",
							  ".08." => "серпня",
							  ".09." => "вересня",
							  ".10." => "жовтня",
							  ".11." => "листопада",
							  ".12." => "грудня"
							  );
							  }
							  $currentD = date('d', strtotime($article['date']));
							  $currentM = date('.m.', strtotime($article['date']));
							  $currentY = date('Y', strtotime($article['date']));

							  $currentDate = $currentD . ' ' . $_monthsList[$currentM] . ' ' . $currentY;
							  echo $currentDate;

							  ?></div>
						  <h4><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h4>
						  <p><?php echo $article['preview']; ?></p>

						  <?php if ($article['attributes']) { ?>
						  <h5><?php echo $text_attributes;?></h5>
						  <?php foreach ($article['attributes'] as $attribute_group) { ?>
						  <?php foreach ($attribute_group['attribute'] as $attribute_item) { ?>
						  <b><?php echo $attribute_item['name'];?>:</b> <?php echo $attribute_item['text'];?><br />
						  <?php } ?>
						  <?php } ?>
						  <?php } ?>
					  </div>
					  <div class="text-more">
						  <a href="<?php echo $article['href']; ?>" ><?=$text_more;?></a>
					  </div>
				  </div>
			  </div>
			  <?php } ?>
		  </div>
		  <?php } ?>
		  <?php if(true) { ?>
<style>
	.stagh2
	{
 		font-weight: bold;
		font-size: 24px;
		text-align: center;
		display: block;
		padding-top: 10px;
		margin-bottom: 10px;
	}
	.faq-content__question
	{
		font-weight: bold;
		font-size: 16px;
		padding: 10px 0;
		display: block;
	}
	.faq-content__answer-text
	{
		font-size: 14px;
		display: block;
		padding: 10px 0;
	}
	.td
	{
		display: inline-block;
		width: calc(50% - 20px);
		padding: 10px;
	}

</style>
		  <h2 class="text-center"><?=$faq_title;?> <?php echo mb_strtolower($heading_title,'utf-8'); ?></h2>
		  <div class="faq-content__list" itemscope="" itemtype="https://schema.org/FAQPage">
			  <?php if(count($products_top)) { ?>
			  <div class="faq-content__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
				  <h3 itemprop="name" class="faq-content__question box-row align-center space-between">
					  <?=$faq_title1;?>
				  </h3>
				  <div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"
					   class="faq-content__answer box-col animated fadeIn">
					  <div itemprop="text" class="faq-content__answer-text">
						  <p><?=$faq_sub1;?>:</p>
						  <ul>
							  <?php foreach($products_top as $k=>$product) { ?>
							  <li><a href="<?=$product['href']?>"><?=$product['name']?></a></li>
							  <?php } ?>
						  </ul>
					  </div>
				  </div>
			  </div>
			  <?php } ?>
			  <div class="faq-content__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
				  <h3 itemprop="name" class="faq-content__question box-row align-center space-between">
					  <?=$faq_title2;?>
				  </h3>
				  <div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"
					   class="faq-content__answer box-col animated fadeIn">
					  <div itemprop="text" class="faq-content__answer-text">
						  <p><?=$faq_sub2;?><?=$products_min[0]['price'];?>:</p>
						  <ul>
							  <?php foreach($products_min as $product) { ?>
							  	<li><a href="<?=$product['href']?>"><?=$product['name']?></a></li>
							  <?php } ?>
						  </ul>

					  </div>
				  </div>
			  </div>
			  <?php if(count($products_special)) { ?>
				  <div class="faq-content__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
					  <h3 itemprop="name" class="faq-content__question box-row align-center space-between">
						  <?=$faq_title3;?>
					  </h3>
					  <div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"
						   class="faq-content__answer box-col animated fadeIn">
						  <div itemprop="text" class="faq-content__answer-text">
							  <p><?=$faq_sub3;?>:</p>
							  <ul>
								  <?php foreach($products_special as $k=>$product) { ?>
									<li><a href="<?=$product['href']?>"><?=$product['name']?></a></li>
								  <?php } ?>
							  </ul>
						  </div>
					  </div>
				  </div>
			  <?php } ?>
			  <div class="faq-content__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
				  <h3 itemprop="name" class="faq-content__question box-row align-center space-between">
					  <?=$faq_title4;?>
				  </h3>
				  <div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"
					   class="faq-content__answer box-col animated fadeIn">
					  <div itemprop="text" class="faq-content__answer-text">
						  <p><?=$faq_sub4;?></p>
						  <ul>
							  <?php foreach($products as $k=>$product) { ?>
							  <li><a href="<?=$product['href']?>"><?=$product['name']?></a></li>
							  <?php if($k==2) break; ?>
							  <?php } ?>
						  </ul>

					  </div>
				  </div>
			  </div>
			  <div class="faq-content__item" itemscope="" itemprop="mainEntity" itemtype="https://schema.org/Question">
				  <h3 itemprop="name" class="faq-content__question box-row align-center space-between">
					  <?=$faq_title5;?>
				  </h3>
				  <div itemscope="" itemprop="acceptedAnswer" itemtype="https://schema.org/Answer"
					   class="faq-content__answer box-col animated fadeIn">
					  <div itemprop="text" class="faq-content__answer-text">
						  <?=$faq_sub5;?> <?=$products_min[0]['price'];?> до <?=$products_max[0]['price'];?>.</p>
					  </div>
				  </div>
			  </div>
		  </div>
		  <?php } ?>
		  <?php echo $content_bottom; ?>
      </div>
    </div>
    <?php echo $column_right; ?></div>
</div>
<?php if (!$categories && !$products) { ?>
<div class="container no-items">
	<p><?php echo $text_empty; ?></p>
	<a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
	
	<?php if (count($product_interest)) { ?>
	  <h3 class="prod-interest"><?=$prod_interest?></h3>
      <div class="row product-list product-interest category-product-list">
	<?php $i = 0; ?>
        <?php foreach ($product_interest as $product) { ?>
        <?php if ($product_id == $product['product_id']) continue; ?>
        
        <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
	   <div class="lable">
                <?php if($product['bestseller']){ ?>
                <img class="bestseller" src="image/catalog/lable/bestseller.png?v=1.0" alt="bestseller">
                <?php } ?>
                <?php if($product['recommended']){ ?>
                <img class="recommended" src="image/catalog/lable/recommended.png?v=1.0" alt="recommended">
                <?php } ?>
            </div>

          <div class="product-thumb" id="product<?php echo  $product['product_id']; ?>">
          <!-- start Tkach web-promo Dynamic price -->
          <div id="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo  $product['price']; ?></div>
          <div id="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
          <!-- end web-promo -->
            <div class="image-home"><a href="<?php echo $product['href']; ?>"><img <?php if (isset($product['thumb'])){ ?>src="<?php echo $product['thumb']; ?>"<?php }else{ ?>src="http://t16.web-promo.biz/image/cache/catalog/subcategory/subcatstol-290x160.jpg"<?php } ?> alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
              <div class="caption">
                <div class="old-h4"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                <p><?php echo $product['description']; ?></p>
				  <div class="rating-box">
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
				  </div>
                <?php if ($product['price']) { ?>

                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <span id="newPrice"><?php echo $product['price']; ?></span>
                  <?php } else { ?>
                  <span class="price-new"><span id="newPrice"><?php echo $product['special'].' грн.'; ?></span></span>
                  <span class="price-old"><span id="oldPrice"><?php echo $product['price']; ?></span></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
              </div>
              <?php 
              //start Tkach web-promo
               $option_nomer = '';
               $select_nomer = '';
              // end
			  
			  uasort($product['options'], function($a, $b){
					if ($a['option_id'] === $b['option_id']) return 0;
					return $a['option_id'] < $b['option_id'] ? -1 : 1;
				});
				
				$is_sortoption_id = 0;
				
				foreach($product['options'] as $val){
					if($val['sort_option']){
						$is_sortoption_id = $val['option_id'];
						break;
					}
				}
              ?>

                <?php foreach ($product['options'] as $option) { ?>
					<?php if (!$is_sortoption_id) { ?>
						<?php if ($option['type'] == 'select') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($select_nomer) or empty($select_nomer)){
								$select_nomer = trim($option['product_option_id']);
							}else{
								$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
							}               
						// end
						?>
						<div class="form-group">
						  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
						  <div class="varSelectWrap">
						  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
							<option value=""><?php echo $text_select; ?></option>
							<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
							<option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?>
							</option>
							<?php } ?>
						  </select>
						  </div>
						</div>
					<?php } ?>
								<!-- <script>
									<?php echo 'var option['.$option['product_option_id'].'] = 0; '; ?>
								</script> -->

					<?php if ($option['type'] == 'image') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($option_nomer) or empty($option_nomer)){
								$option_nomer = $option['product_option_id'];
							}else{
								$option_nomer = $option_nomer .', '.$option['product_option_id'];
							}               
						// end
						?>
					<div class="form-group">
					  <label class="control-label"><?php echo $option['name']; ?></label>
					  <div class="select-colors">
					  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
						<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
						<div class="radio radio-image">
						  <label title="<?php echo $option_value['name'] ?>" class="change-price">
							<input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
							
							<img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo '//'.$_SERVER['SERVER_NAME'].'/image/'.$option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?></span>
						  </label>
						</div>
						<?php } ?>
					  </div>
					  </div>
						<div class="show-more-box">
					  <?php if($curr_lang == 'ru'){ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
						<?php }else{ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
						<?php } ?>
						</div>
					</div>
					<?php } ?>

					<?php break; ?>
				<?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
					<?php if ($option['type'] == 'select') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($select_nomer) or empty($select_nomer)){
								$select_nomer = trim($option['product_option_id']);
							}else{
								$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
							}               
						// end
						?>
						<div class="form-group">
						  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
						  <div class="varSelectWrap">
						  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
							<option value=""><?php echo $text_select; ?></option>
							<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
							<option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?>
							</option>
							<?php } ?>
						  </select>
						  </div>
						</div>
					<?php } ?>
								<!-- <script>
									<?php echo 'var option['.$option['product_option_id'].'] = 0; '; ?>
								</script> -->

					<?php if ($option['type'] == 'image') { ?>
						<?php
						// start Tkach web-promo
							if (!isset($option_nomer) or empty($option_nomer)){
								$option_nomer = $option['product_option_id'];
							}else{
								$option_nomer = $option_nomer .', '.$option['product_option_id'];
							}               
						// end
						?>
					<div class="form-group">
					  <label class="control-label"><?php echo $option['name']; ?></label>
					  <div class="select-colors">
					  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
						<?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
						<div class="radio radio-image">
						  <label title="<?php echo $option_value['name'] ?>" class="change-price">
							<input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
							
							<img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo '//'.$_SERVER['SERVER_NAME'].'/image/'.$option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
							<?php if ($option_value['price']) { ?>
							(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
							<?php } ?></span>
						  </label>
						</div>
						<?php } ?>
					  </div>
					  </div>
						<div class="show-more-box">
					  <?php if($curr_lang == 'ru'){ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
						<?php }else{ ?>
							<a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
						<?php } ?>
						</div>
					</div>
					<?php } ?>
				<?php } ?>
            <?php } ?>

              <div class="button-group">
                <div class="clearfix wish-buttons-card">
	                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist?></button>
	                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare?></button>
                </div>
								<div class="button-cart-add clearfix">
									<?php if (!empty($product['stock_image'])) { ?>
										<img class="stock_image" src="<?=$product['stock_image'];?>">
									<?php } else { ?>
										<?php if (strpos( $product['stock_status'],'жида') !== false) { ?>
											<button class="button-cart-p" type="button" onclick="" disabled><i class="fa fa-shopping-cart"></i> <span><?php echo $button_expect; ?></span></button>
										<?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>
											<button class="button-cart-p" type="button" onclick="" disabled><i class="fa fa-shopping-cart"></i> <span><?php echo $button_out_of_sale; ?></span></button>
										<?php } elseif (strpos( $product['stock_status'],'Предзаказ') !== false) { ?>
											<button class="button-cart-p" type="button" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_toorder1; ?></span></button>
										<?php } else { ?>
											<button class="button-cart-p" type="button" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span></button>
										<?php } ?>
									<?php } ?>
								</div>
              </div>
          </div>
        </div>


        
        <?php } ?>
      </div>
      <?php } ?>
</div>
<?php } ?>
<?php /*
<script type="application/ld+json">

{

            "@context": "http://schema.org/",

            "@type": "Product",

            "name": "<?php echo $heading_title; ?>",

            "offers": {

                        "@type": "AggregateOffer",

                        "priceCurrency": "UAH",

                        "lowprice": "<?php echo $min_price; ?>",

                        "highprice": "<?php echo $max_price; ?>",

                        "offerCount": "<?php echo $product_total; ?>"
            }
}
*/
?>

</script>
<script>
	$(document).ready(function(){
		$(document).on('click','.show-more',function(){
			var hWrapBlockColor = $(this).closest('.form-group').find('.wrap-select-colors').innerHeight();
			$(this).closest('.form-group').find('.select-colors').innerHeight(hWrapBlockColor).css('margin-right', '0');
			$(this).css('display', 'none');
		});
		
		////////////////////////////////////////////////////////////////
		$(document).on('click','.product-list .radio-image',function(){
			var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
			var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');
			
			if(newProdImg){
				$(this).parents('.product-thumb').find('.image-home').find('img').attr('src', newProdImg);
			}else{
				$(this).parents('.product-thumb').find('.image-home').find('img').attr('src', oldProdImg);
			}
		});

		$(document).on('click','.categories-goods__item .radio-image',function(){
			var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
			var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');

			if(newProdImg){
				$(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', newProdImg);
			}else{
				$(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', oldProdImg);
			}
		});
	});
	$(window).load(function(){
			$('.product-layout').each(function(){
				var hBlockColor = $(this).find('.select-colors').innerHeight();
				var hWrapBlockColor = $(this).find('.wrap-select-colors').innerHeight();
				console.log(hBlockColor);
				console.log($(this).find('.wrap-select-colors').height());
				if((hWrapBlockColor - hBlockColor) > 20){
					$(this).find('.show-more').css('display', 'block');
				}
			});
		});
</script>
<script>
	var page_url = '<?=$page_url;?>';
	var page_number = '<?=$page_number;?>';

	$(document).ready(function () {
		$('.seo_text').addClass('short_text').after('<div class="show-full-text"><a href="#" class="show-more-text"><?=$btn_open_text;?> »</a></div>');
		$('.show-full-text').on('click', function (e){
			e.preventDefault();
			$('.seo_text').removeClass('short_text');
			$(this).remove();
		});

		function show_more() {
			var get_p = '&';
			if(page_url.replace('?','')==page_url)
				get_p = '?';
			page_number++;
			$.ajax({
				url: page_url+get_p+'page='+page_number+'&ajax=1',
				success: function(result){


					if(result!=''){
					  $('.category-product-list').append(result);
						setTimeout(function (){
							$('.product-layout').each(function(){
								var hBlockColor = $(this).find('.select-colors').innerHeight();
								var hWrapBlockColor = $(this).find('.wrap-select-colors').innerHeight();
								console.log(hBlockColor);
								console.log($(this).find('.wrap-select-colors').height());
								if((hWrapBlockColor - hBlockColor) > 20){
									$(this).find('.show-more').css('display', 'block');
								}
							});
						},2000)
					}
					else {
						$(".show-more").hide();
					}
					$('.layout').hide();
				}
			});
		}
		$('.btn-show-filter').on('click', function () {
			$('.mfilter-free-button').click();
		});
		$(document).on('click', '.show-more-btn', function () {
			show_more();
		});
	});
</script>


<?php /*
	  <?php foreach($products as $pr) { ?>
			<?php foreach($pr['reviews_item'] as $reviews){ ?>
				<div>
				<?php echo $reviews['text']; ?>
				</div>
			<?php } ?>
	  <?php } ?>
	*/
?>


<?php echo $footer; ?>
