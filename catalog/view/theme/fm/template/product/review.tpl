<?php if ($reviews) { ?>

      <?php foreach ($reviews as $review) { ?>
        <div class="feedback__item">
          <div class="feedback__name"><?php echo $review['author']; ?></div>
          <div class="feedback__date"><?php echo $review['date_added']; ?></div>
          <div class="feedback__rate">
            <?php if($review['rating']>0){ ?>
            <div class="product-review-rating">
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($review['rating'] < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
            </div>
            <?php } ?>
          </div>
          <div class="feedback__text">
            <?php echo $review['text']; ?>
          </div>
        </div>

      <?php } ?>
      <div class="text-right"><?php echo $pagination; ?></div>

<?php } else { ?>
<?php //echo $text_no_reviews; ?>
<?php if($lang == 'ru'){ ?>
	Нет отзывов о данном товаре.
<?php }else{ ?>
	В цього товару немає відгуків.
<?php } ?>

<?php } ?>
