
<?php echo $header; ?>

<!-- breadcrumbs -->
<section class="breadcrumbs ">
  <div class="wrapper">
    <ul class="breadcrumbs__items" itemscope itemtype="https://schema.org/BreadcrumbList">

      <?php $breadcrumbs_item = 0;
			 foreach ($breadcrumbs as $breadcrumb) { ?>
      <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
          <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
          <meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a>
      </li>
      <?php } ?>
      <!--ss_breadcrums_list:<?php foreach ($breadcrumbs as $k=>$breadcrumb) { ?><?php echo $breadcrumb['text']; ?><?php if(count($breadcrumb) != $k) { ?> >> <?php } ?><?php } ?>-->
    </ul>
  </div>
</section>
<!-- end breadcrumbs -->

<!-- categories-views -->
<section class="categories-views">
  <div class="wrapper">
    <h1 class="categories-views__head h1"><?php echo $heading_title; ?></h1>
    <?php if ($categories) { ?>
    <div class="minwrapper">
      <div class="categories-views__items">
        <?php foreach ($categories as $category) { ?>
        <div class="categories-views__item">
          <a href="<?php echo $category['href']; ?>" class="categories-views__img-link"><img class="categories-views__img" src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>"></a>
          <a href="<?php echo $category['href']; ?>" class="categories-views__link h3"><?php echo $category['name']; ?></a>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
  </div>
</section>
<!-- end	categories-views -->


<!-- categories-goods -->
<section class="categories-goods" style="display: none;">
  <div class="wrapper">
    <div class="categories-goods__block">
      <?php if ($products) { ?>
      <div class="categories-goods__items">

      </div>
      <?php } ?>

    </div>
  </div>
</section>

<div class="wrapper">


  <header class="category-header row">

    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?=$heading_title;?>" />
    <meta property="og:url" content="<?='https://feshmebel.com.ua'.$_SERVER['REQUEST_URI']?>" />
    <meta property="og:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
    <meta property="article:author" content="https://www.facebook.com/feshemebel/" />
    <meta property="twitter:card" content="summary" />
    <meta property="twitter:title" content="<?=$heading_title;?>" />
    <meta property="twitter:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
    <meta property="og:image" content="<?=$products[0]['thumb']?>" />
    <meta property="twitter:image" content="<?=$products[0]['thumb']?>" />
    <meta property="og:publisher" content="https://www.facebook.com/feshemebel/" />
    <meta property="og:site_name" content="Интернет магазин мебели Фешемебельный" />

    <!-- h1 class="category-title bordered-title col-sm-5"><?php echo $heading_title; ?></h1 -->
    <!--ss_category_name:<?php echo $heading_title; ?>-->
    <?php if(!isset($_GET['sort']) && !$noindex ){ ?>

    <?php
	  	$f=0;
	  	$heading_title_tmp = explode(': ',$heading_title);
	  	if(isset($heading_title_tmp[1]))
	  	{
	  	$f++;
	  		$heading_title_tmp = explode(', ',$heading_title_tmp[1]);
	  		foreach($heading_title_tmp as $item)
	  		{ ?>
    <!--ss_selected_filters_info|<?php echo str_replace( ' - ','|',$item); ?>-->
    <?php
	  		}
	  	}
	  ?>

    <?php
	  	$heading_title_tmp = explode(' (',$heading_title);
	  	if(isset($heading_title_tmp[1]))
	  	{
	  		$f++;
	  		$heading_title_tmp = explode(')',$heading_title_tmp[1]);


	  		 ?>
    <!--ss_selected_filters_info|<?php echo str_replace( ' - ','|',$heading_title_tmp[0]); ?>-->
    <?php

	  	}

	  ?>
    <?php if($f){ ?>
    <!--seoshield_formulas--fil-traciya-->
    <?php } ?>
    <?php } ?>
    <?php if ($products) { ?>

  </header>

  <div class="row">



    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content"><?php echo $content_top; ?>

      <!--  <?php echo $class; ?> -->
      <style>
        .category-product-list
        {
          margin: 0;
        }
        .product-list .product-layout
        {
          position: relative;
        }
        .privat
        {
          position: absolute;
          right: 0;
          z-index: 1;
        }
        .privat img
        {
          height: 40px;
          width: 40px;
          margin-left: -50px;
          margin-top: 10px;
        }
        .show-more-btn
        {
          cursor: pointer;
          display: inline-block;

          padding: 10px 20px;
          color: #2d4059;
          border: 1px solid #2d4059;
          background-color: transparent;
          text-transform: uppercase;
          font-size: 10px;

          line-height: 10px;
          margin: 5px auto;
          transition: background-color .5s ease,color .5s ease;
        }
        .show-more-text
        {
          cursor: pointer;
          display: inline-block;

          padding: 10px 20px;
          color: #2d4059;
          border: 1px solid #2d4059;
          background-color: transparent;
          text-transform: uppercase;
          font-size: 10px;

          line-height: 10px;
          margin: 5px auto;
          transition: background-color .5s ease,color .5s ease;
          margin-top: 20px;
        }
        @media all and (min-width: 611px) {
          .privat {

            left: 0px;
          }
          .product-layout .privat img.bestseller
          {
            position: relative;
            float: right;
            margin: 0;
            padding: 0;
          }
          .product-layout .privat img.sale_status
          {
            margin: 0;
            padding: 0;
            position: relative;
            z-index: 1;
            width: 100px;
            height: inherit;
            top: 0px;
          }
        }
        .btn-show-filter-box
        {
          display: none;
        }
        .categories-goods__label.youtube
        {
          left: inherit;
          right: 9px;
          background: transparent;
          background-image: url("/image/catalog/youtube.png");
          background-size: 40px 40px;
          background-repeat: no-repeat;
          width: 40px;
          height: 40px;
          top: 1px;

        }
        @media all and (max-width: 610px) {
          /*
          #content,
          #content *
          {
              padding: 0;
              margin: 0;
          }

           */
          #content .faq-content__answer-text
          {
            padding: 10px;
          }

          #content .stagh2
          {
            margin-top: 10px;
            margin-bottom: 10px;
          }

          #content .categories-title
          {
            padding-bottom: 10px;
          }
          #content button.button-cart-p
          {
            width: 100%;
            padding: 10px;
          }

          #content  .btn-show-filter-box
          {
            display: block;
          }
          #content .btn-show-filter
          {
            background: linear-gradient(to bottom, #FFD015 0%, #F9B706 100%);
            text-align: center;
            display: block;
            width: 100%;
            padding: 5px 0 ;
            font-weight: bold;
          }
          #content .btn-show-filter svg {
            margin-right: 10px;
            display: inline-block;
            width: 16px;
            height: 16px;
            vertical-align: middle;
          }
          .product-thumb .caption .old-h4
          {
            height: 36px;
            font-size: 14px;
            text-align: center;
            overflow: hidden;
          }
          .general_img .thumbnail .sale_status, .product-thumb .image-home .sale_status, .product-thumb .image .sale_status,
          .privat,
          img.recommended, img.bestseller
          {
            position: relative;
          }
          .privat
          {
            height: 30px;
          }
          #content .product-layout .sale_status
          {
            padding: 5px;

          }
          .privat
          {

            display: block;
          }
          .product-list .product-thumb
          {
            display: block;
          }
          .category-list-item
          {
            margin: 0;
            width: 25%;
          }
          .product-list .product-layout
          {
            width: 50%;
            margin: 0;
            display: inline-block;
            padding: 5px;
          }
          #content .product-list .product-layout
          {
            padding: 5px;
          }
          #content .product-layout .form-group .show-more-box
          {
            height: 30px;
          }
          #content .product-layout .select-colors
          {
            height: 30px;
          }
          .rating-box
          {
            height: 20px;
          }
          #content .product-layout .form-group .show-more
          {
            float: none;
            display: block;
            padding: 10px;
            text-align: center;

          }
          #content .product-thumb .price {
            color: #2d4057;
          }

          #content .show-more-btn
          {
            padding: 10px;
            display: inline-block;
            width: calc(100% - 20px);
          }
          #content .pagination>li>a, #content .pagination>li>span
          {
            padding: 10px;
          }
          #content .category-select-group .category-select label
          {
            text-align: center;
            display: block;
            width: 100%;
          }
          #content .category-select-group .category-select
          {
            width: 50%;
            text-align: center;
          }
          #content .wish-buttons-card button
          {
            padding: 10px 0;
            margin: 0;
            width: 48%;
            text-align: center;
          }
          .product-layout .privat img.sale_status
          {
            width: calc(100% - 44px);
            height: inherit;
          }
          .product-layout .privat img.bestseller
          {
            float: right;
          }
          .product-thumb .price
          {
            font-size: 12px!important;
          }
          .product-thumb .price .listing_text_price
          {
            font-size: 11px!important;
          }
          .product-thumb .price #oldPrice
          {
            font-size: 12px!important;
          }
        }

      </style>


      <div class="">
        <div class="col-sm-12">
          <div class="category-select-group">
            <div class="category-select">
              <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
              <select id="input-sort" class="form-control" onchange="location = this.value;">
                <?php foreach ($sorts as $sorts) { ?>
                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>

            <div class="category-select ">
              <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
              <select id="input-limit" class="form-control" onchange="location = this.value;">
                <?php foreach ($limits as $limits) { ?>
                <?php if ($limits['value'] == $limit) { ?>
                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>

          </div>
        </div>
      </div>
      <div>
        <?php echo $column_left;?>
        <div class="row product-list category-product-list" >


          <!--isset_listing_page-->
          <?php foreach ($products as $product) { ?>

          <div class="categories-goods__item">
            <div class="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo (!empty( $product['special']) ?  $product['special'] : $product['price'] ); ?></div>
            <div class="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
            <div class="categories-goods__information">
              <?php if($product['sale_status_image']){ ?>
              <div class="categories-goods__label" style="background-color: #<?=$product['sale_status_color'];?>;" ><?=$product['sale_status_name'];?></div>
              <?php } ?>
              <?php if($product['youtube']){ ?>
              <div class="categories-goods__label youtube" >

              </div>
              <?php } ?>
              <div class="categories-goods__image">
                <a href="<?php echo $product['href']; ?>">
                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                </a>
              </div>
              <div class="categories-goods__rating">
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="categories-goods__price">
              <div class="categories-goods__link"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
              <div class="categories-goods__price-block">
                <?php if ($product['price']) { ?>

                <?php if (!$product['special']) { ?>
                <?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
                <div class="categories-goods__old-price">
                  <!-- div class="categories-goods__discount">-3001</div -->
                  <div class="categories-goods__before-discount"><?php echo ceil($product['old_price']).' грн.'; ?></div>
                </div>
                <?php } ?>
                <div class="categories-goods__new-price"><?php echo $product['price']; ?></div>
                <?php } else { ?>
                <div class="categories-goods__old-price">
                  <!-- div class="categories-goods__discount">-3001</div -->
                  <div class="categories-goods__before-discount"><?php echo $product['price']; ?></div>
                </div>
                <div class="categories-goods__new-price"><?php echo $product['special']; ?></div>
                <?php } ?>

                <?php } ?>
              </div>
              <div class="categories-goods__options">

                <?php
										//start Tkach web-promo
										$option_nomer = '';
										$select_nomer = '';
										// end

										uasort($product['options'], function($a, $b){
											if ($a['option_id'] === $b['option_id']) return 0;
											return $a['option_id'] < $b['option_id'] ? -1 : 1;
											}
										);

										$is_sortoption_id = 0;

										foreach($product['options'] as $val){
											if($val['sort_option']){
												$is_sortoption_id = $val['option_id'];
												break;
											}
										}

										?>
                <?php foreach ($product['options'] as $option) { ?>
                <?php if (!$is_sortoption_id) { ?>
                <?php if ($option['type'] == 'select') { ?>
                <?php
													// start Tkach web-promo
													if (!isset($select_nomer) or empty($select_nomer)){
														$select_nomer = trim($option['product_option_id']);
													}else{
														$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
													}
													// end
													?>
                <div class="form-group">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <div class="varSelectWrap">
                    <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                      <option value=""><?php echo $text_select; ?></option>
                      <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                      <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                      <?php if ($option_value['price']) { ?>
                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                      <?php } ?>
                      </option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <?php } ?>


                <?php if ($option['type'] == 'image') { ?>
                <?php
													// start Tkach web-promo
													if (!isset($option_nomer) or empty($option_nomer)){
														$option_nomer = $option['product_option_id'];
													}else{
														$option_nomer = $option_nomer .', '.$option['product_option_id'];
													}
													// end
													?>
                <div class="form-group">
                  <label class="control-label"><?php echo $option['name']; ?></label>
                  <div class="select-colors">
                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                      <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                      <div class="radio radio-image">
                        <label title="<?php echo $option_value['name'] ?>" class="change-price">
                          <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                          <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                          <span class="hidden-descr"><?php echo $option_value['name']; ?>
                            <?php if ($option_value['price']) { ?>
                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                            <?php } ?>
																			</span>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="show-more-box">
                    <?php if($curr_lang == 'ru'){ ?>
                    <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                    <?php }else{ ?>
                    <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                    <?php } ?>
                  </div>

                </div>
                <?php } ?>

                <?php break; ?>
                <?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
                <?php if ($option['type'] == 'select') { ?>
                <?php
															// start Tkach web-promo
															if (!isset($select_nomer) or empty($select_nomer)){
																$select_nomer = trim($option['product_option_id']);
															}else{
																$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
															}
															// end
														?>
                <div class="form-group">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <div class="varSelectWrap">
                    <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                      <option value=""><?php echo $text_select; ?></option>
                      <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                      <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                      <?php if ($option_value['price']) { ?>
                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                      <?php } ?>
                      </option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <?php } ?>


                <?php if ($option['type'] == 'image') { ?>
                <?php
														// start Tkach web-promo
														if (!isset($option_nomer) or empty($option_nomer)){
															$option_nomer = $option['product_option_id'];
														}else{
															$option_nomer = $option_nomer .', '.$option['product_option_id'];
														}
														// end
													?>
                <div class="form-group">
                  <label class="control-label"><?php echo $option['name']; ?></label>
                  <div class="select-colors">
                    <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                      <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                      <div class="radio radio-image">
                        <label title="<?php echo $option_value['name'] ?>" class="change-price">
                          <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                          <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                            <?php if ($option_value['price']) { ?>
                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                            <?php } ?></span>
                        </label>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="show-more-box">
                    <?php if($curr_lang == 'ru'){ ?>
                    <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                    <?php }else{ ?>
                    <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                    <?php } ?>
                  </div>
                </div>
                <?php } ?>
                <?php } ?>
                <?php } ?>

              </div>
              <div class="categories-goods__actions">
                <?php if (!empty($product['stock_image'])) { ?>
                <img class="stock_image" src="<?=$product['stock_image'];?>">
                <?php } else { ?>
                <div class="categories-goods__wish" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></div>
                <div class="categories-goods__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"></div>
                <?php if (strpos( $product['stock_status'],'жида') !== false) { ?>
                <div class="categories-goods__buy" ><span><?php echo $button_expect; ?></span></div>
                <?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>

                <div class="categories-goods__buy" ><span><?php echo $button_out_of_sale; ?></span></div>

                <?php } elseif (strpos( $product['stock_status'],'Предзаказ') !== false) { ?>
                <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_toorder1; ?></span></div>

                <?php } else { ?>
                <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_cart; ?></span></div>

                <?php } ?>
                <?php } ?>


              </div>

            </div>
          </div>

          <?php } ?>

        </div>

      </div>

      <?php } ?>
      <!-- links_block -->
    </div>

    <script>
      $(document).ready(function(){
        $(document).on('click','.show-more',function(){
          var hWrapBlockColor = $(this).closest('.form-group').find('.wrap-select-colors').innerHeight();
          $(this).closest('.form-group').find('.select-colors').innerHeight(hWrapBlockColor).css('margin-right', '0');
          $(this).css('display', 'none');
        });

        ////////////////////////////////////////////////////////////////
        $(document).on('click','.product-list .radio-image',function(){
          var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
          var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');

          if(newProdImg){
            $(this).parents('.product-thumb').find('.image-home').find('img').attr('src', newProdImg);
          }else{
            $(this).parents('.product-thumb').find('.image-home').find('img').attr('src', oldProdImg);
          }
        });

        $(document).on('click','.categories-goods__item .radio-image',function(){
          var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
          var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');

          if(newProdImg){
            $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', newProdImg);
          }else{
            $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', oldProdImg);
          }
        });
      });
      $(window).load(function(){
        $('.product-layout').each(function(){
          var hBlockColor = $(this).find('.select-colors').innerHeight();
          var hWrapBlockColor = $(this).find('.wrap-select-colors').innerHeight();
          console.log(hBlockColor);
          console.log($(this).find('.wrap-select-colors').height());
          if((hWrapBlockColor - hBlockColor) > 20){
            $(this).find('.show-more').css('display', 'block');
          }
        });
      });
    </script>
    <script>
      var page_url = '<?=$page_url;?>';
      var page_number = '<?=$page_number;?>';

      $(document).ready(function () {
        $('.seo_text').addClass('short_text').after('<div class="show-full-text"><a href="#" class="show-more-text"><?=$btn_open_text;?> »</a></div>');
        $('.show-full-text').on('click', function (e){
          e.preventDefault();
          $('.seo_text').removeClass('short_text');
          $(this).remove();
        });

        function show_more() {
          var get_p = '&';
          if(page_url.replace('?','')==page_url)
            get_p = '?';
          page_number++;
          $.ajax({
            url: page_url+get_p+'page='+page_number+'&ajax=1',
            success: function(result){


              if(result!=''){
                $('.category-product-list').append(result);
                setTimeout(function (){
                  $('.product-layout').each(function(){
                    var hBlockColor = $(this).find('.select-colors').innerHeight();
                    var hWrapBlockColor = $(this).find('.wrap-select-colors').innerHeight();
                    console.log(hBlockColor);
                    console.log($(this).find('.wrap-select-colors').height());
                    if((hWrapBlockColor - hBlockColor) > 20){
                      $(this).find('.show-more').css('display', 'block');
                    }
                  });
                },2000)
              }
              else {
                $(".show-more").hide();
              }
              $('.layout').hide();
            }
          });
        }
        $('.btn-show-filter').on('click', function () {
          $('.mfilter-free-button').click();
        });
        $(document).on('click', '.show-more-btn', function () {
          show_more();
        });
      });
    </script>
<?php echo $footer; ?>