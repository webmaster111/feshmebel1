<?php echo $header; ?>
<script>
gtag('event', 'page_view', {
'send_to': 'AW-971613570',
'ecomm_prodid' : <?php echo $products_id; ?>,
'ecomm_pagetype': 'searchresults',

});
</script>
<div class="container">
	<ul class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<?php $breadcrumbs_item = 0;
		 foreach ($breadcrumbs as $breadcrumb) { ?>
		<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		<a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
		<span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a></li>
		<?php } ?>
	</ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <!-- <h1><?php echo $heading_title; ?></h1>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label> -->
      <div class="row">
        <div class="search-header">
          <div class="search-header-item">
            <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
          </div>
          <div class="search-header-item">
            <select name="category_id" class="form-control">
              <option value="0"><?php echo $text_category; ?></option>
              <?php foreach ($categories as $category_1) { ?>
              <?php if ($category_1['category_id'] == $category_id) { ?>
              <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
              <?php } ?>
              <?php foreach ($category_1['children'] as $category_2) { ?>
              <?php if ($category_2['category_id'] == $category_id) { ?>
              <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
              <?php } ?>
              <?php foreach ($category_2['children'] as $category_3) { ?>
              <?php if ($category_3['category_id'] == $category_id) { ?>
              <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
              <?php } ?>
              <?php } ?>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
          <div class="search-header-item">
            <label class="checkbox-inline">
              <?php if ($sub_category) { ?>
              <input type="checkbox" name="sub_category" value="1" checked="checked" />
              <?php } else { ?>
              <input type="checkbox" name="sub_category" value="1" />
              <?php } ?>
              <?php echo $text_sub_category; ?>
            </label>
          </div>
          <div class="search-header-item search-header-item--search">
            <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />
          </div>
        </div>
      </div>
      <!-- <p>
        <label class="checkbox-inline">
          <?php if ($description) { ?>
          <input type="checkbox" name="description" value="1" id="description" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="description" value="1" id="description" />
          <?php } ?>
          <?php echo $entry_description; ?></label>
      </p> -->





      <?php if ($products) { ?>
      <div class="category-header row">
    	  <h2 class="category-title bordered-title col-sm-5"><?php echo $text_search; ?></h2>
    	  <div class="col-sm-7">
    	    <div class="category-select-group">
    				<div class="category-select">
    					<label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
              <select id="input-sort" class="form-control col-sm-3" onchange="location = this.value;">
                <?php foreach ($sorts as $sorts) { ?>
                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
    				</div>
    				<div class="category-select">
              <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
              <select id="input-limit" class="form-control" onchange="location = this.value;">
                <?php foreach ($limits as $limits) { ?>
                <?php if ($limits['value'] == $limit) { ?>
                <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
    				</div>
    	    </div>
    	  </div>
      </div>

      <div class="row featured">
        <?php foreach ($products as $product) { ?>

        <div class="categories-goods__item">
          <div class="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo (!empty( $product['special']) ?  $product['special'] : $product['price'] ); ?></div>
          <div class="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
          <div class="categories-goods__information">
            <?php if($product['sale_status_image']){ ?>
            <div class="categories-goods__label" style="background-color: #<?=$product['sale_status_color'];?>;" ><?=$product['sale_status_name'];?></div>
            <?php } ?>
            <div class="categories-goods__image">
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
              </a>
            </div>
            <div class="categories-goods__rating">
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="categories-goods__price">
            <div class="categories-goods__link"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
            <?php if ($product['price']) { ?>
            <div class="categories-goods__price-block">
              <?php if (!$product['special']) { ?>
              <?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
              <div class="categories-goods__old-price">
                <!-- div class="categories-goods__discount">-3001</div -->
                <div class="categories-goods__before-discount"><?php echo ceil($product['old_price']).' грн.'; ?></div>
              </div>
              <?php } ?>
              <div class="categories-goods__new-price"><?php echo $product['price']; ?></div>
              <?php } else { ?>
              <div class="categories-goods__old-price">
                <!-- div class="categories-goods__discount">-3001</div -->
                <div class="categories-goods__before-discount"><?php echo $product['price']; ?></div>
              </div>
              <div class="categories-goods__new-price"><?php echo $product['special']; ?></div>
              <?php } ?>
            </div>
            <?php } ?>
            <div class="categories-goods__options">

              <?php
										//start Tkach web-promo
										$option_nomer = '';
										$select_nomer = '';
										// end

										uasort($product['options'], function($a, $b){
											if ($a['option_id'] === $b['option_id']) return 0;
											return $a['option_id'] < $b['option_id'] ? -1 : 1;
											}
										);

										$is_sortoption_id = 0;

										foreach($product['options'] as $val){
											if($val['sort_option']){
												$is_sortoption_id = $val['option_id'];
												break;
											}
										}

										?>
              <?php foreach ($product['options'] as $option) { ?>
              <?php if (!$is_sortoption_id) { ?>
              <?php if ($option['type'] == 'select') { ?>
              <?php
													// start Tkach web-promo
													if (!isset($select_nomer) or empty($select_nomer)){
														$select_nomer = trim($option['product_option_id']);
													}else{
														$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
													}
													// end
													?>
              <div class="form-group">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="varSelectWrap">
                  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                    <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <?php } ?>


              <?php if ($option['type'] == 'image') { ?>
              <?php
													// start Tkach web-promo
													if (!isset($option_nomer) or empty($option_nomer)){
														$option_nomer = $option['product_option_id'];
													}else{
														$option_nomer = $option_nomer .', '.$option['product_option_id'];
													}
													// end
													?>
              <div class="form-group">
                <label class="control-label"><?php echo $option['name']; ?></label>
                <div class="select-colors">
                  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                    <div class="radio radio-image">
                      <label title="<?php echo $option_value['name'] ?>" class="change-price">
                        <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                        <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                        <span class="hidden-descr"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?>
																			</span>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="show-more-box">
                  <?php if($curr_lang == 'ru'){ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                  <?php }else{ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                  <?php } ?>
                </div>

              </div>
              <?php } ?>

              <?php break; ?>
              <?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
              <?php if ($option['type'] == 'select') { ?>
              <?php
															// start Tkach web-promo
															if (!isset($select_nomer) or empty($select_nomer)){
																$select_nomer = trim($option['product_option_id']);
															}else{
																$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
															}
															// end
														?>
              <div class="form-group">
                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                <div class="varSelectWrap">
                  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                    <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <?php } ?>


              <?php if ($option['type'] == 'image') { ?>
              <?php
														// start Tkach web-promo
														if (!isset($option_nomer) or empty($option_nomer)){
															$option_nomer = $option['product_option_id'];
														}else{
															$option_nomer = $option_nomer .', '.$option['product_option_id'];
														}
														// end
													?>
              <div class="form-group">
                <label class="control-label"><?php echo $option['name']; ?></label>
                <div class="select-colors">
                  <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                    <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                    <div class="radio radio-image">
                      <label title="<?php echo $option_value['name'] ?>" class="change-price">
                        <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                        <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                          <?php if ($option_value['price']) { ?>
                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                          <?php } ?></span>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="show-more-box">
                  <?php if($curr_lang == 'ru'){ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                  <?php }else{ ?>
                  <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                  <?php } ?>
                </div>
              </div>
              <?php } ?>
              <?php } ?>
              <?php } ?>

            </div>
            <div class="categories-goods__actions">
              <?php if (!empty($product['stock_image'])) { ?>
              <img class="stock_image" src="<?=$product['stock_image'];?>">
              <?php } else { ?>
              <div class="categories-goods__wish" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></div>
              <div class="categories-goods__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"></div>
              <?php if (strpos( $product['stock_status'],'жида') !== false) { ?>
              <div class="categories-goods__buy" ><span><?php echo $button_expect; ?></span></div>
              <?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>

              <div class="categories-goods__buy" ><span><?php echo $button_out_of_sale; ?></span></div>

              <?php } elseif (strpos( $product['stock_status'],'Предзаказ') !== false) { ?>
              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_toorder1; ?></span></div>

              <?php } else { ?>
              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_cart; ?></span></div>

              <?php } ?>
              <?php } ?>


            </div>

          </div>
        </div>

        <?php } ?>

      </div>
      <script>
        jQuery(document).ready(function($){
          $(document).on('click','.categories-goods__item .radio-image',function(){
            var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
            var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');

            if(newProdImg){
              $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', newProdImg);
            }else{
              $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', oldProdImg);
            }
          });

        });
      </script>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
    url = url_search+'?';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<script>
  $(document).ready(function(){
    $('.show-more').click(function(){
      var hWrapBlockColor = $(this).parent('.form-group').find('.wrap-select-colors').innerHeight();
      $(this).parent('.form-group').find('.select-colors').innerHeight(hWrapBlockColor).css('margin-right', '0');
      $(this).css('display', 'none');
    });

    ////////////////////////////////////////////////////////////////
    $('.product-list .radio-image').click(function(){
      var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
      var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');

      if(newProdImg){
        $(this).parents('.product-thumb').find('.image-home').find('img').attr('src', newProdImg);
      }else{
        $(this).parents('.product-thumb').find('.image-home').find('img').attr('src', oldProdImg);
      }
    });
  });
</script>
<?php echo $footer; ?>
