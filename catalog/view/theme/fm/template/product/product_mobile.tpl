<?php echo $header; ?>
<?php $img_url = '//'.$_SERVER['SERVER_NAME'].'/image/'; ?>
<?php $nosale = $data['product_info']['stock_status'] == 'Снято с продажи'; ?>
<?php $product_id = $data['product_info']['product_id']; ?>
<?php  $_SESSION['vproducts'][$product_id] = $data['product_info'];

?>

<!-- Семантическая разметка -->
<script type="application/ld+json">
    {
        "@context": "http://schema.org/",
        "@type": "Product",
        "name": "<?=$heading_title?>",
        "image":
        ["<?=$thumb?>"],
        "description": "<?=$description_meta; ?>",
        "mpn": "<?= $sku?>",
        "sku": "<?= $sku?>",
        "brand":
        {
            "@type": "Thing",
            "name": "<?=$manufacturer?>"
        },
        <? if ($reviews_item) { ?>
    "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "<?=$reviews_rating;?>",
    "bestRating": "5",
    "ratingCount": "<?=$reviews_count;?>"
    },
    "review": [
    <? foreach ($reviews_item as $k=>$review) { ?>
    {
    "@type": "Review",
    "name": "<?=$heading_title?>",
    "reviewBody": "<?php echo $review['text']; ?>",
    "datePublished": "<?php echo $review['date_added']; ?>",
    "author": {
    "@type": "Person",
    "name": "<?php echo $review['author']; ?>"
    },
    "reviewRating": {
    "@type": "Rating",
    "worstRating": "1",
    "bestRating": "5",
    "ratingValue": "<?php echo $review['rating']; ?>"
    }
    }<? if (count($reviews_item)-1 != $k) { ?>, <? } ?>

    <? } ?>
    ],
    <? } ?>
    "offers":
    {
    "@type": "Offer",
    "url": "<?= 'https://'.$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'];?>",
    "priceCurrency": "UAH",
    "price": "<?= str_replace(' грн.', '', $price)?>",
    "availability": "http://schema.org/InStock",
    "priceValidUntil" : "<?php echo date("Y-m-d", (time() + 30*24*60*60 ) ); ?>",

    "seller": {
    "@type": "Organization",
    "name": "Интернет магазин мебели Фешемебельный"
    },

    <? if ($attribute_groups) { ?>
    "additionalProperty": [
    <? foreach ($attribute_groups as $attributes) { ?>
    <? $i=0; foreach ($attributes['attribute'] as $key=>$attribute) { $i++; if ($i<3) continue; ?>
    {
    "@type": "PropertyValue",
    "name": "<? echo $attribute['name']; ?>",
    "value": "<? echo $attribute['text']; ?>"
    }
    <? if ($key!=count($attributes['attribute'])-1) { ?>,<? } ?>
    <? } ?>
    <? } ?>
    ]
    <? } ?>
    }
    }
</script>
<!-- Семантическая разметка -->
<!--seoshield_formulas--kartochka-tovara-->
<!--ss_breadcrums_list:<?php foreach ($breadcrumbs as $breadcrumb) { ?><?php echo $breadcrumb['text']; ?> >> <?php } ?>-->
<? if ($attribute_groups) { ?>
<? foreach ($attribute_groups as $attributes) { ?>
<? $i=0; foreach ($attributes['attribute'] as $key=>$attribute) { $i++; if ($i<3) continue; ?>
<!--ss_product_info|<? echo $attribute['name']; ?>|<? echo $attribute['text']; ?>-->

<? } ?>
<? } ?>
<? } ?>
<!--ss_product_code:<?= $sku?>-->
<!--ss_full_desc:<?php echo $description; ?>-->
<!--ss_reviews_num:<?=count($reviews_item);?>-->
<!--ss_price:<?= str_replace(' грн.', '', $price)?>-->
<!--ss_availability:true-->
<!--ss_product_id:<?=$product_id;?>-->
<script>
	gtag('event', 'page_view', {
    	'send_to': 'AW-971613570',
    	'ecomm_prodid' : '<?php echo $product_id; ?>', 
		'ecomm_pagetype': 'product', 
		'ecomm_totalvalue' : '<?php echo (int)$price; ?>' 
	});
</script>
<div class="container product_mobile">
<div id="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo $price; ?></div>
	<ul class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<?php $breadcrumbs_item = 0;
		 foreach ($breadcrumbs as $breadcrumb) { ?>
		<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		<a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
		<span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a></li>
		<?php } ?>
	</ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <div id="product"  class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } ?>
        <div class="lable  product-recommended">
            <?php if($bestseller){ ?>
            <img class="bestseller" src="image/catalog/lable/bestseller.png" alt="bestseller">
            <?php } ?>
            <?php if($recommended){ ?>
            <img class="recommended" src="image/catalog/lable/recommended.png" alt="recommended">
            <?php } ?>
        </div>
        <div class="col-sm-6 smaller-p left-left"><!-- <?php echo $class; ?> -->
		  <div class="mb-prodinfo-show mbshow-h1"></div>
            <link rel="stylesheet" type="text/css" href="/catalog/view/theme/fm/slick/slick.css" />
            <link rel="stylesheet" type="text/css" href="/catalog/view/theme/fm/slick/slick-theme.css" />
            <script src="/catalog/view/theme/fm/slick/slick.min.js" ></script>
            <div style="position: relative;">
                <span class="sale_status" style="background-color: #<?=$sale_status_color;?>;"><?=$sale_status_name;?></span>
            </div>


            <?php if ($thumb || $images) { ?>
                <?php $i_img = 0; ?>
                <div class="slider single-item">
                    <?php foreach ($options as $option) if($options[0]['type'] == 'image'){ ?>
                        <?php foreach ($option['product_option_value'] as $option_value) if(!empty($option_value['image_prod'])) { ?>
                            <div class="item-<?=$i_img;?>"><img src="<?php echo $option_value['image_prod']; ?>" title="<?php echo $heading_title .' – фото '. $i_img; ?>" alt="<?php echo $heading_title .' – '. $i_img; ?>"  /></div>
                            <?php $i_img++; ?>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($i_img==0) { ?>
                        <?php $i_img_tmp = 1; ?>
                        <?php if ($thumb) { ?>
                            <div >
                                <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title .' – фото '. $i_img_tmp; ?>" alt="<?php echo $heading_title .' – '. $i_img_tmp; ?>"  />
                            </div>
                        <?php } ?>
                        <?php if ($images) { ?>
                            <?php foreach ($images as $image) { ?>
                                <div><img src="<?php echo $image['popup']; ?>" title="<?php echo $heading_title .' – фото '. $i_img_tmp; ?>" alt="<?php echo $heading_title .' – '. $i_img_tmp; ?>"  /></div>
                                <?php $i_img_tmp++; ?>
                            <?php } ?>
                        <?php } ?>


                        <?php } ?>

                </div>
            <?php if ($youtube) { ?>
            <ul class="thumbnails" style="position:relative;" >
                <li class="image-additional">
                    <a class="popup-youtube" href="http://www.youtube.com/watch?v=<?=$youtube;?>">
                        <img src="/image/catalog/youtube.png" style="height: 45px;" />
                    </a>
                </li>
            </ul>
            <?php } ?>
            <style>
                .single-item  img
                {
                    max-width: 100%;
                }
                .radio-image
                {
                    border: 2px solid transparent;
                }
                .radio-image.hover
                {
                    border: 2px solid #f5c827
                }


            </style>
            <script>
                var slick_box = $('.single-item').slick({dots: true});
            </script>
            <?php } ?>
            <?php $num_tmp=0; ?>
            <?php if (count($options)==1 && $options[0]['type'] == 'image') {
                $option = $options[0];
            ?>


                <div class="product-detail-options">
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">

                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                            <div <?=$option_value['image_prod']? 'id="'.$num_tmp.'"':'';?>  class="radio radio-image <?=$option_value['image_prod']? 'radio-image-click':''; ?>">
                                <label title="<?php echo $option_value['name'] ?>">
                                    <input price="<?php echo $option_value['price']; ?>" class="radio-img change-price" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' checked="checked"'; ?>  step="<?php echo $num ?>"/>
                                    <img src="<?php echo $option_value['image']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                                        <?php if ($option_value['price']) { ?>
                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                        <?php } ?></span>
                                </label>

                                <?php if ($option_value['image_prod']) { ?>
                                    <?php $num_tmp++; ?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            <? } ?>

		<div class="clearfix"></div>
		<div class="product-info product-info-left mb-prodinfo-hide clearfix">
              <div class="description-div">
					<h2 class="product-info-title description"><?php echo $title_description; ?></h2>
					<div class="product-info-return" style="display: none;">
					<!--seo_text_start-->
          
           <?php 
           $replaceUrls = array(
              'http://arapparel.ua/', 'https://eximtextil.com.ua/'
           );
           foreach ($replaceUrls as $r_url) {
             if (strpos($description, $r_url)){
              $description = preg_replace('#<a href="'.preg_quote($r_url).'"#', '<a rel="nofollow" href="'.$r_url.'"', $description);
             }
           }
          ?>

          <p><?php echo $description; ?></p><!--seo_text_end-->
					</div>
              </div>
              <div class="options-div">
					<h2 class="product-info-title return"><?php echo $title_attribute; ?></h2>
					<div class="product-info-return" style="display: none;">
				<!--					<table class="table table-bordered">-->
						<table>
					<?php foreach ($attribute_groups as $attribute_group) { ?>
					<thead>
					  <tr>
						<td colspan="2"><strong><?php echo $attribute_group['name']; ?>:</strong></td>
					  </tr>
					</thead>
					<tbody>
					  <tr>
					  <?php $i=0; foreach ($attribute_group['attribute'] as $attribute) { $i++; if ($i<3) continue;?>
					  <tr>
						<td style="width:200px;" ><?php echo $attribute['name']; ?></td>
						<td><?php echo $attribute['text']; ?></td>
					  </tr>
					  <?php } ?>
					</tbody>
					<?php } ?>
				  </table>
					</div>
              </div>
        </div>
		
          <div class="feedback-wrap">


            <?php if ($review_status && $number_review > 0) { ?>
            <span class="feedback-title feedback-title-descr-1"><i class="fa fa-chevron-down" aria-hidden="true"></i><?php echo $tab_review; ?></span>
            <?php } ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
				<?php if ($number_review > 0) { ?>
                <div id="review"></div>
				<?php } ?>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="" id="input-name" class="form-control" />
                </div>
                <div class="form-group required">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>"><i class="plane-send fa fa-paper-plane-o" aria-hidden="true"></i><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
          </div>
        </div>
		
        <?php if ($column_left || $column_right) { ?>
         <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-4'; ?>
        <?php } ?>
        <div class="col-sm-6 bigger-p  right-right"><!-- <?php echo $class; ?> -->
            <?php if(!empty($special_info)){ ?>
            <div class="sale-container">
                <?php if(!empty($special_info['name'])){ ?>
                    <?php if($curr_lang == 'ru' || empty($special_info['name_ua'])){ ?>
                        <div class="sale-title"><?php echo $special_info['name'] ?></div>
                    <?php } elseif($curr_lang == 'uk') { ?>
                        <div class="sale-title"><?php echo $special_info['name_ua'] ?></div>
                    <?php } ?>
                <?php } ?>
                <?php if($special_info['show_time'] == 1){ ?>
                <div class="timer-title"><?php echo $text_product_special_end; ?></div>
                <div class="sale-coutdown"></div>
                <?php } ?>
                <?php if(!empty($special_info['description']) && $special_info['show_description'] == 1){ ?>
                <a href="#" onclick="$(this).next().toggle(); return false;"><?php echo $text_product_special_more; ?></a>
                <?php if($curr_lang == 'ru' || empty($special_info['description_ua'])){ ?>
                    <div class="sale-description" style="display: none;"><?php echo html_entity_decode($special_info['description'], ENT_QUOTES, 'UTF-8') ?></div>
                <?php } elseif($curr_lang == 'uk') { ?>
                    <div class="sale-description" style="display: none;"><?php echo html_entity_decode($special_info['description_ua'], ENT_QUOTES, 'UTF-8') ?></div>
                <?php } ?>
                <?php } ?>
            </div>
            <?php } ?>
          <h1 class="product-heading-title mb-prodinfo-hide">

              <meta property="og:type" content="website" />
              <meta property="og:title" content="<?=$heading_title?>" />
              <meta property="og:url" content="<?='https://feshmebel.com.ua'.$_SERVER['REQUEST_URI']?>" />
              <meta property="og:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
              <meta property="article:author" content="https://www.facebook.com/feshemebel/" />
              <meta property="twitter:card" content="summary" />
              <meta property="twitter:title" content="<?=$heading_title?>" />
              <meta property="twitter:description" content="Ищешь '<?=$heading_title;?>'? Заходи и выбирай прямо сейчас!" />
              <meta property="og:image" content="<?=$image['popup']?>" />
              <meta property="twitter:image" content="<?=$image['popup']?>" />
              <meta property="og:publisher" content="https://www.facebook.com/feshemebel/" />
              <meta property="og:site_name" content="Интернет магазин мебели Фешемебельный" />

          <?php echo $heading_title; ?></h1>
            <!--div class="product__sku">Артикул:<span><?php echo $sku; ?></span></div -->

            <div>

                <?php if (!$nosale) { ?>
                <?php if ($rating > 0) { ?>
                <div class="rating">
                    <p>
                        <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><!-- <?php echo $text_write; ?> --></a>
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($rating < $i) { ?>
                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                        <?php } else { ?>
                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                        <?php } ?>
                        <?php } ?>
                    </p>
                    <!-- AddThis Button BEGIN -->
                    <!--             <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> -->
                    <!-- AddThis Button END -->
                </div>
                <?php } ?>
                <?php if ($price) { ?>
                <div class="product-sale__price-block">
                    <?php if (empty($special_info) && ((!$special || !isset($product_info['sproduct_id']) || ($product_info['sproduct_id']!=$product_info['product_id'])))) { ?>
                    <?php if(isset($old_price) && $old_price > 0) { ?>
                    <div class="product-sale__old-disc">
                        <div class="product-sale__old"><?php echo ceil($old_price); ?></div>
                    </div>
                    <?php } ?>
                    <div class="product__price-main" id="newPrice"><?php echo $price; ?></div>
                    <?php } else { ?>
                    <?php if(isset($price) && $price > 0) { ?>
                    <div class="product-sale__old-disc">
                        <div class="product-sale__old"><?php echo $price; ?></div>
                    </div>
                    <?php } ?>
                    <div class="product__price-main" id="newPrice"><?php echo $special; ?></div>

                    <script>
                        <?php if(preg_match('/([\d\s]+)/',$special, $matches)){ ?>
                            var newPriseSpecial = 0;
                            var SpecialPrice = <?php echo $matches[1];  ?>;
                        <?php } ?>

                    </script>
                    <div id="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo $special; ?></div>
                    <div id="event-mess" style='color: red; font-size: 20px;'><span id="timetoevent"></span></div>

                    <?php } ?>
                </div>

                <ul class="list-unstyled product-prices">

                    <?php if ($discounts) { ?>
                    <li>
                        <hr>
                    </li>
                    <?php foreach ($discounts as $discount) { ?>
                    <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                    <?php } ?>
                    <?php } ?>
                </ul>
                <?php } ?>
                <!-- ul class="list-unstyled">
                  <?php if ($manufacturer) { ?>
                  <li class="info-li"><span class="product-bold-sign"><?php //echo $text_stock; ?></span> <span class="unbold"><?php //echo $stock; ?></span></li>
                  <li class="info-li"><span class="product-bold-sign"><?php //echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>" class="unbold"><?php //echo $manufacturer; ?></a></li>
                  <?php } ?>
                  <li class="info-li"><span class="product-bold-sign"><?php //echo $text_model; ?></span> <span class="unbold"><?php //echo $model; ?></span></li>
                  <?php if ($reward) { ?>
                  <li class="info-li"><span class="product-bold-sign"><?php //echo $text_reward; ?> </span><span class="unbold"><?php //echo $reward; ?></span></li>
                  <?php } ?>

                </ul -->
                <!--
                <pre>
                <?php //echo $data['product_info']['stock_status']; ?>
                <?php //print_r($attribute_groups); ?>
                </pre>
                -->
                <?php
			$dt = $data['product_info']['date_available'];
			$dt = substr( $dt, 8, 2).'.'.substr( $dt, 5, 2).'.'.substr( $dt, 0, 4);
?>
                <?php if (isset($size)&&false) { ?>
                <div class="product__delivery-time"><?=$text_size;?>:<span><?=$size;?></span></div>
                <?php } ?>
                <?php if (isset($material)&&false) { ?>
                <div class="product__delivery-time"><?=$text_material;?>:<span><?=$material;?></span></div>
                <?php } ?>
                <?php if (strpos($data['product_info']['stock_status'],'заказ') !== false) {
				$dt = $data['product_info']['date_delivery'];
			  ?>
                <div class="product__delivery-time">Срок поставки:<span><?php echo $dt.' дней'; ?></span></div>

                <?php } ?>
          <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
					</tr>
                </thead>
                <tbody>
                  <tr>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
              <?php if ((count($options)==1 && $options[0]['type'] != 'image') || count($options)>1 ) { ?>

            <div id="product" class="product-detail-options">
            <?php if ($options) { ?>
            <!-- <hr>
            <h3><?php echo $text_option; ?></h3> -->
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control selectize">
                <option value=""><?php echo $text_select; ?></option>
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                <option class="change-price" price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' selected="selected"'; ?>><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <!-- <p class="">Выберите размер:</p> -->
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                <div class="radio radio-card-wrap">
                  <label>
                    <input price="<?php echo $option_value['price']; ?>" type="radio" class="card-input-radio change-price" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' checked="checked"'; ?>" />
                    <span class="radio-card"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
             <?php if ($option['type'] == 'image') { ?>

             <!-- <p class="chose-option">Выберите цвет:</p> -->
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>

                <div <?=$option_value['image_prod']? 'id="'.$num_tmp.'"':'';?>  class="radio radio-image <?=$option_value['image_prod']? 'radio-image-click':''; ?>">
                  <label title="<?php echo $option_value['name'] ?>">
                    <input price="<?php echo $option_value['price']; ?>" class="radio-img change-price" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' checked="checked"'; ?>  step="<?php echo $num ?>"/>
                    <img src="<?php echo $option_value['image']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>
                  </label>

                </div>
                <?php if ($option_value['image_prod']) { ?>
                  <?php $num_tmp++; ?>
                <?php } ?>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>" >
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                <div class="checkbox">
                  <label>
                    <input price="<?php echo $option_value['price']; ?>" class="change-price" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' checked="checked"'; ?>  step="<?php echo $num ?>"/>
                    <span class="checkbox-styled-2"></span>
                    <span class="checkbox-span">

                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>

                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>

            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label " for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default upload-file-button"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn calendar-button" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn calendar-button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn calendar-button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
                <?php } ?>
            <?php if ($i_img!=0) { ?>
                <script>
                    $('.radio-image-click').click(function () {
                        index = $(this).attr('id');
                        if($('.item-'+index).length>0)
                        $('.single-item').slick('slickGoTo', index, false);
                    });
                </script>
            <?php } ?>

                <?php if (empty($stock_image)) { ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>
            <div class="form-group">
              <label class="control-label" style="display: block" for="input-quantity"><?php echo $entry_qty; ?></label>
            <div class="quantity-input">
                  <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                    <div class="quantity-control">
                        <i class="fa fa-sort-up" data-up="1"></i>
                        <i class="fa fa-sort-down" data-down="1"></i>
                    </div>
            </div>
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />

            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>

            <div class="product__buy-buttons">
            <?php if (strpos($data['product_info']['stock_status'],'жида') !== false) { ?>
            <div class="product__order-btn"  ><?php echo $button_expect; ?></div>
            <?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>
            <div class="product__order-btn"  ><?php echo $button_out_of_sale; ?></div>
            <?php } elseif (strpos( $data['product_info']['stock_status'],'Предзаказ') !== false) { ?>
            <div class="product__order-btn" id="button-cart" ><?php echo $button_toorder2; ?></div>
            <?php }else{ ?>

            <div class="product__order-btn" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_cart; ?></div>
            <?php } ?>



            <?php if($show_btn_try_on){ ?>
            <div class="product__installment-btn try-on" onclick="product_try_on()"><?php echo $text_show_btn_try_on; ?></div>
            <? } ?>
            <?php if($show_btn_where_watch){ ?>
            <div class="product__installment-btn look product_where_watch_run" onclick="gtag('event', 'click', {'event_category': 'Place'}); "><?php echo $text_show_btn_where_watch; ?></div>
            <? } ?>
        </div>

        <div class="product__add-to-share">
            <div class="product__add-to">
                <div class="product__compare"  onclick="compare.add('<?php echo $product_id; ?>');"><span>В сравнение
		</span></div>
                <div class="product__favorites" onclick="wishlist.add('<?php echo $product_id; ?>');"><span>В избранное
		</span></div>
            </div>
            <div class="product__share">
                <span>Поделиться:</span>
                <div class="product__share-link"><a href="" class="product__share-fb"></a></div>
                <div class="product__share-link"><a href="" class="product__share-pinterest"></a></div>
                <div class="product__share-link"><a href="" class="product__share-twitter"></a></div>
                <div class="product__share-link"><a href="" class="product__share-email"></a></div>
            </div>
        </div>
            <div class="row">
                <div class="btns-buy1 col-sm-6"></div>
            </div>

            <script>
                var show_dialog_step = 0
                function show_dialog() {
                    show_dialog_step++;
                 //   $('.ui-dialog').css('top','10px');
                    if(show_dialog_step>2) {

                        show_dialog_step = 0;
                        return 0
                    }
                    setTimeout(show_dialog, 250);
                    console.log('show_dialog');
                    $("#product-where-watch").dialog({
                        height: $(window).width() > 400 ? ($(window).height() - 200) : 400,
                        width: "90%",
                        dialogClass: "product-popup",
                        open: function (event, ui) {
                            $(".product-popup").find('.ui-dialog-titlebar-close').html('<svg width="20px" height="20px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill="#fff" d="M10.707 10.5l5.646-5.646c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.646 5.646-5.646-5.646c-0.195-0.195-0.512-0.195-0.707 0s-0.195 0.512 0 0.707l5.646 5.646-5.646 5.646c-0.195 0.195-0.195 0.512 0 0.707 0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146l5.646-5.646 5.646 5.646c0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146c0.195-0.195 0.195-0.512 0-0.707l-5.646-5.646z"></path></svg>');
                            $(".product-popup").find('#where-watch-map > iframe').css('height', ($(window).width() > 400 ? ($(window).height() - 200) - $("#product-where-watch a").height() : 400) + "px")
                            $('.ui-widget-overlay.ui-front').on('click', function (e) {
                                e.preventDefault();
                                $("#product-where-watch").dialog('close');
                            });
                        },
                        draggable: false,
                        modal: true
                    });

                }
                $(document).ready(function () {

                    $('.product_where_watch_run').click(function (){
                        show_dialog();
                    });
                });


            </script>

            <!-- start Tkach web-promo
		<div class="product-buttons-wrap">
            <div class="btn-group float-buttons clearfix">
				<button class="add-to-compare" type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></button>
              	<button class="add-to-wishes" type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></button>
            </div>-->
            
            <!-- <div class="btns-buy">
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn  btn-lg "><i class="fa fa-shopping-cart" aria-hidden="true"></i><?php echo $button_cart; ?></button>

              <a href="/" class="in-one-click contact-btn"><i class="fa fa-hand-o-right" aria-hidden="true"></i>Купить в один клик</a>
            </div> -->
            
         <!-- end web-promo </div> -->
              <?php } ?>
		  <?php } else { ?>
			<img class="stock_image" src="<?=$stock_image;?>" >
		  <?php }; ?>

          <div>
            <div class="product-info clearfix">



              <div class="delivery-div">
                <h2 class="product-info-title return"><?php echo $title_delivery; ?></h2>
                <div class="product-info-return" style="display: none;">
	                <div class="col-sm-6 clearpad" style="width: 100%;">
						<div class="new-post">
							<?php if($data["product_info"]["nopaydelivery"] == 1 && false){ ?>
								<?php echo $description_nopaydelivery_1; ?>
							<?php }else{ ?>
								<?php echo $description_delivery_1; ?>
							<?php } ?>
						</div>
					</div>
					<div class="col-sm-6 clearpad" style="width: 100%;">
						<div class="new-post">
							<?php if($data["product_info"]["nopaydelivery"] == 1 && false){ ?>
								<?php echo $description_nopaydelivery_2; ?>
							<?php }else{ ?>
								<?php echo $description_delivery_2; ?>
							<?php } ?>
						</div>
					</div>
					<p><?php echo $description_delivery_3; ?></p>
					<p><a href="<?php echo $url_menu_delivery; ?>" target="_blank"><?php echo $description_delivery_4; ?></a></p>
					<p><a href="<?php echo $url_menu_exchange; ?>" target="_blank"><?php echo $description_delivery_5; ?></a></p>
					<p><a href="<?php echo $url_menu_guaranty; ?>" target="_blank"><?php echo $description_delivery_6; ?></a></p>
				</div>
              </div>
              <div class="installation-div" style="display: none;">
                <h2 class="product-info-title return"><?php echo $title_installation; ?></h2>
                <div class="product-info-return" style="display: none;">
					<div class="in-time">
						<p><?php echo $description_installation; ?></p>
						<?php if($data["product_info"]["nopayinstall"] == 1){ ?>
							<p><?php echo $nopay_install; ?></p>
						<?php }elseif(ceil($data["product_info"]["installation_price"]) == 0){ ?>
							<p><?php echo $noneed_install; ?></p>
						<?php }else{ ?>
							<p><?php echo ceil($data["product_info"]["installation_price"]).' грн.'; ?></p>
						<?php } ?>
					</div>
				</div>
              </div>
			  
			  <div class="mb-prodinfo-show">
				  <div class="description-div">
						<h2 class="product-info-title description"><?php echo $title_description; ?></h2>
						<div class="product-info-return" style="display: none;">
						<p><?php echo $description; ?></p>
						</div>
				  </div>
				  <div class="options-div">
						<h2 class="product-info-title return"><?php echo $title_attribute; ?></h2>
						<div class="product-info-return" style="display: none;">
					<!--					<table class="table table-bordered">-->
							<table>
						<?php foreach ($attribute_groups as $attribute_group) { ?>
						<thead>
						  <tr>
							<td colspan="2"><strong><?php echo $attribute_group['name']; ?>:</strong></td>
						  </tr>
						</thead>
						<tbody>
						  <tr>
						  <?php $i=0; foreach ($attribute_group['attribute'] as $attribute) { $i++; if ($i<3) continue;?>
						  <tr>
							<td style="width:200px;" ><?php echo $attribute['name']; ?></td>
							<td><?php echo $attribute['text']; ?></td>
						  </tr>
						  <?php } ?>
						</tbody>
						<?php } ?>
					  </table>
						</div>
				  </div>
				  <br />
              </div>

            </div>
          </div>
        </div>
      </div>
<style>
/* Базовый контейнер табов */
.tabs {
	min-width: 320px;
/*	max-width: 800px;*/
	width:	100%;
	padding: 0px;
	margin: 0 auto;
}
/* Стили секций с содержанием */
.tabs>section {
	display: none;
/*	padding: 15px;*/
	background: #fff;
/*	border: 1px solid #ddd;*/
}
.tabs>section>p {
/*	margin: 0 0 5px;*/
/*	line-height: 1.5;*/
	color: #383838;
	/* прикрутим анимацию */
	-webkit-animation-duration: 1s;
	animation-duration: 1s;
	-webkit-animation-fill-mode: both;
	animation-fill-mode: both;
	-webkit-animation-name: fadeIn;
	animation-name: fadeIn;
}
/* Описываем анимацию свойства opacity */

@-webkit-keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
@keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
/* Прячем чекбоксы */
.tabs>input {
	display: none;
	position: absolute;
}
/* Стили переключателей вкладок (табов) */
.tabs>label {
	display: inline-block;
	margin: 0 0 -1px;
	padding: 15px 25px;
	font-weight: 600;
	text-align: center;
	color: #aaa;
/*	border: 0px solid #ddd;*/
/*	border-width: 1px 1px 1px 1px;*/
	background: #f1f1f1;
	border-radius: 3px 3px 0 0;
}
/* Шрифт-иконки от Font Awesome в формате Unicode */
.tabs>label:before {
	font-family: fontawesome;
	font-weight: normal;
	margin-right: 10px;
}
.tabs>label[for*="1"]:before {
/*	content: "\f17a";*/
}
.tabs>label[for*="2"]:before {
/*	content: "\f17a";*/
}
.tabs>label[for*="3"]:before {
	content: "\f13b";
}
.tabs>label[for*="4"]:before {
	content: "\f13c";
}
/* Изменения стиля переключателей вкладок при наведении */

.tabs>label:hover {
	color: #888;
	cursor: pointer;
}
/* Стили для активной вкладки */
.tabs>input:checked+label {
	color: #555;
	border: 1px solid #009933;
/*	border-top: 1px solid #009933;*/
/*	border-bottom: 1px solid #fff;*/
	background: #fff;
}
/* Активация секций с помощью псевдокласса :checked */
#tab1:checked~#content-tab1, #tab2:checked~#content-tab2, #tab3:checked~#content-tab3, #tab4:checked~#content-tab4 {
	display: block;
}
/* Убираем текст с переключателей
* и оставляем иконки на малых экранах
*/

@media screen and (max-width: 680px) {
	.tabs>label {
		font-size: 20px;
	}
	.tabs>label:before {
		margin: 0;
		font-size: 20px;
	}
}
/* Изменяем внутренние отступы
*  переключателей для малых экранов
*/
@media screen and (max-width: 400px) {
	.tabs>label {
		padding: 15px;
	}
}
</style>
      <div style="margin-bottom: 10px;">
          <?=$text_seo;?>
      </div>
      <div>
          <h2 class="promotions__head head"><?php echo $material_h2; ?></h2>
          <section id="content-tab3">
              <?php if ($material_products) { ?>

              <div class="featured" style="padding-top: 20px;">

                  <?php foreach ($material_products as $product) { ?>
                  <?php if ($product_id == $product['product_id']) continue; ?>
                  <div class="categories-goods__item">
                      <div class="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo (!empty( $product['special']) ?  $product['special'] : $product['price'] ); ?></div>
                      <div class="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
                      <div class="categories-goods__information">
                          <?php if($product['sale_status_image']){ ?>
                          <div class="categories-goods__label" style="background-color: #<?=$product['sale_status_color'];?>;" ><?=$product['sale_status_name'];?></div>
                          <?php } ?>
                          <div class="categories-goods__image">
                              <a href="<?php echo $product['href']; ?>">
                                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                              </a>
                          </div>
                          <div class="categories-goods__rating">
                              <?php if ($product['rating']) { ?>
                              <div class="rating">
                                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                                  <?php if ($product['rating'] < $i) { ?>
                                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                  <?php } else { ?>
                                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                  <?php } ?>
                                  <?php } ?>
                              </div>
                              <?php } ?>
                          </div>
                      </div>
                      <div class="categories-goods__price">
                          <div class="categories-goods__link"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                          <?php if ($product['price']) { ?>
                          <div class="categories-goods__price-block">
                              <?php if (!$product['special']) { ?>
                              <?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
                              <div class="categories-goods__old-price">
                                  <!-- div class="categories-goods__discount">-3001</div -->
                                  <div class="categories-goods__before-discount"><?php echo ceil($product['old_price']).' грн.'; ?></div>
                              </div>
                              <?php } ?>
                              <div class="categories-goods__new-price"><?php echo $product['price']; ?></div>
                              <?php } else { ?>
                              <div class="categories-goods__old-price">
                                  <!-- div class="categories-goods__discount">-3001</div -->
                                  <div class="categories-goods__before-discount"><?php echo $product['price']; ?></div>
                              </div>
                              <div class="categories-goods__new-price"><?php echo $product['special']; ?></div>
                              <?php } ?>
                          </div>
                          <?php } ?>
                          <div class="categories-goods__options">

                              <?php
										//start Tkach web-promo
										$option_nomer = '';
										$select_nomer = '';
										// end

										uasort($product['options'], function($a, $b){
											if ($a['option_id'] === $b['option_id']) return 0;
											return $a['option_id'] < $b['option_id'] ? -1 : 1;
											}
										);

										$is_sortoption_id = 0;

										foreach($product['options'] as $val){
											if($val['sort_option']){
												$is_sortoption_id = $val['option_id'];
												break;
											}
										}

										?>
                              <?php foreach ($product['options'] as $option) { ?>
                              <?php if (!$is_sortoption_id) { ?>
                              <?php if ($option['type'] == 'select') { ?>
                              <?php
													// start Tkach web-promo
													if (!isset($select_nomer) or empty($select_nomer)){
														$select_nomer = trim($option['product_option_id']);
													}else{
														$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
													}
													// end
													?>
                              <div class="form-group">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <div class="varSelectWrap">
                                      <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                                          <option value=""><?php echo $text_select; ?></option>
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                                          <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                                          <?php if ($option_value['price']) { ?>
                                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                          <?php } ?>
                                          </option>
                                          <?php } ?>
                                      </select>
                                  </div>
                              </div>
                              <?php } ?>


                              <?php if ($option['type'] == 'image') { ?>
                              <?php
													// start Tkach web-promo
													if (!isset($option_nomer) or empty($option_nomer)){
														$option_nomer = $option['product_option_id'];
													}else{
														$option_nomer = $option_nomer .', '.$option['product_option_id'];
													}
													// end
													?>
                              <div class="form-group">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <div class="select-colors">
                                      <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                                          <div class="radio radio-image">
                                              <label title="<?php echo $option_value['name'] ?>" class="change-price">
                                                  <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                                                  <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                                                  <span class="hidden-descr"><?php echo $option_value['name']; ?>
                                                      <?php if ($option_value['price']) { ?>
                                                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                      <?php } ?>
																			</span>
                                              </label>
                                          </div>
                                          <?php } ?>
                                      </div>
                                  </div>
                                  <div class="show-more-box">
                                      <?php if($curr_lang == 'ru'){ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                                      <?php }else{ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                                      <?php } ?>
                                  </div>

                              </div>
                              <?php } ?>

                              <?php break; ?>
                              <?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
                              <?php if ($option['type'] == 'select') { ?>
                              <?php
															// start Tkach web-promo
															if (!isset($select_nomer) or empty($select_nomer)){
																$select_nomer = trim($option['product_option_id']);
															}else{
																$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
															}
															// end
														?>
                              <div class="form-group">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <div class="varSelectWrap">
                                      <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                                          <option value=""><?php echo $text_select; ?></option>
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                                          <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                                          <?php if ($option_value['price']) { ?>
                                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                          <?php } ?>
                                          </option>
                                          <?php } ?>
                                      </select>
                                  </div>
                              </div>
                              <?php } ?>


                              <?php if ($option['type'] == 'image') { ?>
                              <?php
														// start Tkach web-promo
														if (!isset($option_nomer) or empty($option_nomer)){
															$option_nomer = $option['product_option_id'];
														}else{
															$option_nomer = $option_nomer .', '.$option['product_option_id'];
														}
														// end
													?>
                              <div class="form-group">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <div class="select-colors">
                                      <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                                          <div class="radio radio-image">
                                              <label title="<?php echo $option_value['name'] ?>" class="change-price">
                                                  <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                                                  <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                                                      <?php if ($option_value['price']) { ?>
                                                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                      <?php } ?></span>
                                              </label>
                                          </div>
                                          <?php } ?>
                                      </div>
                                  </div>
                                  <div class="show-more-box">
                                      <?php if($curr_lang == 'ru'){ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                                      <?php }else{ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                                      <?php } ?>
                                  </div>
                              </div>
                              <?php } ?>
                              <?php } ?>
                              <?php } ?>

                          </div>
                          <div class="categories-goods__actions">
                              <?php if (!empty($product['stock_image'])) { ?>
                              <img class="stock_image" src="<?=$product['stock_image'];?>">
                              <?php } else { ?>
                              <div class="categories-goods__wish" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></div>
                              <div class="categories-goods__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"></div>
                              <?php if (strpos( $product['stock_status'],'жида') !== false) { ?>
                              <div class="categories-goods__buy" ><span><?php echo $button_expect; ?></span></div>
                              <?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>

                              <div class="categories-goods__buy" ><span><?php echo $button_out_of_sale; ?></span></div>

                              <?php } elseif (strpos( $product['stock_status'],'Предзаказ') !== false) { ?>
                              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_toorder1; ?></span></div>

                              <?php } else { ?>
                              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_cart; ?></span></div>

                              <?php } ?>
                              <?php } ?>


                          </div>

                      </div>
                  </div>

                  <?php } ?>
              </div>

              <?php } ?>
          </section>
          <div style="clear: both;"></div>
      </div>
      <div>
          <div class="promotions__head head"><?php echo $text_related; ?></div>
          <section id="content-tab1">
              <?php if ($products) { ?>

              <div class="featured" style="padding-top: 20px;">

                  <?php foreach ($products as $product) { ?>
                  <?php if ($product_id == $product['product_id']) continue; ?>
                  <div class="categories-goods__item">
                      <div class="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo (!empty( $product['special']) ?  $product['special'] : $product['price'] ); ?></div>
                      <div class="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo  $product['special']; ?></div>
                      <div class="categories-goods__information">
                          <?php if($product['sale_status_image']){ ?>
                          <div class="categories-goods__label" style="background-color: #<?=$product['sale_status_color'];?>;" ><?=$product['sale_status_name'];?></div>
                          <?php } ?>
                          <div class="categories-goods__image">
                              <a href="<?php echo $product['href']; ?>">
                                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
                              </a>
                          </div>
                          <div class="categories-goods__rating">
                              <?php if ($product['rating']) { ?>
                              <div class="rating">
                                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                                  <?php if ($product['rating'] < $i) { ?>
                                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                  <?php } else { ?>
                                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                  <?php } ?>
                                  <?php } ?>
                              </div>
                              <?php } ?>
                          </div>
                      </div>
                      <div class="categories-goods__price">
                          <div class="categories-goods__link"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                          <?php if ($product['price']) { ?>
                          <div class="categories-goods__price-block">
                              <?php if (!$product['special']) { ?>
                              <?php if(isset($product['old_price']) && $product['old_price'] > 0){ ?>
                              <div class="categories-goods__old-price">
                                  <!-- div class="categories-goods__discount">-3001</div -->
                                  <div class="categories-goods__before-discount"><?php echo ceil($product['old_price']).' грн.'; ?></div>
                              </div>
                              <?php } ?>
                              <div class="categories-goods__new-price"><?php echo $product['price']; ?></div>
                              <?php } else { ?>
                              <div class="categories-goods__old-price">
                                  <!-- div class="categories-goods__discount">-3001</div -->
                                  <div class="categories-goods__before-discount"><?php echo $product['price']; ?></div>
                              </div>
                              <div class="categories-goods__new-price"><?php echo $product['special']; ?></div>
                              <?php } ?>
                          </div>
                          <?php } ?>
                          <div class="categories-goods__options">

                              <?php
										//start Tkach web-promo
										$option_nomer = '';
										$select_nomer = '';
										// end

										uasort($product['options'], function($a, $b){
											if ($a['option_id'] === $b['option_id']) return 0;
											return $a['option_id'] < $b['option_id'] ? -1 : 1;
											}
										);

										$is_sortoption_id = 0;

										foreach($product['options'] as $val){
											if($val['sort_option']){
												$is_sortoption_id = $val['option_id'];
												break;
											}
										}

										?>
                              <?php foreach ($product['options'] as $option) { ?>
                              <?php if (!$is_sortoption_id) { ?>
                              <?php if ($option['type'] == 'select') { ?>
                              <?php
													// start Tkach web-promo
													if (!isset($select_nomer) or empty($select_nomer)){
														$select_nomer = trim($option['product_option_id']);
													}else{
														$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
													}
													// end
													?>
                              <div class="form-group">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <div class="varSelectWrap">
                                      <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                                          <option value=""><?php echo $text_select; ?></option>
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                                          <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                                          <?php if ($option_value['price']) { ?>
                                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                          <?php } ?>
                                          </option>
                                          <?php } ?>
                                      </select>
                                  </div>
                              </div>
                              <?php } ?>


                              <?php if ($option['type'] == 'image') { ?>
                              <?php
													// start Tkach web-promo
													if (!isset($option_nomer) or empty($option_nomer)){
														$option_nomer = $option['product_option_id'];
													}else{
														$option_nomer = $option_nomer .', '.$option['product_option_id'];
													}
													// end
													?>
                              <div class="form-group">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <div class="select-colors">
                                      <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                                          <div class="radio radio-image">
                                              <label title="<?php echo $option_value['name'] ?>" class="change-price">
                                                  <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                                                  <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                                                  <span class="hidden-descr"><?php echo $option_value['name']; ?>
                                                      <?php if ($option_value['price']) { ?>
                                                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                      <?php } ?>
																			</span>
                                              </label>
                                          </div>
                                          <?php } ?>
                                      </div>
                                  </div>
                                  <div class="show-more-box">
                                      <?php if($curr_lang == 'ru'){ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                                      <?php }else{ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                                      <?php } ?>
                                  </div>

                              </div>
                              <?php } ?>

                              <?php break; ?>
                              <?php }elseif($is_sortoption_id && $is_sortoption_id == $option['option_id']){ ?>
                              <?php if ($option['type'] == 'select') { ?>
                              <?php
															// start Tkach web-promo
															if (!isset($select_nomer) or empty($select_nomer)){
																$select_nomer = trim($option['product_option_id']);
															}else{
																$select_nomer = trim($select_nomer) .', '.trim($option['product_option_id']);
															}
															// end
														?>
                              <div class="form-group">
                                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                  <div class="varSelectWrap">
                                      <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect change-price">
                                          <option value=""><?php echo $text_select; ?></option>
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                                          <option  price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'selected="selected"'; ?>><?php echo $option_value['name']; ?>
                                          <?php if ($option_value['price']) { ?>
                                          (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                          <?php } ?>
                                          </option>
                                          <?php } ?>
                                      </select>
                                  </div>
                              </div>
                              <?php } ?>


                              <?php if ($option['type'] == 'image') { ?>
                              <?php
														// start Tkach web-promo
														if (!isset($option_nomer) or empty($option_nomer)){
															$option_nomer = $option['product_option_id'];
														}else{
															$option_nomer = $option_nomer .', '.$option['product_option_id'];
														}
														// end
													?>
                              <div class="form-group">
                                  <label class="control-label"><?php echo $option['name']; ?></label>
                                  <div class="select-colors">
                                      <div id="input-option<?php echo $option['product_option_id']; ?>" class="wrap-select-colors">
                                          <?php $num=0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                                          <div class="radio radio-image">
                                              <label title="<?php echo $option_value['name'] ?>" class="change-price">
                                                  <input  price="<?php echo $option_value['price']; ?>"  class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num==1) echo 'checked="checked"'; ?>/>
                                                  <img style="width: 30px;" src="<?php if(isset($option_value['image'])){ echo $option_value['image'];} ?>" data-oldprodimg="<?php echo $product['thumb']; ?>" <?=$option_value['image_prod']?'data-prodimg = "'.$option_value['image_prod'].'"':'' ?> alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                                                      <?php if ($option_value['price']) { ?>
                                                      (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                      <?php } ?></span>
                                              </label>
                                          </div>
                                          <?php } ?>
                                      </div>
                                  </div>
                                  <div class="show-more-box">
                                      <?php if($curr_lang == 'ru'){ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ещё</a>
                                      <?php }else{ ?>
                                      <a href="javascript:void(0);" style="display: none;" class="show-more">ще</a>
                                      <?php } ?>
                                  </div>
                              </div>
                              <?php } ?>
                              <?php } ?>
                              <?php } ?>

                          </div>
                          <div class="categories-goods__actions">
                              <?php if (!empty($product['stock_image'])) { ?>
                              <img class="stock_image" src="<?=$product['stock_image'];?>">
                              <?php } else { ?>
                              <div class="categories-goods__wish" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></div>
                              <div class="categories-goods__compare" onclick="compare.add('<?php echo $product['product_id']; ?>');"></div>
                              <?php if (strpos( $product['stock_status'],'жида') !== false) { ?>
                              <div class="categories-goods__buy" ><span><?php echo $button_expect; ?></span></div>
                              <?php } elseif (strpos( $product['stock_status'],'продажи') !== false) { ?>

                              <div class="categories-goods__buy" ><span><?php echo $button_out_of_sale; ?></span></div>

                              <?php } elseif (strpos( $product['stock_status'],'Предзаказ') !== false) { ?>
                              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_toorder1; ?></span></div>

                              <?php } else { ?>
                              <div class="categories-goods__buy" onclick="cart.productadd('<?php echo $product['product_id']; ?>', ['<?php if (isset($option_nomer)){ echo trim($option_nomer); } ?>'], ['<?php if (isset($select_nomer)){ echo trim($select_nomer); } ?>']);"><span><?php echo $button_cart; ?></span></div>

                              <?php } ?>
                              <?php } ?>


                          </div>

                      </div>
                  </div>

                  <?php } ?>
              </div>

              <?php } ?>
          </section>
      </div>

      <div style="clear:both;">
          <div class="promotions__head head"><?php echo $revised_goods; ?></div>
          <section id="content-tab2">

          </section>
      </div>
      <script>
          jQuery(document).ready(function($){
              $('#content-tab2').load('/index.php?route=product/product/viewed&product_id=<?php echo $product_id; ?>');
              $(document).on('click','.categories-goods__item .radio-image',function(){
                  var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
                  var oldProdImg = $(this).find('label').find('.img-thumbnail').attr('data-oldprodimg');

                  if(newProdImg){
                      $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', newProdImg);
                  }else{
                      $(this).closest('.categories-goods__item').find('.categories-goods__image').find('img').attr('src', oldProdImg);
                  }
              });

          });
      </script>




      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php if(isset($rating) and $rating != ''){ ?>
<div class="display-none">
<div >

            <span >Отзывы о Сайте</span>

            <span >

            <span >

            <span><?php echo (int)$rating; ?></span>

                        из

            <span">5</span>

            </span>

            </span>

                        на основе

            <span><?php echo (int)$number_rating; ?></span> оценок.

            <span><?php echo (int)$number_review; ?></span> клиентских отзывов.

</div>
</div>
<?php } ?>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});

var minCount = <?=(isset($minimum))?$minimum:1?>;
var alertErrorCount = $('#product .alert-info').html();

$('input[name="quantity"]').change(function(){
	var currCount = $(this).val();
	if(currCount < minCount){
		$(this).val(minCount);
		$(".error-count").remove();
		$("<div class='alert-info error-count'></div>").insertAfter($(this).parent());
		$(".error-count").html(alertErrorCount);
	}else{
		$(".error-count").remove();
	}
});

// start Tkach web-promo Dynamic price
$(document).ready(function(){
	isValidPrice();	
});
$('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea').change(function(){
	isValidPrice();
});
$('.radio-image').click(function(){
	isValidPrice();
});
function isValidPrice()
{
// $('#thisIsOriginal').html() - start price
// $('#thisIsOriginalspecial') - special price

        var startPrise			= $('#thisIsOriginal').html();
        if (typeof SpecialPrice !== 'undefined') {
            startPrise = SpecialPrice;
        }
        var startPriseSpecial	= $('#thisIsOriginalspecial').html();
        var newPrice			= parseInt(startPrise,10);
        var startPriseSpecial	= parseInt(startPrise,10);

$('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea').map(function() {
  
	switch($(this).attr('type')) {
  case 'radio':  	// if (x === 'radio')
  	radioPrice		= parseInt($(this).attr('price'),10);
  	if($.isNumeric(radioPrice)){
		newPrice		= newPrice + radioPrice;
		if (typeof newPriseSpecial !== 'undefined') {
			newPriseSpecial = newPriseSpecial + radioPrice;
		}
	}
    return radioPrice;
    break;
  case 'checkbox':  // if (x === 'checkbox')
    checkboxPrice	= parseInt($(this).attr('price'),10);
    if($.isNumeric(checkboxPrice)){
		newPrice		= newPrice + checkboxPrice;
		if (typeof newPriseSpecial !== 'undefined') {
			newPriseSpecial = newPriseSpecial + checkboxPrice;
		}
	}
    return checkboxPrice;
    break;
  default:
  	selectPrice		= parseInt($(":checked", this).attr('price'),10);
  	if($.isNumeric(selectPrice)){
		newPrice		= newPrice + selectPrice;
		if (typeof newPriseSpecial !== 'undefined') {
			newPriseSpecial = newPriseSpecial + selectPrice;
		}
	}
   	return selectPrice;
    break;
    


  	}

}).get();
//console.log(newPrice);
$("#newPrice").html(newPrice+" грн.");
	
};
// end web-promo
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {

                update_checkout();
                             //   $('#cart').addClass('open');

				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				if( $('html').attr('lang') == 'uk' )
				{
				$('#cart > ul').load('/ua/index.php?route=common/cart/info ul li');		
				}else{
				$('#cart > ul').load('index.php?route=common/cart/info ul li');	
				}
                console.log('AddToCart');
                fbq('track', 'AddToCart');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
//						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>&lang=<?=$curr_lang?>');

$('#button-review').on('click', function() {
	var btn_review = $(this);
    $.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
                btn_review.parent().before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                btn_review.parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
    $('.popup-youtube').magnificPopup({

        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
    $('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});


	var shtml = '';
	var pos = -1;
	var snone = 'display: none';
	var sblock = 'display: block';

	function changeDisplayAttr(e)
	{
		shtml = jQuery(e).html();
		pos = shtml.indexOf(snone);
		if (pos > 0){
			jQuery(e).html(shtml.replace( snone, sblock ));
		} else {
			pos = shtml.indexOf( sblock );
			if (pos > 0)
			jQuery(e).html(shtml.replace( sblock, snone ));
		}
	}

	changeDisplayAttr('.delivery-div');
	changeDisplayAttr('.description-div');
	changeDisplayAttr('.options-div');
	changeDisplayAttr('.installation-div');
	
	jQuery('.delivery-div').on('click', function(){
		changeDisplayAttr(this);
	});
	jQuery('.description-div').on('click', function(){
		changeDisplayAttr(this);
	});
	jQuery('.options-div').on('click', function(){
		changeDisplayAttr(this);
	});
	jQuery('.installation-div').on('click', function(){
		changeDisplayAttr(this);
	});
	
	$('.mbshow-h1').html('<h1 class="product-heading-title">'+$('.product-heading-title').text()+'</h1>');
	
	/////////////////////////////////////////////////////////////////////
	var widthWindow = $(window).width();
	$('.radio-image').each(function(){
		var imgOffsetLeft = $(this).find('label').find('.img-thumbnail').offset().left;
		if((widthWindow-imgOffsetLeft) < 100){
			$(this).find('.zoom-option-img').css({'left':'auto', 'right':'0'});
		}
	});
	
	var oldProdImg = $('.general_img').find('img').attr('src');
	
	var newProdImg = $('.product-detail-options .radio-image:first-child').find('label').find('.img-thumbnail').attr('data-prodimg');			
			
			if(newProdImg){
				$('.general_img').find('img').attr('src', newProdImg);
			}
	
	$('.radio-image').click(function(){
		var newProdImg = $(this).find('label').find('.img-thumbnail').attr('data-prodimg');
		
		if(newProdImg){
			$('.general_img').find('img').attr('src', newProdImg);
		}else{
			$('.general_img').find('img').attr('src', oldProdImg);
		}
	});
<?php if(!empty($special_info) && $special_info['show_time'] == true){ ?>
    <?php

    $diff = strtotime($special_info['date_end']) - time();
        ?>
    clock = new FlipClock($('.sale-coutdown'), <?php echo $diff; ?>,{
        countdown: true,
        clockFace: 'DailyCounter',
    });
    <?php } ?>



    var values = [];
    $.each($('.product-detail-options select option:checked, .product-detail-options input[type="radio"]:checked'), function(key, val){
        values.push($(val).val());
    })
    $("#try-on-options").val(values.join(','));
    $("#try-on-prod-id").val($("input[name='product_id']").val());

    $('.product-detail-options select, .product-detail-options input[type="radio"]').on('change', function(){
        var values = [];
        $.each($('.product-detail-options select option:checked, .product-detail-options input[type="radio"]:checked'), function(key, val){
            values.push($(val).val());
        })
        $("#try-on-options").val(values.join(','));
    })

});
    var clock;
    $(document).on('click', '.flip-clock-wrapper a', function (e) {
        e.preventDefault();
        return false;
    });
    function clockStop(){
        window.location.reload();
        }
</script>
<?php echo $footer; ?>
