<?php //print_r($product_info); ?>
<?php //$product_info['price_installation'] = preg_replace('/[^0-9.]/', '', $product_info['price_installation']); ?>
<?php //$pos = strpos($product_info['price_installation'],'.'); if ($pos) $product_info['price_installation'] = substr($product_info['price_installation'], 0, $pos + 3 ); ?>
<?php echo $header; ?>
<?php $img_url = '//'.$_SERVER['SERVER_NAME'].'/image/'; ?>
<?php $nosale = $data['product_info']['stock_status'] == 'Снято с продажи'; ?>
<?php //echo 'product_info='; print_r($data['product_info']).'<br>'; ?>
<?php ///print_r($attribute_groups); ?>
<?php //echo '=============================<br>'; ?>
<?php //echo '$nosale='.(strpos($data['product_info']['stock_status'], 'продажи') > 0); ?>
<?php //echo '$_SESSION=<br>'; ?>
<?php //print_r($_SESSION); ?>
<?php $product_id = $data['product_info']['product_id']; ?>
<?php if (!isset($_SESSION['vproducts'][$product_id]))
		$_SESSION['vproducts'][$product_id] = $data['product_info'];
?>
<div class="container">
<div id="thisIsOriginal" style="visibility: hidden; height:0px;"><?php echo $price; ?></div>
	<ul class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<?php $breadcrumbs_item = 0;
		 foreach ($breadcrumbs as $breadcrumb) { ?>
		<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		<a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
		<span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a></li>
		<?php } ?>
	</ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <div class="row">
        <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-8'; ?>
        <?php } ?>
        <div class="lable  product-recommended">
            <?php if($bestseller){ ?>
            <img class="bestseller" src="image/catalog/lable/bestseller.png" alt="bestseller">
            <?php } ?>
            <?php if($recommended){ ?>
            <img class="recommended" src="image/catalog/lable/recommended.png" alt="recommended">
            <?php } ?>
        </div>
        <div class="col-sm-6 smaller-p left-left"><!-- <?php echo $class; ?> -->
          <?php if ($thumb || $images) { 
          $i_img = 1; ?>
          <ul class="thumbnails" style="position:relative; float:left;">
            <?php if ($thumb) { ?>
            
            <li>
			<a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title .' – фото '. $i_img; ?>" alt="<?php echo $heading_title .' – '. $i_img; ?>" width="590px" /></a>
			</li>
            <?php $i_img++;
            } ?>
            <?php if ($images) { ?>
            <?php foreach ($images as $image) { ?>
            <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title .' – фото '. $i_img; ?>" alt="<?php echo $heading_title .' – '. $i_img; ?>"/></a></li>
            <?php $i_img++; 
            } ?>
            <?php } ?>
          </ul>
          <?php } ?>

<!--
			<ul class="pn">
            <span class="feedback-title feedback-title-descr"><i class="fa fa-chevron-down" aria-hidden="true"></i><?php echo $tab_description; ?></span>
			</ul>
          <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
 -->
          <div class="feedback-wrap">


            <?php if ($review_status) { ?>
            <span class="feedback-title feedback-title-descr-1"><i class="fa fa-chevron-down" aria-hidden="true"></i><?php echo $tab_review; ?></span>
            <?php } ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h2><?php echo $text_write; ?></h2>
                <?php if ($review_guest) { ?>
                <div class="form-group required">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="" id="input-name" class="form-control" />
                </div>
                <div class="form-group required">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                <?php echo $captcha; ?>
                <div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>"><i class="plane-send fa fa-paper-plane-o" aria-hidden="true"></i><?php echo $button_continue; ?></button>
                  </div>
                </div>
                <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
          </div>
        </div>
        <?php if ($column_left || $column_right) { ?>
         <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-4'; ?>
        <?php } ?>
        <div class="col-sm-6 bigger-p  right-right"><!-- <?php echo $class; ?> -->

          <h1 class="product-heading-title"><?php echo $heading_title; ?></h1>
          <div>Артикул: <?php echo $sku; ?></div>
          <div>

		  <?php if (!$nosale) { ?>
          <?php if ($review_status) { ?>
          <div class="rating">
            <p>
            <a class="product-reviews" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a><a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><!-- <?php echo $text_write; ?> --></a>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              </p>
            <!-- AddThis Button BEGIN -->
<!--             <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> -->
            <!-- AddThis Button END -->
          </div>
          <?php } ?>
          <?php if ($price) { ?>
          <ul class="list-unstyled">
            <?php if (!$special || !isset($product_info['sproduct_id']) || ($product_info['sproduct_id']!=$product_info['product_id'])) { ?>
            <li>
              <div id="prise" class="h2"><span id="newPrice"><?php echo $price; ?></span></div>
            </li>
            <?php } else { ?>
            <li>
            <div class="new-price-product h2"><span id="priceUpdate"><?php echo $special; ?></span></div>
            </li>
            <li>
            <span class="old-price-product"><span id="newPrice"><?php echo $price; ?></span></span>
            </li>
<div id="thisIsOriginalspecial" style="visibility: hidden; height:0px;"><?php echo $special; ?></div>
<div id="event-mess" style='color: red; font-size: 20px;'><span id="timetoevent"></span></div>

            <?php } ?>
            <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <li>
              <hr>
            </li>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <ul class="list-unstyled">
            <?php if ($manufacturer) { ?>
            <li class="info-li"><span class="product-bold-sign"><?php //echo $text_stock; ?></span> <span class="unbold"><?php //echo $stock; ?></span></li>
            <li class="info-li"><span class="product-bold-sign"><?php //echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>" class="unbold"><?php //echo $manufacturer; ?></a></li>
            <?php } ?>
            <li class="info-li"><span class="product-bold-sign"><?php //echo $text_model; ?></span> <span class="unbold"><?php //echo $model; ?></span></li>
            <?php if ($reward) { ?>
            <li class="info-li"><span class="product-bold-sign"><?php //echo $text_reward; ?> </span><span class="unbold"><?php //echo $reward; ?></span></li>
            <?php } ?>

          </ul>
<!--
<pre>
<?php //echo $data['product_info']['stock_status']; ?>
<?php //print_r($attribute_groups); ?>
</pre>
-->
<?php
			$dt = $data['product_info']['date_available'];
			$dt = substr( $dt, 8, 2).'.'.substr( $dt, 5, 2).'.'.substr( $dt, 0, 4);
?>
			<table>
			<tr>
				<td>
            <div class="form-group">
				<label class="control-label" for="input-option-13"><?php if (isset($text_size)) echo $text_size.':'; ?></label>
            </div>
				</td>
				<td>
            <div class="form-group">
				<span id="input-option-13"><?php echo $size; ?></span>
            </div>
			</tr>
			<tr>
				<td>
            <div class="form-group">
				<label class="control-label" for="input-option-13"><?php if (isset($text_material)) echo $text_material.':'; ?></label>
            </div>
				</td>
				<td>
            <div class="form-group">
				<span id="input-option-13"><?php echo $material; ?></span>
            </div>
			</tr>
			<tr>
				<td width="180">
			<?php if (strpos($data['product_info']['stock_status'],'заказ') !== false) {
				$dt = $data['product_info']['date_delivery'];
			?>
            <div class="form-group">
				<?php if($curr_lang == 'ru'){ ?>
					<label class="control-label" for="input-option-13">Срок поставки:</label>
				<?php }else{ ?>
					<label class="control-label" for="input-option-13">Термін доставки:</label>
				<?php } ?>
            </div>
				</td>
				<td>
            <div class="form-group">
				<span id="input-option-13"><?php echo $dt.' дней'; ?></span>
            </div>
				</td>
			</tr>
			<tr>
				<td>
			<?php  } else if (strpos($data['product_info']['stock_status'],'жида') !== false) {?>
            <div class="form-group">
				<label class="control-label" for="input-option-14">Дата поставки:</label>
            </div>
				</td>
				<td>
            <div class="form-group">
				<span id="input-option-14"><?php echo $dt; ?></span>
            </div>
				</td>
			</tr>
			<?php  }; ?>

			</table>
          <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
					</tr>
                </thead>
                <tbody>
                  <tr>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
            <div id="product">
            <?php if ($options) { ?>
            <!-- <hr>
            <h3><?php echo $text_option; ?></h3> -->
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control selectize">
                <option value=""><?php echo $text_select; ?></option>
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                <option class="change-price" price="<?php echo $option_value['price']; ?>" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' selected="selected"'; ?>><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <!-- <p class="">Выберите размер:</p> -->
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++;?>
                <div class="radio radio-card-wrap">
                  <label>
                    <input price="<?php echo $option_value['price']; ?>" type="radio" class="card-input-radio change-price" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' checked="checked"'; ?>" />
                    <span class="radio-card"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
             <?php if ($option['type'] == 'image') { ?>
             <!-- <p class="chose-option">Выберите цвет:</p> -->
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                <div class="radio radio-image">
                  <label title="<?php echo $option_value['name'] ?>">
                    <input price="<?php echo $option_value['price']; ?>" class="radio-img change-price" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' checked="checked"'; ?>  step="<?php echo $num ?>"/>
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>" >
                <?php $num = 0; foreach ($option['product_option_value'] as $option_value) { $num++; ?>
                <div class="checkbox">
                  <label>
                    <input price="<?php echo $option_value['price']; ?>" class="change-price" type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php if ($num == 1) echo ' checked="checked"'; ?>  step="<?php echo $num ?>"/>
                    <span class="checkbox-styled-2"></span>
                    <span class="checkbox-span">

                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>

                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>

            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label " for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default upload-file-button"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn calendar-button" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn calendar-button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn calendar-button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>
            <div class="form-group">
              <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />

            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
             <div class="btns-buy">
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn  btn-lg "><i class="fa fa-shopping-cart" aria-hidden="true"></i><?php echo $button_cart; ?></button>
              <button class="add-to-compare" type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></button>
              <button class="add-to-wishes" type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></button>

<!-- web-promo <a href="/" class="in-one-click contact-btn"><i class="fa fa-hand-o-right" aria-hidden="true"></i>Купить в один клик</a>-->
            </div>
            <!-- start Tkach web-promo
		<div class="product-buttons-wrap">
            <div class="btn-group float-buttons clearfix">
				<button class="add-to-compare" type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></button>
              	<button class="add-to-wishes" type="button" data-toggle="tooltip" class="btn btn-default" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></button>
            </div>-->
            
            <!-- <div class="btns-buy">
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn  btn-lg "><i class="fa fa-shopping-cart" aria-hidden="true"></i><?php echo $button_cart; ?></button>

              <a href="/" class="in-one-click contact-btn"><i class="fa fa-hand-o-right" aria-hidden="true"></i>Купить в один клик</a>
            </div> -->
            
         <!-- end web-promo </div> -->
		  <?php } else { ?>
			<span style="font-weight: bold;color: #444;font-size: 33px;margin: 20px 0 10px;display: block;">Товар снят с продажи</span>
			<br><br><br><br><br><br><br><br><br>
		  <?php }; ?>

          <div>
            <div class="product-info clearfix">
              <div class="description-div">
                <h2 class="product-info-title description"><?php echo $title_description; ?></h2>
                <div class="product-info-return" style="display: none;">
                <p><?php echo $description; ?></p>
				</div>
              </div>
              <div class="options-div">
                <h2 class="product-info-title return"><?php echo $title_attribute; ?></h2>
                <div class="product-info-return" style="display: none;">
<!--					<table class="table table-bordered">-->
					<table>
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?>:</strong></td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <?php $i=0; foreach ($attribute_group['attribute'] as $attribute) { $i++; if ($i<3) continue;?>
                  <tr>
                    <td style="width:200px;" ><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
				</div>
              </div>
              <div class="delivery-div">
                <h2 class="product-info-title return"><?php echo $title_delivery; ?></h2>
                <div class="product-info-return" style="display: none;">
	                <div class="col-sm-6 clearpad" style="width: 100%;">
						<div class="new-post">
							<?php echo $description_delivery_1; ?>
						</div>
					</div>
					<div class="col-sm-6 clearpad" style="width: 100%;">
						<div class="new-post">
							<?php echo $description_delivery_2; ?>
						</div>
					</div>
					<p><?php echo $description_delivery_3; ?></p>
					<p><a href="<?php echo $url_menu_delivery; ?>" target="_blank"><?php echo $description_delivery_4; ?></a></p>
					<p><a href="<?php echo $url_menu_exchange; ?>" target="_blank"><?php echo $description_delivery_5; ?></a></p>
					<p><a href="<?php echo $url_menu_guaranty; ?>" target="_blank"><?php echo $description_delivery_6; ?></a></p>
				</div>
              </div>
              <div class="installation-div">
                <h2 class="product-info-title return"><?php echo $title_installation; ?></h2>
                <div class="product-info-return" style="display: none;">
					<div class="in-time">
						<p><?php echo $description_installation; ?></p>
						<p><?php echo ceil($data["product_info"]["installation_price"]).' грн.'; ?></p>
					</div>
				</div>
              </div>

            </div>
          </div>
        </div>
      </div>
<style>
/* Базовый контейнер табов */
.tabs {
	min-width: 320px;
/*	max-width: 800px;*/
	width:	100%;
	padding: 0px;
	margin: 0 auto;
}
/* Стили секций с содержанием */
.tabs>section {
	display: none;
/*	padding: 15px;*/
	background: #fff;
/*	border: 1px solid #ddd;*/
}
.tabs>section>p {
/*	margin: 0 0 5px;*/
/*	line-height: 1.5;*/
	color: #383838;
	/* прикрутим анимацию */
	-webkit-animation-duration: 1s;
	animation-duration: 1s;
	-webkit-animation-fill-mode: both;
	animation-fill-mode: both;
	-webkit-animation-name: fadeIn;
	animation-name: fadeIn;
}
/* Описываем анимацию свойства opacity */

@-webkit-keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
@keyframes fadeIn {
	from {
		opacity: 0;
	}
	to {
		opacity: 1;
	}
}
/* Прячем чекбоксы */
.tabs>input {
	display: none;
	position: absolute;
}
/* Стили переключателей вкладок (табов) */
.tabs>label {
	display: inline-block;
	margin: 0 0 -1px;
	padding: 15px 25px;
	font-weight: 600;
	text-align: center;
	color: #aaa;
/*	border: 0px solid #ddd;*/
/*	border-width: 1px 1px 1px 1px;*/
	background: #f1f1f1;
	border-radius: 3px 3px 0 0;
}
/* Шрифт-иконки от Font Awesome в формате Unicode */
.tabs>label:before {
	font-family: fontawesome;
	font-weight: normal;
	margin-right: 10px;
}
.tabs>label[for*="1"]:before {
/*	content: "\f17a";*/
}
.tabs>label[for*="2"]:before {
/*	content: "\f17a";*/
}
.tabs>label[for*="3"]:before {
	content: "\f13b";
}
.tabs>label[for*="4"]:before {
	content: "\f13c";
}
/* Изменения стиля переключателей вкладок при наведении */

.tabs>label:hover {
	color: #888;
	cursor: pointer;
}
/* Стили для активной вкладки */
.tabs>input:checked+label {
	color: #555;
	border: 1px solid #009933;
/*	border-top: 1px solid #009933;*/
/*	border-bottom: 1px solid #fff;*/
	background: #fff;
}
/* Активация секций с помощью псевдокласса :checked */
#tab1:checked~#content-tab1, #tab2:checked~#content-tab2, #tab3:checked~#content-tab3, #tab4:checked~#content-tab4 {
	display: block;
}
/* Убираем текст с переключателей
* и оставляем иконки на малых экранах
*/

@media screen and (max-width: 680px) {
	.tabs>label {
		font-size: 20px;
	}
	.tabs>label:before {
		margin: 0;
		font-size: 20px;
	}
}
/* Изменяем внутренние отступы
*  переключателей для малых экранов
*/
@media screen and (max-width: 400px) {
	.tabs>label {
		padding: 15px;
	}
}
</style>

	<div class="tabs">
		<input id="tab1" type="radio" name="tabs" checked>
		<label for="tab1" title="Вкладка 1" style="font-size: 20px;"><?php echo $text_related; ?></label>

		<input id="tab2" type="radio" name="tabs">
		<label for="tab2" title="Вкладка 2" style="font-size: 20px;"><?php echo $revised_goods; ?></label>

		<section id="content-tab1">
		<p>

      <div class="combo"><?php echo $combo; ?></div>
      <?php if ($products) { ?>
<!--      <h3 class="related-goods"><?php echo $text_related; ?></h3> -->
      <div class="row product-list product-list--full">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <?php if ($product_id == $product['product_id']) continue; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } ?>
        <div class="product-layout">
          <div class="product-thumb">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if ((int)$product['special'] == 0) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
            <div class="button-group">
              <div class="wish-buttons-card clearfix">
                 <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></button>
                 <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></button>
              </div>
							<div class="button-cart-add clearfix">
								<button class="button-cart-p" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
									<i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span>
								</button>
							</div>

            </div>
          </div>
        </div>


        <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif ($i % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>
		</p>
	  </section>
<!--
<pre>
	<?php	//echo '<br>$_SESSION='; print_r($_SESSION); ?>
</pre>
-->
		<section id="content-tab2">
		<p>

      <?php if ($_SESSION['vproducts']) { ?>
<!--		<h3 class="related-goods"><?php echo $text_viewed; ?></h3> -->
      <div class="row product-list product-list--full">
        <?php $i = 1; ?>
        <?php foreach ($_SESSION['vproducts'] as $product) { ?>
        <?php if ($product_id == $product['product_id']) continue; ?>

        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } ?>

        <div class="product-layout">
          <div class="product-thumb transition">
            <div class="image">
				<a href="<?php echo $_SESSION['products'][$product['product_id']]['href']; ?>">
					<img src="<?php echo $_SESSION['products'][$product['product_id']]['thumb']; ?>" alt="<?php echo $product['name'].' '.$product['product_id'].' $product_id='.$product_id; ?>" title="<?php echo $product['name'].' '.$product['product_id'].' $product_id='.$product_id; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $_SESSION['products'][$product['product_id']]['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($n = 1; $n <= 5; $n++) { ?>
                <?php if ($product['rating'] < $n) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if ((int)$product['special'] == 0) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
             <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              <?php } ?>
            </div>
            <div class="button-group">
              <div class="wish-buttons-card clearfix">
                 <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><?php echo $button_wishlist; ?></button>
                 <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><?php echo $button_compare; ?></button>
              </div>
							<div class="button-cart-add clearfix">
								<button class="button-cart-p" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
									<i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span>
								</button>
							</div>

            </div>
          </div>
        </div>
        <?php $i++; } ?>
      </div>
      <?php } ?>
		</p>
	  </section>
	</div>



      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php if(isset($rating) and $rating != ''){ ?>
<div class="display-none">
<div typeof="v:Review-aggregate" xmlns:v="http://rdf.data-vocabulary.org/#">

            <span property="v:itemreviewed">Отзывы о Сайте</span>

            <span rel="v:rating">

            <span typeof="v:Rating">

            <span property="v:average"><?php echo (int)$rating; ?></span>

                        из

            <span property="v:best">5</span>

            </span>

            </span>

                        на основе

            <span property="v:votes"><?php echo (int)$number_rating; ?></span> оценок.

            <span property="v:count"><?php echo (int)$number_review; ?></span> клиентских отзывов.

</div>
</div>
<?php } ?>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
// start Tkach web-promo Dynamic price
$(document).ready(function(){
	isValidPrice();	
});
$('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea').change(function(){
	isValidPrice();
});
$('.radio-image').click(function(){
	isValidPrice();
});
function isValidPrice()
{
// $('#thisIsOriginal').html() - start price
// $('#thisIsOriginalspecial') - special price

var startPrise			= $('#thisIsOriginal').html();
var startPriseSpecial	= $('#thisIsOriginalspecial').html();
var newPrice			= parseInt(startPrise,10);
var startPriseSpecial	= parseInt(startPrise,10);
console.log(newPrice);

console.log($('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea').map(function() {
  
	switch($(this).attr('type')) {
  case 'radio':  	// if (x === 'radio')
  	radioPrice		= parseInt($(this).attr('price'),10);
  	if($.isNumeric(radioPrice)){
		newPrice		= newPrice + radioPrice;
		if (typeof newPriseSpecial !== 'undefined') {
			newPriseSpecial = newPriseSpecial + radioPrice;
		}
	}
    return radioPrice;
    break;
  case 'checkbox':  // if (x === 'checkbox')
    checkboxPrice	= parseInt($(this).attr('price'),10);
    if($.isNumeric(checkboxPrice)){
		newPrice		= newPrice + checkboxPrice;
		if (typeof newPriseSpecial !== 'undefined') {
			newPriseSpecial = newPriseSpecial + checkboxPrice;
		}
	}
    return checkboxPrice;
    break;
  default:
  	selectPrice		= parseInt($(":checked", this).attr('price'),10);
  	if($.isNumeric(selectPrice)){
		newPrice		= newPrice + selectPrice;
		if (typeof newPriseSpecial !== 'undefined') {
			newPriseSpecial = newPriseSpecial + selectPrice;
		}	
	}
   	return selectPrice;
    break;
    
    

  	}

}).get());
console.log(newPrice);
$("#newPrice").html(newPrice+" грн.");
	
};
// end web-promo
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
//	console.log($('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'));
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
                                $('#cart').addClass('open');

				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<i class="fa fa-shopping-cart"></i> ' + json['total']);

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				if( $('html').attr('lang') == 'uk' )
				{
				$('#cart > ul').load('/ua/index.php?route=common/cart/info ul li');		
				}else{
				$('#cart > ul').load('index.php?route=common/cart/info ul li');	
				}
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
//						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>&lang=<?=$curr_lang?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});


	var shtml = '';
	var pos = -1;
	var snone = 'display: none';
	var sblock = 'display: block';

	function changeDisplayAttr(e)
	{
		shtml = jQuery(e).html();
		pos = shtml.indexOf(snone);
		if (pos > 0){
			jQuery(e).html(shtml.replace( snone, sblock ));
		} else {
			pos = shtml.indexOf( sblock );
			if (pos > 0)
			jQuery(e).html(shtml.replace( sblock, snone ));
		}
	}

	jQuery('.delivery-div').on('click', function(){
		changeDisplayAttr(this);
	});
	jQuery('.description-div').on('click', function(){
		changeDisplayAttr(this);
	});
	jQuery('.options-div').on('click', function(){
		changeDisplayAttr(this);
	});
	jQuery('.installation-div').on('click', function(){
		changeDisplayAttr(this);
	});
});
</script>
<?php echo $footer; ?>