<h2><?php echo $text_instruction; ?></h2>
<p><b><?php echo $text_description; ?></b></p>
<div class="well well-sm">
  <p><?php echo $bank; ?></p>
  <p><?php echo $text_payment; ?></p>
</div>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>" />
  </div>
</div>
<script type="text/javascript"><!--
console.log($('html').attr('lang'));
	if( $('html').attr('lang') == 'uk' )
	{
		url1 = '/ua/index.php?route=payment/bank_transfer/confirm';
	}else
	{
		url1 = '/index.php?route=payment/bank_transfer/confirm';
	}
$('#button-confirm').on('click', function() {
	$.ajax({
		type: 'get',
		url: url1,
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			location = '<?php echo $continue; ?>';
		}
	});
});
--></script>
