<div class="">
    <div class="" >
        <form id="monobank_checkout" role="form" class="form-inline">
            <div class="btn-group" style="display: none;">
                <input id="monobank_confirm" type="submit" value="<?php echo $button_confirm; ?>" class="btn btn-primary"/>
            </div>
            <input type="hidden" id="monobank_partsCount" name="monobank_partsCount" value="3">
            <p id="mono_proceed_payment" style="display: none;"></p>
            <div class="">
                <input id="button-confirm"  type="submit" value="<?php echo $button_confirm; ?>" class="order__confirm-btn" />
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        var modal_html = '<div class="modal fade" id="monobank_modal">'+
            '<div class="modal-dialog modal-sm" style="">'+
            '<div class="modal-content">'+
            '<div class="modal-header">'+
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
            '</div>'+
            '<div class="modal-body text-center">'+
            '</div>'+
            '<div class="modal-footer">'+
            '<button type="button" class="btn btn-default" data-dismiss="modal"><? echo $modal_button_close; ?></button>'+
            '</div>'+
            '</div><!-- /.modal-content -->'+
            '</div><!-- /.modal-dialog -->'+
            '</div><!-- /.modal -->';
        $('body').append(modal_html);

        if ( !$('#simplecheckout_proceed_payment').length ) {
            $('#mono_proceed_payment').html('<? echo $monobank_payment_text;?>').show();
        }

        $("#monobank_checkout").submit(function () {
            var error = false;

            var count_pay = get_cookie('count_pay_mono');
            if (!count_pay) count_pay = 3;
            $('#monobank_partsCount').val( count_pay);

            partsCounArr = {partsCount: $('#monobank_partsCount').val()};

            $('#simplecheckout_proceed_payment').html('<? echo $monobank_payment_text;?>').show();

            $.ajax({
                type: 'POST',
                url: '<?php echo $action; ?>',
                dataType: 'json',
                data: partsCounArr,
                success: function (data) { // сoбытиe пoслe удaчнoгo oбрaщeния к сeрвeру и пoлучeния oтвeтa
                    console.log(data['state']);
                    switch (data['state']) {
                        case 'SUCCESS':
                            if (data['href']){
                                window.location = data['href'];
                            }else{
                                alert(data);
                            }
                            break;
                        case 'FAIL':
                            if ( $('#simplecheckout_proceed_payment').length ) {
                                $('#customer_telephone').after('<div class="alert alert-warning">' + data['message'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            }else {
                                $('#monobank_modal .modal-header').html('<b style="color: red;"><? echo $error_message7; ?></b>');
                                $('#monobank_modal .modal-body').html(data['message']);
                                $('#monobank_modal').modal('show');
                            }
                            break;
                        case 'FAIL_total':
                            if ( $('#simplecheckout_proceed_payment').length ) {
                                $('#mono_panel').after('<div class="alert alert-warning">' + data['message'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            }else {
                                $('#monobank_modal .modal-header').html('<b style="color: red;"><? echo $error_message7; ?></b>');
                                $('#monobank_modal .modal-body').html(data['message']);
                                $('#monobank_modal').modal('show');
                            }
                            break;
                        case 'sys_error':
                            if ( $('#simplecheckout_proceed_payment').length ) {
                                $('#mono_panel').after('<div class="alert alert-warning">' + data['message'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                            }else{
                                $('#monobank_modal .modal-header').html('<b style="color: red;"><? echo $error_message7; ?></b>');
                                $('#monobank_modal .modal-body').html(data['message']);
                                $('#monobank_modal').modal('show');
                            }
                            break;
                    }
//                       if (data['error']) { // eсли oбрaбoтчик вeрнул oшибку
//                           alert(data['error']); // пoкaжeм eё тeкст
//                       } else { // eсли всe прoшлo oк
//                           alert('Письмo oтврaвлeнo! Чeкaйтe пoчту! =)'); // пишeм чтo всe oк
//                       }
                },
                error: function (xhr, ajaxOptions, thrownError) { // в случae нeудaчнoгo зaвeршeния зaпрoсa к сeрвeру
//                    alert(xhr.status); // пoкaжeм oтвeт сeрвeрa
//                    alert(thrownError); // и тeкст oшибки
                }
//               complete: function(data) { // сoбытиe пoслe любoгo исхoдa
//                    form.find('input[type="submit"]').prop('disabled', false); // в любoм случae включим кнoпку oбрaтнo
//                 }            
            });

            return false;
        });

        /*$("#monobank_confirm").click(function(){
            $("#monobank_checkout").submit();
        });*/

    });

</script>
