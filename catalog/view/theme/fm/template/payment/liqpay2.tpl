<form action="<?php echo $action; ?>" method="post" class="liqpay-method">
  <input type="hidden" name="data" value="<?php echo $data; ?>">
  <input type="hidden" name="signature" value="<?php echo $signature; ?>">
  <div class="buttons">
    <div class="pull-right">
      <input type="submit" value="<?php echo $button_confirm_p; ?>" class="btn btn-primary lqpayform-sbth" />
    </div>
  </div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
		//$('#instock-check').attr("disabled", true);
        $(".lqpayform-sbth").click(function(event){
			var curr_paymethod = $('.payment-method input[name=payment_method]:checked').val(); 
			var liqpay_regl = $('#instock-check').prop('checked'); 							
				$.ajax({
					type: 'get',
					url: '<?php echo $url_confirm; ?>',
					success: function() {
						$("form.liqpay-method").submit();
					}
				});
				return false;
        });
    });
</script>
