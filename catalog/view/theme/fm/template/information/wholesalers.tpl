<?php echo $header; ?>
<!-- Сем. разметка -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "с.Софиевская Борщаговка",
    "streetAddress": "ул. Ягодная 18"
  },
  "email": "info@feshemebel.com.ua",
  "name": "Интернет магазин мебели Фешемебельный"
}
</script>
<!-- Сем. разметка -->

<script>
    gtag('event', 'page_view', {
        'send_to': 'AW-971613570',
        'ecomm_prodid' : '',
    'ecomm_pagetype': 'other',

    });
</script>
<div class="container">

    <!-- breadcrumbs -->
    <section class="breadcrumbs ">
        <div class="wrapper">
            <ul class="breadcrumbs__items" itemscope itemtype="https://schema.org/BreadcrumbList">

                <?php $breadcrumbs_item = 0;
			 foreach ($breadcrumbs as $breadcrumb) { ?>
                <li class="breadcrumbs__item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        <meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a>
                </li>
                <?php } ?>
                <!--ss_breadcrums_list:<?php foreach ($breadcrumbs as $k=>$breadcrumb) { ?><?php echo $breadcrumb['text']; ?><?php if(count($breadcrumb) != $k) { ?> >> <?php } ?><?php } ?>-->
            </ul>
        </div>
    </section>
    <!-- end breadcrumbs -->

  <div class="row row-contacts"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" >
      <?php echo $content_top; ?>
        <style>
            h1
            {
                text-align: left;
                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 30px;
                line-height: 40px;
                letter-spacing: -0.5px;
                color: #121726;
                margin-left: 25px;
                padding-top: 70px;
                padding-bottom: 48px;
            }
            .left-block
            {
                display: inline-block;
                vertical-align: top;
                width: 270px;
                padding-top: 35px;
                border-top: 1px solid #C8CBD1;
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 24px;
                color: #535F6E;
                font-family: "GothamProMedium", sans-serif;
                margin-left: 25px;
            }
            .phone-free
            {
                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 24px;
                color: #121726;
            }
            .mail-box
            {
                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 24px;
                color: #121726;
            }
            .right-block
            {
                display: inline-block;
                vertical-align: top;
                width: calc(100% - 355px);
                margin-left: 60px;
                padding-top: 35px;
                border-top: 1px solid #C8CBD1;

                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 24px;

                color: #535F6E;
            }
            .stagh2
            {
                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 17px;
                line-height: 22px;


                color: #121726;
                padding-bottom: 24px;
                display: block;
            }
            ul
            {
                padding-bottom: 37px;
            }
            ul li
            {
                padding-bottom: 11px;
            }

            .right-block p
            {
                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 14px;
                line-height: 24px;
                color: #535F6E;
            }
            .form-control
            {
                height: 44px;
                padding: 20px 15px;
                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 13px;
                line-height: 18px;
                /* or 138% */

                display: flex;
                align-items: center;

                /* Secondary / Medium Grey */

                color: #808994;
                width: 100%;
                margin-top: 26px;

            }
            select.form-control, textarea.form-control, input[type="text"].form-control, input[type="password"].form-control, input[type="datetime"].form-control, input[type="datetime-local"].form-control, input[type="date"].form-control, input[type="month"].form-control, input[type="time"].form-control, input[type="week"].form-control, input[type="number"].form-control, input[type="email"].form-control, input[type="url"].form-control, input[type="search"].form-control, input[type="tel"].form-control, input[type="color"].form-control
            {
                font-size: 13px;
            }
            .form-horizontal .form-group
            {
                margin: 0;
            }
            .btn-submit
            {
                font-family: "GothamProMedium", sans-serif;
                font-style: normal;
                font-weight: normal;
                font-size: 11px;
                line-height: 11px;

                align-items: center;

                letter-spacing: 1px;
                text-transform: uppercase;

                /* Primary / Deep Navy */

                color: #121726;
                width: 100%;
                height: 44px;
                text-align: center;
                display: block;

            }
            @media (max-width: 1024px) {
                h1
                {
                    padding-top: 20px;
                    padding-bottom: 20px;
                }
                .left-block
                {
                    width: auto;
                    display: block;
                    margin: 0;
                }
                .right-block
                {
                    width: auto;
                    display: block;
                    margin: 0;
                    padding-top: 10px;
                    border: none;
                }
            }
        </style>
      <h1><?php echo $heading_title; ?></h1>
        <div>
            <div class="left-block">
                <?=$text_left;?>
            </div><div class="right-block">
                <?=$text_right;?>

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" style="max-width: 520px;">
                    <input type="hidden" class="form-url" name="url" value="" />
                    <div class="form-group required">
                        <input placeholder="<?php echo $entry_name; ?>" type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
                        <?php if ($error_name) { ?>
                        <div class="text-danger"><?php echo $error_name; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group required">
                        <input placeholder="<?php echo $entry_email; ?>" type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
                        <?php if ($error_email) { ?>
                        <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group required">
                        <input placeholder="<?php echo $entry_phone; ?>" type="text" name="phone" value="<?php echo $phone; ?>" id="input-phone" class="form-control" />
                        <?php if ($error_phone) { ?>
                        <div class="text-danger"><?php echo $error_phone; ?></div>
                        <?php } ?>
                    </div>
                    <div class="form-group required">
                        <textarea placeholder="<?php echo $entry_enquiry; ?>" name="enquiry" rows="5" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
                        <?php if ($error_enquiry) { ?>
                        <div class="text-danger"><?php echo $error_enquiry; ?></div>
                        <?php } ?>
                    </div>
                    <?php echo $captcha; ?>

                    <div class="buttons">
                        <input class="btn-submit" type="submit" value="<?php echo $button_submit; ?>" />
                    </div>
                </form>

            </div>
        </div>

    </div>
     
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
