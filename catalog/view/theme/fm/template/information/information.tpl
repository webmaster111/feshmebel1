<?php echo $header; ?>
<script>
		gtag('event', 'page_view', {
				'send_to': 'AW-971613570',
				'ecomm_prodid' : '',
				'ecomm_pagetype': 'other',

		});
</script>
<div class="container">
	<ul class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<?php $breadcrumbs_item = 0;
		 foreach ($breadcrumbs as $breadcrumb) { ?>
		<li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		<a href="<?php echo $breadcrumb['href']; ?>" itemprop="item">
		<span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
		<meta itemprop="position" content="<?php echo ++$breadcrumbs_item; ?>" /></a></li>
		<?php } ?>
	</ul>
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-sm-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-sm-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
		<div id="content" class="text-page">
			<?php echo $content_top; ?>
			<h1><?php echo $heading_title; ?></h1>
			<?php echo $description; ?><?php echo $content_bottom; ?>
		</div>
		<!-- <?php echo $column_right; ?> -->
	</div>
</div>
<?php echo $footer; ?>
