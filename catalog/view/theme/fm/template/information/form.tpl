

<style>
	.btn-error-form{
		position: fixed;
	    bottom: 10px;
	    right: 20px;
	    width: 86px;
	    font-size: 11px;
	    background-color: #ffd460;
	    border: 1px solid #b19344;
	    border-radius: 5px;
	    padding: 5px 0;
	    font-weight: bold;
	    z-index: 2;
	}
	#myModal .modal-dialog
	{
		max-width:400px;
		
	}
	#myModal .modal-dialog .form-group>.control-label
	{
		font-size: 14px;
	}
	#myModal .modal-dialog .btn-default, 
	#myModal .modal-dialog .btn-primary
	{
		font-size: 10px;
		padding: 10px;
		margin: 5px;
	}
</style>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $text_contact; ?> </h4>
      </div>
      <div class="modal-body">
      	<div class="col-xs-12">
			<form id="idForm" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
				<input type="hidden" class="form-url" name="url" value="" />
		      <div class="form-group required">
		        <label class=" control-label" for="input-name"><?php echo $entry_name; ?></label>
		        <div class="name">
		          <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control" />
		          <?php if ($error_name) { ?>
		          <div class="text-danger"><?php echo $error_name; ?></div>
		          <?php } ?>
		        </div>
		      </div>
		      <div class="form-group required">
		        <label class=" control-label" for="input-email"><?php echo $entry_email; ?></label>
		        <div class="email">
		          <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="form-control" />
		          <?php if ($error_email) { ?>
		          <div class="text-danger"><?php echo $error_email; ?></div>
		          <?php } ?>
		        </div>
		      </div>   
		      <div class="form-group required"> 
		        <label class=" control-label" for="input-phone"><?php echo $entry_phone; ?></label>
		        <div class="phone">
		          <input type="text" name="phone" value="<?php echo $phone; ?>" id="input-phone" class="form-control" />
		          <?php if ($error_phone) { ?>
		          <div class="text-danger"><?php echo $error_phone; ?></div>
		          <?php } ?>
		        </div>
		      </div>
		      <div class="form-group required">
		        <label class=" control-label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
		        <div class="enquiry ">
		          <textarea name="enquiry" rows="5" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
		          <?php if ($error_enquiry) { ?>
		          <div class="text-danger"><?php echo $error_enquiry; ?></div>
		          <?php } ?>
		        </div>
		      </div>
		      <?php echo $captcha; ?>
		   
		    <div class="buttons">
		      <div class="pull-right">
		      	<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
		        <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
		      </div>
		    </div>
		  </form>
		  </div>
      </div>
      <div class="modal-footer">
        
        
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
	var text_success = '<?=$text_success;?>';
	$("#input-phone").inputmask("+38(999)999-99-99");
	$("#idForm").submit(function(e) {
		$('#idForm .form-url').val(window.location.href);
	    e.preventDefault(); // avoid to execute the actual submit of the form.
	
	    var form = $(this);
	    var url = form.attr('action');

    	$.ajax({
           type: "POST",
           url: url,
           data: form.serialize(), 
           dataType: 'json',
           success: function(data)
           {
           	  $('.text-danger').remove();	
           	  if (data.status == 'error') {
           	  	  $.each(data.error, function(index, value) {
           	  	  	$('.'+index).append('<div class="text-danger">'+value+'</div>')
           	  	  	
				}); 
           	  }
           	  else
           	  {
           	  	$('#myModal .modal-body').html(text_success);
           	  }
            
           }
         });


	});
});
</script>
