<div class="order__promo">
  <div class="order-promo-form">
    <input type="text" name="coupon" value="<?php echo $coupon; ?>" placeholder="<?php echo $entry_coupon; ?>" id="input-coupon" class="order__coupon-code" />
  </div>
  <div class="order__promo-code-submit">
    <button type="submit" id="button-coupon"  class="order__promo-code-btn">Применить</button>
  </div>
</div>

<script type="text/javascript"><!--
  $('#button-coupon').on('click', function() {
    $.ajax({
      url: 'index.php?route=total/coupon/coupon',
      type: 'post',
      data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
      dataType: 'json',
      beforeSend: function() {
        $('#button-coupon').button('loading');
      },
      complete: function() {
        $('#button-coupon').button('reset');
      },
      success: function(json) {

        $('.alert').remove();

        if (json['error']) {
          $('.order__promo').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

          //$('html, body').animate({ scrollTop: 0 }, 'slow');
        }
        else
          update_checkout();
        if (json['redirect']) {
         // location = json['redirect'];

        }

      }
    });
  });
  //--></script>