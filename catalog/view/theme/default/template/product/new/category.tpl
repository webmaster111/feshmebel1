<?php echo $header; ?>
<div class="container">
  <div class="row">
  <ul class="breadcrumb col-sm-3">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <h2 class="bordered-title col-sm-3"><?php echo $heading_title; ?></h2>
  <?php if ($products) { ?>
      <div class="col-sm-6">

        <!-- <div class="col-md-4 hhh">
          <div class="btn-group hidden-xs">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div> -->
        <div class="row">
          <div class="col-md-6">
            <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
          </div>
          <div class="col-md-6">
            <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
          </div>
        </div>
        <div class="row">
<div class="col-md-6 text-right">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        
        <div class="col-md-6 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        </div>
        

      </div>
  </div>

  <div class="row">
    <div id="content" class="col-md-9 col-sm-8">
	<?php echo $content_top; ?>
 <?php if ($categories) { ?>
  <h2><?php echo $text_refine; ?></h2>
  <div class="category-list">
    <ul>
      <?php foreach ($categories as $category) { ?>
      <li class="col-20-perc">
	  <?php
	  if(!empty($category['thumb']))
	  {
	  ?>
      <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive imggg" /></a>
	  <?php
	  }
	  ?>
      <a class="padding-cat-a" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
      </li>
      <?php } ?>
    </ul>
    
  </div>
  <?php } ?>
      <div class="row">

	<?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
     <!--  <?php echo $class; ?> -->
	  

        <?php $cInRow = 1 ?>
        <?php foreach ($products as $product) { ?>
		<?php 	$cInRow++;
				if ($cInRow>4) {
					echo '</div><div class="row">';
					$cInRow = 1;
				}
		?>
		
        <div class="product-layout product-list col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="lable">
                <?php if($product['bestseller']){ ?>
                <img class="bestseller" src="image/catalog/lable/bestseller.png" alt="bestseller">
                <?php } ?>
                <?php if($product['recommended']){ ?>
                <img class="recommended" src="image/catalog/lable/recommended.png" alt="recommended">
                <?php } ?>
            </div>
		<?php
			switch ($product['stock_status_id']){
				case 5:		// Нет в наличии
					break;
				case 6:		// Ожидание 2-3 дня
					break;
				case 7:		// В наличии
					break;
				case 8:		// Предзаказ
					break;
				case 9:		// Снято с продажи
					break;
			};	
		?>
          <div class="product-thumb" style="position:absolute;">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
<div id="triangle-topleft">
<div class="rotatedBlock-100">
<font style="font-size: 15px;">Ожидается</font>
</div>
</div>
			</div>
            <div>
              <div class="caption">
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p><?php echo $product['description']; ?></p>

                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } else { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
              </div>
                <?php foreach ($product['options'] as $option) { ?>    
                     <?php if ($option['type'] == 'select') { ?>
                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> variationProdCat">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <div class="varSelectWrap">
                  <select name="option[<?php echo $option['product_option_id']; ?>] " id="input-option<?php echo $option['product_option_id']; ?>" class="form-control varSelect">
                    <option value=""><?php echo $text_select; ?></option>
                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                    </option>
                    <?php } ?>
                  </select>
                  </div>
                </div>
            <?php } ?>
			
			 <?php if ($option['type'] == 'image') { ?>
             <!-- <p class="chose-option">Выберете цвет:</p> -->
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> variationProdCat">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio radio-image">
                  <label>
                    <input class="radio-img" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <span class="hidden-descr"><?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?></span>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>

            <?php } ?>
             
              <div class="button-group">
                <button class="button-cart-p" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <div class="clearfix wish-buttons-card">
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');">В желания</button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');">В сравнения</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
		</div>
      </div>
      <div class="row">
        <div class="col-sm-12 text-right"><?php echo $pagination; ?></div>
        <!-- <div class="col-sm-6 text-right"><?php echo $results; ?></div> -->
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
