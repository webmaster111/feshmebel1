<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3 sm-c">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3 sm-c">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="index.php?route=information/information&information_id=10">О нас</a></li>
          <li><a href="novosti">Статьи</a></li>
          <li><a href="kontakty">Контакты</a></li>
          <!-- <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li> -->
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>

      <div class="col-sm-2 footer-soc">
        <h5><?php echo $text_social; ?></h5>
        <div class="soc-icons">
          <ul>
            <div class="soc-ico">
              <a href="#">
               <i class="fa fa-vk" aria-hidden="true"></i>
              </a>
            </div>
            <div class="soc-ico">
              <a href="#">
                <i class="fa fa-facebook" aria-hidden="true"></i>
              </a>
            </div>
            <!-- <div class="soc-ico">
              <a href="#">
                <i class="fa fa-twitter" aria-hidden="true"></i>
              </a>
            </div> -->
            <div class="soc-ico">
              <a href="#">
                <i class="fa fa-google-plus" aria-hidden="true"></i>
              </a>
            </div>
          </ul>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="col-sm-6 w100">
          <div class="footer-phones text-right">
<!--		  
            <p><i class="fa fa-phone" aria-hidden="true"></i>+38(044) 383-83-85</p>
            <p class="second-phone mrph"><i class="fa fa-phone" aria-hidden="true"></i>+38(068) 510-81-99 </p>
            <p><i class="fa fa-phone" aria-hidden="true"></i>+38(095) 655-94-21</p>
-->			
            <p class="footer-mail mrph"><a href="#">info@roomix.com.ua</a></p>
          </div>
        </div>
        <div class="col-sm-6 w100 w1002">
          <p class="footer-info"><?php echo $text_work; ?></p>
          <p class="footer-info"><?php echo $text_day_off; ?> </p>
          <div class="footer-call text-left">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <a href="/" class="contact-btn"><?php echo $text_call; ?></a>
          </div>
        </div>
      </div>
      <!-- <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div> -->
      <!-- <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div> -->
    </div>
    <hr>
    <div class="footer-copy-wrap clearfix">
      <p class="copyright-footer">2016 <i class="fa fa-copyright" aria-hidden="true"></i> <a href="#">Roomix.com.ua</a> <?php echo $text_shop; ?></p>
      <p class="powered-by-oxis">Разработано Web Studio <a href="http://www.oxis.com.ua" target="_blank">OXIS</a></p>
    </div>
  </div>
</footer>

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->

</body></html>