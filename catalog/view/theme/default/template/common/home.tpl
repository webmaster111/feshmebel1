<?php echo $header; ?>
<?php
		require_once($_SERVER['DOCUMENT_ROOT'].'/tools/functions.php');
		
		$res = read_table('*', 'oc_option_value',"`option_id` in (21,22)");
		$options = array();
		if (isset($res[0]))
		foreach($res as $r)
			$options[$r['option_id']] = $r;
		$res = read_table('*', 'oc_option_value_description',"`option_id` in (21,22)");
		foreach($res as $r){
			$options[$r['option_id']]['name'] = $r['name'];
		}	
		$href = '//'.$_SERVER['SERVER_NAME'].'/image/';
?>
<div class="container">
    <div class="row">
		<div class="row">
			<div style="float: left; width:50%;">
			<?php  $image['src'] = $options[21]['image']; ?>
			<?php  $image['title'] = $options[21]['name']; ?>
			<a href="<?php echo $informations[13]['href']; ?>"><img src="<?php echo $href.$image['src']; ?>" alt="<?php echo $image['title']; ?>" class="img-responsive" /></a>
			</div>
			<div style="float: left; width:50%;">
			<?php  $image['src'] = $options[22]['image']; ?>
			<?php  $image['title'] = $options[22]['name']; ?>
			<a href="<?php echo $informations[14]['href']; ?>"><img src="<?php echo $href.$image['src']; ?>" alt="<?php echo $image['title']; ?>" class="img-responsive" /></a>
			</div>
		</div>
	
		<div class="row">
			<div id="content" class="col-sm-12"><?php echo $content_bottom; ?></div>
			<?php echo $column_right; ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>
