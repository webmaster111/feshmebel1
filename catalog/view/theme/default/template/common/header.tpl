<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<!-- jQuery library (served from Google) -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> -->
<!-- bxSlider Javascript file -->
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> -->

<!-- bxSlider CSS file -->
<link href="catalog/view/theme/default/stylesheet/jquery.bxslider.css" rel="stylesheet" />
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>

<script src="catalog/view/javascript/jquery.bxslider.min.js"></script>

<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/fonts.css">


<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

<script type="text/javascript" src="catalog/view/javascript/callback.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery.simplemodal.js"></script>
<link href="catalog/view/theme/default/stylesheet/callback.css" rel="stylesheet" type="text/css" />
</head>
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container" style="padding-top: 15px !important;">
    <?php echo $currency; ?>
    <?php echo $language; ?>
    <div id="top-links" class="nav">
<!-- <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?> -->
      <ul class="list-inline float-menu">
       <!-- <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li> -->
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a> 
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <!-- <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $compare; ?>" id="compare-total" title="<?php echo $text_compare; ?>" class="header-product-btn-i"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_compare; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li> -->
      </ul> 
      <div class="col-md-6 top-links-inf">
        <ul class="">
          <li><a href="index.php?route=information/information&information_id=8">Помощь</a></li>
          <li><a href="index.php?route=information/information&information_id=4">Доставка и оплата</a></li>
        </ul>
      </div>
      
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="header-wrap clearfix">
      <div class="col-sm-3 col-xs-12">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div> 

        
        <div class="contacts-wrap clearfix">
        <div class="col-md-5  contacts-md-wrap">
<!--		  
          <div class="col-md-6 contacts-md">
            <div class="phones">
              <p><i class="fa fa-phone" aria-hidden="true"></i>+38(068) 510-81-99</p>
              <p><i class="fa fa-phone" aria-hidden="true"></i>+38(068) 510-81-99</p>
              <p><i class="fa fa-phone" aria-hidden="true"></i>+38(095) 655-94-21</p>
            </div>
          </div>
-->			
          <div class="col-md-6 contacts-md">
            <div class="order-call">
              <p><?php echo $text_work; ?></p>
              <p><?php echo $text_day_off; ?></p>
              <p class="order-call-btn"><i class="fa fa-phone" aria-hidden="true"></i><a class="contact-btn" href="/"><?php echo $text_call; ?></a></p>
            </div>
          </div>
        </div>
        <div class="col-xs-1 wish-list">
          <a class="bookmark" href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a>
          <a class="compare" href="<?php echo $compare; ?>" id="compare-total" title="<?php echo $text_compare; ?>" class="header-product-btn-i"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_compare; ?></span></a>
        </div>
		<div class="col-sm-3 cart-sm">
			<div class="cart-wrap"><?php echo $cart; ?></div>
			<div><?php echo $search; ?></div>
			<div style="display: none;" class="search-result-ajax" id="search-result-ajax">
		</div>				
 <!--   
 <div class="cart-links" id="cart">
          <a class="cart-link" href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a>
          <a class="approve-order" href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a>
        </div> -->
      </div>
      </div>
    </div>
  </div>
  <div class="row">
    <ul class="header-list-p">
      <li><a href="index.php?route=information/information&information_id=10">О нас</a></li>
      <li><a href="index.php?route=information/information&information_id=9">Гарантия</a></li>
      <li><a href="index.php?route=account/return/add">Обмен и Возврат</a></li>
      <li><a href="index.php?route=information/information&information_id=11">Система скидок</a></li>
      <li><a href="novosti">Статьи</a></li>
    </ul>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">

    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>

<?php } ?>
