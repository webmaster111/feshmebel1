<p style="font-size: 85%; line-height: normal;">
	<?=$text_secure;?>
</p>

<form action="<?=($live_demo > 0) ? $live_url : $demo_url;?>" id="pumb" method="GET" accept-charset="utf-8">
	<input type="hidden" value="<?=$merch_id;?>" name="merch_id" />
	<input type="hidden" value="<?=$account_id;?>" name="o.account_id" />
	<input type="hidden" value="<?=$order_info['total'];?>" name="o.amount" />
	<input type="hidden" value="<?=$order_info['order_id'];?>" name="o.order_id" />
	<input type="hidden" value="<?=$order_info['language_code'];?>" name="lang" />
	<input type="hidden" value="<?=$back_url_s;?>" name="back_url_s" />
	<input type="hidden" value="<?=$back_url_f;?>" name="back_url_f" />
	<div class="buttons">
		<div class="pull-right">
		<input type="submit" value="<?php echo $button_confirm; ?>" class="btn btn-primary" />
		</div>
    </div>
</form>
<script type="text/javascript">
	$.ajax({
		type: 'get',
		url: 'index.php?route=payment/pumb/confirm',
		cache: false,
		success: function() {
			$('#pumb').submit();
		}
	});
</script>