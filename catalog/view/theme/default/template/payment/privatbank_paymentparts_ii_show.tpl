<div class="">
	<img class="centered" src="catalog/view/javascript/jquery/pp_calculator/img/ip_logo.png" width="50px">
	<span class="form-element" id="paymentsCountcart"></span>, на
	<select id="termInput" name="partsCount_ii" class="form-control">
		<?php for($i=1;$i<=$partsCounts;$i++){ ?>
		<option><?php echo $i; ?></option>
		<?php } ?>
	</select> <?php echo $text_mounth; ?> по <span id="ipResultValue">0</span> <span class="result-additional-data-phys"></span>
	<input type="hidden" pattern="\d*" id="priceInput" value="<?php echo $total; ?>">
</div>

<div id="activatescript"></div>
<script type="text/javascript" src="catalog/view/javascript/jquery/pp_calculator/calculator.js"></script>
<script>
var curLang="<?php echo $language; ?>"
var constants = {
    'priceMax': 100000,
	'priceInitial': <?php echo $total; ?>,
	'priceInitial2': <?php echo $total; ?>,
    'termMax': <?php echo $partsCounts; ?>,
	'termMax2': <?php echo $partsCounts; ?>,
    'termMin': <?php echo ($partsCountSel ? $partsCountSel : '1'); ?>,
	'termMin2': <?php echo ($partsCountSel ? $partsCountSel : '1'); ?>,
    'termStep': 1,
	'termStep2': 1
};		
$('#activatescript').trigger('click');
<?php if ($partsCountSel) { ?>
$("#termInput").val(<?php echo $partsCountSel; ?>).trigger('change');
<?php } ?>
</script>

