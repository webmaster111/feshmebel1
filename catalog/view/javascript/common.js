function alertObj(obj) {
    var str = "";
    for (k in obj) {
        str += k + ": " + obj[k] + "\r\n";
    }
}

function getURLVar(key) {
    var value = [];
    var query = String(document.location).split('?');
    if (query[1]) {
        var part = query[1].split('&');
        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');
            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }
        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    } else {
        var query = String(document.location.pathname).split('/');
        if (query[query.length - 1] == 'cart') value['route'] = 'checkout/cart';
        if (query[query.length - 1] == 'checkout') value['route'] = 'checkout/checkout';
        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).ready(function () {
    var langPage = $('html').attr('lang');
    switch (langPage) {
        case'ru':
            $('.tc-sm').html('Супер цена');
            break;
        case'uk':
            $('.tc-sm').html('Супер ціна');
            break;
    }
    switch (langPage) {
        case'ru':
            $('.text-ukru').after('НАЙТИ');
            break;
        case'uk':
            $('.text-ukru').after('ЗНАЙТИ');
            break;
    }
    switch (langPage) {
        case'ru':
            $('.textcatbutton a').html('<span>Все статьи</span>');
            break;
        case'uk':
            $('.textcatbutton a').html('<span>Всі статті</span>');
            break;
    }
    $('.related-goods + .row > div').attr('class', 'col-lg-3 col-md-3 col-sm-6 col-xs-12');
    $('.wish-buttons-card button').attr('data-toggle', ' ');
    $('.nav.navbar-nav .dropdown-menu').mouseenter(function () {
        $(this).parent().addClass('ddwned');
    });
    $('.nav.navbar-nav .dropdown-menu').mouseleave(function () {
        $(this).parent().removeClass('ddwned');
    });
    $('.textcatbutton a').removeClass('button btn btn-primary');
    $('.content-records').removeClass('column_width_3');
    $('.contact-content form > input.q2').removeClass('q2');
    $('.seocmspro_content').parent().addClass('container');
    $('.how-to-pay-title').click(function () {
        $('.how-to-pay').slideToggle();
    });
    $('.mfilter-head-icon').addClass('fa fa-angle-down');
    $('.bxslider').bxSlider({slideWidth: 420, maxSlides: 4, moveSlides: 1, pager: false, controls: true});
    $('.mfilter-tb-as-td.mfilter-col-input').append('<label class="chechbox-styled"></label>');
    $('#column-left').attr('class', 'col-md-3 col-sm-4');
    $('.feedback-title').click(function () {
        $(this).find('i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
    });
    $('.feedback-title-descr').click(function () {
        $('#tab-description').slideToggle(400);
    });
    $('.feedback-title-descr-1').click(function () {
        $('#tab-review').slideToggle(400);
    });
    $('body').append('<div class="layout"></div>');
    $('#open-cart-click').click(function () {
        //$('.layout').css('display', 'block');
    });
    $('.cart-compare-button-purchase').click(function () {
        //$('.layout').css('display', 'block');
    });
    $('.dropdown-toggle-cart').click(function () {
        //$('.layout').css('display', 'block');
    });
    $('.button-group button.button-cart-p').click(function () {
        //$('.layout').css('display', 'block');
    });
    $('.button-group button.button-cart-p').click(function () {
        $('#open-cart-click + ul').css('display', 'block');
    });
    $('#button-cart').click(function () {
        //$('.layout').css('display', 'block');
    });
    $('.cart-span').click(function () {
        //$('.layout').css('display', 'block');
        $('#open-cart-click + ul').css('display', 'block');
    });
    $('.mb-header__cart .checkout-box').click(function () {
        //$('.layout').css('display', 'block');
        $('.mb-header__cart #open-cart-click + ul').css('display', 'block');
    });
    $('.btn-combo.btn-combo-cart').click(function () {
        //$('.layout').css('display', 'block');
        $('#open-cart-click + ul').css('display', 'block');
    })
    $('#button-cart').click(function () {
        $('#open-cart-click + ul').css('display', 'block');
    });
    $('#open-cart-click').click(function () {
        $('#open-cart-click + ul').css('display', 'block');
    });
    $('continue-purchases-button').click(function () {
        $('#open-cart-click + ul').css('display', 'none');
    });
    $('svg').click(function () {
        $('#open-cart-click + ul').css('display', 'none');
        //$('.layout').css('display', 'none');
    });
    $('.dropdown-cart-title').prepend('<div class="ddclose"></div>');
    $('body').click(function () {
        $('#combo-notification').css('display', 'none');
    });
    $('.close-ddm').click(function () {
        $('#open-cart-click + ul').css('display', 'none');
        //$('.layout').css('display', 'none');
    });
    $('.button-cart-p').click(function () {
        $('#open-cart-click + ul').css('display', 'block');
        //$('.layout').css('display', 'block');
    });
    $('#open-cart-click + ul').click(function () {
    });
    $('.another-button-cart').click(function () {
        //$('.layout').css('display', 'block');
        $('#open-cart-click + ul').css('display', 'block');
    });
    $('.ddclose').click(function () {
        $('#open-cart-click + ul').css('display', 'none');
        //$('.layout').css('display', 'none');
    });
    $('.visible-lg.clearfix').remove();
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();
        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });
    $('#currency .currency-select').on('click', function (e) {
        e.preventDefault();
        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));
        $('#currency').submit();
    });
    $('#language a').on('click', function (e) {
        e.preventDefault();
        $('#code').attr('value', $(this).attr('href'));
        $('#language').submit();
    });
    $('#search input[name=\'search\']').parent().find('.header-search__search-btn').on('click', function () {
        url = url_search;
        var value = $('#search input[name=\'search\']').val();
        if (value.length > 0) {
            url = url_search + '?search=' + encodeURIComponent(value);
        }
        location = url;
    });
    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('#search input[name=\'search\']').parent().find('.header-search__search-btn').trigger('click');
        }
        ;
    });
    $('#search input[name=\'search\']').on('keyup', function () {
        var t = $(this), o = t.val(), e = t.parent().next();
        o.length > 2 && $.get("/tools/ajax.php?search_string=" + o, function (t) {
            t ? (e.html(t), e.show()) : e.hide()
        })
    });
    $('.search-input, .header-search__input').on('keyup', function () {
        var t = $(this), o = t.val(), e = t.parent().next();
        if (e.keyCode == 13) {
            $('#search input[name=\'search\']').parent().find('.header-search__search-btn').trigger('click');
            return true;
        }
        ;
        if (o.length > 2) {
            $.ajax({
                url: '/tools/ajax.php?search_string=' + o, type: 'get', dataType: 'html', success: function (div) {
                    $('#search-result-ajax').html(div);
                    $('#search-result-ajax').css('display', 'block');
                }
            })
        }
    });
    $(document).mouseup(function (e) {
        var container = $("#search-result-ajax");
        if (container.has(e.target).length === 0) {
            container.hide();
        }
    });
    $('#list-view').click(function () {
        $('#content .product-grid > .clearfix').remove();
        $('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
        localStorage.setItem('display', 'list');
    });
    $('#grid-view').click(function () {
        cols = $('#column-right, #column-left').length;
        if (cols == 2) {
            $('#content .product-list').attr('class', 'pr-l-c product-layout product-grid col-lg-4 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('#content .product-list').attr('class', 'pr-l-c product-layout product-grid col-lg-4 col-md-6 col-sm-12 col-xs-12');
        } else {
            $('#content .product-list').attr('class', 'pr-l-c product-layout product-grid col-lg-3 col-md-3 col-sm-12 col-xs-12');
        }
        localStorage.setItem('display', 'grid');
    });
    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
    } else {
        $('#grid-view').trigger('click');
    }
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });
});
var cart = {
    'add': function (product_id, option, quantity) {
        checked = $('#input-option' + option + ' input[type=\'radio\']:checked').val();
        ///console.log('product_id=' + product_id + '&option[' + option + ']=' + checked + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1));
        $.ajax({
            url: '/index.php?route=checkout/cart/add', type: 'post', data: 'product_id=' + product_id + '&option[' + option + ']=' + checked + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1), dataType: 'json', beforeSend: function () {
                $('#cart > button').button('loading');
            }, complete: function () {
                $('#cart > button').button('reset');
            }, success: function (json) {
                $('.alert, .text-danger').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    console.log('AddToCart');
                    update_checkout();

                    fbq('track', 'AddToCart');
                    $('#cart').addClass('open');
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    setTimeout(function () {
                        $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    }, 100);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                    if ($('html').attr('lang') == 'uk') {
                        $('#cart > ul').load('/ua/index.php?route=common/cart/info ul li');
                    } else {
                        $('#cart > ul').load('/index.php?route=common/cart/info ul li');
                    }



                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'productadd': function (product_id, radio, select) {
        option = '';
        if (typeof (radio) != "undefined" && radio != '' && radio !== null) {
            radio = radio + '';
            radio = radio.split(', ');
            radio.filter(function (number) {
                option = option + '&option[' + number + ']=' + $('#input-option' + number + ' input[type=\'radio\']:checked').val();
            });
        }
        if (typeof (select) != "undefined" && select != '' && select !== null) {
            select = select + '';
            select = select.split(', ');
            select.filter(function (number) {
                option = option + '&option[' + number + ']=' + $('#input-option' + number).val();
            });
        }
        $.ajax({
            url: '/index.php?route=checkout/cart/add', type: 'post', data: 'product_id=' + product_id + '&' + option + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1), dataType: 'json', beforeSend: function () {
                $('#cart > button').button('loading');
            }, complete: function () {
                $('#cart > button').button('reset');
            }, success: function (json) {
                $('.alert, .text-danger').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {

                    update_checkout();

                    $('#cart').addClass('open');
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    setTimeout(function () {
                        $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    }, 100);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                    if ($('html').attr('lang') == 'uk') {
                        $('#cart > ul').load('/ua/index.php?route=common/cart/info ul li');
                    } else {
                        $('#cart > ul').load('/index.php?route=common/cart/info ul li');
                    }
                    console.log('AddToCart');
                    fbq('track', 'AddToCart');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'update': function (key, quantity) {
        $.ajax({
            url: '/index.php?route=checkout/cart/edit', type: 'post', data: 'key=' + key + '&quantity=' + (typeof (quantity) != 'undefined' ? quantity : 1), dataType: 'json', beforeSend: function () {
                $('#cart > button').button('loading');
            }, complete: function () {
                $('#cart > button').button('reset');
            }, success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    if ($('html').attr('lang') == 'uk') {
                        $('#cart > ul').load('/ua/index.php?route=common/cart/info ul li');
                    } else {
                        $('#cart > ul').load('/index.php?route=common/cart/info ul li');
                    }
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'updatequantity': function (key, quantity, mincount) {
        if (quantity < mincount) {
            $('input[name="' + key + '"]').val(mincount);
            quantity = mincount;
        }
        $.ajax({
            url: '/index.php?route=checkout/cart/myEditCart', type: 'post', data: key + '=' + (typeof (quantity) != 'undefined' ? quantity : 1), dataType: 'json', beforeSend: function () {
                $('#cart > button').button('loading');
            }, complete: function () {
                $('#cart > button').button('reset');
            }, success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if ($('html').attr('lang') == 'uk') {
                    $('#cart > ul').load('/ua/index.php?route=common/cart/info ul li');
                } else {
                    $('#cart > ul').load('/index.php?route=common/cart/info ul li');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function (key) {
        $.ajax({
            url: '/index.php?route=checkout/cart/remove', type: 'post', data: 'key=' + key, dataType: 'json', beforeSend: function () {
                $('#cart > button').button('loading');
            }, complete: function () {
                $('#cart > button').button('reset');
            }, success: function (json) {
                update_checkout();

                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    if ($('html').attr('lang') == 'uk') {
                        $('#cart > ul').load('/ua/index.php?route=common/cart/info ul li');
                    } else {
                        $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    }
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}
function update_checkout()
{
    var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
    data += '&_shipping_method='+ jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');
    data += '&input_km='+ jQuery('#input-km').val();

    var langPage_val = $('html').attr('lang');
    if(langPage_val == 'uk')
        langPage_val = '/ua/';
    else
        langPage_val = '';

    $.ajax({
        url: 'index.php?route=checkout/checkout/shipping_set',
        type: 'post',
        data: data,

        beforeSend: function() {
            $('#button-register').button('loading');
        },
        complete: function() {
            $('#button-register').button('reset');
        },
        success: function(json) {
            if($('.order__wrapper').length>0)
                $.ajax({
                    url: langPage_val + 'index.php?route=checkout/checkout/index',

                    beforeSend: function() {
                    },
                    complete: function() {
                    },
                    success: function(data) {
                        $('.order__wrapper').html($(data).find('.order__wrapper').html());
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            $.ajax({
                url: langPage_val +  'index.php?route=common/cart/get',

                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(data) {
                    $('.basket-buy.basket__block').html(data);
                    if($('.checkout-checkout').length<1)
                        $('.basket-buy__block').css('display','flex');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });

        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });


}
var voucher = {
    'add': function () {
    }, 'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove', type: 'post', data: 'key=' + key, dataType: 'json', beforeSend: function () {
                $('#cart > button').button('loading');
            }, complete: function () {
                $('#cart > button').button('reset');
            }, success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}
var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add', type: 'post', data: 'product_id=' + product_id, dataType: 'json', success: function (json) {
                $('.alert').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);
                $('html, body').animate({scrollTop: 0}, 'slow');
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function () {
    }
}
var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add', type: 'post', data: 'product_id=' + product_id, dataType: 'json', success: function (json) {
                $('.alert').remove();
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#compare-total').html(json['total']);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function () {
    }
}
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();
    $('#modal-agree').remove();
    var element = this;
    $.ajax({
        url: $(element).attr('href'), type: 'get', dataType: 'html', success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';
            $('body').append(html);
            $('#modal-agree').modal('show');
        }
    });
});
$(document).delegate('.close', 'click', function (e) {
    $('.simplemodal-close').trigger("click");
});
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();
            $.extend(this, option);
            $(this).attr('autocomplete', 'off');
            $(this).on('focus', function () {
                this.request();
            });
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27:
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });
            this.click = function (event) {
                event.preventDefault();
                value = $(event.target).parent().attr('data-value');
                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }
            this.show = function () {
                var pos = $(this).position();
                $(this).siblings('ul.dropdown-menu').css({top: pos.top + $(this).outerHeight(), left: pos.left});
                $(this).siblings('ul.dropdown-menu').show();
            }
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }
            this.request = function () {
                clearTimeout(this.timer);
                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }
            this.response = function (json) {
                html = '';
                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }
                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }
                    var category = new Array();
                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }
                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }
                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }
                if (html) {
                    this.show();
                } else {
                    this.hide();
                }
                $(this).siblings('ul.dropdown-menu').html(html);
            }
            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
        });
    }
})(window.jQuery);

function cartClose() {
    $('#open-cart-click + ul').css('display', 'none');
    //$('.layout').css('display', 'none');
    $.ajax({
        url: '/index.php?route=checkout/cart', type: 'post', dataType: 'html', success: function (json) {
            $('.alert').remove();
            if (json['redirect']) {
                location = json['redirect'];
            }
            if (json['success']) {
                $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            $('#wishlist-total span').html(json['total']);
            $('#wishlist-total').attr('title', json['total']);
            $('html, body').animate({scrollTop: 0}, 'slow');
        },
    })
    $('#cart-span').empty();
    $('#cart-span').prepend($('#cart').find('.dropdown-cart-title').html());
}

function cartClear() {
    $.ajax({
        url: '/index.php?route=checkout/cart/clear', type: 'post', dataType: 'html', success: function (json) {
            $('.alert').remove();
            if (json['success']) {
                $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            }
            $('#wishlist-total span').html(json['total']);
            $('#wishlist-total').attr('title', json['total']);
            $('html, body').animate({scrollTop: 0}, 'slow');
            $('#cart-span').html('КОРЗИНА (0)');
        },
    })
    return false;
}

jQuery(function ($) {
    $("#input-payment-telephone").inputmask("+38(999)999-99-99");
});
$(document).ready(function () {
    $('.product-thumb').each(function (i, elem) {
        isValidPrice(this);
    });
});
$(document).ready(function () {
    $('.radio-img').on('click', function () {
        isValidPrice(this);
    });
});
$(document).ready(function () {
    $('.product-layout input[type=\'text\'], .product-layout input[type=\'hidden\'], .product-layout input[type=\'radio\']:checked, .product-layout input[type=\'checkbox\']:checked, .product-layout select, .product-layout textarea').change(function () {
        isValidPrice(this);
    });
});

function isValidPrice(nawThis) {
    //console.log(nawThis);
    var startPrise = $(nawThis).parents('.product-layout').find('#thisIsOriginal').html();
    var startPriseSpecial = $(nawThis).parents('.product-layout').find('#thisIsOriginalspecial').html();
    var newPrice = parseInt(startPrise, 10);
    var startPriseSpecial = parseInt(startPrise, 10);
    $(nawThis).parents('.product-layout').find('.product-thumb input[type=\'text\'], .product-thumb input[type=\'hidden\'], .product-thumb input[type=\'radio\']:checked, .product-thumb input[type=\'checkbox\']:checked, .product-thumb select, .product-thumb textarea').map(function () {
        switch ($(this).attr('type')) {
            case'radio':
                radioPrice = parseInt($(this).attr('price'), 10);
                if ($.isNumeric(radioPrice)) {
                    newPrice = newPrice + radioPrice;
                    if (typeof newPriseSpecial !== 'undefined') {
                        newPriseSpecial = newPriseSpecial + radioPrice;
                    }
                }
                return radioPrice;
                break;
            default:
                selectPrice = parseInt($(":checked", this).attr('price'), 10);
                if ($.isNumeric(selectPrice)) {
                    newPrice = newPrice + selectPrice;
                    if (typeof newPriseSpecial !== 'undefined') {
                        newPriseSpecial = newPriseSpecial + selectPrice;
                    }
                }
                return selectPrice;
                break;
        }
    }).get();
    $(nawThis).parents('.product-layout').find("#newPrice").html(newPrice + " грн.");
    //console.log(newPrice);
    $(nawThis).parents('.product-layout').find(".newPrice").html(newPrice + " грн.");
};

$(document).ready(function () {
    $('.categories-goods__item').each(function (i, elem) {
        isValidPriceNew(this);
    });
});
$(document).ready(function () {
    $('.categories-goods__item .radio-img').on('click', function () {
        isValidPriceNew(this);
    });
});
$(document).ready(function () {
    $('.categories-goods__item input[type=\'text\'], .categories-goods__item input[type=\'hidden\'], .categories-goods__item input[type=\'radio\']:checked, .categories-goods__item input[type=\'checkbox\']:checked, .categories-goods__item select, .categories-goods__item textarea').change(function () {
        isValidPriceNew(this);
    });
});

function isValidPriceNew(nawThis) {
    //console.log(nawThis);
    var startPrise = $(nawThis).closest('.categories-goods__item').find('.thisIsOriginal').html();
    var startPriseSpecial = $(nawThis).closest('.categories-goods__item').find('.thisIsOriginalspecial').html();
    var newPrice = parseInt(startPrise, 10);
    var startPriseSpecial = parseInt(startPrise, 10);
    $(nawThis).closest('.categories-goods__item').find('.categories-goods__options input[type=\'text\'], .categories-goods__options input[type=\'hidden\'], .categories-goods__options input[type=\'radio\']:checked, .categories-goods__options input[type=\'checkbox\']:checked, .categories-goods__options select, .categories-goods__options textarea').map(function () {
        switch ($(this).attr('type')) {
            case'radio':
                radioPrice = parseInt($(this).attr('price'), 10);
                if ($.isNumeric(radioPrice)) {
                    newPrice = newPrice + radioPrice;
                    if (typeof newPriseSpecial !== 'undefined') {
                        newPriseSpecial = newPriseSpecial + radioPrice;
                    }
                }
                return radioPrice;
                break;
            default:
                selectPrice = parseInt($(":checked", this).attr('price'), 10);
                if ($.isNumeric(selectPrice)) {
                    newPrice = newPrice + selectPrice;
                    if (typeof newPriseSpecial !== 'undefined') {
                        newPriseSpecial = newPriseSpecial + selectPrice;
                    }
                }
                return selectPrice;
                break;
        }
    }).get();
    //$(nawThis).closest('.categories-goods__item').find("#newPrice").html(newPrice + " грн.");
    //console.log(newPrice);
    $(nawThis).closest('.categories-goods__item').find(".categories-goods__new-price").html(newPrice + " грн.");
};

function product_where_watch() {
    $("#product-where-watch").dialog({
        height: $(window).width() > 400 ? ($(window).height() - 70) : 400, width: "90%", dialogClass: "product-popup", open: function (event, ui) {
            $(".product-popup").find('.ui-dialog-titlebar-close').html('<svg width="20px" height="20px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill="#fff" d="M10.707 10.5l5.646-5.646c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.646 5.646-5.646-5.646c-0.195-0.195-0.512-0.195-0.707 0s-0.195 0.512 0 0.707l5.646 5.646-5.646 5.646c-0.195 0.195-0.195 0.512 0 0.707 0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146l5.646-5.646 5.646 5.646c0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146c0.195-0.195 0.195-0.512 0-0.707l-5.646-5.646z"></path></svg>');
            $(".product-popup").find('#where-watch-map > iframe').css('height', ($(window).width() > 400 ? ($(window).height() - 200) - $("#product-where-watch a").height() : 400) + "px")
            $('.ui-widget-overlay.ui-front').on('click', function (e) {
                e.preventDefault();
                $("#product-where-watch").dialog('close');
            });
        }, draggable: false, modal: true
    });

}


function product_try_on() {
    $("#product-try-on").dialog({
        width: $(window).width() > 600 ? 600 : $(window).width() - 20, dialogClass: "product-popup", open: function (event, ui) {
            $(".product-popup").find('.ui-dialog-titlebar-close').html('<svg width="20px" height="20px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill="#fff" d="M10.707 10.5l5.646-5.646c0.195-0.195 0.195-0.512 0-0.707s-0.512-0.195-0.707 0l-5.646 5.646-5.646-5.646c-0.195-0.195-0.512-0.195-0.707 0s-0.195 0.512 0 0.707l5.646 5.646-5.646 5.646c-0.195 0.195-0.195 0.512 0 0.707 0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146l5.646-5.646 5.646 5.646c0.098 0.098 0.226 0.146 0.354 0.146s0.256-0.049 0.354-0.146c0.195-0.195 0.195-0.512 0-0.707l-5.646-5.646z"></path></svg>');
            $('.ui-widget-overlay.ui-front').on('click', function (e) {
                e.preventDefault();
                $("#product-try-on").dialog('close');
            });
        }, draggable: false, modal: true
    });
}

$(function () {
    $("#try-on-form").on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            type: "post", url: form.attr('action'), data: form.serialize() + "&csrf=" + $("#csrf").val(), success: function (res) {
                form.find('.alert-success').show();
                form.find('input[type="text"], textarea').val('');
            }
        })
    });
    $('.quantity-input .quantity-control i').on('click', function (e) {
        e.preventDefault();
        $input = $(this).parents('.quantity-input').find('input');
        if ($(this).data('up')) {
            $input.val(parseInt($input.val()) + parseInt($(this).data('up')));
        } else if ($input.val() > 1) {
            $input.val(parseInt($input.val()) - parseInt($(this).data('down')));
        }
    });
    $('.quantity-input input').on('keydown', function (e) {
        var allowedKeys = ['Backspace', 'Delete', 'Home', 'End', 'ArrowRight', 'ArrowLeft', 'ArrowDown', 'ArrowUp'];
        if ($.inArray(e.key, allowedKeys) < 0 && !e.key.match(/[\d]+/)) {
            e.preventDefault();
            return false;
        }
    });
    $(document).delegate('.basket-buy__close', 'click', function() {
        $('.basket-buy__block').hide();
        $('body').css('overflow-y','auto');
    });
    $(document).delegate('.basket-buy__head', 'click', function() {
        $('.basket-buy__block').hide();
        $('body').css('overflow-y','auto');
    });

})

$(function () {

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $('.radio.radio-image').on('click', function () {
            //f = 0;
            //if ($(this).hasClass('hover'))
            //    f = 1;
            $('.product-detail-options .radio-image').removeClass('hover');

            if ($(this).hasClass('hover'))
            {
               // $(this).removeClass('hover');
            }
            else
            {
                $(this).addClass('hover');
            }
        })
    } else {
        $('.product-detail-options .radio-image').hover(function () {
                $('.product-detail-options .radio-image').removeClass('hover');
                $(this).addClass('hover');
            },
            function () {
                $('.product-detail-options .radio-image').removeClass('hover');
            })
    }

});

