<?php
// Text
$_['text_items']    = 'Товаров %s (%s)';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Перейти в корзину';
$_['text_checkout'] = 'Оформить заказ';
$_['text_recurring']  = 'Платежный профиль';
$_['text_title']  = 'ваша корзина';
$_['text_remove']  = 'Удалить';
$_['text_price']  = 'Цена';
$_['text_total_new']  = 'Сумма';
$_['text_items_new']  = 'Товаров';
$_['text_btn']  = 'Оформить заказ';

