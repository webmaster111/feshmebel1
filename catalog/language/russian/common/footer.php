<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Обратная связь';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['text_powered']      = 'Работает на <a href="http://opencart-russia.ru">OpenCart "Русская сборка"</a><br /> %s &copy; %s';

$_['footer_schedule']      = 'Мы работаем: <p>Пн-пт с 9:00 до 18:00 <p>Сб с 10:00 до 16:00';
$_['footer_menu_about']    = 'О нас';
$_['footer_menu_delivery'] = 'Доставка и оплата';
$_['footer_menu_guaranty'] = 'Гарантия';
$_['footer_menu_exchange'] = 'Обмен и Возврат';
$_['footer_menu_discount'] = 'Система скидок';
$_['footer_menu_articles'] = 'Статьи';
$_['footer_menu_help']     = 'Часто задаваемые вопросы';
$_['footer_menu_contacts'] = 'Контакты';


$_['advantages_text_1'] = 'Оплата: наличная, картой и безналичная с НДС';
$_['advantages_text_2'] = 'Гарантия возврата и обмена товара';
$_['advantages_text_3'] = 'Примерка мебели в своем интерьере';
$_['advantages_text_4'] = 'Свой выставочный салон мебели';

$_['footer_schedule_1'] = 'Будни';
$_['footer_schedule_2'] = 'Сб-Вс';
$_['footer_schedule_3'] = 'Выходные';
$_['footer_schedule_3_1'] = 'Вс, праздники';

$_['footer_phone_1'] = 'Бесплатно';
$_['footer_phone_2'] = 'Тел';

$_['footer_head_name_1'] = 'Информация';
$_['footer_head_name_2'] = 'Клиентам';
$_['footer_head_name_3'] = 'ТОП категории';

$_['footer_address_inf'] = 'ул. Ягодная, 18, Софиевская Борщаговка, Киевская обл.';
$_['footer_address_inf_2'] = 'на карте';

$_['footer_address_inf_3'] = 'ТЦ Sofia Mall,  Проспект Героев Небесной Сотни 24/83, Софиевская Борщаговка, Киевская обл.';

$_['footer_1'] = 'Интернет магазин мебели ФешеМебельный';
$_['footer_2'] = 'Магазин мебели';




$_['where_watch_shedule'] = 'Мы работаем: Пн-Пт: с 9:00 до 18:00';

$_['where_watch_shedule2'] = 'Мы работаем: Пн-Вс: с 10:00 до 20:00';