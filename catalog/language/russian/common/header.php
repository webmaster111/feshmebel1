<?php
// Menu
$_['top_menu_about']     = 'О нас';
$_['top_menu_delivery']  = 'Доставка и оплата';
$_['top_menu_guaranty']  = 'Гарантия';
$_['top_menu_exchange']  = 'Обмен и Возврат';
$_['top_menu_discount']  = 'Система скидок';
$_['top_menu_articles']  = 'Статьи';
$_['top_menu_help']      = 'Помощь';

$_['top_menu_try_on']      = 'Примерка';

// Text
$_['site_title']         = 'Фешемебельный интернет-магазин';
$_['text_home']          = 'Главная';
$_['text_wishlist']      = 'Желания';
$_['text_compare']       = 'Сравнение';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории товаров';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Посмотреть все товары категории';
$_['text_work']          = 'Пн-пт з 9:00 до 18:00 <p>Сб з 10:00 до 16:00';

$_['text_free']          = 'Бесплатно';
$_['text_day_off']       = 'Выходные: Суббота и воскресенье';
$_['text_call']          = 'ЗАКАЗАТЬ ЗВОНОК';
$_['text_articles']          = 'Блог';
