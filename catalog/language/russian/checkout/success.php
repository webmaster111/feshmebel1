<?php
// Heading
$_['heading_title']        = 'Ваш заказ принят';

// Text
$_['text_basket']          = 'Корзина';
$_['text_checkout']        = 'Оформить заказ';
$_['text_success']         = 'Заказ принят';
$_['text_customer']        = '<p class="checkout-success__description">Заказ оформлен и поступил в обработку. Наш менеджер свяжется с вами в ближайшее время. Статус заказа можно проверить в <a href="%s">Личном кабинете</a></p>
                    <p class="checkout-success__description">Спасибо за покупки в ФешеМебельном.</p>
                    <div class="order__info-contact guest-contact after-prepay">
                        <span class="order__help">Остались вопросы? Позвоните или напишите:</span>
                        <div class="order__phone">0 800 330 190
                            <span>(бесплатно)</span>
                        </div>
                        <span class="order__info-email">info@feshemebel.com.ua</span>
                    </div>
                    ';
$_['text_guest']           = '<p>Ваш заказ принят!</p><p>Если у Вас возникли вопросы, пожалуйста <a href="%s">свяжитесь с нами</a>.</p><p>Спасибо за покупки в нашем интернет-магазине!</p>';
