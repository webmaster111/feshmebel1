<?php
// Heading
$_['heading_title']  = 'Оптовым клиентам';

// Text
$_['text_location']  = 'Наше местонахождение';
$_['text_store']     = 'Наши магазины';
$_['text_contact']   = 'Проблема с оформлением заказа?';
$_['text_address']   = 'Адрес';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'Режим работы';
$_['text_comment']   = 'Дополнительная информация';
$_['text_success']   = '<p>Благодарим за обращение. Мы свяжемся с вами в ближайшее время.</p>';
$_['button_submit']         = 'Отправить';

// Entry
$_['entry_name']     = 'Ваше имя';
$_['entry_email']    = 'Email';
$_['entry_phone']    = 'Ваш телефон';
$_['entry_enquiry']  = 'Сообщение';
$_['entry_captcha']  = 'Введите код, указанный на картинке';

$_['text_left']  = '<div>Нужна помощь?</div>
                <div>Ответим на любые вопросы:</div>
                <div><a class="phone-free" href="tel:0800330190">0 800 330 190 </a>(бесплатно)</div>
                <div><a class="mail-box" href="mailto:info@feshemebel.com.ua">info@feshemebel.com.ua</a></div>';

$_['text_right']  = '<ul>
                    <li>Специальные цены на мебель со скидками</li>
                    <li>Индивидуальные заказы: производство мебели по эскизам и чертежам</li>
                    <li>Договорные поставки товара</li>
                    <li>Персональная помощь в подборе нужной модели</li>
                    <li>Дропшиппинг</li>
                    <li>Гибкая система оплаты: наличный и безналичный расчет с НДС</li>
                </ul>
                <h2>Наши партнеры и бизнес клиенты:</h2>
                <ul>
                    <li>Дизайнеры</li>
                    <li>Архитекторы</li>
                    <li>Представители HoReCa</li>
                    <li>Дилеры</li>
                    <li>Строительные фирми и бюро</li>
                    <li>Производители мебели</li>
                    <li>Корпоративные клиенты</li>
                </ul>
                <p>
                    Оставьте заявку на сотрудничество или напишите на <a class="mail-box" href="mailto:info@feshemebel.com.ua">info@feshemebel.com.ua</a>. Мы свяжемся с вами в ближайшее время, чтобы обсудить детали:
                </p>';


// Email
$_['email_subject']  = 'Сообщение от %s';



// Errors
$_['error_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_phone']  = 'Телефон должно быть 17 символов!';
$_['error_email']    = 'E-mail адрес введен неверно!';
$_['error_enquiry']  = 'Длина текста должна быть от 10 до 3000 символов!';
$_['error_captcha']  = 'Проверочный код не совпадает с изображением!';


