<?php
// Locale
$_['code']                  = 'ru';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd.m.Y';
$_['date_format_long']      = 'l, d F Y';
$_['time_format']           = 'H:i:s';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = '';

// Text
$_['text_home']             = 'Интернет магазин мебели Фешемебельный';
$_['text_yes']              = 'Да';
$_['text_no']               = 'Нет';
$_['text_none']             = ' --- Не выбрано --- ';
$_['text_select']           = ' --- Выберите --- ';
$_['text_all_zones']        = 'Все зоны';
$_['text_pagination']       = 'Показано с %d по %d из %d (всего %d страниц)';
$_['text_loading']          = 'Загрузка...';

// Buttons
$_['button_address_add']    = 'Добавить адрес';
$_['button_back']           = 'Назад';
$_['button_continue']       = 'Продолжить';
$_['button_cart']           = 'Купить';
$_['button_filter_amp']           = 'ФИЛЬТРЫ';
$_['button_expect']         = 'Ожидается';
$_['button_out_of_sale']    = 'Снято с продажи';
$_['button_out_of_sale_img']    = '<img src="/image/catalog/out_of_sale_ru.png"  style="display: inline-block; max-width: 100%;">';
$_['button_toorder1']         = 'На заказ';
$_['button_toorder2']         = 'Заказать';
$_['button_cancel']         = 'Отмена';
$_['button_compare']        = 'В сравнение';
$_['button_wishlist']       = 'В желания';
$_['button_checkout']       = 'Оформление заказа';
$_['button_confirm']        = 'Подтверждение заказа';
$_['button_coupon']         = 'Применение купона';
$_['button_delete']         = 'Удалить';
$_['button_download']       = 'Скачать';
$_['button_edit']           = 'Редактировать';
$_['button_filter']         = 'Поиск';
$_['button_new_address']    = 'Новый адрес';
$_['button_change_address'] = 'Изменить адрес';
$_['button_reviews']        = 'Отзывы';
$_['button_write']          = 'Написать отзыв';
$_['button_login']          = 'Войти';
$_['button_update']         = 'Обновить';
$_['button_remove']         = 'Удалить';
$_['button_reorder']        = 'Дополнительный заказ';
$_['button_return']         = 'Вернуть';
$_['button_shopping']       = 'Продолжить покупки';
$_['button_search']         = 'Поиск';
$_['button_shipping']       = 'Применить Доставку';
$_['button_submit']         = 'Применить';
$_['button_guest']          = 'Оформление заказа без регистрации';
$_['button_view']           = 'Просмотр';
$_['button_voucher']        = 'Применить подарочный сертификат';
$_['button_upload']         = 'Загрузить файл';
$_['button_reward']         = 'Применить бонусные баллы';
$_['button_quote']          = 'Узнать цены';
$_['button_list']           = 'Список';
$_['button_grid']           = 'Сетка';
$_['button_map']            = 'Посмотреть карту';

// Error
$_['error_exception']       = 'Ошибка кода(%s): %s в %s на строке %s';
$_['error_upload_1']        = 'Предупреждение: Размер загружаемого файла превышает значение upload_max_filesize в php.ini!';
$_['error_upload_2']        = 'Предупреждение: Загруженный файл превышает MAX_FILE_SIZE значение, которая была указана в настройках!';
$_['error_upload_3']        = 'Предупреждение: Загруженные файлы были загружены лишь частично!';
$_['error_upload_4']        = 'Предупреждение: Нет файлов для загрузки!';
$_['error_upload_6']        = 'Предупреждение: Временная папка!';
$_['error_upload_7']        = 'Предупреждение: Ошибка записи!';
$_['error_upload_8']        = 'Предупреждение: Запрещено загружать файл с данным расширением!';
$_['error_upload_999']      = 'Предупреждение: Неизвестная ошибка!';
$_['button_confirm_p'] = 'Отправить заказ';

$_['popular_head_title']            = 'Популярные разделы';
$_['brand_head_title']            = 'Выберите бренд';
$_['projects_head_title']            = 'Наши проекты';
$_['partner_head_title']            = 'Клиенты и партнеры';
$_['advice_head_title']            = 'Советы и вдохновение';
$_['text_url_articles']            = 'все статьи';