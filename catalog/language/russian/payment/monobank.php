<?php
// Text
$_['text_title'] = 'Покупка частями (Monobank)';
$_['text_privat_price_mono_before'] = 'по ';
$_['text_privat_price_mono_before1'] = 'Платежа, на ';
$_['text_privat_price_mono_after'] = '100 rub';
$_['text_privat_price_mono_after2'] = 'грн/мес';
$_['text_per_month'] = 'мес.';
$_['text_overpayment'] = 'Переплата 0% - 0 ';

$_['error_message1'] = 'Сумма должна быть больше 1000.';
$_['error_message2'] = 'Ошибка при подачи заявки на кредит.';
$_['error_message3'] = 'Не верный телефон.';
$_['error_message4'] = 'Ожидание.';
$_['error_message5'] = 'Не верная подпись.';
$_['error_message6'] = 'Неверный JSON ответ: ';
$_['error_message7'] = 'Ошибка';

$_['modal_button_close'] = 'Закрыть';