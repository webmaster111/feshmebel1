<?php
// Text
$_['text_title'] = 'Purchase in parts (Monobank)';
$_['text_privat_price_mono_before'] = 'on ';
$_['text_privat_price_mono_after'] = '100 rub';
$_['text_per_month'] = 'per month';
$_['text_overpayment'] = 'Overpayment 0% - 0 ';

$_['error_message1'] = 'The amount must be greater than 1000.';
$_['error_message2'] = 'Error when applying for a loan.';
$_['error_message3'] = 'Not a valid phone.';
$_['error_message4'] = 'Waiting.';
$_['error_message5'] = 'Wrong signature.';
$_['error_message6'] = 'Invalid JSON response: ';
$_['error_message7'] = 'Error';

$_['modal_button_close'] = 'Close';