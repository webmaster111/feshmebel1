<?php

$_['heading_title'] = 'Використовувати промокод';

// Text
$_['text_coupon']   = 'промокод (%s)';
$_['text_success']  = 'промокод на знижку успішно застосований!';

// Entry
$_['entry_coupon']  = 'Введіть код промокода';

// Error
$_['error_coupon']  = 'Помилка. Неправильний код промокодом на знижку. Можливо, закінчився термін дії або досягнутий ліміт використання!';
$_['error_empty']   = 'Увага! Введіть код промокодом';


