<?php
// Text
$_['text_information']  = 'Інформація';
$_['text_service']      = 'Сервісні служби';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = 'Контакти';
$_['text_return']       = 'Повернення';
$_['text_sitemap']      = 'Мапа сайту';
$_['text_manufacturer'] = 'Бренди';
$_['text_voucher']      = 'Подарункові сертифікати';
$_['text_affiliate']    = 'Партнерська програма';
$_['text_special']      = 'Спеціальні пропозиції';
$_['text_account']      = 'Обліковий запис';
$_['text_order']        = 'Історія замовлень';
$_['text_wishlist']     = 'Список побажань';
$_['text_newsletter']   = 'Розсилання новин';
$_['text_powered']      = 'Створено в <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
$_['text_call']         = 'ЗАМОВИТИ ДЗВІНОК';
$_['text_shop']         = 'інтернет-магазин меблів';
$_['text_work']         = 'Ми працюємо: Пн-пт з 9:00 до 20:00 <p>Сб-Нд з 10:00 до 20:00';
$_['text_day_off']      = 'Вихідні: субота і неділя';
$_['text_social']       = 'Ми в соціальних мережах';

$_['footer_schedule']      = 'Ми працюємо: <p>Пн-пт з 9:00 до 20:00 <p>Сб-Нд з 10:00 до 20:00';
$_['footer_menu_about']    = 'Про нас';
$_['footer_menu_delivery'] = 'Доставка та оплата';
$_['footer_menu_guaranty'] = 'Гарантія';
$_['footer_menu_exchange'] = 'Обмін та повернення';
$_['footer_menu_discount'] = 'Система знижок';
$_['footer_menu_articles'] = 'Статті';
$_['footer_menu_help']     = 'Поширенні питання';
$_['footer_menu_contacts'] = 'Контакти';

$_['where_watch_shedule'] = 'Ми працюємо: Пн-Пт: з 9:00 до 20:00';

$_['where_watch_shedule2'] = 'Ми працюємо: Пн-Нд: з 10:00 до 20:00';

$_['advantages_text_1'] = 'Оплата: готівкова, картою і безготівкова з ПДВ';
$_['advantages_text_2'] = 'Гарантія повернення і обміну товару';
$_['advantages_text_3'] = 'Примірка меблів в своєму інтер\'єрі';
$_['advantages_text_4'] = 'Свій виставковий салон меблів';

$_['footer_schedule_1'] = 'Будні';
$_['footer_schedule_2'] = 'Сб-Нд';
$_['footer_schedule_3'] = 'Вихідні';
$_['footer_schedule_3_1'] = 'Нд, свята';

$_['footer_phone_1'] = 'Безкоштовно';
$_['footer_phone_2'] = 'Тел';

$_['footer_head_name_1'] = 'Інформація';
$_['footer_head_name_2'] = 'Клієнтам';
$_['footer_head_name_3'] = 'ТОП категорії';

$_['footer_address_inf'] = 'вул. Ягідна, 18, Софіївська Борщагівка, Київська обл. ';
$_['footer_address_inf_2'] = 'на мапі';

$_['footer_address_inf_3'] = 'ТЦ Sofia Mall, Проспект Героїв Небесної Сотні 24/83, Софіївська Борщагівка, Київська обл.';

$_['footer_1'] = 'Інтернет магазин меблів ФешеМебельний';
$_['footer_2'] = 'Магазин меблів';
    