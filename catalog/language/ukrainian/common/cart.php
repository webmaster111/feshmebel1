<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_items']    = 'КОРЗИНА (%s)';
$_['text_item']    = ' %s ';
$_['text_empty']    = 'Ваш кошик порожній!';
$_['text_cart']     = 'Переглянути кошик';
$_['text_checkout'] = 'Оформити замовлення';
$_['text_recurring']  = 'Профіль оплати';

$_['text_title']  = 'ваша корзина';
$_['text_remove']  = 'Видалити';
$_['text_price']  = 'Ціна';
$_['text_total_new']  = 'Сума';
$_['text_items_new']  = 'Товарів';
$_['text_btn']  = 'Оформити замовлення';
