<?php
// Top Menu
$_['top_menu_about']     = 'Про нас';
$_['top_menu_delivery']  = 'Доставка та оплата';
$_['top_menu_guaranty']  = 'Гарантія';
$_['top_menu_exchange']  = 'Обмін та повернення';
$_['top_menu_discount']  = 'Система знижок';
$_['top_menu_articles']  = 'Статті';
$_['top_menu_help']      = 'Допомога';

$_['top_menu_try_on']      = 'Примірка';

// Text
$_['site_title']         = 'Фешемебельний інтернет-магазин';
$_['text_home']          = 'Головна';
$_['text_wishlist']      = 'Бажання';
$_['text_compare']       = 'Порівняння';
$_['text_shopping_cart'] = 'Кошик';
$_['text_category']      = 'Категорії товарів';
$_['text_account']       = 'Обліковий запис';
$_['text_register']      = 'Реєстрація';
$_['text_login']         = 'Вхід';
$_['text_order']         = 'Історія замовлень';
$_['text_transaction']   = 'Оплати';
$_['text_download']      = 'Завантаження';
$_['text_logout']        = 'Вихід';
$_['text_checkout']      = 'Оформлення замовлення';
$_['text_search']        = 'Пошук';
$_['text_all']           = 'Переглянути всі товари категорії';
$_['text_articles']          = 'Блог';
$_['text_work']          = 'Пн-пт з 9:00 до 18:00 <p>Сб з 10:00 до 16:00';

$_['text_free']          = 'Безкоштовно';
$_['text_day_off']       = 'Вихідні: субота і неділя';
$_['text_call']          = 'ЗАМОВИТИ ДЗВІНОК';
