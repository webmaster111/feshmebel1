<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_title']				= 'Оплата за безготівковим розрахунком';	//Банківський переказ
$_['text_instruction']			= 'Інструкція по оплаті шляхом безготівкового розрахунку';
$_['text_description']			= 'Будь-ласка перекажіть суму замовлення на наступні реквізити.';
$_['text_payment']				= ''; //Ваше замовлення буде відправлене після отримання оплати.';