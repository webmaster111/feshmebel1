<?php
// Text
$_['text_title'] = 'Купівля частинами (Monobank)';
$_['text_privat_price_mono_before'] = 'по';
$_['text_privat_price_mono_before1'] = 'Платежа, на ';
$_['text_privat_price_mono_after'] = '100 rub';
$_['text_privat_price_mono_after2'] = 'грн/міс';
$_['text_per_month'] = 'міс.';
$_['text_overpayment'] = 'Підписка 0% - 0';

$_['error_message1'] = 'Сума повинна бути понад 1000';
$_['error_message2'] = 'Помилка при подачі заявки на кредит.';
$_['error_message3'] = 'Не вірний телефон.';
$_['error_message4'] = 'Очікування.';
$_['error_message5'] = 'Неможливо вірна підпис. ';
$_['error_message6'] = 'Неправильний JSON відповідь:';
$_['error_message7'] = 'Помилка';

$_['modal_button_close'] = 'Закрити';