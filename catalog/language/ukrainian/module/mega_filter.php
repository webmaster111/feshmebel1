<?php
$_['heading_title'] = 'Mega Filter';
$_['name_price'] = 'Цiна';
$_['name_manufacturers'] = 'Виробники';
$_['name_rating'] = 'Rating';
$_['name_search'] = 'Пошук';
$_['name_stock_status'] = 'Статус';
$_['name_location'] = 'Location';
$_['name_length'] = 'Length';
$_['name_width'] = 'Width';
$_['name_height'] = 'Height';
$_['name_mpn'] = 'MPN';
$_['name_isbn'] = 'ISBN';
$_['name_sku'] = 'Артикул';
$_['name_upc'] = 'UPC';
$_['name_ean'] = 'EAN';
$_['name_jan'] = 'JAN';
$_['name_model'] = 'Модель';
$_['text_button_apply'] = 'Apply';
$_['text_reset_all'] = 'Reset All';
$_['text_show_more'] = 'Show more (%s)';
$_['text_show_less'] = 'Show less';
$_['text_display'] = 'Display';
$_['text_grid'] = 'Grid';
$_['text_list'] = 'List';
$_['text_loading'] = 'Loading...';
$_['text_select'] = 'Select...';
$_['text_go_to_top'] = 'Go to top';
$_['text_init_filter'] = 'Initialize the filter';
$_['text_initializing'] = 'Initializing...';
?>