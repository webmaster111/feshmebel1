<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']        = 'Ваше замовлення прийнято';

// Text
$_['text_basket']          = 'Кошик';
$_['text_checkout']        = 'Оформлення замовлення';
$_['text_success']         = 'Успішно';
$_['text_customer']        = '<p class="checkout-success__description">Замовлення оформлене і поступив в обробку. Наш менеджер зв\'яжеться з вами найближчим часом. Статус замовлення можна перевірити в <a href="%s">Особистому кабінеті</a></p>
                    <p class="checkout-success__description">Дякую за покупки в ФешеМебельном.</p>
                    <div class="order__info-contact guest-contact after-prepay">
                        <span class="order__help">Залишилися питання? Зателефонуйте або напишіть:</span>
                        <div class="order__phone">0 800 330 190
                            <span>(безкоштовно)</span>
                        </div>
                        <span class="order__info-email">info@feshemebel.com.ua</span>
                    </div>
                    ';
$_['text_guest']           = '<p>Ваше замовлення було успішно опрацьовано!</p><p>Якщо у Вас є якісь запитання висилайте їх <a href="%s">нам</a>.</p><p>Дякуємо за покупку!</p>';
