<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text
$_['text_refine']       = 'Оберіть підкатегорію';
$_['text_product']      = 'Товари';
$_['text_error']        = 'Категорія не знайдена!';
$_['text_empty']        = 'На жаль, в цій категорії немає товарів.';
$_['prod_interest']     = 'Вас також може зацікавити';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Бренд:';
$_['text_model']        = 'Артикул:';
$_['text_points']       = 'Бонусні бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без ПДВ:';
$_['text_compare']      = 'Порівняти товари (%s)';
$_['text_sort']         = 'Сортувати за:';
$_['text_default']      = 'За замовчуванням';
$_['text_name_asc']     = 'Ім`я (А - Я)';
$_['text_name_desc']    = 'Ім`я (Я - A)';
$_['text_price_asc']    = 'Ціна (Низька)';
$_['text_price_desc']   = 'Ціна (Висока)';
$_['text_rating_asc']   = 'Рейтинг (Низький)';
$_['text_rating_desc']  = 'Рейтинг (Високий)';
$_['text_model_asc']    = 'Модель (A - Я)';
$_['text_model_desc']   = 'Модель (Я - A)';
$_['text_limit']        = 'Показати:';
$_['btn_open_text']        = 'Читати далі';

$_['faq_title']         = 'Часті питання про ';
$_['faq_title1']         = '🛍 Які {text} найпопулярніші?';
$_['faq_sub1']         = '💞 Найпопулярніші {text} на сайті Фешемебельний';
$_['faq_title2']         = '💳 Які {text} найдешевші на сайті?';
$_['faq_sub2']         = 'Найдешевші товари на feshmebel.com.ua за ціною від';
$_['faq_title3']         = '🌟 {text} - які ціни на розпродажах?';
$_['faq_sub3']         = 'Замовте товари за акційними цінами - зараз діють знижки на:';
$_['faq_title4']         = '🚀 Найбільш актуальні {text} в 2021 році!';
$_['faq_sub4']         = '✅ Цього року найбільш актуальні товари:';
$_['faq_title5']         = '⏰ Який діапазон цін на {text}?';
$_['faq_sub5']         = '💜 Діапазон цін від ';

$_['text_material_h2'] = 'Другие товары из каталога';

