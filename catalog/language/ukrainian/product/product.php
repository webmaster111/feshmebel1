<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Text

$_['text_product_special_end']                             = 'До кінця акції залишилось';
$_['text_product_special_more']                             = 'Детальніше про акцію';

$_['text_show_btn_try_on']                             = 'Приміряти';
$_['text_show_btn_where_watch']                             = 'Де подивитись';
$_['text_search']                             = 'Пошук';
$_['text_brand']                              = 'Бренд';
$_['text_manufacturer']                       = 'Виробник:';
$_['text_supplier']                           = 'Постачальник:';
$_['text_model']                              = 'Артикул:';
$_['text_reward']                             = 'Бонусні бали:';
$_['text_points']                             = 'Ціна в бонусних балах:';
$_['text_stock']                              = 'Наявність:';
$_['text_instock']                            = 'В наявності';
$_['text_tax']                                = 'Без ПДВ:';
$_['text_discount']                           = ' чи більше ';
$_['text_option']                             = 'Доступні опції';
$_['text_minimum']                            = 'Мінімальне замовлення цього товару %s';
$_['text_reviews']                            = '%s відгуків';
$_['text_write']                              = 'Написати відгук';
$_['text_login']                              = 'Будь-ласка <a  href="%s">ввійдіть</a> чи <a href="%s">зареєструйтеся</a> для написання відгуку';
$_['text_no_reviews']                         = 'В цього товару немає відгуків.';
$_['text_note']                               = '<span class="text-danger">Зверніть увагу:</span> HTML не перекладено!';
$_['text_success']                            = 'Дякуємо з Ваш відгук. Він направлений для затвердження модератору.';
$_['text_related']                            = 'Схожі товари';
$_['text_viewed']                             = 'Переглянутi товари';
$_['text_tags']                               = 'Теги:';
$_['text_error']                              = 'Товар не знайдено!';
$_['text_payment_recurring']                    = 'Профілі оплати';
$_['text_trial_description']                  = '%s кожен %d %s(s) до %d платежу(ів) тоді';
$_['text_payment_description']                = '%s кожен %d %s(s) до %d патежу(ів)';
$_['text_payment_until_canceled_description'] = '%s кожен %d %s(s) поки не скасовано';
$_['text_day']                                = 'день';
$_['text_week']                               = 'тиждень';
$_['text_semi_month']                         = 'половина місяця';
$_['text_month']                              = 'місяць';
$_['text_year']                               = 'рік';
$_['nopay_install']                               = 'безкоштовний монтаж';
$_['noneed_install']                               = 'не потребує монтажу';

// Entry
$_['entry_qty']                               = 'Кількість';
$_['entry_name']                              = 'Ваше Ім`я';
$_['entry_review']                            = 'Ваш відгук';
$_['entry_rating']                            = 'Рейтинг';
$_['entry_good']                              = 'Хороший';
$_['entry_bad']                               = 'Поганий';
$_['entry_captcha']                           = 'Введіть код з зображення в поле';

// Tabs
$_['tab_description']                         = 'Опис';
$_['tab_attribute']                           = 'Специфікація';
$_['tab_review']                              = 'Відгуки (%s)';

// Error
$_['error_name']                              = 'Назва відгуку повинна містити від 3 до 25 символів!';
$_['error_text']                              = 'Текст відгуку повинен містити від 25 до 1000 символів!';
$_['error_rating']                            = 'Оберіть ретинг відгуку!';
$_['error_captcha']                           = 'Код підтвердження не співпадає з зображенням!';

// description start Tkach web-promo
$_['title_description']          		  = 'Опис';
$_['title_attribute']            		  = 'Характеристики';
$_['title_delivery']            		  = 'Доставка і гарантія';
$_['title_installation']          		  = 'Вартість монтажу';
$_['description_installation']            = 'Вартість монтажу:';
$_['revised_goods']          		  	  = 'Переглянуті товари';
$_['description_delivery_1'] =
'<p>Доставка по Києву:</p>
 <p>- Безкоштовно </p>';
$_['description_delivery_2'] =
'<p>Доставка по Україні:</p>
 <p>- безкоштовно при замовленні від 15000 грн.</p>
 <p>- за рахунок клієнта при замовленні до 15000 грн.</p>';
$_['description_delivery_3'] = 'Оплата готівкою, карткою або безготівковим розрахунком.';
$_['description_delivery_4'] = 'Детальніше про умови доставки та оплати.';
$_['description_delivery_5'] = 'Повернення і обмін протягом 14 днів';
$_['description_delivery_6'] = 'Гарантія від 12 місяців';
$_['description_nopaydelivery_1'] = 
'<p>Доставка по Києву:</p>
 <p>Безкоштовно</p>';
 $_['description_nopaydelivery_2'] = 
'<p>Доставка по Україні:</p>
 <p>Безкоштовно</p>';
 
 $_['description_nopaydelivery_1'] = 
'<p>Доставка по Киеву:</p>
 <p>Безкоштовно</p>';
 $_['description_nopaydelivery_2'] = 
'<p>Доставка по Украине:</p>
 <p>Безкоштовно</p>';

$_['map_address']          		  	  	  = 'вул. Ягідна 18, с. Софіївська Борщагівка';
$_['map_address2']          		  	  	  = 'ТЦ Sofia Mall, Проспект Героїв Небесної Сотні 24/83, Софіївська Борщагівка';
$_['try_on_text'] = '<p>Пропонуємо Вам скористатися ексклюзивною послугою «Примірка».</p>
<p>
Протягом кількох хвилин після відправлення цієї форми наш менеджер зв\'яжеться з Вами, щоб уточнити деталі доставки.
</p>
<p>
Модель не підійшла? Сплатіть доставку, і ми повернемо її назад в магазин.</p>';
$_['text_link_more'] = 'Детальніше';
$_['text_try_on_f_name'] = 'ПІБ';
$_['text_try_on_f_phone'] = 'Телефон';
$_['text_try_on_f_address'] = 'Адрес доставки (відправки)';

$_['text_try_on_f_submit'] = 'Відправити';
$_['text_try_on_f_success'] = 'Заявка відправлена.';

$_['text_material_h2'] = 'Інші товари з каталогу';