<?php
// Text
$_['text_combo'] = 'Produtos Combinados';
$_['text_save'] = 'Economize:';
$_['text_price_all'] = 'Valor total';
$_['text_combo_header'] = 'Frequentemente Comprados Juntos';
$_['text_add_wishlist'] = 'Adicionar à Lista de Desejos';
$_['text_add_cart'] = 'Adicionar todos ao Carrinho';
$_['text_warning'] = 'Atenção: Por favor escolha a(s) opção(ões) para <a href="%s">%s</a> antes de adicionar para o seu <a href="%s">carrinho</a>!';
?>