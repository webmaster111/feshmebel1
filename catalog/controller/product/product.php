<?php
class ControllerProductProduct extends Controller {
	private $error = array();

    public function viewed() {
        $item_product_id = $this->request->get['product_id'];

        if (isset($this->request->get['product_id'])) {
            $_SESSION['viewed_product'][$item_product_id] = $item_product_id;
        }


        $this->load->language('product/category');
        $this->load->model('catalog/category');
        $data['btn_open_text'] = $this->language->get('btn_open_text');

        $data['text_refine'] = $this->language->get('text_refine');
        $data['text_empty'] = $this->language->get('text_empty');
        $data['text_quantity'] = $this->language->get('text_quantity');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_model'] = $this->language->get('text_model');
        $data['text_price'] = $this->language->get('text_price');
        $data['text_tax'] = $this->language->get('text_tax');
        $data['text_points'] = $this->language->get('text_points');
        $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
        $data['text_sort'] = $this->language->get('text_sort');
        $data['text_limit'] = $this->language->get('text_limit');
        $data['text_option'] = $this->language->get('text_option');
        $data['text_select'] = $this->language->get('text_select');


        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_expect'] = $this->language->get('button_expect');
        $data['button_out_of_sale'] = $this->language->get('button_out_of_sale');
        $data['button_toorder1'] = $this->language->get('button_toorder1');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_list'] = $this->language->get('button_list');
        $data['button_grid'] = $this->language->get('button_grid');
        $data['prod_interest'] = $this->language->get('prod_interest');

        $this->load->model('catalog/supplier');
        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $this->load->model('localisation/sale_status');

        $data['sale_statuses'] = $this->model_localisation_sale_status->getSaleStatuses();

        $data['products'] = array();

        if(isset($_SESSION['viewed_product']))
        {
            foreach ($_SESSION['viewed_product'] as $product_id)
            {
                if($item_product_id==$product_id)
                {
                    continue;
                }


                $product_item = $this->model_catalog_product->getProduct($product_id);

                if ($product_id > 0) {
                    $supplier_info = $suppliers[$product_item['supplier_id']];
                }

                if ($product_item) {




                    if ($product_item['image']) {
                        //$image = $this->model_tool_image->resize($product_item['image'], $setting['width'], $setting['height']);
                        $image = $this->model_tool_image->resize($product_item['image'], 300, 300);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 300, 300);
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_item['price'], $product_item['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$product_item['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_item['special'], $product_item['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_item['special'] ? $product_item['special'] : $product_item['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_item['rating'];
                    } else {
                        $rating = false;
                    }
    //*
                    if ($supplier_info['supplier_id'] > 0) {
                        $price = preg_replace('/[^0-9.]/', '', $price) * $supplier_info['coefficient'];
                        $price = ceil($price) . ' грн.';
                        //$special = preg_replace('/[^0-9.]/', '', $special)*$supplier_info['coefficient'];
                        //	$special = ceil($special).' грн.';
                    };
    //*/


                    $images = array();

                    $result_images = $this->model_catalog_product->getProductImages($product_item['product_id']);

                    foreach ($result_images as $result_image) {
                        $images[] = array(
                            'thumb' => $this->model_tool_image->resize($result_image['image'], 256, 256)
                        );
                    }

                    $prodd_options = $this->model_catalog_product->getProductOptions($product_item['product_id']);
                    $prodd_options_new = array();
                    foreach ($prodd_options as $key1 => $option) {
                        $prodd_options_new[] = $option;
                        foreach ($option['product_option_value'] as $key2 => $option_val) {

                            if(!empty($option_val['image_prod']))
                                $images[] = array(
                                    'thumb' => $this->model_tool_image->resize($option_val['image_prod'], 256, 256)
                                );

                            if(!empty($option_val['image_prod']))
                            {
                                $prodd_options_new[$key1]['product_option_value'][$key2]['image_prod'] = $this->model_tool_image->resize($option_val['image_prod'], 300, 300);
                            }
                            $prodd_options_new[$key1]['product_option_value'][$key2]['image'] = $this->model_tool_image->resize($option_val['image'], 32, 32);

                            $supplier_id = $product_item['supplier_id'];

                            $_price = $prodd_options_new[$key1]['product_option_value'][$key2]['price'];
                            if ((int)$supplier_id > 0) {
                                $_price = preg_replace('/[^0-9.]/', '', $_price) * $suppliers[$supplier_id]['coefficient'];
                            };
                            $prodd_options_new[$key1]['product_option_value'][$key2]['price'] = ceil($_price) . ' грн.';
                        }
                    }
                    $special_info_tmp = $this->model_catalog_product->getProductSpecial($product_item['product_id']);

                    if(!empty($special_info_tmp)){

                        if(strtotime($special_info_tmp['date_end']) - time()>0)
                            $product_item['sale_status_id']=1;
                        else
                            $product_item['sale_status_id']=0;
                    }

                    $data['products'][] = array(
                        'product_id' => $product_item['product_id'],
                        'thumb' => $image,
                        'images' => $images,

                        'name' => $product_item['name'].' - '.$product_item['sku'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_item['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $price,
                        'old_price' => $product_item['old_price'],
                        'sale_status_name' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? $data['sale_statuses'][$product_item['sale_status_id']]['name']:'',
                        'sale_status_color' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? $data['sale_statuses'][$product_item['sale_status_id']]['color']:'',
                        'sale_status_image' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$product_item['sale_status_id']]['image']:'',
                        'special' => $special,
                        'tax' => $tax,
                        'rating' => $rating,
                        'stock_status' => $product_item['stock_status'],
                        'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product_item['product_id']),
                        //'options'     => $this->model_catalog_product->getProductOptions($product_item['product_id']),
                        'options' => $prodd_options_new,
                        'supplier' => $suppliers[$product_item['supplier_id']],
                        'privat' => $product_item['product_pp'] || $product_item['product_ii']? 1 : 0,
                    );
                }

            }
        }
        if($item_product_id<1)
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/viewed_product_home.tpl', $data));
        else
        {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/viewed_product.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/viewed_product.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/module/viewed_product.tpl', $data));
            }
        }


    }
    public function download() {


        $this->load->model('account/download');

        if (isset($this->request->get['download_id'])) {
            $download_id = $this->request->get['download_id'];
        } else {
            $download_id = 0;
        }

        $download_info = $this->model_account_download->getDownload($download_id);

        if ($download_info) {
            $file = DIR_DOWNLOAD . $download_info['filename'];
            $mask = basename($download_info['mask']);

            if (!headers_sent()) {
                if (file_exists($file)) {
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));

                    if (ob_get_level()) {
                        ob_end_clean();
                    }

                    readfile($file, 'rb');

                    exit();
                } else {
                    exit('Error: Could not find file ' . $file . '!');
                }
            } else {
                exit('Error: Headers already sent out!');
            }
        } else {
            $this->response->redirect($this->url->link('account/download', '', 'SSL'));
        }
    }

	public function index() {

		$this->load->model('catalog/supplier');
		$_suppliers = $this->model_catalog_supplier->getSuppliers();
		$suppliers = array();
		foreach($_suppliers as $supplier)
			$suppliers[$supplier['supplier_id']] = $supplier;

		$this->load->language('product/product');
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}
				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer')
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
		}



        $_SESSION['viewed_product'][$product_id] = $product_id;

		$res = $this->db->query("SELECT `value` FROM " . DB_PREFIX . "setting WHERE `key`='ob_product_special_on'");
		$_SESSION['ob_product_special_on'] = 0;
		if ($res->numrows > 0){
			$_SESSION['ob_product_special_on'] = (int)$res->rows[0]['value'];
		};

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);



		if ($product_info) {

            $products_materials = $this->model_catalog_product->getProductsMaterial($product_id,$category_id);

            $data['material_h2'] = $this->language->get('text_material_h2').' '.$data['breadcrumbs'][count($data['breadcrumbs'])-1]['text'].' материал: '.implode(', ',$products_materials['names']);

            $this->load->model('tool/image');

            $data['material_products'] = array();
            foreach ($products_materials['products'] as $product_item) {
                if ($product_item) {

                    if ($product_id > 0) {
                        $supplier_info = $suppliers[$product_item['supplier_id']];
                    }


                    if ($product_item['image']) {
                        //$image = $this->model_tool_image->resize($product_item['image'], $setting['width'], $setting['height']);
                        $image = $this->model_tool_image->resize($product_item['image'], 300, 300);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 300, 300);
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_item['price'], $product_item['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$product_item['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_item['special'], $product_item['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_item['special'] ? $product_item['special'] : $product_item['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_item['rating'];
                    } else {
                        $rating = false;
                    }
//*
                    if ($supplier_info['supplier_id'] > 0) {
                        $price = preg_replace('/[^0-9.]/', '', $price) * $supplier_info['coefficient'];
                        $price = ceil($price) . ' грн.';
                        //$special = preg_replace('/[^0-9.]/', '', $special)*$supplier_info['coefficient'];
                        //	$special = ceil($special).' грн.';
                    };
//*/


                    $images = array();

                    $result_images = $this->model_catalog_product->getProductImages($product_item['product_id']);

                    foreach ($result_images as $result_image) {
                        $images[] = array(
                            'thumb' => $this->model_tool_image->resize($result_image['image'], 256, 256)
                        );
                    }

                    $prodd_options = $this->model_catalog_product->getProductOptions($product_item['product_id']);
                    $prodd_options_new = array();
                    foreach ($prodd_options as $key1 => $option) {
                        $prodd_options_new[] = $option;
                        foreach ($option['product_option_value'] as $key2 => $option_val) {

                            if(!empty($option_val['image_prod']))
                                $images[] = array(
                                    'thumb' => $this->model_tool_image->resize($option_val['image_prod'], 256, 256)
                                );

                            if(!empty($option_val['image_prod']))
                            {
                                $prodd_options_new[$key1]['product_option_value'][$key2]['image_prod'] = $this->model_tool_image->resize($option_val['image_prod'], 300, 300);
                            }
                            $prodd_options_new[$key1]['product_option_value'][$key2]['image'] = $this->model_tool_image->resize($option_val['image'], 32, 32);

                            $supplier_id = $product_item['supplier_id'];

                            $_price = $prodd_options_new[$key1]['product_option_value'][$key2]['price'];
                            if ((int)$supplier_id > 0) {
                                $_price = preg_replace('/[^0-9.]/', '', $_price) * $suppliers[$supplier_id]['coefficient'];
                            };
                            $prodd_options_new[$key1]['product_option_value'][$key2]['price'] = ceil($_price) . ' грн.';
                        }
                    }
                    $special_info_tmp = $this->model_catalog_product->getProductSpecial($product_item['product_id']);

                    if(!empty($special_info_tmp)){

                        if(strtotime($special_info_tmp['date_end']) - time()>0)
                            $product_item['sale_status_id']=1;
                        else
                            $product_item['sale_status_id']=0;
                    }

                    $data['material_products'][] = array(
                        'product_id' => $product_item['product_id'],
                        'thumb' => $image,
                        'images' => $images,
                        'name' => $product_item['name'].' - '.$product_item['sku'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_item['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $price,
                        'old_price' => $product_item['old_price'],
                        'sale_status_name' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? $data['sale_statuses'][$product_item['sale_status_id']]['name']:'',
                        'sale_status_color' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? $data['sale_statuses'][$product_item['sale_status_id']]['color']:'',
                        'sale_status_image' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$product_item['sale_status_id']]['image']:'',
                        'special' => $special,
                        'tax' => $tax,
                        'rating' => $rating,
                        'stock_status' => $product_item['stock_status'],
                        'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product_item['product_id']),
                        'options' => $prodd_options_new,
                        'supplier' => $suppliers[$product_item['supplier_id']],
                        'privat' => $product_item['product_pp'] || $product_item['product_ii']? 1 : 0,
                    );
                }
            }

            $product_info['name'] .=' - '.$product_info['sku'];

			if ($product_id > 0) {
				$supplier_info = $suppliers[$product_info['supplier_id']];
			}

			$res = $this->db->query("SELECT `value` FROM " . DB_PREFIX . "setting WHERE `key`='ob_product_special_on'");
			$ob_product_special_on = 0;
			if (isset($res->rows[0])) $ob_product_special_on = $res->rows[0]['value'];
			$_SESSION['ob_product_special_on'] = $ob_product_special_on;
			if ($_SESSION['ob_product_special_on']) {
				$results = $this->model_catalog_product->getProductSpecials();
				if (is_array($results)) {
					foreach( $results as $res) {
						if  ($res['product_id'] == $product_info['product_id'] ) {
							$product_info['sproduct_id'] = $product_info['product_id'];
						} else
						if  ($res['sproduct_id'] == $product_info['product_id']) {
							$product_info['sproduct_id'] = $product_info['sproduct_id'];
						};
					}
				}
			}

			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			
			if ($this->language->get('code') == 'ru'){
				$this->document->setTitle($product_info['name']." - купить в Киеве, ".$category_info['name']." заказать по выгодной цене в интернет магазине мебели feshmebel.com.ua");
				$this->document->setDescription($product_info['name']." - купить мебель с доставкой по Украине 🚚, ".$category_info['name']." 🚀 заказать по лучшей цене в каталоге интернет магазина мебели Фешемебельный");
			}elseif($this->language->get('code') == 'uk'){
				$this->document->setTitle($product_info['name']." - купити в Києві, ". $category_info['name']." замовити за вигідною ціною в інтернет магазині меблів feshmebel.com.ua");
				$this->document->setDescription($product_info['name']." - купити меблі з доставкою по Україні 🚚, ". $category_info['name']." 🚀 замовити за найкращою ціною в каталозі інтернет магазину меблів Фешемебельний");
			}else{
				$this->document->setTitle($product_info['meta_title']);
				$this->document->setDescription($product_info['meta_description']);				
			}
            $data['description_meta'] = $this->document->getDescription();
			// url links
			// Start Tkach web-promo			

			$data['url_menu_delivery'] = $this->url->link('information/information&information_id=4');
			$data['url_menu_exchange'] = $this->url->link('account/return/add');
			$data['url_menu_guaranty'] = $this->url->link('information/information&information_id=9');
			
			// end web-promo
			
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			// start Tkach web-promo
			if (defined('H_ONE') ){
				$data['heading_title'] = H_ONE;
			}else{
				$data['heading_title'] = $product_info['name'];			
			}// end web-promo
			
            $data['sku'] = $product_info['sku'];
            $data['bestseller'] = $product_info['upc'];
            $data['recommended'] = $product_info['ean'];
            $data['downloads'] = array();
            $results = $this->model_catalog_product->getDownload($product_id);
            foreach ($results as $result) {
                if (file_exists(DIR_DOWNLOAD . $result['filename'])) {
                    $size = filesize(DIR_DOWNLOAD . $result['filename']);

                    $i = 0;

                    $suffix = array(
                        'B',
                        'KB',
                        'MB',
                        'GB',
                        'TB',
                        'PB',
                        'EB',
                        'ZB',
                        'YB'
                    );

                    while (($size / 1024) > 1) {
                        $size = $size / 1024;
                        $i++;
                    }

                    $data['downloads'][] = array(
                        'order_id'   => $result['order_id'],
                        'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                        'name'       => $result['name'],
                        'size'       => round(substr($size, 0, strpos($size, '.') + 4), 2) . $suffix[$i],
                        'href'       => $this->url->link('product/product/download', 'download_id=' . $result['download_id'], 'SSL')
                    );
                }
            }


			$data['button_download'] = $this->language->get('button_download');
			$data['text_select'] = $this->language->get('text_select');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_sku'] = $this->language->get('text_sku');
			$data['text_reward'] = $this->language->get('text_reward');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_stock'] = $this->language->get('text_stock');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_option'] = $this->language->get('text_option');
			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_write'] = $this->language->get('text_write');
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'));
			$data['text_note'] = $this->language->get('text_note');
			$data['text_tags'] = $this->language->get('text_tags');
			$data['text_related'] = $this->language->get('text_related');
			$data['text_viewed'] = $this->language->get('text_viewed');
			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
			$data['text_loading'] = $this->language->get('text_loading');
            $data['text_show_btn_where_watch'] = $this->language->get('text_show_btn_where_watch');
            $data['text_show_btn_try_on'] = $this->language->get('text_show_btn_try_on');

            $data['text_product_special_end'] = $this->language->get('text_product_special_end');
            $data['text_product_special_more'] = $this->language->get('text_product_special_more');

			$data['entry_qty'] = $this->language->get('entry_qty');
			$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_review'] = $this->language->get('entry_review');
			$data['entry_rating'] = $this->language->get('entry_rating');
			$data['entry_good'] = $this->language->get('entry_good');
			$data['entry_bad'] = $this->language->get('entry_bad');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_expect'] = $this->language->get('button_expect');
            $data['button_out_of_sale'] = $this->language->get('button_out_of_sale');
            $data['button_out_of_sale_img'] = $this->language->get('button_out_of_sale_img');
			$data['button_toorder1'] = $this->language->get('button_toorder1');
			$data['button_toorder2'] = $this->language->get('button_toorder2');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_upload'] = $this->language->get('button_upload');
			$data['button_continue'] = $this->language->get('button_continue');

			$this->load->model('catalog/review');

			$data['tab_description'] = $this->language->get('tab_description');
			$data['tab_attribute'] = $this->language->get('tab_attribute');
			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);
			$data['number_review'] = $product_info['reviews'];
	
			// description start Tkach web-promo
			$data['title_description'] = sprintf($this->language->get('title_description'));
			$data['nopay_install'] = sprintf($this->language->get('nopay_install'));
			$data['noneed_install'] = sprintf($this->language->get('noneed_install'));
			$data['title_attribute'] = sprintf($this->language->get('title_attribute'));
			$data['title_delivery'] = sprintf($this->language->get('title_delivery'));
			$data['title_installation'] = sprintf($this->language->get('title_installation'));
			$data['description_installation'] = sprintf($this->language->get('description_installation'));
			$data['revised_goods'] = sprintf($this->language->get('revised_goods'));
			$data['description_delivery_1'] = sprintf($this->language->get('description_delivery_1'));
			$data['description_delivery_2'] = sprintf($this->language->get('description_delivery_2'));
			$data['description_delivery_3'] = sprintf($this->language->get('description_delivery_3'));
			$data['description_delivery_4'] = sprintf($this->language->get('description_delivery_4'));
			$data['description_delivery_5'] = sprintf($this->language->get('description_delivery_5'));
			$data['description_delivery_6'] = sprintf($this->language->get('description_delivery_6'));
			$data['description_nopaydelivery_1'] = sprintf($this->language->get('description_nopaydelivery_1'));
			$data['description_nopaydelivery_2'] = sprintf($this->language->get('description_nopaydelivery_2'));
	

			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['manufacturer'] = $product_info['manufacturer'];
			$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['model'] = $product_info['model'];
			$data['youtube'] = $product_info['youtube'];
			$data['sku'] = $product_info['sku'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$data['curr_lang'] = $this->language->get('code');
            $data['stock_image'] = (empty($product_info['stock_image'])) ? '' : '/image/'.$product_info['stock_image'];
    

			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}
            
			$data['supplier'] = $suppliers[$data['product_id']];

			$this->load->model('tool/image');

			if ($product_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
				);
			}

			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['price'] = false;
			}
			// start Tkach web-promo

// tkach			
// echo '<br>$data[price]='.$data['price'];
// echo '<br>$supplier_info[coefficient]='.$supplier_info['coefficient'];

            $this->load->model('catalog/supplier');
            $_suppliers = $this->model_catalog_supplier->getSuppliers();
            $suppliers = array();
            foreach($_suppliers as $supplier)
                $suppliers[$supplier['supplier_id']] = $supplier;

			if ($product_info['supplier_id'] > 0 ) {
				$data['price'] = preg_replace('/[^0-9.]/', '', $data['price'])*$suppliers[$product_info['supplier_id']]['coefficient'];
				$data['price'] = ceil($data['price']).' грн.';
				$product_info['price'] = preg_replace('/[^0-9.]/', '', $product_info['price'])*$suppliers[$product_info['supplier_id']]['coefficient'];
				$product_info['price'] = ceil($product_info['price']).' грн.';
				if(empty($special_info)&&false){
                    $product_info['special'] = preg_replace('/[^0-9.]/', '', $product_info['special'])*$suppliers[$product_info['supplier_id']]['coefficient'];
                    $product_info['special'] = ceil($product_info['special']).' грн.';
                }

			};
			$data['price'] = ceil($data['price']).' грн.';
			$product_info['price'] = ceil($product_info['price']).' грн.';
			
			if ((float)$product_info['special']) {
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$data['special'] = false;
			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($_price, $product_info['tax_class_id'], $this->config->get('config_tax')))
				);				
			}
		
// tkach			
// echo '<br>$data[price]='.print_r($data['discounts']);

			$data['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}
						$_price = $price;
						if ($supplier_info['supplier_id'] > 0) {
							$_price = (float)preg_replace('/[^0-9.]/', '', $price) * (float)$supplier_info['coefficient'];
							$_price = ceil($_price).' грн.';
							if(empty($special_info)){
                                $_special = (float)preg_replace('/[^0-9.]/', '', $special) * (float)$supplier_info['coefficient'];
                                $_special = ceil($_special).' грн.';
                            }
						};
						$_price = ceil($_price).' грн.';

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 100, 100),
							'image_prod'              => $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
							'price'                   => $_price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);

			}

			$_price = ceil($product_info['price']).' грн.';
			$_special = $product_info['special'];

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}

			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['number_rating'] = (int)$product_info['reviews'];
			$data['rating'] = (int)$product_info['rating'];


            $data['reviews_rating'] = 0;
            $data['reviews_item'] = array();
            $results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id']);

            foreach ($results as $result) if((int)$result['rating']>0){
                $data['reviews_item'][] = array(
                    'author'     => $result['author'],
                    'text'       => str_replace('"','\"',nl2br($result['text'])),
                    'rating'     => (int)$result['rating'],
                    'date_added' => date('Y-m-d', strtotime($result['date_added']))
                );
                $data['reviews_rating']+=(int)$result['rating'];
            }

            $data['reviews_count'] = count($data['reviews_item']);
            if($data['reviews_count'])
                $data['reviews_rating'] = round($data['reviews_rating']/$data['reviews_count'],0);
			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
			} else {
				$data['captcha'] = '';
			}
			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);


			$arr = array();
			foreach ($data['attribute_groups'] as $key0 => $attribute_group) {
		if ( $attribute_group['attribute_group_id'] == 1){
					foreach ($attribute_group['attribute'] as $key => $attribute) {

						if ( $attribute_group['attribute'][$key]['attribute_id'] == 61 ) {
							$text_material = $attribute_group['attribute'][$key]['name'];
							$material	= $attribute_group['attribute'][$key]['text'];
							$arr[]	= $material;
							unset( $data['attribute_groups'][$key0]['attribute'][$key] );
						}else
		if ( $attribute_group['attribute'][$key]['attribute_id'] == 74 ||  $attribute_group['attribute'][$key]['attribute_id'] == 442){
								$text_size = $attribute_group['attribute'][$key]['name'];
								$size	= $attribute_group['attribute'][$key]['text'];
								$arr[]	= $size;
								unset( $data['attribute_groups'][$key0]['attribute'][$key] );
						};
					}
					foreach ($attribute_group['attribute'] as $key => $attribute) {
						$arr[]	= $attribute_group['attribute'][$key];
					}
					$data['attribute_groups'][$key0]['attribute'] = $arr;
				}
			};
			$data['text_material']	= $text_material;
			$data['text_size']		= $text_size;			
			$data['material']		= $material;
			$data['size']			= $size;
			$data['old_price']		= $product_info['old_price'];
			$data['sale_status_id']	= $product_info['sale_status_id'];
            $data['show_btn_try_on']	= $product_info['show_btn_try_on'];
            $data['show_btn_where_watch']	= $product_info['show_btn_where_watch'];

            $special_info = $this->model_catalog_product->getProductSpecial($product_id);

            $this->load->model('localisation/sale_status');
            if(!empty($special_info)){
                $data['special_info'] = $special_info;
                if(strtotime($special_info['date_end']) - time()>0)
                    $data['sale_status_id']=1;
                else
                    $data['sale_status_id']=0;
            }
            $data['sale_statuses'] = $this->model_localisation_sale_status->getSaleStatuses();

            $data['sale_status_name'] = isset($data['sale_statuses'][$data['sale_status_id']]) ? $data['sale_statuses'][$data['sale_status_id']]['name']:'';
            $data['sale_status_color'] = isset($data['sale_statuses'][$data['sale_status_id']]) ? $data['sale_statuses'][$data['sale_status_id']]['color']:'';
            $data['sale_status_image'] = isset($data['sale_statuses'][$data['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$data['sale_status_id']]['image']:'';
            $product_info['sale_status_name'] = isset($data['sale_statuses'][$data['sale_status_id']]) ? $data['sale_statuses'][$data['sale_status_id']]['name']:'';
            $product_info['sale_status_color'] = isset($data['sale_statuses'][$data['sale_status_id']]) ? $data['sale_statuses'][$data['sale_status_id']]['color']:'';
            $product_info['sale_status_image'] = isset($data['sale_statuses'][$data['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$data['sale_status_id']]['image']:'';


			$data['products'] = array();

			$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
			if (count($results)){
			foreach ($results as $result) {
                $product_item = $this->model_catalog_product->getProduct($result['product_id']);

                if ($product_item) {

                    if ($product_id > 0) {
                        $supplier_info = $suppliers[$product_item['supplier_id']];
                    }


                    if ($product_item['image']) {
                        //$image = $this->model_tool_image->resize($product_item['image'], $setting['width'], $setting['height']);
                        $image = $this->model_tool_image->resize($product_item['image'], 300, 300);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 300, 300);
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_item['price'], $product_item['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$product_item['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_item['special'], $product_item['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_item['special'] ? $product_item['special'] : $product_item['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_item['rating'];
                    } else {
                        $rating = false;
                    }
//*
                    if ($supplier_info['supplier_id'] > 0) {
                        $price = preg_replace('/[^0-9.]/', '', $price) * $supplier_info['coefficient'];
                        $price = ceil($price) . ' грн.';
                        //$special = preg_replace('/[^0-9.]/', '', $special)*$supplier_info['coefficient'];
                        //	$special = ceil($special).' грн.';
                    };
//*/


                    $images = array();

                    $result_images = $this->model_catalog_product->getProductImages($product_item['product_id']);

                    foreach ($result_images as $result_image) {
                        $images[] = array(
                            'thumb' => $this->model_tool_image->resize($result_image['image'], 256, 256)
                        );
                    }

                    $prodd_options = $this->model_catalog_product->getProductOptions($product_item['product_id']);
                    $prodd_options_new = array();
                    foreach ($prodd_options as $key1 => $option) {
                        $prodd_options_new[] = $option;
                        foreach ($option['product_option_value'] as $key2 => $option_val) {

                            if(!empty($option_val['image_prod']))
                                $images[] = array(
                                    'thumb' => $this->model_tool_image->resize($option_val['image_prod'], 256, 256)
                                );

                            if(!empty($option_val['image_prod']))
                            {
                                $prodd_options_new[$key1]['product_option_value'][$key2]['image_prod'] = $this->model_tool_image->resize($option_val['image_prod'], 300, 300);
                            }
                            $prodd_options_new[$key1]['product_option_value'][$key2]['image'] = $this->model_tool_image->resize($option_val['image'], 32, 32);

                            $supplier_id = $product_item['supplier_id'];

                            $_price = $prodd_options_new[$key1]['product_option_value'][$key2]['price'];
                            if ((int)$supplier_id > 0) {
                                $_price = preg_replace('/[^0-9.]/', '', $_price) * $suppliers[$supplier_id]['coefficient'];
                            };
                            $prodd_options_new[$key1]['product_option_value'][$key2]['price'] = ceil($_price) . ' грн.';
                        }
                    }
                    $special_info_tmp = $this->model_catalog_product->getProductSpecial($product_item['product_id']);

                    if(!empty($special_info_tmp)){

                        if(strtotime($special_info_tmp['date_end']) - time()>0)
                            $product_item['sale_status_id']=1;
                        else
                            $product_item['sale_status_id']=0;
                    }

                    $data['products'][] = array(
                        'product_id' => $product_item['product_id'],
                        'thumb' => $image,
                        'images' => $images,

                        'name' => $product_item['name'].' - '.$product_item['sku'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_item['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $price,
                        'old_price' => $product_item['old_price'],
                        'sale_status_name' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? $data['sale_statuses'][$product_item['sale_status_id']]['name']:'',
                        'sale_status_color' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? $data['sale_statuses'][$product_item['sale_status_id']]['color']:'',
                        'sale_status_image' =>isset($data['sale_statuses'][$product_item['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$product_item['sale_status_id']]['image']:'',
                        'special' => $special,
                        'tax' => $tax,
                        'rating' => $rating,
                        'stock_status' => $product_item['stock_status'],
                        'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product_item['product_id']),
                        //'options'     => $this->model_catalog_product->getProductOptions($product_item['product_id']),
                        'options' => $prodd_options_new,
                        'supplier' => $suppliers[$product_item['supplier_id']],
                        'privat' => $product_item['product_pp'] || $product_item['product_ii']? 1 : 0,
                    );
                }
			}
			} else {
				$data['price'] = $_price;
				$data['product_info']['price'] = $_price;
			};
//echo '<br>$data[price](1)='.$data['price'];

			$result = $this->model_catalog_product->getProduct($product_id);
			if (isset($result['product_id']))
			{
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
		$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				$_price = $price;
				if ($supplier_info['supplier_id'] > 0 ) {
					$_price = preg_replace('/[^0-9.]/', '', $price)*$supplier_info['coefficient'];
					$_special = preg_replace('/[^0-9.]/', '', $special)*$supplier_info['coefficient'];
				};
					$_price = ceil($_price).' грн.';
					$_special = ceil($_special).' грн.';
										
				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

                $special_info_tmp = $this->model_catalog_product->getProductSpecial($result['product_id']);

                if(!empty($special_info_tmp)){

                    if(strtotime($special_info_tmp['date_end']) - time()>0)
                        $result['sale_status_id']=1;
                    else
                        $result['sale_status_id']=0;
                }

				$d = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'].' - '.$result['sku'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $_price,
					'special'     => $_special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating,
                    'sale_status_name' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['name']:'',
                    'sale_status_color' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['color']:'',
                    'sale_status_image' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$result['sale_status_id']]['image']:'',
                    'stock_status'      => $result['stock_status'],
                    'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					'supplier_id' => $result['supplier_id'],
                    'privat' => $result['product_pp'] || $result['product_ii']? 1 : 0,
				);
				$data['products'][] = $d;
				$_SESSION['products'][$result['product_id']] =
					end($data['products']);
				unset($_SESSION['products'][$result['product_id']]['description']);
			}

			$data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}

			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$data['product_info'] = $product_info;
            $data['product_info']['privat'] = $product_info['product_pp'] || $product_info['product_ii']? 1 : 0;

            $arr_new = array(


                0 => Array
                (
                    0 => '{Breadcrumb}',
                ),

                1 => Array
                (
                    0 => 'купить',
                    1 => 'приобрести',
                    2 => 'заказать',
                    3 => 'выбрать',
                    4 => 'подобрать',
                    5 => 'выбрать и купить',
                    6 => 'выбрать и приобрести',
                    7 => 'выбрать и заказать',
                    8 => 'подобрать и купить',
                    9 => 'подобрать и приобрести',
                    10 => 'подобрать и заказать',
                ),

                2 => Array
                (
                    0 => 'по выгодной цене',
                    1 => 'по выгодной стоимости',
                    2 => 'по доступной цене',
                    3 => 'по доступной стоимости',
                    4 => 'по нормальной цене',
                    5 => 'по нормальной стоимости',
                    6 => 'по хорошей цене',
                    7 => 'по хорошей стоимости',
                    8 => 'по выгодной цене',
                    9 => 'по реальной стоимости',
                    10 => 'по адекватной цене',
                    11 => 'по адекватной стоимости',
                    12 => 'по отличной цене',
                    13 => 'по отличной цене',
                    14 => 'по оправданной цене',
                    15 => 'по оправданной стоимости',
                    16 => 'по лучшей цене',
                    17 => 'по лучшей стоимости',
                ),

                4 => Array
                (
                    0 => 'в',
                ),

                5 => Array
                (
                    0 => 'интернет магазине мебели Фешемебельный',
                    1 => 'онлайн магазине мебели Фешемебельный',
                    2 => 'интернет магазине мебели feshmebel.com.ua',
                    3 => 'интернет магазине мебели Feshmebel',
                    4 => 'онлайн магазине мебели feshmebel.com.ua',
                    5 => 'онлайн магазине мебели Feshmebel',
                    6 => 'магазине мебели Фешемебельный',
                    7 => 'магазине мебели feshmebel.com.ua',
                    8 => 'магазине мебели Feshmebel',
                    9 => 'каталоге мебели Фешемебельный',
                    10 => 'каталоге мебели feshmebel.com.ua',
                    11 => 'каталоге мебели Feshmebel',
                    12 => 'интернет каталоге мебели Фешемебельный',
                    13 => 'интернет каталоге мебели feshmebel.com.ua',
                    14 => 'интернет каталоге мебели Feshmebel',
                    15 => 'каталоге Фешемебельный',
                    16 => 'интернет магазине Фешемебельный',
                    17 => 'онлайн магазине feshmebel.com.ua',
                ),

                6 => Array
                (
                    0 => 'с быстрой доставкой',
                    1 => 'с оперативной доставкой',
                    2 => 'с удобной доставкой',
                    3 => 'со своевременной доставкой',
                ),

                7 => Array
                (
                    0 => 'по Киеву и всей Украине.',
                    1 => 'по Киеву.',
                    2 => 'по Украине.',
                    3 => 'во все города Украины.',
                    4 => 'в любой уголок Украины.',
                    5 => 'в любой уголок Киева и Украины.',
                    6 => 'в любой уголок Киева.',
                    7 => 'во все регионы Украины.',
                ),

                8 => Array
                (
                    0 => 'Позвоните нашему',
                    1 => 'Позвоните',
                    2 => 'Наберите',
                    3 => 'Наберите нашему',
                    4 => 'Напишите',
                    5 => 'Напишите нашему',
                ),

                9 => Array
                (
                    0 => 'менеджеру',
                    1 => 'консультанту',
                    2 => 'сотруднику',
                    3 => 'продавцу',
                    4 => 'специалисту',
                    5 => 'эксперту',
                ),

                10 => Array
                (
                    0 => 'и мы',
                ),

                11 => Array
                (
                    0 => 'привезем',
                    1 => 'доставим',
                    2 => 'сделаем поставку',
                    3 => 'совершим доставку',
                    4 => 'совершим поставку',
                    5 => 'организуем доставку',
                    6 => 'организуем поставку',
                    7 => 'сделаем доставку',
                ),

                12 => Array
                (
                    0 => 'в {Сity}',
                ),

                13 => Array
                (
                    0 => 'и другие уголки страны.',
                    1 => 'и другие регионы страны.',
                    2 => 'и другие регионы.',
                    3 => 'и другие города.',
                ),

                14 => Array
                (
                    0 => '{category_3}',
                ),

                15 => Array
                (
                    0 => 'или другой аналог',
                ),

                16 => Array
                (
                    0 => 'лучших',
                    1 => 'новых',
                    2 => 'оригинальных',
                    3 => 'уникальных',
                    4 => 'доступных',
                    5 => 'приятных',
                    6 => 'качественных',
                    7 => 'выгодных',
                ),

                17 => Array
                (
                    0 => 'товаров',
                    1 => 'изделий',
                ),

                18 => Array
                (
                    0 => 'от производителя,',
                ),

                19 => Array
                (
                    0 => 'можно',
                    1 => 'удобно',
                ),

                20 => Array
                (
                    0 => 'заказать',
                    1 => 'приобрести',
                    2 => 'подобрать и заказать',
                    3 => 'купить',
                    4 => 'подобрать',
                    5 => 'выбрать и купить',
                    6 => 'выбрать и приобрести',
                    7 => 'выбрать и заказать',
                    8 => 'подобрать и купить',
                    9 => 'подобрать и приобрести',
                ),

                21 => Array
                (
                    0 => 'из',
                ),

                22 => Array
                (
                    0 => 'широкого',
                    1 => 'огромного',
                    2 => 'обширного',
                    3 => 'богатого',
                    4 => 'большого',
                    5 => 'крупного',
                ),

                23 => Array
                (
                    0 => 'ассортимента',
                    1 => 'выбора',
                ),

                24 => Array
                (
                    0 => 'товаров.',
                    1 => 'изделий.',
                    2 => 'продукции.',
                ),

                25 => Array
                (
                    0 => 'Лучшее предложение,',
                    1 => 'Специальное предложение,',
                    2 => 'Самое лучшее предложение,',
                    3 => 'Выгодное предложение,',
                    4 => 'Заманчивое предложение,',
                ),

                26 => Array
                (
                    0 => 'купить',
                ),

                27 => Array
                (
                    0 => '{Breadcrumb}',
                ),

                28 => Array
                (
                    0 => 'на сайте Фешемебельный или по номеру',
                    1 => 'на сайте feshmebel.com.ua или по номеру',
                ),

                29 => Array
                (
                    0 => '(044) 383 83 85.',
                    1 => '0-800-330-190.',
                ),


            );
            $arr_new_ua = array
            (
                0 => array
                (
                    0 => '{Breadcrumb}',
                ),

                1 => array
                (
                    0 => 'купити',
                    1 => 'придбати',
                    2 => 'замовити',
                    3 => 'вибрати',
                    4 => 'підібрати',
                    5 => 'вибрати і купити',
                    6 => 'вибрати і придбати',
                    7 => 'вибрати і замовити',
                    8 => 'підібрати і купити',
                    9 => 'підібрати і придбати',
                    10 => 'підібрати і замовити'
                ),

                2 => array
                (
                    0 => 'за вигідною ціною',
                    1 => 'за вигідною вартістю',
                    2 => 'за доступною ціною',
                    3 => 'за доступною вартістю',
                    4 => 'за нормальною ціною',
                    5 => 'за нормальною вартістю',
                    6 => 'за хорошою ціною',
                    7 => 'за хорошою вартістю',
                    8 => 'за вигідною ціною',
                    9 => 'за реальною вартістю',
                    10 => 'за адекватною ціною',
                    11 => 'за адекватною вартістю',
                    12 => 'за відмінною ціною',
                    13 => 'за відмінною вартістю',
                    14 => 'по виправданою ціною',
                    15 => 'За виправданою вартістю',
                    16 => 'за найкращою ціною',
                    17 => 'за найкращою вартістю'
                ),

                4 => array
                (
                    0 => 'в'
                ),

                5 => array
                (
                    0 => 'інтернет магазині меблів Фешемебельний',
                    1 => 'онлайн магазині меблів Фешемебельний',
                    2 => 'інтернет магазині меблів feshmebel.com.ua',
                    3 => 'інтернет магазині меблів Feshmebel',
                    4 => 'онлайн магазині меблів feshmebel.com.ua',
                    5 => 'онлайн магазині меблів Feshmebel',
                    6 => 'магазині меблів Фешемебельний',
                    7 => 'магазині меблів feshmebel.com.ua',
                    8 => 'магазині меблів Feshmebel',
                    9 => 'каталозі меблів Фешемебельний',
                    10 => 'каталозі меблів feshmebel.com.ua',
                    11 => 'каталозі меблів Feshmebel',
                    12 => 'інтернет каталозі меблів Фешемебельний',
                    13 => 'інтернет каталозі меблів feshmebel.com.ua',
                    14 => 'інтернет каталозі меблів Feshmebel',
                    15 => 'каталозі Фешемебельний',
                    16 => 'інтернет магазині Фешемебельний',
                    17 => 'онлайн магазині feshmebel.com.ua'
                ),

                6 => array
                (
                    0 => 'з швидкою доставкою',
                    1 => 'з оперативною доставкою',
                    2 => 'зі зручною доставкою',
                    3 => 'зі своєчасною доставкою'
                ),

                7 => array
                (
                    0 => 'по Києву і всій Україні.',
                    1 => 'по Києву.',
                    2 => 'по Україні.',
                    3 => 'в усі міста України.',
                    4 => 'в будь-який куточок України.',
                    5 => 'в будь-який куточок Києва та України.',
                    6 => 'в будь-який куточок Києва.',
                    7 => 'в усі регіони України.'
                ),

                8 => array
                (
                    0 => 'зателефонуйте нашому',
                    1 => 'зателефонуйте',
                    2 => 'наберіть',
                    3 => 'наберіть нашому',
                    4 => 'Напишіть',
                    5 => 'Напишіть нашому'
                ),

                9 => array
                (
                    0 => 'менеджеру',
                    1 => 'консультанту',
                    2 => 'співробітнику',
                    3 => 'продавцеві',
                    4 => 'фахівцеві',
                    5 => 'експерту'
                ),

                10 => array
                (
                    0 => 'і ми'
                ),

                11 => array
                (
                    0 => 'привеземо',
                    1 => 'доставимо',
                    2 => 'зробимо поставку',
                    3 => 'зробимо доставку',
                    4 => 'зробимо поставку',
                    5 => 'організуємо доставку',
                    6 => 'організуємо поставку',
                    7 => 'зробимо доставку'
                ),

                12 => array
                (
                    0 => 'в {Сity}'
                ),

                13 => array
                (
                    0 => 'та інші куточки країни.',
                    1 => 'і інші регіони країни.',
                    2 => 'і інші регіони.',
                    3 => 'та інші міста.'
                ),

                14 => array
                (
                    0 => '{category_3}'
                ),

                15 => array
                (
                    0 => 'або інший аналог'
                ),

                16 => array
                (
                    0 => 'кращих',
                    1 => 'нових',
                    2 => 'оригінальних',
                    3 => 'унікальних',
                    4 => 'доступних',
                    5 => 'приємних',
                    6 => 'якісних',
                    7 => 'вигідних'
                ),

                17 => array
                (
                    0 => 'товарів',
                    1 => 'виробів'
                ),

                18 => array
                (
                    0 => 'від виробника,'
                ),

                19 => array
                (
                    0 => 'можна, можливо',
                    1 => 'зручно'
                ),

                20 => array
                (
                    0 => 'замовити',
                    1 => 'придбати',
                    2 => 'підібрати і замовити',
                    3 => 'купити',
                    4 => 'підібрати',
                    5 => 'вибрати і купити',
                    6 => 'вибрати і придбати',
                    7 => 'вибрати і замовити',
                    8 => 'підібрати і купити',
                    9 => 'підібрати і придбати'
                ),

                21 => array
                (
                    0 => 'з'
                ),

                22 => array
                (
                    0 => 'широкого',
                    1 => 'величезного',
                    2 => 'обширного',
                    3 => 'багатого',
                    4 => 'великого',
                    5 => 'крупного'
                ),

                23 => array
                (
                    0 => 'асортименту',
                    1 => 'вибору'
                ),

                24 => array
                (
                    0 => 'товарів.',
                    1 => 'виробів.',
                    2 => 'продукції.'
                ),

                25 => array
                (
                    0 => 'Кращу пропозицію,',
                    1 => 'Спеціальна пропозиція,',
                    2 => 'Найкраща пропозиція,',
                    3 => 'Вигідна пропозиція,',
                    4 => 'Приваблива пропозиція,'
                ),

                26 => array
                (
                    0 => 'купити'
                ),

                27 => array
                (
                    0 => '{Breadcrumb}'
                ),

                28 => array
                (
                    0 => 'на сайті Фешемебельний або за номером',
                    1 => 'на сайті feshmebel.com.ua або за номером'
                ),

                29 => array
                (
                    0 => '(044) 334-35-32.',
                    1 => '0-800-330-190.'
                )

            );
            if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                $arr_new = $arr_new_ua;
            $text =array();
            foreach ($arr_new as $key=>$val)
            {
                $text[] = $val[rand(0,count($val)-1)];
            }
            $cities =array(
                'Киев',
                'Харьков',
                'Одесса',
                'Днепропетровск',
                'Запорожье',
                'Львов',
                'Кривой Рог',
                'Николаев',
                'Мариуполь',
                'Винница',
                'Херсон',
                'Полтава',
                'Чернигов',
                'Черкассы',
                'Житомир',
                'Сумы',
                'Хмельницкий',
                'Ровно',
                'Кропивницкий',
                'Днепродзержинск',
                'Черновцы',
                'Кременчуг',
                'Ивано-Франковск',
                'Тернополь',
                'Белая Церковь',
                'Луцк',
                'Мелитополь',
                'Никополь',
                'Бердянск',
                'Ужгород',
                'Павлоград',
                'Каменец-Подольский'
            );
            $cities_ua =array(

                'Київ',
                'Харків',
                'Одеса',
                'Дніпропетровськ',
                'Запоріжжя',
                'Львів',
                'Кривий Ріг',
                'Миколаїв',
                'Маріуполь',
                'Вінниця',
                'Херсон',
                'Полтава',
                'Чернігів',
                'Черкаси',
                'Житомир',
                'Суми',
                'Хмельницький',
                'Рівне',
                'Кропивницький',
                'Дніпродзержинськ',
                'Чернівці',
                'Кременчук',
                'Івано-Франківськ',
                'Тернопіль',
                'Біла Церква',
                'Луцьк',
                'Мелітополь',
                'Нікополь',
                'Бердянськ',
                'Ужгород',
                'Павлоград',
                'Кам\'янець-Подільський',

            );

            if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                $cities = $cities_ua;
            $city_val=array();

            while(count($city_val)<3)
            {
               $val = $cities[rand(0,count($cities)-1)];
               $city_val[$val] = $val;
            }
            $category_3 =array(
                'Мебель для столовой',
                'Столы кухонные',
                'Опоры для столов',
                'Обеденные комплекты',
                'Стулья кухонные',
                'Барные стулья',
                'Столы',
                'Столы письменные',
                'Столешницы для столов',
                'Стулья',
                'Офисная мебель',
                'Кресла офисные',
                'Стулья офисные',
                'Мебель для гостинной',
                'Кресла',
                'Журнальные столы'
            );
            $category_3_ua =array(
                'Меблі для їдальні',
                'Столи кухонні',
                'Опори для столів',
                'Обідні комплекти',
                'Стільці',
                'Барні стільці',
                'Столи',
                'Столи письмові',
                'Стільниці для столів',
                'Стільці',
                'Офісні меблі',
                'Крісла офісні',
                'Стільці офісні',
                'Меблі для вітальні',
                'Крісла',
                'Журнальні столи',
            );
            if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                $category_3 = $category_3_ua;
            $category_3_val=array();

            while(count($category_3_val)<3)
            {
               $val = $category_3[rand(0,count($category_3)-1)];
                $category_3_val[$val] = $val;
            }
            $data['text_seo'] = str_replace(['{Breadcrumb}','{Сity}','{category_3}'],[$data['heading_title'],implode(', ',$city_val),implode(', ',$category_3_val)],implode(' ',$text));


            require_once('./tools/Mobile_Detect.php');
            $detect = new Mobile_Detect;
            if($detect->isMobile())
            {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product_mobile.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product_mobile.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/product/product_mobile.tpl', $data));
                }
            }
            else
            {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/product.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/product.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/product/product.tpl', $data));
                }
            }

		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}

	public function review() {
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		$data['text_no_reviews'] = $this->language->get('text_no_reviews');
		$data['lang'] = $this->request->get['lang'];

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/review.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/review.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/review.tpl', $data));
		}
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 0)) {
				$json['error'] = $this->language->get('error_text');
			}

			/* if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			} */

			// Captcha
			if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			}

			if (!isset($json['error'])) {
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getRecurringDescription() {
		$this->language->load('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')));
				$_price = $price;
				if ($supplier_info['supplier_id'] > 0 ) {
					$_price = preg_replace('/[^0-9.]/', '', $price)*$supplier_info['coefficient'];
					$_special = preg_replace('/[^0-9.]/', '', $special)*$supplier_info['coefficient'];
					$_special = ceil($_special).' грн.';
				};
				$price = ceil($_price).' грн.';
				
				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	//--------------------------------------------------------------------------
	public function addtolog($str, $name = 'debug.log') {
		if (file_exists($name)) {
			if (filesize($name)>(50*1024*1024)) return false;
			if (filesize($name)>(50*1024*1024)) { unlink($name);  $file = fopen ($name,"w+"); } else
			$file = fopen ($name,"a+");
		} else {
			$file = fopen ($name,"w+");
		};
		@fputs($file, date("d.m.Y h:i:s",time()).' '.$str);
		fputs($file, "\r");
		fclose ( $file );
	}

}
