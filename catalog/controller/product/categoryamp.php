<?php

class ControllerProductCategoryamp extends Controller
{
    public function index()
    {

        $this->load->model('catalog/supplier');
        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach ($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;

        $this->load->language('product/category');
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $this->session->data['url_refferer'] = $this->request->server['REQUEST_URI'];

        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_product_limit');
        }

        $limit = 50;

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['path'])) {
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = (int)$path_id;
                } else {
                    $path .= '_' . (int)$path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        //'href' => $this->url->link('product/category', 'path=' . $path . $url)
                        'href' => $this->url->link('product/category', 'path=' . $path)
                    );
                }
            }
        } else {
            $category_id = 0;
        }

        $category_info = $this->model_catalog_category->getCategory($category_id);





        if ($category_info) {

            $mfp_index = array();
            if(isset($_GET['mfp']))
            {
                preg_match_all('/\[(.*?)\]/',$_GET['mfp'],$matches);
                foreach ($matches[1] as $item)
                {

                    $item=explode(',',$item);
                    if(!isset($mfp_index[count($item)]))
                        $mfp_index[count($item)]=0;
                    $mfp_index[count($item)]++;
                }
            }
            $data['noindex']=0;
            if(count($mfp_index)>1 || (count($mfp_index)==1 && !isset($mfp_index[1])) || (count($mfp_index)==1 && isset($mfp_index[1]) && $mfp_index[1]>2))
                $data['noindex']=1;

            if(count($mfp_index)==1 && isset($mfp_index[1]) && $mfp_index[1]<3)
            {

                $mfp_attr = array();
                $mfp_tmp = explode(']',$_GET['mfp']);
                foreach ($mfp_tmp as $key=>$item)
                {
                    $item = explode('[',$item);
                    $item = explode(',',$item[0]);
                    $item =  explode('f-',$item[count($item)-1]);
                    if(!empty($item[0]))
                        $mfp_attr[]=$item[0];

                }
                $filter_group_description_tmp = array();
                $filter_group_description = $this->db->query("SELECT * FROM `oc_filter_group_description` WHERE `filter_group_id` in (".implode(',',$mfp_attr).") AND `language_id` = '" . (int)$this->config->get('config_language_id') . "'")->rows;
                foreach ($filter_group_description as $item)
                    $filter_group_description_tmp[$item['filter_group_id']] = $item['name'];
                $filter_description_tmp = array();
                $filter_description = $this->db->query("SELECT * FROM `oc_filter_description` WHERE `filter_id` in (".implode(',',$matches[1]).") AND `language_id` = '" . (int)$this->config->get('config_language_id') . "'")->rows;
                foreach ($filter_description as $item)
                    $filter_description_tmp[$item['filter_id']] = $item['name'];
                $filter_description_seo=array();
                foreach ($mfp_attr as $key=>$filter_group_id)
                {
                    $filter_description_seo[] =$filter_group_description_tmp[$filter_group_id].' - '.$filter_description_tmp[$matches[1][$key]];
                }
                $category_info['name'] = $category_info['name'].': '.implode(', ',$filter_description_seo);

            }

            // start Tkach web-promo
            if (defined('H_ONE')) {
                $data['heading_title'] = H_ONE;
            } else {
                $data['heading_title'] = $category_info['name'];
                $data['heading_breadcrumb'] = $category_info['name'];
            }


            $filterDescriptions = $this->model_catalog_category->getFilterDescriptionsUrl(str_replace('/amp/','/',$_SERVER['REQUEST_URI']));
            $showmeta = 0;
            if(isset($filterDescriptions["meta_title"]))
            {
                $filter_metatitle = html_entity_decode($filterDescriptions["meta_title"], ENT_QUOTES, 'UTF-8');
                $filter_metadescr = html_entity_decode($filterDescriptions["meta_description"], ENT_QUOTES, 'UTF-8');
                $filter_metah1 = html_entity_decode($filterDescriptions["meta_h1"], ENT_QUOTES, 'UTF-8');
                $showmeta = $filterDescriptions["showmeta"];
            }

            if (defined('TITLE') and TITLE != '') {




                if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                    $category_info['meta_title'] = $filter_metatitle;
                } else {
                    $category_info['meta_title'] = TITLE;
                }
                if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                    $category_info['meta_description'] = $filter_metadescr;
                } else {
                    $category_info['meta_description'] = DESCRIPTION;
                }

                if (!empty($filter_metah1) && $showmeta) {
                    $data['heading_title'] = $filter_metah1;
                }

            } else {
                if (defined('_FILTER_NAME_') && defined('page_for_seo') && page_for_seo > 1) {
                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купити ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Києві, Україні | Недорогі ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): ціни - сторінка ' . page_for_seo;
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в інтернет-магазині меблів для дому ➤ Купити ' . mb_strtolower($category_info['name']) . ' з доставкою по Києву та Україні ✔ Кращі ціни ✔ Гарантія якості ✔ Широкий асортимент! - сторінка ' . page_for_seo;
                        }

                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')')) . ' - сторінка ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';
                    } else {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купить ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Киеве, Украине | Недорогие ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): цены - страница ' . page_for_seo;
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в интернет-магазине мебели для дома ➤ Купить ' . mb_strtolower($category_info['name']) . ' с доставкой по Киеву и Украине ✔ Лучшие цены ✔ Гарантия качества ✔ Широкий ассортимент! - страница ' . page_for_seo;
                        }
                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')')) . ' - сторінка ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';
                    }
                } elseif (defined('page_for_seo') && page_for_seo > 1) {

                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        $category_info['meta_title'] = 'Сторінка ' . page_for_seo . ' - розділ ' . mb_strtolower($category_info['name']) . ' інтернет-магазина Фешемебельний';
                        $category_info['meta_description'] = 'Сторінка ' . page_for_seo . ' розділу ' . mb_strtolower($category_info['name']) . ' Величезний вибір меблів, швидка доставка по Києву і всій Україні від інтернет-магазину Фешемебельний  (044)38-38-38-5';
                        $data['heading_title'] = $category_info['name'] . ' - сторінка ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'];
                    } else {
                        $category_info['meta_title'] = 'Страница ' . page_for_seo . ' раздела ' . mb_strtolower($category_info['name']) . ' интернет-магазина Фешемебельный';
                        $category_info['meta_description'] = 'Страница ' . page_for_seo . ' раздела ' . mb_strtolower($category_info['name']) . ' Огромный выбор мебели, быстрая доставка по Киеву и всей Украине от интернет-магазина Фешемебельный (044)38-38-38-5';
                        $data['heading_title'] = $category_info['name'] . ' - страница ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'];
                    }
                } elseif (defined('_FILTER_NAME_')) {

                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купити ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Києві, Україні | Недорогі ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): ціни';
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в інтернет-магазині меблів для дому ➤ Купити ' . mb_strtolower($category_info['name']) . ' з доставкою по Києву та Україні ✔ Кращі ціни ✔ Гарантія якості ✔ Широкий асортимент!';
                        }
                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')'));
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';
                    } else {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купить ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Киеве, Украине | Недорогие ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): цены';
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в интернет-магазине мебели для дома ➤ Купить ' . mb_strtolower($category_info['name']) . ' с доставкой по Киеву и Украине ✔ Лучшие цены ✔ Гарантия качества ✔ Широкий ассортимент!';
                        }

                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')'));
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';
                    }
                } else {

                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        $category_info['meta_title'] = 'Купити ' . mb_strtolower($category_info['name']) . ' в Києві, Україні | Недорогі ' . mb_strtolower($category_info['name']) . ': ціни';
                        $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' в інтернет-магазині меблів для дому ➤ Купити ' . mb_strtolower($category_info['name']) . ' з доставкою по Києву та Україні ✔ Кращі ціни ✔ Гарантія якості ✔ Широкий асортимент!';
                        $data['heading_title'] = !empty($filter_metah1) ? $filter_metah1 : $category_info['name'];
                        $data['heading_breadcrumb'] = $category_info['name'];

                    } else {
                        $category_info['meta_title'] = 'Купить ' . mb_strtolower($category_info['name']) . ' в Киеве, Украине | Недорогие ' . mb_strtolower($category_info['name']) . ': цены';
                        $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' в интернет-магазине мебели для дома ➤ Купить ' . mb_strtolower($category_info['name']) . ' с доставкой по Киеву и Украине ✔ Лучшие цены ✔ Гарантия качества ✔ Широкий ассортимент!';
                        $data['heading_title'] = !empty($filter_metah1) ? $filter_metah1 : $category_info['name'];
                        $data['heading_breadcrumb'] = $category_info['name'];
                    }
                }

            }    //end

            if(count($mfp_index)==1 && isset($mfp_index[1]) && $mfp_index[1]<3 || defined('_FILTER_NAME_'))
            {
                if ($this->language->get('code') == 'ru'){
                    $data['description_meta'] = "Успейте заказать ".$category_info['name']." по самым выгодным ценам в Киеве, закажите ".$category_info['name']." прямо сейчас на сайте интернет магазина мебели feshmebel.com.ua✅";
                }elseif($this->language->get('code') == 'uk'){
                    $data['description_meta'] = "Встигніть замовити ". $category_info['name']." за найвигіднішими цінами в Києві, замовте ". $category_info ['name']." прямо зараз на сайті інтернет магазину меблів feshmebel.com.ua✅";
                }
            }
            else
            {

                $category_info_parent =  $category_info['name'];
                if($category_info['parent_id']>0)
                {
                    $category_info_parent = $this->model_catalog_category->getCategory($category_info['parent_id']);
                    $category_info_parent = $category_info_parent['name'];
                }
                if ($this->language->get('code') == 'ru'){
                    $data['description_meta'] = "Лучшее предложение✅ на ". $category_info['name']." по самой выгодной цене в интернет магазине мебели feshmebel.com.ua🚀, купите ". $category_info_parent." прямо сейчас.";
                }elseif($this->language->get('code') == 'uk'){
                    $data['description_meta'] = "Краща пропозиція✅ на ". $category_info ['name']." по самій вигідною ціною в інтернет магазині меблів feshmebel.com.ua🚀, купіть ". $category_info_parent." прямо зараз.";
                }

            }

            $this->document->setTitle($category_info['meta_title']);
            $this->document->setDescription($category_info['meta_description']);
            $this->document->setKeywords($category_info['meta_keyword']);
            $data['category_id'] = $category_info['category_id'];

            $data['text_refine'] = $this->language->get('text_refine');
            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_quantity'] = $this->language->get('text_quantity');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_price'] = $this->language->get('text_price');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_select'] = $this->language->get('text_select');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_filter_amp'] = $this->language->get('button_filter_amp');
            $data['button_expect'] = $this->language->get('button_expect');
            $data['button_out_of_sale'] = $this->language->get('button_out_of_sale');
            $data['button_toorder1'] = $this->language->get('button_toorder1');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_list'] = $this->language->get('button_list');
            $data['button_grid'] = $this->language->get('button_grid');
            $data['prod_interest'] = $this->language->get('prod_interest');

            // Set the last category breadcrumb
            $data['breadcrumbs'][] = array(
                'text' => $category_info['name'],
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
            );


            if ($this->request->get['mfp'] && _FILTER_ID_ && _FILTER_GROUP_ID_) {
                $data['breadcrumbs'][] = array(
                    'text' => $data['heading_breadcrumb'],
                    'href' => $this->request->request['_route_']
                );
            }

            if ($category_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
            } else {
                $data['thumb'] = '';
            }

            if(isset($filterDescriptions["meta_title"]))

            {
                $filter_description = html_entity_decode($filterDescriptions["description"], ENT_QUOTES, 'UTF-8');


                preg_match_all('/\<img(.*?)>/', $filter_description, $matches);
                foreach ($matches[0] as $val)
                    $filter_description = str_replace($val,'',$filter_description);

                preg_match_all("/<table.*?>.*?<\/[\s]*table>/s", $filter_description, $matches);

                foreach ($matches[0] as $val)
                    $filter_description = str_replace($val,'',$filter_description);

               preg_match_all("/<div.*?>.*?<\/[\s]*div>/s", $filter_description, $matches);

                foreach ($matches[0] as $val)
                    $filter_description = str_replace($val,'',$filter_description);

                preg_match_all('/(\w+)="(.*?)"/', $filter_description, $matches);
                foreach ($matches[0] as $val)
                    $filter_description = str_replace($val,'',$filter_description);

            }


            if (isset($filter_description) && !empty($filter_description) && $filter_description != '') {
                $data['description'] = html_entity_decode($filter_description, ENT_QUOTES, 'UTF-8');
                $data['filter_description'] = true;

            } else {
                $data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
                $data['filter_description'] = false;
            }

            $data['compare'] = $this->url->link('product/compare');

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }




            $data['products'] = array();
            $data['products_id'] = [];


            $filter_data = array(
                'filter_category_id' => $category_id,
                'filter_filter' => $filter,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit,

            );

            $product_total = $this->model_catalog_product->getTotalProducts($filter_data);

            $data['product_total'] = $product_total;

            $results = $this->model_catalog_product->getProducts($filter_data);


            $this->load->model('localisation/sale_status');

            $data['sale_statuses'] = $this->model_localisation_sale_status->getSaleStatuses();

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 256,256);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png',  256,256);
                }
                $supplier_id = $result['supplier_id'];

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    if ($supplier_id > 0) {
                        $result['price'] =  ceil($result['price']* $suppliers[$supplier_id]['coefficient']);
                    };
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    if ($supplier_id > 0) {
                        $result['special'] =  ceil($result['special']* $suppliers[$supplier_id]['coefficient']);
                    };
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                $this->load->model('catalog/product');

                if ($special)
                    $this->model_catalog_product->setPriceCurrent($result['product_id'], $special);
                else
                    $this->model_catalog_product->setPriceCurrent($result['product_id'], $price);

                if (!$result['price_show'])
                    $price = '';


                $images = array();

                $result_images = $this->model_catalog_product->getProductImages($result['product_id']);

                foreach ($result_images as $result_image) {
                    $images[] = array(
                         'thumb' => $this->model_tool_image->resize($result_image['image'], 256, 256)
                    );
                }

                $options_p = array();
                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                    $product_option_value_data_p = array();
                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {

                                $price_opt = $this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                                $supplier_id = $result['supplier_id'];
                                if ($suppliers[$supplier_id] > 0)
                                    $price_opt =ceil($price_opt*$suppliers[$supplier_id]['coefficient']);
                                $price_opt = $this->currency->format($price_opt);

                            } else {
                                $price_opt = false;
                            }
                            if(!empty($option_value['image_prod']))
                                $images[] = array(
                                    'thumb' => $this->model_tool_image->resize($option_value['image_prod'], 256, 256)
                                );
                            //$_special_p = ceil($special_p).' грн.';
                            $product_option_value_data_p[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                //'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'image' => $this->model_tool_image->resize($option_value['image'], 32, 32),
                                'quantity' => $option_value['quantity'],
                                'subtract' => $option_value['subtract'],
                                'price' => $price_opt,
                                'price_prefix' => $option_value['price_prefix'],
                                'weight' => $option_value['weight'],
                                'image_prod' => $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
                                'supplier_id' => $suppliers[$supplier_id],
                                'weight_prefix' => $option_value['weight_prefix']
                            );
                        }
                    }
                    $options_p[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data_p,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required'],
                        'sort_option' => $option['sort_option']
                    );
                }

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'name' => $result['name'].' - '.$result['sku'],
                    'sale_status_name' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['name']:'',
                    'sale_status_color' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['color']:'',
                    'sale_status_image' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$result['sale_status_id']]['image']:'',
                    'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                    'privat' => $result['product_pp'] || $result['product_ii']? 1 : 0,
                    'thumb' => $image,
                    'images' => $images,
                    'price' => $price,
                    'old_price' => $result['old_price'],
                    'rating' => $result['rating'],
                    'special' => $special,
                    'options' => $options_p,
                    'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
                );
            }


            $url = '';
            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }
            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }
            $data['sorts'] = array();
            $data['order_butt_before'] = $this->language->get('text_default');

            $data['sorts'][] = array(
                'text' => $this->language->get('text_default'),
                'value' => 'p.sort_order-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
            );


            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
            );

            if ($this->config->get('config_review_status')) {
                $data['sorts'][] = array(
                    'text' => $this->language->get('text_rating_desc'),
                    'value' => 'rating-DESC',
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
                );

                $data['sorts'][] = array(
                    'text' => $this->language->get('text_rating_asc'),
                    'value' => 'rating-ASC',
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
                );
            }

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
            if ($page == 1) {
            } elseif ($page == 2) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL') . _FILTER_LANGUAGE_ . '/', 'prev');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL')), 'prev');
                }
            } else {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink(str_replace('?page=', _FILTER_LANGUAGE_ . '/page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page - 1), 'SSL')) . '/', 'prev');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page - 1), 'SSL')) . '/', 'prev');
                }
            }
            if ($page == 1) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL') . _FILTER_LANGUAGE_ . '/', 'canonical');
                } else {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'canonical');
                }
            } elseif ($page > 1) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink(str_replace('?page=', _FILTER_LANGUAGE_ . '/page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . $page, 'SSL')) . '/', 'canonical');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . $page, 'SSL')) . '/', 'canonical');
                }
            }
            if ($limit && ceil($product_total / $limit) > $page) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink(str_replace('?page=', _FILTER_LANGUAGE_ . '/page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page + 1), 'SSL')) . '/', 'next');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page + 1), 'SSL')) . '/', 'next');
                }
            }

            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;

            $data['continue'] = $this->url->link('common/home');


            $data['curr_lang'] = $this->language->get('code');

            if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                $data['title_meta'] = $filter_metatitle;
            } else {
                if (defined('TITLE') and TITLE != '') {
                    $data['title_meta'] = TITLE;
                } else {
                    $data['title_meta'] = $this->document->getTitle();
                }
            }

            if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                $data['description_meta'] = $filter_metadescr;
            } else {
                if (defined('TITLE') and TITLE != '') {
                    $data['description_meta'] = DESCRIPTION;
                } else {
                    $data['description_meta'] = $this->document->getDescription();
                }// end
            }


            $data['categories_sub'] = array();
            $results = $this->model_catalog_category->getCategories($category_id);
            foreach ($results as $result) {


                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 256, 256);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 290, 160);
                }

                $data['categories_sub'][] = array(
                    'name' => $result['name'] ,
                    'thumb' => $image,
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
                );
            }

            $categories = $this->model_catalog_category->getCategories(0);

            foreach ($categories as $category) {
                if ($category['top']) {
                    // Level 2
                    $children_data = array();

                    $children = $this->model_catalog_category->getCategories($category['category_id']);

                    foreach ($children as $child) {
                        $filter_data = array(
                            'filter_category_id' => $child['category_id'],
                            'filter_sub_category' => true
                        );

                        $children_data[] = array(
                            'name' => $child['name'],
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                        );
                    }

                    // Level 1
                    $data['categories'][] = array(
                        'id' => $category['category_id'],
                        'name' => $category['name'],
                        'children' => $children_data,
                        'column' => $category['column'] ? $category['column'] : 1,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );
                }
            }

            if ($this->request->server['HTTPS']) {
                $server = $this->config->get('config_ssl');
            } else {
                $server = $this->config->get('config_url');
            }

            if ($this->language->get('code') == 'ru') {
                if ($this->request->get['mfp'] && _FILTER_ID_ && _FILTER_GROUP_ID_) {
                    $data['alternate'] .= '
				<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . _CURRPAGE_LANG1_URL_) . '" hreflang="ru-ua" />
				<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . 'ua/' . _CURRPAGE_LANG2_URL_) . '" hreflang="uk-ua" />';
                } else {
                    if (strpos($_SERVER['REQUEST_URI'], 'page')) {
                        $data['alternate'] .= '
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . substr($_SERVER['REQUEST_URI'], 1)) . '/" hreflang="ru-ua" />
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . 'ua' . $_SERVER['REQUEST_URI']) . '/" hreflang="uk-ua" />';
                    } else {
                        $data['alternate'] .= '
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . substr($_SERVER['REQUEST_URI'], 1)) . '" hreflang="ru-ua" />
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . 'ua' . $_SERVER['REQUEST_URI']) . '" hreflang="uk-ua" />';
                    }

                }
            } else {
                if ($this->request->get['mfp'] && _FILTER_ID_ && _FILTER_GROUP_ID_) {
                    $data['alternate'] .= '
				<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . str_replace('ua/', '', _CURRPAGE_LANG1_URL_)) . '" hreflang="ru-ua" />
				<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . _CURRPAGE_LANG2_URL_) . '" hreflang="uk-ua" />';
                } else {
                    if (strpos($_SERVER['REQUEST_URI'], 'page')) {
                        $data['alternate'] .= '
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . str_replace('/ua/', '', $_SERVER['REQUEST_URI'])) . '/" hreflang="ru-ua" />
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . substr($_SERVER['REQUEST_URI'], 1)) . '/" hreflang="uk-ua" />';
                    } else {
                        $data['alternate'] .= '
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . str_replace('/ua/', '', $_SERVER['REQUEST_URI'])) . '" hreflang="ru-ua" />
						<link rel="alternate" href="' . str_replace('?page=', 'page-', $server . substr($_SERVER['REQUEST_URI'], 1)) . '" hreflang="uk-ua" />';
                    }

                }
            }

            $data['column_left'] = $this->load->controller('common/column_left');

            $data['links'] = $this->document->getLinks();

            $this->load->language('common/header');
            $data['text_home'] = $this->language->get('text_home');
            $data['text_wishlist'] = $this->language->get('text_wishlist');
            $data['text_account'] = $this->language->get('text_account');
            $data['text_register'] = $this->language->get('text_register');
            $data['text_login'] = $this->language->get('text_login');
            $data['text_order'] = $this->language->get('text_order');
            $data['text_transaction'] = $this->language->get('text_transaction');
            $data['text_download'] = $this->language->get('text_download');
            $data['text_logout'] = $this->language->get('text_logout');
            $data['text_checkout'] = $this->language->get('text_checkout');
            $data['text_category'] = $this->language->get('text_category');
            $data['text_all'] = $this->language->get('text_all');
            $data['text_compare'] = $this->language->get('text_compare');

            $data['home'] = $this->url->link('common/home');
            $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
            $data['compare'] = $this->url->link('product/compare');
            $data['logged'] = $this->customer->isLogged();
            $data['account'] = $this->url->link('account/account', '', 'SSL');
            $data['register'] = $this->url->link('account/register', '', 'SSL');
            $data['login'] = $this->url->link('account/login', '', 'SSL');
            $data['order'] = $this->url->link('account/order', '', 'SSL');
            $data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
            $data['download'] = $this->url->link('account/download', '', 'SSL');
            $data['logout'] = $this->url->link('account/logout', '', 'SSL');
            $data['shopping_cart'] = $this->url->link('checkout/cart');
            $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
            $data['contact'] = $this->url->link('information/contact');
            $data['telephone'] = $this->config->get('config_telephone');

            $this->load->language('common/footer');


            $data['footer_schedule'] = $this->language->get('footer_schedule');
            $data['footer_menu_about'] = $this->language->get('footer_menu_about');
            $data['footer_menu_delivery'] = $this->language->get('footer_menu_delivery');
            $data['footer_menu_guaranty'] = $this->language->get('footer_menu_guaranty');
            $data['footer_menu_exchange'] = $this->language->get('footer_menu_exchange');
            $data['footer_menu_discount'] = $this->language->get('footer_menu_discount');
            $data['footer_menu_articles'] = $this->language->get('footer_menu_articles');
            $data['footer_menu_help'] = $this->language->get('footer_menu_help');
            $data['footer_menu_contacts'] = $this->language->get('footer_menu_contacts');



            $data['url_menu_delivery'] = $this->url->link('information/information&information_id=4');
            $data['url_menu_guaranty'] = $this->url->link('information/information&information_id=9');
            $data['url_menu_exchange'] = $this->url->link('account/return/add');
            $data['url_menu_discount'] = $this->url->link('information/information&information_id=11');
            $data['url_menu_help'] = $this->url->link('information/information&information_id=8');
            $data['url_menu_about'] = $this->url->link('information/information&information_id=10');
            $data['url_menu_articles'] = $this->url->link('newsblog/category&newsblog_category_id=1');
            $data['url_menu_contacts'] = $this->url->link('information/contact');
            $data['url_menu_sitemap'] = $this->url->link('information/sitemap');

            $data['language'] = $this->load->controller('common/language');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category_amp.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category_amp.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/product/category_amp.tpl', $data));
            }
        } else {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/category', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $data['waiting'] = array();
            if (count($data['products']) > 0)
                foreach ($data['products'] as $i => $product) {
                    if (strpos($product['stock_status'], 'жида') !== false) {
                        $data['products'][] = $product;
                        $data['waiting'][] = $product;
                        unset($data['products'][$i]);
                    }
                }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }

    }
}
