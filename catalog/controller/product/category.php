<?php

class ControllerProductCategory extends Controller
{
    public function FAQPage($filter_data)
    {

        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        $this->load->model('catalog/supplier');
		$this->load->model('catalog/review');

        $this->load->model('tool/image');

        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach ($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;


        $results = $this->model_catalog_product->getProducts($filter_data);
        $products = array();
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                //$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                $price = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                if ($suppliers[$result['supplier_id']] > 0)
                    $price =ceil($price*$suppliers[$result['supplier_id']]['coefficient']);
                $price_num = $price;
                $price = $this->currency->format($price);
            } else {
                $price = false;
            }

            if ((float)$result['special']) {
                //$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                $special = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
                //if ($suppliers[$result['supplier_id']] > 0)
                //    $special =ceil($special*$suppliers[$result['supplier_id']]['coefficient']);
                $special_num = $special;
                $special = $this->currency->format($special);
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$result['rating'];
            } else {
                $rating = false;
            }
            $json = array();


            if (isset($this->request->post['product_id'])) {
                $product_id = $this->request->post['product_id'];
            } else {
                $product_id = 0;
            }


            $this->load->model('catalog/product');


            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info && false) {
                $options = array();
                foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                    $product_option_value_data = array();
                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                //$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                $price_opt = $this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                                $supplier_id = $product_info['supplier_id'];
                                if ($suppliers[$supplier_id] > 0)
                                    $price_opt =ceil($price_opt*$suppliers[$supplier_id]['coefficient']);
                                $price_opt = $this->currency->format($price_opt);
                            } else {
                                $price_opt = false;
                            }

                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price_opt,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }
                    $options[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required'],
                        'sort_option' => $option['sort_option']
                    );
                    $options['product_id'] = $product_info['product_id'];
                    $options['name'] = $product_info['name'];
                }
                if (!$json) {
                    $json = $options;
                }
            }

            $options_p = array();
            foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                $product_option_value_data_p = array();
                foreach ($option['product_option_value'] as $option_value) {
                    if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {

                            $price_opt = $this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                            $supplier_id = $result['supplier_id'];
                            if ($suppliers[$supplier_id] > 0)
                                $price_opt =ceil($price_opt*$suppliers[$supplier_id]['coefficient']);
                            $price_opt = $this->currency->format($price_opt);

                        } else {
                            $price_opt = false;
                        }


                        //$_special_p = ceil($special_p).' грн.';
                        $product_option_value_data_p[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            //'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'image' => $this->model_tool_image->resize($option_value['image'], 32, 32),
                            'quantity' => $option_value['quantity'],
                            'subtract' => $option_value['subtract'],
                            'price' => $price_opt,
                            'price_prefix' => $option_value['price_prefix'],
                            'weight' => $option_value['weight'],
                            'image_prod' => $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
                            'supplier_id' => $suppliers[$supplier_id],
                            'weight_prefix' => $option_value['weight_prefix']
                        );
                        if(isset( $data['option_value_filter'][$option_value['option_value_id']]) && !empty($option_value['image_prod']))
                            $image = $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));


                    }
                }
                $options_p[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data_p,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required'],
                    'sort_option' => $option['sort_option']
                );
            }



            $special_info_tmp = $this->model_catalog_product->getProductSpecial($result['product_id']);

            if(!empty($special_info_tmp)){
                if(strtotime($special_info_tmp['date_end']) - time()>0)
                    $result['sale_status_id']=1;
                else
                    $result['sale_status_id']=0;
            }



            if($special_num)
                $this->model_catalog_product->setPriceCurrent( $result['product_id'],$special_num);
            else
                $this->model_catalog_product->setPriceCurrent( $result['product_id'],$price_num);

            if (!$result['price_show'])
                $price = '';
				
				
				 $reviews_item = array();
            $reviews = $this->model_catalog_review->getReviewsByProductId($result['product_id']);

            foreach ($reviews as $review) if((int)$result['rating']>0){
                $reviews_item[] = array(
                    'author'     => $review['author'],
                    'text'       => str_replace('"','\"',nl2br($review['text'])),
                    'rating'     => (int)$review['rating'],
                    'date_added' => date('Y-m-d', strtotime($review['date_added']))
                );
                $data['reviews_rating']+=(int)$result['rating'];
            }

            $products[] = array(
                'product_id' => $result['product_id'],
                'name' => $result['name'].' - '.$result['sku'],
                'thumb' => $image,
                'reviews_item' => $reviews_item,
                'sku' => (empty($result['sku'])) ? '' : $this->language->get('text_sku') . ' ' . $result['sku'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'price' => $price,
                'old_price' => $result['old_price'],
                'youtube' => $result['youtube'],
                'sale_status_name' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['name']:'',
                'sale_status_color' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['color']:'',
                'sale_status_image' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$result['sale_status_id']]['image']:'',
                'price_num' => $price_num,
                'special' => $special,

                'special_end' => $special_end,

                'stock_status' => $result['stock_status'],
                'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                'tax' => $tax,
                'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating' => $result['rating'],
                'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id']),
                //'options'   => $this->model_catalog_product->getProductOptions($result['product_id']),
                'options' => $options_p,
                'bestseller' => $result['upc'],
                'recommended' => $result['ean'],
                'privat' => $result['product_pp'] || $result['product_ii']? 1 : 0,
            );
        }
        return $products;
    }
    public function index()
    {
        $this->load->language('newsblog/category');

        $data['blog_title'] = $this->language->get('blog_title');
        $data['text_empty'] = $this->language->get('text_empty');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['text_refine'] = $this->language->get('text_refine');
        $data['text_more'] = $this->language->get('text_more');
        $data['text_attributes'] = $this->language->get('text_attributes');

        $this->load->model('newsblog/article');
        $this->load->model('tool/image');
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "newsblog_article WHERE page = '" . $_SERVER['REQUEST_URI'] . "'");

        $date_format = $this->language->get('date_format_short');

        $data['articles'] = array();

        foreach ($query->rows as $article) {
            $result = $this->model_newsblog_article->getArticle($article['article_id']);

            if ($result['image']) {
                $original 	= HTTP_SERVER.'image/'.$result['image'];
                $thumb 		= $this->model_tool_image->resize($result['image'], 600, 600);
            } else {
                $original 	= '';
                $thumb 		= '';	//or use 'placeholder.png' if you need
            }

            if($this->config->get('config_language_id') == 1){
                $_monthsList = array(
                    '.01.' => 'января',
                    '.02.' => 'февраля',
                    '.03.' => 'марта',
                    '.04.' => 'апреля',
                    '.05.' => 'мая',
                    '.06.' => 'июня',
                    '.07.' => 'июля',
                    '.08.' => 'августа',
                    '.09.' => 'сентября',
                    '.10.' => 'октября',
                    '.11.' => 'ноября',
                    '.12.' => 'декабря'
                );
            }else{
                $_monthsList = array(
                    '.01.' => 'січня',
                    '.02.' => 'лютого',
                    '.03.' => 'березня',
                    '.04.' => 'квітня',
                    '.05.' => 'травня',
                    '.06.' => 'червня',
                    '.07.' => 'липня',
                    '.08.' => 'серпня',
                    '.09.' => 'вересня',
                    '.10.' => 'жовтня',
                    '.11.' => 'листопада',
                    '.12.' => 'грудня'
                );
            }
            $currentD = date('d', strtotime($result['date_available']));
            $currentM = date('.m.', strtotime($result['date_available']));
            $currentY = date('Y', strtotime($result['date_available']));

            $currentDate = $currentD . ' ' . $_monthsList[$currentM] . ' ' . $currentY;

            $data['articles'][] = array(
                'article_id'  		=> $result['article_id'],
                'original'			=> $original,
                'thumb'       		=> $thumb,
                'name'        		=> $result['name'],
                'preview'     		=> html_entity_decode($result['preview'], ENT_QUOTES, 'UTF-8'),
                'attributes'  		=> $result['attributes'],
                'href'        		=> $this->url->link('newsblog/article', 'newsblog_path=' . $this->request->get['newsblog_path'] . '&newsblog_article_id=' . $result['article_id']),
                'date'		  		=> ($date_format ? date($date_format, strtotime($result['date_available'])) : false),
                'date_modified'		=> ($date_format ? date($date_format, strtotime($result['date_modified'])) : false),
                'currentDate'		=> $currentDate,
                'viewed' 			=> $result['viewed']
            );
        }

        $this->load->model('catalog/supplier');
        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach ($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;

        $this->load->language('product/category');
        $this->load->model('catalog/category');
        $data['btn_open_text'] = $this->language->get('btn_open_text');
        $this->load->model('catalog/product');



        $this->session->data['url_refferer'] = $this->request->server['REQUEST_URI'];

        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'p.sort_order';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_product_limit');
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['path'])) {
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = (int)$path_id;
                } else {
                    $path .= '_' . (int)$path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        //'href' => $this->url->link('product/category', 'path=' . $path . $url)
                        'href' => $this->url->link('product/category', 'path=' . $path)
                    );
                }
            }
        } else {
            $category_id = 0;
        }

        $category_info = $this->model_catalog_category->getCategory($category_id);
        $category_info['name_real'] = $category_info['name'];

        $data['breadcrumbs'][] = array(
            'text' => $category_info['name'],
            'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
        );

        /*
        if ($category_info['category_id'] == 41 || $category_info['category_id'] == 42) {
            $url_categ_h1 = str_replace("http://" . $_SERVER['SERVER_NAME'], "", $this->url->link('product/category', 'path=' . $this->request->get['path']));
            $parent_categ_h1 = '';
            if (is_readable($_SERVER ['DOCUMENT_ROOT'] . "/meta.csv")) {
                $handleurl_categ_h1 = fopen($_SERVER ['DOCUMENT_ROOT'] . "/meta.csv", "r");
                while (!feof($handleurl_categ_h1)) {
                    $buffer_url_categ_h1 = fgets($handleurl_categ_h1, 9999);
                    $data_url_categ_h1 = explode(";", $buffer_url_categ_h1);
                    if ($data_url_categ_h1[0] == $url_categ_h1) {
                        $parent_categ_h1 = $data_url_categ_h1[3];
                    }
                }
                fclose($handleurl_categ_h1);
            }
            if ($parent_categ_h1) {
                $category_info['name'] = $parent_categ_h1;
            }
        }
*/

        if ($category_info) {

            $mfp_index = array();
            if(isset($_GET['mfp']))
            {
                preg_match_all('/\[(.*?)\]/',$_GET['mfp'],$matches);
                foreach ($matches[1] as $item)
                {

                    $item=explode(',',$item);
                    if(!isset($mfp_index[count($item)]))
                        $mfp_index[count($item)]=0;
                    $mfp_index[count($item)]++;
                }

            }

            if(count($mfp_index)==1 && isset($mfp_index[1]) && $mfp_index[1]<3)
            {


                $mfp_attr = array();
                $mfp_tmp = explode(']',$_GET['mfp']);
                foreach ($mfp_tmp as $key=>$item)
                {
                    $item = explode('[',$item);
                    $item = explode(',',$item[0]);
                    $item =  explode('f-',$item[count($item)-1]);
                    if(!empty($item[0]))
                        $mfp_attr[]=$item[0];

                }

                if(count($mfp_attr)==2)
                {
                    $filters_group = $this->db->query("SELECT * FROM `oc_filter_group_description` AS fgd LEFT JOIN `oc_filter_group` as fg ON fg.filter_group_id = fgd.filter_group_id WHERE fgd.language_id = '".(int)$this->config->get('config_language_id')."' ORDER BY fg.sort_order ")->rows;
                    $filters_group_tmp = array();
                    foreach ($filters_group as $k=>$filter_group)
                    {
                        $filter_group['sort_order_i'] = $k;
                        $filters_group_tmp[$filter_group['filter_group_id']] = $filter_group;
                    }

                    $filters = $this->db->query("SELECT * FROM `oc_filter_description` AS fd LEFT JOIN `oc_filter` as f ON f.filter_id = fd.filter_id WHERE fd.language_id = '".(int)$this->config->get('config_language_id')."' ORDER BY f.sort_order ")->rows;
                    $filters_tmp = array();
                    foreach ($filters as $k=>$filter_sub)
                    {
                        $filter_sub['sort_order_i'] = $k;
                        $filters_tmp[$filter_sub['filter_id']] = $filter_sub;
                    }


                    if($filters_group_tmp[$mfp_attr[0]]['sort_order_i']>$filters_group_tmp[$mfp_attr[1]]['sort_order_i'])
                    {

                        $filter_id = $matches[1][0];
                        $filter_id_sub = $matches[1][1];
                        $filter2 = $filters_tmp[$filter_id];
                        $filter_sub = $filters_tmp[$filter_id_sub];
                        $url = 'https://feshmebel.com.ua'.strtok($_SERVER['REQUEST_URI'],'?').'?mfp='.$filter_sub['filter_group_id'].'f-'.$filters_group_tmp[$filter_sub['filter_group_id']]['filter_group_url'].'['.$filter_sub['filter_id'].'],'.$filter2['filter_group_id'].'f-'.$filters_group_tmp[$filter2['filter_group_id']]['filter_group_url'].'['.$filter2['filter_id'].']';
                        header("HTTP/1.1 301 Moved Permanently");
                        header("Location: ".$url);
                        exit();
                    }

                }

                $filter_group_description_tmp = array();
                $filter_group_description = $this->db->query("SELECT * FROM `oc_filter_group_description` WHERE `filter_group_id` in (".implode(',',$mfp_attr).") AND `language_id` = '" . (int)$this->config->get('config_language_id') . "'")->rows;
                foreach ($filter_group_description as $item)
                    $filter_group_description_tmp[$item['filter_group_id']] = $item['name'];
                $filter_description_tmp = array();
                $filter_description = $this->db->query("SELECT * FROM `oc_filter_description` WHERE `filter_id` in (".implode(',',$matches[1]).") AND `language_id` = '" . (int)$this->config->get('config_language_id') . "'")->rows;
                foreach ($filter_description as $item)
                    $filter_description_tmp[$item['filter_id']] = $item['name'];
                $filter_description_seo=array();
                foreach ($mfp_attr as $key=>$filter_group_id)
                {
                    $filter_description_seo[] =$filter_group_description_tmp[$filter_group_id].' - '.$filter_description_tmp[$matches[1][$key]];
                }
                $category_info['name'] = $category_info['name'].': '.implode(', ',$filter_description_seo);

            }

            // start Tkach web-promo
            if (defined('H_ONE')) {
                $data['heading_title'] = H_ONE;
            } else {
                $data['heading_title'] = $category_info['name'];
                $data['heading_breadcrumb'] = $category_info['name'];
            }

            $filterDescriptions = array();
            $filterDescriptions = $this->model_catalog_category->getFilterDescriptions();

            if (defined('TITLE') and TITLE != '') {

                $showmeta = 0;
                foreach ($filterDescriptions as $row) {
                    if ($_SERVER['REQUEST_URI'] == $row["filter_url"]) {
                        $filter_metatitle = html_entity_decode($row["meta_title"], ENT_QUOTES, 'UTF-8');
                        $filter_metadescr = html_entity_decode($row["meta_description"], ENT_QUOTES, 'UTF-8');
                        $filter_metah1 = html_entity_decode($row["meta_h1"], ENT_QUOTES, 'UTF-8');


                        $showmeta = $row["showmeta"];
                    }
                }

                if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                    $category_info['meta_title'] = $filter_metatitle;
                } else {
                    $category_info['meta_title'] = TITLE;
                }
                if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                    $category_info['meta_description'] = $filter_metadescr;
                } else {
                    $category_info['meta_description'] = DESCRIPTION;
                }

                if (!empty($filter_metah1) && $showmeta) {
                    $data['heading_title'] = $filter_metah1;
                }

            } else {
                $showmeta = 0;
                foreach ($filterDescriptions as $row) {
                    if ($_SERVER['REQUEST_URI'] == $row["filter_url"]) {
                        $filter_metatitle = html_entity_decode($row["meta_title"], ENT_QUOTES, 'UTF-8');
                        $filter_metadescr = html_entity_decode($row["meta_description"], ENT_QUOTES, 'UTF-8');
                        $filter_metah1 = html_entity_decode($row["meta_h1"], ENT_QUOTES, 'UTF-8');

                        $showmeta = $row["showmeta"];
                    }
                }


                if (defined('_FILTER_NAME_') && defined('page_for_seo') && page_for_seo > 1) {
                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купити ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Києві, Україні | Недорогі ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): ціни - сторінка ' . page_for_seo;
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в інтернет-магазині меблів для дому ➤ Купити ' . mb_strtolower($category_info['name']) . ' з доставкою по Києву та Україні ✔ Кращі ціни ✔ Гарантія якості ✔ Широкий асортимент! - сторінка ' . page_for_seo;
                        }

                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')')) . ' - сторінка ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';
                    } else {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купить ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Киеве, Украине | Недорогие ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): цены - страница ' . page_for_seo;
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в интернет-магазине мебели для дома ➤ Купить ' . mb_strtolower($category_info['name']) . ' с доставкой по Киеву и Украине ✔ Лучшие цены ✔ Гарантия качества ✔ Широкий ассортимент! - страница ' . page_for_seo;
                        }
                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')')) . ' - сторінка ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';
                    }
                } elseif (defined('page_for_seo') && page_for_seo > 1) {

                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        $category_info['meta_title'] = 'Сторінка ' . page_for_seo . ' - розділ ' . mb_strtolower($category_info['name']) . ' інтернет-магазина Фешемебельний';
                        $category_info['meta_description'] = 'Сторінка ' . page_for_seo . ' розділу ' . mb_strtolower($category_info['name']) . ' Величезний вибір меблів, швидка доставка по Києву і всій Україні від інтернет-магазину Фешемебельний  (044)38-38-38-5';
                        $data['heading_title'] = $category_info['name'] . ' - сторінка ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'];
                    } else {
                        $category_info['meta_title'] = 'Страница ' . page_for_seo . ' раздела ' . mb_strtolower($category_info['name']) . ' интернет-магазина Фешемебельный';
                        $category_info['meta_description'] = 'Страница ' . page_for_seo . ' раздела ' . mb_strtolower($category_info['name']) . ' Огромный выбор мебели, быстрая доставка по Киеву и всей Украине от интернет-магазина Фешемебельный (044)38-38-38-5';
                        $data['heading_title'] = $category_info['name'] . ' - страница ' . page_for_seo;
                        $data['heading_breadcrumb'] = $category_info['name'];
                    }
                } elseif (defined('_FILTER_NAME_')) {

                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купити ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Києві, Україні | Недорогі ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): ціни';
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в інтернет-магазині меблів для дому ➤ Купити ' . mb_strtolower($category_info['name']) . ' з доставкою по Києву та Україні ✔ Кращі ціни ✔ Гарантія якості ✔ Широкий асортимент!';
                        }
                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')'));
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';
                    } else {
                        if (isset($filter_metatitle) && $filter_metatitle != '' && $showmeta) {
                            $category_info['meta_title'] = $filter_metatitle;
                        } else {
                            $category_info['meta_title'] = 'Купить ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в Киеве, Украине | Недорогие ' . mb_strtolower($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . '): цены';
                        }
                        if (isset($filter_metadescr) && $filter_metadescr != '' && $showmeta) {
                            $category_info['meta_description'] = $filter_metadescr;
                        } else {
                            $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' (' . mb_strtolower(_FILTER_NAME_) . ') в интернет-магазине мебели для дома ➤ Купить ' . mb_strtolower($category_info['name']) . ' с доставкой по Киеву и Украине ✔ Лучшие цены ✔ Гарантия качества ✔ Широкий ассортимент!';
                        }

                        $data['heading_title'] = (!empty($filter_metah1) ? $filter_metah1 : ($category_info['name'] . ' (' . _FILTER_NAME_ . ')'));
                        $data['heading_breadcrumb'] = $category_info['name'] . ' (' . _FILTER_NAME_ . ')';




                    }
                } else {

                    if (substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/') {
                        $category_info['meta_title'] = 'Купити ' . mb_strtolower($category_info['name']) . ' в Києві, Україні | Недорогі ' . mb_strtolower($category_info['name']) . ': ціни';
                        $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' в інтернет-магазині меблів для дому ➤ Купити ' . mb_strtolower($category_info['name']) . ' з доставкою по Києву та Україні ✔ Кращі ціни ✔ Гарантія якості ✔ Широкий асортимент!';
                        $data['heading_title'] = !empty($filter_metah1) ? $filter_metah1 : $category_info['name'];
                        $data['heading_breadcrumb'] = $category_info['name'];

                    } else {
                        $category_info['meta_title'] = 'Купить ' . mb_strtolower($category_info['name']) . ' в Киеве, Украине | Недорогие ' . mb_strtolower($category_info['name']) . ': цены';
                        $category_info['meta_description'] = '➥ ' . ucfirst($category_info['name']) . ' в интернет-магазине мебели для дома ➤ Купить ' . mb_strtolower($category_info['name']) . ' с доставкой по Киеву и Украине ✔ Лучшие цены ✔ Гарантия качества ✔ Широкий ассортимент!';
                        $data['heading_title'] = !empty($filter_metah1) ? $filter_metah1 : $category_info['name'];
                        $data['heading_breadcrumb'] = $category_info['name'];
                    }
                }
                if (isset($this->request->get['mfp']))
                {
                    $row = $this->db->query("SELECT * FROM `text_seo` WHERE `url` = '" . $_SERVER['REQUEST_URI'] . "'")->row;
                    if(isset($row['h1'])&&!empty($row['h1']))
                        $data['heading_title'] = $row['h1'];
                    if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                    {
                        $category_info['meta_title'] = $data['heading_title'].' купити в Києві та Україні, найкращі ціни на '.$data['heading_title'].' в інтернет магазині стільців, столів, диванів та інших меблів';

                    }
                    else
                    {
                        $category_info['meta_title'] = $data['heading_title'].' - купить в Киеве по лучшей цене, заказать с доставкой по Украине '.$data['heading_title'].', диваны и другую мебель на сайте столов и стульев feshmebel.com.ua';
                        $category_info['meta_description'] = 'Выгодное предложение на '.$category_info['name'].' ✅, переходи на сайт ➡️ feshmebel.com.ua и заказывай '.$data['heading_title'].' по лучшей цене 🚀';

                    }
                }

            }    //end

            if(count($mfp_index)==1 && isset($mfp_index[1]) && $mfp_index[1]<3 || defined('_FILTER_NAME_'))
            {
                if(defined('_FILTER_NAME_'))
                {
                   // $category_info['name'] =  $data['heading_breadcrumb'];
                }

                if ($this->language->get('code') == 'ru'){
                  //  $category_info['meta_description'] = "Успейте заказать ".$category_info['name']." по самым выгодным ценам в Киеве, закажите ".$category_info['name']." прямо сейчас на сайте интернет магазина мебели feshmebel.com.ua✅";
                }elseif($this->language->get('code') == 'uk'){
                    $category_info['name'] =  $data['heading_breadcrumb'];
                   $category_info['meta_description'] = "Встигніть замовити ". $category_info['name']." за найвигіднішими цінами в Києві, замовте ". $category_info ['name']." прямо зараз на сайті інтернет магазину меблів feshmebel.com.ua✅";
                }
                if(!defined('_FILTER_NAME_'))
                {
                    if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                    {

                    }
                    else
                    {
                        $category_info['meta_title'] = $data['heading_title'].' - купить в Киеве по лучшей цене, заказать с доставкой по Украине '.$data['heading_title'].', кресла, столы и стулья на сайте Фешемебельный';
                        $category_info['meta_description'] = $data['heading_title'].' - заказывай с доставкой по Украине 🚚, заходи и выбирай ➡️'.$category_info['name_real'].' по лучшей цене 🚀 в каталоге мебели интернет магазина Фешемебельный';
                    }
                    $data['breadcrumbs'][] = array(
                        'text' => $data['heading_title']
                    );
                }

                
            }
            else
            {

                $category_info_parent =  $category_info['name'];
                if($category_info['parent_id']>0)
                {
                    $category_info_parent = $this->model_catalog_category->getCategory($category_info['parent_id']);
                    $category_info_parent = $category_info_parent['name'];
                }
                if ($this->language->get('code') == 'ru'){
                    $category_info['meta_description'] = "Лучшее предложение✅ на ". $category_info['name']." по самой выгодной цене в интернет магазине мебели feshmebel.com.ua🚀, купите ". $category_info_parent." прямо сейчас.";
                }elseif($this->language->get('code') == 'uk'){
                    $category_info['meta_description'] = "Краща пропозиція✅ на ". $category_info ['name']." по самій вигідною ціною в інтернет магазині меблів feshmebel.com.ua🚀, купіть ". $category_info_parent." прямо зараз.";
                }

            }
            if(!isset($this->request->get['mfp']))
            {
                if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' ){
                    $category_info['meta_title'] = $category_info['name'].' купити в Києві та Україні, замовити '.$category_info['name'].' по найкращим цінам в інтернет магазині крісел, діванів та інших меблів';

                }
            }



            $this->document->setTitle($category_info['meta_title']);
            $this->document->setDescription($category_info['meta_description']);
            $this->document->setKeywords($category_info['meta_keyword']);
            $data['category_id'] = $category_info['category_id'];

            $data['text_refine'] = $this->language->get('text_refine');
            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_quantity'] = $this->language->get('text_quantity');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_price'] = $this->language->get('text_price');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_select'] = $this->language->get('text_select');


            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_expect'] = $this->language->get('button_expect');
            $data['button_out_of_sale'] = $this->language->get('button_out_of_sale');
            $data['button_toorder1'] = $this->language->get('button_toorder1');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_list'] = $this->language->get('button_list');
            $data['button_grid'] = $this->language->get('button_grid');
            $data['prod_interest'] = $this->language->get('prod_interest');

            // Set the last category breadcrumb



            if ($this->request->get['mfp'] && _FILTER_ID_ && _FILTER_GROUP_ID_) {
                $data['breadcrumbs'][] = array(
                    'text' => $data['heading_breadcrumb'],
                    'href' => $this->request->request['_route_']
                );
            }

            if ($category_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
            } else {
                $data['thumb'] = '';
            }

            if ($category_info['image_banner']) {
                $data['thumb_banner'] = $this->model_tool_image->resize($category_info['image_banner'], 1250, 275);
            } else {
                $data['thumb_banner'] = '';
            }

            $filterDescriptions = array();
            $filterDescriptions = $this->model_catalog_category->getFilterDescriptions();

            foreach ($filterDescriptions as $row) {
                if ($_SERVER['REQUEST_URI'] == $row["filter_url"]) {
                    $filter_description = html_entity_decode($row["description"], ENT_QUOTES, 'UTF-8');
                }
            }

            if (isset($filter_description) && !empty($filter_description) && $filter_description != '') {
                $data['description'] = html_entity_decode($filter_description, ENT_QUOTES, 'UTF-8');
                $data['filter_description'] = true;
            } else {
                $data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
                $data['filter_description'] = false;
            }



            $data['compare'] = $this->url->link('product/compare');

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            // start Tkach web-promo
            // no need categories in filter page
            if (!defined('_FILTER_NAME_') and !isset($this->request->get['mfp'])) {


                $data['categories'] = array();

                if ($category_id == 88) {
                    $results = $this->model_catalog_category->getCategoriesById(array(42, 26, 27));
                } elseif ($category_id == 87) {
                    $results = $this->model_catalog_category->getCategoriesById(array(21, 41, 12, 82, 22, 18, 89));
                } else {
                    $results = $this->model_catalog_category->getCategories($category_id);
                }

                foreach ($results as $result) {
                    $filter_data = array(
                        'filter_category_id' => $result['category_id'],
                        'filter_sub_category' => true
                    );

                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], 337, 337);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 337, 337);
                    }

                    $data['categories'][] = array(
                        'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'thumb' => $image,
                        'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
                    );
                }

                $results = $this->model_catalog_category->getCategories2($category_id);

                foreach ($results as $result) {
                    $filter_data = array(
                        'filter_category_id' => $result['category_id'],
                        'filter_sub_category' => true
                    );

                    if ($result['image']) {
                        $image = $this->model_tool_image->resize($result['image'], 337, 337);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', 337, 337);
                    }

                    $data['categories'][] = array(
                        'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                        'thumb' => $image,
                        'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
                    );
                }
            } // end
            $data['option_value_filter'] = array();

            if(isset($this->request->get['mfp']))
            {
                $mfp = $this->request->get['mfp'];
                preg_match_all('/\[(.*?)\]/',$mfp,$matches);

                $filters_id_tmp2= array(0);
                if(isset($matches[1][0]))
                {
                    foreach ($matches[1] as $filters_id)
                    {
                        $filters_id_tmp = explode(',',$filters_id);
                        foreach ($filters_id_tmp as $filter_id)
                            $filters_id_tmp2[]=$filter_id;
                    }
                }
                $results = $this->db->query("SELECT * FROM `oc_option_value_filter` WHERE `filter_id` in (".implode(',',$filters_id_tmp2).")")->rows;
                foreach ($results as $row)
                {
                    if(!isset($data['option_value_filter'][$row['option_value_id']]))
                        $data['option_value_filter'][$row['option_value_id']] = array();
                    $data['option_value_filter'][$row['option_value_id']][] = $row['filter_id'];
                }
            }


            $data['products'] = array();
            $data['products_id'] = [];


            $data['products_special'] = $this->FAQPage(array(
                'filter_category_id' => $category_id,
                'sort' => 'p.price',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 3,
                'old_price' => 1,

            ));

            $data['products_min'] = $this->FAQPage(array(
                'filter_category_id' => $category_id,
                'sort' => 'p.price',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 3
            ));

           $data['products_max'] = $this->FAQPage(array(
                'filter_category_id' => $category_id,
                'sort' => 'p.price',
                'order' => 'DESC',
                'start' => 0,
                'limit' => 3
            ));

           $data['products_top'] = $this->FAQPage(array(
                'filter_category_id' => $category_id,
                'sort' => 'p.price',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 3,
                'sale_status_id' => 2
            ));

            $filter_data = array(
                'filter_category_id' => $category_id,
                'filter_filter' => $filter,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit,
                //'filter_stock_status_id' => array(1, 2, 3, 4, 5, 7, 8, 9, 10),
                //9 - снято с продажи Барный стул IK 831
            );

            $product_total = $this->model_catalog_product->getTotalProducts($filter_data);

            $data['product_total'] = $product_total;
            $data['product_total'] = $limit;
            if (isset($this->request->get['mfp']))
            {
                $filter_data['mfilter_image'] = 1;
            }
            $results = $this->model_catalog_product->getProducts($filter_data);

            /*
            if (($page * $limit) >= $product_total) {
                $filter_data = array(
                    'filter_category_id' => $category_id,
                    'filter_filter' => $filter,
                    'sort' => $sort,
                    'order' => $order,
                    'limit' => $limit,
                    'filter_stock_status_id' => array(6, 11, 12),
                );

                $res = $this->model_catalog_product->getProducts($filter_data);


                if (count($res) < $limit) {
                    $results = array_merge($results, $res);
                    $i = 0;
//print_r($results); die();
                    /*
                                        while(1){
                                            $i++;	if ($i>100) break;
                                            if (count($results) > $limit)
                                            {
                                                reset($results);
                                                unset(current($results));
                                            }
                                        };

                }
            }
            */
            $this->load->model('localisation/sale_status');

            $data['sale_statuses'] = $this->model_localisation_sale_status->getSaleStatuses();

            $data['reviews_cat'] = array();


            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    $reviews_image = $this->model_tool_image->resize($result['image'], 600, 600);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                    $reviews_image = $this->model_tool_image->resize('placeholder.png', 600, 600);
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    //$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                    $price = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                    if ($suppliers[$result['supplier_id']] > 0)
                        $price =ceil($price*$suppliers[$result['supplier_id']]['coefficient']);
                    $price_num = $price;
                    $price = $this->currency->format($price);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    //$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                    $special = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
                    //if ($suppliers[$result['supplier_id']] > 0)
                    //    $special =ceil($special*$suppliers[$result['supplier_id']]['coefficient']);
                    $special_num = $special;
                    $special = $this->currency->format($special);
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }
                $json = array();


                if (isset($this->request->post['product_id'])) {
                    $product_id = $this->request->post['product_id'];
                } else {
                    $product_id = 0;
                }


                $this->load->model('catalog/product');


                $product_info = $this->model_catalog_product->getProduct($product_id);

                if ($product_info && false) {
                    $options = array();
                    foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                        $product_option_value_data = array();
                        foreach ($option['product_option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    //$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                    $price_opt = $this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                                    $supplier_id = $product_info['supplier_id'];
                                    if ($suppliers[$supplier_id] > 0)
                                        $price_opt =ceil($price_opt*$suppliers[$supplier_id]['coefficient']);
                                    $price_opt = $this->currency->format($price_opt);
                                } else {
                                    $price_opt = false;
                                }

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'price' => $price_opt,
                                    'price_prefix' => $option_value['price_prefix']
                                );
                            }
                        }
                        $options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required'],
                            'sort_option' => $option['sort_option']
                        );
                        $options['product_id'] = $product_info['product_id'];
                        $options['name'] = $product_info['name'];
                    }
                    if (!$json) {
                        $json = $options;
                    }
                }

                $options_p = array();
                $products_color = array();
                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                    $product_option_value_data_p = array();
                    $product_option_value_data_p_tmp = array();
                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {

                                $price_opt = $this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                                $supplier_id = $result['supplier_id'];
                                if ($suppliers[$supplier_id] > 0)
                                    $price_opt =ceil($price_opt*$suppliers[$supplier_id]['coefficient']);
                                $price_opt = $this->currency->format($price_opt);

                            } else {
                                $price_opt = false;
                            }


                            //$_special_p = ceil($special_p).' грн.';

                            if(isset( $data['option_value_filter'][$option_value['option_value_id']]))
                            {
                                $product_option_value_data_p[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    //'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'image' => $this->model_tool_image->resize($option_value['image'], 32, 32),
                                    'quantity' => $option_value['quantity'],
                                    'subtract' => $option_value['subtract'],
                                    'price' => $price_opt,
                                    'price_prefix' => $option_value['price_prefix'],
                                    'weight' => $option_value['weight'],
                                    'image_prod' => $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
                                    'supplier_id' => $suppliers[$supplier_id],
                                    'weight_prefix' => $option_value['weight_prefix']
                                );
                                if(!empty($option_value['image_prod']))
                                {
                                    $products_color[] = $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                                }
                             }
                            else
                            {
                                $product_option_value_data_p_tmp[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    //'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'image' => $this->model_tool_image->resize($option_value['image'], 32, 32),
                                    'quantity' => $option_value['quantity'],
                                    'subtract' => $option_value['subtract'],
                                    'price' => $price_opt,
                                    'price_prefix' => $option_value['price_prefix'],
                                    'weight' => $option_value['weight'],
                                    'image_prod' => $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
                                    'supplier_id' => $suppliers[$supplier_id],
                                    'weight_prefix' => $option_value['weight_prefix']
                                );
                            }

                        }
                    }
                    foreach ($product_option_value_data_p_tmp as $item)
                        $product_option_value_data_p[] = $item;

                    $options_p[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data_p,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required'],
                        'sort_option' => $option['sort_option']
                    );
                }



                $special_info_tmp = $this->model_catalog_product->getProductSpecial($result['product_id']);

                if(!empty($special_info_tmp)){
                    if(strtotime($special_info_tmp['date_end']) - time()>0)
                        $result['sale_status_id']=1;
                    else
                        $result['sale_status_id']=0;
                }



                if($special_num)
                    $this->model_catalog_product->setPriceCurrent( $result['product_id'],$special_num);
                else
                    $this->model_catalog_product->setPriceCurrent( $result['product_id'],$price_num);

                if (!$result['price_show'])
                    $price = '';
                if(count($products_color)>0)
                {
                    $image = $products_color[0];
                    unset($products_color[0]);
                }

                $reviews_item = array();
                $reviews = $this->model_catalog_review->getReviewsByProductId($result['product_id']);
                $product_random = random_int(0, 1);
                if(count($reviews) > 0 && count($data['reviews_cat'])<6 && $product_random)
                {
                    foreach ($reviews as $review_tmp) {
                        $text_tmp = explode('.',str_replace('"', '\"', nl2br($review_tmp['text'])));
                        $text = array();
                        $text_count = count($text_tmp);
                        if($text_count>3)
                        {
                            $text_index = random_int(0, 2);
                            if($text_index>$text_count)
                                $text_index = $text_count-2;
                            $text_item_count = 0;
                            foreach ($text_tmp as $index => $text_item )
                            {
                                if($index>$text_index && $text_item_count<17)
                                {
                                    $text[] = $text_item;
                                    $text_item_count += count(explode(" ",$text_item));
                                }
                            }
                            $name_random = random_int(0, 1);
                            if($name_random)
                                $product_name = $result['name'].' - '.$result['sku'];
                            else
                                $product_name = $result['name'];

                            $data['reviews_cat'][] = array(
                                'name' => $product_name,
                                'thumb' => $reviews_image,
                                'author' => $review_tmp['author'],
                                'text' => implode(". ", $text) .'...',
                                'rating' => (int)$review_tmp['rating'],
                                'date_added' => date('Y-m-d', strtotime($review_tmp['date_added'])),
                                'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
                            );
                            break;
                        }
                    }



                }


                foreach ($reviews as $review) if ((int)$result['rating'] > 0) {
                    $reviews_item[] = array(

                        'author' => $review['author'],
                        'text' => str_replace('"', '\"', nl2br($review['text'])),
                        'rating' => (int)$review['rating'],
                        'date_added' => date('Y-m-d', strtotime($review['date_added']))
                    );
                    $data['reviews_rating'] += (int)$result['rating'];
                }



                    $data['products'][] = array(
                        'product_id' => $result['product_id'],
                        'name' => $result['name'].' - '.$result['sku'],
                        'thumb' => $image,
                        'reviews_item' => $reviews_item,
                        'sku' => (empty($result['sku'])) ? '' : $this->language->get('text_sku') . ' ' . $result['sku'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $price,
                        'old_price' => $result['old_price'],
                        'youtube' => $result['youtube'],
                        'sale_status_name' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['name']:'',
                        'sale_status_color' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['color']:'',
                        'sale_status_image' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$result['sale_status_id']]['image']:'',
                        'price_num' => $price_num,
                        'special' => $special,

                        'special_end' => $special_end,

                        'stock_status' => $result['stock_status'],
                        'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                        'tax' => $tax,
                        'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'rating' => $result['rating'],
                        'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
                        //'options'   => $this->model_catalog_product->getProductOptions($result['product_id']),
                        'options' => $options_p,
                        'bestseller' => $result['upc'],
                        'recommended' => $result['ean'],
                        'privat' => $result['product_pp'] || $result['product_ii']? 1 : 0,
                    );
                foreach ($products_color as $product_color)
                    $data['products'][] = array(
                        'product_id' => $result['product_id'],
						 'reviews_item' => $reviews_item,
                        'name' => $result['name'].' - '.$result['sku'],
                        'thumb' => $product_color,
                        'sku' => (empty($result['sku'])) ? '' : $this->language->get('text_sku') . ' ' . $result['sku'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $price,
                        'old_price' => $result['old_price'],
                        'youtube' => $result['youtube'],
                        'sale_status_name' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['name']:'',
                        'sale_status_color' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['color']:'',
                        'sale_status_image' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$result['sale_status_id']]['image']:'',
                        'price_num' => $price_num,
                        'special' => $special,

                        'special_end' => $special_end,

                        'stock_status' => $result['stock_status'],
                        'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                        'tax' => $tax,
                        'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'rating' => $result['rating'],
                        'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url),
                        //'options'   => $this->model_catalog_product->getProductOptions($result['product_id']),
                        'options' => $options_p,
                        'bestseller' => $result['upc'],
                        'recommended' => $result['ean'],
                        'privat' => $result['product_pp'] || $result['product_ii']? 1 : 0,
                    );


                $data['products_id'][] = $result['product_id'];
            }

            $data['products_id'] = json_encode($data['products_id']);
            //print_r($data['products']);
            /*
            //////////////////////////////////////////////////////
            if ($sort == 'p.price' && $order == 'ASC') {
                uasort($data['products'], function ($a, $b) {
                    if ($a['price_num'] === $b['price_num']) return 0;
                    return $a['price_num'] < $b['price_num'] ? -1 : 1;
                });
            } elseif ($sort == 'p.price' && $order == 'DESC') {
                uasort($data['products'], function ($a, $b) {
                    if ($a['price_num'] === $b['price_num']) return 0;
                    return $a['price_num'] < $b['price_num'] ? 1 : -1;
                });
            }
            ////////////////////////////////////////////////////////
            */
            $url = '';
            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }
            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }
            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text' => $this->language->get('text_default'),
                'value' => 'p.sort_order-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
            );

            /* $data['sorts'][] = array(
                'text'  => $this->language->get('text_name_asc'),
                'value' => 'pd.name-ASC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_name_desc'),
                'value' => 'pd.name-DESC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
            ); */

            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
            );

            if ($this->config->get('config_review_status')) {
                $data['sorts'][] = array(
                    'text' => $this->language->get('text_rating_desc'),
                    'value' => 'rating-DESC',
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
                );

                $data['sorts'][] = array(
                    'text' => $this->language->get('text_rating_asc'),
                    'value' => 'rating-ASC',
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
                );
            }

            /* $data['sorts'][] = array(
                'text'  => $this->language->get('text_model_asc'),
                'value' => 'p.model-ASC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text'  => $this->language->get('text_model_desc'),
                'value' => 'p.model-DESC',
                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
            ); */

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

            sort($limits);

            foreach ($limits as $value) {
                $data['limits'][] = array(
                    'text' => $value,
                    'value' => $value,
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
                );
            }

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
            if ($page == 1) {
            } elseif ($page == 2) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL') . _FILTER_LANGUAGE_ . '/', 'prev');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL')), 'prev');
                }
            } else {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink(str_replace('?page=', _FILTER_LANGUAGE_ . '/page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page - 1), 'SSL')) . '/', 'prev');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page - 1), 'SSL')) . '/', 'prev');
                }
            }
            if ($page == 1) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL') . _FILTER_LANGUAGE_ . '/', 'canonical');
                } else {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'canonical');
                }
            } elseif ($page > 1) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink(str_replace('?page=', _FILTER_LANGUAGE_ . '/page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . $page, 'SSL')) . '/', 'canonical');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . $page, 'SSL')) . '/', 'canonical');
                }
            }
            if ($limit && ceil($product_total / $limit) > $page) {
                if (defined('_FILTER_LANGUAGE_')) {
                    $this->document->addLink(str_replace('?page=', _FILTER_LANGUAGE_ . '/page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page + 1), 'SSL')) . '/', 'next');
                } else {
                    $this->document->addLink(str_replace('?page=', 'page-', $this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page + 1), 'SSL')) . '/', 'next');
                }
            }

            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;

            $data['continue'] = $this->url->link('common/home');
            $url=explode('?',$_SERVER['REQUEST_URI']);
            if(isset($url[1]))
            {
                $get_params=[];
                $get=explode('&',$url[1]);
                foreach ($get as $get_val)
                {
                    $get_val=explode('=',$get_val);
                    if(!isset($get_val[1]))
                        $get_val[1]='';
                    $get_params[$get_val[0]]=$get_val[1];
                }
            }
            $data['page_number'] = 1;
            if(isset($get_params['page']))
                $data['page_number']=$get_params['page'];
            unset($get_params['ajax']);
            unset($get_params['page']);
            $get=[];
            foreach ($get_params as $key=>$val)
                $get[]=$key.'='.$val;
            $get=implode('&',$get);
            if($get!='')
                $get='?'.$get;
            $data['page_url']= $url[0].$get;

            if(!isset($this->request->get['ajax'])&&!isset($this->request->get['ajax_cart']))
            {

                $data['column_left'] = $this->load->controller('common/column_left');
                $data['column_right'] = $this->load->controller('common/column_right');
                $data['content_top'] = $this->load->controller('common/content_top');
                $data['content_bottom'] = $this->load->controller('common/content_bottom');
                $data['footer'] = $this->load->controller('common/footer');
                $data['header'] = $this->load->controller('common/header');
                $data['curr_lang'] = $this->language->get('code');

                $product_interest = $this->model_catalog_product->getProductInterest($category_id);
                if (!count($product_interest)) {
                    $product_interest = $this->model_catalog_product->getProductInterest($category_info['parent_id']);
                }
                $data['product_interest'] = array();

                foreach ($product_interest as $result) {
                    if ($result['image']) {
                        $prod_image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    } else {
                        $prod_image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        //$prod_price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                        $prod_price = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                        if ($suppliers[$result['supplier_id']] > 0)
                            $prod_price =ceil($prod_price*$suppliers[$result['supplier_id']]['coefficient']);
                        $prod_price = $this->currency->format($prod_price);
                    } else {
                        $prod_price = false;
                    }

                    if ((float)$result['special']) {
                        //$prod_special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                        $prod_special = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
                        if ($suppliers[$result['supplier_id']] > 0)
                            $prod_special =ceil($prod_special*$suppliers[$result['supplier_id']]['coefficient']);
                        $prod_special = $this->currency->format($prod_special);
                    } else {
                        $prod_special = false;
                    }


                    if ($this->config->get('config_tax')) {
                        $prod_tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                    } else {
                        $prod_tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $prod_rating = (int)$result['rating'];
                    } else {
                        $prod_rating = false;
                    }

                    $prod_options = array();

                    foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                        $product_option_value_data = array();

                        foreach ($option['product_option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    ///$price_opt = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                    $price_opt = $this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false);
                                    if ($suppliers[$result['supplier_id']] > 0)
                                        $price_opt =ceil($price_opt*$suppliers[$result['supplier_id']]['coefficient']);
                                    $price_opt = $this->currency->format($price_opt);
                                } else {
                                    $price_opt = false;
                                }
                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    'image' => $option_value['image'],
                                    'price' => $price_opt,
                                    'price_prefix' => $option_value['price_prefix']
                                );
                            }
                        }

                        $prod_options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required'],
                            'sort_option' => $option['sort_option']
                        );

                    }

                    $d_prod = array(
                        'product_id' => $result['product_id'],
                        'thumb' => $prod_image,
                        'name' => $result['model'],
                        'youtube' => $result['youtube'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $prod_price,
                        'special' => $prod_special,
                        'tax' => $prod_tax,
                        'options' => $prod_options,
                        'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                        'rating' => $prod_rating,
                        'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                    );

                    $d_prod['product_info']['price'] = $prod_price;
                    $data['product_interest'][] = $d_prod;

                }

                /*
                            $data['waiting'] = array();
                            if (count($data['products'])>0)
                            foreach($data['products'] as $i => $product)
                            {
                                if (strpos( $product['stock_status'], 'жида') !==false)
                                {
                                    $data['products'][] = $product;
                                    $data['waiting'][] = $product;
                                    unset($data['products'][$i]);
                                }
                            }
                */


                $data['text_seo'] = '';
                if(!isset($_REQUEST['sort']) && $page == 1 && (count($mfp_index)==1 && isset($mfp_index[1]) && $mfp_index[1]<3 || defined('_FILTER_NAME_')))
                {
                    $row = $this->db->query("SELECT * FROM `text_seo` WHERE `url` = '" . $_SERVER['REQUEST_URI'] . "'")->row;

                    if(!isset($row['text']) || empty($row['text']))
                    {

                        $arr_new = array
                        (
                            0 => array
                            (
                                0 => 'Дарим возможность',
                                1 => 'Предлагаем Вам',
                                2 => 'Уникальная возможность',
                                3 => 'У нас можно',
                                4 => 'У нас Вы можете',
                                5 => 'У нас возможно',
                                6 => 'У нас Вы сможете',
                                7 => 'Предоставляем возможность',
                                8 => 'Рекомендуем',
                                9 => 'Мы рекомендуем',
                                10 => 'Рекомендуем Вам',
                                11 => 'Мы рекомендуем Вам',
                                12 => 'Мы предлагаем',
                                13 => 'Предлагаем',
                                14 => 'Мы предлагаем Вам',
                            ),

                            1 => array
                            (
                                0 => 'купить',
                                1 => 'приобрести',
                                2 => 'заказать',
                                3 => 'выбрать',
                                4 => 'подобрать',
                                5 => 'выбрать и купить',
                                6 => 'выбрать и приобрести',
                                7 => 'выбрать и заказать',
                                8 => 'подобрать и купить',
                                9 => 'подобрать и приобрести',
                                10 => 'подобрать и заказать',
                            ),

                            2 => array
                            (
                                0 => '{GET_PAGE_H1}',
                            ),

                            3 => array
                            (
                                0 => 'по выгодной цене',
                                1 => 'по выгодной стоимости',
                                2 => 'по доступной цене',
                                3 => 'по доступной стоимости',
                                4 => 'по лучшей цене',
                                5 => 'по лучшей стоимости',
                                6 => 'по актуальной цене',
                                7 => 'по актуальной стоимости',
                                8 => 'по нормальной цене',
                                9 => 'по нормальной стоимости',
                                10 => 'по хорошей цене',
                                11 => 'по хорошей стоимости',
                            ),

                            4 => array
                            (
                                0 => 'на сайте',
                                1 => 'на сайте мебели',
                                2 => 'на нашем сайте',
                                3 => 'на сайте интернет магазина мебели',
                                4 => 'в онлайн магазине мебели',
                                5 => 'в магазине мебели',
                                6 => 'в интернет магазине мебели',
                                7 => 'в каталоге мебели',
                                8 => 'в интернет магазине',
                                9 => 'в каталоге интернет магазина мебели',
                                10 => 'в каталоге магазина мебели',
                                11 => 'на сайте столов и стульев',
                                12 => 'на сайте интернет магазина столов и стульев',
                                13 => 'в онлайн магазине столов и стульев',
                                14 => 'в магазине столов и стульев',
                                15 => 'в интернет магазине столов и стульев',
                                16 => 'в каталоге столов и стульев',
                                17 => 'в каталоге интернет магазина столов и стульев',
                                18 => 'в каталоге магазина столов и стульев',
                            ),

                            5 => array
                            (
                                0 => 'Фешемебельный,',
                                1 => 'feshmebel.com.ua,',
                            ),

                            6 => array
                            (
                                0 => 'с доставкой по',
                            ),

                            7 => array
                            (
                                0 => '{geo_dative}',
                            ),

                            8 => array
                            (
                                0 => 'и Украине.',
                                1 => 'и всей Украине.',
                                2 => 'и другим уголкам страны.',
                                3 => 'и других уголках Украины.',
                                4 => 'и других регионах Украины.',
                                5 => 'и другим городам Украины.',
                                6 => 'и другим регионам Украины.',
                            ),

                            9 => array
                            (
                                0 => 'Выбирайте и заказывайте ',
                                1 => 'Выбирайте и покупайте также ',
                                2 => 'Смотрите и заказывайте также ',
                                3 => 'Посмотрите и купите также',
                                4 => 'Лучшие предложения на ',
                                5 => 'Самые лучшие предложения на',
                                6 => 'Выгодные предложения на ',
                                7 => 'Заманчивые предложения на',
                                8 => 'Покупайте также',
                                9 => 'Также обратите внимание на',
                                10 => 'Выбирайте также',
                                11 => 'Заказывайте также',
                            ),

                            10 => array
                            (
                                0 => '{tags}',
                            ),

                            11 => array
                            (
                                0 => 'от лучших производителей',
                                1 => 'от таких брендов как',
                                2 => 'от самых лучших производителей',
                                3 => 'от самых лучших брендов',
                                4 => 'от качественных производителей',
                                5 => 'от качественных брендов',
                                6 => 'от известных производителей',
                                7 => 'от известных брендов',
                                8 => 'от популярных производителей',
                                9 => 'от популярных брендов',
                                10 => 'от надежных производителей',
                            ),

                            12 => array
                            (
                                0 => 'только у нас.',
                                1 => 'у нас в магазине.',
                                2 => 'в нашем магазине.',
                            ),

                            13 => array
                            (
                                0 => 'Самое выгодное предложение на',
                                1 => 'Лучшее предложение на',
                                2 => 'Лучшие предложения на',
                                3 => 'Выгодные предложения на',
                                4 => 'Наилучшие предложения на',
                                5 => 'Доступные предложения на',
                                6 => 'Актуальные предложения на',
                            ),

                            14 => array
                            (
                                0 => '{GET_PAGE_H1}',
                            ),

                            15 => array
                            (
                                0 => 'и',
                            ),

                            16 => array
                            (
                                0 => '{tags_1}',
                            ),

                            17 => array
                            (
                                0 => '- в нашем каталоге',
                                1 => '- в каталоге',
                                2 => '- в магазине',
                                3 => '- в каталоге нашего магазина',
                                4 => '- в каталоге магазина',
                            ),

                            18 => array
                            (
                                0 => 'Вы найдете',
                                1 => 'Вы сможете найти',
                                2 => 'Вы сможете подобрать',
                                3 => 'Вы подберете ',
                            ),

                            19 => array
                            (
                                0 => 'только лучшую мебель.',
                                1 => 'самые надежные товары.',
                                2 => 'самые качественные изделия.',
                                3 => 'все что нужно.',
                                4 => 'только надежные изделия.',
                                5 => 'только качественные товары.',
                            ),

                            20 => array
                            (
                                0 => 'Для того чтобы',
                                1 => 'Чтобы',
                                2 => 'Если Вы уже решили',
                                3 => 'Если Вы решили',
                                4 => 'Если Вы хотите',
                                5 => 'Если хотите',
                            ),

                            21 => array
                            (
                                0 => 'купить',
                                1 => 'заказать',
                                2 => 'оформить заказ на',
                                3 => 'оформить покупку на',
                            ),

                            22 => array
                            (
                                0 => '{Main_Category_H1}',
                            ),

                            23 => array
                            (
                                0 => '- добавьте в корзину товар,',
                                1 => '- добавьте товар в корзину,',
                            ),

                            24 => array
                            (
                                0 => 'и выберите удобный',
                                1 => 'и выберите',
                                2 => 'и укажите',
                                3 => 'и укажите удобный',
                            ),

                            25 => array
                            (
                                0 => 'метод оплаты и доставки.',
                                1 => 'способ оплаты и доставки.',
                                2 => 'тип оплаты и доставки.',
                                3 => 'метод доставки и оплаты.',
                                4 => 'способ доставки и оплаты.',
                                5 => 'тип доставки и оплаты.',
                            ),

                            26 => array
                            (
                                0 => 'После этого,',
                                1 => 'После,',
                                2 => 'Затем,',
                            ),

                            27 => array
                            (
                                0 => 'заполните',
                                1 => 'укажите',
                            ),

                            28 => array
                            (
                                0 => 'свои контактные данные,',
                                1 => 'Ваши контактные данные,',
                                2 => 'Ваши контакты,',
                                3 => 'Ваши данные,',
                                4 => 'свои данные,',
                                5 => 'свои контакты,',
                            ),

                            29 => array
                            (
                                0 => 'и мы сразу',
                                1 => 'и мы',
                            ),

                            30 => array
                            (
                                0 => 'оформим доставку',
                                1 => 'оформим поставку',
                            ),

                            31 => array
                            (
                                0 => 'товара.',
                                1 => 'выбранного товара.',
                                2 => 'указанного товара.',
                                3 => 'нужного товара.',
                            ),

                            32 => array
                            (
                                0 => 'Обратите внимание,',
                                1 => 'Не забудьте,',
                                2 => 'Напоминаем,',
                                3 => 'Хотим обратить Ваше внимание,',
                                4 => 'Рекомендуем обратить Ваше внимание,',
                            ),

                            33 => array
                            (
                                0 => 'что для постоянных покупателей',
                                1 => 'что для постоянных клиентов',
                            ),

                            34 => array
                            (
                                0 => 'действуют скидки,',
                                1 => 'действуют постоянные скидки,',
                                2 => 'действуют выгодные скидки,',
                                3 => 'работают скидки,',
                                4 => 'работают постоянные скидки,',
                                5 => 'работают выгодные скидки,',
                            ),

                            35 => array
                            (
                                0 => 'которые помогут купить',
                                1 => 'которые позволят купить',
                                2 => 'которые позволят приобрести',
                                3 => 'которые дадут возможность купить',
                                4 => 'которые дадут возможность приобрести',
                                5 => 'которые помогут приобрести',
                                6 => 'которые помогут купить',
                            ),

                            36 => array
                            (
                                0 => '{tags_2}',
                            ),

                            37 => array
                            (
                                0 => 'или',
                                1 => 'и',
                            ),

                            38 => array
                            (
                                0 => '{tags_3}',
                            ),

                            39 => array
                            (
                                0 => 'по самой выгодной цене.',
                                1 => 'по наиболее выгодной цене.',
                                2 => 'по самой доступной цене.',
                                3 => 'по самым доступным ценам.',
                                4 => 'по самым выгодным ценам.',
                                5 => 'по самым лучшим ценам.',
                                6 => 'по самой привлекательной цене.',
                                7 => 'по наиболее привлекательным ценам.',
                            ),

                        );
                        $arr_new_ua =
                            array
                            (
                                0 => array
                                (
                                    0 => 'даруємо можливість',
                                    1 => 'пропонуємо Вам',
                                    2 => 'Унікальна можливість',
                                    3 => 'У нас можна',
                                    4 => 'У нас Ви можете',
                                    5 => 'У нас можливо',
                                    6 => 'У нас Ви зможете',
                                    7 => 'надаємо можливість',
                                    8 => 'Рекомендуємо',
                                    9 => 'Ми рекомендуємо',
                                    10 => 'Рекомендуємо Вам',
                                    11 => 'Ми рекомендуємо Вам',
                                    12 => 'Ми пропонуємо',
                                    13 => 'пропонуємо',
                                    14 => 'Ми пропонуємо вам'
                                ),

                                1 => array
                                (
                                    0 => 'купити',
                                    1 => 'придбати',
                                    2 => 'замовити',
                                    3 => 'вибрати',
                                    4 => 'підібрати',
                                    5 => 'вибрати і купити',
                                    6 => 'вибрати і придбати',
                                    7 => 'вибрати і замовити',
                                    8 => 'підібрати і купити',
                                    9 => 'підібрати і придбати',
                                    10 => 'підібрати і замовити'
                                ),

                                2 => array
                                (
                                    0 => '{GET_PAGE_H1}'
                                ),

                                3 => array
                                (
                                    0 => 'за вигідною ціною',
                                    1 => 'за вигідною вартістю',
                                    2 => 'за доступною ціною',
                                    3 => 'за доступною вартістю',
                                    4 => 'за найкращою ціною',
                                    5 => 'за кращою вартістю',
                                    6 => 'по актуальній ціні',
                                    7 => 'з актуальної вартості',
                                    8 => 'за нормальною ціною',
                                    9 => 'за нормальною вартістю',
                                    10 => 'за хорошою ціною',
                                    11 => 'за хорошою вартістю'
                                ),

                                4 => array
                                (
                                    0 => 'на сайті',
                                    1 => 'на сайті меблів',
                                    2 => 'на нашому сайті',
                                    3 => 'на сайті інтернет магазину меблів',
                                    4 => 'в онлайн магазині меблів',
                                    5 => 'в магазині меблів',
                                    6 => 'в інтернет магазині меблів',
                                    7 => 'в каталозі меблів',
                                    8 => 'в інтернет магазині',
                                    9 => 'в каталозі інтернет магазину меблів',
                                    10 => 'в каталозі магазина меблів',
                                    11 => 'на сайті столів і стільців',
                                    12 => 'на сайті інтернет магазину столів і стільців',
                                    13 => 'в онлайн магазині столів і стільців',
                                    14 => 'в магазині столів і стільців',
                                    15 => 'в інтернет магазині столів і стільців',
                                    16 => 'в каталозі столів і стільців',
                                    17 => 'в каталозі інтернет магазину столів і стільців',
                                    18 => 'в каталозі магазина столів і стільців'
                                ),

                                5 => array
                                (
                                    0 => 'Фешемебельний,',
                                    1 => 'feshmebel.com.ua,'
                                ),

                                6 => array
                                (
                                    0 => 'з доставкою по'
                                ),

                                7 => array
                                (
                                    0 => '{geo_dative}'
                                ),

                                8 => array
                                (
                                    0 => 'і Україні.',
                                    1 => 'і всій Україні.',
                                    2 => 'і іншим куточкам країни.',
                                    3 => 'та інших куточках України.',
                                    4 => 'та інших регіонах України.',
                                    5 => 'та іншим містам України.',
                                    6 => 'і іншим регіонам України.'
                                ),

                                9 => array
                                (
                                    0 => 'Обирайте і замовляйте також  ',
                                    1 => 'Обирайте і купуйте також  ',
                                    2 => 'Дивіться і замовляйте також ',
                                    3 => 'Подивіться і купуйте також',
                                    4 => 'Кращі пропозиції на ',
                                    5 => 'Найкращі пропозиції на',
                                    6 => 'Вигідні пропозиції',
                                    7 => 'Привабливі пропозиції на',
                                    8 => 'Купуйте також',
                                    9 => 'Також зверніть увагу на',
                                    10 => 'вибирайте також',
                                    11 => 'замовляйте також'
                                ),

                                10 => array
                                (
                                    0 => '{tags}'
                                ),

                                11 => array
                                (
                                    0 => 'від кращих виробників',
                                    1 => 'від таких брендів як',
                                    2 => 'від найкращих виробників',
                                    3 => 'від найкращих брендів',
                                    4 => 'від якісних виробників',
                                    5 => 'від якісних брендів',
                                    6 => 'від відомих виробників',
                                    7 => 'від відомих брендів',
                                    8 => 'від популярних виробників',
                                    9 => 'від популярних брендів',
                                    10 => 'від надійних виробників'
                                ),

                                12 => array
                                (
                                    0 => 'тільки у нас.',
                                    1 => 'у нас в магазині.',
                                    2 => 'в нашому магазині.'
                                ),

                                13 => array
                                (
                                    0 => 'Найвигіднішу пропозицію на',
                                    1 => 'Краща пропозиція на',
                                    2 => 'Кращі пропозиції на',
                                    3 => 'Вигідні пропозиції на',
                                    4 => 'Найкращі пропозиції на',
                                    5 => 'Доступні пропозиції на',
                                    6 => 'Актуальні пропозиції на'
                                ),

                                14 => array
                                (
                                    0 => '{GET_PAGE_H1}'
                                ),

                                15 => array
                                (
                                    0 => 'і'
                                ),

                                16 => array
                                (
                                    0 => '{tags_1}'
                                ),

                                17 => array
                                (
                                    0 => '- в нашому каталозі',
                                    1 => '- в каталозі',
                                    2 => '- в магазині',
                                    3 => '- в каталозі нашого магазину',
                                    4 => '- в каталозі магазина'
                                ),

                                18 => array
                                (
                                    0 => 'Ви знайдете',
                                    1 => 'Ви зможете знайти',
                                    2 => 'Ви зможете підібрати',
                                    3 => 'Вы подберете '
                                ),

                                19 => array
                                (
                                    0 => 'тільки кращі меблі.',
                                    1 => 'найнадійніші товари.',
                                    2 => 'найякісніші вироби.',
                                    3 => 'все що потрібно.',
                                    4 => 'тільки надійні вироби.',
                                    5 => 'тільки якісні товари.'
                                ),

                                20 => array
                                (
                                    0 => 'Для того щоб',
                                    1 => 'щоб',
                                    2 => 'Якщо Ви вже вирішили',
                                    3 => 'Якщо Ви вирішили',
                                    4 => 'Якщо ви хочете',
                                    5 => 'Якщо хочете'
                                ),

                                21 => array
                                (
                                    0 => 'купити',
                                    1 => 'замовити',
                                    2 => 'оформити замовлення на',
                                    3 => 'оформити покупку на'
                                ),

                                22 => array
                                (
                                    0 => '{Main_Category_H1}'
                                ),

                                23 => array
                                (
                                    0 => '- додайте в корзину товар,',
                                    1 => '- додайте товар в корзину,'
                                ),

                                24 => array
                                (
                                    0 => 'і виберіть зручний',
                                    1 => 'і виберіть',
                                    2 => 'і вкажіть',
                                    3 => 'і вкажіть зручний'
                                ),

                                25 => array
                                (
                                    0 => 'метод оплати і доставки.',
                                    1 => 'спосіб оплати і доставки.',
                                    2 => 'тип оплати та доставки.',
                                    3 => 'метод доставки і оплати.',
                                    4 => 'спосіб доставки і оплати.',
                                    5 => 'тип доставки і оплати.'
                                ),

                                26 => array
                                (
                                    0 => 'Після цього,',
                                    1 => 'після,',
                                    2 => 'потім,'
                                ),

                                27 => array
                                (
                                    0 => 'заповніть',
                                    1 => 'вкажіть'
                                ),

                                28 => array
                                (
                                    0 => 'свої контактні дані,',
                                    1 => 'Ваші контактні дані,',
                                    2 => 'Ваші контакти,',
                                    3 => 'Ваші дані,',
                                    4 => 'свої дані,',
                                    5 => 'свої контакти,'
                                ),

                                29 => array
                                (
                                    0 => 'і ми відразу',
                                    1 => 'і ми'
                                ),

                                30 => array
                                (
                                    0 => 'оформимо доставку',
                                    1 => 'оформимо поставку'
                                ),

                                31 => array
                                (
                                    0 => 'товару.',
                                    1 => 'обраного товару.',
                                    2 => 'вказаного товару.',
                                    3 => 'потрібного товару.'
                                ),

                                32 => array
                                (
                                    0 => 'Зверніть увагу,',
                                    1 => 'Не забудьте,',
                                    2 => 'Нагадуємо,',
                                    3 => 'Хочемо звернути Вашу увагу,',
                                    4 => 'Рекомендуємо звернути Вашу увагу,'
                                ),

                                33 => array
                                (
                                    0 => 'що для постійних покупців',
                                    1 => 'що для постійних клієнтів'
                                ),

                                34 => array
                                (
                                    0 => 'діють знижки,',
                                    1 => 'діють постійні знижки,',
                                    2 => 'діють вигідні знижки,',
                                    3 => 'працюють знижки,',
                                    4 => 'працюють постійні знижки,',
                                    5 => 'працюють вигідні знижки,'
                                ),

                                35 => array
                                (
                                    0 => 'які допоможуть купити',
                                    1 => 'які дозволять купити',
                                    2 => 'які дозволять придбати',
                                    3 => 'які дадуть можливість купити',
                                    4 => 'які дадуть можливість придбати',
                                    5 => 'які допоможуть придбати',
                                    6 => 'які допоможуть купити'
                                ),

                                36 => array
                                (
                                    0 => '{tags_2}'
                                ),

                                37 => array
                                (
                                    0 => 'або',
                                    1 => 'і'
                                ),

                                38 => array
                                (
                                    0 => '{tags_3}'
                                ),

                                39 => array
                                (
                                    0 => 'за самою вигідною ціною.',
                                    1 => 'за найбільш вигідною ціною.',
                                    2 => 'за найдоступнішою ціною.',
                                    3 => 'за найдоступнішими цінами.',
                                    4 => 'за найвигіднішими цінами.',
                                    5 => 'за найкращими цінами.',
                                    6 => 'за найпривабливішою ціною.',
                                    7 => 'за найбільш привабливими цінами.'
                                ),

                            );
                        if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                            $arr_new = $arr_new_ua;
                        $text =array();
                        foreach ($arr_new as $key=>$val)
                        {
                            rand(rand(0,time()), time());
                            $text[] = $val[rand(0,count($val)-1)];
                        }
                        $cities =array(
                            'Мариуполю',
                            'Киеву',
                            'Николаеву',
                            'Херсону',
                            'Полтаве',
                            'Чернигову',
                            'Житомиру',
                            'Ровно',
                            'Черновцам',
                            'Тернополю',
                            'Белой Церкви',
                            'Луцку',
                            'Бердянску',
                            'Харькову',
                            'Одессе',
                            'Запорожью',
                            'Львову',
                            'Кривому Рогу',
                            'Виннице',
                            'Черкассам',
                            'Сумам',
                            'Хмельницкому',
                            'Кропивницкому',
                            'Днепродзержинску',
                            'Кременчугу',
                            'Ивано-Франковску',
                            'Мелитополю',
                            'Никополю',
                            'Ужгороду',
                            'Каменец-Подольскому',
                            'Днепропетровску',
                            'Славянску',
                            'Северодонецку',
                            'Павлограду',
                            'Лисичанску',
                            'Краматорску',
                            'Константиновке',
                            'Каменскому',
                            'Броварам',
                            'Александрии',
                            'Днепру'
                        );
                        $cities_ua =array(

                            'Маріуполю',
                            'Києву',
                            'Миколаєву',
                            'Херсону',
                            'Полтаві',
                            'Чернігову',
                            'Житомиру',
                            'Рівне',
                            'Чернівцям',
                            'Тернополю',
                            'Білій Церкві',
                            'Луцьку',
                            'Бердянську',
                            'Харкову',
                            'Одесі',
                            'Запоріжжю',
                            'Львову',
                            'Кривому Рогу',
                            'Вінниці',
                            'Черкасам',
                            'Сумам',
                            'Хмельницькому',
                            'Кропивницькому',
                            'Дніпродзержинську',
                            'Кременчуку',
                            'Івано-Франківську',
                            'Мелітополя',
                            'Нікополю',
                            'Ужгороду',
                            'Кам\'янець-Подільському',
                            'Дніпропетровську',
                            'Слов\'янську',
                            'Сєвєродонецьку',
                            'Павлограду',
                            'Лисичанську',
                            'Краматорську',
                            'Костянтинівці',
                            'Каменському',
                            'Броварів',
                            'Олександрії',
                            'Дніпру',

                        );
                        if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                            $cities = $cities_ua;
                        rand(rand(0,time()), time());
                        $val = $cities[rand(0,count($cities)-1)];
                        $city_val = $val;
                        $tags_arr = array(
                            'базу стола',
                            'барный стол',
                            'барная стойка',
                            'барные высокие стулья',
                            'барные деревянные стулья',
                            'барные стулья для кухни',
                            'барные складные стулья',
                            'барные столы',
                            'барные стулья',
                            'барный вращающийся стул',
                            'барный высокий стул',
                            'барный стул из дерева',
                            'барный стул дерево',
                            'барный стул мягкий',
                            'барный стул пластиковый',
                            'барный стул регулируемый',
                            'барный стул складной',
                            'барный стол',
                            'барный стул',
                            'белые стулья',
                            'белый стол',
                            'белый стул',
                            'верзалит столешницы',
                            'вращающиеся стулья',
                            'вращающийся барный стул',
                            'вращающийся стул',
                            'высокие стулья',
                            'высокий барный стул',
                            'группа для кухни',
                            'группы для кухни',
                            'деревянная опора',
                            'деревянная столешница',
                            'деревянную столешницу',
                            'деревянные барные стулья',
                            'деревянные журнальные столы',
                            'деревянные стулья для кухни',
                            'деревянные стулья на кухню',
                            'деревянные кухонные стулья',
                            'деревянные ножки',
                            'деревянные опоры',
                            'деревянные столешницы',
                            'деревянные столики',
                            'деревянные столы',
                            'деревянные стулья',
                            'деревянный барный стул',
                            'деревянный журнальный стол',
                            'деревянный стол',
                            'деревянный столик',
                            'деревянный стул',
                            'деревянные столешницы',
                            'деревянные стулья',
                            'журнальний столик',
                            'журнальные столы из дерева',
                            'журнальные столики',
                            'журнальные столы',
                            'журнальный стол из дерева',
                            'журнальный дерево',
                            'журнальный квадратный стол',
                            'журнальный стол на колесиках',
                            'журнальный стол лофт',
                            'журнальный стол массив',
                            'журнальный стол из массива',
                            'журнальный овальный стол',
                            'журнальный раскладной стол',
                            'журнальный стол стекло',
                            'журнальный стеклянный стол',
                            'журнальный стол',
                            'журнальный столик',
                            'журнальные столики',
                            'журнальные столы',
                            'итальянская столовая',
                            'итальянские стулья',
                            'каркас стола',
                            'квадратный стол на кухню',
                            'квадратный стол',
                            'китайские столы',
                            'классика обеденный стол',
                            'классическая мебель',
                            'классическая столовая',
                            'классические кухонные столы',
                            'классические столы',
                            'классические стулья',
                            'классический обеденный',
                            'классический стол',
                            'классической для кухни',
                            'кожаное кресло',
                            'кожаные кресла',
                            'комплект кухонный',
                            'комплект обеденной',
                            'комплект обеденный',
                            'комплект стол',
                            'комплекты для кухни',
                            'конференц стул',
                            'конференц стулья',
                            'кофейные столики',
                            'кофейный стол на колесиках',
                            'кофейный круглый стол',
                            'кофейный стол лофт',
                            'кофейный стол',
                            'кофейный столик',
                            'кресла вращающиеся',
                            'кресла италия',
                            'кресла китай',
                            'кресла кожа',
                            'кресла кожзам',
                            'кресла на колесиках',
                            'кресла для кухни',
                            'кресла для офиса',
                            'кресла офисные',
                            'кресла с подголовником',
                            'кресла для посетителей',
                            'кресла стулья',
                            'кресла тканевые',
                            'кресла ткань',
                            'кресла экокожа',
                            'кресло бежевое',
                            'кресло гостиная',
                            'кресло в гостиную',
                            'кресло заказ',
                            'кресло италия',
                            'кресло китай',
                            'кресло кожа',
                            'кресло на колесах',
                            'кресло на колесиках',
                            'кресло для кухни',
                            'кресло на кухню',
                            'кресло кухонное',
                            'кресло в офис',
                            'кресло для офиса',
                            'кресло офисное',
                            'кресло с подголовником',
                            'кресло стул',
                            'круглая столешница',
                            'круглое кресло',
                            'круглую столешницу',
                            'круглые столы',
                            'круглый деревянный стол',
                            'круглый журнальный столик',
                            'круглый стол',
                            'круглый столик',
                            'кухонная мебель',
                            'кухонное кресло',
                            'кухонную мебель',
                            'кухонные кресла',
                            'кухонные стулья с подлокотниками',
                            'кухонные столы',
                            'кухонные стулья',
                            'кухонный барный стул',
                            'кухонный квадратный стол',
                            'кухонный комплект',
                            'кухонный овальный стол',
                            'кухонный пластиковый стол',
                            'кухонный прямоугольный стол',
                            'кухонный раздвижной стол',
                            'кухонный складной стол',
                            'кухонный стол из стекла',
                            'кухонный стол стекло',
                            'кухонный стеклянный стол',
                            'кухонный стол',
                            'кухонный стул',
                            'кухонные столы',
                            'кухонные стулья',
                            'лофт ножки',
                            'малазийские столы',
                            'маленькие кресла',
                            'мебел для кухни',
                            'мебель италии',
                            'мебель италия',
                            'мебель китай',
                            'мебель китая',
                            'мебель для кухни',
                            'мебель на кухню',
                            'мебель кухня',
                            'мебель кухонная',
                            'мебель кухонную',
                            'мебель лофт',
                            'мебель для офиса',
                            'мебель офисная',
                            'мебель офисную',
                            'мебель столовая',
                            'мебель для столовой',
                            'мебель в столовую'
                        );
                        $tags_arr_ua = array(

                            'базу столу',
                            'барний стіл',
                            'Барна стійка',
                            'барні високі стільці',
                            'барні дерев\'яні стільці',
                            'барні стільці для кухні',
                            'барні складні стільці',
                            'барні столи',
                            'Барні стільці',
                            'барний стілець, що обертається',
                            'барний високий стілець',
                            'барний стілець з дерева',
                            'барний стілець дерево',
                            'барний стілець м\'який',
                            'барний стілець пластиковий',
                            'барний стілець регульований',
                            'барний стілець складаний',
                            'барний стіл',
                            'барний стілець',
                            'білі стільці',
                            'білий стіл',
                            'білий стілець',
                            'верзаліт стільниці',
                            'обертові стільці',
                            'обертається барний стілець',
                            'обертається стілець',
                            'високі стільці',
                            'високий барний стілець',
                            'група для кухні',
                            'групи для кухні',
                            'дерев\'яна опора',
                            'дерев\'яна стільниця',
                            'дерев\'яну стільницю',
                            'дерев\'яні барні стільці',
                            'дерев\'яні журнальні столи',
                            'дерев\'яні стільці для кухні',
                            'дерев\'яні стільці на кухню',
                            'дерев\'яні кухонні стільці',
                            'дерев\'яні ніжки',
                            'дерев\'яні опори',
                            'дерев\'яні стільниці',
                            'дерев\'яні столики',
                            'дерев\'яні столи',
                            'дерев\'яні стільці',
                            'дерев\'яний барний стілець',
                            'дерев\'яний журнальний стіл',
                            'дерев\'яний стіл',
                            'дерев\'яний столик',
                            'дерев\'яний стілець',
                            'дерев\'яні стільниці',
                            'дерев\'яні стільці',
                            'журнальний столик',
                            'журнальні столи з дерева',
                            'журнальні столики',
                            'журнальні столи',
                            'журнальний стіл з дерева',
                            'журнальний дерево',
                            'журнальний квадратний стіл',
                            'журнальний стіл на коліщатках',
                            'журнальний стіл лофт',
                            'журнальний стіл масив',
                            'журнальний стіл з масиву',
                            'журнальний овальний стіл',
                            'журнальний розкладний стіл',
                            'журнальний стіл скло',
                            'журнальний скляний стіл',
                            'журнальний стіл',
                            'журнальний столик',
                            'журнальні столики',
                            'журнальні столи',
                            'італійська їдальня',
                            'італійські стільці',
                            'каркас столу',
                            'квадратний стіл на кухню',
                            'квадратний стіл',
                            'китайські столи',
                            'класика обідній стіл',
                            'класичні меблі',
                            'класична їдальня',
                            'класичні кухонні столи',
                            'класичні столи',
                            'класичні стільці',
                            'класичний обідній',
                            'класичний стіл',
                            'класичної для кухні',
                            'шкіряне крісло',
                            'шкіряні крісла',
                            'комплект кухонний',
                            'комплект обідньої',
                            'комплект обідній',
                            'комплект стіл',
                            'комплекти для кухні',
                            'конференц стілець',
                            'конференц стільці',
                            'кавові столики',
                            'кавовий стіл на коліщатках',
                            'кавовий круглий стіл',
                            'кавовий стіл лофт',
                            'кавовий стіл',
                            'кавовий столик',
                            'крісла обертаються',
                            'крісла італія',
                            'крісла китай',
                            'крісла шкіра',
                            'крісла кожзам',
                            'крісла на коліщатках',
                            'крісла для кухні',
                            'крісла для офісу',
                            'крісла офісні',
                            'крісла з підголівником',
                            'крісла для відвідувачів',
                            'крісла стільці',
                            'крісла тканинні',
                            'крісла тканину',
                            'крісла екошкіра',
                            'крісло бежеве',
                            'крісло вітальня',
                            'крісло у вітальню',
                            'крісло замовлення',
                            'крісло італія',
                            'крісло китай',
                            'крісло шкіра',
                            'крісло на колесах',
                            'крісло на коліщатках',
                            'крісло для кухні',
                            'крісло на кухню',
                            'крісло кухонне',
                            'крісло в офіс',
                            'крісло для офісу',
                            'крісло офісне',
                            'крісло з підголовником',
                            'крісло стілець',
                            'кругла стільниця',
                            'кругле крісло',
                            'круглу стільницю',
                            'круглі столи',
                            'круглий дерев\'яний стіл',
                            'круглий журнальний столик',
                            'круглий стіл',
                            'круглий столик',
                            'кухонні меблі',
                            'кухонне крісло',
                            'кухонні меблі',
                            'кухонні крісла',
                            'кухонні стільці з підлокітниками',
                            'кухонні столи',
                            'кухонні стільці',
                            'кухонний барний стілець',
                            'кухонний квадратний стіл',
                            'кухонний комплект',
                            'кухонний овальний стіл',
                            'кухонний пластиковий стіл',
                            'кухонний прямокутний стіл',
                            'кухонний розсувний стіл',
                            'кухонний складаний стіл',
                            'кухонний стіл зі скла',
                            'кухонний стіл скло',
                            'кухонний скляний стіл',
                            'кухонний стіл',
                            'кухонний стілець',
                            'кухонні столи',
                            'кухонні стільці',
                            'лофт ніжки',
                            'малайзійські столи',
                            'маленькі крісла',
                            'мебел для кухні',
                            'меблі італії',
                            'меблі італія',
                            'меблі китай',
                            'меблі Китаю',
                            'меблі для кухні',
                            'меблі на кухню',
                            'меблі кухня',
                            'меблі для кухні',
                            'меблі кухонні',
                            'меблі лофт',
                            'меблі для офісу',
                            'меблі офісні',
                            'меблі офісну',
                            'меблі їдальня',
                            'меблі для їдальні',
                            'меблі в їдальню',

                        );
                        if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                            $tags_arr = $tags_arr_ua;
                        rand(rand(0,time()), time());
                        $val = $tags_arr[rand(0, count($tags_arr) - 1)];
                        $tags = $val;

                        $tags2_arr = array(
                            'барные деревянные стулья',
                            'барные складные стулья',
                            'барные столы для кухни',
                            'барные столы на кухню',
                            'барные стулья высокие',
                            'барные стулья из дерева',
                            'барные стулья дерево',
                            'барные стулья деревянные',
                            'барные стулья для дома',
                            'барные стулья домой',
                            'барные стулья для кафе',
                            'барные стулья кожзам',
                            'барные стулья из кожи',
                            'барные стулья для кухни',
                            'барные стулья мягкие',
                            'барные стулья онлайн',
                            'барные стулья пластиковые',
                            'барные стулья c подлокотниками',
                            'барные стулья регулируемые',
                            'барные стулья для ресторана',
                            'барные стулья для ресторанов',
                            'барные стулья складные',
                            'барные стулья тканевые',
                            'барные стулья экокожа',
                            'барный деревянный стул',
                            'барный стул со спинкой',
                            'барный стол для кухни',
                            'барный стол на кухню',
                            'барный стул высокий',
                            'барный стул из дерева',
                            'барный стул дерево',
                            'барный стул деревянный',
                            'барный стул для дома',
                            'барный стул для кафе',
                            'барный стул мягкий',
                            'барный стул низкий',
                            'барный стул пластик',
                            'барный стул с подлокотниками',
                            'барный стул раскладной',
                            'барный стул c регулировкой',
                            'барный стул регулируемый',
                            'барный стул складной',
                            'белые кухонные стулья',
                            'белые письменные столы',
                            'белые стулья для кухни',
                            'белые стулья на кухню',
                            'белый письменный стол',
                            'белый стол письменный',
                            'вращающийся барный стул',
                            'высокие барные стулья',
                            'высокие стулья барные',
                            'высокий барный стул',
                            'группы для кухни классика',
                            'деревянная ножка стола',
                            'деревянная опора стола',
                            'деревянная столешница на кухню',
                            'деревянная столешница стола',
                            'деревянную столешницу стола',
                            'деревянные барные стулья',
                            'деревянные журнальные столики',
                            'деревянные кухонные столы',
                            'деревянные кухонные стулья',
                            'деревянные ножки стола',
                            'деревянные обеденные столы',
                            'деревянные опоры стола',
                            'деревянные письменные столы',
                            'деревянные складные стулья',
                            'деревянные столешницы столов',
                            'деревянные столы для кухни',
                            'деревянные столы на кухню',
                            'деревянные столы кухонные',
                            'деревянные стулья для бара',
                            'деревянные стулья для кухни',
                            'деревянные стулья на кухню',
                            'деревянный барный стул',
                            'деревянный журнальный стол',
                            'деревянный журнальный столик',
                            'деревянный кофейный столик',
                            'деревянный кухонный стол',
                            'деревянный кухонный стул',
                            'деревянный обеденный стол',
                            'деревянный письменный стол',
                            'деревянный стол для кухни',
                            'деревянный стол на кухню',
                            'деревянный стол кухонный',
                            'деревянный стол обеденный',
                            'деревянный стул для кухни',
                            'журнальний столик из дерева',
                            'журнальний столик на колесах',
                            'журнальные стеклянные столики',
                            'журнальные столики из дерева',
                            'журнальные столики деревянные',
                            'журнальные столики на колесиках',
                            'журнальные столики лофт',
                            'журнальные столики из массива',
                            'журнальные столики раскладные',
                            'журнальные столики из стекла',
                            'журнальные столики стекло',
                            'журнальные столики стеклянные',
                            'журнальный деревянный столик',
                            'журнальный кофейный столик',
                            'журнальный круглый столик',
                            'журнальный стол из массива дерева',
                            'журнальный раскладной стол',
                            'журнальный стол со стеклом',
                            'журнальный стеклянный столик',
                            'журнальный стол из дерева',
                            'журнальный стол на колесах',
                            'журнальный стол на колесиках',
                            'журнальный стол лофт',
                            'журнальный стол раскладной',
                            'журнальный стол складной',
                            'журнальный стол современный',
                            'журнальный стол из стекла',
                            'журнальный столик из дерева',
                            'журнальный столик дерево',
                            'журнальный столик деревянный',
                            'журнальный столик квадратный',
                            'журнальный столик на колесах',
                            'журнальный столик на колесиках',
                            'журнальный столик кривой',
                            'журнальный столик круглый',
                            'журнальный столик лофт',
                            'журнальный столик массив',
                            'журнальный столик из массива',
                            'журнальный столик овальный',
                            'журнальный столик прямоугольный',
                            'журнальный столик раскладной',
                            'журнальный столик раскладывающийся',
                            'журнальный столик складной',
                            'журнальный столик в современном стиле',
                            'журнальный столик современный',
                            'журнальный столик из стекла',
                            'журнальный столик стекло',
                            'журнальный столик стеклянный',
                            'итальянская столовая мебель',
                            'итальянские стулья для кухни',
                            'квадратный журнальный столик',
                            'квадратный обеденный стол',
                            'квадратный стол для кухни',
                            'китайские столы и стулья',
                            'классическая мебель для столовой',
                            'классическая столовая мебель',
                            'классические кухонные столы',
                            'классические столы и стулья',
                            'классические стулья для кухни',
                            'классический обеденный стол',
                            'классический стол на кухню',
                            'кожаное кресло офисное',
                            'кожаные офисные кресла',
                            'кожаный офисный стул',
                            'комплект для кухни стол и стул',
                            'комплект кухонный стол и стул',
                            'комплект обеденной мебели',
                            'комплект обеденный стол',
                            'комплект стол и стулья',
                            'кофейный стол лофт',
                            'кофейный столик из дерева',
                            'кофейный столик на колесах',
                            'кофейный столик на колесиках',
                            'кофейный столик круглый',
                            'кофейный столик лофт',
                            'кофейный столик стеклянный',
                            'кресла натуральной кожи',
                            'кресло купить онлайн',
                            'кресло купить офисное',
                            'кресло офисное италия',
                            'кресло офисное кожа',
                            'кресло офисное кожаное',
                            'кресло офисное с подголовником',
                            'кресло стул для кухни',
                            'круглая столешница из дерева',
                            'круглая столешница стола',
                            'круглую столешницу стола',
                            'круглые кухонные столы',
                            'круглые обеденные столы',
                            'круглые столы для кухни',
                            'круглые столы на кухню',
                            'круглые столы обеденные',
                            'круглые столы и стулья',
                            'круглый деревянный журнальный стол',
                            'круглый журнальный столик',
                            'круглый кофейный столик',
                            'круглый кухонный стол',
                            'круглый обеденный стол',
                            'круглый прикроватный столик',
                            'круглый стол со стульями',
                            'круглый стеклянный стол',
                            'круглый стол для кухни',
                            'круглый стол на кухню',
                            'круглый стол кухонный',
                            'круглый стол обеденный',
                            'круглый стол и стулья',
                            'кухонная обеденная группа',
                            'кухонные деревянные столы',
                            'кухонные деревянные стулья',
                            'кухонные круглые столы',
                            'кухонные металлические стулья',
                            'кухонные обеденные столы',
                            'кухонные овальные столы',
                            'кухонные пластиковые столы',
                            'кухонные раскладные столы',
                            'кухонные складные стулья',
                            'кухонные стеклянные столы',
                            'кухонные столы из дерева',
                            'кухонные столы дерево',
                            'кухонные столы деревянные',
                            'кухонные столы круглые',
                            'кухонные столы лофт',
                            'кухонные столы из массива',
                            'кухонные столы обеденные',
                            'кухонные столы овальные',
                            'кухонные столы раздвижные',
                            'кухонные столы раскладные',
                            'кухонные столы стекло',
                            'кухонные столы стеклянные',
                            'кухонные столы и стулья',
                            'кухонные стулья белые',
                            'кухонные стулья деревянные',
                            'кухонные стулья китай',
                            'кухонные стулья металлические',
                            'кухонные стулья пластиковые',
                            'кухонные стулья с подлокотниками',
                            'кухонные стулья регулируемые',
                            'кухонные стулья складные',
                            'кухонный барный стул и стол',
                            'кухонный деревянный стол',
                            'кухонный комплект стол и стул',
                            'кухонный круглый стол',
                            'кухонный стол из массива дерева',
                            'кухонный набор стол и стул',
                            'кухонный обеденный стол',
                            'кухонный овальный стол',
                            'кухонный раздвижной стол',
                            'кухонный раскладной стол',
                            'кухонный стол со стеклом',
                            'кухонный стеклянный стол',
                            'кухонный стол из дерева',
                            'кухонный стол дерево',
                            'кухонный стол деревянный',
                            'кухонный стол классика',
                            'кухонный стол круглый',
                            'кухонный стол лофт',
                            'кухонный стол из массива',
                            'кухонный стол овальный',
                            'кухонный стол пластиковой',
                            'кухонный стол раздвижной',
                            'кухонный стол раскладной',
                            'кухонный стол с регулируемой ножкой',
                            'кухонный стол из стекла ',
                            'кухонный стол стекло',
                            'кухонный стол стеклянный',
                            'кухонный стол и стулья',
                            'кухонный стол с хромированными ножками',
                            'лофт журнальный столик',
                            'лофт ножки стола',
                            'лофт письменный стол',
                            'малазийские столы и стулья',
                            'маленькие кресла для кухни',
                            'мебель кухонный стол',
                            'мебель для офиса лофт',
                            'мебель стиле лофт',
                            'мебель для столовой италия',
                            'мебель для столовой китай',
                            'мебель для столовой комнаты',
                            'мебель для столовой в современном стиле',
                            'мебель в столовую комнату',
                            'металлические кухонные стулья',
                            'металлические стулья для кухни',
                            'металлические стулья на кухню',
                            'мягкие барные стулья',
                            'мягкие обеденные стулья',
                            'мягкие стулья для кухни',
                            'мягкие стулья на кухню',
                            'мягкие стулья для столовой',
                            'мягкий барный стул',
                            'набор стол и стулья',
                            'низкие барные стулья',
                            'низкий барный стул',
                            'нога барного стола',
                            'ноги стола лофт',
                            'ножка барного стола',
                            'ножка стола деревянная',
                            'ножка стола чугунная',
                            'ножки барного стола',
                            'ножки барные столов',
                            'ножки из дерева стола',
                            'ножки деревянного стола',
                            'ножки деревянные стола',
                            'ножки до стола',
                            'ножки журнального столика',
                            'ножки кухонного стола',
                            'ножки лофт стола',
                            'ножки опоры стола',
                            'ножки офисного стола',
                            'ножки под столешницу',
                            'ножки в стиле лофт',
                            'ножки для стола из дерева',
                            'ножки для стола дерево',
                            'ножки стола деревянные',
                            'ножки стола лофт',
                            'ножки стола в стиле лофт',
                            'ножки стола хромированные',
                            'ножки хромированные стола',
                            'обеденная группа китай',
                            'обеденная группа классика',
                            'обеденная группа для кухни',
                            'обеденная группа малайзия',
                            'обеденную группу классика',
                            'обеденную группу на кухню',
                            'обеденные группы классика',
                            'обеденные группы для кухни',
                            'обеденные группы малайзии',
                            'обеденные группы малайзия',
                            'обеденные комплекты для кухни',
                            'обеденные комплекты малайзия',
                            'обеденные круглые столы',
                            'обеденные кухонные столы',
                            'обеденные овальные столы',
                            'обеденные раскладные столы',
                            'обеденные стеклянные столы',
                            'обеденные столы из дерева',
                            'обеденные столы деревянные',
                            'обеденные столы китай',
                            'обеденные столы классика',
                            'обеденные столы в классическом стиле',
                            'обеденные столы круглые',
                            'обеденные столы для кухни',
                            'обеденные столы на кухню',
                            'обеденные столы лофт',
                            'обеденные столы малайзии',
                            'обеденные столы малайзия',
                            'обеденные столы из массива',
                            'обеденные столы овальные',
                            'обеденные столы раздвижные',
                            'обеденные столы раскладные',
                            'обеденные столы из стекла',
                            'обеденные столы стекло',
                            'обеденные столы стеклянные',
                            'обеденные столы и стулья',
                            'обеденный деревянный стол',
                            'обеденный комплект для кухни',
                            'обеденный круглый стол',
                            'обеденный овальный стол',
                            'обеденный раздвижной стол',
                            'обеденный раскладной стол',
                            'обеденный стол со стеклом',
                            'обеденный  стол со стульями',
                            'обеденный стеклянный стол',
                            'обеденный стол в стиле лофт',
                            'обеденный стол из дерева',
                            'обеденный стол дерево',
                            'обеденный стол деревянный',
                            'обеденный стол китай',
                            'обеденный стол классика',
                            'обеденный стол в классическом стиле',
                            'обеденный стол круглый',
                            'обеденный стол для кухни',
                            'обеденный стол на кухню',
                            'обеденный стол лофт',
                            'обеденный стол малайзия',
                            'обеденный стол массив',
                            'обеденный стол из массива',
                            'обеденный стол овальный',
                            'обеденный стол прямоугольный',
                            'обеденный стол раздвижной',
                            'обеденный стол раскладной',
                            'обеденный стол регулируемый',
                            'обеденный стол из стекла',
                            'обеденный стол стекло',
                            'обеденный стол стеклянный',
                            'обеденный стол и стулья',
                            'овальная столешница стола',
                            'овальные журнальные столики',
                            'овальные кухонные столы',
                            'овальные обеденные столы',
                            'овальные столы для кухни',
                            'овальный журнальный столик',
                            'овальный кухонный стол',
                            'овальный обеденный стол',
                            'овальный стол на кухне',
                            'овальный стол для кухни',
                            'овальный стол на кухню',
                            'овальный стол обеденный',
                            'опора барного стола',
                            'опора нога стола',
                            'опора под столешницу',
                            'опора стола деревянная',
                            'опора стола лофт',
                            'опоры барные столов',
                            'опоры офисных столов',
                            'опоры столов лофт',
                            'основа стола лофт',
                            'офисная мебель италии',
                            'офисная мебель италия',
                            'офисная мебель китай',
                            'офисная мебель китая',
                            'офисная мебель лофт',
                            'офисное кресло бежевое',
                            'офисное кресло италия',
                            'офисное кресло китай',
                            'офисное кресло кожа',
                            'офисное кресло на колесах',
                            'офисное кресло на колесиках',
                            'офисное кресло с подголовником',
                            'офисные кожаные кресла',
                            'офисные кресла вращающиеся',
                            'офисные кресла италия',
                            'офисные кресла китай',
                            'офисные кресла кожа',
                            'офисные кресла кожзам',
                            'офисные кресла на колесиках',
                            'офисные кресла из натуральной кожи',
                            'офисные кресла с подголовником',
                            'офисные кресла для посетителей',
                            'офисные кресла стулья',
                            'офисные кресла тканевые',
                            'офисные кресла ткань',
                            'офисные кресла экокожа',
                            'офисные столы лофт',
                            'офисные столы современные',
                            'офисные стулья на колесах',
                            'офисные стулья с подлокотниками',
                            'офисные стулья для посетителей',
                            'офисные стулья сетка',
                            'офисные стулья экокожа',
                            'офисный стол лофт',
                            'офисный стул на колесах',
                            'офисный стул на колесиках',
                            'офисный стул с подлокотниками',
                            'офисный стул сетка',
                            'офисный стул с сеткой',
                            'письменные столы белые',
                            'письменные столы из дерева',
                            'письменные столы дерево',
                            'письменные столы лофт',
                            'письменные столы с полками',
                            'письменные столы с ящиками',
                            'письменный белый стол',
                            'письменный стол белый',
                            'письменный стол выдвижными',
                            'письменный стол из дерева',
                            'письменный стол дерево',
                            'письменный стол деревянный',
                            'письменный стол китай',
                            'письменный стол лофт',
                            'письменный стол металлический',
                            'письменный стол с полками',
                            'письменный стол с полкой',
                            'письменный стол современный',
                            'письменный стол с ящиками',
                            'письменный стол с ящиком',
                            'пластиковые барные стулья',
                            'пластиковые кухонные столы',
                            'пластиковые стулья для кухни',
                            'пластиковые стулья на кухню',
                            'пластиковые стулья для офиса',
                            'пластиковый барный стул',
                            'подстолье стола лофт',
                            'подстолья офисные столов',
                            'полубарные стулья высота',
                            'полукруглый стол для кухни',
                            'придиванный столик лофт',
                            'прикроватный столик круглый',
                            'прикроватный столик лофт',
                            'прикроватный столик складной',
                            'прямоугольный журнальный столик',
                            'прямоугольный обеденный стол',
                            'раздвижной кухонный стол',
                            'раздвижной обеденный стол',
                            'раздвижной стол для кухни',
                            'раздвижной стол на кухню',
                            'раздвижной стол кухонный',
                            'раздвижной стол обеденный',
                            'раздвижные кухонные столы',
                            'раздвижные обеденные столы',
                            'раздвижные столы для кухни',
                            'раздвижные столы на кухню',
                            'раздвижные столы кухонные',
                            'раскладной журнальный столик',
                            'раскладной кухонный стол',
                            'раскладной обеденный стол',
                            'раскладной стол журнальный',
                            'раскладной стол для кухни',
                            'раскладной стол на кухню',
                            'раскладной стол кухонный',
                            'раскладной стол обеденный',
                            'раскладной столик на кухню',
                            'раскладной стул для кухни',
                            'раскладные журнальные столики',
                            'раскладные журнальные столы',
                            'раскладные кухонные столы',
                            'раскладные обеденные столы',
                            'раскладные столы для кухни',
                            'раскладные столы кухонные',
                            'раскладные столы обеденные',
                            'раскладывающиеся столы для кухни',
                            'раскладывающийся журнальный столик',
                            'регулируемые барные стулья',
                            'регулируемый барный стул',
                            'складной барный стул',
                            'складной журнальный стол',
                            'складной журнальный столик',
                            'складной кофейный столик',
                            'складной кухонный стол',
                            'складной стол для кухни',
                            'складной стол на кухню',
                            'складной стол обеденный',
                            'складной столик на кухню',
                            'складной барный стул',
                            'складной стул для кухни',
                            'складной стул на кухню',
                            'складные барные стулья',
                            'складные журнальные столики',
                            'складные кухонные стулья',
                            'складные обеденные столы',
                            'складные столы для кухни',
                            'складные стулья для кухни',
                            'складные стулья на кухню',
                            'складные табуретки для кухни',
                            'складывающиеся кухонные столы',
                            'современная мебель для офиса',
                            'современная мебель для столовой',
                            'современная офисная мебель',
                            'современная столовая мебель',
                            'современные журнальные столики',
                            'современные кухонные столы',
                            'современные кухонные стулья',
                            'современные ножки стола',
                            'современные обеденные столы',
                            'современные офисные столы',
                            'современные письменные столы',
                            'современные столы для кухни',
                            'современные стулья для кухни',
                            'современный журнальный столик',
                            'современный кухонный стол',
                            'современный обеденный стол',
                            'современный офисный стол',
                            'современный письменный стол',
                            'современный стол на кухню',
                            'стеклянные журнальные столики',
                            'стеклянные кухонные столы',
                            'стеклянные обеденные столы',
                            'стеклянные раскладные столы',
                            'стеклянные столики журнальные',
                            'стеклянные столы журнальные',
                            'стеклянные столы для кухни',
                            'стеклянные столы на кухню',
                            'стеклянные столы кухонные',
                            'стеклянный журнальный стол',
                            'стеклянный журнальный столик',
                            'стеклянный кофейный столик',
                            'стеклянный кухонный стол',
                            'стеклянный обеденный стол',
                            'стеклянный прикроватный столик',
                            'стеклянный стол журнальный',
                            'стеклянный стол для кухни',
                            'стеклянный стол на кухню',
                            'стеклянный стол кухонный',
                            'стеклянный стол обеденный',
                            'стеклянный столик журнальный',
                            'стол белого цвета',
                            'стол белый письменный',
                            'стол с выдвижными ящиками',
                            'стол из дерева на кухню',
                            'стол деревянный для кухни',
                            'стол деревянный на кухню',
                            'стол деревянный кухонный',
                            'стол деревянный обеденный',
                            'стол деревянный письменный',
                            'стол журнальный из дерева',
                            'стол журнальный дерево',
                            'стол журнальный деревянный',
                            'стол журнальный квадратный',
                            'стол журнальный кофейный',
                            'стол журнальный лофт',
                            'стол журнальный массив',
                            'стол журнальный из массива',
                            'стол журнальный овальный',
                            'стол журнальный раскладной',
                            'стол журнальный стекло',
                            'стол журнальный стеклянный',
                            'стол с изменяемой высотой',
                            'стол квадратный на кухню',
                            'стол классика обеденный',
                            'стол в классическом стиле',
                            'стол кофейный круглый',
                            'стол кофейный лофт',
                            'стол круглый журнальный',
                            'стол круглый для кухни',
                            'стол круглый на кухню',
                            'стол круглый кухонный',
                            'стол круглый обеденный',
                            'стол круглый и стулья',
                            'стол для кухни из дерева',
                            'стол для кухни деревянный',
                            'стол для кухни круглый',
                            'стол для кухни лофт',
                            'стол для кухни из массива',
                            'стол для кухни овальный',
                            'стол для кухни пластик',
                            'стол для кухни раздвижной',
                            'стол для кухни раскладной',
                            'стол для кухни стеклянный',
                            'стол на кухню из дерева',
                            'стол на кухню дерево',
                            'стол на кухню деревянный',
                            'стол на кухню круглый',
                            'стол на кухню лофт',
                            'стол на кухню овальный',
                            'стол на кухню раздвижной',
                            'стол на кухню раскладной',
                            'стол на кухню стекло',
                            'стол на кухню стеклянный',
                            'стол кухонный из дерева',
                            'стол кухонный дерево',
                            'стол кухонный деревянный',
                            'стол кухонный квадратный',
                            'стол кухонный классика',
                            'стол кухонный круглый',
                            'стол кухонный лофт',
                            'стол кухонный массив',
                            'стол кухонный из массива',
                            'стол кухонный обеденный',
                            'стол кухонный овальный',
                            'стол кухонный пластиковый',
                            'стол кухонный прямоугольный',
                            'стол кухонный раздвижной',
                            'стол кухонный раскладной',
                            'стол кухонный современный',
                            'стол кухонный из стекла',
                            'стол кухонный стекло',
                            'стол кухонный стеклянный',
                            'стол лофт обеденный',
                            'стол лофт письменный',
                            'стол из массива дерева',
                            'стол из массива обеденный',
                            'стол на металлическом каркасе',
                            'стол обеденный из дерева',
                            'стол обеденный дерево',
                            'стол обеденный деревянный',
                            'стол обеденный квадратный',
                            'стол обеденный классика',
                            'стол обеденный классический',
                            'стол обеденный круглый',
                            'стол обеденный для кухни',
                            'стол обеденный на кухню',
                            'стол обеденный кухонный',
                            'стол обеденный лофт',
                            'стол обеденный малайзия',
                            'стол обеденный массив',
                            'стол обеденный из массива',
                            'стол обеденный овальный',
                            'стол обеденный пластиковый',
                            'стол обеденный прямоугольный',
                            'стол обеденный раздвижной',
                            'стол обеденный раскладной',
                            'стол обеденный современный',
                            'стол обеденный из стекла',
                            'стол обеденный стекло',
                            'стол обеденный стеклянный',
                            'стол обеденный и стулья',
                            'стол овальный для кухни',
                            'стол овальный на кухню',
                            'стол овальный кухонный',
                            'стол овальный обеденный',
                            'стол на одной ножке',
                            'стол письменный белый',
                            'стол письменный из дерева',
                            'стол письменный дерево',
                            'стол письменный деревянный',
                            'стол письменный лофт',
                            'стол письменный с полками',
                            'стол письменный с полкой',
                            'стол письменный современный',
                            'стол письменный с ящиками',
                            'стол с пластиковой столешницей',
                            'стол с пластиковым покрытием',
                            'стол производства малайзия',
                            'стол раздвижной для кухни',
                            'стол раздвижной на кухню',
                            'стол раздвижной кухонный',
                            'стол раздвижной обеденный',
                            'стол раскладной журнальный',
                            'стол раскладной для кухни',
                            'стол раскладной на кухню',
                            'стол раскладной кухонный',
                            'стол раскладной обеденный',
                            'стол с регулируемой высотой',
                            'стол регулируемый по высоте',
                            'стол со стеклом',
                            'стол со стульями',
                            'стол из стекла для кухни',
                            'стол стеклянный журнальный',
                            'стол стеклянный для кухни',
                            'стол стеклянный на кухню',
                            'стол стеклянный кухонный',
                            'стол стеклянный обеденный',
                            'стол стеклянный овальный',
                            'стол стиле лофт',
                            'стол и стулья для кухни',
                            'стол и стулья на кухню',
                            'стол и стулья малайзия',
                            'стол и стулья набор',
                            'стол и стулья обеденные',
                            'стол с хромированными ножками',
                            'столешница из дерева стола',
                            'столешница круглая для стола',
                            'столешница овальная для стола',
                            'столешница под дерево',
                            'столешница для стола верзалит',
                            'столешница стола из дерева',
                            'столешницу из дерева стола',
                            'столешницу под дерево',
                            'столик журнальный из дерева',
                            'столик журнальный деревянный',
                            'столик журнальный на колесиках',
                            'столик журнальный круглый',
                            'столик журнальный стекло',
                            'столик журнальный стеклянный',
                            'столик кофейный на колесиках',
                            'столик кофейный круглый',
                            'столик из массива дерева',
                            'столик прикроватный круглый',
                            'столик со стеклом',
                            'столик в современном стиле',
                            'столик стеклянный журнальный',
                            'столик в стиле лофт',
                            'столики журнальные стеклянные',
                            'столики со стеклом',
                            'столики стеклянные журнальные',
                            'столики в стиле лофт',
                            'столовая в классическом стиле',
                            'столовая мебель италия',
                            'столовая мебель китай',
                            'столовая мебель китая',
                            'столы барные для кухни',
                            'столы деревянные для кухни',
                            'столы деревянные кухонные',
                            'столы деревянные обеденные',
                            'столы журнальные из дерева',
                            'столы журнальные стеклянные',
                            'столы в классическом стиле',
                            'столы круглые для кухни',
                            'столы круглые кухонные',
                            'столы круглые обеденные',
                            'столы для кухни деревянные',
                            'столы для кухни китай',
                            'столы для кухни классика',
                            'столы для кухни круглые',
                            'столы для кухни овальные',
                            'столы для кухни раскладные',
                            'столы для кухни из стекла',
                            'столы для кухни стеклянные',
                            'столы на кухню круглые',
                            'столы кухонные дерево',
                            'столы кухонные деревянные',
                            'столы кухонные круглые',
                            'столы кухонные из массива',
                            'столы кухонные раздвижные',
                            'столы кухонные раскладные',
                            'столы кухонные стеклянные',
                            'столы массива из дерева',
                            'столы обеденные из дерева',
                            'столы обеденные деревянные',
                            'столы обеденные квадратные',
                            'столы обеденные круглые',
                            'столы обеденные лофт',
                            'столы обеденные малайзия',
                            'столы обеденные из массива',
                            'столы обеденные овальные',
                            'столы обеденные прямоугольные',
                            'столы обеденные раздвижные',
                            'столы обеденные раскладные',
                            'столы обеденные стекло',
                            'столы обеденные стеклянные',
                            'столы овальные для кухни',
                            'столы письменные белые',
                            'столы письменные с полками',
                            'столы раздвижные для кухни',
                            'столы раздвижные кухонные',
                            'столы раздвижные обеденные',
                            'столы раскладные для кухни',
                            'столы раскладные кухонные',
                            'столы раскладные обеденные',
                            'столы со стеклом',
                            'столы из стекла для кухни',
                            'столы стекло для кухни',
                            'столы стеклянные для кухни',
                            'столы стеклянные на кухню',
                            'столы стеклянные кухонные',
                            'столы стеклянные обеденные',
                            'столы в стиле лофт',
                            'столы и стулья китай',
                            'столы и стулья китая',
                            'столы и стулья классика',
                            'столы и стулья для кухни',
                            'столы и стулья малайзии',
                            'столы и стулья малайзия',
                            'столы и стулья обеденные',
                            'стул для барной стойки',
                            'стул барный вращающийся',
                            'стул барный высокий',
                            'стул барный из дерева',
                            'стул барный дерево',
                            'стул барный деревянный',
                            'стул барный мягкий',
                            'стул барный пластиковый',
                            'стул барный регулируемый',
                            'стул барный складной',
                            'стул без спинки',
                            'стул белый кухонный',
                            'стул высокий барный',
                            'стул деревянный барный',
                            'стул деревянный кухонный',
                            'стул кожаный офисный',
                            'стул кресло для кухни',
                            'стул кресло на кухню',
                            'стул купить деревянный',
                            'стул купить офисный',
                            'стул кухонный белый',
                            'стул кухонный дерево',
                            'стул кухонный деревянный',
                            'стул кухонный раскладной',
                            'стул кухонный складной',
                            'стул металлической основе',
                            'стул обеденный белый',
                            'стул обеденный деревянный',
                            'стул обеденный металлический',
                            'стул обеденный с подлокотниками',
                            'стул офисный кожаный',
                            'стул офисный кожзам',
                            'стул офисный на колесиках',
                            'стул офисный сетка',
                            'стул офисный с сеткой',
                            'стул офисный спинка',
                            'стул с регулировкой высоты',
                            'стул с регулируемой высотой',
                            'стул регулируемый по высоте',
                            'стул складной барный',
                            'стул складной кухонный',
                            'стул со спинкой',
                            'стулья для актового зала',
                            'стулья для барной стойки',
                            'стулья барные высокие',
                            'стулья барные деревянные',
                            'стулья барные для дома',
                            'стулья для барные стоек',
                            'стулья для баров и кафе',
                            'стулья для баров и ресторанов',
                            'стулья без спинки',
                            'стулья белые для кухни',
                            'стулья белые на кухню',
                            'стулья высокие барные',
                            'стулья деревянные барные',
                            'стулья деревянные для кухни',
                            'стулья деревянные на кухню',
                            'стулья деревянные кухонные',
                            'стулья для кафе и баров',
                            'стулья для кафе и ресторанов',
                            'стулья классические для кухни',
                            'стулья кожзама для кухни',
                            'стулья конференц зал',
                            'стулья конференц зала',
                            'стулья конференц залов',
                            'стулья для кухни белые',
                            'стулья для кухни из дерева',
                            'стулья для кухни дерево',
                            'стулья для кухни деревянные',
                            'стулья для кухни италия',
                            'стулья для кухни китай',
                            'стулья для кухни классика',
                            'стулья для кухни кожзам',
                            'стулья для кухни лофт',
                            'стулья для кухни малайзия',
                            'стулья для кухни металлические',
                            'стулья для кухни мягкие',
                            'стулья для кухни пластик',
                            'стулья для кухни с подлокотниками',
                            'стулья для кухни складные',
                            'стулья для кухни для столовой',
                            'стулья для кухни черные',
                            'стулья на кухню белые',
                            'стулья на кухню деревянные',
                            'стулья на кухню металлические',
                            'стулья кухонные белые',
                            'стулья кухонные деревянные',
                            'стулья кухонные классика',
                            'стулья кухонные классические',
                            'стулья кухонные кожзам',
                            'стулья кухонные малайзия',
                            'стулья кухонные металлические',
                            'стулья кухонные пластиковые',
                            'стулья кухонные с подлокотниками',
                            'стулья металлические для кухни',
                            'стулья с металлическим каркасом',
                            'стулья с металлическими ножками',
                            'стулья на металлических ножках',
                            'стулья мягкие для кухни',
                            'стулья обеденные группы',
                            'стулья обеденные деревянные',
                            'стулья офисные на колесиках',
                            'стулья офисные для посетителей',
                            'стулья из пластика для кухни',
                            'стулья пластиковые для кухни',
                            'стулья с подлокотниками для кухни',
                            'стулья с подлокотниками полукресло',
                            'стулья регулируемые по высоте',
                            'стулья для ресторана и кафе',
                            'стулья для ресторанов и кафе',
                            'стулья складные для кухни',
                            'стулья со спинкой',
                            'стулья и столы для кухни',
                            'тканевые офисные стулья',
                            'хромированная нога стола',
                            'хромированные ножки стола',
                            'черные барные стулья',
                            'черные стулья на кухню',
                            'чугунная опора стола',
                            'чугунные ножки стола'
                        );
                        $tags2_arr_ua = array(


                            'барні дерев\'яні стільці',
                            'барні складні стільці',
                            'барні столи для кухні',
                            'барні столи на кухню',
                            'барні стільці високі',
                            'барні стільці з дерева',
                            'барні стільці дерево',
                            'барні стільці дерев\'яні',
                            'барні стільці для будинку',
                            'барні стільці додому',
                            'барні стільці для кафе',
                            'барні стільці кожзам',
                            'барні стільці зі шкіри',
                            'барні стільці для кухні',
                            'барні стільці м\'які',
                            'барні стільці онлайн',
                            'барні стільці пластикові',
                            'барні стільці c підлокітниками',
                            'барні стільці регульовані',
                            'барні стільці для ресторану',
                            'барні стільці для ресторанів',
                            'барні стільці складні',
                            'барні стільці тканинні',
                            'барні стільці екошкіра',
                            'барний дерев\'яний стілець',
                            'барний стілець зі спинкою',
                            'барний стіл для кухні',
                            'барний стіл на кухню',
                            'барний стілець високий',
                            'барний стілець з дерева',
                            'барний стілець дерево',
                            'барний стілець дерев\'яний',
                            'барний стілець для дому',
                            'барний стілець для кафе',
                            'барний стілець м\'який',
                            'барний стілець низький',
                            'барний стілець пластик',
                            'барний стілець з підлокітниками',
                            'барний стілець розкладний',
                            'барний стілець c регулюванням',
                            'барний стілець регульований',
                            'барний стілець складаний',
                            'білі кухонні стільці',
                            'білі письмові столи',
                            'білі стільці для кухні',
                            'білі стільці на кухню',
                            'білий письмовий стіл',
                            'білий стіл письмовий',
                            'обертається барний стілець',
                            'високі барні стільці',
                            'високі стільці барні',
                            'високий барний стілець',
                            'групи для кухні класика',
                            'дерев\'яна ніжка столу',
                            'дерев\'яна опора столу',
                            'дерев\'яна стільниця на кухню',
                            'дерев\'яна стільниця столу',
                            'дерев\'яну стільницю столу',
                            'дерев\'яні барні стільці',
                            'дерев\'яні журнальні столики',
                            'дерев\'яні кухонні столи',
                            'дерев\'яні кухонні стільці',
                            'дерев\'яні ніжки столу',
                            'дерев\'яні обідні столи',
                            'дерев\'яні опори столу',
                            'дерев\'яні письмові столи',
                            'дерев\'яні складні стільці',
                            'дерев\'яні стільниці столів',
                            'дерев\'яні столи для кухні',
                            'дерев\'яні столи на кухню',
                            'дерев\'яні столи кухонні',
                            'дерев\'яні стільці для бару',
                            'дерев\'яні стільці для кухні',
                            'дерев\'яні стільці на кухню',
                            'дерев\'яний барний стілець',
                            'дерев\'яний журнальний стіл',
                            'дерев\'яний журнальний столик',
                            'дерев\'яний кавовий столик',
                            'дерев\'яний кухонний стіл',
                            'дерев\'яний кухонний стілець',
                            'дерев\'яний обідній стіл',
                            'дерев\'яний письмовий стіл',
                            'дерев\'яний стіл для кухні',
                            'дерев\'яний стіл на кухню',
                            'дерев\'яний стіл кухонний',
                            'дерев\'яний стіл обідній',
                            'дерев\'яний стілець для кухні',
                            'журнальний столик з дерева',
                            'журнальний столик на колесах',
                            'журнальні скляні столики',
                            'журнальні столики з дерева',
                            'журнальні столики дерев\'яні',
                            'журнальні столики на коліщатках',
                            'журнальні столики лофт',
                            'журнальні столики з масиву',
                            'журнальні столики розкладні',
                            'журнальні столики зі скла',
                            'журнальні столики скло',
                            'журнальні столики скляні',
                            'журнальний дерев\'яний столик',
                            'журнальний кавовий столик',
                            'журнальний круглий столик',
                            'журнальний стіл з масиву дерева',
                            'журнальний розкладний стіл',
                            'журнальний стіл зі склом',
                            'журнальний скляний столик',
                            'журнальний стіл з дерева',
                            'журнальний стіл на колесах',
                            'журнальний стіл на коліщатках',
                            'журнальний стіл лофт',
                            'журнальний стіл розкладний',
                            'журнальний стіл складаний',
                            'журнальний стіл сучасний',
                            'журнальний стіл зі скла',
                            'журнальний столик з дерева',
                            'журнальний столик дерево',
                            'журнальний столик дерев\'яний',
                            'журнальний столик квадратний',
                            'журнальний столик на колесах',
                            'журнальний столик на коліщатках',
                            'журнальний столик кривої',
                            'журнальний столик круглий',
                            'журнальний столик лофт',
                            'журнальний столик масив',
                            'журнальний столик з масиву',
                            'журнальний столик овальний',
                            'журнальний столик прямокутний',
                            'журнальний столик розкладний',
                            'журнальний столик розкладається',
                            'журнальний столик складаний',
                            'журнальний столик в сучасному стилі',
                            'журнальний столик сучасний',
                            'журнальний столик зі скла',
                            'журнальний столик скло',
                            'журнальний столик скляний',
                            'італійська їдальня меблі',
                            'італійські стільці для кухні',
                            'квадратний журнальний столик',
                            'квадратний обідній стіл',
                            'квадратний стіл для кухні',
                            'китайські столи і стільці',
                            'класична меблі для їдальні',
                            'класична їдальня меблі',
                            'класичні кухонні столи',
                            'класичні столи і стільці',
                            'класичні стільці для кухні',
                            'класичний обідній стіл',
                            'класичний стіл на кухню',
                            'шкіряне крісло офісне',
                            'шкіряні офісні крісла',
                            'шкіряний офісний стілець',
                            'комплект для кухні стіл і стілець',
                            'комплект кухонний стіл і стілець',
                            'комплект обідньої меблів',
                            'комплект обідній стіл',
                            'комплект стіл та стільці',
                            'кавовий стіл лофт',
                            'кавовий столик з дерева',
                            'кавовий столик на колесах',
                            'кавовий столик на коліщатках',
                            'кавовий столик круглий',
                            'кавовий столик лофт',
                            'кавовий столик скляний',
                            'крісла натуральної шкіри',
                            'крісло купити онлайн',
                            'крісло купити офісне',
                            'крісло офісне італія',
                            'крісло офісне шкіра',
                            'крісло офісне шкіряне',
                            'крісло офісне з підголовником',
                            'крісло стілець для кухні',
                            'кругла стільниця з дерева',
                            'кругла стільниця столу',
                            'круглу стільницю столу',
                            'круглі кухонні столи',
                            'круглі обідні столи',
                            'круглі столи для кухні',
                            'круглі столи на кухню',
                            'круглі столи обідні',
                            'круглі столи та стільці',
                            'круглий дерев\'яний журнальний стіл',
                            'круглий журнальний столик',
                            'круглий кавовий столик',
                            'круглий кухонний стіл',
                            'круглий обідній стіл',
                            'круглий приліжковий столик',
                            'круглий стіл зі стільцями',
                            'круглий скляний стіл',
                            'круглий стіл для кухні',
                            'круглий стіл на кухню',
                            'круглий стіл кухонний',
                            'круглий стіл обідній',
                            'круглий стіл і стільці',
                            'кухонні обідня група',
                            'кухонні дерев\'яні столи',
                            'кухонні дерев\'яні стільці',
                            'кухонні круглі столи',
                            'кухонні металеві стільці',
                            'кухонні обідні столи',
                            'кухонні овальні столи',
                            'кухонні пластикові столи',
                            'кухонні розкладні столи',
                            'кухонні складні стільці',
                            'кухонні скляні столи',
                            'кухонні столи з дерева',
                            'кухонні столи дерево',
                            'кухонні столи дерев\'яні',
                            'кухонні столи круглі',
                            'кухонні столи лофт',
                            'кухонні столи з масиву',
                            'кухонні столи обідні',
                            'кухонні столи овальні',
                            'кухонні столи розсувні',
                            'кухонні столи розкладні',
                            'кухонні столи скло',
                            'кухонні столи скляні',
                            'кухонні столи і стільці',
                            'кухонні стільці білі',
                            'кухонні стільці дерев\'яні',
                            'кухонні стільці китай',
                            'кухонні стільці металеві',
                            'кухонні стільці пластикові',
                            'кухонні стільці з підлокітниками',
                            'кухонні стільці регульовані',
                            'кухонні стільці складні',
                            'кухонний барний стілець і стіл',
                            'кухонний дерев\'яний стіл',
                            'кухонний комплект стіл і стілець',
                            'кухонний круглий стіл',
                            'кухонний стіл з масиву дерева',
                            'кухонний набір стіл і стілець',
                            'кухонний обідній стіл',
                            'кухонний овальний стіл',
                            'кухонний розсувний стіл',
                            'кухонний розкладний стіл',
                            'кухонний стіл зі склом',
                            'кухонний скляний стіл',
                            'кухонний стіл з дерева',
                            'кухонний стіл дерево',
                            'кухонний стіл дерев\'яний',
                            'кухонний стіл класика',
                            'кухонний стіл круглий',
                            'кухонний стіл лофт',
                            'кухонний стіл з масиву',
                            'кухонний стіл овальний',
                            'кухонний стіл пластикової',
                            'кухонний стіл розсувний',
                            'кухонний стіл розкладний',
                            'кухонний стіл з регульованою ніжкою',
                            'кухонний стіл зі скла',
                            'кухонний стіл скло',
                            'кухонний стіл скляний',
                            'кухонний стіл і стільці',
                            'кухонний стіл з хромованими ніжками',
                            'лофт журнальний столик',
                            'лофт ніжки столу',
                            'лофт письмовий стіл',
                            'малайзійські столи і стільці',
                            'маленькі крісла для кухні',
                            'меблі кухонний стіл',
                            'меблі для офісу лофт',
                            'меблі стилі лофт',
                            'меблі для їдальні італія',
                            'меблі для їдальні китай',
                            'меблі для їдальні кімнати',
                            'меблі для їдальні в сучасному стилі',
                            'меблі в їдальню кімнату',
                            'металеві кухонні стільці',
                            'металеві стільці для кухні',
                            'металеві стільці на кухню',
                            'м\'які барні стільці',
                            'м\'які обідні стільці',
                            'м\'які стільці для кухні',
                            'м\'які стільці на кухню',
                            'м\'які стільці для їдальні',
                            'м\'який барний стілець',
                            'набір стіл і стільці',
                            'низькі барні стільці',
                            'низький барний стілець',
                            'нога барного столу',
                            'ноги столу лофт',
                            'ніжка барного столу',
                            'ніжка столу дерев\'яна',
                            'ніжка столу чавунна',
                            'ніжки барного столу',
                            'ніжки барні столів',
                            'ніжки з дерева столу',
                            'ніжки дерев\'яного столу',
                            'ніжки дерев\'яні столу',
                            'ніжки до столу',
                            'ніжки журнального столика',
                            'ніжки кухонного столу',
                            'ніжки лофт столу',
                            'ніжки опори столу',
                            'ніжки офісного столу',
                            'ніжки під стільницю',
                            'ніжки в стилі лофт',
                            'ніжки для столу з дерева',
                            'ніжки для столу дерево',
                            'ніжки столу дерев\'яні',
                            'ніжки столу лофт',
                            'ніжки столу в стилі лофт',
                            'ніжки столу хромовані',
                            'ніжки хромовані столу',
                            'обідня група китай',
                            'обідня група класика',
                            'обідня група для кухні',
                            'обідня група Малайзія',
                            'обідню групу класика',
                            'обідню групу на кухню',
                            'обідні групи класика',
                            'обідні групи для кухні',
                            'обідні групи малайзії',
                            'обідні групи Малайзія',
                            'обідні комплекти для кухні',
                            'обідні комплекти Малайзія',
                            'обідні круглі столи',
                            'обідні кухонні столи',
                            'обідні овальні столи',
                            'обідні розкладні столи',
                            'обідні скляні столи',
                            'обідні столи з дерева',
                            'обідні столи дерев\'яні',
                            'обідні столи китай',
                            'обідні столи класика',
                            'обідні столи в класичному стилі',
                            'обідні столи круглі',
                            'обідні столи для кухні',
                            'обідні столи на кухню',
                            'обідні столи лофт',
                            'обідні столи малайзії',
                            'обідні столи Малайзія',
                            'обідні столи з масиву',
                            'обідні столи овальні',
                            'обідні столи розсувні',
                            'обідні столи розкладні',
                            'обідні столи зі скла',
                            'обідні столи скло',
                            'обідні столи скляні',
                            'обідні столи та стільці',
                            'обідній дерев\'яний стіл',
                            'обідній комплект для кухні',
                            'обідній круглий стіл',
                            'обідній овальний стіл',
                            'обідній розсувний стіл',
                            'обідній розкладний стіл',
                            'обідній стіл зі склом',
                            'обідній стіл зі стільцями',
                            'обідній скляний стіл',
                            'обідній стіл в стилі лофт',
                            'обідній стіл з дерева',
                            'обідній стіл дерево',
                            'обідній стіл дерев\'яний',
                            'обідній стіл китай',
                            'обідній стіл класика',
                            'обідній стіл в класичному стилі',
                            'обідній стіл круглий',
                            'обідній стіл для кухні',
                            'обідній стіл на кухню',
                            'обідній стіл лофт',
                            'обідній стіл Малайзія',
                            'обідній стіл масив',
                            'обідній стіл з масиву',
                            'обідній стіл овальний',
                            'обідній стіл прямокутний',
                            'обідній стіл розсувний',
                            'обідній стіл розкладний',
                            'обідній стіл регульований',
                            'обідній стіл зі скла',
                            'обідній стіл скло',
                            'обідній стіл скляний',
                            'обідній стіл і стільці',
                            'овальна стільниця столу',
                            'овальні журнальні столики',
                            'овальні кухонні столи',
                            'овальні обідні столи',
                            'овальні столи для кухні',
                            'овальний журнальний столик',
                            'овальний кухонний стіл',
                            'овальний обідній стіл',
                            'овальний стіл на кухні',
                            'овальний стіл для кухні',
                            'овальний стіл на кухню',
                            'овальний стіл обідній',
                            'опора барного столу',
                            'опора нога столу',
                            'опора під стільницю',
                            'опора столу дерев\'яна',
                            'опора столу лофт',
                            'опори барні столів',
                            'опори офісних столів',
                            'опори столів лофт',
                            'основа столу лофт',
                            'офісні меблі італії',
                            'офісні меблі італія',
                            'офісні меблі китай',
                            'офісні меблі Китаю',
                            'офісні меблі лофт',
                            'офісне крісло бежеве',
                            'офісне крісло італія',
                            'офісне крісло китай',
                            'офісне крісло шкіра',
                            'офісне крісло на колесах',
                            'офісне крісло на коліщатках',
                            'офісне крісло з підголовником',
                            'офісні шкіряні крісла',
                            'офісні крісла обертаються',
                            'офісні крісла італія',
                            'офісні крісла китай',
                            'офісні крісла шкіра',
                            'офісні крісла кожзам',
                            'офісні крісла на коліщатках',
                            'офісні крісла з натуральної шкіри',
                            'офісні крісла з підголівником',
                            'офісні крісла для відвідувачів',
                            'офісні крісла стільці',
                            'офісні крісла тканинні',
                            'офісні крісла тканину',
                            'офісні крісла екошкіра',
                            'офісні столи лофт',
                            'офісні столи сучасні',
                            'офісні стільці на колесах',
                            'офісні стільці з підлокітниками',
                            'офісні стільці для відвідувачів',
                            'офісні стільці сітка',
                            'офісні стільці екошкіра',
                            'офісний стіл лофт',
                            'офісний стілець на колесах',
                            'офісний стілець на коліщатках',
                            'офісний стілець з підлокітниками',
                            'офісний стілець сітка',
                            'офісний стілець з сіткою',
                            'письмові столи білі',
                            'письмові столи з дерева',
                            'письмові столи дерево',
                            'письмові столи лофт',
                            'письмові столи з полицями',
                            'письмові столи з ящиками',
                            'письмовий білий стіл',
                            'письмовий стіл білий',
                            'письмовий стіл висувними',
                            'письмовий стіл з дерева',
                            'письмовий стіл дерево',
                            'письмовий стіл дерев\'яний',
                            'письмовий стіл китай',
                            'письмовий стіл лофт',
                            'письмовий стіл металевий',
                            'письмовий стіл з полками',
                            'письмовий стіл з полицею',
                            'письмовий стіл сучасний',
                            'письмовий стіл з ящиками',
                            'письмовий стіл з ящиком',
                            'пластикові барні стільці',
                            'пластикові кухонні столи',
                            'пластикові стільці для кухні',
                            'пластикові стільці на кухню',
                            'пластикові стільці для офісу',
                            'пластиковий барний стілець',
                            'подстолье столу лофт',
                            'подстолья офісні столів',
                            'полубарние стільці висота',
                            'напівкруглий стіл для кухні',
                            'прідіванний столик лофт',
                            'приліжковий столик круглий',
                            'приліжковий столик лофт',
                            'приліжковий столик складаний',
                            'прямокутний журнальний столик',
                            'прямокутний обідній стіл',
                            'розсувний кухонний стіл',
                            'розсувний обідній стіл',
                            'розсувний стіл для кухні',
                            'розсувний стіл на кухню',
                            'розсувний стіл кухонний',
                            'розсувний стіл обідній',
                            'розсувні кухонні столи',
                            'розсувні обідні столи',
                            'розсувні столи для кухні',
                            'розсувні столи на кухню',
                            'розсувні столи кухонні',
                            'розкладний журнальний столик',
                            'розкладний кухонний стіл',
                            'розкладний обідній стіл',
                            'розкладний стіл журнальний',
                            'розкладний стіл для кухні',
                            'розкладний стіл на кухню',
                            'розкладний стіл кухонний',
                            'розкладний стіл обідній',
                            'розкладний столик на кухню',
                            'розкладний стілець для кухні',
                            'розкладні журнальні столики',
                            'розкладні журнальні столи',
                            'розкладні кухонні столи',
                            'розкладні обідні столи',
                            'розкладні столи для кухні',
                            'розкладні столи кухонні',
                            'розкладні столи обідні',
                            'розкладаються столи для кухні',
                            'розкладається журнальний столик',
                            'регульовані барні стільці',
                            'регульований барний стілець',
                            'складаний барний стілець',
                            'складаний журнальний стіл',
                            'складаний журнальний столик',
                            'складаний кавовий столик',
                            'складаний кухонний стіл',
                            'складаний стіл для кухні',
                            'складаний стіл на кухню',
                            'складаний стіл обідній',
                            'складаний столик на кухню',
                            'складаний барний стілець',
                            'складаний стілець для кухні',
                            'складаний стілець на кухню',
                            'складні барні стільці',
                            'складні журнальні столики',
                            'складні кухонні стільці',
                            'складні обідні столи',
                            'складні столи для кухні',
                            'складні стільці для кухні',
                            'складні стільці на кухню',
                            'складні табуретки для кухні',
                            'складаються кухонні столи',
                            'сучасні меблі для офісу',
                            'сучасні меблі для їдальні',
                            'сучасні офісні меблі',
                            'сучасна їдальня меблі',
                            'сучасні журнальні столики',
                            'сучасні кухонні столи',
                            'сучасні кухонні стільці',
                            'сучасні ніжки столу',
                            'сучасні обідні столи',
                            'сучасні офісні столи',
                            'сучасні письмові столи',
                            'сучасні столи для кухні',
                            'сучасні стільці для кухні',
                            'сучасний журнальний столик',
                            'сучасний кухонний стіл',
                            'сучасний обідній стіл',
                            'сучасний офісний стіл',
                            'сучасний письмовий стіл',
                            'сучасний стіл на кухню',
                            'скляні журнальні столики',
                            'скляні кухонні столи',
                            'скляні обідні столи',
                            'скляні розкладні столи',
                            'скляні столики журнальні',
                            'скляні столи журнальні',
                            'скляні столи для кухні',
                            'скляні столи на кухню',
                            'скляні столи кухонні',
                            'скляний журнальний стіл',
                            'скляний журнальний столик',
                            'скляний кавовий столик',
                            'скляний кухонний стіл',
                            'скляний обідній стіл',
                            'скляний приліжковий столик',
                            'скляний стіл журнальний',
                            'скляний стіл для кухні',
                            'скляний стіл на кухню',
                            'скляний стіл кухонний',
                            'скляний стіл обідній',
                            'скляний столик журнальний',
                            'стіл білого кольору',
                            'стіл білий письмовий',
                            'стіл з висувними ящиками',
                            'стіл з дерева на кухню',
                            'стіл дерев\'яний для кухні',
                            'стіл дерев\'яний на кухню',
                            'стіл дерев\'яний кухонний',
                            'стіл дерев\'яний обідній',
                            'стіл дерев\'яний письмовий',
                            'стіл журнальний з дерева',
                            'стіл журнальний дерево',
                            'стіл журнальний дерев\'яний',
                            'стіл журнальний квадратний',
                            'стіл журнальний кавовий',
                            'стіл журнальний лофт',
                            'стіл журнальний масив',
                            'стіл журнальний з масиву',
                            'стіл журнальний овальний',
                            'стіл журнальний розкладний',
                            'стіл журнальний скло',
                            'стіл журнальний скляний',
                            'стіл із змінною висотою',
                            'стіл квадратний на кухню',
                            'стіл класика обідній',
                            'стіл в класичному стилі',
                            'стіл кавовий круглий',
                            'стіл кавовий лофт',
                            'стіл круглий журнальний',
                            'стіл круглий для кухні',
                            'стіл круглий на кухню',
                            'стіл круглий кухонний',
                            'стіл круглий обідній',
                            'стіл круглий і стільці',
                            'стіл для кухні з дерева',
                            'стіл для кухні дерев\'яний',
                            'стіл для кухні круглий',
                            'стіл для кухні лофт',
                            'стіл для кухні з масиву',
                            'стіл для кухні овальний',
                            'стіл для кухні пластик',
                            'стіл для кухні розсувний',
                            'стіл для кухні розкладний',
                            'стіл для кухні скляний',
                            'стіл на кухню з дерева',
                            'стіл на кухню дерево',
                            'стіл на кухню дерев\'яний',
                            'стіл на кухню круглий',
                            'стіл на кухню лофт',
                            'стіл на кухню овальний',
                            'стіл на кухню розсувний',
                            'стіл на кухню розкладний',
                            'стіл на кухню скло',
                            'стіл на кухню скляний',
                            'стіл кухонний з дерева',
                            'стіл кухонний дерево',
                            'стіл кухонний дерев\'яний',
                            'стіл кухонний квадратний',
                            'стіл кухонний класика',
                            'стіл кухонний круглий',
                            'стіл кухонний лофт',
                            'стіл кухонний масив',
                            'стіл кухонний з масиву',
                            'стіл кухонний обідній',
                            'стіл кухонний овальний',
                            'стіл кухонний пластиковий',
                            'стіл кухонний прямокутний',
                            'стіл кухонний розсувний',
                            'стіл кухонний розкладний',
                            'стіл кухонний сучасний',
                            'стіл кухонний зі скла',
                            'стіл кухонний скло',
                            'стіл кухонний скляний',
                            'стіл лофт обідній',
                            'стіл лофт письмовий',
                            'стіл з масиву дерева',
                            'стіл з масиву обідній',
                            'стіл на металевому каркасі',
                            'стіл обідній з дерева',
                            'стіл обідній дерево',
                            'стіл обідній дерев\'яний',
                            'стіл обідній квадратний',
                            'стіл обідній класика',
                            'стіл обідній класичний',
                            'стіл обідній круглий',
                            'стіл обідній для кухні',
                            'стіл обідній на кухню',
                            'стіл обідній кухонний',
                            'стіл обідній лофт',
                            'стіл обідній Малайзія',
                            'стіл обідній масив',
                            'стіл обідній з масиву',
                            'стіл обідній овальний',
                            'стіл обідній пластиковий',
                            'стіл обідній прямокутний',
                            'стіл обідній розсувний',
                            'стіл обідній розкладний',
                            'стіл обідній сучасний',
                            'стіл обідній зі скла',
                            'стіл обідній скло',
                            'стіл обідній скляний',
                            'стіл обідній і стільці',
                            'стіл овальний для кухні',
                            'стіл овальний на кухню',
                            'стіл овальний кухонний',
                            'стіл овальний обідній',
                            'стіл на одній ніжці',
                            'стіл письмовий білий',
                            'стіл письмовий з дерева',
                            'стіл письмовий дерево',
                            'стіл письмовий дерев\'яний',
                            'стіл письмовий лофт',
                            'стіл письмовий з полками',
                            'стіл письмовий з полицею',
                            'стіл письмовий сучасний',
                            'стіл письмовий з ящиками',
                            'стіл з пластикової стільницею',
                            'стіл з пластиковим покриттям',
                            'стіл виробництва Малайзія',
                            'стіл розсувний для кухні',
                            'стіл розсувний на кухню',
                            'стіл розсувний кухонний',
                            'стіл розсувний обідній',
                            'стіл розкладний журнальний',
                            'стіл розкладний для кухні',
                            'стіл розкладний на кухню',
                            'стіл розкладний кухонний',
                            'стіл розкладний обідній',
                            'стіл з регульованою висотою',
                            'стіл регульований по висоті',
                            'стіл зі склом',
                            'стіл зі стільцями',
                            'стіл зі скла для кухні',
                            'стіл скляний журнальний',
                            'стіл скляний для кухні',
                            'стіл скляний на кухню',
                            'стіл скляний кухонний',
                            'стіл скляний обідній',
                            'стіл скляний овальний',
                            'стіл стилі лофт',
                            'стіл і стільці для кухні',
                            'стіл і стільці на кухню',
                            'стіл і стільці Малайзія',
                            'стіл і стільці набір',
                            'стіл і стільці обідні',
                            'стіл з хромованими ніжками',
                            'стільниця з дерева столу',
                            'стільниця кругла для столу',
                            'стільниця овальна для столу',
                            'стільниця під дерево',
                            'стільниця для столу верзаліт',
                            'стільниця столу з дерева',
                            'стільницю з дерева столу',
                            'стільницю під дерево',
                            'столик журнальний з дерева',
                            'столик журнальний дерев\'яний',
                            'столик журнальний на коліщатках',
                            'столик журнальний круглий',
                            'столик журнальний скло',
                            'столик журнальний скляний',
                            'столик кавовий на коліщатках',
                            'столик кавовий круглий',
                            'столик з масиву дерева',
                            'столик приліжковий круглий',
                            'столик зі склом',
                            'столик в сучасному стилі',
                            'столик скляний журнальний',
                            'столик в стилі лофт',
                            'столики журнальні скляні',
                            'столики зі склом',
                            'столики скляні журнальні',
                            'столики в стилі лофт',
                            'їдальня в класичному стилі',
                            'їдальня меблі італія',
                            'їдальня меблі китай',
                            'їдальня меблі Китаю',
                            'столи барні для кухні',
                            'столи дерев\'яні для кухні',
                            'столи дерев\'яні кухонні',
                            'столи дерев\'яні обідні',
                            'столи журнальні з дерева',
                            'столи журнальні скляні',
                            'столи в класичному стилі',
                            'столи круглі для кухні',
                            'столи круглі кухонні',
                            'столи круглі обідні',
                            'столи для кухні дерев\'яні',
                            'столи для кухні китай',
                            'столи для кухні класика',
                            'столи для кухні круглі',
                            'столи для кухні овальні',
                            'столи для кухні розкладні',
                            'столи для кухні зі скла',
                            'столи для кухні скляні',
                            'столи на кухню круглі',
                            'столи кухонні дерево',
                            'столи кухонні дерев\'яні',
                            'столи кухонні круглі',
                            'столи кухонні з масиву',
                            'столи кухонні розсувні',
                            'столи кухонні розкладні',
                            'столи кухонні скляні',
                            'столи масиву з дерева',
                            'столи обідні з дерева',
                            'столи обідні дерев\'яні',
                            'столи обідні квадратні',
                            'столи обідні круглі',
                            'столи обідні лофт',
                            'столи обідні Малайзія',
                            'столи обідні з масиву',
                            'столи обідні овальні',
                            'столи обідні прямокутні',
                            'столи обідні розсувні',
                            'столи обідні розкладні',
                            'столи обідні скло',
                            'столи обідні скляні',
                            'столи овальні для кухні',
                            'столи письмові білі',
                            'столи письмові з полками',
                            'столи розсувні для кухні',
                            'столи розсувні кухонні',
                            'столи розсувні обідні',
                            'столи розкладні для кухні',
                            'столи розкладні кухонні',
                            'столи розкладні обідні',
                            'столи зі склом',
                            'столи зі скла для кухні',
                            'столи скло для кухні',
                            'столи скляні для кухні',
                            'столи скляні на кухню',
                            'столи скляні кухонні',
                            'столи скляні обідні',
                            'столи в стилі лофт',
                            'столи і стільці китай',
                            'столи і стільці Китаю',
                            'столи і стільці класика',
                            'столи і стільці для кухні',
                            'столи і стільці малайзії',
                            'столи і стільці Малайзія',
                            'столи і стільці обідні',
                            'стілець для барної стійки',
                            'стілець барний обертається',
                            'стілець барний високий',
                            'стілець барний з дерева',
                            'стілець барний дерево',
                            'стілець барний дерев\'яний',
                            'стілець барний м\'який',
                            'стілець барний пластиковий',
                            'стілець барний регульований',
                            'стілець барний складаний',
                            'стілець без спинки',
                            'стілець білий кухонний',
                            'стілець високий барний',
                            'стілець дерев\'яний барний',
                            'стілець дерев\'яний кухонний',
                            'стілець шкіряний офісний',
                            'стілець крісло для кухні',
                            'стілець крісло на кухню',
                            'стілець купити дерев\'яний',
                            'стілець купити офісний',
                            'стілець кухонний білий',
                            'стілець кухонний дерево',
                            'стілець кухонний дерев\'яний',
                            'стілець кухонний розкладний',
                            'стілець кухонний складаний',
                            'стілець металевій основі',
                            'стілець обідній білий',
                            'стілець обідній дерев\'яний',
                            'стілець обідній металевий',
                            'стілець обідній з підлокітниками',
                            'стілець офісний шкіряний',
                            'стілець офісний кожзам',
                            'стілець офісний на коліщатках',
                            'стілець офісний сітка',
                            'стілець офісний з сіткою',
                            'стілець офісний спинка',
                            'стілець з регулюванням висоти',
                            'стілець з регульованою висотою',
                            'стілець регульований по висоті',
                            'стілець складаний барний',
                            'стілець складаний кухонний',
                            'стілець зі спинкою',
                            'стільці для актового залу',
                            'стільці для барной стійки',
                            'стільці барні високі',
                            'стільці барні дерев\'яні',
                            'стільці барні для дому',
                            'стільці для барні стійок',
                            'стільці для барів та кафе',
                            'стільці для барів та ресторанів',
                            'стільці без спинки',
                            'стільці білі для кухні',
                            'стільці білі на кухню',
                            'стільці високі барні',
                            'стільці дерев\'яні барні',
                            'стільці дерев\'яні для кухні',
                            'стільці дерев\'яні на кухню',
                            'стільці дерев\'яні кухонні',
                            'стільці для кафе і барів',
                            'стільці для кафе і ресторанів',
                            'стільці класичні для кухні',
                            'стільці шкірозамінника для кухні',
                            'стільці конференц зал',
                            'стільці конференц залу',
                            'стільці конференц залів',
                            'стільці для кухні білі',
                            'стільці для кухні з дерева',
                            'стільці для кухні дерево',
                            'стільці для кухні дерев\'яні',
                            'стільці для кухні італія',
                            'стільці для кухні китай',
                            'стільці для кухні класика',
                            'стільці для кухні кожзам',
                            'стільці для кухні лофт',
                            'стільці для кухні Малайзія',
                            'стільці для кухні металеві',
                            'стільці для кухні м\'які',
                            'стільці для кухні пластик',
                            'стільці для кухні з підлокітниками',
                            'стільці для кухні складні',
                            'стільці для кухні для їдальні',
                            'стільці для кухні чорні',
                            'стільці на кухню білі',
                            'стільці на кухню дерев\'яні',
                            'стільці на кухню металеві',
                            'стільці кухонні білі',
                            'стільці кухонні дерев\'яні',
                            'стільці кухонні класика',
                            'стільці кухонні класичні',
                            'стільці кухонні кожзам',
                            'стільці кухонні Малайзія',
                            'стільці кухонні металеві',
                            'стільці кухонні пластикові',
                            'стільці кухонні з підлокітниками',
                            'стільці металеві для кухні',
                            'стільці з металевим каркасом',
                            'стільці з металевими ніжками',
                            'стільці на металевих ніжках',
                            'стільці м\'які для кухні',
                            'стільці обідні групи',
                            'стільці обідні дерев\'яні',
                            'стільці офісні на коліщатках',
                            'стільці офісні для відвідувачів',
                            'стільці з пластику для кухні',
                            'стільці пластикові для кухні',
                            'стільці з підлокітниками для кухні',
                            'стільці з підлокітниками напівкрісло',
                            'стільці регульовані по висоті',
                            'стільці для ресторану і кафе',
                            'стільці для ресторанів і кафе',
                            'стільці складні для кухні',
                            'стільці зі спинкою',
                            'стільці і столи для кухні',
                            'тканинні офісні стільці',
                            'хромована нога столу',
                            'хромовані ніжки столу',
                            'чорні барні стільці',
                            'чорні стільці на кухню',
                            'чавунна опора столу',
                            'чавунні ніжки столу',


                        );

                        if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                            $tags2_arr = $tags2_arr_ua;

                        rand(rand(0,time()), time());
                        $val = $tags2_arr[rand(0, count($tags2_arr) - 1)];
                        $tags2 = $val;

                        $tags3_arr =array('металлические стулья',
                            'мягкие стулья',
                            'мягкий барный стул',
                            'мягкий стул',
                            'низкий барный стул',
                            'нога стола',
                            'ноги стола',
                            'ноги столешницы',
                            'ножка стола',
                            'ножки из дерева',
                            'ножки лофт',
                            'ножки опоры',
                            'ножки для стола',
                            'ножки столешницы',
                            'ножки столов',
                            'ножки к столу',
                            'ножки хромированные',
                            'ножку стола',
                            'обеденная группа',
                            'обеденную группу',
                            'обеденные группы',
                            'обеденные из дерева',
                            'обеденные зоны',
                            'обеденные комплекты',
                            'обеденные прямоугольные столы',
                            'обеденные раздвижные столы',
                            'обеденные столы',
                            'обеденные стулья',
                            'обеденный квадратный стол',
                            'обеденный классический стол',
                            'обеденный комплект',
                            'обеденный металлический стол',
                            'обеденный набор',
                            'обеденный овальный стол',
                            'обеденный пластиковый стул',
                            'обеденный стул с подлокотниками',
                            'обеденный прямоугольный стол',
                            'обеденный раздвижной стол',
                            'обеденный современный стол',
                            'обеденный стеклянный стол',
                            'обеденный стол',
                            'обеденный стол и стулья',
                            'обеденные столы',
                            'обеденый стол',
                            'овальная столешница',
                            'овальную столешницу',
                            'овальные столы для кухни',
                            'овальные кухонные столы',
                            'овальные столы',
                            'овальный стол для кухни',
                            'овальный стол',
                            'опора для стола',
                            'опора столешницы',
                            'опору стола',
                            'опоры стола',
                            'опоры столов',
                            'основа стола',
                            'основание стола',
                            'основания столов',
                            'офисная мебель',
                            'офисное кресло',
                            'офисное кресло',
                            'офисную мебель',
                            'офисные кресла',
                            'офисные столы',
                            'офисные стулья',
                            'офисный стол',
                            'офисный стул',
                            'офисные стулья',
                            'письменные полками',
                            'письменные столы',
                            'письменный деревянный сто',
                            'письменный стол лофт',
                            'письменный стол полками',
                            'письменный с полкой',
                            'письменный современный стол',
                            'письменный стол',
                            'письменные столы',
                            'письменый стол',
                            'пластиковые столы для кухни',
                            'пластиковые столы',
                            'пластиковые стулья',
                            'пластиковый барный стул',
                            'пластиковый стул',
                            'пластмассовый стул',
                            'подстолье стола',
                            'подстолье чугунное',
                            'подстолья офисные',
                            'подстолья чугунные',
                            'полубарные стулья',
                            'полубарный стул',
                            'полукруглый стол',
                            'придиванный столик',
                            'раздвижной стол',
                            'раздвижные столы',
                            'раскладной стол',
                            'раскладной столик',
                            'раскладной стул',
                            'раскладные столы',
                            'раскладные стульчики',
                            'раскладывающиеся столы',
                            'растущий стул',
                            'регулируемый барный стул',
                            'регулируемый стул',
                            'ротанговые стулья',
                            'ротанговый стул',
                            'складной барный стул',
                            'складной стол',
                            'складной столик',
                            'складной стул',
                            'складные столы',
                            'складные стулья',
                            'складные табуретки',
                            'складывающиеся стулья',
                            'современная мебель',
                            'современная офисная',
                            'современная столовая',
                            'современные кухонные',
                            'современные столы',
                            'современные стулья',
                            'современный стол',
                            'стеклянные столики',
                            'стеклянные столы',
                            'стеклянный стол для кухни',
                            'стеклянный стол на кухню',
                            'стеклянный кухонный стол',
                            'стеклянный раздвижной стол',
                            'стеклянный стол',
                            'стеклянный столик',
                            'стеклянные журнальные столики',
                            'стол белый',
                            'стол верзалит',
                            'стол из дерева',
                            'стол дерево',
                            'стол деревянный',
                            'стол журнальный',
                            'стол квадратный',
                            'стол китай',
                            'стол классика',
                            'стол на колесах',
                            'стол на колесиках',
                            'стол кофейный',
                            'стол круглый',
                            'стол на кухне',
                            'стол для кухни',
                            'стол на кухню',
                            'стол кухонный',
                            'стол лофт',
                            'стол малайзия',
                            'стол массив',
                            'стол из массива',
                            'стол металлический',
                            'стол обеденный',
                            'стол овальный',
                            'стол для письма',
                            'стол письменный',
                            'стол с полками',
                            'стол с полкой',
                            'стол прямоугольный',
                            'стол раздвижной',
                            'стол раскладной',
                            'стол регулируемый',
                            'стол складной',
                            'стол современный',
                            'стол из стекла',
                            'стол стекло',
                            'стол стеклянный',
                            'стол и стулья',
                            'стол со стульями',
                            'стол с ящиками',
                            'стол с ящиком',
                            'стола верзалит',
                            'стола из дерева',
                            'стола дерево',
                            'стола деревянная',
                            'стола деревянные',
                            'столешница австрия',
                            'столешница верзалит',
                            'столешница верзалита',
                            'столешница германия',
                            'столешница из дерева');
                        $tags3_arr_ua =array(
                            'металеві стільці',
                            'м\'які стільці',
                            'м\'який барний стілець',
                            'м\'який стілець',
                            'низький барний стілець',
                            'нога столу',
                            'ноги столу',
                            'ноги стільниці',
                            'ніжка столу',
                            'ніжки з дерева',
                            'ніжки лофт',
                            'ніжки опори',
                            'ніжки для столу',
                            'ніжки стільниці',
                            'ніжки столів',
                            'ніжки до столу',
                            'ніжки хромовані',
                            'ніжку стола',
                            'обідня група',
                            'обідню групу',
                            'обідні групи',
                            'обідні з дерева',
                            'обідні зони',
                            'обідні комплекти',
                            'обідні прямокутні столи',
                            'обідні розсувні столи',
                            'обідні столи',
                            'обідні стільці',
                            'обідній квадратний стіл',
                            'обідній класичний стіл',
                            'обідній комплект',
                            'обідній металевий стіл',
                            'обідній набір',
                            'обідній овальний стіл',
                            'обідній пластиковий стілець',
                            'обідній стілець з підлокітниками',
                            'обідній прямокутний стіл',
                            'обідній розсувний стіл',
                            'обідній сучасний стіл',
                            'обідній скляний стіл',
                            'обідній стіл',
                            'обідній стіл і стільці',
                            'обідні столи',
                            'обідній стіл',
                            'овальна стільниця',
                            'овальну стільницю',
                            'овальні столи для кухні',
                            'овальні кухонні столи',
                            'овальні столи',
                            'овальний стіл для кухні',
                            'овальний стіл',
                            'опора для столу',
                            'опора стільниці',
                            'опору столу',
                            'опори столу',
                            'опори столів',
                            'основа столу',
                            'підставу столу',
                            'основи столів',
                            'офісні меблі',
                            'офісне крісло',
                            'офісне крісло',
                            'офісні меблі',
                            'офісні крісла',
                            'офісні столи',
                            'офісні стільці',
                            'офісний стіл',
                            'офісний стілець',
                            'офісні стільці',
                            'письмові полками',
                            'письмові столи',
                            'письмовий дерев\'яний сто',
                            'письмовий стіл лофт',
                            'письмовий стіл полками',
                            'письмовий з полицею',
                            'письмовий сучасний стіл',
                            'письмовий стіл',
                            'письмові столи',
                            'Письмовий стіл',
                            'пластикові столи для кухні',
                            'пластикові столи',
                            'пластикові стільці',
                            'пластиковий барний стілець',
                            'пластиковий стілець',
                            'пластмасовий стілець',
                            'подстолье столу',
                            'подстолье чавунне',
                            'подстолья офісні',
                            'подстолья чавунні',
                            'полубарние стільці',
                            'полубарний стілець',
                            'напівкруглий стіл',
                            'прідіванний столик',
                            'розсувний стіл',
                            'розсувні столи',
                            'розкладний стіл',
                            'розкладний столик',
                            'розкладний стілець',
                            'розкладні столи',
                            'розкладні стільчики',
                            'розкладаються столи',
                            'зростаючий стілець',
                            'регульований барний стілець',
                            'регульований стілець',
                            'ротангові стільці',
                            'ротангових стілець',
                            'складаний барний стілець',
                            'стіл, що складається',
                            'складаний столик',
                            'складний стілець',
                            'складні столи',
                            'складні стільці',
                            'складні табуретки',
                            'складаються стільці',
                            'сучасні меблі',
                            'сучасні офісні',
                            'сучасна їдальня',
                            'сучасні кухонні',
                            'сучасні столи',
                            'сучасні стільці',
                            'сучасний стіл',
                            'скляні столики',
                            'скляні столи',
                            'скляний стіл для кухні',
                            'скляний стіл на кухню',
                            'скляний кухонний стіл',
                            'скляний розсувний стіл',
                            'Склянний стіл',
                            'скляний столик',
                            'скляні журнальні столики',
                            'стіл білий',
                            'стіл верзаліт',
                            'стіл з дерева',
                            'стіл дерево',
                            'стіл дерев\'яний',
                            'стіл журнальний',
                            'стіл квадратний',
                            'стіл китай',
                            'стіл класика',
                            'стіл на колесах',
                            'стіл на коліщатках',
                            'стіл кавовий',
                            'стіл круглий',
                            'стіл на кухні',
                            'стіл для кухні',
                            'стіл на кухню',
                            'стіл кухонний',
                            'стіл лофт',
                            'стіл малайзія',
                            'стіл масив',
                            'стіл з масиву',
                            'стіл металевий',
                            'стіл обідній',
                            'стіл овальний',
                            'стіл для письма',
                            'письмовий стіл',
                            'стіл з полками',
                            'стіл з полицею',
                            'стіл прямокутний',
                            'стіл розсувний',
                            'стіл розкладний',
                            'стіл регульований',
                            'стіл складаний',
                            'стіл сучасний',
                            'стіл зі скла',
                            'стіл скло',
                            'стіл скляний',
                            'стіл і стільці',
                            'стіл зі стільцями',
                            'стіл з ящиками',
                            'стіл з ящиком',
                            'столу верзаліт',
                            'столу з дерева',
                            'столу дерево',
                            'столу дерев\'яна',
                            'столу дерев\'яні',
                            'стільниця австрія',
                            'стільниця верзаліт',
                            'стільниця верзаліт',
                            'стільниця германію',
                            'стільниця з дерева',
                        );

                        if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                            $tags3_arr = $tags3_arr_ua;

                        rand(rand(0,time()), time());
                        $val = $tags3_arr[rand(0, count($tags3_arr) - 1)];
                        $tags3 = $val;

                        $tags4_arr =array('столешница дерево',
                            'столешница деревянная',
                            'столешница круглая',
                            'столешница на кухню',
                            'столешница овальная',
                            'столешница прямоугольная',
                            'столешница для стола',
                            'столешница топалит',
                            'столешница werzalit',
                            'столешницу из дерева',
                            'столешницу деревянную',
                            'столешницу для стола',
                            'столешницы верзалит',
                            'столешницы для столов',
                            'столешницы topalit',
                            'столик из дерева',
                            'столик дерево',
                            'столик деревянный',
                            'столик журнальный',
                            'столик на квадратный',
                            'столик на колесах',
                            'столик на колесиках',
                            'столик кофейный',
                            'столик круглый',
                            'столик на кухню',
                            'столик лофт',
                            'столик массив',
                            'столик из массива',
                            'столик овальный',
                            'столик прикроватный',
                            'столик прямоугольный',
                            'столик раскладной',
                            'столик раскладывающийся',
                            'столик складной',
                            'столик современный',
                            'столик из стекла',
                            'столик стекло',
                            'столик стеклянный',
                            'столики из дерева',
                            'столики деревянные',
                            'столики журнальные',
                            'столики на колесиках',
                            'столики лофт',
                            'столики из массива',
                            'столики раскладные',
                            'столики из стекла',
                            'столики стекло',
                            'столики стеклянные',
                            'столовая классика',
                            'столовая мебель',
                            'столовую мебель',
                            'столовые стулья',
                            'столы барные',
                            'столы белые',
                            'столы из дерева',
                            'столы дерево',
                            'столы деревянные',
                            'столы журнальные',
                            'столы китай',
                            'столы классика',
                            'столы круглые',
                            'столы для кухни',
                            'столы на кухню',
                            'столы кухонные',
                            'столы лофт',
                            'столы малайзии',
                            'столы малайзия',
                            'столы из массива',
                            'столы обеденные',
                            'столы овальные',
                            'столы письменные',
                            'столы с полками',
                            'столы раздвижные',
                            'столы раскладные',
                            'столы современные',
                            'столы из стекла',
                            'столы стекло',
                            'столы стеклянные',
                            'столы и стулья',
                            'столы с ящиками',
                            'стул барный',
                            'стул белый',
                            'стул вращающийся',
                            'стул высокий',
                            'стул из дерева',
                            'стул дерево',
                            'стул деревянный',
                            'стул для дома',
                            'стул для кафе',
                            'стул кожаный',
                            'стул на колесах',
                            'стул на колесиках',
                            'стул кресло',
                            'стул для кухни',
                            'стул на кухню',
                            'стул кухонный',
                            'стул из металла',
                            'стул металлический',
                            'стул мягкий',
                            'стул низкий',
                            'стул обеденный',
                            'стул для офиса',
                            'стул офисный',
                            'стул пластик',
                            'стул пластиковый',
                            'стул пластмассовый',
                            'стул с подлокотниками',
                            'стул полубарный',
                            'стул для посетителей',
                            'стул раскладной',
                            'стул с регулировкой',
                            'стул регулируемый',
                            'стул ротанг',
                            'стул сетка',
                            'стул с сеткой',
                            'стул складной',
                            'стул черный',
                            'стул экокожа',
                            'стульчик складной',
                            'стульчики складные',
                            'стулья для бара',
                            'стулья барные',
                            'стулья барные',
                            'стулья для баров',
                            'стулья белые',
                            'стулья вращающиеся',
                            'стулья высокие',
                            'стулья из дерева',
                            'стулья дерево',
                            'стулья деревянные',
                            'стулья для дома',
                            'стулья домой',
                            'стулья для кафе',
                            'стулья китай',
                            'стулья китая',
                            'стулья классика',
                            'стулья кожзам',
                            'стулья кожзама',
                            'стулья кожи',
                            'стулья на колесах',
                            'стулья конференц',
                            'стулья конференционные',
                            'стулья кресла',
                            'стулья для кухни',
                            'стулья на кухню',
                            'стулья кухонные',
                            'стулья малайзии',
                            'стулья малайзия',
                            'стулья из металла',
                            'стулья металлические',
                            'стулья на металлокаркасе',
                            'стулья мягкие',
                            'стулья в офис',
                            'стулья для офиса',
                            'стулья офисные',
                            'стулья из пластика',
                            'стулья пластиковые',
                            'стулья пластмассовые',
                            'стулья с подлокотниками',
                            'стулья для посетителей',
                            'стулья регулируемые',
                            'стулья в ресторан',
                            'стулья для ресторана',
                            'стулья для ресторанов',
                            'стулья ротанг',
                            'стулья из ротанга',
                            'стулья сетка',
                            'стулья складные',
                            'стулья для столовой',
                            'стулья в столовую',
                            'стулья в столовые',
                            'стулья и столы',
                            'стулья тканевые',
                            'стулья для улицы',
                            'стулья уличные',
                            'стулья экокожа',
                            'стулья из экокожи',
                            'табуретки для кухни',
                            'черные стулья',
                            'чугунная опора',
                            'чугунное подстолье',
                            'чугунные ножки',
                            'чугунные подстолья',
                            'topalit столешницы',
                            'werzalit столешницы'
                        );
                        $tags4_arr_ua =array(

                            'стільниця дерево',
                            'стільниця дерев\'яна',
                            'стільниця кругла',
                            'стільниця на кухню',
                            'стільниця овальна',
                            'стільниця прямокутна',
                            'стільниця для столу',
                            'стільниця Топалов',
                            'стільниця werzalit',
                            'стільницю з дерева',
                            'стільницю дерев\'яну',
                            'стільницю для столу',
                            'стільниці верзаліт',
                            'стільниці для столів',
                            'стільниці topalit',
                            'столик з дерева',
                            'столик дерево',
                            'столик дерев\'яний',
                            'столик журнальний',
                            'столик на квадратний',
                            'столик на колесах',
                            'столик на коліщатках',
                            'столик кавовий',
                            'столик круглий',
                            'столик на кухню',
                            'столик лофт',
                            'столик масив',
                            'столик з масиву',
                            'столик овальний',
                            'столик приліжковий',
                            'столик прямокутний',
                            'столик розкладний',
                            'столик розкладається',
                            'столик складаний',
                            'столик сучасний',
                            'столик зі скла',
                            'столик скло',
                            'столик скляний',
                            'столики з дерева',
                            'столики дерев\'яні',
                            'столики журнальні',
                            'столики на коліщатках',
                            'столики лофт',
                            'столики з масиву',
                            'столики розкладні',
                            'столики зі скла',
                            'столики скло',
                            'столики скляні',
                            'їдальня класика',
                            'їдальня меблі',
                            'столову меблі',
                            'столові стільці',
                            'столи барні',
                            'столи білі',
                            'столи з дерева',
                            'столи дерево',
                            'столи дерев\'яні',
                            'столи журнальні',
                            'столи китай',
                            'столи класика',
                            'столи круглі',
                            'столи для кухні',
                            'столи на кухню',
                            'столи кухонні',
                            'столи лофт',
                            'столи малайзії',
                            'столи малайзія',
                            'столи з масиву',
                            'столи обідні',
                            'столи овальні',
                            'столи письмові',
                            'столи з полицями',
                            'столи розсувні',
                            'столи розкладні',
                            'столи сучасні',
                            'столи зі скла',
                            'столи скло',
                            'столи скляні',
                            'столи та стільці',
                            'столи з ящиками',
                            'стілець барний',
                            'стілець білий',
                            'стілець обертовий',
                            'стілець високий',
                            'стілець з дерева',
                            'стілець дерево',
                            'стілець дерев\'яний',
                            'стілець для дому',
                            'стілець для кафе',
                            'стілець шкіряний',
                            'стілець на колесах',
                            'стілець на коліщатках',
                            'стул крісло',
                            'стілець для кухні',
                            'стілець на кухню',
                            'стілець кухонний',
                            'стілець з металу',
                            'стілець металевий',
                            'стілець м\'який',
                            'стілець низький',
                            'стілець обідній',
                            'стілець для офісу',
                            'стілець офісний',
                            'стілець пластик',
                            'стілець пластиковий',
                            'стілець пластмасовий',
                            'стілець з підлокітниками',
                            'стілець полубарний',
                            'стілець для відвідувачів',
                            'стілець розкладний',
                            'стілець з регулюванням',
                            'стілець регульований',
                            'стілець ротанг',
                            'стілець сітка',
                            'стілець з сіткою',
                            'стілець складаний',
                            'стілець чорний',
                            'стілець екошкіра',
                            'стільчик складаний',
                            'стільчики складні',
                            'стільці для бару',
                            'стільці барні',
                            'стільці барні',
                            'стільці для барів',
                            'стільці білі',
                            'стільці обертаються',
                            'стільці високі',
                            'стільці з дерева',
                            'стільці дерево',
                            'стільці дерев\'яні',
                            'стільці для дому',
                            'стільці додому',
                            'стільці для кафе',
                            'стільці китай',
                            'стільці Китаю',
                            'стільці класика',
                            'стільці кожзам',
                            'стільці шкірозамінника',
                            'стільці шкіри',
                            'стільці на колесах',
                            'стільці конференц',
                            'стільці конференційні',
                            'стільці крісла',
                            'стільці для кухні',
                            'стільці на кухню',
                            'стільці кухонні',
                            'стільці малайзії',
                            'стільці малайзія',
                            'стільці з металу',
                            'стільці металеві',
                            'стільці на металлокаркасе',
                            'стільці м\'які',
                            'стільці в офіс',
                            'стільці для офісу',
                            'стільці офісні',
                            'стільці з пластику',
                            'стільці пластикові',
                            'стільці пластмасові',
                            'стільці з підлокітниками',
                            'стільці для відвідувачів',
                            'стільці регульовані',
                            'стільці в ресторан',
                            'стільці для ресторану',
                            'стільці для ресторанів',
                            'стільці ротанг',
                            'стільці з ротанга',
                            'стільці сітка',
                            'стільці складні',
                            'стільці для їдальні',
                            'стільці в їдальню',
                            'стільці в їдальні',
                            'стільці і столи',
                            'стільці тканинні',
                            'стільці для вулиці',
                            'стільці вуличні',
                            'стільці екошкіра',
                            'стільці з екокожі',
                            'табуретки для кухні',
                            'чорні стільці',
                            'чавунна опора',
                            'чавунне подстолье',
                            'чавунні ніжки',
                            'чавунні підстілля',
                            'topalit стільниці',
                            'werzalit стільниці',

                        );

                        if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
                            $tags4_arr = $tags4_arr_ua;

                        rand(rand(0,time()), time());
                        $val = $tags4_arr[rand(0, count($tags4_arr) - 1)];
                        $tags4 = $val;

                        $data['text_seo'] = str_replace(['{GET_PAGE_H1}','{geo_dative}','{Main_Category_H1}','{tags}','{tags_1}','{tags_2}','{tags_3}'],[$data['heading_title'],$city_val,$data['breadcrumbs'][count($data['breadcrumbs'])-2]['text'],$tags,$tags2,$tags3,$tags4],implode(' ',$text));
                        $this->db->query("INSERT INTO `text_seo` SET `url` = '" . $this->db->escape($_SERVER['REQUEST_URI']) . "', `text` = '" . $this->db->escape($data['text_seo']) . "'");
                        $this->db->query("UPDATE `text_seo` SET `text` = '" . $this->db->escape($data['text_seo']) . "' WHERE `url` = '" . $this->db->escape($_SERVER['REQUEST_URI']) . "' ");
                    }
                    else

                    {
                        $data['text_seo'] = $row['text'];
                        if(!empty($row['h1']))
                            $data['heading_title'] = $row['h1'];
                    }
                    $not_text_seo = array(
                        '/stolovaja/stil_sovremennyy/'=>'',
                        '/stoli/kra-na-virobnik_kitay/'=>'',
                        '/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-rukovoditeley/'=>'',
                        '/mjagkaja-mebel/'=>'',
                        '/stolovaja/stoly/'=>'',
                        '/stolovaja/stil_klassicheskiy/'=>'',
                        '/stoli/kra-na-virobnik_malayziya/'=>'',
                        '/ofis/kresla-ofisnie/tip-krisla_kresla-dlya-personala/'=>'',
                        '/mjagkaja-mebel/kreslo/'=>'',

                        '/stolovaja/kra-na-virobnik_italiya/'=>'',
                        '/stoli/stil_sovremennyy/'=>'',
                        '/ofis/kresla-ofisnie/tip-krisla_konferents-kresla/'=>'',
                        '/mjagkaja-mebel/kresla-barnye/'=>'',
                        '/stolovaja/obedennye-komplekty/'=>'',
                        '/stolovaja/kra-na-virobnik_kitay/'=>'',
                        '/stoli/stil_klassicheskiy/'=>'',
                        '/ofis/kresla-ofisnie/tip-krisla_geymerskie-kresla/'=>'',
                        '/mjagkaja-mebel/banketki/'=>'',
                        '/stolovaja/stulja/'=>'',
                        '/stolovaja/stoly/forma-stola_pryamougol-nyy/'=>'stolovaja/material-karkasa_derevo',
                        '/stoli/stil_loft/'=>'',
                        '/ofis/kresla-ofisnie/tip-krisla_detskie-ofisnye-kresla/'=>'',
                        '/mjagkaja-mebel/pufiki/'=>'',
                        '/stolovaja/barnye-stulja/'=>'',
                        '/stolovaja/stoly/forma-stola_kruglyy/'=>'',
                        '/stulya/kolir_belyy/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_fanera/'=>'',
                        '/stoli/'=>'',
                        '/stolovaja/stoly/forma-stola_kvadratnyy/'=>'',
                        '/stolovaja/stulja/kolir_belyy/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_dub/'=>'',
                        '/ofis/stoly-pismennye/'=>'',
                        '/stolovaja/stoly/forma-stola_oval-nyy/'=>'',
                        '/stulya/kolir_chernyy/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_yasen/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/'=>'',
                        '/stolovaja/stoly/material-stil-nitsi_derevo/'=>'',
                        '/stolovaja/stulja/kolir_chernyy/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_sosna/'=>'',
                        '/stulya/'=>'',
                        '/stolovaja/stoly/material-karkasa_hromirovannyy-metall/'=>'',
                        '/stulya/material-sidinnya_plastik/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_ol-ha/'=>'',
                        '/ofis/'=>'',
                        '/stolovaja/stoly/material-stil-nitsi_steklo/'=>'',
                        '/stulya/material-sidinnya_rotang/'=>'',
                        '/stoli/material-stil-nitsi_mdf/'=>'',
                        '/ofis/kresla-ofisnie/'=>'',
                        '/stolovaja/stoly/material-stil-nitsi_plastik/'=>'',
                        '/stulya/material-sidinnya_metall/'=>'',
                        '/stoli/material-stil-nitsi_dsp/'=>'',
                        '/ofis/ofisnye-stulja/'=>'',
                        '/stolovaja/stoly/material-karkasa_metall/'=>'',
                        '/stulya/osoblivosti_raskladnye/'=>'',
                        '/stoli/material-stil-nitsi_dub/'=>'',
                        '/ofis/karkasy-i-opory-dlja-stolov/'=>'',
                        '/stolovaja/stoly/tip-opori_kolonna/'=>'',
                        '/stulya/osoblivosti_s-podlokotnikami/'=>'',
                        '/stoli/material-stil-nitsi_yasen/'=>'',
                        '/gostinye/kresla/'=>'',
                        '/stolovaja/stoly/osoblivosti_raskladnye/'=>'',
                        '/stulya/osoblivosti_myagkie/'=>'',
                        '/stoli/osoblivosti_eko/'=>'',
                        '/gostinye/zhurnalnye-stoly/'=>'',
                        '/stolovaja/stoly/osoblivosti_skladnye/'=>'',
                        '/stulya/osoblivosti_reguliruemaya-vysota/'=>'',
                        '/stolovaja/stulja/kolir_venge/'=>'',
                        '/stolovaja/taburety-i-skami/'=>'',
                        '/stolovaja/stoly/osoblivosti_reguliruemaya-vysota/'=>'',
                        '/stulya/osoblivosti_vrashchenie-360/'=>'',
                        '/stolovaja/stulja/kolir_temnoe-derevo/'=>'',
                        '/stolovaja/stoly/osoblivosti_massiv-dereva/'=>'',
                        '/stulya/priznachennya_dom/'=>'',
                        '/ofis/kresla-ofisnie/osoblivosti_spinka-setka/'=>'',
                        '/stolovaja/opory-dlja-stolov/material-karkasa_derevo/'=>'',
                        '/stulya/priznachennya_kafe-i-bary/'=>'',
                        '/ofis/kresla-ofisnie/osoblivosti_ortopedicheskie/'=>'',
                        '/stolovaja/opory-dlja-stolov/'=>'',
                        '/stulya/priznachennya_ulitsa/'=>'',
                        '/stolovaja/stulja/kolir_prozrachnyy/'=>'',
                        '/stolovaja/opory-dlja-stolov/material-karkasa_chugun/'=>'',
                        '/ofis/stoly-pismennye/stil_sovremennyy/'=>'',
                        '/stulya/stil_hay-tek/'=>'',
                        '/stolovaja/obedennye-komplekty/forma-stola_kruglyy/'=>'',
                        '/ofis/stoly-pismennye/stil_loft/'=>'',
                        '/ofis/stil_hay-tek/'=>'',
                        '/stolovaja/obedennye-komplekty/stil_klassicheskiy/'=>'',
                        '/ofis/stoly-pismennye/kra-na-virobnik_kitay/'=>'',
                        '/ofis/kresla-ofisnie/osoblivosti_ergonomicheskie/'=>'',
                        '/stolovaja/obedennye-komplekty/kra-na-virobnik_malayziya/'=>'',
                        '/ofis/stoly-pismennye/osoblivosti_s-vydvizhnymi-yashchikami/'=>'',
                        '/ofis/kresla-ofisnie/osoblivosti_bez-rolikov/'=>'',
                        '/stolovaja/obedennye-komplekty/kra-na-virobnik_kitay/'=>'',
                        '/ofis/stoly-pismennye/osoblivosti_s-polkami/'=>'',
                        '/gostinye/kresla/osoblivosti_kresla-kachalki/'=>'',
                        '/stolovaja/stulja/material-sidinnya_kozhzam/'=>'',
                        '/ofis/stoly-pismennye/material-karkasa_derevo/'=>'',
                        '/stoli/stil_skandinavskiy/'=>'',
                        '/stolovaja/stulja/material-karkasa_derevo/'=>'',
                        '/ofis/stoly-pismennye/material-karkasa_metall/'=>'',
                        '/stolovaja/stulja/material-sidinnya_plastik/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/forma-stola_pryamougol-nyy/'=>'',
                        '/stolovaja/stulja/material-karkasa_metall/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/forma-stola_kruglyy/'=>'',
                        '/stolovaja/stulja/osoblivosti_raskladnye/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/forma-stola_oval-nyy/'=>'',
                        '/stolovaja/stulja/osoblivosti_s-podlokotnikami/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_derevo/'=>'',
                        '/stolovaja/stulja/osoblivosti_myagkie/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_topalit/'=>'',
                        '/stolovaja/stulja/osoblivosti_reguliruemaya-vysota/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/material-stil-nitsi_werzalit/'=>'',
                        '/stolovaja/stulja/stil_sovremennyy/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/kra-na-virobnik_avstriya/'=>'',
                        '/stolovaja/stulja/stil_klassicheskiy/'=>'',
                        '/stoli/stoleshnitsy-dlja-stolov/kra-na-virobnik_germaniya/'=>'',
                        '/stolovaja/stulja/stil_loft/'=>'',
                        '/stolovaja/opory-dlja-stolov/visota_bar-85-110-sm/'=>'',
                        '/stolovaja/stulja/kra-na-virobnik_italiya/'=>'',
                        '/stolovaja/opory-dlja-stolov/visota_kofe-do-60-sm/'=>'',
                        '/stolovaja/stulja/kra-na-virobnik_kitay/'=>'',
                        '/stolovaja/opory-dlja-stolov/stil_sovremennyy/'=>'',
                        '/stolovaja/stulja/kra-na-virobnik_malayziya/'=>'',
                        '/stolovaja/opory-dlja-stolov/stil_loft/'=>'',
                        '/stolovaja/barnye-stulja/material-sidinnya_kozhzam/'=>'',
                        '/ofis/stil_sovremennyy/'=>'',
                        '/stolovaja/barnye-stulja/material-sidinnya_tkan/'=>'',
                        '/ofis/stil_loft/'=>'',
                        '/stolovaja/barnye-stulja/material-sidinnya_kozha/'=>'',
                        '/ofis/kra-na-virobnik_italiya/'=>'',
                        '/stolovaja/barnye-stulja/material-karkasa_derevo/'=>'',
                        '/ofis/kra-na-virobnik_kitay/'=>'',
                        '/stolovaja/barnye-stulja/material-sidinnya_plastik/'=>'',
                        '/ofis/kresla-ofisnie/material-sidinnya_kozhzam/'=>'',
                        '/stolovaja/barnye-stulja/visota-posadki_vysokie-do-83-sm/'=>'',
                        '/ofis/kresla-ofisnie/material-sidinnya_tkan/'=>'',
                        '/stolovaja/barnye-stulja/visota-posadki_nizkie-do-65-sm/'=>'',
                        '/ofis/kresla-ofisnie/material-sidinnya_kozha/'=>'',
                        '/stolovaja/barnye-stulja/osoblivosti_raskladnye/'=>'',
                        '/ofis/kresla-ofisnie/osoblivosti_vrashchenie-360/'=>'',
                        '/stolovaja/barnye-stulja/osoblivosti_s-podlokotnikami/'=>'',
                        '/ofis/kresla-ofisnie/osoblivosti_na-rolikah/'=>'',
                        '/stolovaja/barnye-stulja/osoblivosti_myagkie/'=>'',
                        '/ofis/kresla-ofisnie/osoblivosti_s-podgolovnikom/'=>'',
                        '/stolovaja/barnye-stulja/osoblivosti_reguliruemaya-vysota/'=>'',
                        '/ofis/kresla-ofisnie/kra-na-virobnik_italiya/'=>'',
                        '/stolovaja/barnye-stulja/osoblivosti_vrashchenie-360/'=>'',
                        '/ofis/kresla-ofisnie/kra-na-virobnik_kitay/'=>'',
                        '/stolovaja/barnye-stulja/osoblivosti_so-spinkoy/'=>'',
                        '/ofis/stoly-pismennye/kolir_belyy/'=>'',
                        '/stolovaja/barnye-stulja/osoblivosti_bez-spinki/'=>'',
                        '/ofis/ofisnye-stulja/tip-stil-tsiv_stul-ya-na-rolikah/'=>'',
                        '/stolovaja/barnye-stulja/priznachennya_dom/'=>'',
                        '/ofis/ofisnye-stulja/tip-stil-tsiv_stul-ya-dlya-posetiteley/'=>'',
                        '/stolovaja/barnye-stulja/priznachennya_kafe-i-bary/'=>'',
                        '/ofis/ofisnye-stulja/tip-stil-tsiv_konferents-stul-ya/'=>'',
                        '/stulya/material-sidinnya_kozhzam/'=>'',
                        '/ofis/ofisnye-stulja/material-sidinnya_kozhzam/'=>'',
                        '/stulya/material-sidinnya_tkan/'=>'',
                        '/ofis/ofisnye-stulja/material-sidinnya_tkan/'=>'',
                        '/stulya/material-karkasa_derevo/'=>'',
                        '/ofis/ofisnye-stulja/material-sidinnya_kozha/'=>'',
                        '/stolovaja/stoly/visota_bar-85-110-sm/'=>'',
                        '/ofis/ofisnye-stulja/material-sidinnya_plastik/'=>'',
                        '/ofis/ofisnye-stulja/osoblivosti_s-podlokotnikami/'=>'',
                        '/ofis/ofisnye-stulja/osoblivosti_spinka-setka/'=>'',
                        '/gostinye/zhurnalnye-stoly/forma-stola_pryamougol-nyy/'=>'',
                        '/gostinye/zhurnalnye-stoly/forma-stola_kruglyy/'=>'',
                        '/gostinye/zhurnalnye-stoly/forma-stola_kvadratnyy/'=>'',
                        '/gostinye/zhurnalnye-stoly/forma-stola_oval-nyy/'=>'',
                        '/gostinye/zhurnalnye-stoly/material-stil-nitsi_derevo/'=>'',
                        '/gostinye/zhurnalnye-stoly/material-stil-nitsi_steklo/'=>'',
                        '/gostinye/zhurnalnye-stoly/material-stil-nitsi_plastik/'=>'',
                        '/gostinye/zhurnalnye-stoly/osoblivosti_massiv-dereva/'=>'',
                        '/gostinye/zhurnalnye-stoly/osoblivosti_na-rolikah/'=>'',
                        '/gostinye/zhurnalnye-stoly/stil_sovremennyy/'=>''
                    );
                    if(isset($not_text_seo[$_SERVER['REQUEST_URI']]))
                        $data['text_seo'] = '';

                }

                $data['faq_name'] = mb_strtolower($data['heading_title'], 'UTF-8');

                $data['faq_title'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_title'));
                $data['faq_title1'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_title1'));
                $data['faq_sub1'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_sub1'));
                $data['faq_title2'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_title2'));
                $data['faq_sub2'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_sub2'));
                $data['faq_title3'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_title3'));
                $data['faq_sub3'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_sub3'));
                $data['faq_title4'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_title4'));
                $data['faq_sub4'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_sub4'));
                $data['faq_title5'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_title5'));
                $data['faq_sub5'] = str_replace('{text}',$data['faq_name'],$this->language->get('faq_sub5'));

                // --- [dg][dEF] setup variables for faq block in footers [seoshield] --- //
                /*
                $arr_prod_prices = array();
                if (count($data['products']) > 0){
                    foreach ($data['products'] as $single_prod) {
                        $arr_prod_prices[] = (float)$single_prod['price_num'];
                    }
                    $data['min_price_prods'] = min($arr_prod_prices);
                    $data['max_price_prods'] = max($arr_prod_prices);
                }
                $filter_data_cat = array(
                    'filter_category_id' => $category_id,
                    'sort' => 'p.price',
                    'order' => 'DESC',
                    'start' => ($page - 1) * $limit,
                    'limit' => 1000,
                );
                $prods_of_cat = $this->model_catalog_product->getAllProductsOfCat($filter_data_cat);
                $arr_random_prods_of_cat = array();
                foreach ($prods_of_cat as $single_cat_prod) {
                    $arr_random_prods_of_cat[] = $single_cat_prod['name'].' - '.$single_cat_prod['sku'];
                }
                $arr_lower_price_prods = $arr_random_prods_of_cat;
                $arr_lower_price_prods = array_slice($arr_lower_price_prods, 0, 3);
                $data['arr_lower_price_prods'] = $arr_lower_price_prods;
                shuffle($arr_random_prods_of_cat);
                $arr_random_prods_of_cat = array_slice($arr_random_prods_of_cat, 0, 6);
                $data['arr_random_prods_of_cat'] = $arr_random_prods_of_cat;
                */


                $data['osobennosti_h2'] = '';
                $data['osobennosti_arr'] = array();
                if(isset($this->request->get['mfp']))
                {
                    if(str_replace('5f-','',$this->request->get['mfp'])!=$this->request->get['mfp'])
                    {
                        $name = $data['breadcrumbs'][count($data['breadcrumbs'])-2]['text'];
                        $data['osobennosti_h2'] = 'Смотрите '.$name.' другие особенности:';
                        $filters_sections = $this->model_catalog_category->getCategoryFiltersSection($category_id,5);

                        foreach ($filters_sections as $filters_section)
                        {
                            $url = $this->url->link('product/category', 'path=' . $category_id).$filters_section['filter_group_url'].'_'.$filters_section['filter_url'].'/';
                            if(str_replace($_SERVER['REQUEST_URI'],'',$url)==$url)
                            {
                                $data['osobennosti_arr'][] = array(
                                    'text' => $name.' '.$filters_section['name'],
                                    'href' => $url
                                );
                            }

                        }

                    }
                }

                $data['tsvet_h2'] = '';
                $data['tsvet_arr'] = array();
                if(isset($this->request->get['mfp']))
                {
                    if(str_replace('1f-','',$this->request->get['mfp'])!=$this->request->get['mfp'])
                    {
                        $name = $data['breadcrumbs'][count($data['breadcrumbs'])-1]['text'];
                        $data['tsvet_h2'] = 'В каталоге '.$name.' представлены следующие товары';
                        $filters_sections = $this->model_catalog_category->getCategoryFiltersSection($category_id,'2,3,4,15,30,31');
                        foreach ($filters_sections as $filters_section)
                        {
                            $url = $this->url->link('product/category', 'path=' . $category_id).$filters_section['filter_group_url'].'_'.$filters_section['filter_url'].'/';
                            if(str_replace($_SERVER['REQUEST_URI'],'',$url)==$url)
                            {
                                $data['tsvet_arr'][] = array(
                                    'text' => $name.' '.$filters_section['name'],
                                    'href' => $url
                                );
                            }
                        }
                    }
                }

                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/product/category.tpl', $data));
                }

            }
            else
            {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category_ajax.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/category_ajax.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/product/category_ajax.tpl', $data));
                }
            }
        } else {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/category', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $data['waiting'] = array();
            if (count($data['products']) > 0)
                foreach ($data['products'] as $i => $product) {
                    if (strpos($product['stock_status'], 'жида') !== false) {
                        $data['products'][] = $product;
                        $data['waiting'][] = $product;
                        unset($data['products'][$i]);
                    }
                }
//			print_r($data['products']); die();

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }

    }
}
