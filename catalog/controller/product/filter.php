<?php

class ControllerProductFilter extends Controller
{
    public function index()
    {
        //SELECT * FROM `oc_product_filter


        $this->load->model('catalog/category');
        $this->load->model('catalog/product');



        $product_filters = $this->db->query("SELECT * FROM `oc_product_filter")->rows;
        $product_filters_arr = array();
        foreach ($product_filters as $product_filter)
        {
            if(!isset($product_filters_arr[$product_filter['product_id']]))
                $product_filters_arr[$product_filter['product_id']] = array();
            $product_filters_arr[$product_filter['product_id']][$product_filter['filter_id']] = $product_filter['filter_id'];
        }



        $languages = $this->model_catalog_category->getMyLanguages();
        $urls = array(
            array('h1','link')
        );

        foreach ($languages as $language) {

            $filters_group = $this->db->query("SELECT * FROM `oc_filter_group_description` AS fgd LEFT JOIN `oc_filter_group` as fg ON fg.filter_group_id = fgd.filter_group_id WHERE fgd.language_id = '".$language['language_id']."' ORDER BY fg.sort_order ")->rows;
            $filters_group_tmp = array();
            foreach ($filters_group as $filter_group)
                $filters_group_tmp[$filter_group['filter_group_id']] = $filter_group;

            $filters = $this->db->query("SELECT * FROM `oc_filter_description` AS fd LEFT JOIN `oc_filter` as f ON f.filter_id = fd.filter_id WHERE fd.language_id = '".$language['language_id']."' ORDER BY f.sort_order ")->rows;
            $filters_tmp = array();
            foreach ($filters as $filter)
                $filters_tmp[$filter['filter_id']] = $filter;

            $cats = array();
            $this->getCategories($cats, $filters,0, '',$language);



            foreach ($cats as $cat)
            {

                $filter_data = array(
                    'filter_category_id' => $cat['category_id'],
                );
                $products = $this->model_catalog_product->getProductsFilter($filter_data);
                $product_filters_arr_tmp = array();
                $product_filters_arr_tmp2 = array();

                foreach ($products as $product) {

                    foreach ($product_filters_arr[$product['product_id']] as $filter_id) if(isset($filters_tmp[$filter_id])) {
                        $filter = $filters_tmp[$filter_id];

                        if(!isset($product_filters_arr_tmp2[$filter_id]))
                        {
                            $product_filters_arr_tmp2[$filter_id] = $filter_id;
                            $urls[]=array(
                                'name' => $cat['name'].' ('.$filters_group_tmp[$filter['filter_group_id']]['name'].' - '.$filter['name'].')',
                                'href' => $cat['href'].$filters_group_tmp[$filter['filter_group_id']]["filter_group_url"].'_'.$filter["filter_url"].'/'
                            );
                        }


                        if (!isset($product_filters_arr_tmp[$filter_id]))
                            $product_filters_arr_tmp[$filter_id] = array();

                        foreach ($product_filters_arr[$product['product_id']] as $filter_id_sub)
                        {
                            $filter_sub = $filters_tmp[$filter_id_sub];
                            if (
                                $filter_id != $filter_id_sub &&
                                !isset($product_filters_arr_tmp[$filter_id_sub][$filter_id]) &&
                                !isset($product_filters_arr_tmp[$filter_id][$filter_id_sub]) &&
                                $filter['filter_group_id']!=$filter_sub['filter_group_id'] &&
                                isset($filters_group_tmp[$filter_sub['filter_group_id']])
                            ) {
                                $product_filters_arr_tmp[$filter_id][$filter_id_sub] = $filter_id_sub;
                                if($filters_group_tmp[$filter['filter_group_id']]['sort_order']<$filters_group_tmp[$filter_sub['filter_group_id']]['sort_order'])
                                {
                                    $urls[]=array(
                                        'name' => $cat['name'].': '.$filters_group_tmp[$filter['filter_group_id']]['name'].' - '.$filter['name'].', '.$filters_group_tmp[$filter_sub['filter_group_id']]['name'].' - '.$filter_sub['name'],
                                        'href' => $cat['href'].'?mfp='.$filter['filter_group_id'].'f-'.$filters_group_tmp[$filter['filter_group_id']]['filter_group_url'].'['.$filter['filter_id'].'],'.$filter_sub['filter_group_id'].'f-'.$filters_group_tmp[$filter_sub['filter_group_id']]['filter_group_url'].'['.$filter_sub['filter_id'].']'
                                    );
                                }
                                else
                                {
                                    $urls[]=array(
                                        'name' => $cat['name'].': '.$filters_group_tmp[$filter_sub['filter_group_id']]['name'].' - '.$filter_sub['name'].', '.$filters_group_tmp[$filter['filter_group_id']]['name'].' - '.$filter['name'],
                                        'href' => $cat['href'].'?mfp='.$filter_sub['filter_group_id'].'f-'.$filters_group_tmp[$filter_sub['filter_group_id']]['filter_group_url'].'['.$filter_sub['filter_id'].'],'.$filter['filter_group_id'].'f-'.$filters_group_tmp[$filter['filter_group_id']]['filter_group_url'].'['.$filter['filter_id'].']'
                                    );
                                }

                            }
                        }

                    }

                }




            }

        }

        require_once($_SERVER['DOCUMENT_ROOT'].'/system/PHPExcel/Classes/PHPExcel.php');

        require_once($_SERVER['DOCUMENT_ROOT'].'/system/PHPExcel/Classes/PHPExcel/Writer/Excel5.php');

        $xls = new PHPExcel();
        $xls->setActiveSheetIndex(0);
        $xls->getActiveSheet()->fromArray($urls, null, 'A1');

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=cat_filters.xls" );

        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');

    }

    protected function getCategories(&$cats, $filters,$parent_id, $current_path='',$lang) {

        $this->load->model('catalog/category');


        $output = '';

        $results = $this->model_catalog_category->getCategoriesMod($parent_id,$lang['language_id']);



        foreach ($results as $result) {

            if (!$current_path) {
                $new_path = $result['category_id'];
            } else {
                $new_path = $current_path . '_' . $result['category_id'];
            }

            $this->load->model('catalog/category');

            $cats[]= array(
                'category_id' => $result['category_id'],
                'name' => $result['name'],
                'href' => $this->url->link('product/category', 'path=' . $new_path, $secure = false, $lang['code'] )
            );
            /*
            foreach ($filters as $filter) {
                if (!$current_path) {
                    $new_path = $filter['category_id'];
                } else {
                    $new_path = $current_path . '_' . $filter['category_id'];
                }

                if ($filter["language_id"] == 1){
                    $filterl = 'ru';
                }else{
                    $filterl = 'ua';
                }

                if( $result["category_id"] == $filter["category_id"] and $result['status'] == 1 )
                {
                    $filter_url = $this->url->link('product/category', 'path=' . $new_path, $secure = false, $filterl,$http).$filter["filter_group_url"].'_'.$filter["filter_url"].'/';
                    $cats[]= array(
                        'category_id' => $result['category_id'],
                        'name' => $filter['name'],
                        'href' => $filter_url,
                        'filter' => $filter,
                    );

                }
            }
            */
            $this->getCategories($cats,$filters, $result['category_id'], $new_path,$lang);
        }
        return $output;
    }
}
