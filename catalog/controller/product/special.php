<?php
class ControllerProductSpecial extends Controller {
	public function index() {
		$this->load->language('product/special');

		$this->load->model('catalog/product');
		$this->load->model('catalog/review');

		$this->load->model('tool/image');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_product_limit');
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/special', $url)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');
		$data['button_continue'] = $this->language->get('button_continue');

		$data['compare'] = $this->url->link('product/compare');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit,
			'limit' => 10000
		);

        $this->load->model('catalog/supplier');

        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach ($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;


        $this->load->model('localisation/sale_status');

        $data['sale_statuses'] = $this->model_localisation_sale_status->getSaleStatuses();

		$product_total = $this->model_catalog_product->getTotalProductSpecials();

		$results = $this->model_catalog_product->getProductSpecials($filter_data);

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
            }

            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                //$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                $price = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                if ($suppliers[$result['supplier_id']] > 0)
                    $price =ceil($price*$suppliers[$result['supplier_id']]['coefficient']);
                $price_num = $price;
                $price = $this->currency->format($price);
            } else {
                $price = false;
            }

            if ((float)$result['special']) {
                //$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                $special = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
                //if ($suppliers[$result['supplier_id']] > 0)
                //    $special =ceil($special*$suppliers[$result['supplier_id']]['coefficient']);
                $special_num = $special;
                $special = $this->currency->format($special);
            } else {
                $special = false;
            }

            if ($this->config->get('config_tax')) {
                $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
            } else {
                $tax = false;
            }

            if ($this->config->get('config_review_status')) {
                $rating = (int)$result['rating'];
            } else {
                $rating = false;
            }
            $json = array();


            if (isset($this->request->post['product_id'])) {
                $product_id = $this->request->post['product_id'];
            } else {
                $product_id = 0;
            }


            $this->load->model('catalog/product');


            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info && false) {
                $options = array();
                foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                    $product_option_value_data = array();
                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                //$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                $price_opt = $this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                                $supplier_id = $product_info['supplier_id'];
                                if ($suppliers[$supplier_id] > 0)
                                    $price_opt =ceil($price_opt*$suppliers[$supplier_id]['coefficient']);
                                $price_opt = $this->currency->format($price_opt);
                            } else {
                                $price_opt = false;
                            }

                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price_opt,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }
                    $options[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required'],
                        'sort_option' => $option['sort_option']
                    );
                    $options['product_id'] = $product_info['product_id'];
                    $options['name'] = $product_info['name'];
                }
                if (!$json) {
                    $json = $options;
                }
            }

            $options_p = array();
            foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
                $product_option_value_data_p = array();
                foreach ($option['product_option_value'] as $option_value) {
                    if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {

                            $price_opt = $this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                            $supplier_id = $result['supplier_id'];
                            if ($suppliers[$supplier_id] > 0)
                                $price_opt =ceil($price_opt*$suppliers[$supplier_id]['coefficient']);
                            $price_opt = $this->currency->format($price_opt);

                        } else {
                            $price_opt = false;
                        }


                        //$_special_p = ceil($special_p).' грн.';
                        $product_option_value_data_p[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            //'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'image' => $this->model_tool_image->resize($option_value['image'], 32, 32),
                            'quantity' => $option_value['quantity'],
                            'subtract' => $option_value['subtract'],
                            'price' => $price_opt,
                            'price_prefix' => $option_value['price_prefix'],
                            'weight' => $option_value['weight'],
                            'image_prod' => $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
                            'supplier_id' => $suppliers[$supplier_id],
                            'weight_prefix' => $option_value['weight_prefix']
                        );
                        if(isset( $data['option_value_filter'][$option_value['option_value_id']]) && !empty($option_value['image_prod']))
                            $image = $this->model_tool_image->resize($option_value['image_prod'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));


                    }
                }
                $options_p[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data_p,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required'],
                    'sort_option' => $option['sort_option']
                );
            }



            $special_info_tmp = $this->model_catalog_product->getProductSpecial($result['product_id']);

            if(!empty($special_info_tmp)){
                if(strtotime($special_info_tmp['date_end']) - time()>0)
                    $result['sale_status_id']=1;
                else
                    $result['sale_status_id']=0;
            }



            if($special_num)
                $this->model_catalog_product->setPriceCurrent( $result['product_id'],$special_num);
            else
                $this->model_catalog_product->setPriceCurrent( $result['product_id'],$price_num);

            if (!$result['price_show'])
                $price = '';


            $reviews_item = array();
            $reviews = $this->model_catalog_review->getReviewsByProductId($result['product_id']);

            foreach ($reviews as $review) if((int)$result['rating']>0){
                $reviews_item[] = array(
                    'author'     => $review['author'],
                    'text'       => str_replace('"','\"',nl2br($review['text'])),
                    'rating'     => (int)$review['rating'],
                    'date_added' => date('Y-m-d', strtotime($review['date_added']))
                );
                $data['reviews_rating']+=(int)$result['rating'];
            }

            $data['products'][] = array(
                'product_id' => $result['product_id'],
                'name' => $result['name'].' - '.$result['sku'],
                'thumb' => $image,
                'reviews_item' => $reviews_item,
                'sku' => (empty($result['sku'])) ? '' : $this->language->get('text_sku') . ' ' . $result['sku'],
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                'price' => $price,
                'old_price' => $result['old_price'],
                'youtube' => $result['youtube'],
                'sale_status_name' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['name']:'',
                'sale_status_color' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? $data['sale_statuses'][$result['sale_status_id']]['color']:'',
                'sale_status_image' =>isset($data['sale_statuses'][$result['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$result['sale_status_id']]['image']:'',
                'price_num' => $price_num,
                'special' => $special,

                'special_end' => $special_end,

                'stock_status' => $result['stock_status'],
                'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                'tax' => $tax,
                'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating' => $result['rating'],
                'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id']),
                //'options'   => $this->model_catalog_product->getProductOptions($result['product_id']),
                'options' => $options_p,
                'bestseller' => $result['upc'],
                'recommended' => $result['ean'],
                'privat' => $result['product_pp'] || $result['product_ii']? 1 : 0,
            );
        }

		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['sorts'] = array();

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href'  => $this->url->link('product/special', 'sort=p.sort_order&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'value' => 'pd.name-ASC',
			'href'  => $this->url->link('product/special', 'sort=pd.name&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'value' => 'pd.name-DESC',
			'href'  => $this->url->link('product/special', 'sort=pd.name&order=DESC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_asc'),
			'value' => 'ps.price-ASC',
			'href'  => $this->url->link('product/special', 'sort=ps.price&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_desc'),
			'value' => 'ps.price-DESC',
			'href'  => $this->url->link('product/special', 'sort=ps.price&order=DESC' . $url)
		);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC',
				'href'  => $this->url->link('product/special', 'sort=rating&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => 'rating-ASC',
				'href'  => $this->url->link('product/special', 'sort=rating&order=ASC' . $url)
			);
		}

		$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/special', 'sort=p.model&order=ASC' . $url)
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_model_desc'),
			'value' => 'p.model-DESC',
			'href'  => $this->url->link('product/special', 'sort=p.model&order=DESC' . $url)
		);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['limits'] = array();

		$limits = array_unique(array($this->config->get('config_product_limit'), 25, 50, 75, 100));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('product/special', $url . '&limit=' . $value)
			);
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('product/special', $url . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

		// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
		if ($page == 1) {
		    $this->document->addLink($this->url->link('product/special', '', 'SSL'), 'canonical');
		} elseif ($page == 2) {
		    $this->document->addLink($this->url->link('product/special', '', 'SSL'), 'prev');
		} else {
		    $this->document->addLink($this->url->link('product/special', 'page='. ($page - 1), 'SSL'), 'prev');
		}

		if ($limit && ceil($product_total / $limit) > $page) {
		    $this->document->addLink($this->url->link('product/special', 'page='. ($page + 1), 'SSL'), 'next');
		}

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/special.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/special.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/product/special.tpl', $data));
		}
	}
}
