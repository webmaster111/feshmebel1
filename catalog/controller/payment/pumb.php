<?php
class ControllerPaymentPumb extends Controller {
	public function index() {
		
		$this->load->language('payment/pumb');
		
		$data['button_confirm'] = $this->language->get('button_confirm');
		
		$this->load->model('checkout/order');
		
		$order_id = $this->session->data['order_id'];
		
		$data['merch_id'] = $this->config->get('pumb_merch_id');
		$data['account_id'] = $this->config->get('pumb_account_id');
		$data['live_demo'] = $this->config->get('pumb_live_demo');
		$data['live_url'] = $this->config->get('pumb_live_url');
		$data['demo_url'] = $this->config->get('pumb_demo_url');
		$data['order_info'] = $this->model_checkout_order->getOrder($order_id);
		$data['order_info']['language_code'] = substr($data['order_info']['language_code'], 0, 2);
		
		
		$data['text_secure'] = $this->language->get('text_secure');

		$data['back_url_f'] = $this->url->link('checkout/failure', '', 'SSL');
		$data['back_url_s'] = $this->url->link('checkout/success', '', 'SSL');
		

		return $this->load->view('default/template/payment/pumb.tpl', $data);
	}

	public function check() {
		
		$data['live_demo'] = $this->config->get('pumb_live_demo');
	
		if ($data['live_demo'] == 0) {
		
			$log_path = DIR_LOGS.'/pumb.log';
			
			if (file_exists($log_path)) {
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . $_SERVER['REQUEST_URI'] . "\n", FILE_APPEND);
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . print_r($_POST, true) . "\n", FILE_APPEND);
			} else {
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . $_SERVER['REQUEST_URI'] . "\n");
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . print_r($_POST, true) . "\n", FILE_APPEND);
			}
		
		}
		
		if (isset($_GET['o_order_id']) && !empty($_GET['o_order_id'])) {
			$order_id = $_GET['o_order_id'];
			$merchant_trx = $order_id.'_'.time();
		} else 
			$error[] = 'Invalid `o_order_id`';
		
		if (isset($_GET['o_amount']) && !empty($_GET['o_amount'])) 
			$amount = $_GET['o_amount'] * 100;
		else 
			$error[] = 'Invalid `o_amount`';
		
		if (isset($_GET['o_account_id']) && !empty($_GET['o_account_id'])) $account_id = $_GET['o_account_id'];
		
		if (!isset($error) or empty($error)) {
		
			$response = 
				'<payment-avail-response>
					<result>
						<code>1</code> 
						<desc>OK</desc> 
					</result> 
					<merchant-trx>'.$merchant_trx.'</merchant-trx> 
					<purchase> 
						<shortDesc>Оплата заказа №'.$order_id.'</shortDesc> 
						<longDesc>Оплата заказа №'.$order_id.'</longDesc> 
						<account-amount> 
							<amount>'.$amount.'</amount> 
							<currency>980</currency>
							<exponent>2</exponent>';
			
			if (isset($account_id)) {
				$response .= 
							'<id>'.$account_id.'</id>';
			}
			
			$response .= 
					'</account-amount> 
				</purchase> 
			</payment-avail-response>';
		
		} else {
			$response =
				'<payment-avail-response>
					<result>
						<code>2</code>
						<desc>'.implode(", ", $error).'</desc>
					</result>
				</payment-avail-response>';
		}
		
		header('Content-type: text/xml; charset=utf-8'); 
		echo $response;		
		
	}
	
	public function register() {
		
		$data['live_demo'] = $this->config->get('pumb_live_demo');
		
		if ($data['live_demo'] == 0) {
		
			$log_path = DIR_LOGS.'/pumb.log';
			
			if (file_exists($log_path)) {
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . $_SERVER['REQUEST_URI'] . "\n", FILE_APPEND);
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . print_r($_POST, true) . "\n", FILE_APPEND);
			} else {
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . $_SERVER['REQUEST_URI'] . "\n");
				file_put_contents($log_path, date('d.m.Y H:i:s ') . $_SERVER['REMOTE_ADDR'] . ' ' . print_r($_POST, true) . "\n", FILE_APPEND);
			}		
		}

		$full_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$signed_data = substr($full_url, 0, strpos($full_url, "&signature"));
		$signature = base64_decode(urldecode(substr($full_url, strpos($full_url, "&signature") + strlen("&signature="))));  
		
		if ($data['live_demo'] > 0) $cert = $this->config->get('pumb_signature_live');
		else $cert = $this->config->get('pumb_signature_demo');
		
		if (isset($cert) and !empty($cert)) {
			$pubkeyid = openssl_get_publickey($cert);
		} else {
			//$error[] = 'Invalid parameter `Signature`';
		}
		
		
		if (isset($signed_data) and !empty($signed_data) and
			isset($signature) and !empty($signature) and
			isset($pubkeyid) and !empty($pubkeyid)) {
			$sing_status = openssl_verify($signed_data, $signature, $pubkeyid);
			openssl_free_key($pubkeyid);
		} else {
			//$error[] = 'Incorrect parameters for openssl_verify()';
		}
		
		if (!isset($sing_status) or $sing_status < 1) {
			//$error[] = 'Signature status is not valid';
		}
		
		
		if (!isset($error) or empty($error)) {
			$response = 
			'<register-payment-response>
				<result>
					<code>1</code>
					<desc>OK</desc>
				</result>
			</register-payment-response>';
			
			if (isset($_GET['result_code']) && !empty($_GET['result_code'])) {
				$result_code = $_GET['result_code'];
				
				$this->load->model('checkout/order');
			
				if ($result_code == 1) {
					$this->model_checkout_order->addOrderHistory($_GET['o_order_id'], $this->config->get('pumb_order_status_success_id'));
				} else {
					$this->model_checkout_order->addOrderHistory($_GET['o_order_id'], $this->config->get('pumb_order_status_failed_id'));
				}			
			}
		} else {
			$response = 
			'<register-payment-response>
				<result>
					<code>2</code>
					<desc>'.implode(", ", $error).'</desc>
				</result>
			</register-payment-response>';
		}
		
		header('Content-type: text/xml; charset=utf-8');
		echo $response;
		
	}
	
	public function confirm() {
		
		if ($this->session->data['payment_method']['code'] == 'pumb') {
			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('pumb_order_status_id'));
		}
	}
}
?>