<?php
class ControllerPaymentLiqPay extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['button_confirm_p'] = $this->language->get('button_confirm_p');

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		/*$data['action'] = 'https://liqpay.com/?do=clickNbuy';

		$xml  = '<request>';
		$xml .= '	<version>1.2</version>';
		$xml .= '	<result_url>' . $this->url->link('checkout/success', '', 'SSL') . '</result_url>';
		//$xml .= '	<result_url>' . $this->url->link('payment/liqpay/callback', '', 'SSL') . '</result_url>';
		$xml .= '	<server_url>' . $this->url->link('payment/liqpay/callback', '', 'SSL') . '</server_url>';
		$xml .= '	<merchant_id>' . $this->config->get('liqpay_merchant') . '</merchant_id>';
		$xml .= '	<order_id>' . $this->session->data['order_id'] . '</order_id>';
		$xml .= '	<amount>' . $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false) . '</amount>';
		$xml .= '	<currency>' . $order_info['currency_code'] . '</currency>';
		$xml .= '	<description>' . $this->config->get('config_name') . ' ' . $order_info['payment_firstname'] . ' ' . $order_info['payment_address_1'] . ' ' . $order_info['payment_address_2'] . ' ' . $order_info['payment_city'] . ' ' . $order_info['email'] . '</description>';
		$xml .= '	<default_phone></default_phone>';
		$xml .= '	<pay_way>' . $this->config->get('liqpay_type') . '</pay_way>';
		$xml .= '</request>';*/

		//if($order_info['twostage_check'] > 0){
			$data['action'] = 'https://www.liqpay.ua/api/3/checkout';
			
			$description = 'Order #'.$this->session->data['order_id'];
			$order_id .= '#'.time();
			//$result_url = $this->url->link('payment/liqpay/server', '', 'SSL');
			//$server_url = $this->url->link('payment/liqpay/server', '', 'SSL');
			$result_url = $this->url->link('checkout/success', '', 'SSL');
			$server_url = $this->url->link('payment/liqpay/callback2', '', 'SSL');

			$private_key = $this->config->get('liqpay_signature');
			$public_key = $this->config->get('liqpay_merchant');
			$type = 'buy';
			$currency = $order_info['currency_code'];
			$amount = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
			
			$send_data = array('version'    => '3', 
                            //'sandbox'    => '1',   
                          'action'      => 'hold', 
                          'public_key'  => $public_key,
                          'amount'      => $amount,
                          'currency'    => $currency,
                          'description' => $description,
                          'order_id'    => $order_id,
                          'type'        => $type,
                          'language'    => 'ru',
                          'server_url'  => $server_url,
                          'result_url'  => $result_url);
			
			$json = base64_encode(json_encode($send_data));
			
			$data['data'] = $json;
			$data['signature'] = base64_encode(sha1($this->config->get('liqpay_signature') . $json . $this->config->get('liqpay_signature'), true));		
			$data['url_confirm'] = $this->url->link('payment/liqpay/confirm2');
		/*}else{		
			$data['xml'] = base64_encode($xml);
			$data['signature'] = base64_encode(sha1($this->config->get('liqpay_signature') . $xml . $this->config->get('liqpay_signature'), true));		
			$data['url_confirm'] = $this->url->link('payment/liqpay/confirm');
		}*/
		
		/*if($order_info['twostage_check'] > 0){
				return $this->load->view($this->config->get('config_template') . '/template/payment/liqpay2.tpl', $data);
		}else{*/
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/liqpay2.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/payment/liqpay2.tpl', $data);
			} else {
				return $this->load->view('default/template/payment/liqpay.tpl', $data);
			}
		//}
	}

	public function confirm() {
		$this->load->model('checkout/order'); 
        $order = $this->session->data['order_id'];
		$liqpay_order_status_id = $this->config->get('liqpay_order_status_id');
        //$this->model_checkout_order->confirm($order, $this->config->get('config_order_status_id'), 'unpaid');
        //$this->model_checkout_order->confirm($this->session->data['order_id'], $liqpay_order_status_id);
		$this->model_checkout_order->setOrderStatus($this->session->data['order_id'], $liqpay_order_status_id);
	}
	
	public function confirm2() {
		$this->load->model('checkout/order'); 
        $order = $this->session->data['order_id'];
		$liqpay_order_status_id = $this->config->get('liqpay_order_status_id');
        //$this->model_checkout_order->confirm($order, $this->config->get('config_order_status_id'), 'unpaid');
        //$this->model_checkout_order->confirm($this->session->data['order_id'], $liqpay_order_status_id);
		$this->model_checkout_order->setOrderStatus($this->session->data['order_id'], 1);
	}
	
	 /**
     * get real order ID
     *
     * @return string
     */
    public function getRealOrderID($order_id)
    {
        $real_order_id = explode('#', $order_id);
        return $real_order_id[0];
    }
	
	public function callback() {
		$xml = base64_decode($this->request->post['operation_xml']);
		$signature = base64_encode(sha1($this->config->get('liqpay_signature') . $xml . $this->config->get('liqpay_signature'), true));

		$posleft = strpos($xml, 'order_id');
		$posright = strpos($xml, '/order_id');

		$order_id = substr($xml, $posleft + 9, $posright - $posleft - 10);

		if ($signature == $this->request->post['signature']) {
			$this->load->model('checkout/order');

			/* $liqpay_order_status_id = $this->config->get('liqpay_order_status_id');
			$this->model_checkout_order->setOrderStatus($order_id, $liqpay_order_status_id); */
			
			$this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'));
			
			$this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
		}
	}
	
	public function callback2() {
		if (!$posts = $this->getPosts()) { die('Posts error'); }

        list(
            $data,
            $signature
        ) = $posts;
        
        if(!$data || !$signature) {die("No data or signature");}

        $parsed_data = json_decode(base64_decode($data), true);

        $received_public_key = (isset($parsed_data['public_key']))?$parsed_data['public_key']:'';
        $order_id            = (isset($parsed_data['order_id']))?$parsed_data['order_id']:'';
        $status              = (isset($parsed_data['status']))?$parsed_data['status']:'';
        $sender_phone        = (isset($parsed_data['sender_phone']))?$parsed_data['sender_phone']:'';
        $amount              = (isset($parsed_data['amount']))?$parsed_data['amount']:'';
        $currency            = (isset($parsed_data['currency']))?$parsed_data['currency']:'';
        $transaction_id      = (isset($parsed_data['transaction_id']))?$parsed_data['transaction_id']:'';

        $real_order_id = $this->getRealOrderID($order_id);

        if ($real_order_id <= 0) { die("Order_id real_order_id < 0"); }

        $this->load->model('checkout/order');
        if (!$this->model_checkout_order->getOrder($real_order_id)) { die("Order_id fail");}

        /* $private_key = $this->config->get('liqpay_private_key');
        $public_key  = $this->config->get('liqpay_public_key');

        $generated_signature = base64_encode(sha1($private_key.$data.$private_key, 1));

        if ($signature  != $generated_signature) { die("Signature secure fail"); }
        if ($public_key != $received_public_key) { die("public_key secure fail"); } */
        
        if ($status == 'success') {
            //$this->model_checkout_order->addOrderHistory($real_order_id, $this->config->get('liqpay_order_status_id'));
            $this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
        } 
		/* elseif (preg_match('/wait|hold|processing/',$status)) {
            $this->model_checkout_order->addOrderHistory($real_order_id, 1);
            $this->response->redirect($this->url->link('checkout/wait', '', 'SSL'));        
        } else {
            //$this->session->data['error'] = $this->session->data['status'];
            $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));
        }  */ 
	}
	
}