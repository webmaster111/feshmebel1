<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		
		$this->load->model('catalog/category');
		$filterDescriptions = array();
		$filterDescriptions = $this->model_catalog_category->getFilterDescriptions();
		$showmeta = 0;
		foreach ($filterDescriptions as $row){
			if($_SERVER['REQUEST_URI'] == $row["filter_url"])
			{
				$filter_metatitle = html_entity_decode($row["meta_title"], ENT_QUOTES, 'UTF-8');
				$filter_metadescr = html_entity_decode($row["meta_description"], ENT_QUOTES, 'UTF-8');
				$showmeta = $row["showmeta"];
			}				
		}
		
		// start Tkach web-promo
		if(isset($filter_metatitle) && $filter_metatitle != '' && $showmeta){
			$data['title'] = $filter_metatitle;
		}else{ 
			if (defined('TITLE') and TITLE != '' ){
				$data['title'] = TITLE;
			}else{
				$data['title'] = $this->document->getTitle();	
			}
		}


		if(isset($filter_metadescr) && $filter_metadescr != '' && $showmeta){
			$data['description'] = $filter_metadescr;
		}else{
			if (defined('TITLE') and TITLE != '' ){
				$data['description'] = DESCRIPTION;
			}else{
				$data['description'] = $this->document->getDescription();
			}// end
		}
        if($this->request->get['route']=='product/category')
        {
            $data['description'] = $this->document->getDescription();
        }
		$data['base'] = $server;
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		//start Tkach web-promo
		
		if ( $this->language->get('code') == 'ru' )
		{
			if($this->request->get['mfp'] && _FILTER_ID_ && _FILTER_GROUP_ID_){	
				$data['alternate'].= '
				<link rel="alternate" href="'.str_replace('?page=', 'page-', $server._CURRPAGE_LANG1_URL_).'" hreflang="ru-ua" />
				<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.'ua/'._CURRPAGE_LANG2_URL_).'" hreflang="uk-ua" />';
			}else{
				if(strpos($_SERVER['REQUEST_URI'], 'page')){
					$data['alternate'].= '
						<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.substr($_SERVER['REQUEST_URI'], 1)).'/" hreflang="ru-ua" />
						<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.'ua'.$_SERVER['REQUEST_URI']).'/" hreflang="uk-ua" />';
				}else{
					$data['alternate'].= '
						<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.substr($_SERVER['REQUEST_URI'], 1)).'" hreflang="ru-ua" />
						<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.'ua'.$_SERVER['REQUEST_URI']).'" hreflang="uk-ua" />';
				}
				
			}
		}else{
			if($this->request->get['mfp'] && _FILTER_ID_ && _FILTER_GROUP_ID_){
				$data['alternate'].= '
				<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.str_replace('ua/','',_CURRPAGE_LANG1_URL_)).'" hreflang="ru-ua" />
				<link rel="alternate" href="'.str_replace('?page=', 'page-',$server._CURRPAGE_LANG2_URL_).'" hreflang="uk-ua" />';
			}else{
				if(strpos($_SERVER['REQUEST_URI'], 'page')){
					$data['alternate'].= '
						<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.str_replace('/ua/','',$_SERVER['REQUEST_URI'])).'/" hreflang="ru-ua" />
						<link rel="alternate" href="'.str_replace('?page=', 'page-',$server.substr($_SERVER['REQUEST_URI'], 1)).'/" hreflang="uk-ua" />';
				}else{
					$data['alternate'].= '
						<link rel="alternate" href="'.str_replace('?page=', 'page-', $server.str_replace('/ua/','',$_SERVER['REQUEST_URI'])).'" hreflang="ru-ua" />
						<link rel="alternate" href="'.str_replace('?page=', 'page-',$server.substr($_SERVER['REQUEST_URI'], 1)).'" hreflang="uk-ua" />';
				}
				
			}
		}


		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->model('design/bannerhome');
		$banner_home = $this->model_design_bannerhome->getBanner(1);
		$data['banner_home'] = $banner_home[0];
		
		$this->load->language('common/header');
		// Top menu
		$data['top_menu_about'] = $this->language->get('top_menu_about');
		$data['top_menu_delivery'] = $this->language->get('top_menu_delivery');
		$data['top_menu_guaranty'] = $this->language->get('top_menu_guaranty');
		$data['top_menu_exchange'] = $this->language->get('top_menu_exchange');
		$data['top_menu_discount'] = $this->language->get('top_menu_discount');
		$data['top_menu_articles'] = $this->language->get('top_menu_articles');
		$data['top_menu_help'] = $this->language->get('top_menu_help');
        $data['top_menu_try_on'] = $this->language->get('top_menu_try_on');

		$data['site_title'] = $this->language->get('site_title');
		$data['text_home'] = $this->language->get('text_home');
        $data['text_work'] = $this->language->get('text_work');
        $data['text_day_off'] = $this->language->get('text_day_off');
        $data['text_call'] = $this->language->get('text_call');

        $data['text_free'] = $this->language->get('text_free');


		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');
        $data['text_compare'] = $this->language->get('text_compare');
        $data['text_articles'] = $this->language->get('text_articles');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['compare'] = $this->url->link('product/compare');
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['register'] = $this->url->link('account/register', '', 'SSL');
		$data['login'] = $this->url->link('account/login', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');
		
		// url links
		// Start Tkach web-promo			
		$data['url_about']	  = $this->url->link('information/information&information_id=10');
		$data['url_delivery'] = $this->url->link('information/information&information_id=4');
		$data['url_guaranty'] = $this->url->link('information/information&information_id=9');
		$data['url_exchange'] = $this->url->link('account/return/add');
		$data['url_discount'] = $this->url->link('information/information&information_id=11');
        $data['url_try_on']	  = $this->url->link('information/information&information_id=17');
        $data['url_articles']	  = $this->url->link('newsblog_category_id=1');

        $data['url_search']	  = $this->url->link('product/search');
		// end web-promo
		
		$status = true;

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'id'       => $category['category_id'],
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}
        $data['route'] =  str_replace('/', '-', $this->request->get['route']);


        $mfp_index = array();
        if(isset($_GET['mfp']))
        {
            preg_match_all('/\[(.*?)\]/',$_GET['mfp'],$matches);

            foreach ($matches[1] as $item)
            {

                $item=explode(',',$item);
                if(!isset($mfp_index[count($item)]))
                    $mfp_index[count($item)]=0;
                $mfp_index[count($item)]++;
            }
        }
        $data['noindex']=0;
        if(count($mfp_index)>1 || (count($mfp_index)==1 && !isset($mfp_index[1])) || (count($mfp_index)==1 && isset($mfp_index[1]) && $mfp_index[1]>2))
            $data['noindex']=1;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header_new.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/header_new.tpl', $data);
		} else {
			return $this->load->view('default/template/common/header.tpl', $data);
		}
	}
}
