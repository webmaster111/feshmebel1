<?php
class ControllerCommonHome extends Controller {
	public function index() {

        $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
        $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

        $this->load->language('newsblog/category');
        $data['blog_title'] = $this->language->get('blog_title');
        $data['text_empty'] = $this->language->get('text_empty');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['text_refine'] = $this->language->get('text_refine');
        $data['text_more'] = $this->language->get('text_more');
        $data['text_attributes'] = $this->language->get('text_attributes');

        $data['popular_head_title'] = $this->language->get('popular_head_title');
        $data['brand_head_title'] = $this->language->get('brand_head_title');
        $data['projects_head_title'] = $this->language->get('projects_head_title');
        $data['partner_head_title'] = $this->language->get('partner_head_title');
        $data['advice_head_title'] = $this->language->get('advice_head_title');

        $data['url_articles']	  = $this->url->link('newsblog_category_id=1');
        $data['text_url_articles'] = $this->language->get('text_url_articles');
        $this->load->model('newsblog/article');
        $this->load->model('tool/image'); 
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "newsblog_article WHERE page = '" . $_SERVER['REQUEST_URI'] . "'");

        $date_format = $this->language->get('date_format_short');

        $data['articles'] = array();

        foreach ($query->rows as $article) {
            $result = $this->model_newsblog_article->getArticle($article['article_id']);

            if ($result['image']) {
                $original 	= HTTP_SERVER.'image/'.$result['image'];
                $thumb 		= $this->model_tool_image->resize($result['image'], 600, 600);
            } else {
                $original 	= '';
                $thumb 		= '';	//or use 'placeholder.png' if you need
            }

            if($this->config->get('config_language_id') == 1){
                $_monthsList = array(
                    '.01.' => 'января',
                    '.02.' => 'февраля',
                    '.03.' => 'марта',
                    '.04.' => 'апреля',
                    '.05.' => 'мая',
                    '.06.' => 'июня',
                    '.07.' => 'июля',
                    '.08.' => 'августа',
                    '.09.' => 'сентября',
                    '.10.' => 'октября',
                    '.11.' => 'ноября',
                    '.12.' => 'декабря'
                );
            }else{
                $_monthsList = array(
                    '.01.' => 'січня',
                    '.02.' => 'лютого',
                    '.03.' => 'березня',
                    '.04.' => 'квітня',
                    '.05.' => 'травня',
                    '.06.' => 'червня',
                    '.07.' => 'липня',
                    '.08.' => 'серпня',
                    '.09.' => 'вересня',
                    '.10.' => 'жовтня',
                    '.11.' => 'листопада',
                    '.12.' => 'грудня'
                );
            }
            $currentD = date('d', strtotime($result['date_available']));
            $currentM = date('.m.', strtotime($result['date_available']));
            $currentY = date('Y', strtotime($result['date_available']));

            $currentDate = $currentD . ' ' . $_monthsList[$currentM] . ' ' . $currentY;

            $data['articles'][] = array(
                'article_id'  		=> $result['article_id'],
                'original'			=> $original,
                'thumb'       		=> $thumb,
                'name'        		=> $result['name'],
                'preview'     		=> html_entity_decode($result['preview'], ENT_QUOTES, 'UTF-8'),
                'attributes'  		=> $result['attributes'],
                'href'        		=> $this->url->link('newsblog/article', 'newsblog_path=' . $this->request->get['newsblog_path'] . '&newsblog_article_id=' . $result['article_id']),
                'date'		  		=> ($date_format ? date($date_format, strtotime($result['date_available'])) : false),
                'date_modified'		=> ($date_format ? date($date_format, strtotime($result['date_modified'])) : false),
                'viewed' 			=> $result['viewed'],
                'currentDate'		=> $currentDate,
            );
        }

        /*
        if ( isset($this->session->data['language']) && $this->session->data['language'] === 'ua' )
        {
            $meta_title = 'Фешемебельний - інтернет магазин меблів для дому, купити стільці, столи, дивани та інші меблі в Києві по найкращій ціні';
            $meta_description = 'Меблі для дому від Фешемебельний ✔ Висока якість ✔Доступні ціни ✔ Сучасний дизайн ✔ Послуги монтажа ➡ Швидка доставка по Києву і всій Україні 🚀 - замовляйте меблі саме зараз 🛋';
            $this->document->setTitle($meta_title);
            $this->document->setDescription($meta_description);
        }*/
        $this->load->model('catalog/category');
        $this->load->model('catalog/section');

        $data['categories_home_image'] = '';
        $data['categories_home_url'] = '';
        $data['categories_home'] = array();

        $categories = $this->model_catalog_category->getCategoriesHome();

        foreach ($categories as $category) {

            $children_data = array();

            $children = $this->model_catalog_section->getSections($category['category_id']);

            foreach ($children as $child) {
                $children_data[] = array(
                    'name'  => $child['title'] ,
                    'url'  =>  $child['url'],
                    'image'            => $this->model_tool_image->resize($child['image'], 647, 565),
                );
                if(empty($data['categories_home_image']))
                {
                    $data['categories_home_url'] = $child['url'];
                    $data['categories_home_image'] = $this->model_tool_image->resize($child['image'], 647, 565);
                }

            }

            $data['categories_home'][] = array(
                'name'            => $category['name'],
                'href'            => $this->url->link('product/category', 'path=' . $category['category_id']),
                'children' => $children_data
            );
        }

        $data['informations_home'] = array();

        $this->load->model('catalog/information');

        $informations = $this->model_catalog_information->getInformationsHome();

        $data['informations_home'][] = array(
            'name'            =>  $this->language->get('text_wholesalers'),
            'href'            => $this->url->link('information/contact/wholesalers', '', 'SSL'),
        );

        foreach ($informations as $information) {
            $data['informations_home'][] = array(
                'name'            => $information['title_home'],
                'href'            => $this->url->link('information/information&information_id=' . $information['information_id']),
            );
        }

        $data['manufacturers'] =array();
        $this->load->model('catalog/manufacturer');
        $results = $this->model_catalog_manufacturer->getManufacturers();

        foreach ($results as $result) {
            $data['manufacturers'][] = array(
                'name'            => $result['name'],
                'image'            => $this->model_tool_image->resize($result['image'], 100, 100),
            );
        }


        $data['projects'] =array();
        $this->load->model('catalog/project');
        $results = $this->model_catalog_project->getProjects();

        foreach ($results as $result) {
            $data['projects'][] = array(
                'name'            => $result['name'],
                'thumb'            => $this->model_tool_image->resize($result['image'], 1000, 1000),
                'image'            => $this->model_tool_image->resize($result['image'], 316, 316),
            );
        }


        $data['partners'] =array();
        $this->load->model('catalog/partner');
        $results = $this->model_catalog_partner->getPartners();

        foreach ($results as $result) {
            $data['partners'][] = array(
                'name'            => $result['name'],
                'image'            => $this->model_tool_image->resize($result['image'], 100, 100),
            );
        }


        if ($this->config->get('config_image')) {
            $data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), 647, 468);
        } else {
            $data['image'] = $this->model_tool_image->resize('no_image.png', 647, 468);
        }

        $config_description = $this->config->get('config_description');
        $data['config_description'] = $config_description[$this->config->get('config_language_id')];

        $this->document->setTitle($data['config_description']['meta_title']);
        $this->document->setDescription($data['config_description']['meta_description']);
        $this->document->setKeywords($data['config_description']['meta_keyword']);


        if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->load->model('catalog/information');
		$info = array();
		foreach ($this->model_catalog_information->getInformations() as $result) {
			$id = $result['information_id'];
			$info[$id] = $result;
			$info[$id]['href'] = 
				$this->url->link('information/information', 'information_id=' . $result['information_id']);
		}		
		$data['informations'] = $info;
		
		$this->load->model('design/banner');
		$home_banner = array();
		foreach ($this->model_design_banner->getBanner(9) as $result) {
			$id = $result['banner_id'];
			$home_banner[] = $result;
		}		
		$data['home_banner'] = $home_banner;
		$data['curr_lang'] = $this->language->get('code');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}

	}
}