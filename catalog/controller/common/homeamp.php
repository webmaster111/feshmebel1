<?php
class ControllerCommonHomeamp extends Controller {
	public function index() {

	    $GLOBALS['amp'] = 1;
        $this->load->language('newsblog/category');
        $data['blog_title'] = $this->language->get('blog_title');
        $data['text_empty'] = $this->language->get('text_empty');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['text_refine'] = $this->language->get('text_refine');
        $data['text_more'] = $this->language->get('text_more');
        $data['text_attributes'] = $this->language->get('text_attributes');

        $this->load->model('newsblog/article');
        $this->load->model('tool/image'); 
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "newsblog_article WHERE page = '/'");

        $date_format = $this->language->get('date_format_short');

        $data['articles'] = array();

        foreach ($query->rows as $article) {
            $result = $this->model_newsblog_article->getArticle($article['article_id']);

            if ($result['image']) {
                $original 	= HTTP_SERVER.'image/'.$result['image'];
                $thumb 		= $this->model_tool_image->resize($result['image'], 600, 600);
            } else {
                $original 	= '';
                $thumb 		= '';	//or use 'placeholder.png' if you need
            }

            $data['articles'][] = array(
                'article_id'  		=> $result['article_id'],
                'original'			=> $original,
                'thumb'       		=> $thumb,
                'name'        		=> $result['name'],
                'preview'     		=> html_entity_decode($result['preview'], ENT_QUOTES, 'UTF-8'),
                'attributes'  		=> $result['attributes'],
                'href'        		=> $this->url->link('newsblog/article', 'newsblog_path=' . $this->request->get['newsblog_path'] . '&newsblog_article_id=' . $result['article_id']),
                'date'		  		=> ($date_format ? date($date_format, strtotime($result['date_available'])) : false),
                'date_modified'		=> ($date_format ? date($date_format, strtotime($result['date_modified'])) : false),
                'viewed' 			=> $result['viewed']
            );
        }

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->load->model('catalog/information');
		$info = array();
		foreach ($this->model_catalog_information->getInformations() as $result) {
			$id = $result['information_id'];
			$info[$id] = $result;
			$info[$id]['href'] = 
				$this->url->link('information/information', 'information_id=' . $result['information_id']);
		}		
		$data['informations'] = $info;
		
		$this->load->model('design/banner');
		$home_banner = array();
		foreach ($this->model_design_banner->getBanner(9) as $result) {
			$id = $result['banner_id'];
			$home_banner[] = $result;
		}		
		$data['home_banner'] = $home_banner;
		$data['curr_lang'] = $this->language->get('code');


        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    $filter_data = array(
                        'filter_category_id' => $child['category_id'],
                        'filter_sub_category' => true
                    );

                    $children_data[] = array(
                        'name' => $child['name'],
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                    );
                }

                // Level 1
                $data['categories'][] = array(
                    'id' => $category['category_id'],
                    'name' => $category['name'],
                    'children' => $children_data,
                    'column' => $category['column'] ? $category['column'] : 1,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }



        $data['column_left'] = $this->load->controller('common/column_left');

        $data['links'] = $this->document->getLinks();

        $this->load->language('common/header');
        $data['text_home'] = $this->language->get('text_home');
        $data['text_wishlist'] = $this->language->get('text_wishlist');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_all'] = $this->language->get('text_all');
        $data['text_compare'] = $this->language->get('text_compare');

        $data['home'] = $this->url->link('common/home');
        $data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
        $data['compare'] = $this->url->link('product/compare');
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', 'SSL');
        $data['register'] = $this->url->link('account/register', '', 'SSL');
        $data['login'] = $this->url->link('account/login', '', 'SSL');
        $data['order'] = $this->url->link('account/order', '', 'SSL');
        $data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
        $data['download'] = $this->url->link('account/download', '', 'SSL');
        $data['logout'] = $this->url->link('account/logout', '', 'SSL');
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['button_cart'] = $this->language->get('button_cart');

        $this->load->language('common/footer');


        $data['footer_schedule'] = $this->language->get('footer_schedule');
        $data['footer_menu_about'] = $this->language->get('footer_menu_about');
        $data['footer_menu_delivery'] = $this->language->get('footer_menu_delivery');
        $data['footer_menu_guaranty'] = $this->language->get('footer_menu_guaranty');
        $data['footer_menu_exchange'] = $this->language->get('footer_menu_exchange');
        $data['footer_menu_discount'] = $this->language->get('footer_menu_discount');
        $data['footer_menu_articles'] = $this->language->get('footer_menu_articles');
        $data['footer_menu_help'] = $this->language->get('footer_menu_help');
        $data['footer_menu_contacts'] = $this->language->get('footer_menu_contacts');



        $data['url_menu_delivery'] = $this->url->link('information/information&information_id=4');
        $data['url_menu_guaranty'] = $this->url->link('information/information&information_id=9');
        $data['url_menu_exchange'] = $this->url->link('account/return/add');
        $data['url_menu_discount'] = $this->url->link('information/information&information_id=11');
        $data['url_menu_help'] = $this->url->link('information/information&information_id=8');
        $data['url_menu_about'] = $this->url->link('information/information&information_id=10');
        $data['url_menu_articles'] = $this->url->link('newsblog/category&newsblog_category_id=1');
        $data['url_menu_contacts'] = $this->url->link('information/contact');
        $data['url_menu_sitemap'] = $this->url->link('information/sitemap');

        $data['language'] = $this->load->controller('common/language');



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/homeamp.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/homeamp.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/homeamp.tpl', $data));
		}

	}
}