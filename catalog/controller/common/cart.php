<?php
class ControllerCommonCart extends Controller {
	public function index() {
		$this->load->model('catalog/supplier');
		$_suppliers = $this->model_catalog_supplier->getSuppliers();
		$suppliers = array();
		foreach($_suppliers as $supplier)
			$suppliers[$supplier['supplier_id']] = $supplier;

		$this->load->language('common/cart');

		// Totals
		$this->load->model('extension/extension');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		// Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_cart'] = $this->language->get('text_cart');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_recurring'] = $this->language->get('text_recurring');
		$data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), ceil($this->currency->format($total)).' грн.');
		$data['text_item'] = sprintf($this->language->get('text_item'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), ceil($this->currency->format($total)).' грн.');
    $data['text_loading'] = $this->language->get('text_loading');

		$data['button_remove'] = $this->language->get('button_remove');

		$this->load->model('tool/image');
		$this->load->model('tool/upload');

		$data['products'] = array();
		$data['quantity'] = 0;

		foreach ($this->cart->getProducts() as $product) {
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();

			$this->load->model('catalog/product');
			$option2 = $this->model_catalog_product->getProductOptions($product['product_id']);
			$option_prod_img = '';
			
			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
					'type'  => $option['type']
				);
				
				foreach($option2 as $optn){
					foreach($optn['product_option_value'] as $optn_val){
						if($optn_val['product_option_value_id'] == $option['product_option_value_id']){
							if($option_prod_img == ''){
								$option_prod_img = $this->model_tool_image->resize($optn_val['image_prod'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
							}
							break 2;
						}
					}
				}
			}

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
			if ($product['supplier_id'] > 0) {
				$supplier_info = $suppliers[$product['supplier_id']];
				$data['price'] = preg_replace('/[^0-9.]/', '', $data['price'])*$supplier_info['coefficient'];
				$data['price'] = ceil($data['price']).' грн.';
				$data['total'] = preg_replace('/[^0-9.]/', '', $data['total'])*$supplier_info['coefficient'];
				$data['price'] = ceil($data['total']).' грн.';
			};
			$data['price'] = ceil($data['price']).' грн.';
			$data['total'] = ceil($data['total']).' грн.';

			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
			} else {
				$total = false;
			}

			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'option_img'    => $option_prod_img,
				'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
				'quantity'  => $product['quantity'],
				'minimum' => ($product['minimum'] ? $product['minimum'] : 1),
				'price'     => ceil($price). ' грн.',
				'total'     => ceil($total). ' грн.',
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);

            $data['quantity'] += $product['quantity'];

			if ($product['supplier_id'] > 0) {
				$supplier_info = $suppliers[$product['supplier_id']];
				$end = end($data);
				$data['price'] = preg_replace('/[^0-9.]/', '', $data['price'])*$supplier_info['coefficient'];
				$data['price'] = ceil($data['price']).' грн.';
				$data['total'] = preg_replace('/[^0-9.]/', '', $data['total'])*$supplier_info['coefficient'];
				$data['total'] = ceil($data['total']).' грн.';
			};
			$data['price'] = ceil($data['price']).' грн.';
			$data['total'] = ceil($data['total']).' грн.';

		}

		// Gift Voucher
		$data['vouchers'] = array();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $key => $voucher) {
				$data['vouchers'][] = array(
					'key'         => $key,
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'])
				);
			}
		}

		$data['totals'] = array();

		foreach ($total_data as $result) {
			$data['totals'][] = array(
				'title' => $result['title'],
				'text'  => ceil($this->currency->format($result['value'])). ' грн.',
			);
		}

		$data['cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		//$data['checkout'] = $this->url->link('checkout/cart');

        $data['text_title'] = $this->language->get('text_title');
        $data['text_total'] = $this->language->get('text_total_new');
        $data['text_remove'] = $this->language->get('text_remove');
        $data['text_price'] = $this->language->get('text_price');
        $data['text_items_new'] = $this->language->get('text_items_new');
        $data['text_btn'] = $this->language->get('text_btn');

        if(false)
        {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/cart.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/common/cart.tpl', $data);
            } else {
                return $this->load->view('default/template/common/cart.tpl', $data);
            }
        }
        else
        {
            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/cart_new.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/common/cart_new.tpl', $data);
            } else {
                return $this->load->view('default/template/common/cart_new.tpl', $data);
            }
        }

	}

    public function get() {

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE status = '1'");
        $languages = array();
        foreach ($query->rows as $result) {

            $languages[$result['code']] = $result;
        }
        /*

        if(isset($_GET['language']))
        {
            $code = $_GET['language'];
            if($code != 'ru')
                $code = 'ua';

            $this->config->set('config_language_id', $languages[$code]['language_id']);
            $this->config->set('config_language', $languages[$code]['code']);

            $language = new Language($this->languages[$code]['directory']);
            $language->load('default');
            $language->load($this->languages[$code]['directory']);
            $this->registry->set('language', $language);

        }
*/

        $this->load->model('catalog/supplier');
        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;

        $this->load->language('common/cart');

        // Totals
        $this->load->model('extension/extension');

        $total_data = array();
        $total = 0;
        $taxes = $this->cart->getTaxes();

        // Display prices
        if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
            $sort_order = array();

            $results = $this->model_extension_extension->getExtensions('total');

            foreach ($results as $key => $value) {
                $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
            }

            array_multisort($sort_order, SORT_ASC, $results);

            foreach ($results as $result) {
                if ($this->config->get($result['code'] . '_status')) {
                    $this->load->model('total/' . $result['code']);

                    $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
                }
            }

            $sort_order = array();

            foreach ($total_data as $key => $value) {
                $sort_order[$key] = $value['sort_order'];
            }

            array_multisort($sort_order, SORT_ASC, $total_data);
        }

        $data['text_empty'] = $this->language->get('text_empty');
        $data['text_cart'] = $this->language->get('text_cart');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_recurring'] = $this->language->get('text_recurring');
        $data['text_items'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), ceil($this->currency->format($total)).' грн.');
        $data['text_item'] = sprintf($this->language->get('text_item'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), ceil($this->currency->format($total)).' грн.');
        $data['text_loading'] = $this->language->get('text_loading');



        $data['button_remove'] = $this->language->get('button_remove');

        $this->load->model('tool/image');
        $this->load->model('tool/upload');

        $data['products'] = array();
        $data['quantity'] = 0;

        foreach ($this->cart->getProducts() as $product) {
            if ($product['image']) {
                $image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
            } else {
                $image = '';
            }

            $option_data = array();

            $this->load->model('catalog/product');
            $option2 = $this->model_catalog_product->getProductOptions($product['product_id']);
            $option_prod_img = '';

            foreach ($product['option'] as $option) {
                if ($option['type'] != 'file') {
                    $value = $option['value'];
                } else {
                    $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                    if ($upload_info) {
                        $value = $upload_info['name'];
                    } else {
                        $value = '';
                    }
                }

                $option_data[] = array(
                    'name'  => $option['name'],
                    'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value),
                    'type'  => $option['type']
                );

                foreach($option2 as $optn){
                    foreach($optn['product_option_value'] as $optn_val){
                        if($optn_val['product_option_value_id'] == $option['product_option_value_id']){
                            if($option_prod_img == ''){
                                $option_prod_img = $this->model_tool_image->resize($optn_val['image_prod'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
                            }
                            break 2;
                        }
                    }
                }
            }

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
            } else {
                $price = false;
            }
            if ($product['supplier_id'] > 0) {
                $supplier_info = $suppliers[$product['supplier_id']];
                $data['price'] = preg_replace('/[^0-9.]/', '', $data['price'])*$supplier_info['coefficient'];
                $data['price'] = ceil($data['price']).' грн.';
                $data['total'] = preg_replace('/[^0-9.]/', '', $data['total'])*$supplier_info['coefficient'];
                $data['price'] = ceil($data['total']).' грн.';
            };
            $data['price'] = ceil($data['price']).' грн.';
            $data['total'] = ceil($data['total']).' грн.';

            // Display prices
            if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                $total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
            } else {
                $total = false;
            }

            $data['products'][] = array(
                'cart_id'   => $product['cart_id'],
                'thumb'     => $image,
                'name'      => $product['name'],
                'model'     => $product['model'],
                'option'    => $option_data,
                'option_img'    => $option_prod_img,
                'recurring' => ($product['recurring'] ? $product['recurring']['name'] : ''),
                'quantity'  => $product['quantity'],
                'minimum' => ($product['minimum'] ? $product['minimum'] : 1),
                'price'     => ceil($price). ' грн.',
                'total'     => ceil($total). ' грн.',
                'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
            );

            $data['quantity'] += $product['quantity'];

            if ($product['supplier_id'] > 0) {
                $supplier_info = $suppliers[$product['supplier_id']];
                $end = end($data);
                $data['price'] = preg_replace('/[^0-9.]/', '', $data['price'])*$supplier_info['coefficient'];
                $data['price'] = ceil($data['price']).' грн.';
                $data['total'] = preg_replace('/[^0-9.]/', '', $data['total'])*$supplier_info['coefficient'];
                $data['total'] = ceil($data['total']).' грн.';
            };
            $data['price'] = ceil($data['price']).' грн.';
            $data['total'] = ceil($data['total']).' грн.';

        }

        // Gift Voucher
        $data['vouchers'] = array();

        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $key => $voucher) {
                $data['vouchers'][] = array(
                    'key'         => $key,
                    'description' => $voucher['description'],
                    'amount'      => $this->currency->format($voucher['amount'])
                );
            }
        }

        $data['totals'] = array();

        foreach ($total_data as $result) {
            $data['totals'][] = array(
                'title' => $result['title'],
                'text'  => ceil($this->currency->format($result['value'])). ' грн.',
            );
        }

        $data['cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');


        $data['text_title'] = $this->language->get('text_title');
        $data['text_total'] = $this->language->get('text_total_new');
        $data['text_remove'] = $this->language->get('text_remove');
        $data['text_price'] = $this->language->get('text_price');
        $data['text_items_new'] = $this->language->get('text_items_new');
        $data['text_btn'] = $this->language->get('text_btn');
        //$data['checkout'] = $this->url->link('checkout/cart');
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/cart_new.tpl')) {
            echo $this->load->view($this->config->get('config_template') . '/template/common/cart_new.tpl', $data);
        } else {
            echo $this->load->view('default/template/common/cart_new.tpl', $data);
        }

    }

	public function info() {
		$this->response->setOutput($this->index());
	}
}
