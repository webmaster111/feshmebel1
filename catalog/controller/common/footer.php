<?php
class ControllerCommonFooter extends Controller {
	public function index() {


		if ($this->request->get["route"] == "information/contact" or $this->request->get["information_id"] == 3)
		{
			$data['contact_information'] = '';
		}else{
			$data['contact_information'] ='
			<div class="display-none">
			<div itemscope itemtype="http://schema.org/Organization">
			 <span itemprop="name">Интернет-магазин roomix.com.ua </span>.
			 <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			   <span itemprop="streetAddress">проспект Мира 4, офис 505 </span>
			   <span itemprop="postalCode">02105</span>
			   <span itemprop="addressLocality">г. Киев</span>,
			 </div>
			 Телефон:
			<span itemprop="telephone">+38 (044) 383-83-85</span>,
			<span itemprop="telephone">+38(095) 655-94-21</span>,
			<span itemprop="telephone">+38(068) 510-81-99</span>,
			 Электронная почта: <span itemprop="email">info@roomix.com.ua</span>
			</div>
			</div>';
		}


		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

		// Links
		$data['footer_schedule'] = $this->language->get('footer_schedule');
		$data['footer_menu_about'] = $this->language->get('footer_menu_about');
		$data['footer_menu_delivery'] = $this->language->get('footer_menu_delivery');
		$data['footer_menu_guaranty'] = $this->language->get('footer_menu_guaranty');
		$data['footer_menu_exchange'] = $this->language->get('footer_menu_exchange');
		$data['footer_menu_discount'] = $this->language->get('footer_menu_discount');
		$data['footer_menu_articles'] = $this->language->get('footer_menu_articles');
		$data['footer_menu_help'] = $this->language->get('footer_menu_help');
		$data['footer_menu_contacts'] = $this->language->get('footer_menu_contacts');

		// url links
		// Start Tkach web-promo			

		$data['url_menu_delivery'] = $this->url->link('information/information&information_id=4');
		$data['url_menu_guaranty'] = $this->url->link('information/information&information_id=9');
		$data['url_menu_exchange'] = $this->url->link('account/return/add');
		$data['url_menu_discount'] = $this->url->link('information/information&information_id=11');
		$data['url_menu_help'] = $this->url->link('information/information&information_id=8');
		
		$data['url_menu_about'] = $this->url->link('information/information&information_id=10');
		$data['url_menu_articles'] = $this->url->link('newsblog/category&newsblog_category_id=1');
		$data['url_menu_contacts'] = $this->url->link('information/contact');
		$data['url_menu_sitemap'] = $this->url->link('information/sitemap');
		
		// end web-promo

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

        $data['try_on_link_more'] = $this->url->link('information/information&information_id=17');
        $data['try_on_action'] = $this->url->link('callback/form');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));


        $this->load->language('product/product');

        $data['map_address'] = $this->language->get('map_address');
        $data['map_address2'] = $this->language->get('map_address2');
        $data['where_watch_shedule'] = $this->language->get('where_watch_shedule');
        $data['where_watch_shedule2'] = $this->language->get('where_watch_shedule2');
        $data['try_on_text'] = $this->language->get('try_on_text');
        $data['text_link_more'] = $this->language->get('text_link_more');
        $data['text_try_on_f_name'] = $this->language->get('text_try_on_f_name');
        $data['text_try_on_f_phone'] = $this->language->get('text_try_on_f_phone');
        $data['text_try_on_f_address'] = $this->language->get('text_try_on_f_address');
        $data['text_try_on_f_submit'] = $this->language->get('text_try_on_f_submit');
        $data['text_try_on_f_success'] = $this->language->get('text_try_on_f_success');

        $data['advantages_text_1'] = $this->language->get('advantages_text_1');
        $data['advantages_text_2'] = $this->language->get('advantages_text_2');
        $data['advantages_text_3'] = $this->language->get('advantages_text_3');
        $data['advantages_text_4'] = $this->language->get('advantages_text_4');
        $data['footer_schedule_1'] = $this->language->get('footer_schedule_1');
        $data['footer_schedule_2'] = $this->language->get('footer_schedule_2');
        $data['footer_schedule_3'] = $this->language->get('footer_schedule_3');
        $data['footer_schedule_3_1'] = $this->language->get('footer_schedule_3_1');
        $data['footer_phone_1'] = $this->language->get('footer_phone_1');
        $data['footer_phone_2'] = $this->language->get('footer_phone_2');
        $data['footer_head_name_1'] = $this->language->get('footer_head_name_1');
        $data['footer_head_name_2'] = $this->language->get('footer_head_name_2');
        $data['footer_head_name_3'] = $this->language->get('footer_head_name_3');
        $data['footer_address_inf'] = $this->language->get('footer_address_inf');
        $data['footer_address_inf_2'] = $this->language->get('footer_address_inf_2');
        $data['footer_address_inf_3'] = $this->language->get('footer_address_inf_3');
        $data['footer_1'] = $this->language->get('footer_1');
        $data['footer_2'] = $this->language->get('footer_2');


        $data['csrf'] = $this->session->data['csrf'];

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {


                // Level 1
                $data['categories'][] = array(
                    'id'       => $category['category_id'],
                    'name'     => $category['name'],
                    'column'   => $category['column'] ? $category['column'] : 1,
                    'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
        }

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}
