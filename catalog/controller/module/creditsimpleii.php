<?php
class ControllerModuleCreditSimpleII extends Controller {

	public function index(){
		$this->language->load('payment/privatbank_paymentparts_ii');

		$data['button_confirm'] = $this->language->get('button_confirm');
        $data['text_label_partsCount'] = $this->language->get('text_label_partsCount');
		$data['text_mounth'] = $this->language->get('text_mounth');
		$data['language'] = str_replace('ua', 'uk', $this->session->data['language']);
		$data['text_loading'] = $this->language->get('text_loading');
		
        $partsCount = 24;
		foreach ($this->cart->getProducts() as $cart) {
			$privat_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_privat WHERE product_id = '" . (int)$cart['product_id'] . "'");
			if ($privat_query->row) {
				if ($privat_query->row['partscount_ii'] <= $partsCount && $privat_query->row['partscount_ii'] !=0) {
					$partsCount = (int)$privat_query->row['partscount_ii'];
				}
			}
		}
		if ($partsCount == 24) {
			$partsCount = $this->config->get('privatbank_paymentparts_ii_paymentquantity');
		}
		
        $data['partsCounts'] = $partsCount;
		
		if (isset ($this->session->data['privatbank_paymentparts_ii_sel'])) {
			$data['partsCountSel'] = $this->session->data['privatbank_paymentparts_ii_sel'];
		} else {
			$data['partsCountSel'] = '';
		}	

		// Totals
		$this->load->model('extension/extension');

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();
		// Display prices
		if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);
					
					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}

		$replace_array = array('<span class="currency">' . $this->currency->getSymbolRight() . '</span>',$this->currency->getSymbolLeft(),$this->currency->getSymbolRight(),$this->language->get('thousand_point'));
		
		$data['total'] = str_replace($replace_array,"",$this->currency->format($total));

		if(version_compare( VERSION, '2.2.0.0', '>=' )) {
			$this->response->setOutput($this->load->view('module/credit_simple_ii', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/credit_simple_ii.tpl')) {
				return $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/credit_simple_ii.tpl', $data));
			} else {
				return $this->response->setOutput($this->load->view('default/template/module/credit_simple_ii.tpl', $data));
			}
		}
    }
}