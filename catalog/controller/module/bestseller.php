<?php
class ControllerModuleBestSeller extends Controller {
	public function index($setting) {
		
		$this->load->model('catalog/supplier');
		$_suppliers = $this->model_catalog_supplier->getSuppliers();
		$suppliers = array();
		foreach($_suppliers as $supplier)
			$suppliers[$supplier['supplier_id']] = $supplier;
		$data['suppliers'] = $suppliers;
		
		$this->load->language('module/bestseller');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');
        $data['text_option'] = $this->language->get('text_option');
        $data['text_select'] = $this->language->get('text_select');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
                               
                                                                // Function Products Options in category


                $json = array();


                if (isset($this->request->post['product_id'])) {
                    $product_id = $this->request->post['product_id'];
                } else {
                    $product_id = 0;
                }


                $this->load->model('catalog/product');


                $product_info = $this->model_catalog_product->getProduct($product_id);


                if ($product_info) {
                    if ($product_id > 0) {
                        $supplier_info = $suppliers[$product_info['supplier_id']];
                    }


                    $options = array();


                    foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                        $product_option_value_data = array();


                        foreach ($option['product_option_value'] as $option_value) {
                            if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                                } else {
                                    $price = false;
                                }
                                if ($supplier_info['supplier_id'] > 0) {
                                    $price = preg_replace('/[^0-9.]/', '', $price) * $supplier_info['coefficient'];
                                    $price = ceil($price) . ' грн.';
                                };

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'price' => $price,
                                    'price_prefix' => $option_value['price_prefix']
                                );
                            }
                        }


                        $options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required']
                        );


                        $options['product_id'] = $product_info['product_id'];
                        $options['name'] = $product_info['name'];
                    }


                    if (!$json) {
                        $json = $options;
                    }
                }


// END Function

                if ($supplier_info['supplier_id'] > 0) {
                    $price = preg_replace('/[^0-9.]/', '', $price) * $supplier_info['coefficient'];
                    $price = ceil($_price) . ' грн.';
                    $special = preg_replace('/[^0-9.]/', '', $special) * $supplier_info['coefficient'];
                    $special = ceil($_special) . ' грн.';
                };

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'name' => $result['name'].' - '.$result['sku'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price' => $price,
                    'special' => $special,
                    'tax' => $tax,
                    'rating' => $rating,
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                    'options' => $this->model_catalog_product->getProductOptions($result['product_id']),
                    'supplier' => $suppliers[$product_info['supplier_id']],
                    'privat' => $result['product_pp'] || $result['product_ii'] ? 1 : 0,
                );
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/bestseller.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/bestseller.tpl', $data);
            } else {
                return $this->load->view('default/template/module/bestseller.tpl', $data);
            }
        }
    }
}
