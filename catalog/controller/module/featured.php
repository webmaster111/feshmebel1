<?php

class ControllerModuleFeatured extends Controller
{
    public function index($setting)
    {
//*		
        $this->load->model('catalog/supplier');
        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach ($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;
        $data['suppliers'] = $suppliers;
//*/		
        $this->load->language('module/featured');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');
        $data['text_option'] = $this->language->get('text_option');
        $data['text_select'] = $this->language->get('text_select');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_toorder1'] = $this->language->get('button_toorder1');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['products'] = array();

        if (!$setting['limit']) {
            $setting['limit'] = 4;
        }

        if (!empty($setting['product'])) {
            $products = array_slice($setting['product'], 0, (int)$setting['limit']);


            // Function Products Options in category


            $json = array();


            if (isset($this->request->post['product_id'])) {
                $product_id = $this->request->post['product_id'];
            } else {
                $product_id = 0;
            }


            $this->load->model('catalog/product');


            $product_info = $this->model_catalog_product->getProduct($product_id);

            if ($product_info) {
//*
                if ($product_id > 0) {
                    $supplier_info = $suppliers[$product_info['supplier_id']];
                }

//*/
                $options = array();


                foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                    $product_option_value_data = array();


                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            } else {
                                $price = false;
                            }


                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 32, 32),
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }


                    $options[] = array(
                        'product_option_id' => $option['product_option_id'],
                        'option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );


                    $options['product_id'] = $product_info['product_id'];
                    $options['name'] = $product_info['name'];
                }


                if (!$json) {
                    $json = $options;
                }
            }


// END Function

            $this->load->model('localisation/sale_status');

            $data['sale_statuses'] = $this->model_localisation_sale_status->getSaleStatuses();

            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);

                if ($product_info) {

                    if ($product_id > 0) {
                        $supplier_info = $suppliers[$product_info['supplier_id']];
                    }


                    if ($product_info['image']) {
                        //$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                        $image = $this->model_tool_image->resize($product_info['image'], 300, 300);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }

                    if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $price = false;
                    }

                    if ((float)$product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_info['rating'];
                    } else {
                        $rating = false;
                    }
//*
                    if ($supplier_info['supplier_id'] > 0) {
                        $price = preg_replace('/[^0-9.]/', '', $price) * $supplier_info['coefficient'];
                        $price = ceil($price) . ' грн.';
                        //$special = preg_replace('/[^0-9.]/', '', $special)*$supplier_info['coefficient'];
                        //	$special = ceil($special).' грн.';
                    };
//*/


                    $images = array();

                    $result_images = $this->model_catalog_product->getProductImages($product_info['product_id']);

                    foreach ($result_images as $result_image) {
                        $images[] = array(
                            'thumb' => $this->model_tool_image->resize($result_image['image'], 256, 256)
                        );
                    }

                    $prodd_options = $this->model_catalog_product->getProductOptions($product_info['product_id']);
                    $prodd_options_new = array();
                    foreach ($prodd_options as $key1 => $option) {
                        $prodd_options_new[] = $option;
                        foreach ($option['product_option_value'] as $key2 => $option_val) {

                            if(!empty($option_val['image_prod']))
                                $images[] = array(
                                    'thumb' => $this->model_tool_image->resize($option_val['image_prod'], 256, 256)
                                );

                            if(!empty($option_val['image_prod']))
                            {
                                $prodd_options_new[$key1]['product_option_value'][$key2]['image_prod'] = $this->model_tool_image->resize($option_val['image_prod'], 300, 300);
                            }
                            $prodd_options_new[$key1]['product_option_value'][$key2]['image'] = $this->model_tool_image->resize($option_val['image'], 32, 32);

                            $supplier_id = $product_info['supplier_id'];

                            $_price = $prodd_options_new[$key1]['product_option_value'][$key2]['price'];
                            if ((int)$supplier_id > 0) {
                                $_price = preg_replace('/[^0-9.]/', '', $_price) * $suppliers[$supplier_id]['coefficient'];
                            };
                            $prodd_options_new[$key1]['product_option_value'][$key2]['price'] = ceil($_price) . ' грн.';
                        }
                    }
                    $special_info_tmp = $this->model_catalog_product->getProductSpecial($product_info['product_id']);

                    if(!empty($special_info_tmp)){

                        if(strtotime($special_info_tmp['date_end']) - time()>0)
                            $product_info['sale_status_id']=1;
                        else
                            $product_info['sale_status_id']=0;
                    }

                    $data['products'][] = array(
                        'product_id' => $product_info['product_id'],
                        'thumb' => $image,
                        'images' => $images,

                        'name' => $product_info['name'].' - '.$product_info['sku'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                        'price' => $price,
                        'old_price' => $product_info['old_price'],
                        'sale_status_name' =>isset($data['sale_statuses'][$product_info['sale_status_id']]) ? $data['sale_statuses'][$product_info['sale_status_id']]['name']:'',
                        'sale_status_color' =>isset($data['sale_statuses'][$product_info['sale_status_id']]) ? $data['sale_statuses'][$product_info['sale_status_id']]['color']:'',
                        'sale_status_image' =>isset($data['sale_statuses'][$product_info['sale_status_id']]) ? '/image/'.$data['sale_statuses'][$product_info['sale_status_id']]['image']:'',
                        'special' => $special,
                        'tax' => $tax,
                        'rating' => $rating,
                        'stock_status' => $product_info['stock_status'],
                        'stock_image' => (empty($result['stock_image'])) ? '' : '/image/'.$result['stock_image'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                        //'options'     => $this->model_catalog_product->getProductOptions($product_info['product_id']),
                        'options' => $prodd_options_new,
                        'supplier' => $suppliers[$product_info['supplier_id']],
                        'privat' => $product_info['product_pp'] || $product_info['product_ii']? 1 : 0,
                    );
                }
            }
        }

        if ($data['products']) {
            if(isset($GLOBALS['amp']))
            {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featuredamp.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/featuredamp.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/featuredamp.tpl', $data);
                }
            }
            else
            {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/featured.tpl', $data);
                }
            }

        }
    }
}