﻿ <?php
class ControllerFeedGetFeed extends Controller {
    public function index() {
		$sql_prods = $this->db->query("SELECT p.product_id, p.topseller, pd.name, p.model, pd.description, p.price, p.image, p.supplier_id FROM ".DB_PREFIX."product p LEFT JOIN ".DB_PREFIX."product_description pd ON p.product_id=pd.product_id WHERE (p.stock_status_id=7 OR p.stock_status_id=8) AND p.feed=1 AND pd.language_id=1 AND p.price > 0 ORDER BY p.product_id ASC");

		$this->load->model('catalog/product');
		$this->load->model('catalog/supplier');
		$_suppliers = $this->model_catalog_supplier->getSuppliers();
		$suppliers = array();
		foreach($_suppliers as $supplier)
			$suppliers[$supplier['supplier_id']] = $supplier;
		
		
		$xml = new DOMDocument('1.0','utf-8');
		$rss = $xml->appendChild($xml->createElement('rss'));
		$rss->setAttribute('version', '2.0');
		$rss->setAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
		$channel = $rss->appendChild($xml->createElement('channel'));
		$main_title = $channel->appendChild($xml->createElement('title'));
		$main_title->appendChild($xml->createTextNode($_SERVER['SERVER_NAME']));
		$main_link = $channel->appendChild($xml->createElement('link'));
		$main_link->appendChild($xml->createTextNode('https://'.$_SERVER['SERVER_NAME']));
		$main_desc = $channel->appendChild($xml->createElement('description'));
		$main_desc->appendChild($xml->createTextNode('Интернет магазин мебели для дома'));

		//$prod = new ControllerProductCategory();

		if ($sql_prods->num_rows > 0) {
			foreach ($sql_prods->rows as $result) {
                $result =  $this->model_catalog_product->getProduct($result['product_id']);


				$descr = strip_tags(str_replace(array("\r","\n"), "", preg_replace('#<style.+?</style>#is', '', str_replace("&nbsp;"," ", html_entity_decode($result['description'])))));
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}



				$supplier_id = $result['supplier_id'];
				$_price = $price;
				//$_special = $special;
				if ($supplier_id>0) {
					$_price = preg_replace('/[^0-9.]/', '', $_price )*$suppliers[$supplier_id]['coefficient'];
					//$_special = preg_replace('/[^0-9.]/', '', $_special)*$suppliers[$supplier_id]['coefficient'];
					$_price_num = ceil($_price);
					$_price = ceil($_price).' UAH';
					//$_special = ceil($_special).' UAH';
				};
				$_price_num = ceil($_price);
				$_price = ceil($_price).' UAH';
				
				$item = $channel -> appendChild( $xml->createElement('item'));
				$item -> appendChild( $xml->createElement('g:id', $result['product_id']));

				$item -> appendChild( $xml->createElement('g:title', $result['name']));
				$item -> appendChild( $xml->createElement('g:description', $descr));
				$item -> appendChild( $xml->createElement('g:link', $this->url->link('product/product', 'product_id=' . $result['product_id'], false, '')));
				$item -> appendChild( $xml->createElement('g:image_link', 'https://'.$_SERVER['SERVER_NAME'].'/image/'.$result['image']));
				$item -> appendChild( $xml->createElement('g:condition', 'new'));
				$item -> appendChild( $xml->createElement('g:availability', 'in stock'));
				if($result['old_price']>0)
                {
                    $item -> appendChild( $xml->createElement('g:price', $result['old_price'].' UAH'));
                    $item -> appendChild( $xml->createElement('g:sale_price', $_price));
                }
				else
                {
                    $item -> appendChild( $xml->createElement('g:price', $_price));
                }

				//$item -> appendChild( $xml->createElement('link', $url->link('product/product', 'product_id='.$result['product_id'])));
				$item -> appendChild( $xml->createElement('g:identifier_exists', 'no'));
                if($result['topseller'])
                    $item -> appendChild( $xml->createElement('g:custom_label_0', 'topseller'));
			}
		}
		//$xml->formatOutput = true; #-> устанавливаем выходной формат документа в true
		//$xml->save($_SERVER['DOCUMENT_ROOT'].'/genxml/goods.xml');
        ob_clean();
        $output = $xml->saveXML();
		header('Content-Type: text/xml');
        echo $output;


    }
	
}