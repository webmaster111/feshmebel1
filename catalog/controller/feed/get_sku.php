﻿<?php

class ControllerFeedGetSku extends Controller
{
    public function find($arr,$price=0,&$values=array(),$index=0,$val=array(),$name='',$value='')
    {
        if(!empty($name))
        {
            if(!isset($val['attrs']))
                $val['attrs'] = '';
            $val[$name] = $value;
            $val['attrs'] .= $value.'/';

        }
        if(isset($arr[$index]))
        {
            foreach ($arr[$index]['product_option_value'] as $item)
            {
                $this->find($arr,$price+=$item['price'],$values,$index+1,$val,$arr[$index]['name'],$item['name']);
            }
        }
        else
        {
            $val['price'] = $price;
            $values[]=$val;
            return 0;
        }
    }
    public function index()
    {
        $this->config->set('config_language_id',2);

        $cats = array();
        $rows = $this->db->query("SELECT cd.name, cd.category_id FROM " . DB_PREFIX . "category_description as cd
        WHERE cd.language_id=2 ")->rows;
        foreach ($rows as $row)
        {
            $cats[$row['category_id']] = $row['name'];
        }

        $rows = $this->db->query("SELECT p.product_id, ptc.category_id FROM " . DB_PREFIX . "product p 
        LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id=pd.product_id 
        LEFT JOIN " . DB_PREFIX . "product_to_category ptc ON p.product_id=ptc.product_id 
        WHERE (p.stock_status_id=7 OR p.stock_status_id=8) AND pd.language_id=2 AND p.price > 0 
    
         ORDER BY p.product_id ASC")->rows;
 
        $this->load->model('catalog/product');
        $this->load->model('catalog/supplier');
        $_suppliers = $this->model_catalog_supplier->getSuppliers();
        $suppliers = array();
        foreach ($_suppliers as $supplier)
            $suppliers[$supplier['supplier_id']] = $supplier;

        $options_name = array();
        $products = array();
        foreach ($rows as $row) {
            $item = $this->model_catalog_product->getProduct($row['product_id']);


            $supplier_id = $item['supplier_id'];

            if ($supplier_id > 0) {
                $price = ceil($item['price'] * $suppliers[$supplier_id]['coefficient']);
            };

            $prodd_options = $this->model_catalog_product->getProductOptions($row['product_id']);
            $prodd_options_new = array();
            foreach ($prodd_options as $key1 => $option) {
                $prodd_options_new[] = $option;
                foreach ($option['product_option_value'] as $key2 => $option_val) {
                    $_price = $prodd_options_new[$key1]['product_option_value'][$key2]['price'];
                    if ($supplier_id)
                        $_price = $_price * $suppliers[$supplier_id]['coefficient'];
                    $prodd_options_new[$key1]['product_option_value'][$key2]['price'] = ceil($_price);
                }
                $options_name[$option['name']] =$option['name'];
            }
            $values = array();
            $this->find($prodd_options_new,$price,$values);

            $items_cat = $this->model_catalog_product->getCategories($row['product_id']);

            $category_arr = array();
            foreach ($items_cat as $item_cat)
                if(isset($cats[$item_cat['category_id']])&&$item_cat['main_category'])
                    $category_arr[]=$cats[$item_cat['category_id']];

            $category_arr = implode(', ',$category_arr);


            $products[]=array(
                'sku' => $item['sku'],
                'name' => $item['name'],
                'category' => $category_arr,
                'supplier' => $supplier_id>0? $suppliers[$supplier_id]['name'] : '',
                'values' => $values,
            );



            unset($values);


        }
        sort($options_name);
        $file="";
        $file .= "sku;";
        $file .= "name;";
        $file .= "supplier;";
        $file .= "cat;";
        $file .= "price;";
        $file .= "attrs;";
        foreach ($options_name as $name)
            $file .= $name.";";
        $file .= "\n";

        foreach ($products as $product)
        {

            foreach ($product['values'] as $k=>$value)
            {
                $file .= $product['sku'].";";
                $file .= $product['name'].";";
                $file .= $product['supplier'].";";
                $file .= $product['category'].";";
                $file .= $value['price'].";";
                $file .= $value['attrs'].";";
                foreach ($options_name as $name)
                {
                    if(isset($value[$name]))
                        $file .= $value[$name].";";
                    else
                        $file .= ";";
                }
                $file .= "\n";
            }

        }
        file_put_contents('sku_main_ukr.csv', $file);
        echo 'ok';
    }

}
