<?php
class ControllerFeedGoogleSitemap extends Controller {
	public function graber() {
		$this->load->model('catalog/product');
		$products = $this->model_catalog_product->getProducts();
        foreach ($products as $product) {
        	echo $this->url->link('product/product', 'product_id=' . $product['product_id'], $secure = false) ."\n";
        	echo $this->url->link('product/product', 'product_id=' . $product['product_id'], $secure = false,'ua') ."\n";
		}
		$this->load->model('catalog/category');
		$this->getCategoriesGraber(0);
	}
    public function index() {

        if ($this->config->get('google_sitemap_status')) {
            if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "")
            {
                $output  = '<?xml version="1.0" encoding="UTF-8"?>
                <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                   <sitemap>
                      <loc>https://feshmebel.com.ua/sitemap-ru.xml</loc>
                      <lastmod>'.date('Y-m-d').'</lastmod>
                   </sitemap>
                   <sitemap>
                      <loc>https://feshmebel.com.ua/sitemap-ua.xml</loc>
                      <lastmod>'.date('Y-m-d').'</lastmod>
                   </sitemap>
                </sitemapindex>';
                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);
            }
            else
            {

            $output  = '<?xml version="1.0" encoding="UTF-8"?>';
            $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
            $output .= '<url>';
            $output .= '<loc>http://feshmebel.com.ua/</loc>';
            $output .= '<changefreq>daily</changefreq>';
            $output .= '<priority>1</priority>';
            $output .= '</url>';
            $output .= '<url>';
            $output .= '<loc>http://feshmebel.com.ua/ua/</loc>';
            $output .= '<changefreq>daily</changefreq>';
            $output .= '<priority>1</priority>';
            $output .= '</url>';


            $this->load->model('catalog/category');
            $languages = $this->model_catalog_category->getMyLanguages();

            foreach ($languages as $language) {

//			$output .=  $language['code'].'tkach';
                $this->load->model('catalog/product');
                $this->load->model('tool/image');

                $products = $this->model_catalog_product->getProducts();

                foreach ($products as $product) {
                    if($this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') ==
                        'http://feshmebel.com.ua/stolovaja/barnye-stulja/barnyjj-taburet-tolix-tol%D1%96ks-h-76-belyjj' ||
                        $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'http://feshmebel.com.ua/stolovaja/stulja/kreslo-bavaria/' ||
                        $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'http://feshmebel.com.ua/ua/stolovaja/stulja/kreslo-bavaria/'){
                        continue;
                    }
                    if ($product['image']) {
                        $output .= '<url>';
                        $output .= '<loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id'], $secure = false, $language['code']) . '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<lastmod>' . date('Y-m-d',strtotime($product['date_modified'])) . '</lastmod>';
                        $output .= '<priority>1.0</priority>';
                        $output .= '</url>';
                    }
                }

                $this->load->model('catalog/category');

                $this->load->model('catalog/manufacturer');

                $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

                foreach ($manufacturers as $manufacturer) {
                    $output .= '<url>';
                    $output .= '<loc>' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'] , $secure = false, $language['code']) . '</loc>';
                    $output .= '<changefreq>weekly</changefreq>';
                    $output .= '<priority>0.9</priority>';
                    $output .= '</url>';

                    $products = $this->model_catalog_product->getProducts(array('filter_manufacturer_id' => $manufacturer['manufacturer_id']));

                    /*				foreach ($products as $product) {
                                        $output .= '<url>';
                                        $output .= '<loc>' . $this->url->link('product/product', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $product['product_id']) . '</loc>';
                                        $output .= '<changefreq>monthly</changefreq>';
                                        $output .= '<priority>1.0</priority>';
                                        $output .= '</url>';
                                    }*/
                }

                $this->load->model('catalog/information');

                $informations = $this->model_catalog_information->getInformations();

                foreach ($informations as $information) {
                    if(
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'http://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/ua/help/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/uslovija-soglashenija/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/ua/uslovija-soglashenija/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'http://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd/'
                    ){
                        continue;
                    }
                    $output .= '<url>';
                    $output .= '<loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) . '</loc>';
                    $output .= '<changefreq>monthly</changefreq>';
                    $output .= '<priority>0.5</priority>';
                    $output .= '</url>';
                }

            }




            $output .= '</urlset>';

            //для генерации всех товаров в CSV
//            $this->generateProductsCSV();

            $this->response->addHeader('Content-Type: application/xml');
            $this->response->setOutput($output);

            }
        }
    }

    public function ru() {

        if ($this->config->get('google_sitemap_status')) {
            if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "")
            {
                $output  = '<?xml version="1.0" encoding="UTF-8"?>';
                $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
                $output .= '<url>';
                $output .= '<loc>https://feshmebel.com.ua/</loc>';
                $output .= '<changefreq>daily</changefreq>';
                $output .= '<priority>1</priority>';
                $output .= '</url>';
                $output .= '<url>';
                $output .= '<loc>https://feshmebel.com.ua/ua/</loc>';
                $output .= '<changefreq>daily</changefreq>';
                $output .= '<priority>1</priority>';
                $output .= '</url>';

                 $output .= '<url><loc>https://feshmebel.com.ua/shipping-and-payment/kiev/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/harkov/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/odessa/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/dnepropetrovsk/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/zaporozhe/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/lvov/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/krivoy-rog/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/nikolaev/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/mariupol/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/vinnitsa/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/herson/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/poltava/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/chernigov/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/cherkassyi/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/zhitomir/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/sumyi/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/hmelnitskiy/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/rovno/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/kropivnitskiy/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/kamenskoe/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/chernovtsyi/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/kremenchug/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/ivano-frankovsk/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/ternopol/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/belaya-tserkov/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/lutsk/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/melitopol/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/nikopol/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/berdyansk/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/uzhgorod/</loc></url>
<url><loc>https://feshmebel.com.ua/shipping-and-payment/kamenets-podolskiy/</loc></url>';

            $this->load->model('catalog/product');
            $this->load->model('tool/image');

            $products = $this->model_catalog_product->getProducts();

            foreach ($products as $product) {
                if ($product['image']) {
                        $output .= '<url>';
                        $output .= '<loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id'], $secure = false, $language['code']) . '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<lastmod>' . date('Y-m-d',strtotime($product['date_modified'])) . '</lastmod>';
                        $output .= '<priority>1.0</priority>';
                        $output .= '<image:image>';
                        $output .= '<image:loc>'.$this->model_tool_image->resize($product['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')).'</image:loc>';
                        $output .= '<image:caption>' . $product['name'] . '</image:caption>';
                        $output .= '<image:title>' . $product['name'] . '</image:title>';
                        $output .= '</image:image>';
                        $output .= '</url>';

                }
            }


                $this->load->model('catalog/category');
                $languages = $this->model_catalog_category->getMyLanguages();

                foreach ($languages as $language) {

//			$output .=  $language['code'].'tkach';
                    $this->load->model('catalog/product');
                    $this->load->model('tool/image');

                    $products = $this->model_catalog_product->getProducts();

                    foreach ($products as $product) {
                        if($this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') ==
                            'https://feshmebel.com.ua/stolovaja/barnye-stulja/barnyjj-taburet-tolix-tol%D1%96ks-h-76-belyjj' ||
                            $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'https://feshmebel.com.ua/stolovaja/stulja/kreslo-bavaria/' ||
                            $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'https://feshmebel.com.ua/ua/stolovaja/stulja/kreslo-bavaria/'){
                            continue;
                        }
                        if ($product['image']) {
                            $output .= '<url>';
                            $output .= '<loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id'], $secure = false, $language['code']) . '</loc>';
                            $output .= '<changefreq>weekly</changefreq>';
                            $output .= '<lastmod>' . date('Y-m-d',strtotime($product['date_modified'])) . '</lastmod>';
                            $output .= '<priority>1.0</priority>';
                            $output .= '</url>';
                        }
                    }

                    $this->load->model('catalog/category');

                    $this->load->model('catalog/manufacturer');

                    $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

                    foreach ($manufacturers as $manufacturer) {
                        $output .= '<url>';
                        $output .= '<loc>' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'] , $secure = false, $language['code']) . '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<priority>0.9</priority>';
                        $output .= '</url>';

                        $products = $this->model_catalog_product->getProducts(array('filter_manufacturer_id' => $manufacturer['manufacturer_id']));

                        /*				foreach ($products as $product) {
                                            $output .= '<url>';
                                            $output .= '<loc>' . $this->url->link('product/product', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $product['product_id']) . '</loc>';
                                            $output .= '<changefreq>monthly</changefreq>';
                                            $output .= '<priority>1.0</priority>';
                                            $output .= '</url>';
                                        }*/
                    }

                    $this->load->model('catalog/information');

                    $informations = $this->model_catalog_information->getInformations();

                    foreach ($informations as $information) {
                        if(
                            $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'https://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd' or
                            $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'https://feshmebel.com.ua/ua/help/' or
                            $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'https://feshmebel.com.ua/uslovija-soglashenija/' or
                            $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'https://feshmebel.com.ua/ua/uslovija-soglashenija/' or
                            $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'https://feshmebel.com.ua/ua/contacts-information/' or
                            $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'https://feshmebel.com.ua/contacts-information/' or
                            $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'https://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd/'
                        ){
                            continue;
                        }
                        $output .= '<url>';
                        $output .= '<loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) . '</loc>';
                        $output .= '<changefreq>monthly</changefreq>';
                        $output .= '<priority>0.5</priority>';
                        $output .= '</url>';
                    }

                }
                //$output .= $this->getCategories(0, $language['code'],'');

                $this->load->model('catalog/category');
                $this->load->model('catalog/product');



                $product_filters = $this->db->query("SELECT * FROM `oc_product_filter")->rows;
                $product_filters_arr = array();
                foreach ($product_filters as $product_filter)
                {
                    if(!isset($product_filters_arr[$product_filter['product_id']]))
                        $product_filters_arr[$product_filter['product_id']] = array();
                    $product_filters_arr[$product_filter['product_id']][$product_filter['filter_id']] = $product_filter['filter_id'];
                }



                $languages = $this->model_catalog_category->getMyLanguages();

                $language=$languages[0];

                    $filters_group = $this->db->query("SELECT * FROM `oc_filter_group_description` AS fgd LEFT JOIN `oc_filter_group` as fg ON fg.filter_group_id = fgd.filter_group_id WHERE fgd.language_id = '".$language['language_id']."' ORDER BY fg.sort_order ")->rows;
                    $filters_group_tmp = array();
                    foreach ($filters_group as $filter_group)
                        $filters_group_tmp[$filter_group['filter_group_id']] = $filter_group;

                    $filters = $this->db->query("SELECT * FROM `oc_filter_description` AS fd LEFT JOIN `oc_filter` as f ON f.filter_id = fd.filter_id WHERE fd.language_id = '".$language['language_id']."' ORDER BY f.sort_order ")->rows;
                    $filters_tmp = array();
                    foreach ($filters as $filter)
                        $filters_tmp[$filter['filter_id']] = $filter;

                    $cats = array();
                    $this->getCategories($cats, $filters,0, '',$language);


                    foreach ($cats as $cat)
                    {
                        $output .= '<url>';
                        $output .= '<loc>' . $cat['href'] . '</loc>';
                        $output .= '<changefreq>daily</changefreq>';
                        $output .= '<priority>0.9</priority>';
                        $output .= '</url>';

                        $filter_data = array(
                            'filter_category_id' => $cat['category_id'],
                        );
                        $products = $this->model_catalog_product->getProducts($filter_data);
                        $product_filters_arr_tmp = array();
                        $product_filters_arr_tmp2 = array();
                        foreach ($products as $product) {
                            foreach ($product_filters_arr[$product['product_id']] as $filter_id) if(isset($filters_tmp[$filter_id])) {
                                $filter = $filters_tmp[$filter_id];

                                if(!isset($product_filters_arr_tmp2[$filter_id]))
                                {
                                    $product_filters_arr_tmp2[$filter_id] = $filter_id;

                                    $output .= '<url>';
                                    $output .= '<loc>' .$cat['href'].$filters_group_tmp[$filter['filter_group_id']]["filter_group_url"].'_'.$filter["filter_url"].'/' . '</loc>';
                                    $output .= '<changefreq>daily</changefreq>';
                                    $output .= '<priority>0.9</priority>';
                                    $output .= '</url>'."\n";
                                }


                                if (!isset($product_filters_arr_tmp[$filter_id]))
                                    $product_filters_arr_tmp[$filter_id] = array();

                                foreach ($product_filters_arr[$product['product_id']] as $filter_id_sub)
                                {
                                    $filter_sub = $filters_tmp[$filter_id_sub];
                                    if (
                                        $filter_id != $filter_id_sub &&
                                        !isset($product_filters_arr_tmp[$filter_id_sub][$filter_id]) &&
                                        !isset($product_filters_arr_tmp[$filter_id][$filter_id_sub]) &&
                                        $filter['filter_group_id']!=$filter_sub['filter_group_id'] &&
                                        isset($filters_group_tmp[$filter_sub['filter_group_id']])
                                    ) {
                                        $product_filters_arr_tmp[$filter_id][$filter_id_sub] = $filter_id_sub;
                                        if($filters_group_tmp[$filter['filter_group_id']]['sort_order']<$filters_group_tmp[$filter_sub['filter_group_id']]['sort_order']) {
                                            $output .= '<url>';
                                            $output .= '<loc>' . $cat['href'] . '?mfp=' . $filter['filter_group_id'] . 'f-' . $filters_group_tmp[$filter['filter_group_id']]['filter_group_url'] . '[' . $filter['filter_id'] . '],' . $filter_sub['filter_group_id'] . 'f-' . $filters_group_tmp[$filter_sub['filter_group_id']]['filter_group_url'] . '[' . $filter_sub['filter_id'] . ']' . '</loc>';
                                            $output .= '<changefreq>daily</changefreq>';
                                            $output .= '<priority>0.9</priority>';
                                            $output .= '</url>'."\n";
                                        } else {
                                            $output .= '<url>';
                                            $output .= '<loc>' . $cat['href'].'?mfp='.$filter_sub['filter_group_id'].'f-'.$filters_group_tmp[$filter_sub['filter_group_id']]['filter_group_url'].'['.$filter_sub['filter_id'].'],'.$filter['filter_group_id'].'f-'.$filters_group_tmp[$filter['filter_group_id']]['filter_group_url'].'['.$filter['filter_id'].']' . '</loc>';
                                            $output .= '<changefreq>daily</changefreq>';
                                            $output .= '<priority>0.9</priority>';
                                            $output .= '</url>'."\n";
                                        }
                                    }
                                }

                            }

                        }


                    }



                $output .= '</urlset>';

                //для генерации всех товаров в CSV
//            $this->generateProductsCSV();

                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);
            }
            else
            {

            $output  = '<?xml version="1.0" encoding="UTF-8"?>';
            $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
            $output .= '<url>';
            $output .= '<loc>http://feshmebel.com.ua/</loc>';
            $output .= '<changefreq>daily</changefreq>';
            $output .= '<priority>1</priority>';
            $output .= '</url>';
            $output .= '<url>';
            $output .= '<loc>http://feshmebel.com.ua/ua/</loc>';
            $output .= '<changefreq>daily</changefreq>';
            $output .= '<priority>1</priority>';
            $output .= '</url>';


            $this->load->model('catalog/category');
            $languages = $this->model_catalog_category->getMyLanguages();

            foreach ($languages as $language) {

//			$output .=  $language['code'].'tkach';
                $this->load->model('catalog/product');
                $this->load->model('tool/image');

                $products = $this->model_catalog_product->getProducts();

                foreach ($products as $product) {
                    if($this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') ==
                        'http://feshmebel.com.ua/stolovaja/barnye-stulja/barnyjj-taburet-tolix-tol%D1%96ks-h-76-belyjj' ||
                        $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'http://feshmebel.com.ua/stolovaja/stulja/kreslo-bavaria/' ||
                        $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'http://feshmebel.com.ua/ua/stolovaja/stulja/kreslo-bavaria/'){
                        continue;
                    }
                    if ($product['image']) {
                        $output .= '<url>';
                        $output .= '<loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id'], $secure = false, $language['code']) . '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<lastmod>' . date('Y-m-d',strtotime($product['date_modified'])) . '</lastmod>';
                        $output .= '<priority>1.0</priority>';
                        $output .= '</url>';
                    }
                }

                $this->load->model('catalog/category');

                $this->load->model('catalog/manufacturer');

                $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

                foreach ($manufacturers as $manufacturer) {
                    $output .= '<url>';
                    $output .= '<loc>' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'] , $secure = false, $language['code']) . '</loc>';
                    $output .= '<changefreq>weekly</changefreq>';
                    $output .= '<priority>0.9</priority>';
                    $output .= '</url>';

                    $products = $this->model_catalog_product->getProducts(array('filter_manufacturer_id' => $manufacturer['manufacturer_id']));

                    /*				foreach ($products as $product) {
                                        $output .= '<url>';
                                        $output .= '<loc>' . $this->url->link('product/product', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $product['product_id']) . '</loc>';
                                        $output .= '<changefreq>monthly</changefreq>';
                                        $output .= '<priority>1.0</priority>';
                                        $output .= '</url>';
                                    }*/
                }

                $this->load->model('catalog/information');

                $informations = $this->model_catalog_information->getInformations();

                foreach ($informations as $information) {
                    if(
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'http://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/ua/help/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/uslovija-soglashenija/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/ua/uslovija-soglashenija/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'http://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd/'
                    ){
                        continue;
                    }
                    $output .= '<url>';
                    $output .= '<loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) . '</loc>';
                    $output .= '<changefreq>monthly</changefreq>';
                    $output .= '<priority>0.5</priority>';
                    $output .= '</url>';
                }

            }




            $output .= '</urlset>';

            //для генерации всех товаров в CSV
//            $this->generateProductsCSV();

            $this->response->addHeader('Content-Type: application/xml');
            $this->response->setOutput($output);

            }
        }
    }

    public function ua() {

        if ($this->config->get('google_sitemap_status')) {
            if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "")
            {
                $output  = '<?xml version="1.0" encoding="UTF-8"?>';
                $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

                $this->load->model('catalog/category');
                $this->load->model('catalog/product');



                $product_filters = $this->db->query("SELECT * FROM `oc_product_filter")->rows;
                $product_filters_arr = array();
                foreach ($product_filters as $product_filter)
                {
                    if(!isset($product_filters_arr[$product_filter['product_id']]))
                        $product_filters_arr[$product_filter['product_id']] = array();
                    $product_filters_arr[$product_filter['product_id']][$product_filter['filter_id']] = $product_filter['filter_id'];
                }



                $languages = $this->model_catalog_category->getMyLanguages();

                $language=$languages[1];

                    $filters_group = $this->db->query("SELECT * FROM `oc_filter_group_description` AS fgd LEFT JOIN `oc_filter_group` as fg ON fg.filter_group_id = fgd.filter_group_id WHERE fgd.language_id = '".$language['language_id']."' ORDER BY fg.sort_order ")->rows;
                    $filters_group_tmp = array();
                    foreach ($filters_group as $filter_group)
                        $filters_group_tmp[$filter_group['filter_group_id']] = $filter_group;

                    $filters = $this->db->query("SELECT * FROM `oc_filter_description` AS fd LEFT JOIN `oc_filter` as f ON f.filter_id = fd.filter_id WHERE fd.language_id = '".$language['language_id']."' ORDER BY f.sort_order ")->rows;
                    $filters_tmp = array();
                    foreach ($filters as $filter)
                        $filters_tmp[$filter['filter_id']] = $filter;

                    $cats = array();
                    $this->getCategories($cats, $filters,0, '',$language);


                    foreach ($cats as $cat)
                    {
                        $output .= '<url>';
                        $output .= '<loc>' . $cat['href'] . '</loc>';
                        $output .= '<changefreq>daily</changefreq>';
                        $output .= '<priority>0.9</priority>';
                        $output .= '</url>';

                        $filter_data = array(
                            'filter_category_id' => $cat['category_id'],
                        );
                        $products = $this->model_catalog_product->getProducts($filter_data);
                        $product_filters_arr_tmp = array();
                        $product_filters_arr_tmp2 = array();
                        foreach ($products as $product) {
                            foreach ($product_filters_arr[$product['product_id']] as $filter_id) if(isset($filters_tmp[$filter_id])) {
                                $filter = $filters_tmp[$filter_id];

                                if(!isset($product_filters_arr_tmp2[$filter_id]))
                                {
                                    $product_filters_arr_tmp2[$filter_id] = $filter_id;

                                    $output .= '<url>';
                                    $output .= '<loc>' .$cat['href'].$filters_group_tmp[$filter['filter_group_id']]["filter_group_url"].'_'.$filter["filter_url"].'/' . '</loc>';
                                    $output .= '<changefreq>daily</changefreq>';
                                    $output .= '<priority>0.9</priority>';
                                    $output .= '</url>';
                                }


                                if (!isset($product_filters_arr_tmp[$filter_id]))
                                    $product_filters_arr_tmp[$filter_id] = array();

                                foreach ($product_filters_arr[$product['product_id']] as $filter_id_sub)
                                {
                                    $filter_sub = $filters_tmp[$filter_id_sub];
                                    if (
                                        $filter_id != $filter_id_sub &&
                                        !isset($product_filters_arr_tmp[$filter_id_sub][$filter_id]) &&
                                        !isset($product_filters_arr_tmp[$filter_id][$filter_id_sub]) &&
                                        $filter['filter_group_id']!=$filter_sub['filter_group_id'] &&
                                        isset($filters_group_tmp[$filter_sub['filter_group_id']])
                                    ) {
                                        $product_filters_arr_tmp[$filter_id][$filter_id_sub] = $filter_id_sub;
                                        if($filters_group_tmp[$filter['filter_group_id']]['sort_order']<$filters_group_tmp[$filter_sub['filter_group_id']]['sort_order']) {
                                            $output .= '<url>';
                                            $output .= '<loc>' . $cat['href'] . '?mfp=' . $filter['filter_group_id'] . 'f-' . $filters_group_tmp[$filter['filter_group_id']]['filter_group_url'] . '[' . $filter['filter_id'] . '],' . $filter_sub['filter_group_id'] . 'f-' . $filters_group_tmp[$filter_sub['filter_group_id']]['filter_group_url'] . '[' . $filter_sub['filter_id'] . ']' . '</loc>';
                                            $output .= '<changefreq>daily</changefreq>';
                                            $output .= '<priority>0.9</priority>';
                                            $output .= '</url>';
                                        } else {
                                            $output .= '<url>';
                                            $output .= '<loc>' . $cat['href'].'?mfp='.$filter_sub['filter_group_id'].'f-'.$filters_group_tmp[$filter_sub['filter_group_id']]['filter_group_url'].'['.$filter_sub['filter_id'].'],'.$filter['filter_group_id'].'f-'.$filters_group_tmp[$filter['filter_group_id']]['filter_group_url'].'['.$filter['filter_id'].']' . '</loc>';
                                            $output .= '<changefreq>daily</changefreq>';
                                            $output .= '<priority>0.9</priority>';
                                            $output .= '</url>';
                                        }
                                    }
                                }

                            }

                        }


                    }



                $output .= '</urlset>';

                //для генерации всех товаров в CSV
//            $this->generateProductsCSV();

                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);
            }
            else
            {

            $output  = '<?xml version="1.0" encoding="UTF-8"?>';
            $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
            $output .= '<url>';
            $output .= '<loc>http://feshmebel.com.ua/</loc>';
            $output .= '<changefreq>daily</changefreq>';
            $output .= '<priority>1</priority>';
            $output .= '</url>';
            $output .= '<url>';
            $output .= '<loc>http://feshmebel.com.ua/ua/</loc>';
            $output .= '<changefreq>daily</changefreq>';
            $output .= '<priority>1</priority>';
            $output .= '</url>';


            $this->load->model('catalog/category');
            $languages = $this->model_catalog_category->getMyLanguages();

            foreach ($languages as $language) {

//			$output .=  $language['code'].'tkach';
                $this->load->model('catalog/product');
                $this->load->model('tool/image');

                $products = $this->model_catalog_product->getProducts();

                foreach ($products as $product) {
                    if($this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') ==
                        'http://feshmebel.com.ua/stolovaja/barnye-stulja/barnyjj-taburet-tolix-tol%D1%96ks-h-76-belyjj' ||
                        $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'http://feshmebel.com.ua/stolovaja/stulja/kreslo-bavaria/' ||
                        $this->url->link('product/product', 'product_id=' . $product['product_id'], false, '') == 'http://feshmebel.com.ua/ua/stolovaja/stulja/kreslo-bavaria/'){
                        continue;
                    }
                    if ($product['image']) {
                        $output .= '<url>';
                        $output .= '<loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id'], $secure = false, $language['code']) . '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<lastmod>' . date('Y-m-d',strtotime($product['date_modified'])) . '</lastmod>';
                        $output .= '<priority>1.0</priority>';
                        $output .= '</url>';
                    }
                }

                $this->load->model('catalog/category');

                $this->load->model('catalog/manufacturer');

                $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

                foreach ($manufacturers as $manufacturer) {
                    $output .= '<url>';
                    $output .= '<loc>' . $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'] , $secure = false, $language['code']) . '</loc>';
                    $output .= '<changefreq>weekly</changefreq>';
                    $output .= '<priority>0.9</priority>';
                    $output .= '</url>';

                    $products = $this->model_catalog_product->getProducts(array('filter_manufacturer_id' => $manufacturer['manufacturer_id']));

                    /*				foreach ($products as $product) {
                                        $output .= '<url>';
                                        $output .= '<loc>' . $this->url->link('product/product', 'manufacturer_id=' . $manufacturer['manufacturer_id'] . '&product_id=' . $product['product_id']) . '</loc>';
                                        $output .= '<changefreq>monthly</changefreq>';
                                        $output .= '<priority>1.0</priority>';
                                        $output .= '</url>';
                                    }*/
                }

                $this->load->model('catalog/information');

                $informations = $this->model_catalog_information->getInformations();

                foreach ($informations as $information) {
                    if(
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'http://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/ua/help/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/uslovija-soglashenija/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) == 'http://feshmebel.com.ua/ua/uslovija-soglashenija/' or
                        $this->url->link('information/information', 'information_id=' . $information['information_id'], false, '') == 'http://feshmebel.com.ua/index.php%3Froute%3Daccount%2Freturn%2Fadd/'
                    ){
                        continue;
                    }
                    $output .= '<url>';
                    $output .= '<loc>' . $this->url->link('information/information', 'information_id=' . $information['information_id'], $secure = false, $language['code']) . '</loc>';
                    $output .= '<changefreq>monthly</changefreq>';
                    $output .= '<priority>0.5</priority>';
                    $output .= '</url>';
                }

            }




            $output .= '</urlset>';

            //для генерации всех товаров в CSV
//            $this->generateProductsCSV();

            $this->response->addHeader('Content-Type: application/xml');
            $this->response->setOutput($output);

            }
        }
    }

    public function robots() {

        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "")
        {
            $output = file_get_contents('./robots_https.txt');
        }
        else
        {
            $output = file_get_contents('./robots_http.txt');
        }
        $this->response->addHeader('Content-Type: text/plain; charset=UTF-8');
        $this->response->setOutput($output);

    }

    protected function getCategories(&$cats, $filters,$parent_id, $current_path='',$lang) {

        $this->load->model('catalog/category');
        $results = $this->model_catalog_category->getCategoriesMod($parent_id,$lang['language_id']);
        foreach ($results as $result) {

            if (!$current_path) {
                $new_path = $result['category_id'];
            } else {
                $new_path = $current_path . '_' . $result['category_id'];
            }

            $this->load->model('catalog/category');

            $cats[]= array(
                'category_id' => $result['category_id'],
                'name' => $result['name'],
                'href' => $this->url->link('product/category', 'path=' . $new_path, $secure = false, $lang['code'] )
            );

            $this->getCategories($cats,$filters, $result['category_id'], $new_path,$lang);
        }

    }
	
	protected function getCategoriesGraber($parent_id, $current_path='') {

        $this->load->model('catalog/category');
        $results = $this->model_catalog_category->getCategories($parent_id);
        foreach ($results as $result) {

            if (!$current_path) {
                $new_path = $result['category_id'];
            } else {
                $new_path = $current_path . '_' . $result['category_id'];
            }

            $this->load->model('catalog/category');

            echo $this->url->link('product/category', 'path=' . $new_path, $secure = false)."\n";
            echo $this->url->link('product/category', 'path=' . $new_path, $secure = false,'ua')."\n";
            $this->getCategoriesGraber($result['category_id'], $new_path);
        }

    }
	
    /*

    protected function getCategories($parent_id, $current_path = '', $code,$http='') {

		$this->load->model('catalog/category');
		$languages = $this->model_catalog_category->getMyLanguages();
		
		$output = '';

		$results = $this->model_catalog_category->getCategories($parent_id);

		// start Tkach web-promo		
		$filtersUrl = array();
		if (is_readable ($_SERVER ['DOCUMENT_ROOT']."/seo/filterUrl.csv")) {
		        $handle = fopen ($_SERVER ['DOCUMENT_ROOT']."/seo/filterUrl.csv", "r");
		        while (!feof ($handle)) { 
		         $buffer = fgets($handle, 9999); 
		         $data = explode (";", $buffer);
		         if ( !empty($data [0]) ) { 
					array_push($filtersUrl, $data [0]);
		         }     
		        } 
		        fclose ($handle);   
		   }
		   // end web-promo

		foreach ($results as $result) {
			
			if (!$current_path) {
				$new_path = $result['category_id'];
			} else {
				$new_path = $current_path . '_' . $result['category_id'];
			}
			
			$this->load->model('catalog/category');
			$languages = $this->model_catalog_category->getMyLanguages();
									
			foreach ($languages as $language) {			
			$output .= '<url>';
			$output .= '<loc>' . $this->url->link('product/category', 'path=' . $new_path, $secure = false, $language['code'],$http ) . '</loc>';
			$output .= '<changefreq>daily</changefreq>';
			$output .= '<priority>0.9</priority>';
			$output .= '</url>';
			
			}
						
			// start Tkach web-promo

			$filters = $this->model_catalog_category->getCategoryFiltersSeoUrl();
			foreach ($filters as $filter) {
				if (!$current_path) {
					$new_path = $filter['category_id'];
				} else {
					$new_path = $current_path . '_' . $filter['category_id'];
				}

				if ($filter["language_id"] == 1){
					$filterl = 'ru';
				}else{
					$filterl = 'ua';
				}



				if( $result["category_id"] == $filter["category_id"] and $result['status'] == 1 )
				{
	$filter_url = $this->url->link('product/category', 'path=' . $new_path, $secure = false, $filterl,$http).$filter["filter_group_url"].'_'.$filter["filter_url"].'/';

				if( !in_array($filter_url, $filtersUrl) ){
					$output .= '<url>';
					$output .= '<loc>'.$filter_url. '</loc>';
					$output .= '<changefreq>daily</changefreq>';
					$output .= '<priority>0.8</priority>';
					$output .= '</url>';					
				}
				
				}				
			}			
			// end web-promo

//			$products = $this->model_catalog_product->getProducts(array('filter_category_id' => $result['category_id']));

/*			foreach ($products as $product) {
if($this->url->link('product/product', 'product_id=' . $product['product_id']) ==
'http://roomix.com.ua/stolovaja/barnye-stulja/barnyjj-taburet-tolix-tol%D1%96ks-h-76-belyjj'){
	continue;
}
				$output .= '<url>';
				$output .= '<loc>' . $this->url->link('product/product', 'path=' . $new_path . '&product_id=' . $product['product_id'], $secure = false, $language['code']) . '</loc>';
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>0.9</priority>';
				$output .= '</url>';
			}*\/

			$output .= $this->getCategories($result['category_id'], $new_path, $code,$http);
		}
			return $output;
	}
       */
    protected function generateProductsCSV(){
        $this->load->model('catalog/product');
        $products = $this->model_catalog_product->getProducts();
        $this->load->model('catalog/category');
		$languages = $this->model_catalog_category->getMyLanguages();
        
        $handle = @fopen($_SERVER['DOCUMENT_ROOT'].'/products.csv', 'w+');
        file_put_contents($handle, '');
        
        foreach($languages as $language){
            foreach($products as $product){
                fwrite($handle, $this->url->link('product/product', 'product_id=' . $product['product_id'], false, $language['code']). PHP_EOL);
            }
        }
        fclose($handle);

    }
	
}