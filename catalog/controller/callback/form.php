<?php
class ControllerCallbackForm extends Controller {
    public function index() {

        $email_to = $this->config->get('config_email');
        if($this->request->post['csrf'] == $this->session->data['csrf']){

            $prod_info = $this->getProductInfo();

          $info = "ФИО: " . $this->request->post['name'] . "<br/>Телефон: " . $this->request->post['phone'] . "<br/>Адрес доставки: " . $this->request->post['address'];

          $info .= $prod_info;
            $headers = "From: no-reply@feshmebel.com.ua" .  "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1";


            var_dump(mail($email_to, "Заявка с формы 'Примерить'",$info, $headers));
            
        }
    }


    private function getProductInfo(){
        $this->load->model('catalog/product');
        $info = '';
        $product_info = $this->model_catalog_product->getProduct((int) $this->request->post['prod_id']);

        if($product_info){
            $prod_link = $this->url->link('product/product', 'product_id=' . $product_info['product_id']);
            $info .= '<br/>Товар: <a href="'.$prod_link.'">'.$product_info['name'].'</a>';
            if(!empty($this->request->post['options'])){
                $options = array();
                foreach ( explode(',',$this->request->post['options'] ) as $option_id){
                    $option = $this->model_catalog_product->getOptionByValue($option_id);
                    $info .= '<br/>'.$option['name'] . ': ' . $option['value']['name'];
                }
            }

        }

        return $info;
    }
}
