<?php
// exit('Already imported');
include'config.php';
$data = [
['https://feshmebel.com.ua/stolovaja/barnye-stulja/barnyj-stul-tolix-toliks-mc-012k-h-76/','Алекс','','5','Очень хорош в исполнении. Порадовало то, что стульчик можно ставить где угодно. Главное, только подобрать тематические аксессуары. Круто смотрится сочетание разных по фактуре материалов — дерева и металла. Кстати это же позволяет сделать конструкцию более устойчивой, придать массивности.'],
['https://feshmebel.com.ua/stolovaja/barnye-stulja/barnyj-stul-tolix-toliks-mc-012k-h-76/','Антон 95','','5','Мне нравится эта модификация. Смотрится отлично. Рад, что купил ее. Особо не тестил еще, два дня как приехала. Но добротно смотрится. Надежно прям, солидно.'],
['https://feshmebel.com.ua/stolovaja/barnye-stulja/barnyj-stul-tolix-toliks-mc-012k-h-76/','Адель','','5','Впечатлила брутальность этих стульев. Как раз это и побудило открыть кафе с морским оформлением. Эти стулья будто было создано прямо для моего зала. Устойчивые, с классным сочетанием двух разных фактур материалов. Мой восторг не передать словами.'],
['https://feshmebel.com.ua/stolovaja/barnye-stulja/barnyj-stul-tolix-toliks-mc-012k-h-76/','Валерий Николаевич','','4','Неплохой вариант. сначала понравился, но потом поняли, что в квартире места такой конструкции не будет. Пришлось забрать в загородный дом. Там эти стулья пришлись ко двору)). Теперь ходим обедать с ними только на улицу. Особенно нравится, что их можно сложить один в один. Так они меньше пачкаются и портятся.'],
['https://feshmebel.com.ua/stolovaja/barnye-stulja/barnyj-stul-tolix-toliks-mc-012k-h-76/','Сосиска','','4','Модель в целом неплоха. Но не нравится, что нет возможности регуляции высоты. Это делает «заползание» на стул трудоемким процессом для низкорослого человека. А так все хорошо. Тоже понравилось сочетание дерева с металлом. Это смотрится благородно, брутально и в тоже время органично.'],
['https://feshmebel.com.ua/stolovaja/barnye-stulja/barnyj-stul-tolix-toliks-mc-012k-h-76/','Дима','','5','Давно подыскивали такую модель для загородного дома.'],
['https://feshmebel.com.ua/stolovaja/stulja/stul-chic-velvet /','Ольга','','5','У меня в квартире каждая комната имеет свой цвет и надо было подобрать мебель, чтобы хоть как-то сочеталась с интерьером. Стульчики Вельвет заказала сразу в трех разных оттенках и не прогадала. Для женщины важно, чтобы каждая деталь в доме была на своём месте и Фешмебель помогли осуществить мне мою идею, огромное спасибо.'],
['https://feshmebel.com.ua/stolovaja/stulja/stul-chic-velvet /','Никита','','5','При таких финансовых сложностях, как сегодня, тяжело выделить значительную сумму на обновку комнат в доме. Такие стулья являются настоящим маст-хевом, если надо приобрести мебель быстро и недорого. Хорошая зборка и приемлемая цена очень порадовали. На сайте есть возможность выбрать подходящий цвет, что очень хорошо.'],
['https://feshmebel.com.ua/stolovaja/stulja/stul-chic-velvet /','Зинаида','','5','Небольшой и недорогой стул стал отличной покупкой для нашей дачи. Пожалели, что взяли всего один, все-таки семья у нас большая, а заказ пришёл быстро и полностью целый. Думала, что с мойкой будут проблемы, но материал отлично обрабатывается мокрой губкой. И ещё отметила лёгкий вес изделия, так что при уборке поднимать его просто.'],
['https://feshmebel.com.ua/stolovaja/stulja/stul-chic-velvet /','Тема','','5','Я не думал, что стулья могут быть настолько разными. Впервые делал покупку через интернет,  менеджер рассказал мне, как правильно сделать выбор и помог определиться с желаниями. В итоге, получил за копейки так сказать надежного «коня», да еще и очень симпатичного в нескольких цветах. Материал сиденья приятный на ощупь и к телу, комфортно пользоваться в жару, он как-будто холодит.'],
['https://feshmebel.com.ua/stolovaja/stulja/stul-chic-velvet /','Олег','','5','Строгий и лаконичный дизайн кресла подходит под любой интерьер, универсально.'],
];

$link = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
if (!$link){
    exit("Error");
}

// var_dump($link->character_set_name());
$link->set_charset('utf8');
// var_dump($link->character_set_name());
$uri_parts = [];
foreach ($data as $row) {
    $url = $row[0];
    $name = $row[1];
    $date = $row[2];
    $score = $row[3];
    $text = $row[4];
    $uri_part = explode('/', $url);
    array_pop($uri_part);
    $uri_part = array_pop($uri_part);
    $uri_parts[] = $uri_part;
}
$uri_parts = array_unique($uri_parts);
$uri_parts_ids = [];
foreach ($uri_parts as $uri_part) {
    $q = "SELECT * from oc_url_alias where keyword REGEXP'^".$uri_part."$'";
    $result = $link->query($q);
    
    while ($row = $result->fetch_assoc()) {
        $uri_parts_ids[mb_strtolower($row['keyword'])] = array_pop(explode('=', $row['query']));
    }
}

$result->close();

$date_start = strtotime("15.01.2020");
$date_end = strtotime("02.07.2020");
$insert_rows = [];
foreach ($data as $row) {
    $url = $row[0];
    $name = trim($row[1]);
    $date = $row[2];
    $score = $row[3];
    $text = trim($row[4]);
    $date = date('Y-m-d H:i:s', mt_rand($date_start, $date_end));
    $uri_part = explode('/', $url);
    array_pop($uri_part);
    $uri_part = array_pop($uri_part);
    $product_id = $uri_parts_ids[$uri_part];
    if (is_null($product_id)){
        var_dump($url);
    }
    $insert_q ='INSERT INTO oc_review (product_id, parent_id, sorthex, customer_id, author, text, rating, rating_mark, status, language_id, type_id, cmswidget, date_added, date_modified) VALUES '.'('.$product_id.', 0, 0, 0, "'.$name.'", "'.$link->real_escape_string($text).'",'.$score.',0, 1, 1, 1, 0,"'.$date.'", "'.$date.'")';
    $link->query($insert_q);
    // $insert_rows[] ='('.$product_id.', 0, 0, 0, "'.$name.'", "'.$link->real_escape_string($text).'",'.$score.',0, 1, 1, 1, 0,"'.$date.'", "'.$date.'")';
    // break;
}

//$insert_q ='INSERT INTO oc_review (product_id, parent_id, sorthex, customer_id, author, text, rating, rating_mark, status, language_id, type_id, cmswidget, date_added, date_modified) VALUES'.implode(',', $insert_rows).';';
// var_dump($insert_q);exit();

var_dump($link->error);
$link->close();
exit();