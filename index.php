<?php
global $get_param;
$get_param = explode( "?", $_SERVER['REQUEST_URI']);
if(isset($get_param[1]))
{

    parse_str($get_param[1], $result);
    unset($result['li_op']);
    unset($result['sort']);
    unset($result['order']);
    unset($result['page']);
    $get_param = urldecode(http_build_query($result));

    if(!empty($get_param))
        $get_param = '?'.$get_param;

}
else
    $get_param = '';


header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");

if(isset($_GET['mfp']))
	$_COOKIE['mfp']=$_GET['mfp'];
/*
 * start Tkach web-promo 
 * redirect
 */

/* $currPage = strtok($_SERVER['REQUEST_URI'], '?');

if($currPage === '/stolovaja/stul-kasper-prozrachne'){
	if(isset($_GET['test'])){
		print_r($_REQUEST);
		exit();
	}
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://feshmebel.com.ua/stolovaja/stul-kasper-prozrachne/");
} */
$_url = $_SERVER['REQUEST_URI'];

if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])){
	$_SERVER["REMOTE_ADDR"] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}

if (is_readable ($_SERVER ['DOCUMENT_ROOT']."/301.csv")) {
    $handle = fopen ($_SERVER ['DOCUMENT_ROOT']."/301.csv", "r");
    while (!feof ($handle)) {
        $buffer = fgets($handle, 9999);
        $data = explode (";", $buffer);
        if ($data [0] == $_url || $data [0]==$_SERVER['REQUEST_URI']){

            header("HTTP/1.1 301 Moved Permanently");
            header("Location: "."https://".$_SERVER['HTTP_HOST'].trim($data [1]));
            exit();
        }
    }
    fclose ($handle);
}


if((!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "")&&$_SERVER['REQUEST_URI']!='/sitemap.xml'&&$_SERVER['REQUEST_URI']!='/robots.txt')
{
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header("HTTP/1.1 301 Moved Permanently");
    header("Location: $redirect");
}
if(!isset($_GET['ajax']))
{
    $pos = mb_ereg( '//', $_SERVER['REQUEST_URI'] );
    if ( $pos != false){

        $url = strtolower(preg_replace('|([/]+)|s', '/', $_SERVER['REQUEST_URI']));
        header("HTTP/1.1 301 Moved Permanently");
        header('Location: /'.$url );

    }

    if(strstr($_SERVER['REQUEST_URI'], '/page-1/')){
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ". str_replace( '/page-1/', '/', $_SERVER['REQUEST_URI'] ) );
        exit();
    }

// redirect from old site

    if (substr($_url , -1) != '/'){
        if(strstr($_url, 'page-')){
            /*
            $_url = $_url.'/';
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".trim($_url));
            exit();
            */
        }else{$_url = $_url.'/';}
    }


    /*******************************************************************************************************/
    $filter_redirect = require_once($_SERVER['DOCUMENT_ROOT'].'/301_for_filter.php');
    if(isset($_GET['test'])){
        $currPageLang = 1;
        $currUrl = strtok($_SERVER['REQUEST_URI'], '?');
        $currUrlArrTemp = explode("/", $currUrl);
        $currUrlArr = array_diff($currUrlArrTemp, array(''));
        if($currUrlArr[1] == 'ua'){
            $currPageLang = 2;
        }

        $paramPageFilter = array_pop($currUrlArr);

        if(isset($filter_redirect['lang_'.$currPageLang][$paramPageFilter])){
            $redirectUrl = "/".implode("/", $currUrlArr)."/".$filter_redirect['lang_'.$currPageLang][$paramPageFilter]."/";
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$redirectUrl);
            exit();
        }
    }
    /*******************************************************************************************************/

    if (is_readable ($_SERVER ['DOCUMENT_ROOT']."/redirect.csv")) {
        $handle = fopen ($_SERVER ['DOCUMENT_ROOT']."/redirect.csv", "r");
        while (!feof ($handle)) {
            $buffer = fgets($handle, 9999);
            $data = explode (";", $buffer);
            if ( strstr($_SERVER['REQUEST_URI'], '/'.$data [0].'/') ){
                $redirect = str_replace($data [0], $data [1], $_SERVER['REQUEST_URI']);
                header("HTTP/1.1 301 Moved Permanently");
                header("Location: ".$redirect);
                exit();
            }
        }
        fclose ($handle);
    }


// pagination
    if(strstr($_SERVER['REQUEST_URI'], '?page=')){
        preg_match("/(.*)\?page=([0-9]+)(.*)/", $_SERVER['REQUEST_URI'], $output_array);
        if(substr($output_array[1] , -1) == '/'){$output_array[1]  = substr($output_array[1], 0, -1);}
        $url= $output_array[1].'/page-'.$output_array[2].'/'.$output_array[3];
        header("HTTP/1.1 301 Moved Permanently");
        header("Location: ".trim($url));
        exit();
    }

    if(strstr($_SERVER['REQUEST_URI'], '&mfilterAjax=')){
        $_SERVER['REQUEST_URI'] = preg_replace('/(.*)\&mfilterAjax=([0-9]+)(.*)/', '', $_SERVER['REQUEST_URI']);
    }


    if(strstr($_SERVER['REQUEST_URI'], '/page-')){
        preg_match("/(.*)page-([0-9]++)(.*)/Uis", $_SERVER['REQUEST_URI'], $output_array);
        if(isset($output_array[1]) && isset($output_array[3])){
            if(strstr($output_array[1].$output_array[3], '?')){
                $_SERVER['QUERY_STRING'] = $_SERVER['REQUEST_URI'] = $output_array[1].$output_array[3].'&page='.$output_array[2];
            }else{
                $_SERVER['QUERY_STRING'] = $_SERVER['REQUEST_URI'] = $output_array[1].$output_array[3].'?page='.$output_array[2];
            }
            $_GET['_route_'] = substr($output_array[1].'/', 1);
            $_GET['page'] = $output_array[2];
            define('page_for_seo', $output_array[2]);
        }
    }
/*
//
    $url=$_SERVER['REQUEST_URI'];
    if (is_readable ($_SERVER ['DOCUMENT_ROOT']."/meta.csv")) {
        $handle = fopen ($_SERVER ['DOCUMENT_ROOT']."/meta.csv", "r");
        while (!feof ($handle)) {
            $buffer = fgets($handle, 9999);
            $data = explode (";", $buffer);
            if ($data [0] == $url) {
                define("TITLE", $data[1]);
                define("DESCRIPTION", $data[2]);
                define("H_ONE", $data[3]);
            }
        }
        fclose ($handle);
    }
// end web-promo
*/
}


// Version
define('VERSION', '2.1.0.0');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

if (file_exists($li = DIR_APPLICATION.'/controller/extension/lightning/gamma.php')) require_once($li); //Lightning

// VirtualQMOD
require_once('./vqmod/vqmod.php');
VQMod::bootup();

// VQMODDED Startup
require_once(VQMod::modCheck(DIR_SYSTEM . 'startup.php'));

// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
$config = new Config();
$registry->set('config', $config);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);
$registry->set('db', $db);

// Store
if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
	$store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`ssl`, 'www.', '') = '" . $db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
} else {
	$store_query = $db->query("SELECT * FROM " . DB_PREFIX . "store WHERE REPLACE(`url`, 'www.', '') = '" . $db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
}

// Start Web-promo Tkach




    $url = $_SERVER['REQUEST_URI'];
	$one_url = strtok (  $url ,'?' );
	$two_url = strstr ( $url , '?');
	$lowerURI = strtolower ( $one_url );
	if( $one_url != $lowerURI )
	{
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://".$_SERVER['HTTP_HOST'].$lowerURI.$two_url);
	exit();
	}

	if( $_SERVER['REQUEST_URI'] == '/index.php?route=common/home' )
	{
	header("HTTP/1.1 301 Moved Permanently");
	header("Location: https://".$_SERVER['HTTP_HOST']);
	exit();
	}

// End Web-promo

if ($store_query->num_rows) {
	$config->set('config_store_id', $store_query->row['store_id']);
} else {
	$config->set('config_store_id', 0);
}

// Settings
$query = $db->query("SELECT * FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' OR store_id = '" . (int)$config->get('config_store_id') . "' ORDER BY store_id ASC");

foreach ($query->rows as $result) {
	if (!$result['serialized']) {
		$config->set($result['key'], $result['value']);
	} else {
		$config->set($result['key'], json_decode($result['value'], true));
	}
}

if (!$store_query->num_rows) {
	$config->set('config_url', HTTP_SERVER);
	$config->set('config_ssl', HTTPS_SERVER);
}

// Url
$url = new Url($config->get('config_url'), $config->get('config_secure') ? $config->get('config_ssl') : $config->get('config_url'));
$registry->set('url', $url);

// Log
$log = new Log($config->get('config_error_filename'));
$registry->set('log', $log);

function error_handler($code, $message, $file, $line) {
	global $log, $config;

	// error suppressed with @
	if (error_reporting() === 0) {
		return false;
	}

	switch ($code) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$error = 'Notice';
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$error = 'Warning';
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$error = 'Fatal Error';
			break;
		default:
			$error = 'Unknown';
			break;
	}

	if ($config->get('config_error_display')) {
		echo '<b>' . $error . '</b>: ' . $message . ' in <b>' . $file . '</b> on line <b>' . $line . '</b>';
	}

	if ($config->get('config_error_log')) {
		$log->write('PHP ' . $error . ':  ' . $message . ' in ' . $file . ' on line ' . $line);
	}

	return true;
}

// Error Handler
set_error_handler('error_handler');

// Request
$request = new Request();
$registry->set('request', $request);

// Response
$response = new Response();
$response->addHeader('Content-Type: text/html; charset=utf-8');
$response->setCompression($config->get('config_compression'));
$registry->set('response', $response);

// Cache
$cache = new Cache('file');
$registry->set('cache', $cache);

// Session
if (isset($request->get['token']) && isset($request->get['route']) && substr($request->get['route'], 0, 4) == 'api/') {
	$db->query("DELETE FROM `" . DB_PREFIX . "api_session` WHERE TIMESTAMPADD(HOUR, 1, date_modified) < NOW()");

	$query = $db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "api` `a` LEFT JOIN `" . DB_PREFIX . "api_session` `as` ON (a.api_id = as.api_id) LEFT JOIN " . DB_PREFIX . "api_ip `ai` ON (as.api_id = ai.api_id) WHERE a.status = '1' AND as.token = '" . $db->escape($request->get['token']) . "' AND ai.ip = '" . $db->escape($request->server['REMOTE_ADDR']) . "'");

	if ($query->num_rows) {
		// Does not seem PHP is able to handle sessions as objects properly so so wrote my own class
		$session = new Session($query->row['session_id'], $query->row['session_name']);
		$registry->set('session', $session);

		// keep the session alive
		$db->query("UPDATE `" . DB_PREFIX . "api_session` SET date_modified = NOW() WHERE api_session_id = '" . $query->row['api_session_id'] . "'");
	}
} else {
	$session = new Session();
	$registry->set('session', $session);
}

if(empty($session->data['csrf'])) $session->data['csrf'] = time() . md5('Bathroom Salt');

// Language Detection
$languages = array();

$query = $db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE status = '1'");

foreach ($query->rows as $result) {
	$languages[$result['code']] = $result;
}

if (isset($session->data['language']) && array_key_exists($session->data['language'], $languages)) {
	$code = $session->data['language'];
} elseif (isset($request->cookie['language']) && array_key_exists($request->cookie['language'], $languages)) {
	$code = $request->cookie['language'];
} else {
	$detect = '';

	if (isset($request->server['HTTP_ACCEPT_LANGUAGE']) && $request->server['HTTP_ACCEPT_LANGUAGE']) {
		$browser_languages = explode(',', $request->server['HTTP_ACCEPT_LANGUAGE']);

		foreach ($browser_languages as $browser_language) {
			foreach ($languages as $key => $value) {
				if ($value['status']) {
					$locale = explode(',', $value['locale']);

					if (in_array($browser_language, $locale)) {
						$detect = $key;
						break 2;
					}
				}
			}
		}
	}

	$code = $detect ? $detect : $config->get('config_language');
}

if (!isset($session->data['language']) || $session->data['language'] != $code) {
	$session->data['language'] = $code;
}

if (!isset($request->cookie['language']) || $request->cookie['language'] != $code) {
	setcookie('language', $code, time() + 60 * 60 * 24 * 30, '/', $request->server['HTTP_HOST']);
}

$config->set('config_language_id', $languages[$code]['language_id']);
$config->set('config_language', $languages[$code]['code']);

// Language
$language = new Language($languages[$code]['directory']);
$language->load($languages[$code]['directory']);
$registry->set('language', $language);

// Document
$registry->set('document', new Document());

// Customer
$customer = new Customer($registry);
$registry->set('customer', $customer);

// Customer Group
if ($customer->isLogged()) {
	$config->set('config_customer_group_id', $customer->getGroupId());
} elseif (isset($session->data['customer']) && isset($session->data['customer']['customer_group_id'])) {
	// For API calls
	$config->set('config_customer_group_id', $session->data['customer']['customer_group_id']);
} elseif (isset($session->data['guest']) && isset($session->data['guest']['customer_group_id'])) {
	$config->set('config_customer_group_id', $session->data['guest']['customer_group_id']);
}

// Tracking Code
if (isset($request->get['tracking'])) {
	setcookie('tracking', $request->get['tracking'], time() + 3600 * 24 * 1000, '/');

	$db->query("UPDATE `" . DB_PREFIX . "marketing` SET clicks = (clicks + 1) WHERE code = '" . $db->escape($request->get['tracking']) . "'");
}

// Affiliate
$registry->set('affiliate', new Affiliate($registry));

// Currency
$registry->set('currency', new Currency($registry));

// Tax
$registry->set('tax', new Tax($registry));

// Weight
$registry->set('weight', new Weight($registry));

// Length
$registry->set('length', new Length($registry));

// Cart
$registry->set('cart', new Cart($registry));

// Encryption
$registry->set('encryption', new Encryption($config->get('config_encryption')));

// OpenBay Pro
$registry->set('openbay', new Openbay($registry));

// Event
$event = new Event($registry);
$registry->set('event', $event);

$query = $db->query("SELECT * FROM " . DB_PREFIX . "event");

foreach ($query->rows as $result) {
	$event->register($result['trigger'], $result['action']);
}

// Front Controller
$controller = new Front($registry);

// Maintenance Mode
$controller->addPreAction(new Action('common/maintenance'));

// SEO URL's
if (!$seo_type = $config->get('config_seo_url_type')) {
    $seo_type = 'seo_url';
}
$controller->addPreAction(new Action('common/' . $seo_type));

// Router
if (isset($request->get['route'])) {
	$action = new Action($request->get['route']);
} else {
	$action = new Action('common/home');
}

// Start web-promo


$ajaxRequest = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

if(!empty($_REQUEST['mfp']) && !$ajaxRequest){
    if(substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/')
    {
        $my_language_id = 2;
    }else{
        $my_language_id = 1;
    }

    if(preg_match('/^([\d]+)f-([\w-_]+)\[([\d]+)\]$/', $_REQUEST['mfp'], $matches)){
        $filterUrls	= $db->query(
            "SELECT * 
				FROM " . DB_PREFIX . "filter_description 
				WHERE filter_id = '" . $matches[3] . "'
			   AND language_id = '" . $my_language_id . "' LIMIT 1");



        if($filterUrls->rows){
            $get = '';

            if (isset($request->get['sort'])) {
                $get .= '&sort=' . $request->get['sort'];
            }

            if (isset($request->get['order'])) {
                $get .= '&order=' . $request->get['order'];
            }

            if (isset($request->get['limit'])) {
                $get .= '&limit=' . $request->get['limit'];
            }

            if($get!='')
                $get='?'.$get;


                //$filter_url =  'https://feshmebel.com.ua/'.($my_language_id == 2 ? '/ua/' : '' ) .$request->get['_route_']. $filterUrls->rows[0]['filter_group_url'] . "_" . $filterUrls->rows[0]['filter_url'] . "/".$get;
                $filter_url =  'https://feshmebel.com.ua/'.$request->get['_route_']. $filterUrls->rows[0]['filter_group_url'] . "_" . $filterUrls->rows[0]['filter_url'] . "/".$get;
                header("HTTP/1.1 301 Moved Permanently");
                header('Location: ' . $filter_url );


        }

    }
}

$url_filt  = strtok ($_SERVER['REQUEST_URI'] ,'?');
if(substr($url_filt , -1) == '/')
{
	$url_filt  = substr($url_filt, 0, -1);
}
	$endExplodeEndSeoUrl = end(explode( "/", $url_filt));
	$explodeEndSeoUrl = explode("_", $endExplodeEndSeoUrl);

	$myFilter = array();

	if(substr($_SERVER['REQUEST_URI'], 0, 4) == '/ua/')
	{
		$my_language_id = 2;
	}else{
		$my_language_id = 1;
	}

	$filterUrls	= $db->query(
				"SELECT * 
				FROM " . DB_PREFIX . "filter_description 
				WHERE filter_group_url = '" . $explodeEndSeoUrl[0] . "'
				AND filter_url = '" . $explodeEndSeoUrl[1] . "' AND language_id = '" . $my_language_id . "'");

    foreach ($filterUrls->rows as $row)
    {
    	$myFilter["filter_id"] 			= $row["filter_id"];
    	$myFilter["filter_group_id"] 	= $row["filter_group_id"];
    	$myFilter['filter_group_url'] 	= $row['filter_group_url'];
    	$myFilter['filter_url'] 		= $row['filter_url'];
    	$myFilter['name']				= $row['name'];
    	$myFilter['language_id']		= $row['language_id'];
	}

	$is_mfp = strpos($_SERVER['REQUEST_URI'] ,'?mfp=');

if ( isset($myFilter['filter_id'] ) and $myFilter['filter_id'] )
{
	if ($myFilter['language_id'] != $my_language_id)
	{
		$lang_redirect	= $db->query(
					"SELECT * 
					FROM " . DB_PREFIX . "filter_description 
					WHERE filter_id = '" . $row["filter_id"] . "'
					AND language_id = " . $my_language_id . "");

		foreach ($lang_redirect->rows as $row){ $filterRedirect = $row["filter_group_url"] .'_'.$row["filter_url"]; }

		if($endExplodeEndSeoUrl != $filterRedirect)
		{
			$replaceFilterRedirect = str_replace( $endExplodeEndSeoUrl, $filterRedirect, $url_filt);
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: https://feshmebel.com.ua'.$replaceFilterRedirect."/" );
			exit();
		}
	}
}

if ( isset($myFilter['filter_id'] ) and $myFilter['filter_id'] != NULL or $is_mfp)
{

	$fquery	= $db->query(
				"SELECT * 
				FROM " . DB_PREFIX . "filter_group_description 
				WHERE filter_group_id = '" . $myFilter["filter_group_id"] . "' 
				AND language_id = '" . $my_language_id . "'");

	foreach ($fquery->rows as $row){ $myFilter['filter_group_seo_url'] 	= $row['filter_group_url']; $myFilter['subname'] = $row['subname']; }

	$lang_url_query	= $db->query(
				"SELECT * 
				FROM " . DB_PREFIX . "filter_description 
				WHERE filter_group_url = '" . $explodeEndSeoUrl[0] . "'
				AND filter_url = '" . $explodeEndSeoUrl[1] . "'");

	foreach ($lang_url_query->rows as $row)
    {
    	$langUrlFilter['filter_url'] 			= $row['filter_url'];
    	$langUrlFilter['filter_group_seo_url'] 	= $row['filter_group_url'];
	}

	$newRoute = str_replace($explodeEndSeoUrl[0].'_'.$explodeEndSeoUrl[1], '', $url_filt);
	$newMfp = $myFilter["filter_group_id"] . 'f-' . $myFilter['filter_group_seo_url'] . '['. $myFilter["filter_id"] .']';

	if(substr($newRoute, 0, 1) == '/')
	{
		$newRoute = substr($newRoute, 1);
	}

	if($is_mfp)
	{
	$request->get['_route_'] 	= strtok ($newRoute ,'?');
	$request->get['mfp'] 		= substr($_SERVER['REQUEST_URI'], $is_mfp);
	}else{
	$request->get['_route_'] 	= $newRoute;
	$request->get['mfp'] 		= $newMfp;

	$fflName = $myFilter['subname'] ? $myFilter['subname'].' - '.$myFilter['name'] : $myFilter['name'];
	//define("_FILTER_NAME_", $myFilter['name']);
	define("_FILTER_NAME_", $fflName);

	define("_FILTER_LANGUAGE_", $langUrlFilter['filter_group_seo_url'].'_'.$langUrlFilter['filter_url']);
	}


	// constants
	define("_NEW_ROUTE_",				$newRoute);
	define("_FILTER_ID_",				$myFilter["filter_id"]);
	define("_FILTER_SEO_NAME_",			$myFilter['filter_url']);
	define("_FILTER_GROUP_ID_",			$myFilter["filter_group_id"]);
	define("_FILTER_GROUP_SEO_URL_",	$myFilter['filter_group_seo_url']);

		$currpage_lang_urls	= $db->query(
					"SELECT * 
					FROM " . DB_PREFIX . "filter_description 
					WHERE filter_id = '" . _FILTER_ID_ . "'");
		$currpage_lang_urls_arr = $currpage_lang_urls->rows;
		

		if(isset($_GET['page'])){
			define('_CURRPAGE_LANG'.$currpage_lang_urls_arr[0]['language_id'].'_URL_', _NEW_ROUTE_.$currpage_lang_urls_arr[0]['filter_group_url'].'_'.$currpage_lang_urls_arr[0]['filter_url'].'/page-'.$_GET['page'].'/');
			define('_CURRPAGE_LANG'.$currpage_lang_urls_arr[1]['language_id'].'_URL_', _NEW_ROUTE_.$currpage_lang_urls_arr[1]['filter_group_url'].'_'.$currpage_lang_urls_arr[1]['filter_url'].'/page-'.$_GET['page'].'/');
		}else{
			define('_CURRPAGE_LANG'.$currpage_lang_urls_arr[0]['language_id'].'_URL_', _NEW_ROUTE_.$currpage_lang_urls_arr[0]['filter_group_url'].'_'.$currpage_lang_urls_arr[0]['filter_url'].'/');
			define('_CURRPAGE_LANG'.$currpage_lang_urls_arr[1]['language_id'].'_URL_', _NEW_ROUTE_.$currpage_lang_urls_arr[1]['filter_group_url'].'_'.$currpage_lang_urls_arr[1]['filter_url'].'/');
		}
}


// Start Web-promo Tkach
	if( defined('_FILTER_LANGUAGE_') )
	{
	    $tmp_url=explode('?',$_SERVER['REQUEST_URI']);
		//$sstrtok = strtok ($newRoute ,'?');
		$ssubstr = substr($tmp_url[0], -1);
		if($ssubstr != '/' and empty($request->get['page']))
		{
		    $Location=$tmp_url[0]."/";
		    if(isset($tmp_url[1]))
                $Location.='?'.$tmp_url[1];
			header("HTTP/1.1 301 Moved Permanently");
			header('Location: '.$Location );
			exit();
		}

	}
// End Web-promo

// end web-promo

// Dispatch
$controller->dispatch($action, new Action('error/not_found'));

// Output
$response->output();

