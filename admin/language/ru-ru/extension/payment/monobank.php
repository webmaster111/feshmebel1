<?php
// Heading
$_['heading_title']					= 'Покупка частями (Monobank)';

// Text
$_['text_extension']	            = 'Платежи';
$_['text_success']		 			= 'Настройки модуля успешно обновлены!';
$_['text_edit']          			= 'Редактирование модуля';
$_['text_monobank']		            = '<img src="/image/payment/mono-logo-check.png" alt="Monobank" title="Monobank" style="width:25%;border: 1px solid #EEEEEE;" />';

$_['text_store_identificator']	    = 'Идентификатор предприятия';
$_['text_store_identificator_placeholder']	    = 'Идентификатор предприятия (выданый при заключении договора)';
$_['text_secret_key']	            = 'Секретный ключ';
$_['text_host']	                    = 'Хост';
$_['text_host_placeholder']	        = 'u2-demo.ftband.com';
$_['text_parts']	                = 'Кол-во частей оплаты';
$_['text_parts_placeholder']	    = 'Кол-во частей оплаты, варианты через запятую';
$_['text_payment_text']	            = 'Текст сообщения при процессе оплаты';

// Entry
$_['entry_total']					= 'Минимальная сумма:';
$_['entry_order_status'] 			= 'Статус заказа после оплаты:';
$_['entry_monobank_order_status_confirm_id'] 			= 'Статус заказа подтверждения заявки:';
$_['entry_monobank_order_status_cancel_id'] 			= 'Статус заказа отмены заявки:';
$_['entry_monobank_order_status_return_id'] 			= 'Статус заказа возврата товара по заявке:';
$_['entry_geo_zone']    			= 'Географическая зона:';
$_['entry_status']       			= 'Статус:';
$_['entry_sort_order']   			= 'Порядок сортировки:';

// Help
$_['help_total']					= 'Ниже этой суммы метод будет недоступен.';

// Error
$_['error_permission']				= 'У Вас нет прав для управления этим модулем!';
$_['error_monobank_parts']			= 'Минимальное кол-во частей 3. То есть 3,4,5...';