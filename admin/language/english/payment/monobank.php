<?php
// Heading
$_['heading_title']					= 'Purchase in parts (Monobank)';

// Text
$_['text_extension']	            = 'Payments';
$_['text_success']		 			= 'Module settings successfully updated!';
$_['text_edit']          			= 'Editing module';
$_['text_monobank']		            = '<img src="/image/payment/mono-logo-check.png" alt="Monobank" title="Monobank" style="width:25%;border: 1px solid #EEEEEE;" />';

$_['text_store_identificator']	    = 'Store ID';
$_['text_store_identificator_placeholder']	    = 'Store ID (issued at the conclusion of the contract)';
$_['text_secret_key']	            = 'The secret key';
$_['text_host']	                    = 'Host';
$_['text_host_placeholder']	        = 'u2-demo.ftband.com';
$_['text_parts']	                = 'Number of payment units';
$_['text_parts_placeholder']	    = 'Number of parts of payment, options separated by commas';
$_['text_payment_text']	            = 'Message text during the payment process';

// Entry
$_['entry_total']					= 'Minimum amount:';
$_['entry_order_status'] 			= 'Order status after payment:';
$_['entry_monobank_order_status_confirm_id'] 			= 'Order confirmation order status:';
$_['entry_monobank_order_status_cancel_id'] 			= 'Order Cancellation Status:';
$_['entry_monobank_order_status_return_id'] 			= 'Order return status on request:';
$_['entry_geo_zone']    			= 'Geographical area:';
$_['entry_status']       			= 'Status:';
$_['entry_sort_order']   			= 'The sort order:';

// Help
$_['help_total']					= 'Below this amount, the method will be unavailable..';

// Error
$_['error_permission']				= 'You are not authorized to manage this module!';
$_['error_monobank_parts']			= 'The minimum number of parts is 3. That is 3,4,5 ...';