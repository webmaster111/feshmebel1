<?php
// Heading
$_['heading_title']							= 'Новая Почта API';
$_['heading_tariff']						= 'Настройка тарифов';
$_['heading_options_seat']					= 'Параметры каждого места отправления';

//Button
$_['button_save_and_stay']					= 'Сохранить и остаться';
$_['button_tariff']							= 'Настроить тариф';
$_['button_generate']						= 'Генерировать';

$_['button_create_ei']						= 'Создать &laquo;ЭН&raquo; для &laquo;Новой Почты&raquo;';
$_['button_edit_ei']						= 'Редактировать &laquo;ЭН&raquo; &laquo;Новой Почты&raquo;';
$_['button_save_ei']						= 'Сохранить экспресс накладную';
$_['button_ei_list']						= 'Список &laquo;ЭН&raquo;';
$_['button_pdf']							= 'Скачать в формате PDF';
$_['button_print']							= 'Печать';
$_['button_ei']								= '&laquo;ЭН&raquo;';
$_['button_mark']							= 'Маркировки';
$_['button_mark_zebra']						= 'Маркировки &laquo;Зебра&raquo;';
$_['button_edit']							= 'Редактировать';
$_['button_delete']							= 'Удалить';
$_['button_back_to_orders']					= 'К заказам';
$_['button_options_seat']					= 'Параметры каждого места отправления';
$_['button_add_seat']						= 'Добавить место отправления';

// Tab
$_['tab_general']							= 'Основные настройки';
$_['tab_database']							= 'База данных';
$_['tab_sender']							= 'Отправитель';
$_['tab_recipient']							= 'Получатель';
$_['tab_departure']							= 'Отправление';
$_['tab_cron']								= 'Задания Cron';
$_['tab_support']							= 'Поддержка';

// Column
$_['column_type']           				= 'Тип данных';
$_['column_date']							= 'Последнее обновление';
$_['column_amount']							= 'Количество';
$_['column_description']   					= 'Описание';
$_['column_action']         				= 'Действие';

$_['column_weight']           				= 'Вес';
$_['column_service_type']           		= 'Тип услуги';
$_['column_warehouse_warehouse']        	= 'Отделение-Отделение';
$_['column_warehouse_doors']        		= 'Отделение-Адрес';
$_['column_doors_warehouse']        		= 'Адрес-Отделение';
$_['column_doors_doors']        			= 'Адрес-Адрес';

$_['column_novaposhta_status']         	 	= 'Статус Новой Почты';
$_['column_store_status']           		= 'Статус магазина';
$_['column_notification']        			= 'Уведомление';
$_['column_message']        				= 'Сообщение';

$_['column_ei_number']						= '№ &laquo;ЭН&raquo;';
$_['column_order_number']					= '№ заказа';
$_['column_estimated_delivery_date']		= 'Дата доставки';
$_['column_recipient']          			= 'Получатель';
$_['column_address']         				= 'Адрес';
$_['column_announced_price']         		= 'Объявленная стоимость';
$_['column_shipping_cost']         			= 'Стоимость доставки';
$_['column_state']          				= 'Статус';

$_['column_number_order']          			= '№ п/п';
$_['column_volume']          				= 'Объем';
$_['column_width']          				= 'Ширина';
$_['column_length']         				= 'Длина';
$_['column_height']          				= 'Высота';
$_['column_actual_weight']         			= 'Фактический вес';
$_['column_volume_weight']         			= 'Объемный вес';

// Entry
$_['entry_status']     						= 'Статус';
$_['entry_sort_order'] 						= 'Порядок сортировки';
$_['entry_courier_shipping'] 				= 'Доставка курьером';
$_['entry_shipping_warehouse_name']     	= 'Название для доставки в отделение';
$_['entry_shipping_doors_name']     		= 'Название для доставки на адрес';
$_['entry_geo_zone']   						= 'Географическая зона';
$_['entry_tax_class']  						= 'Налоговый класс';
$_['entry_key_api'] 						= 'Ключ API';
$_['entry_cost'] 							= 'Стоимость';
$_['entry_api_calculation'] 				= 'API расчет';
$_['entry_tariff_calculation'] 				= 'Тарифный расчет';
$_['entry_discount'] 						= 'Скидка';
$_['entry_additional_commission'] 			= 'Дополнительная комиссия';
$_['entry_additional_commission_bottom']	= 'Нижняя граница дополнительной комиссии';
$_['entry_free_shipping'] 					= 'Бесплатная доставка от';
$_['entry_delivery_period'] 				= 'Срок доставки';
$_['entry_warehouses_filter_weight']		= 'Фильтр отделений по весу';
$_['entry_warehouse_types'] 				= 'Тип отделений';
$_['entry_payment_cod'] 					= 'Метод оплаты наложенным платежом';
$_['entry_update_areas'] 					= 'Области';
$_['entry_update_cities'] 					= 'Города';
$_['entry_update_warehouses']				= 'Отделения';
$_['entry_update_references']				= 'Справочники';
$_['entry_sender']              			= 'Отправитель';
$_['entry_recipient']             			= 'Получатель';
$_['entry_contact_person'] 					= 'Контактное лицо';
$_['entry_phone']              				= 'Телефон';
$_['entry_city']              				= 'Город';
$_['entry_address'] 						= 'Адрес';
$_['entry_cargo_description'] 				= 'Описание отправления';
$_['entry_additional_information'] 			= 'Дополнительная информация об отправлении';
$_['entry_weight'] 							= 'Вес';
$_['entry_dimensions'] 						= 'Размеры (Ш x Д x В)';
$_['entry_calculate_volume'] 				= 'Учёт объема';
$_['entry_use_parameters']					= 'Применение параметров';
$_['entry_key_cron'] 						= 'Cron ключ';
$_['entry_shipments_tracking'] 				= 'Отслеживание отправлений';
$_['entry_tracking_statuses'] 				= 'Статусы для отслеживания';
$_['entry_admin_notification'] 				= 'Для администратора';
$_['entry_customer_notification'] 			= 'Для покупателя';

$_['entry_ei_number'] 						= 'Номер &laquo;ЭН&raquo';
$_['entry_third_person']             		= 'Третье лицо';
$_['entry_cargo_type']              		= 'Тип груза';
$_['entry_width']              				= 'Ширина';
$_['entry_length']              			= 'Длина';
$_['entry_height']              			= 'Высота';
$_['entry_volume_weight']              		= 'Объёмный вес (Объём x 250)';
$_['entry_volume_general']             		= 'Общий объем отправления';
$_['entry_seats_amount']              		= 'Количество мест';
$_['entry_announced_price']            	 	= 'Объявленная стоимость';
$_['entry_payer']              				= 'Плательщик';
$_['entry_payment_type']              		= 'Форма оплаты';
$_['entry_backward_delivery']           	= 'Обратная доставка';
$_['entry_backward_delivery_total']			= 'Сумма обратной доставки';
$_['entry_backward_delivery_payer']			= 'Плательщик обратной доставки';
$_['entry_shipment_date']					= 'Дата отправки';
$_['entry_sales_order_number'] 				= 'Внутренний номер заказа клиента';
$_['entry_payment_control'] 				= 'Контроль оплаты';

// Help
$_['help_status'] 							= 'Включить/выключить модуль';
$_['help_sort_order'] 						= 'Порядок сортировки модуля';
$_['help_courier_shipping'] 				= 'Включить/выключить доставку курьером на адрес';
$_['help_shipping_warehouse_name']     		= 'Данное название будет отображаться покупателю во время оформления заказа для  доставки в отделение или почтомат';
$_['help_shipping_doors_name']     			= 'Данное название будет отображаться покупателю во время оформления заказа для  доставки курьером на адрес';
$_['help_geo_zone'] 						= 'Выберите географическую зону, для которой будет доступен этот способ доставки';
$_['help_tax_class'] 						= 'Выберите налоговый класс';
$_['help_key_api'] 							= 'Вставьте значение ключа API, который Вы сможете найти на сайте компании &laquo;Новая Почта&raquo; в собственном кабинете. Смотрите раздел Настройки &rarr; API 2.0';
$_['help_cost'] 							= 'Рассчитывать стоимость доставки?';
$_['help_api_calculation'] 					= 'Расчет стоимости через API &laquo;Новой Почты&raquo; - дает самые точные и актуальные данные о стоимости доставки';
$_['help_tariff_calculation'] 				= 'Если расчет через API &laquo;Новой Почты&raquo; выключен, то будет использоваться только тарифный расчет. Если расчет через API включен - будет использоваться расчет стоимости доставки согласно тарифам только в случае недоступности API';
$_['help_discount'] 						= 'Укажите скидку на доставку. Если у Вас нет скидки оставьте поле пустым';
$_['help_additional_commission'] 			= 'Укажите размер дополнительной комиссии, которая рассчитывается как процент от объявленной стоимости отправления';
$_['help_additional_commission_bottom']		= 'Укажите минимальную объявленную стоимость отправления начиная с которой будет рассчитываться дополнительная комиссия';
$_['help_free_shipping'] 					= 'Укажите минимальную сумму заказа для бесплатной доставки';
$_['help_delivery_period']	 				= 'Рассчитывать срок доставки?';
$_['help_warehouses_filter_weight']			= 'Если включить фильтр, то для клиента список доступных отделений будет формироваться в соответствии с общим весом товаров в корзине и максимально допустимым весом для отделений';
$_['help_warehouse_types']	 				= 'Выбранный тип отделений будет доступный для клиентов во время оформления заказа. Если не выбран ни один тип, то будут доступны все отделения';
$_['help_payment_cod'] 						= 'Укажите способ оплаты который соответствует наложенному платежу для Новой Почты';
$_['help_update_areas'] 					= 'Будет выполнено обновление областей компании &laquo;Новая Почта&raquo;. Действие не повлияет на стандартные регионы';
$_['help_update_cities'] 					= 'Будет выполнено обновление городов и населенных пунктов в которые возможна доставка компанией &laquo;Новая Почта&raquo;';
$_['help_update_warehouses']				= 'Будет выполнено обновление отделений компании &laquo;Новая Почта&raquo;';
$_['help_update_references']				= 'Будет выполнено обновление списка отправителей, контактных лиц, справочной и другой информации компании &laquo;Новая Почта&raquo; необходимой для работы дополнения. Рекомендуемая частота обновлений не реже 1-го раза в месяц';
$_['help_sender'] 							= 'Выберите отправителя';
$_['help_sender_contact_person'] 			= 'Выберите контактное лицо';
$_['help_sender_city']              		= 'Выберите город с которого будет осуществляться отправление заказа';
$_['help_sender_address'] 					= 'Выберите адрес с которого будет осуществляться отправление заказа';
$_['help_recipient'] 						= 'Выберите получателя по умолчанию или укажите переменные заказа которые содержат название. Пример: Приватна особа';
$_['help_recipient_contact_person'] 		= 'Укажите переменные заказа, которые содержат Ф.И.О контактного лица. Пример: &laquo;{shipping_lastname} {shipping_firstname}&raquo;';
$_['help_recipient_contact_person_phone']	= 'Укажите переменную заказа, которая содержит номер телефона контактного лица. Пример: &laquo;{telephone}&raquo;';
$_['help_recipient_city']              		= 'Укажите переменную заказа, которая содержит город получателя. Пример: &laquo;{shipping_city}&raquo;';
$_['help_recipient_address'] 				= 'Укажите переменную заказа, которая содержит адрес получателя. Пример: &laquo;{shipping_address_1}&raquo;';
$_['help_cargo_description'] 				= 'Используется как описание товара по умолчанию при создании &laquo;ЭН&raquo;, удобно если в магазине много товаров, которые имеют одинаковое описание';
$_['help_additional_information'] 			= 'Используется как шаблон для поля дополнительной информации об отправлении при создании &laquo;ЭН&raquo;. Возможно применение переменных. При использовании переменных товара разделяйте текст на два блока при помощи символа &laquo;|&raquo; (переменные товара используйте во втором блоке). Максимальное количество символов - 100';
$_['help_weight'] 							= 'Укажите фактический вес по умолчанию. Будет использоваться в расчетах, если в карточке товара не указан вес';
$_['help_dimensions'] 						= 'Укажите габаритные размеры по умолчанию. Будут использоваться в расчетах, если в карточке товара не указаны размеры';
$_['help_calculate_volume'] 				= 'Учитывать объёмный вес при расчете предварительной стоимости доставки? Используйте данную опцию осторожно, так как модуль считает объем отправления как сумму объемов всех товаров в корзине, что не всегда правильно и стоимость доставки может значительно превышать реальные значения';
$_['help_use_parameters']					= 'Укажите способ применения параметров по умолчанию (вес и габариты). К каждому товару в корзине или как общие параметры всего заказа? Если ко всему заказу, то применение будет работать даже если параметры в карточке товара указаны';
$_['help_key_cron'] 						= 'Задайте или сгенерируйте ключ';
$_['help_tracking_statuses'] 				= 'Выберите статусы заказов для которых будет осуществляться отслеживание';
$_['help_cron'] 							= 'Задайте ключ и сохраните настройки модуля, далее скопируйте требуемую команду и добавьте её в крон в Вашей панеле управления хостингом. Оптимальное частота запуска:<br/>
<ul>
	<li>города и отделения - 1 раз в сутки</li>
	<li>области и справочники - 1 раз в месяц</li>
	<li>отслеживание посылок - по необходимости</li>
</ul>';
$_['help_order_template_vars'] 				= '
<strong>Переменные заказа:</strong><br/>
<code>{order_id}</code> - номер заказа<br/>
<code>{invoice}</code> - номер счета<br/>
<code>{store_name}</code> - название магазина<br/>
<code>{store_url}</code> - адрес магазина<br/>
<code>{name}</code> - ФИО покупателя<br/>
<code>{shipping_name}</code> - ФИО получателя<br/>
<code>{date_added}</code> - дата оформления заказа<br/>
<code>{date_modified}</code> - дата последнего изменения заказа';
$_['help_products_template_vars'] 			= '<strong>Переменные товара:</strong><br/>
<code>{product_name}</code> - название<br/>
<code>{model}</code> - модель<br/>
<code>{sku}</code> - артикул<br/>
<code>{quantity}</code> - количество каждого товара';
$_['help_ei_template_vars'] 				= '<strong>Переменные Экспресс Накладной:</strong><br/>
<code>{Number}</code> - номер ЭН<br/>
<code>{Redelivery}</code> - обратная доставка<br/>
<code>{RedeliverySum}</code> - сумма за обратную доставку<br/>
<code>{RedeliveryNum}</code> - номер ЭН обратной доставки<br/>
<code>{RedeliveryPayer}</code> - плательщик за обратную доставку<br/>
<code>{OwnerDocumentType}</code> - создан на основе<br/>
<code>{LastCreatedOnTheBasisDocumentType}</code> - последние изменения, тип документа<br/>
<code>{LastCreatedOnTheBasisPayerType}</code> - последние изменения, тип плательщика<br/>
<code>{LastCreatedOnTheBasisDateTime}</code> - последние изменения, дата создания<br/>
<code>{LastTransactionStatusGM}</code> - дата последнего статуса<br/>
<code>{LastTransactionDateTimeGM}</code> - дата последнего передвижения<br/>
<code>{DateCreated}</code> - дата создания<br/>
<code>{DocumentWeight}</code> - вес который указан в интернет документе<br/>
<code>{CheckWeight}</code> - вес после контрольного взвешивания<br/>
<code>{DocumentCost}</code> - стоимость за доставку<br/>
<code>{SumBeforeCheckWeight}</code> - сумма после контрольного взвешивания<br/>
<code>{PayerType}</code> - тип плательщика<br/>
<code>{RecipientFullName}</code> - полное имя получателя<br/>
<code>{RecipientDateTime}</code> - дата и время получения отправления<br/>
<code>{ScheduledDeliveryDate}</code> - расчетная дата доставки<br/>
<code>{PaymentMethod}</code> - тип оплаты<br/>
<code>{CargoDescriptionString}</code> - описание отправления<br/>
<code>{CargoType}</code> - тип груза<br/>
<code>{CitySender}</code> - город отправителя<br/>
<code>{CityRecipient}</code> - город получателя<br/>
<code>{WarehouseRecipient}</code> - отделение получателя<br/>
<code>{CounterpartyType}</code> - тип контрагента<br/>
<code>{AfterpaymentOnGoodsCost}</code> - сумма обратной доставки Ц1П<br/>
<code>{ServiceType}</code> - тип доставки<br/>
<code>{UndeliveryReasonsSubtypeDescription}</code> - причины не доставки отправления<br/>
<code>{WarehouseRecipientNumber}</code> - номер отделения получателя<br/>
<code>{LastCreatedOnTheBasisNumber}</code> - последние изменения, номер ЭН<br/>
<code>{Status}</code> - статус ЭН<br/>
<code>{StatusCode}</code> - номер статуса трекинга<br/>';

// Text
$_['text_shipping']    						= 'Доставка';
$_['text_success']     						= 'Настройки модуля доставки &laquo;Новая Почта&raquo; успешно сохранены';
$_['text_update'] 							= 'Обновить данные';
$_['text_update_success'] 					= 'Данные успешно обновлены';
$_['text_table_tariffs'] 					= 'Таблица тарифов';
$_['text_each_goods'] 						= 'Применить к каждому товару';
$_['text_all_goods'] 						= 'Применить ко всему заказу';
$_['text_base_update']						= 'Обновление базы данных';
$_['text_shipments_tracking']				= 'Отслеживание отправлений';
$_['text_settings_shipments_statuses']	 	= 'Настройки статусов отправления';
$_['text_message_template_variables']	 	= 'Переменные для шаблона сообщения';
$_['text_order']							= 'Заказ';
$_['text_orders']							= 'Все заказы';
$_['text_form_create']						= 'Создание экспресс накладной';
$_['text_form_edit']						= 'Редактирование экспресс накладной';
$_['text_list']								= 'Список экспресс накладных';
$_['text_sender']							= 'Отправитель';
$_['text_recipient']						= 'Получатель';
$_['text_shipment']							= 'Параметры отправления';
$_['text_payment']							= 'Оплата';
$_['text_additionally']						= 'Дополнительно';
$_['text_no_backward_delivery']				= 'Нет обратной доставки';
$_['text_ei_success_save']					= 'Экспресс накладная успешно сохранена';
$_['text_ei_success_delete']				= 'Удаление прошло успешно';
$_['text_or']								= 'или';
$_['text_grn']								= 'грн';
$_['text_cm']								= 'см';
$_['text_kg']								= 'кг';
$_['text_pct']								= '%';
$_['text_cubic_meter']						= 'м&sup3;';
$_['text_pc']								= 'шт';
$_['text_confirm']							= 'Вы уверены?';

// Error
$_['error_permission']						= 'Ошибка: у Вас отсутствуют права на изменение настроек!';
$_['error_update']							= 'Ошибка обновления данных!';
$_['error_empty'] 							= 'Поле обязательно к заполнению!';

$_['error_get_order']						= 'Заказ не найден в базе';
$_['error_get_ei']							= 'Ошибка загрузки экспресс накладной';
$_['error_ei_save'] 						= 'Не удалось сохранить экспресс накладную';
$_['error_ei_delete'] 						= 'Удаление не удалось';
$_['error_sender'] 							= 'Отправитель не найден в базе';
$_['error_sender_address'] 					= 'Адрес отправителя не найден в базе';
$_['error_sender_contact_person'] 			= 'Контактное лицо для данного отправителя не найдено в базе';
$_['error_recipient_address_city'] 			= 'Адрес для данного города не найден в базе данных по описанию. Проверьте название города';	
$_['error_recipient_address']	 			= 'Адрес для данного города не найден в базе данных по описанию. Выберите нужный адрес из списка';
$_['error_third_person'] 					= 'Третье лицо не найдено в базе';
$_['error_full_name_correct'] 				= 'Проверьте правильность написания фамилии, имени и отчества. Пример: Шевченко Тарас Григорьевич';
$_['error_characters'] 						= 'Запрещённые символы';
$_['error_city'] 							= 'Город не найден в базе данных по описанию';
$_['error_phone'] 							= 'Неверный формат номера телефона. Пример: 380501234567';
$_['error_width']	              			= 'Ширина должна быть целым числом не больше 35 см';
$_['error_length']  	            		= 'Длина должна быть целым числом не больше 61 см';
$_['error_height']      	        		= 'Высота должна быть целым числом не больше 37 см';
$_['error_weight'] 							= 'Вес должен быть дробным или целым числом больше 0. Корректные примеры: 7, 1.002 и 0,024';	
$_['error_volume'] 							= 'Объем должен быть дробным или целым числом больше 0. Корректные примеры: 7, 1.002 и 0,024';	
$_['error_seats_amount'] 					= 'Количество мест должно быть целым числом больше 0';
$_['error_announced_price'] 				= 'Объявленная стоимость должна быть дробным или целым числом больше 0. Корректные примеры: 700, 100.5 и 77,25';
$_['error_cargo_description']	 			= 'Описание отправления должно состоять не менее чем из 3-х символов и иметь смысл';
$_['error_backward_delivery_total']		 	= 'Сумма обратной доставки должна быть дробным или целым числом больше 0. Корректные примеры: 77, 100.7 и 34,25';
$_['error_date']							= 'Неверный формат даты. Корректный пример: 24.08.1991';
$_['error_date_past']						= 'Дата отправки не может быть в прошлом';
$_['error_additional_information']			= 'Дополнительная информация об отправлении не может быть больше 100 символов';