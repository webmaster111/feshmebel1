<?php
// Heading
$_['heading_title']      = 'Поставщики';

// Text
$_['text_success']       = 'Настройки успешно изменены!';
$_['text_list']          = 'Поставщики';
$_['text_add']          = 'Добавить';
$_['text_edit']         = 'Редактирование';
$_['text_percent']       = 'Процент';
$_['text_amount']        = 'Фиксированная сумма';

// Column
$_['column_name']        = 'Поставщики';
$_['column_coefficient'] = 'Коэффициент (к курсу)';
$_['column_sort_order']  = 'Порядок сортировки';
$_['column_action']      = 'Действие';

// Entry
$_['entry_name']         = 'Поставщики';
$_['entry_coefficient']  = 'Коэффициент (к курсу)';
$_['entry_sort_order']   = 'Порядок сортировки';

// Error
$_['error_permission']   = 'У Вас нет прав для изменения производителей!';
$_['error_name']         = 'Название поставщика должно быть от 3 до 64 символов!';
$_['error_coefficient']  = 'Коэффициент (к курсу)';
$_['error_product']      = 'Внимание: Данного поставщика нельзя удалить так как назначен %s товарам!!';

