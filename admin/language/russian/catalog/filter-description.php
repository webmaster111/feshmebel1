<?php
// Heading
$_['heading_title']			= 'Описание фильтров';

// Text
$_['column_name']			= 'Название';
$_['column_url']			= 'Ссилка на сторинку фильтра';
$_['column_action']			= 'Действие';


$_['text_add']				= 'Добавить';
$_['text_edit']				= 'Редактирование';
$_['tab_general']			= 'Основное';
$_['text_list']				= 'Список фильтров';
$_['text_success']			= 'Настройки успешно изменены!';

// Entry
$_['entry_name']             = 'Название фильтра';
$_['entry_url']              = 'URL фільтра';
$_['entry_description']      = 'Описание';
$_['entry_meta_title'] 	     = 'Мета-тег Title';
$_['entry_meta_keyword'] 	 = 'Мета-тег Keyword';
$_['entry_meta_description'] = 'Мета-тег Description';
$_['entry_showmeta']         = 'Выводить мета-теги';



// Help
$_['help_keyword']           = 'Должно быть уникальным на всю систему и без пробелов.';

// Error
$_['error_warning']          = 'Внимательно проверьте форму на ошибки!';
$_['error_permission']       = 'У Вас нет прав для изменения фільтров!';
$_['error_name']             = 'Название товара должно содержать от 3 до 255 символов!';
$_['error_meta_title']       = 'Мета-тег Title должен содержать от 3 до 255 символов!';
$_['error_model']            = 'Модель товара должна содержать от 3 до 64 символов!';
$_['error_keyword']			 = 'SEO URL занят!';
