<?php
// Heading
$_['heading_title']      = 'Партнеры';

// Text
$_['text_success']       = 'Настройки успешно изменены!';
$_['text_list']          = 'Партнеры';
$_['text_add']          = 'Добавить';
$_['text_edit']         = 'Редактирование';
$_['text_default']       = 'Основной магазин';
$_['text_percent']       = 'Процент';
$_['text_amount']        = 'Фиксированная сумма';

// Column
$_['column_name']        = 'Партнер';
$_['column_sort_order']  = 'Порядок сортировки';
$_['column_action']      = 'Действие';
$_['column_image']      = 'Изображение';

// Entry
$_['entry_name']         = 'Партнер';
$_['entry_store']        = 'Магазины';
$_['entry_keyword']      = 'SEO URL';
$_['entry_image']        = 'Изображение';
$_['entry_sort_order']   = 'Порядок сортировки';
$_['entry_type']         = 'Тип';

// Help
$_['help_keyword']       = 'Должно быть уникальным на всю систему и без пробелов';

// Error
$_['error_permission']   = 'У Вас нет прав для изменения производителей!';
$_['error_name']         = 'Название производителя должно быть от 3 до 64 символов!';
$_['error_keyword']			 = 'SEO URL занят!';
$_['error_product']      = 'Внимание: Данного производителя нельзя удалить так как назначен %s товарам!!';

