<?php
// Heading
$_['heading_title']          = 'Страницы для индексирования';

// Text
$_['text_success']           = 'Страницу успешно изменены!';
$_['text_list']              = 'Список страниц';
$_['text_add']               = 'Добавить страницу';
$_['text_edit']              = 'Редактирование страницы';
$_['text_default']           = 'По умолчанию';

// Column
$_['column_name']            = 'Название страницы';
$_['column_image']           = 'Изображение';
$_['column_status']          = 'Статус';
$_['column_action']          = 'Действие';

// Entry
$_['entry_name']             = 'Название страницы';
$_['entry_description']      = 'Текст';
$_['entry_description_short']= 'Краткий текст';
$_['entry_meta_title'] 	     = 'Мета-тэг "Title"';
$_['entry_meta_keyword']     = 'Мета-тэг "Keywords"';
$_['entry_meta_description'] = 'Мета-тэг "Description"';
$_['entry_keyword']          = 'SEO URL';
$_['entry_store']            = 'Магазины';
$_['entry_image']            = 'Изображение';
$_['entry_youtube_video']    = 'YouTube видео';
$_['entry_text']             = 'Текст';
$_['entry_required']         = 'Обязательно';
$_['entry_status']           = 'Статус';
$_['entry_sort_order']       = 'Порядок сортировки';
$_['entry_category']         = 'Категории';
$_['entry_main_category']    = 'Главная категория';
$_['entry_layout']           = 'Макет';
$_['entry_viewed']           = 'Просмотров';
$_['entry_related_products'] = 'Рекомендуемые товары';
$_['entry_product']          = 'Товары';

// Help
$_['help_keyword']           = 'Вместо пробелов используйте дефис \"-\". Должно быть уникально на всю систему.';
$_['help_category']          = '(Автоподстановка)';

// Error
$_['error_warning']          = 'Ошибка: Проверьте форму на наличие ошибок!';
$_['error_permission']       = 'Ошибка: Вы не имеете доступа к управлению страницами!';
$_['error_name']             = 'Название страницы должно быть от 3 до 255 символов!';
$_['error_meta_title']       = 'Мета-тэг "Title" должен быть от 3 до 255 символов!';
$_['error_keyword']          = 'Такой URL уже используется!';