<?php

// Heading
$_['heading_title']      = 'Оплата банковской картой Visa / MasterCard через ПАО «ПУМБ»';

// Text
$_['text_payment']	 			    = 'Оплата';
$_['text_success']					= 'Настройки модуля обновлены!';
$_['text_edit']                     = 'Изменить';
$_['text_pumb']        				= '<a href="http://pumb.ua/ru/" target="_blank"><img src="view/image/payment/pumb.png" alt="ПУМБ" title="ПУМБ" /></a>';
$_['text_live']						= 'Live';
$_['text_demo']						= 'Demo';
$_['text_merchant_console']			= 'Merchant Console';

// Entry
$_['entry_merch_id']				= 'Merchant ID';
$_['entry_account_id']				= 'Account ID';
$_['entry_total']					= 'Нижняя граница';
$_['entry_order_status']			= 'Статус заказа после оплаты';
$_['entry_order_status_success']	= 'Статус заказа после успешной оплаты';
$_['entry_order_status_failed']		= 'Статус заказа после неудачной оплаты';
$_['entry_geo_zone']				= 'Географическая зона';
$_['entry_status']					= 'Статус';
$_['entry_sort_order']				= 'Порядок сортировки';
$_['entry_live_url']				= 'Live connection URL';
$_['entry_demo_url']				= 'Demo connection URL';
$_['entry_live_demo']				= 'Live / Demo';
$_['entry_signature_live']			= 'Signature';
$_['entry_signature_demo']			= 'Signature';

// Tab
$_['tab_account']					 = 'API info';
$_['tab_advanced']					 = 'Advanced';

// Help
$_['help_total']		 = 'Минимальная сумма заказа. Ниже данной суммы, способ оплаты будет недоступен';

// Error
$_['error_permission']				= 'У Вас нет прав для управления этим модулем!';

?>