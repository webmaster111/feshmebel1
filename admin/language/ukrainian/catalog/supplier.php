<?php

//version 2.0.0.0
//Made by Sirchyk for www.marketplus.if.ua on 16 of october 2014.
//info@marketplus.if.ua

// Heading
$_['heading_title']      = 'Постачальники';

// Text
$_['text_success']       = 'Ви успішно змінили постачальників!';
$_['text_list']          = 'Список постачальників';
$_['text_add']           = 'Додати постачальника';
$_['text_edit']          = 'Редагувати постачальника';
$_['text_default']       = 'За замовчуванням';

// Column
$_['column_name']        = 'Назва постачальника';
$_['column_coefficient'] = 'Коeффіціент (до курсу)';
$_['column_sort_order']  = 'Порядок сортування';
$_['column_action']      = 'Дія';

// Entry
$_['entry_name']         = 'Назва постачальника';
$_['entry_coefficient'] = 'Коeффіціент (до курсу)';
$_['entry_sort_order']   = 'Порядок сортування';


// Error
$_['error_permission']   = 'У вас немає доступу для редагування виробників!';
$_['error_name']         = 'Назва постачальника повинна містити від 2 до 64 символів!';
$_['error_coefficient']  = 'Коeффіціент (до курсу)';
$_['error_product']      = 'Цей постачальник не може бути видалений, оскільки він використовується в %s товарах!';