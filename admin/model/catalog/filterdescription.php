<?php
class ModelCatalogFilterDescription extends Model {
	public function addFilterDescription($data) {
	
		$this->db->query("INSERT INTO " . DB_PREFIX . "filter_url SET date_added = NOW()");
	
		$filter_id = $this->db->getLastId();

		foreach ($data["fDescriptions"] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "filter_url_description SET filter_id = " . (int)$filter_id . ", language_id = " . (int)$language_id . ", name = '" . str_replace("'", "\'", $value['name']) ."', filter_url = '" . $value['filter_url'] ."', description = '" . str_replace("'", "\'", $value['description']) ."', meta_title = '" . str_replace("'", "\'", $value['meta_title']) ."', meta_h1 = '". str_replace("'", "\'", $value['meta_h1']) ."', meta_description = '" . str_replace("'", "\'", $value['meta_description']) ."', showmeta = " . (int)$value['showmeta']);

		}
	
	}
	
	public function editFilterDescription($filter_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "filter_url SET date_modified = NOW() WHERE filter_id = '" . (int)$filter_id . "'");
		
		foreach ($data["fDescriptions"] as $language_id => $value) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "filter_url_description WHERE language_id = " . (int)$language_id . " AND filter_id = '" . (int)$filter_id . "'");
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "filter_url_description SET filter_id = " . (int)$filter_id . ", language_id = " . (int)$language_id . ", name = '" . str_replace("'", "\'", $value['name']) ."', filter_url = '" . $value['filter_url'] ."', description = '" . str_replace("'", "\'", $value['description']) ."', meta_title = '" . str_replace("'", "\'", $value['meta_title']) ."', meta_h1 = '". str_replace("'", "\'", $value['meta_h1']) ."', meta_description = '" . str_replace("'", "\'", $value['meta_description']) ."', showmeta = " . (int)$value['showmeta']);
		}
	
	}
	
	public function getTotalFilterDescription($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "filter_url_description` WHERE `language_id` = '" . (int)$this->config->get('config_language_id') . "'";

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getFilterDescription($filter_id) {
		$filter_description_data = array();

		$sql = "SELECT * FROM `" . DB_PREFIX . "filter_url_description` WHERE `filter_id` = " . (int)$filter_id . "";

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			
			$filter_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'filter_id'        => $result['filter_id'],
				'filter_url'       => $result['filter_url'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
                'meta_h1'       => $result['meta_h1'],
				'meta_description' => $result['meta_description'],
				'showmeta' => $result['showmeta']
			);
		}
		return $filter_description_data;
	}
		
	public function deleteFilterDescription($filter_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "filter_url WHERE filter_id = '" . (int)$filter_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "filter_url_description WHERE filter_id = '" . (int)$filter_id . "'");

		$this->cache->delete('product');
	}	

}