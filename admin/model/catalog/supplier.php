<?php
class ModelCatalogSupplier extends Model {
	public function addSupplier($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "suppliers SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$supplier_id = $this->db->getLastId();

		if (isset($data['supplier_store'])) {
			foreach ($data['supplier_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "supplier_to_store SET supplier_id = '" . (int)$supplier_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->cache->delete('supplier');

		return $supplier_id;
	}

	public function editSupplier($supplier_id, $data) {
		$sql = "UPDATE " . DB_PREFIX . "suppliers SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order']."' ";
		if (isset($data['coefficient'])){
			$sql .= " , coefficient = '" . $data['coefficient']."'";
		}	
		$sql .= " WHERE supplier_id = '" . (int)$supplier_id . "'";
		
		$this->db->query($sql);

		$this->cache->delete('supplier');
	}

	public function deleteSupplier($supplier_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "suppliers WHERE supplier_id = '" . (int)$supplier_id . "'");

		$this->cache->delete('supplier');
	}

	public function getSupplier($supplier_id) {
		$query = $this->db->query("SELECT DISTINCT *  FROM " . DB_PREFIX . "suppliers WHERE supplier_id = '" . (int)$supplier_id . "'");

		return $query->row;
	}

	public function getSuppliers($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "suppliers";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalSuppliers() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "suppliers");

		return $query->row['total'];
	}
}
