<?php
class ModelCatalogSection extends Model {
	public function addSection($data) {
		$this->event->trigger('pre.admin.section.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "section SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "', category_id = '" . (int)$data['category_id'] . "'");

		$section_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "section SET image = '" . $this->db->escape($data['image']) . "' WHERE section_id = '" . (int)$section_id . "'");
		}

        foreach ($data['section_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "section_description SET section_id = '" . (int)$section_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', url = '" . $this->db->escape($value['url']) . "'");
        }
        
		if (isset($data['section_store'])) {
			foreach ($data['section_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "section_to_store SET section_id = '" . (int)$section_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'section_id=" . (int)$section_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('section');

		$this->event->trigger('post.admin.section.add', $section_id);

		return $section_id;
	}

	public function editSection($section_id, $data) {
		$this->event->trigger('pre.admin.section.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "section SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "', category_id = '" . (int)$data['category_id'] . "' WHERE section_id = '" . (int)$section_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "section SET image = '" . $this->db->escape($data['image']) . "' WHERE section_id = '" . (int)$section_id . "'");
		}


        $this->db->query("DELETE FROM " . DB_PREFIX . "section_description WHERE section_id = '" . (int)$section_id . "'");

        foreach ($data['section_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "section_description SET section_id = '" . (int)$section_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', url = '" . $this->db->escape($value['url']) . "'");
        }
        
		$this->db->query("DELETE FROM " . DB_PREFIX . "section_to_store WHERE section_id = '" . (int)$section_id . "'");

		if (isset($data['section_store'])) {
			foreach ($data['section_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "section_to_store SET section_id = '" . (int)$section_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'section_id=" . (int)$section_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'section_id=" . (int)$section_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('section');

		$this->event->trigger('post.admin.section.edit', $section_id);
	}

	public function deleteSection($section_id) {
		$this->event->trigger('pre.admin.section.delete', $section_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "section WHERE section_id = '" . (int)$section_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "section_to_store WHERE section_id = '" . (int)$section_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'section_id=" . (int)$section_id . "'");

		$this->cache->delete('section');

		$this->event->trigger('post.admin.section.delete', $section_id);
	}

	public function getSection($section_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'section_id=" . (int)$section_id . "') AS keyword FROM " . DB_PREFIX . "section WHERE section_id = '" . (int)$section_id . "'");

		return $query->row;
	}

	public function getSections($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "section";

		if (!empty($data['filter_name'])) {
			$sql .= " WHERE name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getSectionStores($section_id) {
		$section_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "section_to_store WHERE section_id = '" . (int)$section_id . "'");

		foreach ($query->rows as $result) {
			$section_store_data[] = $result['store_id'];
		}

		return $section_store_data;
	}

	public function getTotalSections() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "section");

		return $query->row['total'];
	}

    public function getSectionDescriptions($section_id) {
        $section_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "section_description WHERE section_id = '" . (int)$section_id . "'");

        foreach ($query->rows as $result) {
            $section_description_data[$result['language_id']] = array(
                'title'            => $result['title'],
                'url'            => $result['url'],

            );
        }

        return $section_description_data;
    }
    
}
