<?php
class ModelIndexIndex extends Model {
	public function addIndex($data) {

		$this->event->trigger('pre.admin.index.add', $data);

		$this->db->query("INSERT INTO ocp_index SET 
        url = '" . $data['url']  . "', 
        status = '" . (int)$data['status']. "', 
        sort_order = '" . (int)$data['sort_order'] . "', 
        step = '" . $data['step'] . "', 
        date_added = NOW()");

		$index_id = $this->db->getLastId();

		$this->cache->delete('index');

		$this->event->trigger('post.admin.index.add', $index_id);

		return $index_id;
	}

	public function editIndex($index_id, $data) {
		$this->event->trigger('pre.admin.index.edit', $data);

		$this->db->query("UPDATE ocp_index SET 
        url = '" . $data['url']  . "', 
        status = '" . (int)$data['status']. "', 
        sort_order = '" . (int)$data['sort_order'] . "', 
        date_modified = NOW(), 
        step = '" . (int)$data['step'] . "' WHERE index_id = '" . (int)$index_id . "'");

		$this->cache->delete('index');

		$this->event->trigger('post.admin.index.edit', $index_id);
	}

	public function deleteIndex($index_id) {
		$this->event->trigger('pre.admin.index.delete', $index_id);

		$this->db->query("DELETE FROM ocp_index WHERE index_id = '" . (int)$index_id . "'");

		$this->cache->delete('index');

		$this->event->trigger('post.admin.index.delete', $index_id);
	}

	public function getIndex($index_id) {
		$query = $this->db->query("SELECT * FROM ocp_index a WHERE a.index_id = '" . (int)$index_id . "'");

		return $query->row;
	}

	public function getIndexs($data = array()) {
        $sql = "SELECT * FROM ocp_index a ";

        if (!empty($data['filter_name'])) {
			$sql .= " AND a.url LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND a.status = '" . (int)$data['filter_status'] . "'";
		}
                

		
		$sql .= " GROUP BY a.index_id";

		$sort_data = array(
			'a.date_modified',
			'a.url',
			'a.status',
			'a.sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY a.date_modified";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalIndexs($data = array()) {

        $sql = "SELECT COUNT(DISTINCT a.index_id) AS total FROM ocp_index a ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND a.url LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND a.status = '" . (int)$data['filter_status'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}
