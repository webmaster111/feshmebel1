<?php
class ModelDesignBannerhome extends Model {
	public function addBanner($data) {
		$this->event->trigger('pre.admin.bannerhome.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "bannerhome SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "'");

		$banner_id = $this->db->getLastId();

		if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $banner_image) {
				$arrImgs = array();
				foreach ($banner_image['image0'] as $language_id => $banner_image2) {
					$arrImgs[$language_id] = array(
						'image0' => $banner_image['image0'][$language_id]['image'],
						'image1' => $banner_image['image1'][$language_id]['image'],
						'image2' => $banner_image['image2'][$language_id]['image']
					);
				}
				foreach ($arrImgs as $language_id => $banner_images) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "bannerhome_image SET language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', image1 = '" .  $this->db->escape($banner_images['image0']) . "', image2 = '" .  $this->db->escape($banner_images['image1']) . "', image3 = '" .  $this->db->escape($banner_images['image2']) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'");

					$banner_image_id = $this->db->getLastId();

					//foreach ($banner_image['bannerhome_image_description'] as $language_id => $banner_image_description) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "bannerhome_image_description SET banner_image_id = '" . (int)$banner_image_id . "', language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', title = '" .  $this->db->escape($banner_image['banner_image_description'][$language_id]['title']) . "'");
					//}
				}
			}
		}

		$this->event->trigger('post.admin.bannerhome.add', $banner_id);

		return $banner_id;
	}

	public function editBanner($banner_id, $data) {
		$this->event->trigger('pre.admin.bannerhome.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "bannerhome SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "' WHERE banner_id = '" . (int)$banner_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "bannerhome_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "bannerhome_image_description WHERE banner_id = '" . (int)$banner_id . "'");

		if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $banner_image) {
				$arrImgs = array();
				foreach ($banner_image['image0'] as $language_id => $banner_image2) {
					$arrImgs[$language_id] = array(
						'image0' => $banner_image['image0'][$language_id]['image'],
						'image1' => $banner_image['image1'][$language_id]['image'],
						'image2' => $banner_image['image2'][$language_id]['image']
					);
				}
				//print_r($data['banner_image']);die;
				foreach ($arrImgs as $language_id => $banner_images) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "bannerhome_image SET language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', image1 = '" .  $this->db->escape($banner_images['image0']) . "', image2 = '" .  $this->db->escape($banner_images['image1']) . "', image3 = '" .  $this->db->escape($banner_images['image2']) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'");

					$banner_image_id = $this->db->getLastId();

					//foreach ($banner_image['bannerhome_image_description'] as $language_id => $banner_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "bannerhome_image_description SET banner_image_id = '" . (int)$banner_image_id . "', language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', title = '" .  $this->db->escape($banner_image['banner_image_description'][$language_id]['title']) . "'");
					//}
				}
			}
		}

		$this->event->trigger('post.admin.bannerhome.edit', $banner_id);
	}

	public function deleteBanner($banner_id) {
		$this->event->trigger('pre.admin.bannerhome.delete', $banner_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "bannerhome WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "banner_image_description WHERE banner_id = '" . (int)$banner_id . "'");

		$this->event->trigger('post.admin.bannerhome.delete', $banner_id);
	}

	public function getBanner($banner_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "bannerhome WHERE banner_id = '" . (int)$banner_id . "'");

		return $query->row;
	}

	public function getBanners($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "bannerhome";

		$sort_data = array(
			'name',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getBannerImages($banner_id) {
		$banner_image_data = array();

		$banner_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "bannerhome_image WHERE banner_id = '" . (int)$banner_id . "' ORDER BY sort_order ASC");

		$arrImgs = array();
		$banner_image_description_data = array();
		foreach ($banner_image_query->rows as $banner_image) {			

			$banner_image_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "bannerhome_image_description WHERE banner_image_id = '" . (int)$banner_image['banner_image_id'] . "' AND banner_id = '" . (int)$banner_id . "'");

						
			foreach ($banner_image_description_query->rows as $banner_image_description) {
				$banner_image_description_data[$banner_image_description['language_id']] = array('title' => $banner_image_description['title']);
			}

						$arrImgs['image0'][$banner_image['language_id']] = array('image' => $banner_image['image1']);
						$arrImgs['image1'][$banner_image['language_id']] = array('image' => $banner_image['image2']);
						$arrImgs['image2'][$banner_image['language_id']] = array('image' => $banner_image['image3']);
				
		}	
			$banner_image_data[] = array(
				'banner_image_description' => $banner_image_description_data,
				'image0'                    => $arrImgs['image0'],
				'image1'                    => $arrImgs['image1'],
				'image2'                    => $arrImgs['image2'],
				'sort_order'               => $banner_image['sort_order']
			);
		
		return $banner_image_data;
	}

	public function getTotalBanners() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "bannerhome");

		return $query->row['total'];
	}
}