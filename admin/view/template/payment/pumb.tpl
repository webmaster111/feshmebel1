<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger">
			<i class="fa fa-exclamation-circle"></i>
			<?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
					<div class="form-group">
						<label for="pumb_merch_id" class="col-sm-2 control-label"><?php echo $entry_merch_id; ?></label>
						<div class="col-sm-10">
							<input type="text" name="pumb_merch_id" value="<?php echo $pumb_merch_id; ?>" placeholder="<?php echo $entry_merch_id; ?>" class="form-control" />
							<?php if (!empty($pumb_merch_id)) { ?>
								<?php if ($pumb_live_demo) { ?>
									<p class="help-block"><a href="https://pps.fuib.com/merchant/<?php echo $pumb_merch_id; ?>/" target="_blank"><?php echo $text_merchant_console; ?></a></p>
								<?php } else { ?>
									<p class="help-block"><a href="https://pps03.fuib.com/merchant/<?php echo $pumb_merch_id; ?>/" target="_blank"><?php echo $text_merchant_console; ?></a></p>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label for="pumb_account_id" class="col-sm-2 control-label"><?php echo $entry_account_id; ?></label>
						<div class="col-sm-10">
							<input type="text" name="pumb_account_id" value="<?php echo $pumb_account_id; ?>" placeholder="<?php echo $entry_account_id; ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="pumb_live_demo" class="col-sm-2 control-label"><?php echo $entry_live_demo; ?></label>
						<div class="col-sm-10">
							 <select name="pumb_live_demo" class="form-control">
								<?php if ($pumb_live_demo) { ?>
									<option value="1" selected="selected"><?php echo $text_live; ?></option>
									<option value="0"><?php echo $text_demo; ?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_live; ?></option>
									<option value="0" selected="selected"><?php echo $text_demo; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="pumb_live_url" class="col-sm-2 control-label"><?php echo $entry_live_url; ?></label>
						<div class="col-sm-10">
							<input type="text" name="pumb_live_url" value="<?php echo $pumb_live_url; ?>" placeholder="<?php echo $entry_live_url; ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="pumb_signature_live" class="col-sm-2 control-label"><?php echo $entry_signature_live; ?></label>
						<div class="col-sm-10">
							<textarea rows="5" name="pumb_signature_live" class="form-control"><?php echo $pumb_signature_live; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="pumb_demo_url" class="col-sm-2 control-label"><?php echo $entry_demo_url; ?></label>
						<div class="col-sm-10">
							<input type="text" name="pumb_demo_url" value="<?php echo $pumb_demo_url; ?>" placeholder="<?php echo $entry_demo_url; ?>" class="form-control" />
						</div>
					</div>
					<div class="form-group">
						<label for="pumb_signature_demo" class="col-sm-2 control-label"><?php echo $entry_signature_demo; ?></label>
						<div class="col-sm-10">
							<textarea rows="5" name="pumb_signature_demo" class="form-control"><?php echo $pumb_signature_demo; ?></textarea>
						</div>
					</div>
					 <div class="form-group">
						<label class="col-sm-2 control-label" for="input-total"><span data-toggle="tooltip" title="<?php echo $help_total; ?>"><?php echo $entry_total; ?></span></label>
						<div class="col-sm-10">
						  <input type="text" name="pumb_total" value="<?php echo $pumb_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
						<div class="col-sm-10">
						  <select name="pumb_order_status_id" id="input-order-status" class="form-control">
							<?php foreach ($order_statuses as $order_status) { ?>
							<?php if ($order_status['order_status_id'] == $pumb_order_status_id) { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
							<?php } ?>
							<?php } ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-order-status-success-id"><?php echo $entry_order_status_success; ?></label>
						<div class="col-sm-10">
						  <select name="pumb_order_status_success_id" id="input-order-status-success-id" class="form-control">
							<?php foreach ($order_statuses as $order_status) { ?>
							<?php if ($order_status['order_status_id'] == $pumb_order_status_success_id) { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
							<?php } ?>
							<?php } ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-order-status-failed-id"><?php echo $entry_order_status_failed; ?></label>
						<div class="col-sm-10">
						  <select name="pumb_order_status_failed_id" id="input-order-status-failed-id" class="form-control">
							<?php foreach ($order_statuses as $order_status) { ?>
							<?php if ($order_status['order_status_id'] == $pumb_order_status_failed_id) { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
							<?php } ?>
							<?php } ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
						  <select name="pumb_status" id="input-status" class="form-control">
							<?php if ($pumb_status) { ?>
							<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
							<option value="0"><?php echo $text_disabled; ?></option>
							<?php } else { ?>
							<option value="1"><?php echo $text_enabled; ?></option>
							<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
							<?php } ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
						<div class="col-sm-10">
						  <input type="text" name="pumb_sort_order" value="<?php echo $pumb_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
						</div>
					  </div>
				</form>
			</div>
		</div>
	</div>			
</div>			
<?php echo $footer; ?> 