<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_copy; ?>" class="btn btn-default" onclick="$('#form-product').attr('action', '<?php echo $copy; ?>').submit()"><i class="fa fa-copy"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-supplier"><?php echo $entry_supplier; ?></label>
                <select name="filter_supplier" id="input-supplier" class="form-control">
                  <option value="*"></option>
                  <?php foreach($suppliers as $supplier) { ?>
                      <?php if ($filter_supplier==$supplier['supplier_id']) { ?>
                        <option value="<?php echo $supplier['supplier_id']; ?>"  selected="selected" ><?php echo $supplier['name']; ?></option>
                      <?php } else { ?>
                        <option value="<?php echo $supplier['supplier_id']; ?>"><?php echo $supplier['name']; ?></option>
                      <?php } ?>
                  <?php } ?>

                </select>
              </div>

              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-price"><?php echo $entry_price; ?></label>
                <input type="text" name="filter_price" value="<?php echo $filter_price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-price-current"><?php echo $entry_price_current; ?></label>
                <input type="text" name="filter_price_current" value="<?php echo $filter_price_current; ?>" placeholder="<?php echo $entry_price_current; ?>" id="input-price-current" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">

              <div class="form-group">
                <label class="control-label" for="input-sale_status"><?php echo $entry_sale_status; ?></label>
                <select name="filter_sale_status" id="input-sale_status" class="form-control">
                  <option value="*"></option>
                  <?php foreach($sale_statuses as $sale_status) { ?>
                  <?php if ($filter_sale_status==$sale_status['sale_status_id']) { ?>
                  <option value="<?php echo $sale_status['sale_status_id']; ?>"  selected="selected" ><?php echo $sale_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $sale_status['sale_status_id']; ?>"><?php echo $sale_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>

                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-stock_status"><?php echo $entry_stock_status; ?></label>
                <select name="filter_stock_status" id="input-stock_status" class="form-control">
                  <option value="*"></option>
                  <?php foreach($stock_statuses as $stock_status) { ?>
                  <?php if ($filter_stock_status==$stock_status['stock_status_id']) { ?>
                  <option value="<?php echo $stock_status['stock_status_id']; ?>"  selected="selected" ><?php echo $stock_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>

                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-center"><?php echo $column_image; ?></td>
                  <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php echo $column_feed; ?></td>
                  <td class="text-left"><?php echo $column_topseller; ?></td>
                  <td class="text-right"><?php if ($sort == 'p.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'p.price_current') { ?>
                    <a href="<?php echo $sort_price_current; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price_current; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price_current; ?>"><?php echo $column_price_current; ?></a>
                    <?php } ?></td>

                  <td class="text-left"><?php if ($sort == 'p.sale_status') { ?>
                    <a href="<?php echo $sort_sale_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sale_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sale_status; ?>"><?php echo $column_sale_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.stock_status') { ?>
                    <a href="<?php echo $sort_stock_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_stock_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_stock_status; ?>"><?php echo $column_stock_status; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'p.quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-center"><?php if ($product['image']) { ?>
                    <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" />
                    <?php } else { ?>
                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                    <?php } ?></td>
                  <td class="text-left"><?php echo $product['name']; ?></td>
                  <td class="text-left">
                    <select name="feed" class="form-control input-feed" data-product_id="<?=$product['product_id'];?>">
                      <?php if ($product['feed']) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </td>
                  <td class="text-left">
                    <select name="topseller" class="form-control input-topseller" data-product_id="<?=$product['product_id'];?>">
                      <?php if ($product['topseller']) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </td>


                  <td class="text-right"><?php if ($product['special']) { ?>
                    <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                    <div class="text-danger"><?php echo $product['special']; ?></div>
                    <?php } else { ?>

                    <?php } ?>
                    <div style="white-space: nowrap">
                      <input type="text" name="price" value="<?php echo $product['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control "  data-product_id="<?=$product['product_id'];?>" style="display: inline-block; width: 100px;" />
                      <span class="btn btn-success input-price"><i class="fa fa-save"></i></span>
                    </div>
                  </td>
                  <td class="text-right" id="price_current-<?=$product['product_id'];?>"><?php echo $product['price_current']; ?></td>

                  <td class="text-left">
                    <select name="sale_status_id" class="form-control input-sale-status" data-product_id="<?=$product['product_id'];?>">
                      <option value=""></option>
                      <?php foreach ($sale_statuses as $sale_status) { ?>
                      <?php if ($sale_status['sale_status_id'] == $product['sale_status_id']) { ?>
                      <option value="<?php echo $sale_status['sale_status_id']; ?>" selected="selected"><?php echo $sale_status['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $sale_status['sale_status_id']; ?>"><?php echo $sale_status['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </td>
                  <td class="text-left">
                    <select name="stock_status_id" class="form-control input-stock-status" data-product_id="<?=$product['product_id'];?>">
                      <?php foreach ($stock_statuses as $stock_status) { ?>
                      <?php if ($stock_status['stock_status_id'] == $product['stock_status_id']) { ?>
                      <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </td>
                  <td class="text-left">
                    <select name="status" class="form-control input-status" data-product_id="<?=$product['product_id'];?>">
                      <?php if ($product['status']) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
                  </td>
                  <td class="text-right">
                    <div style="white-space: nowrap">
                      <input type="text" name="quantity" value="<?php echo $product['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control "  data-product_id="<?=$product['product_id'];?>" style="display: inline-block; width: 70px;" />
                      <span class="btn btn-success input-quantity"><i class="fa fa-save"></i></span>
                    </div>

                  </td>
                  <td class="text-right"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
    $('.input-feed, .input-topseller, .input-sale-status, .input-stock-status, .input-status').on('change', function() {
      var product_id = $(this).data('product_id');
      var name = $(this).attr('name');
      var value = $(this).val();

      $.post( 'index.php?route=catalog/product/save&token=<?php echo $token; ?>', { product_id: product_id, name: name, value: value })
      .done(function( data ) {
      console.log(data);
        $('#price_current-' + product_id).html(data);
      });

    });
    $('.input-price, .input-quantity').on('click', function() {
      input = $(this).closest('.text-right').find('input');
      var product_id = $(input).data('product_id');
      var name = $(input).attr('name');
      var value = $(input).val();


      $.post( 'index.php?route=catalog/product/save&token=<?php echo $token; ?>', { product_id: product_id, name: name, value: value })
      .done(function( data ) {
        console.log(data);
        $('#price_current-' + product_id).html(data);
      });

    });
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_price_current = $('input[name=\'filter_price_current\']').val();

	if (filter_price_current) {
		url += '&filter_price_current=' + encodeURIComponent(filter_price_current);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

    var filter_stock_status = $('select[name=\'filter_stock_status\']').val();

	if (filter_stock_status != '*') {
		url += '&filter_stock_status=' + encodeURIComponent(filter_stock_status);
	}

    var filter_sale_status = $('select[name=\'filter_sale_status\']').val();

	if (filter_sale_status != '*') {
		url += '&filter_sale_status=' + encodeURIComponent(filter_sale_status);
	}

    var filter_supplier = $('select[name=\'filter_supplier\']').val();

    if (filter_supplier != '*') {
      url += '&filter_supplier=' + encodeURIComponent(filter_supplier);
    }


  location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});
//--></script></div>
<?php echo $footer; ?>