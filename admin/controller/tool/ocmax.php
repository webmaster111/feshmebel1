<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.5
 * @ Release on : 22.05.2019
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 */
class ControllerToolOCMax extends Controller
{
    private $error = array();

    public function index($license, $extension)
    {
        $this->load->language('tool/ocmax');
        $data['token'] = $this->session->data['token'];
        $data['text_license'] = $this->language->get('text_license');
        $data['text_license_request'] = $this->language->get('text_license_request');
        $data['text_support'] = $this->language->get('text_support');
        $data['text_about_license'] = $this->language->get('text_about_license');
        $data['text_about_support'] = $this->language->get('text_about_support');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['error_unexpected'] = $this->language->get('error_unexpected');
        $data['entry_license'] = $this->language->get('entry_license');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_domain'] = $this->language->get('entry_domain');
        $data['entry_market'] = $this->language->get('entry_market');
        $data['entry_check'] = $this->language->get('entry_check');
        $data['help_activate'] = $this->language->get('help_activate');
        $data['help_license'] = $this->language->get('help_license');
        $data['help_email'] = $this->language->get('help_email');
        $data['help_domain'] = $this->language->get('help_domain');
        $data['help_market'] = $this->language->get('help_market');
        $data['help_check'] = $this->language->get('help_check');
        $data['help_send'] = $this->language->get('help_send');
        $data['extension'] = $extension;


        if (isset($this->request->post[$extension . '[license]'])) {
            $data['license'] = $this->request->post[$extension . '_license'];
        }
        else {
            $data['license'] = $this->config->get($extension . '_license');
        }

        $data['check_license'] = $license;
        return $this->load->view('tool/ocmax.tpl', $data);
    }

    public function purchase()
    {
        $json = array();
        $this->load->language('tool/ocmax');
        $extension = $this->request->get['extension'];

        if ($this->validate()) {
            switch ($this->request->get['action']) {
                case 'send':
                    if (isset($this->request->get['email'])) {
                        $email = urlencode($this->request->get['email']);
                    }

                    if (isset($this->request->get['domain'])) {
                        $domain = urlencode($this->request->get['domain']);
                    }

                    if (isset($this->request->get['market'])) {
                        $market = urlencode($this->request->get['market']);
                    }

                    if (isset($this->request->get['check'])) {
                        $check = urlencode($this->request->get['check']);
                    }

                    $ch = curl_init('http://oc-max.com/index.php?route=module/ocmax/addPurchase&extension=' . $extension . '&email=' . $email . '&domain=' . $domain . '&market=' . $market . '&check=' . $check);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    $response = curl_exec($ch);
                    curl_close($ch);

                    if ($response) {
                        $json['success'] = $this->language->get('text_success_sent');
                    }
                    else {
                        $json['error'] = $this->language->get('error_sent');
                    }

                    break;
                case 'activate':
                    if ($this->licenseVerification($this->request->get['license'], $extension)) {
                        $this->load->model('setting/setting');
                        $this->model_setting_setting->editSetting($extension, array($extension . '_license' => $this->request->get['license']));
                        $json['success'] = $this->language->get('text_success_activate');
                        $json['redirect'] = true;
                    }
                    else {
                        $json['error'] = $this->language->get('error_activate');
                    }
            }
        }
        else {
            $json['error'] = $this->error['error'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function licenseVerification($license, $extension)
    {
        $verification = false;
        $key = md5('PPNH');

        if (HTTP_SERVER) {
            $url = parse_url(HTTP_SERVER);
            $d_1 = str_replace('www.', '', $url['host']);
        }
        else {
            $d_1 = '';
        }

        $d_2 = str_replace('www.', '', getenv('SERVER_NAME'));
        $d_3 = str_replace('www.', '', getenv('HTTP_HOST'));

        $domain = (($d_1 == $d_2) && ($d_2 == $d_3) ? $d_1 : $d_1 . '-' . $d_2 . '-' . $d_3);
        $license_check = md5(md5(md5($key) . $domain . $extension));
        $options = array(
            CURLOPT_HTTPHEADER     => array('Content-Type: application/json'),
            CURLOPT_HEADER         => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CONNECTTIMEOUT => 2,
            CURLOPT_TIMEOUT        => 4,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FRESH_CONNECT  => 1
        );
        $ch = curl_init('http://oc-max.com/index.php?route=module/ocmax/licenseVerification&extension=' . $extension . '&domain=' . $domain . '&license=' . $license);
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $active = md5(date('LyL') . md5(date('yLy') * 54321));
        $block = md5(date('L') . md5(date('L') * 12345));

        if ($response === $active) {
            $verification = true;
        }
        else if ($response === $block) {
            $this->db->query('DELETE FROM `' . DB_PREFIX . 'setting` WHERE `code` = \'' . $this->db->escape($extension) . '\'');

            if ($extension == 'novaposhta') {
                $this->db->query('TRUNCATE `' . DB_PREFIX . 'novaposhta_cities`');
                $this->db->query('TRUNCATE `' . DB_PREFIX . 'novaposhta_warehouses`');
                $this->db->query('TRUNCATE `' . DB_PREFIX . 'novaposhta_references`');
            }
            else if ($extension == 'membership') {
                $this->db->query('TRUNCATE `' . DB_PREFIX . 'member_card`');
                $this->db->query('TRUNCATE `' . DB_PREFIX . 'member_card_history`');
                $this->db->query('TRUNCATE `' . DB_PREFIX . 'member_card_category`');
                $this->db->query('TRUNCATE `' . DB_PREFIX . 'member_card_product`');
            }
        }
        else {
            if (($response === false) || ($http_code != 200)) {
                if (md5($license) == $license_check) {
                    $verification = true;
                }
            }
        }

        return $verification ? md5(date('WwtLIZ') . md5(date('WwtLIZ'))) : $verification;
    }

    private function validate()
    {
        if (!$this->user->hasPermission('modify', 'tool/ocmax')) {
            $this->error['error'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}


?>
