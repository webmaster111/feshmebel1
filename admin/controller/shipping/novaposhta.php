<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.5
 * @ Release on : 22.05.2019
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 */
class ControllerShippingNovaPoshta extends Controller
{
    public $version = '3.1';
    private $error = array();
    private $license;

    public function __construct($registry)
    {
        parent::__construct($registry);
        require_once DIR_APPLICATION . 'controller/tool/ocmax.php';
        require_once DIR_SYSTEM . 'helper/novaposhta.php';
        $registry->set('ocmax', new ControllerToolOCMax($registry));
        $registry->set('novaposhta', new NovaPoshta($registry));
        $this->license = true;
    }

    public function install()
    {
        $this->db->query('CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'novaposhta_cities`' . "\r\n" . '   ' . "\t\t\t" . '(`id` int(11) NOT NULL AUTO_INCREMENT, ' . "\r\n" . '   ' . "\t\t\t" . '`Description` varchar(200) NOT NULL, ' . "\r\n" . '   ' . "\t\t\t" . '`DescriptionRu` varchar(200) NOT NULL,  ' . "\r\n" . '   ' . "\t\t\t" . '`Ref` varchar(100) NOT NULL,  ' . "\r\n" . '   ' . "\t\t\t" . '`Area` varchar(100) NOT NULL,   ' . "\r\n" . '   ' . "\t\t\t" . '`Delivery1` tinyint(1) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Delivery2` tinyint(1) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Delivery3` tinyint(1) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Delivery4` tinyint(1) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Delivery5` tinyint(1) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Delivery6` tinyint(1) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Delivery7` tinyint(1) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Conglomerates` text NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`CityID` int(11) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . 'UNIQUE(`id`),' . "\r\n" . '   ' . "\t\t\t" . 'PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
        $this->db->query('CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'novaposhta_warehouses`' . "\r\n" . '   ' . "\t\t\t" . '(`id` int(11) NOT NULL AUTO_INCREMENT, ' . "\r\n" . '   ' . "\t\t\t" . '`SiteKey` varchar(100) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Description` varchar(500) NOT NULL, ' . "\r\n" . '   ' . "\t\t\t" . '`DescriptionRu` varchar(500) NOT NULL,  ' . "\r\n" . '   ' . "\t\t\t" . '`Phone` varchar(100) NOT NULL,  ' . "\r\n" . '   ' . "\t\t\t" . '`TypeOfWarehouse` varchar(100) NOT NULL,   ' . "\r\n" . '   ' . "\t\t\t" . '`Ref` varchar(100) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Number` int(10) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`CityRef` varchar(100) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`CityDescription` varchar(200) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`CityDescriptionRu` varchar(200) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Longitude` double NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Latitude` double NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`PostFinance` tinyint(3) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`TotalMaxWeightAllowed` int(10) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`PlaceMaxWeightAllowed` int(10) NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Reception` text NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Delivery` text NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . '`Schedule` text NOT NULL,' . "\r\n" . '   ' . "\t\t\t" . 'UNIQUE(`id`),' . "\r\n" . '   ' . "\t\t\t" . 'PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
        $this->db->query('CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'novaposhta_references`' . "\r\n" . '   ' . "\t\t\t" . '(`id` int(11) NOT NULL AUTO_INCREMENT, ' . "\r\n" . '   ' . "\t\t\t" . '`type` varchar(100) NOT NULL, ' . "\r\n" . '   ' . "\t\t\t" . '`value` mediumtext NOT NULL,  ' . "\r\n" . '   ' . "\t\t\t" . 'UNIQUE(`type`),' . "\r\n" . '   ' . "\t\t\t" . 'PRIMARY KEY (`id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1');
        $this->db->query('ALTER TABLE `' . DB_PREFIX . 'order` ' . "\r\n\t\t\t" . 'ADD `novaposhta_ei_number` varchar(100) NOT NULL AFTER `invoice_prefix`, ' . "\r\n\t\t\t" . 'ADD `novaposhta_ei_ref` varchar(100) NOT NULL AFTER `novaposhta_ei_number`;');
    }

    public function uninstall()
    {
        $this->db->query('DROP TABLE `' . DB_PREFIX . 'novaposhta_cities`,  `' . DB_PREFIX . 'novaposhta_warehouses`, `' . DB_PREFIX . 'novaposhta_references`');
    }

    public function index()
    {
        $this->load->language('shipping/novaposhta');
        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('novaposhta', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');

            if (!isset($this->request->get['saveAndStay'])) {
                $this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
            }
        }

        $data['action'] = $this->url->link('shipping/novaposhta', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'));
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_shipping'), 'href' => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
        $data['breadcrumbs'][] = array('text' => $this->language->get('heading_title'), 'href' => $this->url->link('shipping/novaposhta', 'token=' . $this->session->data['token'], 'SSL'));

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        }
        else {
            $data['success'] = '';
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        }
        else {
            $data['error_warning'] = '';
        }

        $data['token'] = $this->session->data['token'];
        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_tariff'] = $this->language->get('heading_tariff');
        $data['button_save_and_stay'] = $this->language->get('button_save_and_stay');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_tariff'] = $this->language->get('button_tariff');
        $data['button_generate'] = $this->language->get('button_generate');
        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_database'] = $this->language->get('tab_database');
        $data['tab_sender'] = $this->language->get('tab_sender');
        $data['tab_recipient'] = $this->language->get('tab_recipient');
        $data['tab_departure'] = $this->language->get('tab_departure');
        $data['tab_cron'] = $this->language->get('tab_cron');
        $data['tab_support'] = $this->language->get('tab_support');
        $data['column_weight'] = $this->language->get('column_weight');
        $data['column_service_type'] = $this->language->get('column_service_type');
        $data['column_warehouse_warehouse'] = $this->language->get('column_warehouse_warehouse');
        $data['column_warehouse_doors'] = $this->language->get('column_warehouse_doors');
        $data['column_doors_warehouse'] = $this->language->get('column_doors_warehouse');
        $data['column_doors_doors'] = $this->language->get('column_doors_doors');
        $data['column_type'] = $this->language->get('column_type');
        $data['column_date'] = $this->language->get('column_date');
        $data['column_amount'] = $this->language->get('column_amount');
        $data['column_description'] = $this->language->get('column_description');
        $data['column_action'] = $this->language->get('column_action');
        $data['column_novaposhta_status'] = $this->language->get('column_novaposhta_status');
        $data['column_store_status'] = $this->language->get('column_store_status');
        $data['column_notification'] = $this->language->get('column_notification');
        $data['column_message'] = $this->language->get('column_message');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_update'] = $this->language->get('text_update');
        $data['text_table_tariffs'] = $this->language->get('text_table_tariffs');
        $data['text_each_goods'] = $this->language->get('text_each_goods');
        $data['text_all_goods'] = $this->language->get('text_all_goods');
        $data['text_grn'] = $this->language->get('text_grn');
        $data['text_kg'] = $this->language->get('text_kg');
        $data['text_cm'] = $this->language->get('text_cm');
        $data['text_pct'] = $this->language->get('text_pct');
        $data['text_base_update'] = $this->language->get('text_base_update');
        $data['text_shipments_tracking'] = $this->language->get('text_shipments_tracking');
        $data['text_settings_shipments_statuses'] = $this->language->get('text_settings_shipments_statuses');
        $data['text_message_template_variables'] = $this->language->get('text_message_template_variables');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_courier_shipping'] = $this->language->get('entry_courier_shipping');
        $data['entry_shipping_warehouse_name'] = $this->language->get('entry_shipping_warehouse_name');
        $data['entry_shipping_doors_name'] = $this->language->get('entry_shipping_doors_name');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_tax_class'] = $this->language->get('entry_tax_class');
        $data['entry_key_api'] = $this->language->get('entry_key_api');
        $data['entry_cost'] = $this->language->get('entry_cost');
        $data['entry_api_calculation'] = $this->language->get('entry_api_calculation');
        $data['entry_tariff_calculation'] = $this->language->get('entry_tariff_calculation');
        $data['entry_discount'] = $this->language->get('entry_discount');
        $data['entry_additional_commission'] = $this->language->get('entry_additional_commission');
        $data['entry_additional_commission_bottom'] = $this->language->get('entry_additional_commission_bottom');
        $data['entry_free_shipping'] = $this->language->get('entry_free_shipping');
        $data['entry_delivery_period'] = $this->language->get('entry_delivery_period');
        $data['entry_warehouses_filter_weight'] = $this->language->get('entry_warehouses_filter_weight');
        $data['entry_warehouse_types'] = $this->language->get('entry_warehouse_types');
        $data['entry_payment_cod'] = $this->language->get('entry_payment_cod');
        $data['entry_update_areas'] = $this->language->get('entry_update_areas');
        $data['entry_update_cities'] = $this->language->get('entry_update_cities');
        $data['entry_update_warehouses'] = $this->language->get('entry_update_warehouses');
        $data['entry_update_references'] = $this->language->get('entry_update_references');
        $data['entry_sender'] = $this->language->get('entry_sender');
        $data['entry_recipient'] = $this->language->get('entry_recipient');
        $data['entry_contact_person'] = $this->language->get('entry_contact_person');
        $data['entry_phone'] = $this->language->get('entry_phone');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_address'] = $this->language->get('entry_address');
        $data['entry_cargo_description'] = $this->language->get('entry_cargo_description');
        $data['entry_additional_information'] = $this->language->get('entry_additional_information');
        $data['entry_weight'] = $this->language->get('entry_weight');
        $data['entry_dimensions'] = $this->language->get('entry_dimensions');
        $data['entry_calculate_volume'] = $this->language->get('entry_calculate_volume');
        $data['entry_use_parameters'] = $this->language->get('entry_use_parameters');
        $data['entry_key_cron'] = $this->language->get('entry_key_cron');
        $data['entry_shipments_tracking'] = $this->language->get('entry_shipments_tracking');
        $data['entry_tracking_statuses'] = $this->language->get('entry_tracking_statuses');
        $data['entry_admin_notification'] = $this->language->get('entry_admin_notification');
        $data['entry_customer_notification'] = $this->language->get('entry_customer_notification');
        $data['help_status'] = $this->language->get('help_status');
        $data['help_sort_order'] = $this->language->get('help_sort_order');
        $data['help_courier_shipping'] = $this->language->get('help_courier_shipping');
        $data['help_shipping_warehouse_name'] = $this->language->get('help_shipping_warehouse_name');
        $data['help_shipping_doors_name'] = $this->language->get('help_shipping_doors_name');
        $data['help_geo_zone'] = $this->language->get('help_geo_zone');
        $data['help_tax_class'] = $this->language->get('help_tax_class');
        $data['help_key_api'] = $this->language->get('help_key_api');
        $data['help_cost'] = $this->language->get('help_cost');
        $data['help_api_calculation'] = $this->language->get('help_api_calculation');
        $data['help_tariff_calculation'] = $this->language->get('help_tariff_calculation');
        $data['help_discount'] = $this->language->get('help_discount');
        $data['help_additional_commission'] = $this->language->get('help_additional_commission');
        $data['help_additional_commission_bottom'] = $this->language->get('help_additional_commission_bottom');
        $data['help_free_shipping'] = $this->language->get('help_free_shipping');
        $data['help_delivery_period'] = $this->language->get('help_delivery_period');
        $data['help_warehouses_filter_weight'] = $this->language->get('help_warehouses_filter_weight');
        $data['help_warehouse_types'] = $this->language->get('help_warehouse_types');
        $data['help_payment_cod'] = $this->language->get('help_payment_cod');
        $data['help_update_areas'] = $this->language->get('help_update_areas');
        $data['help_update_cities'] = $this->language->get('help_update_cities');
        $data['help_update_warehouses'] = $this->language->get('help_update_warehouses');
        $data['help_update_references'] = $this->language->get('help_update_references');
        $data['help_sender'] = $this->language->get('help_sender');
        $data['help_sender_contact_person'] = $this->language->get('help_sender_contact_person');
        $data['help_sender_city'] = $this->language->get('help_sender_city');
        $data['help_sender_address'] = $this->language->get('help_sender_address');
        $data['help_recipient'] = $this->language->get('help_recipient');
        $data['help_recipient_contact_person'] = $this->language->get('help_recipient_contact_person');
        $data['help_recipient_contact_person_phone'] = $this->language->get('help_recipient_contact_person_phone');
        $data['help_recipient_city'] = $this->language->get('help_recipient_city');
        $data['help_recipient_address'] = $this->language->get('help_recipient_address');
        $data['help_cargo_description'] = $this->language->get('help_cargo_description');
        $data['help_additional_information'] = $this->language->get('help_additional_information');
        $data['help_weight'] = $this->language->get('help_weight');
        $data['help_dimensions'] = $this->language->get('help_dimensions');
        $data['help_calculate_volume'] = $this->language->get('help_calculate_volume');
        $data['help_use_parameters'] = $this->language->get('help_use_parameters');
        $data['help_key_cron'] = $this->language->get('help_key_cron');
        $data['help_tracking_statuses'] = $this->language->get('help_tracking_statuses');
        $data['help_cron'] = $this->language->get('help_cron');
        $data['help_order_template_vars'] = $this->language->get('help_order_template_vars');
        $data['help_products_template_vars'] = $this->language->get('help_products_template_vars');
        $data['help_ei_template_vars'] = $this->language->get('help_ei_template_vars');
        $this->load->model('localisation/language');
        $this->load->model('localisation/geo_zone');
        $this->load->model('localisation/tax_class');
        $this->load->model('extension/extension');
        $this->load->model('localisation/order_status');
        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
        $data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
        $data['languages'] = $this->model_localisation_language->getLanguages();
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $data['language_id'] = '';
        $extensions = $this->model_extension_extension->getInstalled('payment');

        foreach ($extensions as $extension) {
            if ($this->config->get($extension . '_status')) {
                $this->load->language('payment/' . $extension);
                $data['payment_methods'][$extension] = $this->language->get('heading_title');
            }
        }

        if (isset($this->request->post['novaposhta_status'])) {
            $data['novaposhta_status'] = $this->request->post['novaposhta_status'];
        }
        else {
            $data['novaposhta_status'] = $this->config->get('novaposhta_status');
        }

        if (isset($this->request->post['novaposhta_sort_order'])) {
            $data['novaposhta_sort_order'] = $this->request->post['novaposhta_sort_order'];
        }
        else {
            $data['novaposhta_sort_order'] = $this->config->get('novaposhta_sort_order');
        }

        if (isset($this->request->post['novaposhta_courier_shipping'])) {
            $data['novaposhta_courier_shipping'] = $this->request->post['novaposhta_courier_shipping'];
        }
        else {
            $data['novaposhta_courier_shipping'] = $this->config->get('novaposhta_courier_shipping');
        }

        foreach ($data['languages'] as $language) {
            if ($language['code'] == $this->config->get('config_admin_language')) {
                $data['language_id'] = $language['language_id'];
            }

            $data['language_flag_' . $language['language_id']] = version_compare(VERSION, '2.2', '>=') ? 'language/' . $language['code'] . '/' . $language['code'] . '.png' : 'view/image/flags/' . $language['image'];

            if (isset($this->request->post['novaposhta_shipping_warehouse_name_' . $language['language_id']])) {
                $data['novaposhta_shipping_warehouse_name_' . $language['language_id']] = $this->request->post['novaposhta_shipping_warehouse_name_' . $language['language_id']];
            }
            else if ($this->config->get('novaposhta_shipping_warehouse_name_' . $language['language_id'])) {
                $data['novaposhta_shipping_warehouse_name_' . $language['language_id']] = $this->config->get('novaposhta_shipping_warehouse_name_' . $language['language_id']);
            }
            else {
                $data['novaposhta_shipping_warehouse_name_' . $language['language_id']] = '';
            }

            if (isset($this->request->post['novaposhta_shipping_doors_name_' . $language['language_id']])) {
                $data['novaposhta_shipping_doors_name_' . $language['language_id']] = $this->request->post['novaposhta_shipping_doors_name_' . $language['language_id']];
            }
            else if ($this->config->get('novaposhta_shipping_doors_name_' . $language['language_id'])) {
                $data['novaposhta_shipping_doors_name_' . $language['language_id']] = $this->config->get('novaposhta_shipping_doors_name_' . $language['language_id']);
            }
            else {
                $data['novaposhta_shipping_doors_name_' . $language['language_id']] = '';
            }
        }

        if (isset($this->request->post['novaposhta_geo_zone_id'])) {
            $data['novaposhta_geo_zone_id'] = $this->request->post['novaposhta_geo_zone_id'];
        }
        else {
            $data['novaposhta_geo_zone_id'] = $this->config->get('novaposhta_geo_zone_id');
        }

        if (isset($this->request->post['novaposhta_tax_class_id'])) {
            $data['novaposhta_tax_class_id'] = $this->request->post['novaposhta_tax_class_id'];
        }
        else {
            $data['novaposhta_tax_class_id'] = $this->config->get('novaposhta_tax_class_id');
        }

        if (isset($this->request->post['novaposhta_key_api'])) {
            $data['novaposhta_key_api'] = $this->request->post['novaposhta_key_api'];
        }
        else {
            $data['novaposhta_key_api'] = $this->config->get('novaposhta_key_api');
        }

        if (isset($this->request->post['novaposhta_cost'])) {
            $data['novaposhta_cost'] = $this->request->post['novaposhta_cost'];
        }
        else {
            $data['novaposhta_cost'] = $this->config->get('novaposhta_cost');
        }

        if (isset($this->request->post['novaposhta_api_calculation'])) {
            $data['novaposhta_api_calculation'] = $this->request->post['novaposhta_api_calculation'];
        }
        else {
            $data['novaposhta_api_calculation'] = $this->config->get('novaposhta_api_calculation');
        }

        if (isset($this->request->post['novaposhta_tariff_calculation'])) {
            $data['novaposhta_tariff_calculation'] = $this->request->post['novaposhta_tariff_calculation'];
        }
        else {
            $data['novaposhta_tariff_calculation'] = $this->config->get('novaposhta_tariff_calculation');
        }

        if (isset($this->request->post['novaposhta_tariffs'])) {
            $data['novaposhta_tariffs'] = $this->request->post['novaposhta_tariffs'];
        }
        else {
            $data['novaposhta_tariffs'] = $this->config->get('novaposhta_tariffs');
        }

        if (isset($this->request->post['novaposhta_discount'])) {
            $data['novaposhta_discount'] = $this->request->post['novaposhta_discount'];
        }
        else {
            $data['novaposhta_discount'] = $this->config->get('novaposhta_discount');
        }

        if (isset($this->request->post['novaposhta_additional_commission'])) {
            $data['novaposhta_additional_commission'] = $this->request->post['novaposhta_additional_commission'];
        }
        else {
            $data['novaposhta_additional_commission'] = $this->config->get('novaposhta_additional_commission');
        }

        if (isset($this->request->post['novaposhta_additional_commission_bottom'])) {
            $data['novaposhta_additional_commission_bottom'] = $this->request->post['novaposhta_additional_commission_bottom'];
        }
        else {
            $data['novaposhta_additional_commission_bottom'] = $this->config->get('novaposhta_additional_commission_bottom');
        }

        if (isset($this->request->post['novaposhta_free_shipping'])) {
            $data['novaposhta_free_shipping'] = $this->request->post['novaposhta_free_shipping'];
        }
        else {
            $data['novaposhta_free_shipping'] = $this->config->get('novaposhta_free_shipping');
        }

        if (isset($this->request->post['novaposhta_delivery_period'])) {
            $data['novaposhta_delivery_period'] = $this->request->post['novaposhta_delivery_period'];
        }
        else {
            $data['novaposhta_delivery_period'] = $this->config->get('novaposhta_delivery_period');
        }

        if (isset($this->request->post['novaposhta_warehouses_filter_weight'])) {
            $data['novaposhta_warehouses_filter_weight'] = $this->request->post['novaposhta_warehouses_filter_weight'];
        }
        else {
            $data['novaposhta_warehouses_filter_weight'] = $this->config->get('novaposhta_warehouses_filter_weight');
        }

        if (isset($this->request->post['novaposhta_warehouse_types'])) {
            $data['novaposhta_warehouse_types'] = $this->request->post['novaposhta_warehouse_types'];
        }
        else {
            $data['novaposhta_warehouse_types'] = $this->config->get('novaposhta_warehouse_types');
        }

        if (isset($this->request->post['novaposhta_payment_cod'])) {
            $data['novaposhta_payment_cod'] = $this->request->post['novaposhta_payment_cod'];
        }
        else {
            $data['novaposhta_payment_cod'] = $this->config->get('novaposhta_payment_cod');
        }

        if (isset($this->request->post['novaposhta_sender'])) {
            $data['novaposhta_sender'] = $this->request->post['novaposhta_sender'];
        }
        else {
            $data['novaposhta_sender'] = $this->config->get('novaposhta_sender');
        }

        if (isset($this->request->post['novaposhta_sender_contact_person'])) {
            $data['novaposhta_sender_contact_person'] = $this->request->post['novaposhta_sender_contact_person'];
        }
        else {
            $data['novaposhta_sender_contact_person'] = $this->config->get('novaposhta_sender_contact_person');
        }

        if (isset($this->request->post['novaposhta_sender_city'])) {
            $data['novaposhta_sender_city'] = $this->request->post['novaposhta_sender_city'];
        }
        else {
            $data['novaposhta_sender_city'] = $this->config->get('novaposhta_sender_city');
        }

        if (isset($this->request->post['novaposhta_sender_city_name'])) {
            $data['novaposhta_sender_city_name'] = $this->request->post['novaposhta_sender_city_name'];
        }
        else {
            $data['novaposhta_sender_city_name'] = $this->config->get('novaposhta_sender_city_name');
        }

        if (isset($this->request->post['novaposhta_sender_address'])) {
            $data['novaposhta_sender_address'] = $this->request->post['novaposhta_sender_address'];
        }
        else {
            $data['novaposhta_sender_address'] = $this->config->get('novaposhta_sender_address');
        }

        if (isset($this->request->post['novaposhta_sender_address_type'])) {
            $data['novaposhta_sender_address_type'] = $this->request->post['novaposhta_sender_address_type'];
        }
        else {
            $data['novaposhta_sender_address_type'] = $this->config->get('novaposhta_sender_address_type');
        }

        if (isset($this->request->post['novaposhta_recipient_name'])) {
            $data['novaposhta_recipient_name'] = $this->request->post['novaposhta_recipient_name'];
        }
        else {
            $data['novaposhta_recipient_name'] = $this->config->get('novaposhta_recipient_name');
        }

        if (isset($this->request->post['novaposhta_recipient_contact_person'])) {
            $data['novaposhta_recipient_contact_person'] = $this->request->post['novaposhta_recipient_contact_person'];
        }
        else if ($this->config->get('novaposhta_recipient_contact_person')) {
            $data['novaposhta_recipient_contact_person'] = $this->config->get('novaposhta_recipient_contact_person');
        }
        else {
            $data['novaposhta_recipient_contact_person'] = '{shipping_lastname} {shipping_firstname}';
        }

        if (isset($this->request->post['novaposhta_recipient_contact_person_phone'])) {
            $data['novaposhta_recipient_contact_person_phone'] = $this->request->post['novaposhta_recipient_contact_person_phone'];
        }
        else if ($this->config->get('novaposhta_recipient_contact_person_phone')) {
            $data['novaposhta_recipient_contact_person_phone'] = $this->config->get('novaposhta_recipient_contact_person_phone');
        }
        else {
            $data['novaposhta_recipient_contact_person_phone'] = '{telephone}';
        }

        if (isset($this->request->post['novaposhta_recipient_city'])) {
            $data['novaposhta_recipient_city'] = $this->request->post['novaposhta_recipient_city'];
        }
        else if ($this->config->get('novaposhta_recipient_city')) {
            $data['novaposhta_recipient_city'] = $this->config->get('novaposhta_recipient_city');
        }
        else {
            $data['novaposhta_recipient_city'] = '{shipping_city}';
        }

        if (isset($this->request->post['novaposhta_recipient_address'])) {
            $data['novaposhta_recipient_address'] = $this->request->post['novaposhta_recipient_address'];
        }
        else if ($this->config->get('novaposhta_recipient_address')) {
            $data['novaposhta_recipient_address'] = $this->config->get('novaposhta_recipient_address');
        }
        else {
            $data['novaposhta_recipient_address'] = '{shipping_address_1}';
        }

        if (isset($this->request->post['novaposhta_cargo_description'])) {
            $data['novaposhta_cargo_description'] = $this->request->post['novaposhta_cargo_description'];
        }
        else {
            $data['novaposhta_cargo_description'] = $this->config->get('novaposhta_cargo_description');
        }

        if (isset($this->request->post['novaposhta_additional_information'])) {
            $data['novaposhta_additional_information'] = $this->request->post['novaposhta_additional_information'];
        }
        else {
            $data['novaposhta_additional_information'] = $this->config->get('novaposhta_additional_information');
        }

        if (isset($this->request->post['novaposhta_weight'])) {
            $data['novaposhta_weight'] = $this->request->post['novaposhta_weight'];
        }
        else {
            $data['novaposhta_weight'] = $this->config->get('novaposhta_weight');
        }

        if (isset($this->request->post['novaposhta_dimensions_w'])) {
            $data['novaposhta_dimensions_w'] = $this->request->post['novaposhta_dimensions_w'];
        }
        else {
            $data['novaposhta_dimensions_w'] = $this->config->get('novaposhta_dimensions_w');
        }

        if (isset($this->request->post['novaposhta_dimensions_l'])) {
            $data['novaposhta_dimensions_l'] = $this->request->post['novaposhta_dimensions_l'];
        }
        else {
            $data['novaposhta_dimensions_l'] = $this->config->get('novaposhta_dimensions_l');
        }

        if (isset($this->request->post['novaposhta_dimensions_h'])) {
            $data['novaposhta_dimensions_h'] = $this->request->post['novaposhta_dimensions_h'];
        }
        else {
            $data['novaposhta_dimensions_h'] = $this->config->get('novaposhta_dimensions_h');
        }

        if (isset($this->request->post['novaposhta_calculate_volume'])) {
            $data['novaposhta_calculate_volume'] = $this->request->post['novaposhta_calculate_volume'];
        }
        else {
            $data['novaposhta_calculate_volume'] = $this->config->get('novaposhta_calculate_volume');
        }

        if (isset($this->request->post['novaposhta_use_parameters'])) {
            $data['novaposhta_use_parameters'] = $this->request->post['novaposhta_use_parameters'];
        }
        else {
            $data['novaposhta_use_parameters'] = $this->config->get('novaposhta_use_parameters');
        }

        if (isset($this->request->post['novaposhta_key_cron'])) {
            $data['novaposhta_key_cron'] = $this->request->post['novaposhta_key_cron'];
        }
        else {
            $data['novaposhta_key_cron'] = $this->config->get('novaposhta_key_cron');
        }

        if (isset($this->request->post['novaposhta_tracking_statuses'])) {
            $data['novaposhta_tracking_statuses'] = $this->request->post['novaposhta_tracking_statuses'];
        }
        else {
            $data['novaposhta_tracking_statuses'] = $this->config->get('novaposhta_tracking_statuses');
        }

        if (isset($this->request->post['novaposhta_settings_tracking_statuses'])) {
            $data['novaposhta_settings_tracking_statuses'] = $this->request->post['novaposhta_settings_tracking_statuses'];
        }
        else {
            $data['novaposhta_settings_tracking_statuses'] = $this->config->get('novaposhta_settings_tracking_statuses');
        }

        $references = $this->novaposhta->getReferences();

        if (isset($references['database'])) {
            $data['database'] = $references['database'];
        }
        else {
            $data['database'] = array();
        }

        if (isset($references['senders'])) {
            $data['senders'] = $references['senders'];
        }
        else {
            $data['senders'] = array();
        }

        if (isset($references['sender_contact_persons']) && isset($references['sender_contact_persons'][$data['novaposhta_sender']])) {
            $data['sender_contact_persons'] = $references['sender_contact_persons'][$data['novaposhta_sender']];
        }
        else {
            $data['sender_contact_persons'] = array();
        }

        if (isset($references['sender_addresses']) && $data['novaposhta_sender_city']) {
            $data['sender_addresses'] = array_merge($this->novaposhta->getSenderAddressesByCityRef($data['novaposhta_sender'], $data['novaposhta_sender_city']), $this->novaposhta->getWarehousesByCityRef($data['novaposhta_sender_city']));
        }
        else {
            $data['sender_addresses'] = array();
        }

        if (isset($references['warehouse_types'])) {
            $data['warehouse_types'] = $references['warehouse_types'];
        }
        else {
            $data['warehouse_types'] = array();
        }

        if (isset($references['document_statuses'])) {
            $data['document_statuses'] = $references['document_statuses'];
        }
        else {
            $data['document_statuses'] = array();
        }

        $data['cron_path'] = '/usr/bin/wget -t 1 -O -';
        $data['cron_update_areas'] = $data['cron_path'] . ' \'http://' . $_SERVER['HTTP_HOST'] . '/index.php?route=module/novaposhta_cron/update&type=areas&key=' . $data['novaposhta_key_cron'] . '\'';
        $data['cron_update_cities'] = $data['cron_path'] . ' \'http://' . $_SERVER['HTTP_HOST'] . '/index.php?route=module/novaposhta_cron/update&type=cities&key=' . $data['novaposhta_key_cron'] . '\'';
        $data['cron_update_warehouses'] = $data['cron_path'] . ' \'http://' . $_SERVER['HTTP_HOST'] . '/index.php?route=module/novaposhta_cron/update&type=warehouses&key=' . $data['novaposhta_key_cron'] . '\'';
        $data['cron_update_references'] = $data['cron_path'] . ' \'http://' . $_SERVER['HTTP_HOST'] . '/index.php?route=module/novaposhta_cron/update&type=references&key=' . $data['novaposhta_key_cron'] . '\'';
        $data['cron_shipments_tracking'] = $data['cron_path'] . ' \'http://' . $_SERVER['HTTP_HOST'] . '/index.php?route=module/novaposhta_cron/shipmentsTracking&key=' . $data['novaposhta_key_cron'] . '\'';
        $data['v'] = $this->version;
        $data['license'] = $this->license;
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
       $data['ocmax'] = $this->ocmax->index($this->license, 'novaposhta');

        $this->response->setOutput($this->load->view('shipping/novaposhta.tpl', $data));
    }

    public function getEIList()
    {
        $this->load->language('shipping/novaposhta');
        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->get['filter_ei_number'])) {
            $filter_ei_number = $this->request->get['filter_ei_number'];
        }
        else {
            $filter_ei_number = NULL;
        }

        if (isset($this->request->get['filter_recipient'])) {
            $filter_recipient = $this->request->get['filter_recipient'];
        }
        else {
            $filter_recipient = NULL;
        }

        if (isset($this->request->get['filter_shipment_date'])) {
            $filter_shipment_date = $this->request->get['filter_shipment_date'];
        }
        else {
            $filter_shipment_date = date('d.m.Y');
        }

        if (isset($this->request->get['page'])) {
        }
        else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_ei_number'])) {
            $url .= '&filter_ei_number=' . $this->request->get['filter_ei_number'];
        }

        if (isset($this->request->get['filter_recipient'])) {
            $url .= '&filter_recipient=' . $this->request->get['filter_recipient'];
        }

        if (isset($this->request->get['filter_shipment_date'])) {
            $url .= '&filter_shipment_date=' . $this->request->get['filter_shipment_date'];
        }

        $data['pdf'] = $this->url->link('shipping/novaposhta/doPDF', 'token=' . $this->session->data['token'], 'SSL');
        $data['print'] = 'https://my.novaposhta.ua/orders';
        $data['edit'] = $this->url->link('shipping/novaposhta/getForm', 'token=' . $this->session->data['token'], 'SSL');
        $data['back_to_orders'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL');
        $data['order'] = $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=', 'SSL');
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'));
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_orders'), 'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL'));
        $data['token'] = $this->session->data['token'];

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            $data['ei'] = $this->session->data['ei'];
            unset($this->session->data['success']);
            unset($this->session->data['ei']);
        }
        else {
            $data['success'] = '';
            $data['ei'] = '';
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_create_ei'] = $this->language->get('button_create_ei');
        $data['button_back_to_orders'] = $this->language->get('button_back_to_orders');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_pdf'] = $this->language->get('button_pdf');
        $data['button_print'] = $this->language->get('button_print');
        $data['button_ei'] = $this->language->get('button_ei');
        $data['button_mark'] = $this->language->get('button_mark');
        $data['button_mark_zebra'] = $this->language->get('button_mark_zebra');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['column_ei_number'] = $this->language->get('column_ei_number');
        $data['column_order_number'] = $this->language->get('column_order_number');
        $data['column_estimated_delivery_date'] = $this->language->get('column_estimated_delivery_date');
        $data['column_recipient'] = $this->language->get('column_recipient');
        $data['column_address'] = $this->language->get('column_address');
        $data['column_announced_price'] = $this->language->get('column_announced_price');
        $data['column_shipping_cost'] = $this->language->get('column_shipping_cost');
        $data['column_state'] = $this->language->get('column_state');
        $data['column_action'] = $this->language->get('column_action');
        $data['entry_ei_number'] = $this->language->get('entry_ei_number');
        $data['entry_recipient'] = $this->language->get('entry_recipient');
        $data['entry_shipment_date'] = $this->language->get('entry_shipment_date');
        $documents = $this->novaposhta->getEIList($filter_shipment_date);
        $documents_total = count($documents);
        $pagination = new Pagination();
        $pagination->total = $documents_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('shipping/novaposhta/getEIList', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), $documents_total ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ($documents_total - $this->config->get('config_limit_admin')) < (($page - 1) * $this->config->get('config_limit_admin')) ? $documents_total : (($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin'), $documents_total, ceil($documents_total / $this->config->get('config_limit_admin')));

        if ($documents) {
            $documents = array_slice($documents, ($page - 1) * $this->config->get('config_limit_admin'), $this->config->get('config_limit_admin'));
            $this->load->model('shipping/novaposhta');

            foreach ($documents as $k => $document) {
                $documents[$k]['Cost'] = $this->currency->format($document['Cost'], 'UAH', 1);
                $documents[$k]['CostOnSite'] = $this->currency->format($document['CostOnSite'], 'UAH', 1);
                $order = $this->model_shipping_novaposhta->getOrderByDocumentNumber($document['IntDocNumber']);

                if ($order) {
                    $documents[$k]['order_id'] = $order['order_id'];
                }
            }
        }

        $data['documents'] = $documents;
        $data['filter_ei_number'] = $filter_ei_number;
        $data['filter_recipient'] = $filter_recipient;
        $data['filter_shipment_date'] = $filter_shipment_date;
        $data['v'] = $this->version;
        $data['api_key'] = $this->novaposhta->key_api;
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('shipping/novaposhta_ei_list.tpl', $data));
    }

    public function getForm()
    {
        $this->load->language('shipping/novaposhta');
        $this->load->model('shipping/novaposhta');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $json = array();

            if ($this->validate()) {
                $json['success'] = $this->request->post;
            }
            else {
                $json = $this->error;
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return NULL;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        }
        else {
            $data['error_warning'] = '';
        }

        if (isset($this->request->get['ei_ref'])) {
            $ei_ref = $this->request->get['ei_ref'];
        }
        else {
            $ei_ref = '';
        }

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
            $this->load->model('sale/order');
            $order_info = $this->model_sale_order->getOrder($order_id);
            $order_totals = $this->model_sale_order->getOrderTotals($order_id);
            $products = $this->model_shipping_novaposhta->getOrderProducts($order_id);

            if (!$order_info) {
                $data['error_warning'] = $this->language->get('error_get_order');
            }
            else if (isset($order_info['novaposhta_ei_ref']) && $order_info['novaposhta_ei_ref']) {
                $ei_ref = $order_info['novaposhta_ei_ref'];
            }
        }
        else {
            $order_id = 0;
            $order_info = false;
        }

        if ($ei_ref) {
            $ei = $this->novaposhta->getEI($ei_ref);

            if (!$ei) {
                $data['error_warning'] = $this->novaposhta->error;
                $data['error_warning'][] = $this->language->get('error_get_ei');
            }
        }
        else {
            $ei = false;
        }

        $this->document->setTitle($this->language->get('heading_title'));
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'));
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_orders'), 'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL'));
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_order'), 'href' => $this->url->link('sale/order/info&order_id=' . $order_id, 'token=' . $this->session->data['token'], 'SSL'));
        $data['ei_list'] = $this->url->link('shipping/novaposhta/getEIList', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'], 'SSL');
        $data['token'] = $this->session->data['token'];
        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_options_seat'] = $this->language->get('heading_options_seat');
        $data['button_ei_list'] = $this->language->get('button_ei_list');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_save_ei'] = $this->language->get('button_save_ei');
        $data['button_options_seat'] = $this->language->get('button_options_seat');
        $data['button_add_seat'] = $this->language->get('button_add_seat');
        $data['column_number_order'] = $this->language->get('column_number_order');
        $data['column_volume'] = $this->language->get('column_volume');
        $data['column_width'] = $this->language->get('column_width');
        $data['column_length'] = $this->language->get('column_length');
        $data['column_height'] = $this->language->get('column_height');
        $data['column_actual_weight'] = $this->language->get('column_actual_weight');
        $data['column_volume_weight'] = $this->language->get('column_volume_weight');
        $data['column_action'] = $this->language->get('column_action');
        $data['text_form'] = $ei ? $this->language->get('text_form_edit') : $this->language->get('text_form_create');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_sender'] = $this->language->get('text_sender');
        $data['text_recipient'] = $this->language->get('text_recipient');
        $data['text_shipment'] = $this->language->get('text_shipment');
        $data['text_payment'] = $this->language->get('text_payment');
        $data['text_additionally'] = $this->language->get('text_additionally');
        $data['text_no_backward_delivery'] = $this->language->get('text_no_backward_delivery');
        $data['text_grn'] = $this->language->get('text_grn');
        $data['text_cubic_meter'] = $this->language->get('text_cubic_meter');
        $data['text_cm'] = $this->language->get('text_cm');
        $data['text_kg'] = $this->language->get('text_kg');
        $data['text_pc'] = $this->language->get('text_pc');
        $data['text_or'] = $this->language->get('text_or');
        $data['entry_sender'] = $this->language->get('entry_sender');
        $data['entry_recipient'] = $this->language->get('entry_recipient');
        $data['entry_third_person'] = $this->language->get('entry_third_person');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_address'] = $this->language->get('entry_address');
        $data['entry_contact_person'] = $this->language->get('entry_contact_person');
        $data['entry_phone'] = $this->language->get('entry_phone');
        $data['entry_cargo_type'] = $this->language->get('entry_cargo_type');
        $data['entry_width'] = $this->language->get('entry_width');
        $data['entry_length'] = $this->language->get('entry_length');
        $data['entry_height'] = $this->language->get('entry_height');
        $data['entry_weight'] = $this->language->get('entry_weight');
        $data['entry_volume_weight'] = $this->language->get('entry_volume_weight');
        $data['entry_volume_general'] = $this->language->get('entry_volume_general');
        $data['entry_seats_amount'] = $this->language->get('entry_seats_amount');
        $data['entry_announced_price'] = $this->language->get('entry_announced_price');
        $data['entry_cargo_description'] = $this->language->get('entry_cargo_description');
        $data['entry_payer'] = $this->language->get('entry_payer');
        $data['entry_payment_type'] = $this->language->get('entry_payment_type');
        $data['entry_backward_delivery'] = $this->language->get('entry_backward_delivery');
        $data['entry_backward_delivery_total'] = $this->language->get('entry_backward_delivery_total');
        $data['entry_backward_delivery_payer'] = $this->language->get('entry_backward_delivery_payer');
        $data['entry_shipment_date'] = $this->language->get('entry_shipment_date');
        $data['entry_service_type'] = $this->language->get('entry_service_type');
        $data['entry_sales_order_number'] = $this->language->get('entry_sales_order_number');
        $data['entry_payment_control'] = $this->language->get('entry_payment_control');
        $data['entry_additional_information'] = $this->language->get('entry_additional_information');

        if ($ei) {
            $data['sender'] = $ei['SenderRef'];
            $data['sender_contact_person'] = $ei['ContactSenderRef'];
            $data['sender_contact_person_phone'] = $ei['SendersPhone'];
            $data['sender_city'] = $ei['CitySenderRef'];
            $data['sender_city_name'] = $ei['CitySender'];
            $data['sender_address'] = $ei['SenderAddressRef'];
            $data['recipient'] = $ei['Recipient'];
            $data['recipient_contact_person'] = $ei['ContactRecipient'];
            $data['recipient_contact_person_phone'] = $ei['RecipientsPhone'];
            $data['recipient_city'] = $ei['CityRecipient'];
            $data['recipient_address'] = preg_replace('/"([^"]+)"/', '«$1»', $ei['RecipientAddress']);
            $data['cargo'] = $ei['CargoTypeRef'];
            $data['width'] = isset($ei['OptionsSeat'][0]) ? $ei['OptionsSeat'][0]['volumetricWidth'] : '';
            $data['length'] = isset($ei['OptionsSeat'][0]) ? $ei['OptionsSeat'][0]['volumetricLength'] : '';
            $data['height'] = isset($ei['OptionsSeat'][0]) ? $ei['OptionsSeat'][0]['volumetricHeight'] : '';
            $data['weight'] = isset($ei['OptionsSeat'][0]) ? $ei['OptionsSeat'][0]['weight'] : $ei['Weight'];
            $data['volume_general'] = isset($ei['OptionsSeat'][0]) ? $ei['OptionsSeat'][0]['volumetricVolume'] : $ei['VolumeGeneral'];
            $data['volume_weight'] = isset($ei['OptionsSeat'][0]) ? $ei['OptionsSeat'][0]['volumetricWeight'] : $ei['VolumeWeight'];
            $data['seats_amount'] = $ei['SeatsAmount'];
            $data['announced_price'] = $ei['Cost'];
            $data['cargo_description'] = $ei['Description'];
            $data['payer'] = $ei['PayerTypeRef'];
            $data['payment_type'] = $ei['PaymentMethodRef'];
            $data['cod'] = isset($ei['BackwardDeliveryData'][0]) ? $ei['BackwardDeliveryData'][0]['CargoTypeRef'] : false;
            $data['cod_payer'] = isset($ei['BackwardDeliveryData'][0]) ? $ei['BackwardDeliveryData'][0]['PayerTypeRef'] : 'Recipient';
            $data['backward_delivery_total'] = isset($ei['BackwardDeliveryData'][0]) ? preg_replace('/[^0-9]/', '', $ei['BackwardDeliveryData'][0]['RedeliveryString']) : '';
            $data['shipment_date'] = date('d.m.Y', strtotime($ei['DateTime']));
            $data['sales_order_number'] = $ei['InfoRegClientBarcodes'];
            $data['payment_control'] = $ei['AfterpaymentOnGoodsCost'];
            $data['additional_information'] = $ei['AdditionalInformation'];
        }
        else if ($order_info) {
            $find_order = array('{order_id}', '{invoice}', '{store_name}', '{store_url}', '{name}', '{shipping_name}', '{date_added}', '{date_modified}', '{customer}', '{firstname}', '{lastname}', '{email}', '{telephone}', '{fax}', '{payment_firstname}', '{payment_lastname}', '{payment_company}', '{payment_address_1}', '{payment_address_2}', '{payment_postcode}', '{payment_city}', '{payment_zone}', '{payment_country}', '{shipping_firstname}', '{shipping_lastname}', '{shipping_company}', '{shipping_address_1}','{shipping_km}', '{shipping_address_2}', '{shipping_postcode}', '{shipping_city}', '{shipping_zone}', '{shipping_country}');
            $replace_order = array('order_id' => $order_info['order_id'], 'invoice' => $order_info['invoice_prefix'] . $order_info['invoice_no'], 'store_name' => $order_info['store_name'], 'store_url' => $order_info['store_url'], 'name' => $order_info['lastname'] . ' ' . $order_info['firstname'], 'shipping_name' => $order_info['shipping_lastname'] . ' ' . $order_info['shipping_firstname'], 'date_added' => $order_info['date_added'], 'date_modified' => $order_info['date_modified'], 'customer' => $order_info['customer'], 'firstname' => $order_info['firstname'], 'lastname' => $order_info['lastname'], 'email' => $order_info['email'], 'telephone' => $order_info['telephone'], 'fax' => $order_info['fax'], 'payment_firstname' => $order_info['payment_firstname'], 'payment_lastname' => $order_info['payment_lastname'], 'payment_company' => $order_info['payment_company'], 'payment_address_1' => $order_info['payment_address_1'], 'payment_address_2' => $order_info['payment_address_2'], 'payment_postcode' => $order_info['payment_postcode'], 'payment_city' => $order_info['payment_city'], 'payment_zone' => $order_info['payment_zone'], 'payment_country' => $order_info['payment_country'], 'shipping_firstname' => $order_info['shipping_firstname'], 'shipping_lastname' => $order_info['shipping_lastname'], 'shipping_company' => $order_info['shipping_company'], 'shipping_address_1' => $order_info['shipping_address_1'], 'shipping_km' => $order_info['shipping_km'], 'shipping_address_2' => $order_info['shipping_address_2'], 'shipping_postcode' => $order_info['shipping_postcode'], 'shipping_city' => $order_info['shipping_city'], 'shipping_zone' => $order_info['shipping_zone'], 'shipping_country' => $order_info['shipping_country']);
            $data['sender'] = $this->config->get('novaposhta_sender');
            $data['sender_contact_person'] = $this->config->get('novaposhta_sender_contact_person');
            $data['sender_contact_person_phone'] = $ei['SendersPhone'];
            $data['sender_city'] = $this->config->get('novaposhta_sender_city');
            $data['sender_city_name'] = $this->config->get('novaposhta_sender_city_name');
            $data['sender_address'] = $this->config->get('novaposhta_sender_address');
            $data['recipient'] = trim(str_replace($find_order, $replace_order, $this->config->get('novaposhta_recipient_name')));
            $data['recipient_contact_person'] = trim(mb_convert_case(str_replace($find_order, $replace_order, $this->config->get('novaposhta_recipient_contact_person')), MB_CASE_TITLE, 'UTF-8'));
            $data['recipient_contact_person_phone'] = preg_replace('/[^0-9]/', '', trim(str_replace($find_order, $replace_order, $this->config->get('novaposhta_recipient_contact_person_phone'))));
            $data['recipient_city'] = trim(str_replace($find_order, $replace_order, $this->config->get('novaposhta_recipient_city')));
            $data['recipient_address'] = trim(str_replace($find_order, $replace_order, $this->config->get('novaposhta_recipient_address')));
            $data['cargo'] = '';
            $data['width'] = '';
            $data['length'] = '';
            $data['height'] = '';
            $data['weight'] = $this->novaposhta->getWeight($products);
            $data['volume_general'] = $this->novaposhta->getVolume($products);
            $data['volume_weight'] = $data['volume_general'] * 250;
            $data['seats_amount'] = 1;
            $data['announced_price'] = $this->getAnnouncedPrice($order_totals);
            $data['cargo_description'] = $this->config->get('novaposhta_cargo_description');

            $data['payer'] = ($this->config->get('novaposhta_free_shipping') <= $data['announced_price']) && $this->config->get('novaposhta_free_shipping') ? 'Sender' : 'Recipient';
            $data['payment_type'] = 'Cash';

            if ($this->config->get('novaposhta_payment_cod')) {
                if (isset($order_info['payment_code']) && ($order_info['payment_code'] == $this->config->get('novaposhta_payment_cod'))) {
                    $data['cod'] = 'Money';
                }
                else {
                    $this->load->language('payment/' . $this->config->get('novaposhta_payment_cod'));
                    $payment_method = $this->language->get('heading_title');
                    $data['cod'] = $order_info['payment_method'] == $payment_method ? 'Money' : false;
                }
            }
            else {
                $data['cod'] = false;
            }

            $data['cod_payer'] = $this->config->get('cod_plus_free') && ($this->config->get('cod_plus_free') <= $data['announced_price']) ? 'Sender' : 'Recipient';
            $data['backward_delivery_total'] = $data['announced_price'];
            $data['shipment_date'] = date('d.m.Y');
            $data['sales_order_number'] = $order_id;
            $data['payment_control'] = '';
            $data['additional_information'] = '';
            $template = explode('|', $this->config->get('novaposhta_additional_information'));

            if ($template[0]) {
                $data .= 'additional_information';
            }

            if (isset($template[1])) {
                $find_product = array('{product_name}', '{model}', '{sku}', '{quantity}');

                foreach ($products as $k => $product) {
                    $replace_product = array('name' => $product['name'], 'model' => $product['model'], 'sku' => $product['sku'], 'quantity' => $product['quantity']);
                    $data .= 'additional_information';
                }
            }
        }
        else {
            $data['sender'] = $this->config->get('novaposhta_sender');
            $data['sender_contact_person'] = $this->config->get('novaposhta_sender_contact_person');
            $data['sender_city'] = $this->config->get('novaposhta_sender_city');
            $data['sender_city_name'] = $this->config->get('novaposhta_sender_city_name');
            $data['sender_address'] = $this->config->get('novaposhta_sender_address');
            $data['recipient'] = '';
            $data['recipient_contact_person'] = '';
            $data['recipient_contact_person_phone'] = '';
            $data['recipient_city'] = '';
            $data['recipient_address'] = '';
            $data['cargo'] = '';
            $data['width'] = '';
            $data['length'] = '';
            $data['height'] = '';
            $data['weight'] = '';
            $data['volume_general'] = '';
            $data['volume_weight'] = '';
            $data['seats_amount'] = '';
            $data['announced_price'] = '';
            $data['cargo_description'] = $this->config->get('novaposhta_cargo_description');
            $data['payer'] = 'Recipient';
            $data['payment_type'] = 'Cash';
            $data['cod'] = false;
            $data['cod_payer'] = 'Recipient';
            $data['backward_delivery_total'] = $data['announced_price'];
            $data['shipment_date'] = date('d.m.Y');
            $data['sales_order_number'] = '';
            $data['payment_control'] = '';
            $data['additional_information'] = '';
        }

        $data['references'] = $this->novaposhta->getReferences();

        if (isset($data['references']['senders'])) {
            $data['senders'] = $data['references']['senders'];
        }
        else {
            $data['senders'] = array();
        }

        if (isset($data['references']['sender_contact_persons']) && isset($data['references']['sender_contact_persons'][$data['sender']])) {
            $data['sender_contact_persons'] = $data['references']['sender_contact_persons'][$data['sender']];
        }
        else {
            $data['sender_contact_persons'] = array();
        }

        if (isset($data['references']['sender_addresses']) && $data['sender_city']) {
            $data['sender_addresses'] = array_merge($this->novaposhta->getSenderAddressesByCityRef($data['sender'], $data['sender_city']), $this->novaposhta->getWarehousesByCityRef($data['sender_city']));
        }
        else {
            $data['sender_addresses'] = array();
        }

        $data['order_id'] = $order_id;
        $data['ei_ref'] = $ei_ref;
        $data['v'] = $this->version;
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('shipping/novaposhta_ei_form.tpl', $data));
    }

    public function saveEI()
    {
        $this->load->language('shipping/novaposhta');
        $this->load->model('shipping/novaposhta');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $recipient_city = $this->novaposhta->getCityRef($this->request->post['recipient_city']);
            $result = $this->novaposhta->getCounterparties('Recipient', $this->request->post['recipient']);
            $recipient_data = array_shift($result);

            if ($recipient_data) {
            }
            else {
                $name = explode(' ', preg_replace('/ {2,}/', ' ', trim($this->request->post['recipient'])));

                if (!isset($name[1])) {
                    $name[1] = $name[0];
                    $name[0] = 'ПП';
                }

                $ownership_form = $this->getOwnreshipForm($name[0]);
                $properties_r = array('CityRef' => $recipient_city, 'FirstName' => $name[1], 'CounterpartyType' => 'Organization', 'CounterpartyProperty' => 'Recipient', 'OwnershipForm' => $ownership_form['Ref']);
                $recipient_data = $this->novaposhta->saveCounterparties($properties_r);
                $recipient = ($recipient_data ? $recipient_data['Ref'] : $recipient_data);
            }

            $recipient_contact_person = '';
            $recipient_contact_person_phone = '';

            if ($recipient) {
                $r_full_name = preg_replace('/ {2,}/', ' ', mb_convert_case(trim($this->request->post['recipient_contact_person']), MB_CASE_TITLE, 'UTF-8'));
                $r_full_name_parts = explode(' ', $r_full_name);
                $recipient_contact_persons = $this->novaposhta->getContactPerson($recipient, $r_full_name_parts[0] . ' ' . $r_full_name_parts[1]);

                if ($recipient_contact_persons) {
                    foreach ($recipient_contact_persons as $contact_person) {
                        if (isset($r_full_name_parts[2]) && ($contact_person['MiddleName'] != $r_full_name_parts[2])) {
                            $contact_person['CounterpartyRef'] = $recipient;
                            $contact_person['MiddleName'] = $r_full_name_parts[2];
                            $contact_person['Phone'] = $contact_person['Phones'];
                            $this->novaposhta->updateContactPerson($contact_person);
                        }

                        $recipient_contact_person_phone = $contact_person['Phones'];
                        break;
                    }
                }

                if (!$recipient_contact_person) {
                    $properties_c_p = array('CounterpartyRef' => $recipient, 'LastName' => $r_full_name_parts[0], 'FirstName' => $r_full_name_parts[1], 'MiddleName' => isset($r_full_name_parts[2]) ? $r_full_name_parts[2] : '', 'Phone' => $this->request->post['recipient_contact_person_phone'], 'Email' => '');
                    $result = $this->novaposhta->saveContactPerson($properties_c_p);

                    if ($result) {
                        $recipient_contact_person_phone = $result['Phones'];
                    }
                }
            }

            $recipient_address = $this->novaposhta->getWarehouseRef($this->request->post['recipient_address']);

            if (!$recipient_address) {
                $recipient_address_type = 'Doors';
            }
            else {
                $recipient_address_type = 'Warehouse';
            }

            $sender_address_type = ($this->novaposhta->getWarehouseName($this->request->post['sender_address']) ? 'Warehouse' : 'Doors');
            $references = $this->novaposhta->getReferences();
            $properties_ei = array('Sender' => $this->request->post['sender'], 'ContactSender' => $this->request->post['sender_contact_person'], 'SendersPhone' => $references['sender_contact_persons'][$this->request->post['sender']][$this->request->post['sender_contact_person']]['Phones'], 'CitySender' => $this->novaposhta->getCityRef($this->request->post['sender_city_name']), 'SenderAddress' => $this->request->post['sender_address'], 'Recipient' => $recipient, 'ContactRecipient' => $recipient_contact_person, 'RecipientsPhone' => $recipient_contact_person_phone, 'CityRecipient' => $recipient_city, 'CargoType' => $this->request->post['cargo_type'], 'SeatsAmount' => $this->request->post['seats_amount'], 'Cost' => $this->request->post['announced_price'], 'Description' => $this->request->post['cargo_description'], 'PayerType' => $this->request->post['payer'], 'PaymentMethod' => $this->request->post['payment_type'], 'DateTime' => $this->request->post['shipment_date'], 'ServiceType' => $sender_address_type . $recipient_address_type);

            if ($recipient_address_type == 'Doors') {
                $recipient_address = str_ireplace($this->request->post['recipient_city'] . ', ', '', $this->request->post['recipient_address']);
                $doors_address = $this->parseAddress($recipient_address);
                $properties_ei['NewAddress'] = 1;
                $properties_ei['RecipientType'] = isset($recipient_data['CounterpartyType']) ? $recipient_data['CounterpartyType'] : 'PrivatPerson';
                $properties_ei['RecipientCityName'] = $this->request->post['recipient_city'];
                $properties_ei['RecipientAddressName'] = $doors_address['street'];
                $properties_ei['RecipientHouse'] = $doors_address['house'];

                if ($doors_address['flat']) {
                    $properties_ei['RecipientFlat'] = $doors_address['flat'];
                }
            }
            else {
                $properties_ei['RecipientAddress'] = $recipient_address;
            }

            if ($recipient_data['CounterpartyType'] == 'Organization') {
                $properties_ei['OwnershipForm'] = isset($recipient_data['OwnershipForm']) ? $recipient_data['OwnershipForm'] : $recipient_data['OwnershipFormRef'];
            }

            if (isset($this->request->post['third_person'])) {
                $properties_ei['ThirdPerson'] = $this->request->post['third_person'];
            }

            if (preg_match('/поштомат|почтомат/ui', $this->request->post['recipient_address']) && (($this->request->post['cargo_type'] == 'Cargo') || ($this->request->post['cargo_type'] == 'Pallet'))) {
                $properties_ei['OptionsSeat'][] = array('volumetricVolume' => $this->request->post['volume_general'], 'volumetricWidth' => $this->request->post['width'], 'volumetricLength' => $this->request->post['length'], 'volumetricHeight' => $this->request->post['height'], 'weight' => $this->request->post['weight'], 'volumetricWeight' => $this->request->post['volume_weight']);
            }
            else {
                if (isset($this->request->post['weight'])) {
                    $properties_ei['Weight'] = $this->request->post['weight'];
                }

                if (isset($this->request->post['volume_weight'])) {
                    $properties_ei['VolumeWeight'] = $this->request->post['volume_weight'];
                }

                if (isset($this->request->post['volume_general'])) {
                    $properties_ei['VolumeGeneral'] = $this->request->post['volume_general'];
                }
            }

            if (!empty($this->request->post['backward_delivery'])) {
                switch ($this->request->post['backward_delivery']) {
                    case 'Money':
                        $properties_ei['BackwardDeliveryData'][] = array('CargoType' => $this->request->post['backward_delivery'], 'PayerType' => $this->request->post['backward_delivery_payer'], 'RedeliveryString' => $this->request->post['backward_delivery_total']);
                }
            }

            if (!empty($this->request->post['sales_order_number'])) {
                $properties_ei['InfoRegClientBarcodes'] = $this->request->post['sales_order_number'];
            }

            if (!empty($this->request->post['payment_control'])) {
                $properties_ei['AfterpaymentOnGoodsCost'] = $this->request->post['payment_control'];
            }

            if (!empty($this->request->post['additional_information'])) {
                $properties_ei['AdditionalInformation'] = $this->request->post['additional_information'];
            }

            if ($this->request->get['ei_ref']) {
                $properties_ei['Ref'] = $this->request->get['ei_ref'];
            }

            $data = $this->novaposhta->saveEI($properties_ei);

            if (isset($data[0])) {
                if (isset($this->request->get['order_id']) && $this->request->get['order_id']) {
                    $this->model_shipping_novaposhta->addEIToOrder($this->request->get['order_id'], $data[0]);
                }
            }
            else {
                $this->error['warning'] = $this->novaposhta->error;
                $this->error['warning'][] = $this->language->get('error_ei_save');
            }
        }

        if ($this->error) {
            $json = $this->error;
        }
        else {
            $this->session->data['success'] = $this->language->get('text_ei_success_save');
            $this->session->data['ei'] = $data[0]['IntDocNumber'];
            $json['redirect'] = true;
            $json['shipment_date'] = $this->request->post['shipment_date'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function deleteEI()
    {
        $json = array();
        $this->load->language('shipping/novaposhta');
        if ($this->validate() && isset($this->request->post['refs'])) {
            $data = $this->novaposhta->deleteEI($this->request->post['refs']);
            $this->load->model('shipping/novaposhta');
            $this->model_shipping_novaposhta->deleteEIFromOrder($this->request->post['refs']);

            if ($data) {
                $json['success']['refs'] = $data;
                $json['success']['text'] = $this->language->get('text_ei_success_delete');
            }
            else {
                $json['warning'] = $this->novaposhta->error;
                $json['warning'][] = $this->language->get('error_ei_delete');
            }
        }
        else {
            $json['warning'][] = $this->error['warning'];
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function doPDF()
    {
        $documents = array();

        if (isset($this->request->post['selected'])) {
            $documents = $this->request->post['selected'];
        }
        else if (isset($this->request->get['order'])) {
            $documents[] = $this->request->get['order'];
        }

        if (isset($this->request->get['type'])) {
        }
        else {
            $type = 'printDocument';
        }

        $name = implode('-', $documents);
        $data = $this->novaposhta->printDocument($documents, $type, 'pdf');
        $this->response->addheader('Pragma: public');
        $this->response->addheader('Expires: 0');
        $this->response->addheader('Content-Description: File Transfer');
        $this->response->addheader('Content-Type: application/octet-stream');
        $this->response->addheader('Content-Disposition: attachment; filename=Nova_Poshta_' . $name . '_' . date('Y-m-d_H-i-s') . '.pdf');
        $this->response->addheader('Content-Transfer-Encoding: binary');
        $this->response->setOutput($data);
    }

    public function update()
    {
        $this->load->language('shipping/novaposhta');

        if (!$this->validate()) {
            $json['error'] = $this->error['warning'];
        }
        else {
            if (isset($this->request->get['type'])) {
            }

            $amount = $this->novaposhta->update($type);

            if ($amount) {
                $json['success'] = $this->language->get('text_update_success');
                $json['amount'] = $amount;
            }
            else if ($amount === false) {
                $json['error'] = $this->language->get('error_update');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getNPData()
    {
        $json = array();

        if (isset($this->request->post['action'])) {
            $action = $this->request->post['action'];
        }
        else {
            $action = '';
        }

        if (isset($this->request->post['sender'])) {
            $sender = $this->request->post['sender'];
        }
        else {
            $sender = '';
        }

        if (isset($this->request->post['city'])) {
        }
        else {
            $city = '';
        }

        if (isset($this->request->post['address'])) {
            $address = $this->request->post['address'];
        }
        else {
            $address = '';
        }

        switch ($action) {
            case 'getAddress':
                $json = array_merge($this->novaposhta->getSenderAddressesByCityRef($sender, $city), $this->novaposhta->getWarehousesByCityRef($city));
                break;
            case 'getContactPerson':
                $sender_contact_persons = $this->novaposhta->getReferences('sender_contact_persons');

                if (isset($sender_contact_persons[$sender])) {
                    $json = $sender_contact_persons[$sender];
                }

                break;
            case 'getAddressType':
                $json['address_type'] = $this->novaposhta->getWarehouseName($address) ? 'Warehouse' : 'Doors';
                break;
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function generateKey()
    {
        $data['code'] = '';
        $length = 36;
        $characters = array('1234567890', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz');

        while ($length--) {
            $characters_type = mt_rand(0, count($characters) - 1);
            $data .= 'code';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($data));
    }

    public function autocomplete()
    {
        $json = array();

        if (isset($this->request->post['city'])) {
            $json = $this->novaposhta->getCitiesWithAreasByCityName($this->request->post['city']);
        }
        else if (isset($this->request->post['cargo_description']) && $this->request->post['cargo_description']) {
            $limit = 5;
            $descriptions = $this->novaposhta->getReferences('cargo_description');

            foreach ($descriptions as $description) {
                if (preg_match('/^(' . $this->request->post['cargo_description'] . ').+/iu', $description[$this->novaposhta->description_field])) {
                    --$limit;
                    $json[] = array('Ref' => $description[$this->novaposhta->description_field], 'Description' => $description[$this->novaposhta->description_field]);
                }

                break;
            }
        }
        else if (isset($this->request->post['recipient_name'])) {
            $recipients = $this->novaposhta->getCounterparties('Recipient', $this->request->post['recipient_name']);

            if ($recipients) {
                $recipients = array_slice($recipients, 0, 5);

                foreach ($recipients as $k => $recipient) {
                    $recipients[$k]['AllDescription'] = $recipient['OwnershipFormDescription'] . ' ' . $recipient['Description'];

                    if ($recipient['CityDescription']) {
                        $recipients[$k] .= 'AllDescription';
                    }
                }

                $json = $recipients;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    private function getAnnouncedPrice($totals)
    {
        $announced_price = 0;

        foreach ($totals as $total) {
            switch ($total['code']) {
                case 'total':
                    $announced_price += $total['value'];
                    break;
                case 'shipping':
                    $announced_price -= $total['value'];
                    break;
                case 'cod_plus_total':
                    $announced_price -= $total['value'];
            }
        }

        $currency_value = $this->currency->getValue('UAH');

        if ($currency_value != 1) {
            $announced_price *= $currency_value;
        }

        return round($announced_price);
    }

    private function getOwnreshipForm($name)
    {
        $ownership_forms = $this->novaposhta->getReferences('ownership_forms');
        $data = $ownership_forms[0];

        foreach ($ownership_forms as $ownership_form) {
            $data = $ownership_form;
            break;
        }

        return $data;
    }

    private function parseAddress($address)
    {
        $data = array();
        $address = explode(',', preg_replace('/\\b(вулиця|вул|улица|ул|будинок|буд|дом|д|квартира|кв)\\b\\.*/ui', '', $address));
        $data['street'] = isset($address[0]) ? trim($address[0]) : '';
        $data['house'] = isset($address[1]) ? trim($address[1]) : '';
        $data['flat'] = isset($address[2]) ? trim($address[2]) : '';
        return $data;
    }

    private function validate()
    {
        $array_matches = array();

        if (!$this->user->hasPermission('modify', 'shipping/novaposhta')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->license) {
            $this->load->language('tool/ocmax');
            $this->error['warning'] = $this->language->get('error_activate');
        }

        if (isset($this->request->post['sender'])) {
            $senders = $this->novaposhta->getReferences('senders');

            if (!array_key_exists($this->request->post['sender'], $senders)) {
                $this->error['errors']['sender'] = $this->language->get('error_sender');
            }
        }

        if (isset($this->request->post['sender_contact_person'])) {
            $sender_contact_persons = $this->novaposhta->getReferences('sender_contact_persons');

            if (isset($this->request->get['sender'])) {
                $sender = $this->request->get['sender'];
            }
            else {
                $sender = '';
            }

            if (isset($sender_contact_persons[$sender]) && !array_key_exists($this->request->post['sender_contact_person'], $sender_contact_persons[$sender])) {
                $this->error['errors']['sender_contact_person'] = $this->language->get('error_sender_contact_person');
            }
        }

        if (isset($this->request->post['sender_city_name']) && !$this->novaposhta->getCityRef($this->request->post['sender_city_name'])) {
            $this->error['errors']['sender_city_name'] = $this->language->get('error_city');
        }

        if (isset($this->request->post['sender_address'])) {
            if (isset($this->request->get['sender'])) {
                $sender = $this->request->get['sender'];
            }
            else if (isset($this->request->post['sender'])) {
                $sender = $this->request->post['sender'];
            }
            else {
                $sender = '';
            }

            if (isset($this->request->get['filter'])) {
                $filter = $this->request->get['filter'];
            }
            else if (isset($this->request->post['sender_city_name'])) {
                $filter = $this->novaposhta->getCityRef($this->request->post['sender_city_name']);
            }
            else {
                $filter = '';
            }

            $sender_addresses = $this->novaposhta->getSenderAddressesByCityRef($sender, $filter);

            if (!$this->request->post['sender_address']) {
                $this->error['errors']['sender_address'] = $this->language->get('error_empty');
            }
            else if (!$this->novaposhta->getWarehouseByCity($this->request->post['sender_address'], $filter) && !isset($sender_addresses[$this->request->post['sender_address']])) {
                $this->error['errors']['sender_address'] = $this->language->get('error_sender_address');
            }
        }

        if (isset($this->request->post['recipient'])) {
            if (!$this->request->post['recipient']) {
                $this->error['errors']['recipient'] = $this->language->get('error_empty');
            }
            else if (preg_match('/[^А-яҐґЄєIіЇїё0-9\\-\\`\'\\s]+/iu', $this->request->post['recipient'], $array_matches['recipient'])) {
                $this->error['errors']['recipient'] = $this->language->get('error_characters');
            }
        }

        if (isset($this->request->post['recipient_contact_person'])) {
            if (!preg_match('/[А-яҐґЄєIіЇїё\\-\\`\']{2,}\\s[А-яҐґЄєIіЇїё\\-\\`\']{2,}/iu', $this->request->post['recipient_contact_person'], $array_matches['recipient_contact_person'])) {
                $this->error['errors']['recipient_contact_person'] = $this->language->get('error_full_name_correct');
            }
            else if (preg_match('/[^А-яҐґЄєIіЇїё\\-\\`\'\\s]+/iu', $this->request->post['recipient_contact_person'], $array_matches['recipient_contact_person'])) {
                $this->error['errors']['recipient_contact_person'] = $this->language->get('error_characters');
            }
        }

        if (isset($this->request->post['recipient_contact_person_phone']) && !preg_match('/^(380)[0-9]{9}$/', $this->request->post['recipient_contact_person_phone'], $array_matches['recipient_contact_person_phone'])) {
            $this->error['errors']['recipient_contact_person_phone'] = $this->language->get('error_phone');
        }

        if (isset($this->request->post['recipient_city']) && !$this->novaposhta->getCityRef($this->request->post['recipient_city'])) {
            $this->error['errors']['recipient_city'] = $this->language->get('error_city');
        }

        if (isset($this->request->post['recipient_address'])) {
            if (isset($this->request->get['filter'])) {
                $filter = $this->request->get['filter'];
            }
            else if (isset($this->request->post['recipient_city'])) {
                $filter = $this->request->post['recipient_city'];
            }
            else {
                $filter = '';
            }

            if (!$this->novaposhta->getWarehouseByCity($this->request->post['recipient_address'], $filter) && preg_match('/відділення|отделение|поштомат|почтомат|склад нп/ui', $this->request->post['recipient_address'])) {
                $warehouses = $this->novaposhta->getWarehousesByCityName($filter);

                if ($warehouses) {
                    $this->error['errors']['recipient_address_list'] = $warehouses;
                    $this->error['errors']['recipient_address'] = $this->language->get('error_recipient_address');
                }
                else {
                    $this->error['errors']['recipient_address'] = $this->language->get('error_recipient_address_city');
                }
            }
        }

        if (isset($this->request->post['third_person'])) {
            $third_persons = $this->novaposhta->getReferences('third_persons');

            if (!array_key_exists($this->request->post['third_person'], $third_persons)) {
                $this->error['errors']['third_person'] = $this->language->get('error_third_person');
            }
        }

        if (isset($this->request->post['width']) && (!preg_match('/^[1-9]{1}[0-9]*$/', $this->request->post['width'], $array_matches['width']) || (35 < $this->request->post['width']))) {
            $this->error['errors']['width'] = $this->language->get('error_width');
        }

        if (isset($this->request->post['length']) && (!preg_match('/^[1-9]{1}[0-9]*$/', $this->request->post['length'], $array_matches['length']) || (61 < $this->request->post['length']))) {
            $this->error['errors']['length'] = $this->language->get('error_length');
        }

        if (isset($this->request->post['height']) && (!preg_match('/^[1-9]{1}[0-9]*$/', $this->request->post['height'], $array_matches['width']) || (37 < $this->request->post['height']))) {
            $this->error['errors']['height'] = $this->language->get('error_height');
        }

        if (isset($this->request->post['weight']) && !preg_match('/^[0-9]+(\\.|\\,)?[0-9]*$/', $this->request->post['weight'], $array_matches['total_weight'])) {
            $this->error['errors']['weight'] = $this->language->get('error_weight');
        }

        if (isset($this->request->post['volume_general']) && !preg_match('/^[0-9]+(\\.|\\,)?[0-9]*$/', $this->request->post['volume_general'], $array_matches['volume_general'])) {
            $this->error['errors']['volume_general'] = $this->language->get('error_volume');
        }

        if (isset($this->request->post['seats_amount']) && !preg_match('/^[1-9]{1}[0-9]*$/', $this->request->post['seats_amount'], $array_matches['seats_amount'])) {
            $this->error['errors']['seats_amount'] = $this->language->get('error_seats_amount');
        }

        if (isset($this->request->post['announced_price']) && !preg_match('/^[0-9]+(\\.|\\,)?[0-9]{1,2}$/', $this->request->post['announced_price'], $array_matches['announced_price'])) {
            $this->error['errors']['announced_price'] = $this->language->get('error_announced_price');
        }

        if (isset($this->request->post['cargo_description']) && (utf8_strlen($this->request->post['cargo_description']) < 3)) {
            $this->error['errors']['cargo_description'] = $this->language->get('error_cargo_description');
        }

        if (isset($this->request->post['backward_delivery_total']) && !preg_match('/^[0-9]+(\\.|\\,)?[0-9]{1,2}$/', $this->request->post['backward_delivery_total'], $array_matches['backward_delivery_total'])) {
            $this->error['errors']['backward_delivery_total'] = $this->language->get('error_backward_delivery_total');
        }

        if (isset($this->request->post['shipment_date']) && !preg_match('/(0[1-9]|1[0-9]|2[0-9]|3[01])\\.(0[1-9]|1[012])\\.(20)\\d\\d/', $this->request->post['shipment_date'], $array_matches['shipment_date'])) {
            $this->error['errors']['shipment_date'] = $this->language->get('error_date');
        }
        else if (isset($this->request->post['shipment_date']) && ($this->novaposhta->dateDiff($this->request->post['shipment_date']) < 0)) {
            $this->error['errors']['shipment_date'] = $this->language->get('error_date_past');
        }

        if (isset($this->request->post['additional_information']) && (100 < utf8_strlen($this->request->post['additional_information']))) {
            $this->error['errors']['additional_information'] = $this->language->get('error_additional_information');
        }

        return !$this->error;
    }
}


?>
