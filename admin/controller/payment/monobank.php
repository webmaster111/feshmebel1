<?php
class ControllerPaymentMonobank extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/monobank');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('monobank', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

        $data['text_store_identificator'] = $this->language->get('text_store_identificator');
        $data['text_store_identificator_placeholder'] = $this->language->get('text_store_identificator_placeholder');
        $data['text_secret_key'] = $this->language->get('text_secret_key');
        $data['text_host'] = $this->language->get('text_host');
        $data['text_host_placeholder'] = $this->language->get('text_host_placeholder');
        $data['text_parts'] = $this->language->get('text_parts');
        $data['text_parts_placeholder'] = $this->language->get('text_parts_placeholder');
        $data['text_payment_text'] = $this->language->get('text_payment_text');

		$data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_monobank_order_status_confirm_id'] = $this->language->get('entry_monobank_order_status_confirm_id');
        $data['entry_monobank_order_status_cancel_id'] = $this->language->get('entry_monobank_order_status_cancel_id');
        $data['entry_monobank_order_status_return_id'] = $this->language->get('entry_monobank_order_status_return_id');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

        if (isset($this->error['error_monobank_parts'])) {
            $data['error_monobank_parts'] = $this->error['error_monobank_parts'];
        } else {
            $data['error_monobank_parts'] = '';
        }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'] . '&type=payment', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/monobank', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('payment/monobank', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'] . '&type=payment', true);

		if (isset($this->request->post['monobank_total'])) {
			$data['monobank_total'] = $this->request->post['monobank_total'];
		} else {
			$data['monobank_total'] = $this->config->get('monobank_total');
		}

		if (isset($this->request->post['monobank_order_status_id'])) {
			$data['monobank_order_status_id'] = $this->request->post['monobank_order_status_id'];
		} else {
			$data['monobank_order_status_id'] = $this->config->get('monobank_order_status_id');
		}

        if (isset($this->request->post['monobank_order_status_confirm_id'])) {
            $data['monobank_order_status_confirm_id'] = $this->request->post['monobank_order_status_confirm_id'];
        } else {
            $data['monobank_order_status_confirm_id'] = $this->config->get('monobank_order_status_confirm_id');
        }

        if (isset($this->request->post['monobank_order_status_cancel_id'])) {
            $data['monobank_order_status_cancel_id'] = $this->request->post['monobank_order_status_cancel_id'];
        } else {
            $data['monobank_order_status_cancel_id'] = $this->config->get('monobank_order_status_cancel_id');
        }

        if (isset($this->request->post['monobank_order_status_return_id'])) {
            $data['monobank_order_status_return_id'] = $this->request->post['monobank_order_status_return_id'];
        } else {
            $data['monobank_order_status_return_id'] = $this->config->get('monobank_order_status_return_id');
        }

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['monobank_geo_zone_id'])) {
			$data['monobank_geo_zone_id'] = $this->request->post['monobank_geo_zone_id'];
		} else {
			$data['monobank_geo_zone_id'] = $this->config->get('monobank_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['monobank_status'])) {
			$data['monobank_status'] = $this->request->post['monobank_status'];
		} else {
			$data['monobank_status'] = $this->config->get('monobank_status');
		}

		if (isset($this->request->post['monobank_sort_order'])) {
			$data['monobank_sort_order'] = $this->request->post['monobank_sort_order'];
		} else {
			$data['monobank_sort_order'] = $this->config->get('monobank_sort_order');
		}

        if (isset($this->request->post['monobank_store_identificator'])) {
            $data['monobank_store_identificator'] = $this->request->post['monobank_store_identificator'];
        } else {
            $data['monobank_store_identificator'] = $this->config->get('monobank_store_identificator');
        }

        if (isset($this->request->post['monobank_secret_key'])) {
            $data['monobank_secret_key'] = $this->request->post['monobank_secret_key'];
        } else {
            $data['monobank_secret_key'] = $this->config->get('monobank_secret_key');
        }

        if (isset($this->request->post['monobank_host'])) {
            $data['monobank_host'] = $this->request->post['monobank_host'];
        } else {
            $data['monobank_host'] = $this->config->get('monobank_host');
        }

        if (isset($this->request->post['monobank_parts'])) {
            $data['monobank_parts'] = $this->request->post['monobank_parts'];
        } else {
            $data['monobank_parts'] = $this->config->get('monobank_parts');
        }

        if (isset($this->request->post['monobank_payment_text'])) {
            $data['monobank_payment_text'] = $this->request->post['monobank_payment_text'];
        } else {
            $data['monobank_payment_text'] = $this->config->get('monobank_payment_text');
        }

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('payment/monobank.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/monobank')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$parts_array = explode(',',$this->request->post['monobank_parts']);
		foreach ($parts_array as $part){
            if ($part < 3 && $part != 0){
                $this->error['error_monobank_parts'] = $this->language->get('error_monobank_parts');
            }
        }

		return !$this->error;
	}
}