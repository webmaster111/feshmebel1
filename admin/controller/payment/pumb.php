<?php
class ControllerPaymentPumb extends Controller
{
	private $error = array();

 	public function index()
	{
		$this->language->load('payment/pumb');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('pumb', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'] . '&type=payment', 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_live'] = $this->language->get('text_live');
		$data['text_demo'] = $this->language->get('text_demo');
		$data['text_merchant_console'] = $this->language->get('text_merchant_console');

		$data['entry_merch_id'] = $this->language->get('entry_merch_id');
		$data['entry_account_id'] = $this->language->get('entry_account_id');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_order_status_success'] = $this->language->get('entry_order_status_success');
		$data['entry_order_status_failed'] = $this->language->get('entry_order_status_failed');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_live_url'] = $this->language->get('entry_live_url');
		$data['entry_demo_url'] = $this->language->get('entry_demo_url');
		$data['entry_live_demo'] = $this->language->get('entry_live_demo');
		$data['entry_signature_live'] = $this->language->get('entry_signature_live');
		$data['entry_signature_demo'] = $this->language->get('entry_signature_demo');
		
		$data['tab_account'] = $this->language->get('tab_account');
		$data['tab_advanced'] = $this->language->get('tab_advanced');

		$data['help_total'] = $this->language->get('help_total');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/pumb', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('payment/pumb', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		
		if (isset($this->request->post['pumb_merch_id'])) {
			$data['pumb_merch_id'] = $this->request->post['pumb_merch_id'];
		} else {
			$data['pumb_merch_id'] = $this->config->get('pumb_merch_id');
		}
		
		if (isset($this->request->post['pumb_account_id'])) {
			$data['pumb_account_id'] = $this->request->post['pumb_account_id'];
		} else {
			$data['pumb_account_id'] = $this->config->get('pumb_account_id');
		}		

		if (isset($this->request->post['pumb_total'])) {
			$data['pumb_total'] = $this->request->post['pumb_total'];
		} else {
			$data['pumb_total'] = $this->config->get('pumb_total');
		}

		if (isset($this->request->post['pumb_order_status_id'])) {
			$data['pumb_order_status_id'] = $this->request->post['pumb_order_status_id'];
		} else {
			$data['pumb_order_status_id'] = $this->config->get('pumb_order_status_id');
		}	
		
		if (isset($this->request->post['pumb_order_status_success_id'])) {
			$data['pumb_order_status_success_id'] = $this->request->post['pumb_order_status_success_id'];
		} else {
			$data['pumb_order_status_success_id'] = $this->config->get('pumb_order_status_success_id');
		}
		
		if (isset($this->request->post['pumb_order_status_failed_id'])) {
			$data['pumb_order_status_failed_id'] = $this->request->post['pumb_order_status_failed_id'];
		} else {
			$data['pumb_order_status_failed_id'] = $this->config->get('pumb_order_status_failed_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['pumb_geo_zone_id'])) {
			$data['pumb_geo_zone_id'] = $this->request->post['pumb_geo_zone_id'];
		} else {
			$data['pumb_geo_zone_id'] = $this->config->get('pumb_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['pumb_status'])) {
			$data['pumb_status'] = $this->request->post['pumb_status'];
		} else {
			$data['pumb_status'] = $this->config->get('pumb_status');
		}

		if (isset($this->request->post['pumb_sort_order'])) {
			$data['pumb_sort_order'] = $this->request->post['pumb_sort_order'];
		} else {
			$data['pumb_sort_order'] = $this->config->get('pumb_sort_order');
		}
		
		if (isset($this->request->post['pumb_live_demo'])) {
			$data['pumb_live_demo'] = $this->request->post['pumb_live_demo'];
		} else {
			$data['pumb_live_demo'] = $this->config->get('pumb_live_demo');
		}
		
		if (isset($this->request->post['pumb_live_url'])) {
			$data['pumb_live_url'] = $this->request->post['pumb_live_url'];
		} else {
			$data['pumb_live_url'] = $this->config->get('pumb_live_url');
		}

		if (empty($data['pumb_live_url'])) {
			$data['pumb_live_url'] = 'https://pps.fuib.com/payment/start.wsm';
		}

		if (isset($this->request->post['pumb_demo_url'])) {
			$data['pumb_demo_url'] = $this->request->post['pumb_demo_url'];
		} else {
			$data['pumb_demo_url'] = $this->config->get('pumb_demo_url');
		}
		
		if (empty($data['pumb_demo_url'])) {
			$data['pumb_demo_url'] = 'https://pps03.fuib.com/payment/start.wsm';
		}
		
		if (isset($this->request->post['pumb_signature_live'])) {
			$data['pumb_signature_live'] = $this->request->post['pumb_signature_live'];
		} else {
			$data['pumb_signature_live'] = $this->config->get('pumb_signature_live');
		}
		
		if (isset($this->request->post['pumb_signature_demo'])) {
			$data['pumb_signature_demo'] = $this->request->post['pumb_signature_demo'];
		} else {
			$data['pumb_signature_demo'] = $this->config->get('pumb_signature_demo');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('payment/pumb.tpl', $data));
	}
	
	private function validate()
	{
		if (!$this->user->hasPermission('modify', 'payment/pumb')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>