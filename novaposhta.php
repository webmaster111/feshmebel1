<?php

require_once('config.php');
$DSN = 'mysql:host=%s;dbname=%s';

$opts = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
);

try {
    $db = new PDO(sprintf($DSN, DB_HOSTNAME, DB_DATABASE), DB_USERNAME, DB_PASSWORD, $opts);
} catch (PDOException $ex) {
    die('Database Connection Error: #' . $ex->getCode() . ' > ' . $ex->getMessage());
}
$res = $db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'oc_novaposhta_cities' ");
$oc_novaposhta_cities = [];
while ($row = $res->fetch()) if ($row['COLUMN_NAME'] != 'id') {
    $oc_novaposhta_cities[] = $row['COLUMN_NAME'];
}
$res = $db->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'oc_novaposhta_warehouses' ");
$oc_novaposhta_warehouses = [];
while ($row = $res->fetch()) if ($row['COLUMN_NAME'] != 'id') {
    $oc_novaposhta_warehouses[] = $row['COLUMN_NAME'];
}


function get_info($json)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.novaposhta.ua/v2.0/json/",
        CURLOPT_RETURNTRANSFER => True,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($json),
        CURLOPT_HTTPHEADER => array("content-type: application/json",),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
        return null;
    } else {
        return json_decode($response, 1);
    }
}

$apiKey = '5e2acbbeee3b86878208fb6d739650b0';
$json = array(
    "modelName" => "Address",
    "calledMethod" => "getCities",
    "methodProperties" => [
        "" => ""
    ],
    "apiKey" => $apiKey,
);


$db->query("TRUNCATE TABLE `oc_novaposhta_cities`");
$db->query("TRUNCATE TABLE `oc_novaposhta_warehouses`");

$getCities = get_info($json);
if (isset($getCities['data'])) {
    foreach ($getCities['data'] as $one) {
        $query = "SELECT * FROM `oc_novaposhta_cities` WHERE `CityID` = '" . $one['CityID'] . "'";

        $novaposhta_cities = $db->query($query)->fetch();
        $params = [];
        if (isset($novaposhta_cities['id'])) {
            foreach ($oc_novaposhta_cities as $param)
                if (isset($one[$param]) || empty($one[$param])) {
                    if (!is_array($one[$param]))
                        $params[] = "`" . $param . "` = '" . str_replace("'", "\'", $one[$param]) . "'";
                    else
                        $params[] = "`" . $param . "` = '" . str_replace("'", "\'", serialize($one[$param])) . "'";
                }
            $query = "UPDATE `oc_novaposhta_cities` SET " . implode(', ', $params) . " WHERE `CityID` = '" . $one['CityID'] . "'";
            $db->query($query);
        } else {
            foreach ($oc_novaposhta_cities as $param)
                if (isset($one[$param]) || empty($one[$param])) {
                    if (!is_array($one[$param]))
                        $params[$param] = str_replace("'", "\'", $one[$param]);
                    else
                        $params[$param] = str_replace("'", "\'", serialize($one[$param]));
                }
            $query = "INSERT INTO `oc_novaposhta_cities` (`" . implode('`, `', $oc_novaposhta_cities) . "`) SELECT '" . implode("', '", $params) . "'";
            $db->query($query);
        }

        $json = array(
            "modelName" => "AddressGeneral",
            "calledMethod" => "getWarehouses",
            "methodProperties" => [
                "CityRef" => $one['Ref'],
            ],
            "apiKey" => $apiKey,
        );
        $getWarehouses = get_info($json);

        if (isset($getWarehouses['data'])) {
            foreach ($getWarehouses['data'] as $one_w) {
                $params = [];
                $query = "SELECT * FROM `oc_novaposhta_warehouses` WHERE `SiteKey` = '" . $one_w['SiteKey'] . "'";
                $novaposhta_warehouses = $db->query($query)->fetch();
                if (isset($novaposhta_warehouses['id'])) {
                    foreach ($oc_novaposhta_warehouses as $param)
                        if (isset($one_w[$param])) {
                            if (!is_array($one_w[$param]))
                                $params[] = "`" . $param . "` = '" . str_replace("'", "\'", $one_w[$param]) . "'";
                            else
                                $params[] = "`" . $param . "` = '" . str_replace("'", "\'", serialize($one_w[$param])) . "'";
                        }
                    $query = "UPDATE `oc_novaposhta_warehouses` SET " . implode(', ', $params) . " WHERE `SiteKey` = '" . $one_w['SiteKey'] . "'";
                    $db->query($query);
                } else {
                    foreach ($oc_novaposhta_warehouses as $param)
                        if (isset($one_w[$param]) || empty($one_w[$param])) {
                            if (!is_array($one_w[$param]))
                                $params[$param] = str_replace("'", "\'", $one_w[$param]);
                            else
                                $params[$param] = str_replace("'", "\'", serialize($one_w[$param]));
                        }
                    $query = "INSERT INTO `oc_novaposhta_warehouses` (`" . implode('`, `', $oc_novaposhta_warehouses) . "`) SELECT '" . implode("', '", $params) . "'";
                    $db->query($query);
                }
            }
        }




    }
}