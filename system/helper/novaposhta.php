<?php
/**
 * @ PHP 5.6
 * @ Decoder version : 1.0.0.5
 * @ Release on : 22.05.2019
 * @ Website    : http://EasyToYou.eu
 *
 * @ Zend guard decoder PHP 5.6
 */
class NovaPoshta
{
    private $registry;
    private $api_url = 'https://api.novaposhta.ua/v2.0/json/';
    public $key_api = '';
    public $error = array();
    public $description_field;

    public function __construct($registry)
    {
        $this->registry = $registry;
        $this->key_api = $this->config->get('novaposhta_key_api');
        $this->description_field = $this->language->get('code') == 'uk' ? 'Description' : 'DescriptionRu';
    }

    public function __get($name)
    {
        return $this->registry->get($name);
    }

    public function apiRequest($model, $method, $properties = array())
    {
        $data = false;
        $array = array('apiKey' => $this->key_api, 'modelName' => $model, 'calledMethod' => $method);

        if (!empty($properties)) {
            $array['methodProperties'] = $properties;
        }

        $options = array(
            CURLOPT_HTTPHEADER     => array('Content-Type: application/json'),
            CURLOPT_HEADER         => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_CONNECTTIMEOUT => 3,
            CURLOPT_TIMEOUT        => 15,
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => json_encode($array),
            CURLOPT_RETURNTRANSFER => 1
        );
        $ch = curl_init($this->api_url);
        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        if (($method == 'printDocument') || ($method == 'printMarkings')) {
            $data = $response;
        }
        else {
            $response = json_decode($response, true);
            $this->catchError($response);
            $data = ($response['success'] ? $response['data'] : false);
        }

        return $data;
    }

    private function catchError($response)
    {
        if (is_array($response['errors']) && !empty($response['errors'])) {
            $beginning = 'Nova Poshta error: ';

            foreach ($response['errors'] as $error) {
                $this->error[] = $beginning . $error;
                $this->log->write($beginning . $error);
            }
        }

        if (is_array($response['warnings']) && !empty($response['warnings'])) {
            $beginning = 'Nova Poshta warning: ';

            foreach ($response['warnings'] as $warning) {
                $this->error[] = $beginning . $warning;
                $this->log->write($beginning . $warning);
            }
        }
    }

    public function update($type)
    {
        $data = array();

        switch ($type) {
            case 'areas':
                $data = $this->apiRequest('Address', 'getAreas');

                if ($data) {
                    foreach ($data as $k => $v) {
                        $data[$v['Description']] = $v;
                        unset($data[$k]);
                    }

                    $this->db->query("INSERT INTO `" . DB_PREFIX . "novaposhta_references` (`type`, `value`) VALUES ('areas', '" . $this->db->escape(serialize($data)) . "') ON DUPLICATE KEY UPDATE `value`='" . $this->db->escape(serialize($data)) . "'");
                }

                break;
            case 'cities':
                $data = $this->apiRequest('Address', 'getCities');

                if ($data) {
                    $item = 0;
                    $data_count = count($data);
                    $this->db->query('TRUNCATE TABLE `' . DB_PREFIX . 'novaposhta_cities`');

                    while ($data_count) {
                        $sql = "INSERT INTO `" . DB_PREFIX . "novaposhta_cities` (`Description`, `DescriptionRu`, `Ref`, `Area`,  `Delivery1`, `Delivery2`, `Delivery3`, `Delivery4`, `Delivery5`, `Delivery6`, `Delivery7`, `Conglomerates`, `CityID`) VALUES ";
                        $i = 0;

                        while ($i < $data_count) {
                            if ($i != 0) {
                                $sql .= ',';
                            }

                            $sql .= "('" . $this->db->escape($data[$item]["Description"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["DescriptionRu"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Ref"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Area"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Delivery1"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Delivery2"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Delivery3"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Delivery4"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Delivery5"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Delivery6"]) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Delivery7"]) . "'," . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape(serialize($data[$item]["Conglomerates"])) . "', " . "\r\n\t\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["CityID"]) . "')";
                            ++$i;
                            ++$item;
                            --$data_count;
                        }

                        $this->db->query($sql);
                    }
                }

                break;
            case 'warehouses':
                $data = $this->apiRequest('Address', 'getWarehouses');

                if ($data) {
                    $item = 0;
                    $data_count = count($data);
                    $this->db->query('TRUNCATE TABLE `' . DB_PREFIX . 'novaposhta_warehouses`');

                    while ($data_count) {
                        $sql = "INSERT INTO `" . DB_PREFIX . "novaposhta_warehouses` (`SiteKey`, `Description`, `DescriptionRu`, `Phone`, `TypeOfWarehouse`,  `Ref`, `Number`, `CityRef`, `CityDescription`, `CityDescriptionRu`, `Longitude`, `Latitude`, `PostFinance`, `TotalMaxWeightAllowed`, `PlaceMaxWeightAllowed`, `Reception`, `Delivery`, `Schedule`) VALUES ";
                        $i = 0;

                        while ($i < $data_count) {
                            if ($i != 0) {
                                $sql .= ',';
                            }

                            $sql .= "('" . $this->db->escape($data[$item]["SiteKey"]) . "'," . "\r\n\t\t\t\t\t\t\t" . "'" . preg_replace('/"([^"]+)"/', "«$1»", $this->db->escape($data[$item]["Description"])) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . preg_replace('/"([^"]+)"/', "«$1»", $this->db->escape($data[$item]["DescriptionRu"])) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Phone"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["TypeOfWarehouse"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Ref"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Number"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["CityRef"]) . "'," . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["CityDescription"]) . "'," . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["CityDescriptionRu"]) . "'," . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Longitude"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["Latitude"]) . "'," . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["PostFinance"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["TotalMaxWeightAllowed"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape($data[$item]["PlaceMaxWeightAllowed"]) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape(serialize($data[$item]["Reception"])) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape(serialize($data[$item]["Delivery"])) . "', " . "\r\n\t\t\t\t\t\t\t" . "'" . $this->db->escape(serialize($data[$item]["Schedule"])) . "')";
                            ++$i;
                            ++$item;
                            --$data_count;
                        }

                        $this->db->query($sql);
                    }
                }

                break;
            case 'references':
                $data['senders'] = $this->getCounterparties('Sender');
                $data['third_persons'] = $this->getCounterparties('ThirdPerson');

                if ($data['senders']) {
                    foreach ($data['senders'] as $sender) {
                        $data['sender_contact_persons'][$sender['Ref']] = $this->getContactPerson($sender['Ref']);
                        $data['sender_addresses'][$sender['Ref']] = $this->getAddress($sender['Ref']);
                    }
                }

                sleep(1);
                $data['warehouse_types'] = $this->apiRequest('Address', 'getWarehouseTypes');
                $data['cargo_types'] = $this->apiRequest('Common', 'getCargoTypes');
                $data['payer_types'] = $this->apiRequest('Common', 'getTypesOfPayers');
                $data['payment_types'] = $this->apiRequest('Common', 'getPaymentForms');
                $data['backward_delivery_types'] = $this->apiRequest('Common', 'getBackwardDeliveryCargoTypes');
                $data['backward_delivery_payers'] = $this->apiRequest('Common', 'getTypesOfPayersForRedelivery');
                $data['cargo_description'] = $this->apiRequest('Common', 'getCargoDescriptionList');
                $data['document_statuses'] = $this->apiRequest('Common', 'getDocumentStatuses');
                $data['ownership_forms'] = $this->apiRequest('Common', 'getOwnershipFormsList');

                foreach ($data as $k => $v) {
                    $this->db->query("INSERT INTO `" . DB_PREFIX . "novaposhta_references` (`type`, `value`) VALUES ('" . $k . "', '" . $this->db->escape(serialize($v)) . "') ON DUPLICATE KEY UPDATE `value`='" . $this->db->escape(serialize($v)) . "'");
                }

                break;
        }

        $database = $this->getReferences('database');
        $database[$type]['update_datetime'] = date('d-m-Y H:i:s');
        $database[$type]['amount'] = $data ? count($data) : 0;
        $this->db->query("INSERT INTO `" . DB_PREFIX . "novaposhta_references` (`type`, `value`) VALUES ('database', '" . serialize($database) . "') ON DUPLICATE KEY UPDATE `value`='" . serialize($database) . "'");

        if ($data) {
        }

        return $data ? $database[$type]['amount'] : $data;
    }

    public function getAreaRef($name)
    {
        $data = '';
        $novaposhta_areas = $this->getAreas();


        $areas = array(
            'АРК'                            => array('Крим', 'АРК', 'Крым', 'АРК', 'Krym', 'Crimea'),
            'Вінницька'                => array('Вінниця', 'Вінницька', 'Винница', 'Винницкая', 'Vinnitsa', 'Vinnitskaya'),
            'Волинська'                => array('Волинь', 'Волинська', 'Волынь', 'Волынская', 'Volyn', 'Volynskaya'),
            'Дніпропетровська'  => array('Дніпро', 'Дніпропетровськ', 'Дніпропетровська', 'Днепропетровск', 'Днепропетровская', 'Dnipropetrovsk', 'Dnepropetrovskaya'),
            'Донецька'                  => array('Донецьк', 'Донецька', 'Донецк', 'Донецкая', 'Donetsk', 'Donetskaya'),
            'Житомирська'            => array('Житомир', 'Житомирська', 'Житомир', 'Житомирская', 'Zhytomyr', 'Zhitomirskaya'),
            'Закарпатська'          => array('Закарпаття', 'Закарпатська', 'Закарпатье', 'Закарпатская', 'Zakarpattya', 'Zakarpatskaya'),
            'Запорізька'              => array('Запоріжжя', 'Запорізька', 'Запорожье', 'Запорожская', 'Zaporizhia', 'Zaporozhskaya'),
            'Івано-Франківська' => array('Івано-Франківськ', 'Івано-Франківська', 'Ивано-Франковск', 'Ивано-Франковская', 'Ivano-Frankivsk', 'Ivano-Frankovskaya'),
            'Київська'                  => array('Київ', 'Київська', 'Киев', 'Киевская', 'Kyiv', 'Kiyevskaya'),
            'Кіровоградська'      => array('Кіровоград', 'Кіровоградська', 'Кировоград', 'Кировоградская', 'Kirovohrad', 'Kirovogradskaya'),
            'Луганська'                => array('Луганськ', 'Луганська', 'Луганск', 'Луганская', 'Lugansk', 'Luganskaya'),
            'Львівська'                => array('Львів', 'Львівська', 'Львов', 'Львовская', 'Lviv', 'L\'vovskaya'),
            'Миколаївська'          => array('Миколаїв', 'Миколаївська', 'Николаев', 'Николаевская', 'Mykolaiv', 'Nikolayevskaya'),
            'Одеська'                    => array('Одеса', 'Одеська', 'Одесса', 'Одесская', 'Odessa', 'Odesskaya'),
            'Полтавська'              => array('Полтава', 'Полтавська', 'Полтава', 'Полтавская', 'Poltava', 'Poltavskaya'),
            'Рівненська'              => array('Рівне', 'Рівненська', 'Ровно', 'Ровненская', 'Ровенская', 'Rivne', 'Rovenskaya'),
            'Сумська'                    => array('Суми', 'Сумська', 'Сумы', 'Сумская', 'Sums', 'Sumskaya'),
            'Тернопільська'        => array('Тернопіль', 'Тернопільська', 'Тернополь', 'Тернопольская', 'Ternopil', 'Ternopol\'skaya'),
            'Харківська'              => array('Харків', 'Харківська', 'Харьков', 'Харьковская', 'Kharkov', 'Khar\'kovskaya'),
            'Херсонська'              => array('Херсон', 'Херсонська', 'Херсон', 'Херсонская', 'Herson', 'Khersonskaya'),
            'Хмельницька'            => array('Хмельницьк', 'Хмельницька', 'Хмельницкий', 'Хмельницкая', 'Khmelnytsky', 'Khmel\'nitskaya'),
            'Черкаська'                => array('Черкаси', 'Черкаська', 'Черкассы', 'Черкасская', 'Cherkassy', 'Cherkasskaya'),
            'Чернівецька'            => array('Чернівці', 'Чернівецька', 'Черновцы', 'Черновицкая', 'Chernivtsi', 'Chernovitskaya'),
            'Чернігівська'          => array('Чернігів', 'Чернігівська', 'Чернигов', 'Черниговская', 'Chernihiv', 'Chernigovskaya')
        );
        $sub_name = mb_substr($name, 0, 6, 'UTF-8');

        foreach ($areas as $d => $v) {
            $match = preg_grep('/^' . $sub_name . '/ui', $v);
            if(count($match)>0)
            {
                $data=$novaposhta_areas[$d]['Ref'];
                break;
            }

        }

       //print_r($novaposhta_areas);
        return $data;
    }

    public function getAreaName($ref)
    {
        $data = '';
        $novaposhta_areas = $this->getAreas();

        foreach ($novaposhta_areas as $d => $v) {
            $data = $d;
            break;
        }

        return $data;
    }

    public function getAreas()
    {
        $data = $this->getReferences('areas');
        return $data;
    }

    public function getCityArea($ref)
    {
        $result = $this->db->query("SELECT `Area` FROM `" . DB_PREFIX . "novaposhta_cities` WHERE `Ref` = '" . $this->db->escape($ref) . "'")->row;
        return $result ? $result['Area'] : '';
    }

    public function getCityRef($name)
    {
        $result = $this->db->query("SELECT `Ref` FROM `" . DB_PREFIX . "novaposhta_cities` WHERE `Description` = '" . $this->db->escape($name) . "' OR `DescriptionRu` = '" . $this->db->escape($name) . "'")->row;
        return $result ? $result['Ref'] : '';
    }

    public function getCityName($ref)
    {
        $result = $this->db->query("SELECT `" . $this->description_field . "` FROM `" . DB_PREFIX . "novaposhta_cities` WHERE `Ref` = '" . $this->db->escape($ref) . "'")->row;
        return $result ? $result[$this->description_field] : '';
    }

    public function getCities($area = false)
    {
        if ($area) {
            $data = $this->db->query("SELECT `" . $this->description_field . "` as `Description`, `Ref` FROM `" . DB_PREFIX . "novaposhta_cities` WHERE `Area` = '" . $this->db->escape($area) . "'")->rows;
        }
        else {
            $data = $this->db->query("SELECT `" . $this->description_field . "` as `Description`, `Ref` FROM `" . DB_PREFIX . "novaposhta_cities`")->rows;
        }

        return $data;
    }

    public function getCitiesWithAreasByCityName($name)
    {
        $data = array();
        if (($this->language->get('code') == 'uk') || ($this->language->get('code') == 'ru')) {
            $reg = 'обл.';
        }
        else {
            $reg = 'reg.';
        }

        $results = $this->db->query("SELECT `" . $this->description_field . "`, `Ref`, `Area` FROM `" . DB_PREFIX . "novaposhta_cities` WHERE `" . $this->description_field . "` LIKE '" . $this->db->escape($name) . "%' LIMIT 5")->rows;

        foreach ($results as $result) {
            $data[] = array('CityName' => $result[$this->description_field], 'CityRef' => $result['Ref'], 'Description' => $result[$this->description_field] . ', ' . $this->novaposhta->getAreaName($result['Area']) . ' ' . $reg);
        }

        return $data;
    }

    public function getWarehouseRef($name)
    {
        $results = $this->db->query("SELECT `Ref` FROM `" . DB_PREFIX . "novaposhta_warehouses` WHERE `Description` = '" . $this->db->escape($name) . "' OR `DescriptionRu` = '" . $this->db->escape($name) . "'")->row;
        return $results ? $results['Ref'] : '';
    }

    public function getWarehouseName($ref)
    {
        $results = $this->db->query("SELECT `" . $this->description_field . "` FROM `" . DB_PREFIX . "novaposhta_warehouses` WHERE `Ref` = '" . $this->db->escape($ref) . "'")->row;
        return $results ? $results[$this->description_field] : '';
    }

    public function getWarehouseByCity($warehouse, $city)
    {
        $result = $this->db->query("SELECT `" . $this->description_field . "` as `Description`, `Ref` FROM `" . DB_PREFIX . "novaposhta_warehouses` WHERE (`Ref` = '" . $this->db->escape($warehouse) . "' OR `Description` = ' ". $this->db->escape($warehouse) . "' OR `DescriptionRu` = '" . $this->db->escape($warehouse) . "') AND (`CityRef` = '" . $this->db->escape($city) . "' OR `CityDescription` = '" . $this->db->escape($city) . "' OR `CityDescriptionRu` = '" . $this->db->escape($city) . "')")->row;
        return $result;
    }

    public function getWarehousesByCityRef($ref)
    {
        $results = $this->db->query("SELECT `" . $this->description_field . "` as `Description`, `Ref` FROM `" . DB_PREFIX . "novaposhta_warehouses` WHERE `CityRef` = '" . $ref . "'")->rows;
        return $results;
    }

    public function getWarehousesByCityName($name)
    {
        $results = $this->db->query("SELECT `" . $this->description_field . "` as `Description`, `Ref` FROM `" . DB_PREFIX . "novaposhta_warehouses` WHERE `CityDescription` = '" . $name . "' OR `CityDescriptionRu` = '" . $name . "'")->rows;
        return $results;
    }

    public function getSenderAddressesByCityRef($counterparty_ref, $city_ref)
    {
        $results = array();
        $addresses = $this->novaposhta->getReferences('sender_addresses');

        if (isset($addresses[$counterparty_ref])) {
            foreach ($addresses[$counterparty_ref] as $k => $sender_address) {
                if ($sender_address['CityRef'] == $city_ref) {
                    $results[$k] = $sender_address;
                }
            }
        }

        return $results;
    }

    public function getReferences($type = '')
    {
        $data = array();

        if ($type) {
            $result = $this->db->query("SELECT `value` FROM `" . DB_PREFIX . "novaposhta_references` WHERE `type` = '" . $type . "'")->row;
            $data = (isset($result['value']) ? unserialize($result['value']) : false);
        }
        else {
            $results = $this->db->query("SELECT `type`, `value` FROM `" . DB_PREFIX . "novaposhta_references` WHERE `type` != 'cargo_description'")->rows;

            if (is_array($results)) {
                foreach ($results as $r) {
                    $data[$r['type']] = unserialize($r['value']);
                }
            }
        }

        return $data;
    }

    public function getCounterparties($counterparty_type, $search = '', $city_ref = '')
    {
        $properties = array('CounterpartyProperty' => $counterparty_type);
        if ($search && !preg_match('/[^А-яҐґЄєIіЇїё0-9\-\`\'\s]+/iu', $search)) {
            $properties['FindByString'] = $search;
        }
        if ($city_ref) {
            $properties['CityRef'] = $city_ref;
        }

        $data = $this->apiRequest('Counterparty', 'getCounterparties', $properties);

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $data[$v['Ref']] = $v;
                unset($data[$k]);
            }
        }

        return $data;
    }

    public function saveCounterparties($properties)
    {
        $data = $this->apiRequest('Counterparty', 'save', $properties);
        return isset($data[0]) ? $data[0] : $data;
    }

    public function getContactPerson($ref, $search = '')
    {
        $properties = array('Ref' => $ref);

        if ($search) {
            $properties['FindByString'] = $search;
        }

        $data = $this->apiRequest('Counterparty', 'getCounterpartyContactPersons', $properties);

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $data[$v['Ref']] = $v;
                unset($data[$k]);
            }
        }

        return $data;
    }

    public function saveContactPerson($properties)
    {
        $data = $this->apiRequest('ContactPerson', 'save', $properties);
        return isset($data[0]) ? $data[0] : $data;
    }

    public function updateContactPerson($properties)
    {
        $data = $this->apiRequest('ContactPerson', 'update', $properties);
        return $data;
    }

    public function getAddress($ref)
    {
        $properties = array('Ref' => $ref);
        $data = $this->apiRequest('Counterparty', 'getCounterpartyAddresses', $properties);

        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $data[$v['Ref']] = $v;
                unset($data[$k]);
            }
        }

        return $data;
    }

    public function getDocumentPrice($properties)
    {
        $data = $this->apiRequest('InternetDocument', 'getDocumentPrice', $properties);
        return $data ? $data[0]['Cost'] : 0;
    }

    public function getDocumentDeliveryDate($properties)
    {
        $data = $this->apiRequest('InternetDocument', 'getDocumentDeliveryDate', $properties);
        return $data ? $this->dateDiff($data[0]['DeliveryDate']['date']) : 0;
    }

    public function saveEI($properties)
    {
        $method = (isset($properties['Ref']) ? 'update' : 'save');
        $data = $this->apiRequest('InternetDocument', $method, $properties);
        return $data;
    }

    public function getEI($ref)
    {
        $properties = array('Ref' => $ref);
        $data = $this->apiRequest('InternetDocument', 'getDocument', $properties);
        return isset($data[0]) ? $data[0] : false;
    }

    public function getEIList($date = '')
    {
        $properties = array('DateTime' => $date);
        $data = $this->apiRequest('InternetDocument', 'getDocumentList', $properties);
        return $data;
    }

    public function deleteEI($refs)
    {
        $properties = array('DocumentRefs' => $refs);
        $data = $this->apiRequest('InternetDocument', 'delete', $properties);
        return $data;
    }

    public function tracking($documents = array())
    {
        $properties = array('Documents' => $documents);
        $data = $this->apiRequest('TrackingDocument', 'getStatusDocuments', $properties);
        return $data;
    }

    public function printDocument($orders, $type, $format)
    {
        $properties = array('DocumentRefs' => $orders, 'Type' => $format);
        $data = $this->apiRequest('InternetDocument', $type, $properties);
        return $data;
    }

    public function getWeight($products = array())
    {
        $weight = 0;

        if ($this->config->get('novaposhta_use_parameters')) {
            foreach ($products as $product) {
                $p_weight = $this->weightConvert($product['weight'], $this->weight->getUnit($product['weight_class_id']));
                $weight += (0 < $p_weight ? $p_weight : $this->config->get('novaposhta_weight'));
            }
        }
        else {
            $weight = $this->config->get('novaposhta_weight');
        }

        return round($weight, 5);
    }

    public function getVolume($products)
    {
        $volume = 0;

        if ($this->config->get('novaposhta_use_parameters')) {
            foreach ($products as $product) {
                $p_volume = $this->volumeConvert($product['length'] * $product['width'] * $product['height'] * $product['quantity'], $this->length->getUnit($product['length_class_id']));
                $volume += (0 < $p_volume ? $p_volume : ($this->config->get('novaposhta_dimensions_l') * $this->config->get('novaposhta_dimensions_w') * $this->config->get('novaposhta_dimensions_h')) / 1000000);
            }
        }
        else {
            $volume = ($this->config->get('novaposhta_dimensions_l') * $this->config->get('novaposhta_dimensions_w') * $this->config->get('novaposhta_dimensions_h')) / 1000000;
        }

        return round($volume, 5);
    }

    public function weightConvert($value, $unit)
    {
        switch ($unit) {
            case 'g':
            case 'gr':
            case 'gram':
            case 'gramm':
            case 'gramme':
            case 'г':
            case 'гр':
            case 'грам':
            case 'грамм':
                return $value / 1000;
        }

        return $value;
    }

    public function volumeConvert($value, $unit)
    {
        switch ($unit) {
            case 'mm':
            case 'millimeter':
            case 'мм':
            case 'міліметр':
            case 'миллиметр':
                return $value / 1000000000;
            case 'cm':
            case 'centimeter':
            case 'см':
            case 'сантиметр':
                return $value / 1000000;
        }

        return $value;
    }

    public function tariffCalculation($service_type, $total, $weight)
    {
        $services = array('DoorsDoors', 'DoorsWarehouse', 'WarehouseDoors', 'WarehouseWarehouse');
        $service_type = (in_array($service_type, $services) ? $service_type : 'WarehouseWarehouse');
        $cost = 0;

        if (is_array($this->config->get('novaposhta_tariffs'))) {
            foreach ($this->config->get('novaposhta_tariffs') as $tariff) {
                $cost = $tariff[$service_type];
                break;
            }
        }

        if ($this->config->get('novaposhta_additional_fee') && ($this->config->get('novaposhta_additional_fee_bottom') < $total)) {
            $cost += ($total * $this->config->get('novaposhta_additional_fee')) / 100;
        }

        if ($this->config->get('novaposhta_personal_discount')) {
            $cost -= ($cost * $this->config->get('novaposhta_personal_discount')) / 100;
        }

        return round($cost);
    }

    public function dateDiff($string_time)
    {
        return ceil((strtotime($string_time) - time()) / 86400);
    }

    public function arrayKey($array, $key)
    {
        $new_array = array();

        foreach ($array as $v) {
            $new_array[$v[$key]] = $v;
        }

        return $new_array;
    }
}


?>
