<?php
/* All rights reserved belong to the module, the module developers http://opencartadmin.com */
// http://opencartadmin.com � 2011-2016 All Rights Reserved
// Distribution, without the author's consent is prohibited
// Commercial license
class agooLanguage extends Controller
{
	protected $scLanguage;
	private $directory;
	private $data = array();

	public function __call($name, array $params)
	{
		$modules = false;
		$this_language  = $this->registry->get('language');
		$this->scLanguage = $this->registry->get('language_old');

  			$modules  = call_user_func_array(array(
				$this->scLanguage,
				$name
			), $params);

		$this->registry->set('language', $this_language);

        if ($name == "load") {
	        $seocms_folder = substr($params[0], 0, strpos($params[0], '/'));
            if ($this->config->get('blog_work')) {
            	$lang = substr(strtolower($this->config->get('config_admin_language')), 0,2);
            } else {            	$lang = substr(strtolower($this->config->get('config_language')), 0,2);
            }

			if (($seocms_folder == 'agoo' || $seocms_folder == 'seocms' || $params[0]=='module/blog' || $params[0]=='feed/google_sitemap_blog') && ($lang == 'ru')) {

				$class = get_class($this->scLanguage);
				$reflection = new ReflectionClass($class);
				$priv_attr  = $reflection->getProperties(ReflectionProperty::IS_PRIVATE);

                 if ($reflection->hasProperty('data')) {					$reflectionProperty = $reflection->getProperty('data');
					$reflectionProperty->setAccessible(true);

					$data_private = $reflectionProperty->getValue($this->scLanguage);
					$property_flag = true;
				} else {					$data_private = array();
					$property_flag = false;
				}

				$modules = $this->lang_merge($params[0], $modules, $data_private);
  	           	$reflectionProperty->setValue($this->scLanguage, $modules);
                unset($reflection);
			}
        }
        unset($this->scLanguage);
		return $modules;
	}

	public function lang_merge($filename, $data = array(), $_= array()) {

	 	$file = DIR_LANGUAGE . 'russian/' . $filename . '.php';

		if (is_file($file)) {
			require($file);
		}

		$this->data = array_merge($data, $_);

		return $this->data;
	}

}
