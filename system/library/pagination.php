<?php
class Pagination {
	public $total = 0;
	public $page = 1;
	public $limit = 20;
	public $num_links = 8;
	public $url = '';
	public $text_first = '|&lt;';
	public $text_last = '&gt;|';
	public $text_next = '&gt;';
	public $text_prev = '&lt;';

	public function render() {
		$total = $this->total;

		if ($this->page < 1) {
			$page = 1;
		} else {
			$page = $this->page;
		}

		if (!(int)$this->limit) {
			$limit = 10;
		} else {
			$limit = $this->limit;
		}

		$num_links = $this->num_links;
		$num_pages = ceil($total / $limit);

// ---------------------Настройка страниц пагинации----------
		if (isset($_GET['page']) && $_GET['page'] > $num_pages){
			$new_uri = str_replace('?page='.$_GET['page'], 'page-'.$num_pages."/", $_SERVER['REQUEST_URI']);
			header('Location: //' . $_SERVER['HTTP_HOST'] . $new_uri, true, 301);
		}
		unset($new_uri);
		
		if (isset($_GET['page']) && $_GET['page'] =='0'){
			$new_uri = str_replace('?page=0', '', $_SERVER['REQUEST_URI']);
			header('Location: //' . $_SERVER['HTTP_HOST'] . $new_uri, true, 301);
		}
		unset($new_uri);
// ---------------------Настройка страниц пагинации----------

		$this->url = str_replace('?mfp=?mfp=', '?mfp=',  $this->url);
		$this->url = str_replace('%7Bpage%7D', '{page}', $this->url);
		$this->url = str_replace('?page={page}', 'page-{page}/', $this->url);
		$this->url = str_replace('&page={page}', 'page-{page}', $this->url);
		$this->url = str_replace('&sort', 'page-{page}/?sort', $this->url);
		$this->url = str_replace('&limit', 'page-{page}/&limit', $this->url);



        global $get_param;
		$output = '<ul class="pagination pagination__items">';

		if(strstr($_SERVER['REQUEST_URI'], '?page=')){
			preg_match("/(.*)\?page=([0-9]+)(.*)/", $_SERVER['REQUEST_URI'], $output_array);
		}else{
			preg_match("/(.*)\&page=([0-9]+)(.*)/", $_SERVER['REQUEST_URI'], $output_array);
		}
		
		if ($page == 2) {
				$output .= '<li class="pagination__item 12"><a href="' . $output_array[1].$output_array[3] .$get_param. '">' . $this->text_first . '</a></li>';
				$output .= '<li class="pagination__item 13"><a href="' . $output_array[1].$output_array[3].$get_param . '">' . $this->text_prev . '</a></li>';
		}elseif($page > 2){
			$output .= '<li class="pagination__item 14"><a href="' . $output_array[1].$output_array[3] .$get_param. '">' . $this->text_first . '</a></li>';
			if(defined('_FILTER_LANGUAGE_')){
			$_page = $page-1;
			$output .= '<li class="pagination__item 15"><a href="'.$output_array[1].'page-'. $_page .$output_array[3]. '/'.$get_param.'">' . $this->text_prev . '</a></li>';
			}else{
			$output .= '<li class="pagination__item 16"><a href="' . str_replace('{page}', $page - 1, $this->url).$get_param . '">' . $this->text_prev . '</a></li>';
			}
		}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);

				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}

				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= '<li class="pagination__item 7 active"><span>' . $i . '</span></li>';
				}elseif(($i == 1)){
					$output .= '<li class="pagination__item 8"><a href="' . $output_array[1].$output_array[3] . ''.$get_param.'">' . $i . '</a></li>';
//					$output .= '<li><a href="' . str_replace('page-{page}', '', $this->url) . '">' . $i . '</a></li>';
				} else {
					if(defined('_FILTER_LANGUAGE_')){
						if($page == 1){
					$_url = strtok ($_SERVER['REQUEST_URI'] ,'?');
					$output .= '<li class="pagination__item 9 '.$total.'"><a href="'.$_url.'page-'.$i.'/'.$get_param.'">' . $i . '</a></li>';
						}else{
					$output .= '<li class="pagination__item 10"><a href="'.$output_array[1].'page-'. $i .$output_array[3].'/'.$get_param.'">' . $i . '</a></li>';
						}					
					}else{
					$output .= '<li class="pagination__item 11"><a href="'.str_replace('{page}', $i, $this->url).''.$get_param.'">' . $i . '</a></li>';
					}
//					$output .= '<li><a href="' . str_replace('{page}', $i, $this->url) . '">' . $i . '</a></li>';
				}
			}
		}

		if ($page < $num_pages) {
			if(defined('_FILTER_LANGUAGE_')){
			$page_ = $page+1;
				if($page == 1){
				$_url = strtok ($_SERVER['REQUEST_URI'] ,'?');
				$output .= '<li class="pagination__item 1"><a href="'.$_url.'page-'.$page_ .'/'.$get_param.'">'.$this->text_next.'</a></li>';
				$output .= '<li class="pagination__item 2"><a href="'.$_url.'page-'.$num_pages.'/'.$get_param.'">'.$this->text_next.'</a></li>';
				}else{
				$output .= '<li class="pagination__item 3"><a href="'.$output_array[1].'page-'.$page_.$output_array[3].'/'.$get_param.'">'.$this->text_next.'</a></li>';
				$output .= '<li class="pagination__item 4"><a href="'.$output_array[1].'page-'.$num_pages.$output_array[3].'/'.$get_param.'">'.$this->text_next.'</a></li>';
				}
			}else{
			$output .= '<li class="pagination__item 5"><a href="' . str_replace('{page}', $page + 1, $this->url) . ''.$get_param.'">' . $this->text_next . '</a></li>';
			$output .= '<li class="pagination__item 6"><a href="' . str_replace('{page}', $num_pages, $this->url) . ''.$get_param.'">' . $this->text_last . '</a></li>';
			}
		}

		$output .= '</ul>';

		if ($num_pages > 1) {
			return $output;
		} else {
			return '';
		}
	}
}