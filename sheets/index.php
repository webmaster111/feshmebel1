<?php

require_once __DIR__ . '/vendor/autoload.php';

// Путь к файлу ключа сервисного аккаунта
$googleAccountKeyFilePath = __DIR__ . '/Quickstart-0a2a0b3d1066.json';
putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );

// Документация https://developers.google.com/sheets/api/
$client = new Google_Client();
$client->useApplicationDefaultCredentials();

// Области, к которым будет доступ
// https://developers.google.com/identity/protocols/googlescopes
$client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );

$service = new Google_Service_Sheets( $client );

// ID таблицы
$spreadsheetId = '1MT3B1DpkMLlJ7bpURn05eKFFA3P44xDpeA1owuJ2h6s';

$response = $service->spreadsheets->get($spreadsheetId);

$spreadsheetProperties = $response->getProperties();
$spreadsheetProperties->title; // Название таблицы

foreach ($response->getSheets() as $sheet) {

    // Свойства листа
    $sheetProperties = $sheet->getProperties();
    echo $sheetProperties->title."\n"; // Название листа

    $gridProperties = $sheetProperties->getGridProperties();
    echo $gridProperties->columnCount."\n"; // Количество колонок
    echo $gridProperties->rowCount."\n"; // Количество строк
}
$range = 'Лист1!A1:A2';
$response = $service->spreadsheets_values->get($spreadsheetId, $range);

//Время заказа и дата
//№
//Дата доставки
//Город	Время доставки
//Товар
//Всего сумма заказа
//Оплата
//Заказчик
//Номер заказчика
//E-Mail
//Адрес
//Имя получателя
//Номер получателя
//Открытка
//Доп параметры
//Комментарий к заказу
$values = [
    ["2016-02-12", "5453 543543", "=C2+C3"],
    ["2017-02-12", "5453 543543", "=C2+C3"],
    ["2018-02-12", "5453 543543", "=C2+C3"],
];
$body    = new Google_Service_Sheets_ValueRange( [ 'values' => $values ] );


$options = array( 'valueInputOption' => 'RAW' );

$service->spreadsheets_values->update( $spreadsheetId, 'Лист1!A13', $body, $options );


