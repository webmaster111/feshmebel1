<?php
	error_reporting(E_ALL);
	
	include_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	
	global	$base_url;
	global $dbhost,$dbname,$dbusername, $dbpassword, $connect;

	$base_url	 = '//'.$_SERVER['SERVER_NAME'].'/';

	$dbhost		 = DB_HOSTNAME;
	$dbname		 = DB_DATABASE;
	$dbusername  = DB_USERNAME;
	$dbpassword  = DB_PASSWORD; 

	db_connect();
	//--------------------------------------------------------------------------
	function db_connect() {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
//echo '<br>'.$dbhost.' '.$dbname.' '.$dbusername.' '.$dbpassword.' '.$connect;
		@$connect = mysqli_connect($dbhost, $dbusername, $dbpassword, $dbname)
						 or die("Can't connect to MYSQL database	error".mysql_error());
		mysqli_select_db(  $connect, $dbname);
//echo '<br>'.$dbhost.' '.$dbname.' '.$dbusername.' '.$dbpassword.' '.$connect;
//addtolog($dbhost.' '.$dbname.' '.$dbusername.' '.$dbpassword);
//addtolog('$connect='.print_r( $connect,1));
		mysqli_query( $connect, "SET NAMES 'UTF8';");
	};
	
	//--------------------------------------------------------------------------
	function getpath()
	{
		$path = getcwd();  if (strpos($path, ':') > 0) { $path.="\\"; } else { $path.="/";};
		return  $path;
	};
	//--------------------------------------------------------------------------
	function geturl() {
		$path = '//'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
		if ($path[0] == '/') $path = substr($path, 1, strlen($path)-1);
		$pos  = strpos($path,'//');
		$pos1 = strpos($path,'/',$pos+2);
		while ($pos1>0) { $pos = $pos1; $pos1 = strpos($path,'/',$pos1+1); };
		$path = substr( $path, 0, $pos+1);
		return $path;
	};
	//--------------------------------------------------------------------------
	function getname() {
		$path = '//'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
		return $path;
	};
	//--------------------------------------------------------------------------
	function getdomain() {
		$path = '//'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
		if ($path[0] == '/') $path = substr($path, 1, strlen($path)-1);
		$pos  = strpos($path,'//');
		$pos1 = strpos($path,'.',$pos);
		$pos  = $pos1+1;
		while ($pos1>0) { $pos = $pos1+1; $pos1  = strpos($path,'.',$pos1+1); };
		$path = substr( $path, 0, $pos-1);

		$pos1 = strpos($path,'.');
		$pos  = $pos1+1;
		while ($pos1>0) { $pos = $pos1+1; $pos1  = strpos($path,'.',$pos1+1); };
		$pos  = strpos($path,'/',$pos);
		$path = substr( $path, 0, $pos+1);
		return $path;
	};
	//--------------------------------------------------------------------------
	function get_script_name() {
		$name =  $_SERVER['REQUEST_URI'];
		$pos  = 0; $pos1 = strpos($name,'.php',$pos); 
		if ($pos1>0) {
			$pos1 = $pos1 + 4;
			$name = substr($name, $pos,$pos1-$pos);
		};
		addtolog('get_script_name '.$_SERVER['REQUEST_URI'].':'.$name);
		return $name;
	};
	//--------------------------------------------------------------------------
	function get_name($name) {
		$pos1=0;
		while (1) {
			$pos = strpos($name, '/', $pos1);
			if ($pos===false) break;
			$pos1 = $pos + 1;
		};
		$pos = $pos1;
		return substr($name, $pos, strlen($name)-$pos);
	};
	//--------------------------------------------------------------------------
	function redirect($url)
	{
		addtolog('redirect '.$url);
		$output =
		'<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">';
		$output.='<html><head>';
		$output.='<meta http-equiv="content-type" content="text/html; charset=utf-8">';
		$output.='<meta http-equiv="refresh" content="0;';
		$output.=' url='.$url.'">';
		$output.='<link rel="StyleSheet" type="text/css" href="design/style.css">';
		$output.='</head>';
		$output.='<body>';
		$output.='</body></html>';
		@header("HTTP/1.0 200 OK");
		@header("Content-type: text/html;charset=utf-8");
		@header("Cache-Control: no-cache, must-revalidate, max-age=0");
		@header("Expires: 0");
		@header("Pragma: no-cache");
		print $output;
		die();
	};

	function rewritereport($name,$str)
	{
		$file = @fopen ($name,"w+");
		if (strlen($str)>0) @fputs($file, $str);
		@fclose ( $file );
	};
	function rewritereportarr($name,$arr)
	{
		$file = @fopen ($name,"w+");
		foreach($arr as $a) {
			addtolog('rewritereportarr', $a);
			if (strlen($a)>0) @fputs($file, trimall($a)."\x0D\x0A");
		}
		@fclose ( $file );
	};
	//============================================================================
	function save_lots($name,$lots)
	{
		$s =	print_r($lots,true);
		$s =	str_replace('stdClass Object', 'Array', $s);
		rewritereport($name,$s);
	};
	//============================================================================
	//	function trimall($str, $substr = '', $charlist = "\t\n\r\x0B\x97\xAB\xBB")
	function trimall($str, $substr = '', $charlist = "\t\n\r\x0B")
	{
		return str_ireplace(str_split($charlist), $substr, $str);
	};
	//============================================================================
	function trimspaces($str)
	{
		$s = $str;
		while (1) {
			if (strlen($s)>0) {
				if ($s[0] == ' ') { $s = substr($s, 1, strlen($s)-1);	} else { break;};
			} else { break; };
		};
		while (1) {
			if (strlen($s)>0) {
				if ($s[strlen($s)-1] == ' ') { $s = substr($s, 0, strlen($s)-1);	} else { break;};
			} else { break;};
		};
		return $s;
	};
	//--------------------------------------------------------------------------
	function decho($str) {
		echo "<br>".$str;
	};
	//--------------------------------------------------------------------------
	function detect_utf($s){
		$s=urlencode($s); // в некоторых случаях - лишняя операция (закоментируйте)
		$res = 0;
		$j=strlen($s);

		$s2=strtoupper($s);
		$s2=str_replace("%D0",'',$s2);
		$s2=str_replace("%D1",'',$s2);
		$k=strlen($s2);

		$m=1;
		if ($k>0){
			$m=$j/$k;
			if (($m>1.2)&&($m<2.2)){ $res = 1; }
		}
		return $res;
	}
	//----------------------------------------------------------------------
	function rewritefile($name, $val)
	{
		@file_put_contents($name,$val);
		//exec('chmod '.$name.' 0777');
		@file_put_contents($name,$val);
	};
  //----------------------------------------------------------------------
	function print_arr($hdr, $arr = array(), $view=1, $utf = true, $name='debug.log') {
		rewritereport(getpath().'report.txt',print_r($arr,true));
		$file = file(getpath().'report.txt');
		if ($view) decho('------------------'.$hdr.'------------------------------------');
		addtolog('------------------'.$hdr.'------------------------------------', $name);
		foreach($file as $str) {
			$str = str_ireplace("\x0A",'',$str);
			$str = str_ireplace("\x0D",'',$str);
//  if ($utf==true) $str = mb_convert_encoding($str,"UTF-8", "windows-1251");
			if ($str<>'') addtolog($str);
			if (is_string($str))
			$str = str_ireplace("\x20",'&nbsp;',$str);
			if (($str<>'') and ($view))  decho($str);
		};
//		@unlink(getpath().'report.txt');
	};
 //----------------------------------------------------------------------
	function get_field( $name, $table, $where ) {
		$sql   = 'select `'.$name.'` from `'.$table.'` where '.$where;
		$q   = mysqli_query($connect,$sql);
		$row = mysqli_fetch_assoc($q);
		return $row[$name];
	};
  //----------------------------------------------------------------------
	function insert_row($row, $table, $debug=0) {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$sval  = '';
		$sql   = 'insert into `'.$table.'` (';
		foreach($row as $name=>$value) {
			$sql .="`$name`,";
			$sval.="'$value',";
		};
		$sql = substr($sql,0,strlen($sql)-1);
		$sval= substr($sval,0,strlen($sval)-1);
		$sql.= ') values ('.$sval.')';
		$q   = mysqli_query($connect,$sql);
		addtolog('sql='.$sql.' error='.mysqli_error($connect));
		decho('sql='.$sql.' error='.mysqli_error($connect));
		return true;
	};
  //----------------------------------------------------------------------
	function delete_rows($table, $where='') {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$sval  =	'';
		$sql   = 'delete from `'.$table.'`';
		if (strlen($where)>0) $sql.= ' where '.$where;
		$q   = mysqli_query($connect,$sql);
		addtolog('sql='.$sql.' error='.mysqli_error());
		return true;
	};

	//----------------------------------------------------------------------
	function get_update_str($row, $table, $where, $debug=0) {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$sval  = '';
		$sql   = 'update `'.$table.'` set ';
		foreach($row as $name=>$value) $sql .="`$name`='$value',";
		$sql = substr($sql,0,strlen($sql)-1);
		if (strlen($where)>0) $sql.= ' where '.$where;
		return $sql;
	};
  //----------------------------------------------------------------------
	function update_row($row, $table, $where, $debug=0) {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$sql = get_update_str($row, $table, $where, $debug);
		mysqli_query($connect,$sql);
//		if (mysqli_error() || ($debug)) 
		addtolog('update_row sql='.$sql.' error='.mysqli_error($connect));//
//		addtolog('sql='.mb_convert_encoding($sql,"windows-1251","UTF-8").' error='.mysqli_error());
		return true;
	};
  //----------------------------------------------------------------------
	function update_field($name, $value, $table, $where) {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$sql   = 'update `'.$table.'` set `'.$name."`='$value' where ".$where;
		$q   = mysqli_query($connect,$sql);
//		addtolog('sql='.$sql.' error='.mysqli_error());
		$row = mysqli_fetch_assoc($q);
		return true;
	};
	//----------------------------------------------------------------------
	function read_table($select, $table, $where = '', $all=1, $order = '', $limit='') {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$sql = 'select '.$select.' from `'.$table.'`';
		if (strlen($where)>0) $sql.= ' where '.$where;
		if (strlen($order)>0) $sql.= ' order by '.$order;
		if (strlen($limit)>0) $sql.= ' LIMIT '.$limit;

		$q   = mysqli_query($connect,$sql);
//		@addtolog('read_table sql='.$sql.' error='.mysqli_error().' count='.mysqli_num_rows($q));
		$res = 0;
		if ($all) {
			$res = array();
			if ($q)
			while ( $row= mysqli_fetch_assoc($q)) {
				$res[] = $row; 
			};
		} else {
			if ($q)
			if ($row = mysqli_fetch_assoc($q)) {
				if ((strpos($select, '*')===false) && (strpos($select, ',')===false)) $res = $row[$select]; 
				else $res = $row;
			};
		};
//		addtolog("read_table $table res=".print_r($res,true));
//		decho('res='.print_r($res,true));
		return $res;
	};
  //----------------------------------------------------------------------
	function get_row( $table, $where ) {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$sql   = 'select * from `'.$table.'` where '.$where;
		$q   = mysqli_query($connect,$sql);
		echo '<br>sql='.$sql.' error='.mysqli_error();
//		addtolog('sql='.$sql.' error='.mysqli_error());
		$row = mysqli_fetch_assoc($q);
		return $row;
	};
  //----------------------------------------------------------------------
	function check_and_save_table($row, $table, $where) {
		global $dbhost,$dbname,$dbusername, $dbpassword, $connect;
		$arr = read_table('*', $table, $where);
		if (count($arr)>0) {
			update_row($row, $table, $where);
		} else {
			insert_row($row,$table); 
		};
	};
	//--------------------------------------------------------------------------
	function addtolog($str, $name = 'debug.log', $flag=false) {
		global $debuglog_name, $base_time_bot;
//$file = fopen ($name,"w+");
//*
		if (!$flag)
		if (strlen($debuglog_name)>3) $name = $debuglog_name;
	  
//			chmod (getpath().'/', 0777);
		if (file_exists($name)) { 
			if (filesize($name)>(50*1024*1024)) { unlink($name);  $file = fopen ($name,"w+"); } else
			$file = fopen ($name,"a+"); 
		} else { 
			$file = fopen ($name,"w+");
		};
//			@fputs($file, date("d.m.Y h:i:s",time()).' $base_time_bot='.$base_time_bot.' '.$str);
//*/
		@fputs($file, date("d.m.Y h:i:s",time()).' '.$str);
		fputs($file, "\r");
		fclose ( $file );
	};
	//--------------------------------------------------------------------------
	function addtofile($name, $str) {
		if (file_exists($name)) { 
			$file = fopen ($name,"a+"); 
		} else	{ 
			$file = fopen ($name,"w+");
		};
		fputs($file, $str);
		fputs($file, "\n");
		fclose ( $file );
	};
	//--------------------------------------------------------------------------
	function set_field($type, $style, $rows, $cols, $name, $num='_1', $body='', $altname='', $row = array(), $arr = array()) {
		if (count($arr)==0) {
			if (count($row)>0) {
				$pos  = strpos($name,'_');
				if ($pos!==false) {
					$pos++;
					$name = substr($name, $pos, strlen($name)-$pos);
				}
				$f   = $row[$name];
			} else {
				$f = get_value($name,'setup');
			};
			$pos = 0;
			while(1) {
				$pos1  = strpos($name,'.',$pos);
				addtolog('set_field pos1='.$pos1.' $name='.$name);
				if ($pos1===false) break;
				$pos = $pos1 + 1;
			};
			addtolog('set_field pos='.$pos.' $name='.$name);
			$name = substr($name, $pos, strlen($name)-$pos);
			$f = htmlspecialchars($f);
		} else {
			$f = implode(',',$arr);
		}
		if($type=="textarea")
$ret = "<textarea rows=\"{$rows}\" style=\"{$style}\" name=\"{$name}\" cols=\"{$cols}\">".stripslashes($f)."</textarea>";
else if($type=="checkbox") {
$ret = "<input  type=\"checkbox\" id=\"check_1\" ";
if ($f=='on') $ret.= "checked";
$ret.= " onClick=\"set_flag(this)\">";
$ret.= "<input  type=\"hidden\" name=\"{$name}\" id=\"{$name}\"value=\"{$f}\">";
  	} else if($type=="select") {
  $ret = "<select  type=\"select\" style=\"{$style}\" id=\"select\"{$num} name=\"{$name}\">";
  $ret.= $body;
  $ret.= "</select>";
  	} else
  $ret = "<input  type=\"{$type}\" style=\"{$style}\" name=\"{$name}\" value=\"{$f}\">";
//addtolog($type.' '.$style.' '.$rows.' '.$cols.' '.$name.' '.$ret);
echo $ret;
//addtolog($type.' '.$style.' '.$rows.' '.$cols.' '.$name.' '.$ret);
return($ret);
	};
	//--------------------------------------------------------------------------
	function set_textarea($style, $rows, $cols, $tname, $body='', $row = array()) {
		$s = '';
		foreach($row as $name=>$val) {
			$s.= $name.'='.$val."\x0A";
		};
		$ret = "<textarea rows=\"{$rows}\" style=\"{$style}\" name=\"{$tname}\" cols=\"{$cols}\">".stripslashes($s)."</textarea>";
		echo $ret;
		return($ret);
	};
	//--------------------------------------------------------------------------
	function get_input( $p, $tag, $sub, $key, $limit) {
		addtolog('get_input---------------------------------');
		$s = $p;
		$pos = 0;
		addtolog('get_input tag='.$tag.' sub='.$sub.' key='.$key);

		while(1)	{
			$pos = strpos($p,'<'.$tag, $pos);  addtolog('pos='.$pos);
			if (!($pos>1)) return false;   addtolog('pos='.$pos);
			$pos1 = strpos($p,'>', $pos);  addtolog('pos1='.$pos1);
			if (!($pos1>1)) return false;
			$s1 = substr($s, $pos, $pos1 - $pos);  addtolog('s1(1)='.$s1);
			if (strpos( $s1, $sub) >0) {
				$pos2 = strpos($s1, $sub); 
				if (!($pos2>1)) { 
					$pos = $pos1; 
				} else {
					$pos2 = strpos($s1, $key); 
					if ($pos2>1){
						$s1 = substr($s1, $pos2, strlen($s1)-$pos2);addtolog('s1(2)='.$s1);
						$pos2 = strpos($s1, $limit); 
						$s1 = substr($s1, $pos2+1, strlen($s1)-$pos2);  addtolog('s1(3)='.$s1);
						$pos2 = strpos($s1, $limit);   
						$s1 = substr($s1, 0, $pos2);addtolog('s1(4)='.$s1);
						return $s1;
					} else {
						$pos = $pos1;
					};
				};
			} else {
				$pos = $pos1;
			};
		};
		return false;
	};
  //-------------------------------------------------------------------------
	function sendmail($to,$from_mail,$from_name,$subject,$message) {
		ini_set('smtp_port','25');
		ini_set('SMTP','mail.feshmebel.com.ua');
		$header="from: ".$from_name." <".$from_mail.">\n";
		$header.="to: ".$to."\n";
		$header.="subject: ".$subject."\n";
		$header.="mime-version: 1.0\n";
		$header.="content-type: text/html; charset=windows-1251\n";
		$header.="Content-Transfer-Encoding: 8bit\n";
		$res = mail($to, $subject, $message, $header);
		addtolog('sendmail header='.$header);
		addtolog('sendmail res='.$res);
		return $res;
	}
  //-------------------------------------------------------------------
	function tolower($phrases) {
//foreach($phrases as $key=>$phrase) {
		$phrase = $phrases;

		$phrase =  mb_convert_encoding($phrase, "CP1251","UTF-8");
		$phrase =  mb_strtolower($phrase);
		$phrase =  str_replace("\xDF", "\xFF", $phrase);
		$phrase =  mb_convert_encoding($phrase,"UTF-8", "CP1251");
//  	$phrases[$key] = $phrase;
//  	};
		return $phrase;
		return $phrases;
	};
  //-------------------------------------------------------------------
	function toupper($phrases) {
		foreach($phrases as $key=>$phrase) {
			$phrase =  mb_convert_encoding($phrase, "CP1251","UTF-8");
			$ch =  strtoupper(substr($phrase,0,1));
			$pos = 0;
			if ($ch=='+') {
				$pos++;
				$ch =  strtoupper(substr($phrase,$pos,1));
				if ($ch=='я') $ch = 'Я';
				$phrase =  '+'.$ch.substr($phrase,2,strlen($phrase)-2);
			} else 
				if ($ch=="\xAB") {
					$ch =  strtoupper(substr($phrase,0,1));
					if ($ch=='я') $ch = 'Я';
					$phrase =  "\xAB".$ch.substr($phrase,0,strlen($phrase)-1);
				} else {
					$ch =  strtoupper(substr($phrase,0,1));
					if ($ch=='я') $ch = 'Я';
					$phrase =  $ch.substr($phrase,1,strlen($phrase)-1);
				};
				$phrase =  mb_convert_encoding($phrase,"UTF-8", "CP1251");
				$phrases[$key] = $phrase;
			};
		return $phrases;
	};
// ============================================================
	function utf8($struct) {
		foreach ($struct as $key => $value) {
			if (is_array($value)) {
				$struct[$key] = utf8($value);
			}
			elseif (is_string($value)) {
				$struct[$key] = utf8_encode($value);
			}
		}
		return $struct;
	}
  //-------------------------------------------------------------------------
	//вынимаем JSON со страницы
	function getRetCode($ret){
		global	$json;
		$pos  = strpos( $ret, "\x0D\x0A\x0D\x0A") + 4;
		$res  = array();
		if ($pos) {
			$ret = substr( $ret, $pos, strlen($ret) - $pos);
			$json= $ret;
			$res = JSON::decode($ret);
		};
		return $res;
	}
  //-------------------------------------------------------------------------
	function ReadIniFile($name='bot.cfg') {
		global $username, $password;

		$name = getpath().$name;
		$file = file($name);
		addtolog('ReadIniFile-----$name='.$name);

		$cfg = array();
		foreach($file as $s) {
			addtolog('ReadIniFile s='.$s);
			$pos = strpos($s,'//');
			if ($pos!==false) $s = substr($s, 0, $pos);
			$a = explode('=',$s);
			if (count($a)<2) continue; // если коммент или просто текст
			$cfg[trim($a[0])] = trimall(trim($a[1]));
		};
		return $cfg;
	};
  //-------------------------------------------------------------------------
	function SaveIniFile($setup=array(),$fname='4yandex.cfg') {

//		addtolog('SaveIniFile---------------------------------');
		$file = @fopen ($fname,"w+");
		foreach($setup as $name=>$val) {
			$s = $name.'='.$val;
			addtolog('SaveIniFile $s='.$s);
			if (strlen($s)>0) @fputs($file, trimall($s)."\x0A");
		};
		@fclose ( $file );
	};
	  
	//-------------------------------------------------------------------------
	function check_and_die($bot_id){
		db_connect();
		$res = read_table( '*', 'states',"`bot_id`='$bot_id'");
		mysqli_close(); 		
		$delta	= time() - $state['dt_last'];
		if ($delta<5) {
			addtolog('die $delta='.$delta);
			die();
		}
	};	  
	date_default_timezone_set('Europe/Kiev');
