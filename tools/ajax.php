<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');
	require_once('functions.php');
	global $dbhost,$dbname,$dbusername, $dbpassword, $connect;

	$languages = preg_match("/ua/", $_SERVER['REQUEST_URI']);
	
	if(isset($languages) and $languages == TRUE)
	{
		$my_language_id = 2;
	}else{
		$my_language_id = 1;
	}

	if (isset($_REQUEST['search_string']))
	{
		$search	= $_REQUEST['search_string'];
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM oc_review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM oc_product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = 0 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";
		$sql .= " AND (";
		$sql = "SELECT *,  (SELECT price FROM oc_product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = 1 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special FROM oc_product p ";
		$sql .=	"LEFT JOIN  `oc_product_description` ON (  p.product_id =  `oc_product_description`.product_id )";
		$sql .= "WHERE ";
//		$sql .= " ( LCASE(p.model) LIKE '%" . mb_strtolower($search) . "%'";
		$sql .= " ( LCASE(oc_product_description.name) LIKE '%" . mb_strtolower($search, 'UTF-8') . "%'";
		$sql .= " OR LCASE(p.sku) LIKE '%" . mb_strtolower($search, 'UTF-8') . "%'";
		$sql .= " OR LCASE(p.upc) LIKE '%" . mb_strtolower($search, 'UTF-8') . "%'";
		$sql .= " OR LCASE(p.ean) LIKE '%" . mb_strtolower($search, 'UTF-8') . "%'";
		$sql .= " OR LCASE(p.jan) LIKE '%" . mb_strtolower($search, 'UTF-8') . "%'";
		$sql .= " OR LCASE(p.isbn) LIKE '%" . mb_strtolower($search, 'UTF-8') . "%'";
		$sql .= " OR LCASE(p.mpn) LIKE '%" . mb_strtolower($search, 'UTF-8') . "%'";
//		$sql .= " AND `oc_product_description`.language_id = '" . $my_language_id . "'";
		$sql .= ")";
		$sql .= " AND p.stock_status_id in (1,2,3,4,5,7,8,10)";
		$sql .= " GROUP BY p.product_id";
		$sql .= " ORDER BY p.sort_order";
		$sql .= " DESC";

		if (isset($_REQUEST['start']) || isset($_REQUEST['limit'])) {
			if ($_REQUEST['start'] < 0) {
				$_REQUEST['start'] = 0;
			}

			if ($_REQUEST['limit'] < 1) {
				$_REQUEST['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$_REQUEST['start'] . "," . (int)$_REQUEST['limit'];
		} else
			$sql .= " LIMIT 0,10";

		$product_data = array();


		db_connect();
		$res = mysqli_query( $connect, $sql );
		addtolog('sql='.$sql.' error='.mysqli_error($connect));
		addtolog('$res='.print_r($res,1));

		$products = array();
		while ($row = $res->fetch_array()) {
			$products[] = $row;
		}
		addtolog('$products='.print_r($products,1));

		$sql = "SELECT * FROM `oc_suppliers`";
		$_suppliers = mysqli_query( $connect, $sql );
		addtolog('sql='.$sql.' error='.mysqli_error($connect));

		$suppliers = array();
		if (@count($_suppliers) > 0)
		foreach($_suppliers as $supplier)
			$suppliers[$supplier['supplier_id']] = $supplier;
		unset($_suppliers);
		addtolog('$suppliers='.print_r($suppliers,1));

		$html = '';
/*
		$html = '
<div style="display: block;" class="search-result-ajax">
		';
*/



		if (isset($products[0])){
			foreach($products as $product) {
				if (strpos( $product['image'],'http://') === false) {
					$product['image'] = '//'.$_SERVER['SERVER_NAME'].'/image/'.$product['image'];
				};

				$href = '/index.php?route=product/product&product_id='.$product['product_id'];
				$html .= '
	<div class="search-result-row-wrap">
	<a class="search-result-row-ajax" href="'.$href.'">
		<span class="search-result-image-ajax">
			<img alt="'.$product['name'].'" title="'.$product['name'].'" src="'.$product['image'].'">
		</span>
		<span class="search-result-body-ajax">
			<span class="search-result-articul-ajax">Артикул '.$product['sku'].'</span>
			<span class="search-result-text-ajax">'.$product['name'].'</span>';
			if ((int)$product['supplier_id'] > 0 ) {
			   if(isset($suppliers[$product['supplier_id']]['coefficient']) )

			$product['price'] = (float)$product['price']*(float)$suppliers[$product['supplier_id']]['coefficient'];
			};
			$price = $product['price'];
			$price = ceil($price).' грн.';
                if(!empty($product['special'])){
                    $special = ceil($product['special']).' грн';
                    $html .= '
			<span class="search-result-price-ajax">'. $special .'</span>
			<span class="search-result-price-ajax-old">'. $price .'</span>
			';
                }  else {
                    $html .= '
			<span class="search-result-price-ajax">'. $price .'</span>';
                }

                $html .= '
		</span>
	</a>
	</div>
				';

			}
/*
			$html .= '
</div>
			';
*/
			addtolog('$html='.$html);
			echo $html;
		}
		die();
	}

	switch ($_REQUEST['action'])
	{
		case 'specialOnOff':
			db_connect();
			decho('specialOnOff');

			session_start();
			$_SESSION['ob_product_special_on'] = (int)$_REQUEST['val'];

			$row = array(
				'code'		=> 'config',
				'value'		=> $_SESSION['ob_product_special_on'],
				'serialized'=> 0,
			);

			$res = read_table('oc_setting',"`key`='ob_product_special_on'");
			if (isset($res[0])){
				update_row( $row, 'oc_setting',"`key`='ob_product_special_on'");
			} else {
				$row['key'] = 'ob_product_special_on';
				insert_row( $row, 'oc_setting');
			}

			$res = read_table('*','oc_product_special');
//			print_arr('$res',$res);

			foreach($res as $i=>$r){
				$row = array();
				if ((int)$r['product_id']>0)
				{
					$row['sproduct_id'] = $r['product_id'];
				};

				if 	((int)$_REQUEST['val'] > 0)
				{
					if ((int)$r['product_id']>0)
					{
						update_row( $row, 'oc_product_special',"`product_id`='{$r['product_id']}'");
					} else
					if ((int)$r['sproduct_id']>0)
					{
						$row['product_id'] = $r['sproduct_id'];
						update_row( $row, 'oc_product_special',"`sproduct_id`='{$r['sproduct_id']}'");
					}
				} else
				if (((int)$r['sproduct_id']>0) && ((int)$r['product_id']>0))
				{
					$row['product_id'] = 0;
					update_row( $row, 'oc_product_special',"`sproduct_id`='{$r['sproduct_id']}'");
				};
			};

			$res = read_table('*','oc_product_special');
		addtolog('$_SESSION[ob_product_special_on] ='.$_SESSION['ob_product_special_on']);
			session_write_close();
			die();
			break;
	};