const fs = require('fs');
var request = require('/var/www/feshmebel/data/www/feshmebel.com.ua/google_console/node_modules/request');
var { google } = require('/var/www/feshmebel/data/www/feshmebel.com.ua/google_console/node_modules/googleapis');
var key = require('/var/www/feshmebel/data/www/feshmebel.com.ua/google_console/service_account.json');

const jwtClient = new google.auth.JWT(
  key.client_email,
  null,
  key.private_key,
  ['https://www.googleapis.com/auth/indexing'],
  null
);

const batch = fs
  .readFileSync('/var/www/feshmebel/data/www/feshmebel.com.ua/google_console/urls.txt')
  .toString()
  .split('\n');

jwtClient.authorize(function(err, tokens) {
  if (err) {
    console.log(err);
    return;
  }

  const items = batch.map(line => {
    return {
      'Content-Type': 'application/http',
      'Content-ID': '',
      body:
        'POST /v3/urlNotifications:publish HTTP/1.1\n' +
        'Content-Type: application/json\n\n' +
        JSON.stringify({
          url: line,
          type: 'URL_UPDATED'
        })
    };
  });

  const options = {
    url: 'https://indexing.googleapis.com/batch',
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/mixed'
    },
    auth: { bearer: tokens.access_token },
    multipart: items
  };
  request(options, (err, resp, body) => {
  
    request.post({url:'https://feshmebel.com.ua/google_console/result.php', form: {urls:batch,result:body} }, function(err,httpResponse,body){
      console.log(body);
    })
  });
});
