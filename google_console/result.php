<?php
require_once('/var/www/feshmebel/data/www/feshmebel.com.ua/config.php');
$DSN = 'mysql:host=%s;dbname=%s';

$opts = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
);

try {
    $db = new PDO(sprintf($DSN, DB_HOSTNAME, DB_DATABASE), DB_USERNAME, DB_PASSWORD, $opts);
} catch (PDOException $ex) {
    die('Database Connection Error: #' . $ex->getCode() . ' > ' . $ex->getMessage());
}

$text = array();
foreach ($_POST['urls'] as $url)
{
    $db->query("UPDATE `ocp_index` SET `sort_order` = `sort_order` + 1, `google`='error', `date_modified` = '".date("Y-m-d H:i:s")."' WHERE `url` = '".$url."';");
}

preg_match_all('/"url": "(.*?)"/',$_POST['result'],$matches);
$result = array_unique($matches[1]);
sort($result);
print_r($result);
foreach ($result as $url)
{
    $db->query("UPDATE `ocp_index` SET `google`='update' WHERE `url` = '".$url."';");
}
